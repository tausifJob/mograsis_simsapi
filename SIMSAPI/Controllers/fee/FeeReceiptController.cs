﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Globalization;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/FeeReceipt")]
    public class FeeReceiptController : ApiController
    {
        //string source_format = "dd/MM/yyyy";
        //string desti_format = "yyyy/MM/dd";

        [Route("getAllRecordsFeeReceipt")]
        public HttpResponseMessage getAllRecordsFeeReceipt(string from_date, string to_date, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {

                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date =dr["creation_date"].ToString();

                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = dr["doc_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.Doc_status = dr["Doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getAllRecordsFeeReceiptNew")]
        public HttpResponseMessage getAllRecordsFeeReceiptNew(string from_date, string to_date, string search, string doc_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@doc_no",doc_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date = dr["creation_date"].ToString();
                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = dr["doc_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.Doc_status = dr["Doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getAllRecordsFeeReceiptNew1_ABQIS")]
        public HttpResponseMessage getAllRecordsFeeReceiptNew1_ABQIS(string from_date, string to_date, string search, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                if (search == "undefined" || search == "" || search == "null")
                {
                    search = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc_new1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@doc_academic_year",ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.dt_code = dr["dt_code"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date = dr["creation_date"].ToString();
                            simsobj.creation_date = dr["creation_date"].ToString();
                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = dr["doc_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.doc_status_new = dr["Doc_status"].ToString();
                            simsobj.creation_user_name = dr["creation_user_name"].ToString();
                            simsobj.cash_amount = dr["cash_amount"].ToString();
                            simsobj.cheque_amount = dr["cheque_amount"].ToString();
                            simsobj.credit_card_amount = dr["credit_card_amount"].ToString();
                            simsobj.other_amount = dr["other_amount"].ToString();
                            simsobj.comp_curcy_dec = dr["comp_curcy_dec"].ToString();
                            simsobj.doc_narration = dr["doc_narration"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
              //  message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        [Route("getAllRecordsFeeReceiptNew1")]
        public HttpResponseMessage getAllRecordsFeeReceiptNew1(string from_date, string to_date, string search, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                if (search == "undefined" || search == "" || search == "null")
                {
                    search = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc_new1]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@doc_academic_year",ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.dt_code = dr["dt_code"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date = dr["creation_date"].ToString();
                            simsobj.creation_date = dr["creation_date"].ToString();
                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = dr["doc_date"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.doc_status_new = dr["Doc_status"].ToString();
                            simsobj.creation_user_name = dr["creation_user_name"].ToString();
                            simsobj.cash_amount = dr["cash_amount"].ToString();
                            simsobj.cheque_amount = dr["cheque_amount"].ToString();
                            simsobj.credit_card_amount = dr["credit_card_amount"].ToString();
                            simsobj.other_amount = dr["other_amount"].ToString();
                            simsobj.comp_curcy_dec = dr["comp_curcy_dec"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                //  message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }



        
        [Route("DocDetailsGet")]
        public HttpResponseMessage DocDetailsGet(Sim615 search)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_fee_receipt_proc_New]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@doc_no",search.doc_no),
                            new SqlParameter("@academic_year",search.doc_academic_year)
                         });
                    ds.DataSetName = "DS";
                    object o = ds;
                    return Request.CreateResponse(HttpStatusCode.OK, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("UpdateDoc")]
        public HttpResponseMessage UpdateDoc(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("sims.sims_fee_receipt_proc_New", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("sims.sims_fee_receipt_proc_New",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","F"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("Cashier")]
        public HttpResponseMessage Cashier()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.cashier_code = dr["creation_user"].ToString();
                            simsobj.cashier_name = dr["emp_name"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("PaymentMode")]
        public HttpResponseMessage PaymentMode()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","P")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.payment_mode_code = dr["sims_appl_parameter"].ToString();
                            simsobj.payment_mode_name = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("FeeType")]
        public HttpResponseMessage FeeType()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","F")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("FeeCollection1")]
        public HttpResponseMessage FeeCollection1(Sim615 abc)
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(abc.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(abc.to_date)),
                            new SqlParameter("@Cashier",abc.cashier_code),
                            new SqlParameter("@payment_mode",abc.payment_mode_code),
                              new SqlParameter("@search",abc.enroll_number)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.enroll_number = dr["enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.doc_reference_no = dr["doc_reference_no"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.dd_fee_cheque_date = db.UIDDMMYYYYformat(dr["dd_fee_cheque_date"].ToString());
                            simsobj.bank_name = dr["bank_name"].ToString();
                            simsobj.fee_paid = dr["fee_paid"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("FeeCollection")]
        public HttpResponseMessage FeeCollection(Sim615 abc)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[sims_fee_collection_summary_proc]",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(abc.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(abc.to_date)),
                            new SqlParameter("@Cashier",abc.cashier_code),
                            new SqlParameter("@payment_mode",abc.payment_mode_code),
                              new SqlParameter("@search",abc.enroll_number)
                         });

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
        }

        [Route("getAllRecordsClasswiseFeeReceipt")]
        public HttpResponseMessage getAllRecordsClasswiseFeeReceipt(string sims_cur_code,string sims_academic_year, string sims_grade_code,string sims_section_code, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if(sims_grade_code == "undefined")
                {
                    from_date = null;
                    sims_grade_code = null;
                }
                if (sims_section_code == "undefined")
                {
                    sims_section_code = null;
                }



                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_multiple_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_academic_year",sims_academic_year),
                            new SqlParameter("@sims_grade_code",sims_grade_code),
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            simsobj.enroll_number = dr["enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.receipt_status = dr["receipt_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }


        [Route("getReoprtParamenterClasswise")]
        public HttpResponseMessage getReoprtParamenterClasswise()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_multiple_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","B")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                           // simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("getViewFor")]
        public HttpResponseMessage getViewFor()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","R")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("getAcademicstatus")]
        public HttpResponseMessage getAcademicstatus()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","K")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            

                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("getViewmode")]
        public HttpResponseMessage getViewmode()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","T")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();


                            simsobj.comn_appl_parameter2 = dr["comn_appl_parameter2"].ToString();
                            simsobj.comn_appl_form_field_value12 = dr["comn_appl_form_field_value12"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("getperiodsdetails")]
        public HttpResponseMessage getperiodsdetails(string view_mode, string cur_code, string acad_year)
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","W"),
                            new SqlParameter("@view_mode",view_mode),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();


                            simsobj.comn_appl_parameter1 = dr["comn_appl_parameter1"].ToString();
                            simsobj.comn_appl_form_field_value11 = dr["comn_appl_form_field_value11"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("getFeeType")]
        public HttpResponseMessage getFeeType(string cur_code, string grade_code, string academic_year,string section)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sim615> mod_list = new List<Sim615>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "L"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@acad_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code),
                                 new SqlParameter("@section_code",section)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim615 simsobj = new Sim615();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }




        //Defaulter LIst APIS



        [Route("getAllDefaulterListdata")]
        public HttpResponseMessage getAllDefaulterListdata(string cur_code, string academic_year,string grade_code, string section,string feetype,string academic_status,string enrollno,string doc_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            if(enrollno == "undefined")
            {
                enrollno = null;
            }
            List<Sim615> mod_list = new List<Sim615>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@acad_year", academic_year),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code",section),
                                new SqlParameter("@fee_code",feetype),
                                new SqlParameter("@acad_status",academic_status),
                                new SqlParameter("@enroll_number",enrollno),
                                new SqlParameter("@doc_date",db.DBYYYYMMDDformat(doc_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            Sim615 simsobj = new Sim615();
                            
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.sims_fee_category = dr["sims_fee_number"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            simsobj.sims_fee_period_term_name = dr["sims_fee_period_term_name"].ToString();
                            simsobj.final_total = dr["final_total"].ToString();
                            
                            simsobj.sims_parent_contact = dr["sims_parent_contact"].ToString();
                            simsobj.pending_memo = dr["pending_memo"].ToString();

                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();

                            

                            
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getAllDefaulterListdataNew")]
        public HttpResponseMessage getAllDefaulterListdataNew(string cur_code, string academic_year, string grade_code, string section, string feetype, string academic_status, string enrollno, string doc_date)

        {
            string  str = string.Empty;
            List<Sim615> mod_list = new List<Sim615>();
            List<outstand> mod_list1 = new List<outstand>();
            object[] ob = new object[2];

            if (enrollno == "undefined")
            {
                enrollno = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                     new List<SqlParameter>()
                      {
                                                 new SqlParameter("@opr", "S"),
                                                 new SqlParameter("@cur_code", cur_code),
                                                 new SqlParameter("@acad_year", academic_year),
                                                 new SqlParameter("@grade_code", grade_code),
                                                 new SqlParameter("@section_code",section),
                                                 new SqlParameter("@fee_code",feetype),
                                                 new SqlParameter("@acad_status",academic_status),
                                                 new SqlParameter("@enroll_number",enrollno),
                                                 new SqlParameter("@doc_date",db.DBYYYYMMDDformat(doc_date)),
                                             


                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            string str2 = "";

                            Sim615 simsobj = new Sim615();

                            str1 = dr["sims_enroll_number"].ToString();

                            str2 = dr["sims_fee_code"].ToString();

                            var v = from p in mod_list where p.sims_enroll_number == str1  select p;

                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            simsobj.sims_fee_period_term_name = dr["sims_fee_period_term_name"].ToString();
                            simsobj.final_total = dr["final_total"].ToString();

                            simsobj.sims_parent_contact = dr["sims_parent_contact"].ToString();
                            simsobj.pending_memo = dr["pending_memo"].ToString();

                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();

                            simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();


                            simsobj.feedes = new List<feeDesc>();

                           
                            feeDesc h = new feeDesc();
                            h.sims_fee_code = dr["sims_fee_code"].ToString();

                            h.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            h.sims_fee_period_paid = dr["sims_fee_period_paid"].ToString();
                           // h.sims_status = dr["assign"].ToString().Equals("1") ? true : false;

                            if (v.Count() == 0)
                            {
                                simsobj.feedes.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                var c = from p in v where p.sims_enroll_number == str1 &&p.sims_fee_code==str2 select p ;
                                if(c.Count() == 0)
                                v.ElementAt(0).feedes.Add(h);

                            }
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {

                            outstand simsobj = new outstand();
                            simsobj.cnt_90 = dr["cnt_90"].ToString();
                            simsobj.cnt_75 = dr["cnt_75"].ToString();
                            simsobj.cnt_50 = dr["cnt_50"].ToString();
                            simsobj.cnt_30 = dr["cnt_30"].ToString();
                            simsobj.cnt_bel_30 = dr["cnt_bel_30"].ToString();
                            simsobj.bal_90 = dr["bal_90"].ToString();
                            simsobj.bal_75 = dr["bal_75"].ToString();
                            simsobj.bal_50 = dr["bal_50"].ToString();
                            simsobj.bal_30 = dr["bal_30"].ToString();
                            simsobj.bal_bel_30 = dr["bal_bel_30"].ToString();
                            //   total_cnt_90 += float.Parse(simsobj.cnt_90);
                            mod_list1.Add(simsobj);
                        }

                       
                        ob[0] = new object();
                        ob[1] = new object();

                        ob[0] = mod_list;
                        ob[1] = mod_list1;

                        return Request.CreateResponse(HttpStatusCode.OK, ob);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, ob);


        }

        //[Route("getAllDefaulterListdataNewALLGrade")]
        //public HttpResponseMessage getAllDefaulterListdataNew(string cur_code, string academic_year, string academic_status, string enrollno, string doc_date)
        //{
        //    string str = string.Empty;
        //    List<Sim615> mod_list = new List<Sim615>();
        //    if (enrollno == "undefined")
        //    {
        //        enrollno = null;
        //    }
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
        //             new List<SqlParameter>()
        //              {
        //                                         new SqlParameter("@opr", "C"),
        //                                         new SqlParameter("@cur_code", cur_code),
        //                                         new SqlParameter("@acad_year", academic_year),
        //                                         new SqlParameter("@acad_status",academic_status),
        //                                         new SqlParameter("@enroll_number",enrollno),
        //                                         new SqlParameter("@doc_date",db.DBYYYYMMDDformat(doc_date)),
        //              });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    string str1 = "";
        //                    string str2 = "";

        //                    Sim615 simsobj = new Sim615();

        //                    str1 = dr["sims_enroll_number"].ToString();

        //                    str2 = dr["sims_fee_code"].ToString();

        //                    var v = from p in mod_list where p.sims_enroll_number == str1 select p;

        //                    simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
        //                    simsobj.student_name = dr["student_name"].ToString();
        //                    simsobj.sims_fee_number = dr["sims_fee_number"].ToString();
        //                    simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
        //                    simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
        //                    simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
        //                    simsobj.section_name = dr["section_name"].ToString();
        //                    simsobj.sims_fee_period_term_name = dr["sims_fee_period_term_name"].ToString();
        //                    simsobj.final_total = dr["final_total"].ToString();

        //                    simsobj.sims_parent_contact = dr["sims_parent_contact"].ToString();
        //                    simsobj.pending_memo = dr["pending_memo"].ToString();

        //                    simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
        //                    simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();

        //                    simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
        //                    simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
        //                    simsobj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
        //                    simsobj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();


        //                    simsobj.feedes = new List<feeDesc>();


        //                    feeDesc h = new feeDesc();
        //                    h.sims_fee_code = dr["sims_fee_code"].ToString();

        //                    h.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
        //                    h.sims_fee_period_paid = dr["sims_fee_period_paid"].ToString();
        //                    // h.sims_status = dr["assign"].ToString().Equals("1") ? true : false;
        //                }

        //                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        //}


        ///
        [Route("getAllDefaulterListdataNewALLGrade")]
        public HttpResponseMessage getAllDefaulterListdataNewALLGrade(string cur_code, string academic_year, string academic_status, string enrollno, string doc_date)
        {
            List<Sim615> goaltarget_list = new List<Sim615>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                        new List<SqlParameter>()
                         {
                                      new SqlParameter("@opr", "C"),
                                      new SqlParameter("@cur_code", cur_code),
                                      new SqlParameter("@acad_year", academic_year),
                                      new SqlParameter("@acad_status",academic_status),
                                      new SqlParameter("@enroll_number",enrollno),
                                      new SqlParameter("@doc_date",db.DBYYYYMMDDformat(doc_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            string str2 = "";

                            Sim615 simsobj = new Sim615();

                            str1 = dr["sims_enroll_number"].ToString();

                            str2 = dr["sims_fee_code"].ToString();

                            var v = from p in goaltarget_list where p.sims_enroll_number == str1 select p;

                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            simsobj.sims_fee_period_term_name = dr["sims_fee_period_term_name"].ToString();
                            simsobj.final_total = dr["final_total"].ToString();

                            simsobj.sims_parent_contact = dr["sims_parent_contact"].ToString();
                            simsobj.pending_memo = dr["pending_memo"].ToString();

                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();

                            simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();


                            simsobj.feedes = new List<feeDesc>();


                            feeDesc h = new feeDesc();
                            h.sims_fee_code = dr["sims_fee_code"].ToString();

                            h.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            h.sims_fee_period_paid = dr["sims_fee_period_paid"].ToString();
                            // h.sims_status = dr["assign"].ToString().Equals("1") ? true : false;
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }
        ///





        [Route("getAllDefaulterListdataNew_pearl")]
        public HttpResponseMessage getAllDefaulterListdataNew_pearl(string cur_code, string academic_year, string grade_code, string section, string feetype, string academic_status, string enrollno, string doc_date, string view_mode, string period_details)
        {
            string str = string.Empty;
            List<Sim615> mod_list = new List<Sim615>();

            if (enrollno == "undefined")
            {
                enrollno = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
                     new List<SqlParameter>()
                      {
                                                 new SqlParameter("@opr", "S"),
                                                 new SqlParameter("@cur_code", cur_code),
                                                 new SqlParameter("@acad_year", academic_year),
                                                 new SqlParameter("@grade_code", grade_code),
                                                 new SqlParameter("@section_code",section),
                                                 new SqlParameter("@fee_code",feetype),
                                                 new SqlParameter("@acad_status",academic_status),
                                                 new SqlParameter("@enroll_number",enrollno),
                                                 new SqlParameter("@doc_date",db.DBYYYYMMDDformat(doc_date)),
                                              //   new SqlParameter("@comn_appl_parameter",view_mode),
                                                 new SqlParameter("@period_code",period_details),


                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            string str2 = "";

                            Sim615 simsobj = new Sim615();

                            str1 = dr["sims_enroll_number"].ToString();

                            str2 = dr["sims_fee_code"].ToString();

                            var v = from p in mod_list where p.sims_enroll_number == str1 select p;

                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.sims_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name = dr["sims_parent_name"].ToString();
                            simsobj.section_name = dr["section_name"].ToString();
                            simsobj.sims_fee_period_term_name = dr["sims_fee_period_term_name"].ToString();
                            simsobj.final_total = dr["final_total"].ToString();

                            simsobj.sims_parent_contact = dr["sims_parent_contact"].ToString();
                            simsobj.pending_memo = dr["pending_memo"].ToString();

                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();


                            simsobj.feedes = new List<feeDesc>();


                            feeDesc h = new feeDesc();
                            h.sims_fee_code = dr["sims_fee_code"].ToString();

                            h.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            h.sims_fee_period_paid = dr["sims_fee_period_paid"].ToString();
                            // h.sims_status = dr["assign"].ToString().Equals("1") ? true : false;

                            if (v.Count() == 0)
                            {
                                simsobj.feedes.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                var c = from p in v where p.sims_enroll_number == str1 && p.sims_fee_code == str2 select p;
                                if (c.Count() == 0)
                                    v.ElementAt(0).feedes.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }



        //[Route("getAllOutStandingList")]
        //public HttpResponseMessage getAllOutStandingList(string cur_code, string academic_year, string grade_code, string section, string feetype, string param_type, string param1, string param2, string search)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
        //    //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

        //    if (param1 == "undefined"){param1 = null;}
        //    if (param2 == "undefined") { param2 = null; }
        //    if (search == "undefined") { search = null; }
        ////    float total_cnt_90 = 0;
        //    List<outstand> mod_list = new List<outstand>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_defaulter_list_on_date]",
        //                new List<SqlParameter>()
        //                 {
        //                        new SqlParameter("@opr", "B"),
        //                        new SqlParameter("@cur_code", cur_code),
        //                        new SqlParameter("@acad_year", academic_year),
        //                        new SqlParameter("@grade_code", grade_code),
        //                        new SqlParameter("@section_code",section),
        //                        new SqlParameter("@fee_code",feetype),
        //                        new SqlParameter("@param_type",param_type),
        //                        new SqlParameter("@param1",param1),
        //                        new SqlParameter("@param2",param2),
        //                        new SqlParameter("@search",search),
                             
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {


        //                    outstand simsobj = new outstand();
        //                    simsobj.cnt_90 = dr["cnt_90"].ToString();
        //                    simsobj.cnt_75 = dr["cnt_75"].ToString();
        //                    simsobj.cnt_50 = dr["cnt_50"].ToString();
        //                    simsobj.cnt_30 = dr["cnt_30"].ToString();
        //                    simsobj.cnt_bel_30 = dr["cnt_bel_30"].ToString();
        //                    simsobj.bal_90 = dr["bal_90"].ToString();
        //                    simsobj.bal_75 = dr["bal_75"].ToString();
        //                    simsobj.bal_50 = dr["bal_50"].ToString();
        //                    simsobj.bal_30 = dr["bal_30"].ToString();
        //                    simsobj.bal_bel_30 = dr["bal_bel_30"].ToString();
        //                 //   total_cnt_90 += float.Parse(simsobj.cnt_90);
        //                    mod_list.Add(simsobj);
        //                }
        //            }
        //        }
        //     //   object s[] = new object();
                

        //        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //    }
        //    catch (Exception e)
        //    {
        //        //Log.Error(e);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
        //    }
        //}



        [Route("gettransactiondetail")]
        public HttpResponseMessage gettransactiondetail(string frmdate, string todate, string auth_code, string enroll,string amt)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));
            if (frmdate == "undefined")
            {
                frmdate = null;
            }

            if (todate == "undefined")
            {
                todate = null;
            }

            if (auth_code == "undefined")
            {
                auth_code = null;
            }
            if (enroll == "undefined")
            {
                enroll = null;
            }
            if (amt == "undefined")
            {
                amt = null;
            }

            List<appro> mod_list = new List<appro>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_online_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {

                                 new SqlParameter("@opr", "S"),
                                 new SqlParameter("@from_date", db.DBYYYYMMDDformat(frmdate)),
                                 new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
                                 new SqlParameter("@temp_dd_fee_auth_code", auth_code),
                                 new SqlParameter("@temp_dd_enroll_number",enroll),
                                 new SqlParameter("@temp_dd_fee_amount",amt)

                                 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            appro simsobj = new appro();
                            simsobj.temp_dd_doc_no = dr["temp_dd_doc_no"].ToString();
                            simsobj.temp_dd_doc_date = db.DBYYYYMMDDformat(dr["temp_dd_doc_date"].ToString());
                            simsobj.temp_dd_enroll_number = dr["temp_dd_enroll_number"].ToString();
                            simsobj.total_amt = dr["total_amt"].ToString();
                            simsobj.name = dr["name"].ToString();
                            simsobj.temp_dd_fee_auth_code = dr["temp_dd_fee_auth_code"].ToString();
                            try
                            {
                                simsobj.temp_dd_grade = dr["sims_grade_name_en"].ToString();
                                simsobj.temp_dd_section = dr["sims_section_name_en"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }                    
                                 
                            simsobj.temp_dd_fee_card_last_number = dr["temp_dd_fee_card_last_number"].ToString();
                            simsobj.temp_dd_fee_transaction_id = dr["temp_dd_fee_transaction_id"].ToString();
                            simsobj.temp_dd_doc_date_time = dr["temp_dd_doc_date_time"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("gettransactiondetail_IIS_FAILURE")]
        public HttpResponseMessage gettransactiondetail_IIS_FAILURE(string frmdate, string todate, string auth_code, string enroll, string amt)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : gettransactiondetail_IIS_FAILURE(),PARAMETERS :: NO";
            
            if (frmdate == "undefined")
            {
                frmdate = null;
            }

            if (todate == "undefined")
            {
                todate = null;
            }

            if (auth_code == "undefined")
            {
                auth_code = null;
            }
            if (enroll == "undefined")
            {
                enroll = null;
            }
            if (amt == "undefined")
            {
                amt = null;
            }

            List<appro> mod_list = new List<appro>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_online_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {

                                 new SqlParameter("@opr", "F"),
                                 new SqlParameter("@from_date", db.DBYYYYMMDDformat(frmdate)),
                                 new SqlParameter("@to_date", db.DBYYYYMMDDformat(todate)),
                                 new SqlParameter("@temp_dd_fee_auth_code", auth_code),
                                 new SqlParameter("@temp_dd_enroll_number",enroll),
                                 new SqlParameter("@temp_dd_fee_amount",amt)


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            appro simsobj = new appro();
                            simsobj.temp_dd_doc_no = dr["temp_dd_doc_no"].ToString();
                            simsobj.temp_dd_doc_date = db.DBYYYYMMDDformat(dr["temp_dd_doc_date"].ToString());
                            simsobj.temp_dd_enroll_number = dr["temp_dd_enroll_number"].ToString();
                            simsobj.total_amt = dr["total_amt"].ToString();
                            simsobj.name = dr["name"].ToString();
                            simsobj.temp_dd_fee_auth_code = dr["temp_dd_fee_auth_code"].ToString();
                            try
                            {
                                simsobj.temp_dd_grade = dr["sims_grade_name_en"].ToString();
                                simsobj.temp_dd_section = dr["sims_section_name_en"].ToString();
                                simsobj.temp_dd_error_messsage = dr["temp_dd_error_messsage"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }

                            simsobj.temp_dd_fee_card_last_number = dr["temp_dd_fee_card_last_number"].ToString();
                            simsobj.temp_dd_fee_transaction_id = dr["temp_dd_fee_transaction_id"].ToString();
                            simsobj.temp_dd_doc_date_time = dr["temp_dd_doc_date_time"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        //Online Payment Fee Receipt View

        [Route("getAllRecordsForOnlineFeeReceipt")]
        public HttpResponseMessage getAllRecordsForOnlineFeeReceipt(string from_date, string to_date, string search)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsForOnlineFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","B"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["temp_dd_fee_academic_year"].ToString();
                            simsobj.doc_no = dr["temp_dd_doc_no"].ToString();
                            //simsobj.creation_date = dr["creation_date"].ToString();
                            simsobj.dd_enroll_number = dr["temp_dd_enroll_number"].ToString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["temp_dd_doc_date"].ToString());
                            simsobj.dd_realize_date = db.UIDDMMYYYYformat(dr["temp_dd_realize_date"].ToString());
                            simsobj.reciept_number = dr["reciept_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["amt"].ToString();
                           // simsobj.Doc_status = dr["Doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                     
                }
            }
            catch (Exception x)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("FetchAllReceipt")]
        public HttpResponseMessage FetchAllReceipt(string doc_no)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[get_sims_fee_receipt_data]",
                       new List<SqlParameter>() 
                         { 
                            /// new SqlParameter("@academic_year", academicYear),
                              new SqlParameter("@doc_no", doc_no)
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }

        [Route("FetchMograsysLicenceDetails")]
        public HttpResponseMessage FetchMograsysLicenceDetails(string opr)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[dbo].[pp_Mogra_license_details]",
                       new List<SqlParameter>() 
                         { 
                            /// new SqlParameter("@academic_year", academicYear),
                              new SqlParameter("@opr", opr)
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }

        [Route("InsertCommunication")]
        public HttpResponseMessage InsertCommunication(List<Sim615> data)
        {
            SIMSAPI.Models.ERP.messageClass.Message message = new SIMSAPI.Models.ERP.messageClass.Message();
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim615 hrmsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_receivable_communication_details]",

                        new List<SqlParameter>()
                     {
                                    new SqlParameter("@opr","I"),
                                    new SqlParameter("@sims_cur_code", hrmsobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_academic_year", hrmsobj.sims_fee_academic_year),
                                    new SqlParameter("@sims_parent_number", hrmsobj.sims_parent_number),
                                    new SqlParameter("@sims_student_enroll", hrmsobj.sims_enroll_number),
                                    new SqlParameter("@sims_grade_code", hrmsobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code", hrmsobj.sims_fee_section_code),
                                    new SqlParameter("@sims_due_amount", hrmsobj.final_total),
                                    new SqlParameter("@sims_transaction_by", hrmsobj.sims_transaction_by),
                                    new SqlParameter("@sims_school_remark", hrmsobj.sims_school_remark),
                                    new SqlParameter("@sims_parent_remark", hrmsobj.sims_parent_remark),
                                    new SqlParameter("@expected_payment_date", db.DBYYYYMMDDformat(hrmsobj.expected_payment_date)),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getcommunicationdetails")]
        public HttpResponseMessage getcommunicationdetails(string sims_parent_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRecordsForOnlineFeeReceipt()PARAMETERS ::NA";
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_receivable_communication_details]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@sims_parent_number",sims_parent_number),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.sims_transaction_date = dr["sims_transaction_date"].ToString();
                            simsobj.sims_school_remark = dr["sims_school_remark"].ToString();
                            simsobj.sims_parent_remark = dr["sims_parent_remark"].ToString();
                            simsobj.expected_payment_date = db.UIDDMMYYYYformat(dr["expected_payment_date"].ToString());
                            simsobj.sims_transaction_by = dr["sims_transaction_by"].ToString();
                            com_list.Add(simsobj);
                        }
                    }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }
    }
}