﻿using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using SIMSAPI.Models.FEE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using System.Data;
using SIMSAPI.Models.SIMS.simsClass;
using System.Globalization;
namespace SIMSAPI.Controllers.FEE
{
    [RoutePrefix("api/StudentFee")]
    [BasicAuthentication]

    public class StudentFeeTransactionController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[Route("GetStudentFee")]
        //public HttpResponseMessage GetStudentFee(string data)
        //{
        //    FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
        //    List<StudentFee> outp = new List<StudentFee>();
        //    List<SqlParameter> lst = new List<SqlParameter>();
        //    lst.Add(new SqlParameter("@OPR", "G"));
        //    lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
        //    lst.Add(new SqlParameter("@CURR", sf.StudCurr));
        //    lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
        //    lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
        //            while (dr.Read())
        //            {
        //                StudentFee stdFee = new StudentFee();
        //                stdFee.FeeDetails = new List<FeeDetails>();
        //                stdFee.OtherFeeDetails = new List<OtherFee>();
        //                stdFee.StudCurr = dr["sims_cur_code"].ToString();
        //                stdFee.StudAcademic = dr["sims_academic_year"].ToString();
        //                stdFee.StudGrade = dr["sims_grade_code"].ToString();
        //                stdFee.StudSection = dr["sims_section_code"].ToString();
        //                stdFee.StudGradeName = dr["sims_grade_name_en"].ToString();
        //                stdFee.StudSectionName = dr["sims_section_name_en"].ToString();
        //                stdFee.StudDName = dr["STUDENTNAME"].ToString();
        //                stdFee.StudDEnroll = dr["sims_enroll_number"].ToString();
        //                stdFee.ParentID = dr["sims_sibling_parent_number"].ToString();
        //                stdFee.ParentName = dr["ParentName"].ToString();
        //                outp.Add(stdFee);
        //            }
        //            dr.NextResult();
        //            while (dr.Read())
        //            {
        //                OtherFee of = new OtherFee();
        //                string str = dr["sims_enroll_number"].ToString();
        //                of.OF_AMT = dr["sims_fee_amount"].ToString();
        //                of.OF_Fee_code = dr["sims_fee_code"].ToString();
        //                of.OF_Fee_cat = dr["sims_fee_category"].ToString();
        //                of.OF_Desc = dr["sims_fee_code_description"].ToString();
        //                var v = outp.Where(q => q.StudDEnroll == str);
        //                if (v.Count() > 0)
        //                {
        //                    v.ElementAt(0).OtherFeeDetails.Add(of);
        //                }
        //            }
        //            dr.NextResult();
        //            int i = 0;
        //            while (dr.Read())
        //            {
        //                FeeDetails fd = new FeeDetails();
        //                fd.SrNo = (++i) + "";
        //                fd.StudEnroll = dr["sims_enroll_number"].ToString();
        //                fd.StudCurr = dr["sims_fee_cur_code"].ToString();
        //                fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
        //                fd.StudGrade = dr["sims_fee_grade_code"].ToString();
        //                fd.StudSection = dr["sims_fee_section_code"].ToString();
        //                fd.StudFeeNumber = dr["sims_fee_number"].ToString();
        //                fd.StudFeeCode = dr["sims_fee_code"].ToString();
        //                fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
        //                fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
        //                //fd.StudFeeCategoryDesc = dr[""].ToString();
        //                fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
        //                //fd.StudFeeFrequencyDesc = dr[""].ToString();
        //                fd.StudFeePeriodNo = dr["periodNo"].ToString();
        //                fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
        //                fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
        //                fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
        //                //fd.StudTotalFeeAMT = dr[""].ToString();
        //                fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

        //                fd.StudConcessionAMT = dr["conc_Fee"].ToString();
        //                fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

        //                fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
        //                fd.StudBalanceAMT = dr["bal_Fee"].ToString();
        //                fd.StudBalancePayingAMT = fd.StudBalanceAMT;
        //                fd.payingAgent = dr["isPayingAgent"].ToString();
        //                fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
        //                fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
        //                fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
        //                fd.valid = "false";
        //                var v = outp.Where(q => q.StudDEnroll == fd.StudEnroll);
        //                if (v.Count() > 0)
        //                {
        //                    v.ElementAt(0).FeeDetails.Add(fd);
        //                }
        //            }
        //            foreach (var std in outp)
        //            {
        //                List<FeeDetails> tmplst = new List<FeeDetails>();
        //                var gp = from p in std.FeeDetails
        //                         group p by p.StudFeeCode into fees
        //                         select new { FeeCode = fees.Key, Terms = fees };

        //                foreach (var item in gp)
        //                {
        //                    FeeDetails fd = new FeeDetails();
        //                    fd.StudEnroll = std.StudDEnroll;
        //                    fd.StudFeeCode = item.FeeCode;
        //                    fd.StudExpectedFeeAMT = item.Terms.Sum(q => double.Parse(q.StudExpectedFeeAMT)) + "";
        //                    fd.StudConcessionAMT = item.Terms.Sum(q => double.Parse(q.StudConcessionAMT)) + "";
        //                    fd.StudTotalPaidAMT = item.Terms.Sum(q => double.Parse(q.StudTotalPaidAMT)) + "";
        //                    fd.StudBalanceAMT = item.Terms.Sum(q => double.Parse(q.StudBalanceAMT)) + "";
        //                    fd.SrNo = "grid";
        //                    string FeeDesc = item.Terms.Select(q => q.StudFeeDesc).Distinct().ElementAt(0);
        //                    fd.StudFeeDesc = FeeDesc;
        //                    fd.subItems = new List<FeeDetails>();
        //                    fd.subItems.AddRange(item.Terms);
        //                    fd.rem = "fa fa-minus-circle";
        //                    tmplst.Add(fd);
        //                }
        //                std.FeeDetails = new List<FeeDetails>();
        //                std.FeeDetails = tmplst;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, outp);
        //}


        #region
        public string UIDDMMYYYYformat(string dt)
        {

            string dbgetdate = dt;
            string uigetformatDate = null;

            if (!string.IsNullOrEmpty(dbgetdate))
            {
                DateTime uigetDate = Convert.ToDateTime(dbgetdate);
                //string uigetday = uigetDate.Day.ToString();
                //string uigetmonth = uigetDate.Month.ToString();
                //string uigetyear = uigetDate.Year.ToString();
                //uigetformatDate = Convert.ToDateTime(uigetday + "-" + uigetmonth + "-" + uigetyear).ToString("dd-MM-yyyy");
                uigetformatDate = uigetDate.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                return uigetformatDate;
            }

            return uigetformatDate;
        }
        #endregion
        //Date Format dd-mm-yyyy to yyyy-mm-dd
        #region Date_Convert_Function_Client
        public string DBYYYYMMDDformat(string dt)
        {
            var uigetdate = dt;
            string dbgetformatDate = null;
            if (uigetdate == null || uigetdate == "")
            {
                return dbgetformatDate;
            }
            else
            {
                var day = uigetdate.Split('-')[0];
                var month = uigetdate.Split('-')[1];
                var year = uigetdate.Split('-')[2];

                dbgetformatDate = year + "-" + month + "-" + day;


            }
            return dbgetformatDate;
        }
        #endregion


        [Route("fb363977d4b6b42df05454fd72ce2518fe0f0302")]
        public HttpResponseMessage fb363977d4b6b42df05454fd72ce2518fe0f0302(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentFee")]
        public HttpResponseMessage GetStudentFee(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentFee1")]
        public HttpResponseMessage GetStudentFee1(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA1", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        //of.vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        //of.vat_calc_amount = dr["fee_code_calc_vat_value"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();

                        //fd.stud_vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        //fd.stud_vat_calc_amt = dr["fee_code_calc_vat_value"].ToString();


                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentFee_VAT")]
        public HttpResponseMessage GetStudentFee_VAT(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA1", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        of.vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        of.vat_calc_amount = dr["fee_code_calc_vat_value"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();

                        fd.stud_vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        fd.stud_vat_calc_amt = dr["fee_code_calc_vat_value"].ToString();


                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }


        [Route("GetStudentFeeNew")]
        public HttpResponseMessage GetStudentFeeNew(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA1New", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        //sha1==>getStudentFee1 
        [Route("f4381e2c1ac6486ec1697a0db1475c9e69fb1287")]
        public HttpResponseMessage f4381e2c1ac6486ec1697a0db1475c9e69fb1287(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA1_pref", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        of.vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        of.vat_calc_amount = dr["fee_code_calc_vat_value"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        fd.pref_id = dr["sims_display_order"].ToString();
                        fd.term_flag = dr["sims_term_flag"].ToString();
                        fd.stud_vat_per = dr["sims_fee_code_vat_percentage"].ToString();
                        fd.stud_vat_calc_amt = dr["fee_code_calc_vat_value"].ToString();

                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("StudentFeeGet")]
        public HttpResponseMessage StudentFeeGet(FeeDetails sf)
        {
            //FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRAGet", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudDiscount")]
        public HttpResponseMessage GetStudDiscount(string ay)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("dbo.sims_student_fee_TRA",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@ACADEMIC", ay)
                        });
                    ds.DataSetName = "PAs";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("GetPaymentMode")]
        public HttpResponseMessage GetPaymentMode()
        {
            List<PaymentModes> PM = new List<PaymentModes>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "PM"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS", lst);
                    while (dr.Read())
                    {
                        PaymentModes p = new PaymentModes();
                        p.PMType = dr["sims_appl_form_field_value2"].ToString();
                        p.PMDesc = dr["sims_appl_form_field_value1"].ToString();
                        p.PMDescShort = dr["sims_appl_parameter"].ToString();
                        PM.Add(p);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, PM);
        }

        [Route("SubmitFees")]
        public HttpResponseMessage SubmitFees(param sf)
        {
            //FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            //FeeNumber []pDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeNumber[]>(sf.rem);
            List<SqlParameter> lstFeenum = new List<SqlParameter>();
            List<SqlParameter> lstFeePer = new List<SqlParameter>();
            List<SqlParameter> lstFinal = new List<SqlParameter>();
            string op = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int cnt = 0;
                    sf.Students.ForEach(q => q.ParentID = sf.ParentID);
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.ParentID = q.ParentID));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudDEnroll = q.Enroll));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudCurr = q.StudCurr));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudAcademic = q.StudAcademic));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudGrade = q.StudGrade));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudSection = q.StudSection));
                    var fees = sf.Students.SelectMany(q => q.Fees);
                    #region TempEntries
                    foreach (var i in fees)
                    {
                        lstFeenum.Clear();
                        lstFeenum.Add(new SqlParameter("@OPR", "SF"));
                        lstFeenum.Add(new SqlParameter("@ENROLL", i.StudDEnroll));
                        lstFeenum.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                        lstFeenum.Add(new SqlParameter("@CURR", i.StudCurr));
                        lstFeenum.Add(new SqlParameter("@ACADEMIC", i.StudAcademic));
                        lstFeenum.Add(new SqlParameter("@GRADE", i.StudGrade));
                        lstFeenum.Add(new SqlParameter("@SECTION", i.StudSection));
                        string my = i.GetRecord();
                        lstFeenum.Add(new SqlParameter("@FEE_Per", my));
                        int dr = db.ExecuteStoreProcedureforInsert("sims.sims_student_fee_TRANS1", lstFeenum);
                        cnt = cnt + dr;
                        //db.Dispose();
                    }
                    if (cnt != 0)
                    {
                        var Periods = fees.SelectMany(q => q.Periods);
                        DBConnection DBc = new DBConnection();
                        DBc.Open();
                        int ct = 0;
                        foreach (var item in Periods)
                        {
                            lstFeePer.Clear();
                            lstFeePer.Add(new SqlParameter("@OPR", "DD"));
                            lstFeePer.Add(new SqlParameter("@ENROLL", item.Enroll));
                            lstFeePer.Add(new SqlParameter("@ReceiptDate",  DBYYYYMMDDformat(sf.ReceiptDate)));
                            //lstFeePer.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
                            //lstFeePer.Add(new SqlParameter("@GRADE", sf.StudGrade));
                            //lstFeePer.Add(new SqlParameter("@SECTION", sf.StudSection));
                            lstFeePer.Add(new SqlParameter("@BankCode", item.BankCode));
                            lstFeePer.Add(new SqlParameter("@ChqDate",  DBYYYYMMDDformat(item.ChequeDt)));
                            lstFeePer.Add(new SqlParameter("@ChqNo", item.ChequeNo));
                            lstFeePer.Add(new SqlParameter("@PayMode", item.PayMode));
                            lstFeePer.Add(new SqlParameter("@TXNNo", item.TXNNo));
                            string q = item.getDocDetails();
                            lstFeePer.Add(new SqlParameter("@FEE_Per", q));
                            int dr1 = DBc.ExecuteStoreProcedureforInsert("sims.sims_student_fee_TRANS1", lstFeePer);
                            ct = ct + dr1;
                        }
                    }
                    #endregion

                    #region CommitEntries
                    lstFinal.Add(new SqlParameter("@OPR", "FN"));
                    lstFinal.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                    lstFinal.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                    lstFinal.Add(new SqlParameter("@UserNm", sf.UserName));
                    lstFinal.Add(new SqlParameter("@Remark", sf.Remark));

                    DBConnection dbfin = new DBConnection();
                    dbfin.Open();
                    SqlDataReader drfinal = dbfin.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lstFinal);
                    while (drfinal.Read())
                    {
                        string sts = drfinal[0].ToString();
                        op = drfinal[1].ToString();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        [Route("SubmitFeesD")]
        public HttpResponseMessage SubmitFeesD(param sf)
        {
            //FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            //FeeNumber []pDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeNumber[]>(sf.rem);
            List<SqlParameter> lstFeenum = new List<SqlParameter>();
            List<SqlParameter> lstFeePer = new List<SqlParameter>();
            List<SqlParameter> lstFinal = new List<SqlParameter>();
            string op = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int cnt = 0;
                    sf.Students.ForEach(q => q.ParentID = sf.ParentID);
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.ParentID = q.ParentID));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudDEnroll = q.Enroll));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudCurr = q.StudCurr));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudAcademic = q.StudAcademic));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudGrade = q.StudGrade));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudSection = q.StudSection));
                    var fees = sf.Students.SelectMany(q => q.Fees);
                    #region TempEntries
                    foreach (var i in fees)
                    {
                        lstFeenum.Clear();
                        lstFeenum.Add(new SqlParameter("@OPR", "SF"));
                        lstFeenum.Add(new SqlParameter("@ENROLL", i.StudDEnroll));
                        lstFeenum.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                        lstFeenum.Add(new SqlParameter("@CURR", i.StudCurr));
                        lstFeenum.Add(new SqlParameter("@ACADEMIC", i.StudAcademic));
                        lstFeenum.Add(new SqlParameter("@GRADE", i.StudGrade));
                        lstFeenum.Add(new SqlParameter("@SECTION", i.StudSection));
                        string my = i.GetRecord();
                        lstFeenum.Add(new SqlParameter("@FEE_Per", my));
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_fee_TRANSWithDis]", lstFeenum);
                        cnt = cnt + dr;
                        //db.Dispose();
                    }
                    if (cnt != 0)
                    {
                        var Periods = fees.SelectMany(q => q.Periods);
                        DBConnection DBc = new DBConnection();
                        DBc.Open();
                        int ct = 0;
                        foreach (var item in Periods)
                        {
                            lstFeePer.Clear();
                            lstFeePer.Add(new SqlParameter("@OPR", "DD"));
                            lstFeePer.Add(new SqlParameter("@ENROLL", item.Enroll));
                            lstFeePer.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                            //lstFeePer.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
                            //lstFeePer.Add(new SqlParameter("@GRADE", sf.StudGrade));
                            //lstFeePer.Add(new SqlParameter("@SECTION", sf.StudSection));
                            lstFeePer.Add(new SqlParameter("@BankCode", item.BankCode));
                            lstFeePer.Add(new SqlParameter("@ChqDate", DBYYYYMMDDformat(item.ChequeDt)));//(
                            lstFeePer.Add(new SqlParameter("@ChqNo", item.ChequeNo));
                            lstFeePer.Add(new SqlParameter("@PayMode", item.PayMode));
                            lstFeePer.Add(new SqlParameter("@UserNm", sf.UserName));
                            lstFeePer.Add(new SqlParameter("@TXNNo", item.TXNNo));
                            string q = item.getDocDetails();
                            lstFeePer.Add(new SqlParameter("@FEE_Per", q));
                            int dr1 = DBc.ExecuteStoreProcedureforInsert("[sims].[sims_student_fee_TRANSWithDis]", lstFeePer);
                            ct = ct + dr1;
                        }
                    }
                    #endregion

                    #region CommitEntries
                    lstFinal.Add(new SqlParameter("@OPR", "FN"));
                    lstFinal.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                    lstFinal.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                    lstFinal.Add(new SqlParameter("@UserNm", sf.UserName));
                    lstFinal.Add(new SqlParameter("@Remark", sf.Remark));

                    DBConnection dbfin = new DBConnection();
                    dbfin.Open();
                    SqlDataReader drfinal = dbfin.ExecuteStoreProcedure("[sims].[sims_student_fee_TRANSWithDis]", lstFinal);
                    while (drfinal.Read())
                    {
                        string sts = drfinal[0].ToString();
                        op = drfinal[1].ToString();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        [Route("SubmitFeesD_VAT")]
        public HttpResponseMessage SubmitFeesD_VAT(param sf)
        {
            //FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            //FeeNumber []pDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeNumber[]>(sf.rem);
            List<SqlParameter> lstFeenum = new List<SqlParameter>();
            List<SqlParameter> lstFeePer = new List<SqlParameter>();
            List<SqlParameter> lstFinal = new List<SqlParameter>();
            string op = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int cnt = 0;
                    sf.Students.ForEach(q => q.ParentID = sf.ParentID);
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.ParentID = q.ParentID));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudDEnroll = q.Enroll));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudCurr = q.StudCurr));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudAcademic = q.StudAcademic));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudGrade = q.StudGrade));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudSection = q.StudSection));
                    var fees = sf.Students.SelectMany(q => q.Fees);
                    #region TempEntries
                    foreach (var i in fees)
                    {
                        lstFeenum.Clear();
                        lstFeenum.Add(new SqlParameter("@OPR", "SF"));
                        lstFeenum.Add(new SqlParameter("@ENROLL", i.StudDEnroll));
                        lstFeenum.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                        lstFeenum.Add(new SqlParameter("@CURR", i.StudCurr));
                        lstFeenum.Add(new SqlParameter("@ACADEMIC", i.StudAcademic));
                        lstFeenum.Add(new SqlParameter("@GRADE", i.StudGrade));
                        lstFeenum.Add(new SqlParameter("@SECTION", i.StudSection));
                        string my = i.GetRecord();
                        lstFeenum.Add(new SqlParameter("@FEE_Per", my));
                        int dr = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_fee_TRANSWithDis]", lstFeenum);
                        cnt = cnt + dr;
                        //db.Dispose();
                    }
                    if (cnt != 0)
                    {
                        var Periods = fees.SelectMany(q => q.Periods);
                        DBConnection DBc = new DBConnection();
                        DBc.Open();
                        int ct = 0;
                        foreach (var item in Periods)
                        {
                            lstFeePer.Clear();
                            lstFeePer.Add(new SqlParameter("@OPR", "DD"));
                            lstFeePer.Add(new SqlParameter("@ENROLL", item.Enroll));
                            lstFeePer.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                            //lstFeePer.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
                            //lstFeePer.Add(new SqlParameter("@GRADE", sf.StudGrade));
                            //lstFeePer.Add(new SqlParameter("@SECTION", sf.StudSection));
                            lstFeePer.Add(new SqlParameter("@BankCode", item.BankCode));
                            lstFeePer.Add(new SqlParameter("@ChqDate", DBYYYYMMDDformat(item.ChequeDt)));//(
                            lstFeePer.Add(new SqlParameter("@ChqNo", item.ChequeNo));
                            lstFeePer.Add(new SqlParameter("@PayMode", item.PayMode));
                            lstFeePer.Add(new SqlParameter("@UserNm", sf.UserName));
                            lstFeePer.Add(new SqlParameter("@TXNNo", item.TXNNo));
                            string q = item.getDocDetails_VAT();
                            lstFeePer.Add(new SqlParameter("@FEE_Per", q));
                            int dr1 = DBc.ExecuteStoreProcedureforInsert("[sims].[sims_student_fee_TRANSWithDis]", lstFeePer);
                            ct = ct + dr1;
                        }
                    }
                    #endregion

                    #region CommitEntries
                    lstFinal.Add(new SqlParameter("@OPR", "FN"));
                    lstFinal.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                    lstFinal.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                    lstFinal.Add(new SqlParameter("@UserNm", sf.UserName));
                    lstFinal.Add(new SqlParameter("@Remark", sf.Remark));

                    DBConnection dbfin = new DBConnection();
                    dbfin.Open();
                    SqlDataReader drfinal = dbfin.ExecuteStoreProcedure("[sims].[sims_student_fee_TRANSWithDis]", lstFinal);
                    while (drfinal.Read())
                    {
                        string sts = drfinal[0].ToString();
                        op = drfinal[1].ToString();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }


        [Route("GetStudentPAFee1")]
        public HttpResponseMessage GetStudentPAFee1(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> outp = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS_PA", lst);
                    while (dr.Read())
                    {
                        StudentFee stdFee = new StudentFee();
                        stdFee.FeeDetails = new List<FeeDetails>();
                        stdFee.OtherFeeDetails = new List<OtherFee>();
                        stdFee.StudCurr = dr["sims_cur_code"].ToString();
                        stdFee.StudAcademic = dr["sims_academic_year"].ToString();
                        stdFee.StudGrade = dr["sims_grade_code"].ToString();
                        stdFee.StudSection = dr["sims_section_code"].ToString();
                        stdFee.StudGradeName = dr["sims_grade_name_en"].ToString();
                        stdFee.StudSectionName = dr["sims_section_name_en"].ToString();
                        stdFee.StudDName = dr["STUDENTNAME"].ToString();
                        stdFee.StudDEnroll = dr["sims_enroll_number"].ToString();
                        stdFee.ParentID = dr["sims_sibling_parent_number"].ToString();
                        stdFee.ParentName = dr["ParentName"].ToString();
                        stdFee.PayingAgentName = dr["PANAME"].ToString();
                        outp.Add(stdFee);
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = outp.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, outp);
        }

        [Route("GetStudentDiscount")]
        public HttpResponseMessage GetStudentDiscount(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            StudentFee stdFee = new StudentFee();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "C"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS", lst);
                    while (dr.Read())
                    {
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentOtherFees")]
        public HttpResponseMessage GetStudentOtherFees(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<OtherFee> stdFee = new List<OtherFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "OF"));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@GRADE", sf.StudGrade));
            lst.Add(new SqlParameter("@SECTION", sf.StudSection));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS", lst);
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        stdFee.Add(of);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentPayingAgents")]
        public HttpResponseMessage GetStudentPayingAgents(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            StudentFee stdFee = new StudentFee();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "PA"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS", lst);
                    while (dr.Read())
                    {
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }
   
        [Route("SubmitPAFees")]
        private HttpResponseMessage SubmitPAFees(param sf)
        {
            //FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            //FeeNumber []pDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeNumber[]>(sf.rem);
            List<SqlParameter> lstFeenum = new List<SqlParameter>();
            List<SqlParameter> lstFeePer = new List<SqlParameter>();
            List<SqlParameter> lstFinal = new List<SqlParameter>();
            string op = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int cnt = 0;
                    sf.Students.ForEach(q => q.ParentID = sf.ParentID);
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.ParentID = q.ParentID));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudDEnroll = q.Enroll));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudCurr = q.StudCurr));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudAcademic = q.StudAcademic));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudGrade = q.StudGrade));
                    sf.Students.ForEach(q => q.Fees.ForEach(p => p.StudSection = q.StudSection));
                    var fees = sf.Students.SelectMany(q => q.Fees);
                    #region TempEntries
                    foreach (var i in fees)
                    {
                        lstFeenum.Clear();
                        lstFeenum.Add(new SqlParameter("@OPR", "SF"));
                        lstFeenum.Add(new SqlParameter("@ENROLL", i.StudDEnroll));
                        lstFeenum.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                        lstFeenum.Add(new SqlParameter("@CURR", i.StudCurr));
                        lstFeenum.Add(new SqlParameter("@ACADEMIC", i.StudAcademic));
                        lstFeenum.Add(new SqlParameter("@GRADE", i.StudGrade));
                        lstFeenum.Add(new SqlParameter("@SECTION", i.StudSection));
                        string my = i.GetRecord();
                        lstFeenum.Add(new SqlParameter("@FEE_Per", my));
                        int dr = db.ExecuteStoreProcedureforInsert("sims.sims_student_fee_TRANS_PA", lstFeenum);
                        cnt = cnt + dr;
                        //db.Dispose();
                    }
                    if (cnt != 0)
                    {
                        var Periods = fees.SelectMany(q => q.Periods);
                        DBConnection DBc = new DBConnection();
                        DBc.Open();
                        int ct = 0;
                        foreach (var item in Periods)
                        {
                            lstFeePer.Clear();
                            lstFeePer.Add(new SqlParameter("@OPR", "DD"));
                            string q = item.getDocDetails();
                            lstFeePer.Add(new SqlParameter("@FEE_Per", q));
                            int dr1 = DBc.ExecuteStoreProcedureforInsert("sims.sims_student_fee_TRANS_PA", lstFeePer);
                            ct = ct + dr1;
                        }
                    }
                    #endregion

                    #region CommitEntries
                    lstFinal.Add(new SqlParameter("@OPR", "FN"));
                    lstFinal.Add(new SqlParameter("@P_NUMBER", sf.ParentID));
                    lstFinal.Add(new SqlParameter("@ReceiptDate", DBYYYYMMDDformat(sf.ReceiptDate)));
                    lstFinal.Add(new SqlParameter("@UserNm", sf.UserName));
                    lstFinal.Add(new SqlParameter("@Remark", sf.Remark));

                    DBConnection dbfin = new DBConnection();
                    dbfin.Open();
                    SqlDataReader drfinal = dbfin.ExecuteStoreProcedure("sims.sims_student_fee_TRANS_PA", lstFinal);
                    while (drfinal.Read())
                    {
                        string sts = drfinal[0].ToString();
                        op = drfinal[1].ToString();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, op);
        }

        [Route("CollectionDetails")]
        public HttpResponseMessage CollectionDetails()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            TodaysCollectionDetails p = new TodaysCollectionDetails();
            lst.Add(new SqlParameter("@OPR", "CD"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS", lst);
                    if (dr.Read())
                    {
                        p.cancelledReceipt = dr["CancelledReceipt"].ToString();
                        p.TotalCollection = dr["TotalCollection"].ToString();
                        p.validReceipts = dr["validReceipt"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, p);
        }

        [Route("GetBanks")]
        public HttpResponseMessage GetBanks()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<BankDet> BD = new List<BankDet>();
            lst.Add(new SqlParameter("@OPR", "BD"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
                    while (dr.Read())
                    {
                        BankDet p = new BankDet();
                        p.bankCode = dr["pb_bank_code"].ToString();
                        p.bankName = dr["pb_bank_name"].ToString();
                        p.glAcno = dr["pb_gl_acno"].ToString();
                        p.slno = dr["pb_srl_no"].ToString();
                        BD.Add(p);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, BD);
        }

        [Route("GetBanksWithCompany")]
        public HttpResponseMessage GetBanksWithCompany(string cur,string ayear,string grade)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<BankDet> BD = new List<BankDet>();
            lst.Add(new SqlParameter("@OPR", "BN"));
            lst.Add(new SqlParameter("@CURR", cur));
            lst.Add(new SqlParameter("@ACADEMIC", ayear));
            lst.Add(new SqlParameter("@GRADE", grade));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
                    while (dr.Read())
                    {
                        BankDet p = new BankDet();
                        p.bankCode = dr["pb_bank_code"].ToString();
                        p.bankName = dr["pb_bank_name"].ToString();
                        p.glAcno = dr["pb_gl_acno"].ToString();
                        p.slno = dr["pb_srl_no"].ToString();
                        BD.Add(p);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, BD);
        }


        [Route("PDetails")]
        public HttpResponseMessage PDetails(Student st)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_fee_term_per_mapp",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'T'),
                            new SqlParameter("@cur_code", st.StudCurr),
                            new SqlParameter("@academic_year", st.StudAcademic)
                        });
                    ds.DataSetName = "Pers";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("GetReceipt")]
        public HttpResponseMessage GetReceipt()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string reportName = "Sims.SIMR51";
            lst.Add(new SqlParameter("@OPR", "FR"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
                    if (dr.Read())
                    {
                        reportName = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, reportName);
        }

        [Route("GetReceiptCon")]
        public HttpResponseMessage GetReceiptCon()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string reportName = "Sims.SIMR51";
            lst.Add(new SqlParameter("@OPR", "FR"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
                    if (dr.Read())
                    {
                        reportName = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, reportName);
        }


        [Route("GetDefaultPM")]
        public HttpResponseMessage GetDefaultPM()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string defaultPM = "Ca";
            lst.Add(new SqlParameter("@OPR", "GP"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_TRANS1", lst);
                    if (dr.Read())
                    {
                        defaultPM = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, defaultPM);
        }

         //////////////////////////////////////////////////////////////////////////////////

        [Route("GetStudentPAFee")]
        public HttpResponseMessage GetStudentPAFee(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            FeeDetailsPR sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetailsPR>(data);
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic)); 
            lst.Add(new SqlParameter("@T_code", sf.StudFeeTermCode));
            lst.Add(new SqlParameter("@P_code", sf.PACODE));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_student_fee_TRANS_PA1", lst);
                    ds.DataSetName = "Terms";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                o = ex;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //Getterms
        [Route("da7b7454507b2a8fa08803a7cb06f938758d109d")]
        public HttpResponseMessage da7b7454507b2a8fa08803a7cb06f938758d109d(string academic_year,string cur_code)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_student_fee_TRANS_PA1",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'T'),
                             new SqlParameter("@ACADEMIC", academic_year),
                              new SqlParameter("@CURR", cur_code)
                        });
                    ds.DataSetName = "Terms";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //GetPAs
        [Route("d5b1d259f086f320d2f5479209eb338a6ce47bc0")]
        public HttpResponseMessage d5b1d259f086f320d2f5479209eb338a6ce47bc0()
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_student_fee_TRANS_PA1",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", 'P')
                        });
                    ds.DataSetName = "PAs";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        //Submit Fees
        [Route("a0f1490a20d0211c997b44bc357e1972deab8ae3")]
        public HttpResponseMessage a0f1490a20d0211c997b44bc357e1972deab8ae3(List<Dictionary<string, string>> sf)
        {
            try
            {
                object o = null;
                HttpStatusCode s = HttpStatusCode.NoContent;
                List<SqlParameter> sp = new List<SqlParameter>();
                foreach (var v in sf)
                {
                    sp.Clear();
                    foreach (var p in v.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        pr1.Value = v[p];
                        sp.Add(pr1);
                    }
                    SqlParameter pr = new SqlParameter();
                    pr.ParameterName = "@OPR";
                    pr.Value = "S";
                    sp.Add(pr);
                    try
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_student_fee_submit_PA]", sp);
                            ds.DataSetName = "AdjData";
                            o = ds;
                            s = HttpStatusCode.OK;
                        }
                    }
                    catch (Exception x)
                    {
                        o = x;
                        s = HttpStatusCode.InternalServerError;
                    }
                }
                return Request.CreateResponse(s, o);
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }


        [Route("pasmfb")]
        public HttpResponseMessage pasmfb(Dictionary<string, string> sf)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            object o = new object();
            lst.Add(new SqlParameter("@OPR", "SP"));
            try
            {
                if (sf != null)
                    foreach (var p in sf.Keys)
                    {
                        SqlParameter pr1 = new SqlParameter();
                        pr1.ParameterName = "@" + p;
                        pr1.Value = sf[p];
                        lst.Add(pr1);
                    }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_fee_param", lst);
                    o = ds;
                }
            }
            catch (Exception ex)
            {
                o = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, o);
        }

        /* Total Fee Due*/
        [Route("GetStudentTotalFeeDue")]
        public HttpResponseMessage GetStudentTotalFeeDue(string data,string option)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            lst.Add(new SqlParameter("@option",option));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_due", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("getFeeTerm")]
        public HttpResponseMessage getFeeTerm(string acdemicYear)
        {
            List<FeeDetails> lstModules = new List<FeeDetails>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_due",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "F"),
                            new SqlParameter("@academic_year",acdemicYear)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeDetails sequence = new FeeDetails();
                            sequence.sims_fee_term_code = dr["sims_fee_term_code"].ToString();
                            sequence.sims_fee_term_desc_en = dr["sims_fee_term_desc_en"].ToString();
                            sequence.sims_fee_term_start_date = UIDDMMYYYYformat(dr["sims_fee_term_start_date"].ToString());
                            sequence.sims_fee_term_end_date = UIDDMMYYYYformat(dr["sims_fee_term_end_date"].ToString());
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStudent_DueFees")]
        public HttpResponseMessage getStudent_DueFees(string cc, string ay,string search_stud,string option,string month_no,string term_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            //minval = 0;
            //maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            month_no = string.IsNullOrEmpty(month_no) ? null : month_no;
            term_code = string.IsNullOrEmpty(term_code) ? null : term_code;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            //search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_due",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", option),                               
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),                                
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@month_no", month_no),
                                new SqlParameter("@term_code",term_code),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            //simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            //simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            //simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            //simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            //simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.sims_fee_code_description = dr["fee_discription_new"].ToString();
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total_Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("getDecimalPlaces")]
        public HttpResponseMessage getDecimalPlaces()
        {
            List<decimalplce> lstModules = new List<decimalplce>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_due",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                          
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            decimalplce sequence = new decimalplce();
                            sequence.comp_curcy_dec = dr["comp_curcy_dec"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("GetScheduleEmail")]
        public HttpResponseMessage GetScheduleEmail(string stud_enroll_no, string parent_id, string fee_amount)
        {
            Boolean insert = false;
            List<SqlParameter> lst = new List<SqlParameter>();
            List<BankDet> BD = new List<BankDet>();
            lst.Add(new SqlParameter("@OPR", "BM"));
            lst.Add(new SqlParameter("@ENROLL", stud_enroll_no));
            lst.Add(new SqlParameter("@P_NUMBER", parent_id));
            lst.Add(new SqlParameter("@fee_amount", fee_amount));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_fee_TRANS1_ASD]", lst);
                     if(dr.RecordsAffected > 0)
                    {
                        insert = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}