﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;


namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/FeesSplit")]
   
    public class FeesSplitController : ApiController
    {
        [Route("getFeesplitEnroll")]
        public HttpResponseMessage getFeesplitEnroll(string enroll)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Split_student_fees",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_student_enroll_numbers",enroll)
                         });
                   
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}




