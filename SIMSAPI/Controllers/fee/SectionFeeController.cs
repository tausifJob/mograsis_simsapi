﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/SectionFee")]
    [BasicAuthentication]
    public class SectionFeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@tbl_name", "sims.sims_academic_year"),
                            new SqlParameter("@tbl_col_name1", "*"),
                            new SqlParameter("@tbl_cond", "sims_academic_year_status='c' AND sims_cur_code='"+curCode+"'")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getCurrentAdvanceAY")]
        public HttpResponseMessage getCurrentAdvanceAY(string curcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrentAdvanceAY(),PARAMETERS :: CURCODE{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getCurrentAdvanceAY", curcode));

            List<Sims036> acayear_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'P'),
                                new SqlParameter("@sims_cur_code", curcode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_academic_year = dr["sims_academic_year"].ToString();
                            acayear_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }

        [Route("CheckTypeCode")]
        public HttpResponseMessage CheckTypeCode(string feecode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckTypeCode(),PARAMETERS :: FEECODE{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckTypeCode", feecode));

            bool ifexists = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_type",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'A')
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ifexists);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ifexists);
            }
        }

        [Route("InsertSectionFeeDetails")]
        public HttpResponseMessage InsertSectionFeeDetails(List<Sims036> simsobjlist)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSectionFeeDetails(),PARAMETERS :: OPR {2}";
            //Log.Debug(string.Format(debug, "STUDENT", "CUDSectionFeeDetails"));

            //  Sims036 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims036>(data);
            //   List<Sims036> mylist = new List<Sims036>();

            bool result = false;
            try
            {
                if (simsobjlist != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        SqlDataReader dr;
                        db.Open();
                        foreach (Sims036 simsobj in simsobjlist)
                        {
                            dr = db.ExecuteStoreProcedure("sims_section_fee",
                               new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                           // new SqlParameter("@sims_fee_number", feenumber),//simsobj.section_fee_number);
                            new SqlParameter("@sims_fee_cur_code", simsobj.section_fee_cur_code_name),
                            new SqlParameter("@sims_fee_academic_year", simsobj.section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj.section_fee_grade_name),
                            new SqlParameter("@sims_fee_section_code", simsobj.section_fee_section_name),
                            new SqlParameter("@sims_fee_category", simsobj.section_fee_category_name),
                            new SqlParameter("@sims_fee_code", simsobj.section_fee_code),
                            new SqlParameter("@sims_fee_code_type", !string.IsNullOrEmpty(simsobj.section_fee_code_type)?simsobj.section_fee_code_type:"O"),
                            new SqlParameter("@sims_fee_refundable", simsobj.section_fee_refundable==true?'Y':'N'),
                            new SqlParameter("@sims_fee_manual_receipt", simsobj.section_fee_manual_receipt==true?'Y':'N'),
                            new SqlParameter("@sims_fee_frequency", simsobj.section_fee_frequency),
                            new SqlParameter("@sims_fee_mandatory", simsobj.section_fee_mandatory==true?'Y':'N'),
                            new SqlParameter("@sims_fee_amount", simsobj.section_fee_amount),
                            new SqlParameter("@sims_fee_installment_mode", simsobj.section_fee_installment_mode==true?'Y':'N'),
                            new SqlParameter("@sims_fee_installment_min_amount", simsobj.section_fee_installment_min_amount),
                            new SqlParameter("@sims_fee_period1", simsobj.section_fee_period1),
                            new SqlParameter("@sims_fee_period2", simsobj.section_fee_period2),
                            new SqlParameter("@sims_fee_period3", simsobj.section_fee_period3),
                            new SqlParameter("@sims_fee_period4", simsobj.section_fee_period4),
                            new SqlParameter("@sims_fee_period5", simsobj.section_fee_period5),
                            new SqlParameter("@sims_fee_period6", simsobj.section_fee_period6),
                            new SqlParameter("@sims_fee_period7", simsobj.section_fee_period7),
                            new SqlParameter("@sims_fee_period8", simsobj.section_fee_period8),
                            new SqlParameter("@sims_fee_period9", simsobj.section_fee_period9),
                            new SqlParameter("@sims_fee_period10", simsobj.section_fee_period10),
                            new SqlParameter("@sims_fee_period11", simsobj.section_fee_period11),
                            new SqlParameter("@sims_fee_period12", simsobj.section_fee_period12),
                            new SqlParameter("@sims_fee_status", simsobj.section_fee_status==true?'A':'I'),
                            new SqlParameter("@sims_fee_split_flag",simsobj.section_fee_split_flag==true?'Y':'N'),
                         });

                            if (dr.RecordsAffected > 0)
                            {
                                result = true;
                                dr.Close();
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDSectionFeeDetailsnew")]
        public HttpResponseMessage CUDSectionFeeDetailsnew(Sims036 simsobj)
        {

            List<Sims036> obj = new List<Sims036>();

            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                            new SqlParameter("@sims_fee_number", simsobj.section_fee_number),//simsobj.section_fee_number);
                            new SqlParameter("@sims_fee_cur_code", simsobj.section_fee_cur_code),
                            new SqlParameter("@sims_fee_academic_year", simsobj.section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj.section_fee_grade_code),
                            new SqlParameter("@sims_fee_section_code", simsobj.section_fee_section_code),
                            new SqlParameter("@sims_fee_category", simsobj.section_fee_category),
                            new SqlParameter("@sims_fee_code", simsobj.section_fee_code),
                            new SqlParameter("@sims_fee_code_type", !string.IsNullOrEmpty(simsobj.section_fee_code_type)?simsobj.section_fee_code_type:"O"),
                            new SqlParameter("@sims_fee_refundable", simsobj.section_fee_refundable==true?'Y':'N'),
                            new SqlParameter("@sims_fee_manual_receipt", simsobj.section_fee_manual_receipt==true?'Y':'N'),
                            new SqlParameter("@sims_fee_frequency", simsobj.section_fee_frequency),
                            new SqlParameter("@sims_fee_mandatory", simsobj.section_fee_mandatory==true?'Y':'N'),
                            new SqlParameter("@sims_fee_amount", simsobj.section_fee_amount),
                            new SqlParameter("@sims_fee_installment_mode", simsobj.section_fee_installment_mode==true?'Y':'N'),
                            new SqlParameter("@sims_fee_installment_min_amount", simsobj.section_fee_installment_min_amount),
                            new SqlParameter("@sims_fee_period1", simsobj.section_fee_period1),
                            new SqlParameter("@sims_fee_period2", simsobj.section_fee_period2),
                            new SqlParameter("@sims_fee_period3", simsobj.section_fee_period3),
                            new SqlParameter("@sims_fee_period4", simsobj.section_fee_period4),
                            new SqlParameter("@sims_fee_period5", simsobj.section_fee_period5),
                            new SqlParameter("@sims_fee_period6", simsobj.section_fee_period6),
                            new SqlParameter("@sims_fee_period7", simsobj.section_fee_period7),
                            new SqlParameter("@sims_fee_period8", simsobj.section_fee_period8),
                            new SqlParameter("@sims_fee_period9", simsobj.section_fee_period9),
                            new SqlParameter("@sims_fee_period10", simsobj.section_fee_period10),
                            new SqlParameter("@sims_fee_period11", simsobj.section_fee_period11),
                            new SqlParameter("@sims_fee_period12", simsobj.section_fee_period12),
                            new SqlParameter("@sims_fee_status", simsobj.section_fee_status==true?'A':'I'),
                            new SqlParameter("@sims_fee_split_flag",simsobj.section_fee_split_flag==true?'Y':'N'),
                     });
                    if (dr.RecordsAffected > 0)
                    {
                        result = true;
                    }
                    dr.Close();
                    if (result == true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims_section_fee",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_fee_cur_code", simsobj.section_fee_cur_code),
                            new SqlParameter("@sims_fee_academic_year", simsobj.section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj.section_fee_grade_code),
                            new SqlParameter("@sims_fee_section_code", simsobj.section_fee_section_code),
                            new SqlParameter("@sims_fee_code", simsobj.section_fee_code)
                    });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Sims036 b = new Sims036();
                                b.section_fee_amount = dr1["section_fee_amount"].ToString();
                                b.full_name = dr1["student_name"].ToString();
                                b.sims_fee_new_amt = dr1["sims_fee_new_amt"].ToString();
                                b.section_fee_status = true;
                                obj.Add(b);
                            }
                        }
                        else
                        {
                            Sims036 b = new Sims036();
                            b.section_fee_status = true;
                            b.full_name = "NONE";
                            obj.Add(b);
                        }
                    }

                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        [Route("getSectionFee")]
        public HttpResponseMessage getSectionFee(string cur_name, string academic_year, string grade_name, string section_name, string fee_category)//, int pageIndex, int PageSize
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFee(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5},FEECATEGORY{6}";
            //Log.Debug(string.Format(debug, "STUDENT", "getSectionFee"));

            List<Sims036> fee_list = new List<Sims036>();
            //int total = 0, skip = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'H'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                                new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                                new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                                new SqlParameter("@sims_fee_number", ""),
                                new SqlParameter("@sims_fee_cur_code", cur_name),
                                new SqlParameter("@sims_fee_academic_year", academic_year),
                                new SqlParameter("@sims_fee_grade_code", grade_name),
                                new SqlParameter("@sims_fee_section_code", section_name),
                                new SqlParameter("@sims_fee_category", fee_category),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_number = dr["sims_fee_number"].ToString();
                            objNew.section_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.section_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.section_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            objNew.section_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.section_fee_category = dr["sims_fee_category"].ToString();
                            objNew.section_fee_code = dr["sims_fee_code"].ToString();
                            objNew.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            objNew.section_fee_code_type = dr["sims_fee_code_type"].ToString();
                            objNew.sims_term_start_date = dr["term_start_date"].ToString();
                            objNew.sims_month_code = dr["month_code"].ToString();
                            if (dr["sims_fee_code_type"].ToString().Equals("M"))
                            {
                                using (DBConnection db11 = new DBConnection())
                                {
                                    db11.Open();
                                    List<Sims039> subject_list1 = new List<Sims039>();
                                    SqlDataReader dr11 = db11.ExecuteStoreProcedure("sims_section_fee", new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'R'),
                                    new SqlParameter("@section_feenumber", objNew.section_fee_number),
                                });
                                    if (dr11.HasRows)
                                    {
                                        while (dr11.Read())
                                        {
                                            objNew.section_fee_amount = dr11["sims_membership_fee_amount"].ToString();
                                            if (dr11["sims_membership_fee_installment_mode"].ToString().Equals("Y"))
                                                objNew.section_fee_installment_mode = true;
                                            else
                                                objNew.section_fee_refundable = false;
                                            objNew.section_fee_installment_min_amount = dr11["sims_membership_fee_installment_min_amount"].ToString();
                                            if (dr11["sims_membership_fee_status"].ToString().Equals("A"))
                                                objNew.section_fee_status = true;
                                            else
                                                objNew.section_fee_status = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                objNew.section_fee_amount = dr["sims_fee_amount"].ToString();
                                if (dr["sims_fee_installment_mode"].ToString().Equals("Y"))
                                    objNew.section_fee_installment_mode = true;
                                else
                                    objNew.section_fee_installment_mode = false;
                                objNew.section_fee_installment_min_amount = dr["sims_fee_installment_min_amount"].ToString();
                                if (dr["sims_fee_status"].ToString().Equals("A"))
                                    objNew.section_fee_status = true;
                                else
                                    objNew.section_fee_status = false;
                                objNew.section_fee_period1 = dr["sims_fee_period1"].ToString();
                                objNew.section_fee_period2 = dr["sims_fee_period2"].ToString();
                                objNew.section_fee_period3 = dr["sims_fee_period3"].ToString();
                                objNew.section_fee_period4 = dr["sims_fee_period4"].ToString();
                                objNew.section_fee_period5 = dr["sims_fee_period5"].ToString();
                                objNew.section_fee_period6 = dr["sims_fee_period6"].ToString();
                                objNew.section_fee_period7 = dr["sims_fee_period7"].ToString();
                                objNew.section_fee_period8 = dr["sims_fee_period8"].ToString();
                                objNew.section_fee_period9 = dr["sims_fee_period9"].ToString();
                                objNew.section_fee_period10 = dr["sims_fee_period10"].ToString();
                                objNew.section_fee_period11 = dr["sims_fee_period11"].ToString();
                                objNew.section_fee_period12 = dr["sims_fee_period12"].ToString();
                            }
                            objNew.section_fee_code_type_name = dr["FeeCode"].ToString();
                            if (dr["sims_fee_refundable"].ToString().Equals("Y"))
                                objNew.section_fee_refundable = true;
                            else
                                objNew.section_fee_refundable = false;
                            objNew.section_fee_frequency = dr["sims_fee_frequency"].ToString();
                            objNew.section_fee_frequency_name = dr["FeeFrequency"].ToString();
                            if (dr["sims_fee_mandatory"].ToString().Equals("Y"))
                                objNew.section_fee_mandatory = true;
                            else
                                objNew.section_fee_mandatory = false;

                            if (dr["sims_fee_manual_receipt"].ToString().Equals("Y"))
                                objNew.section_fee_manual_receipt = true;
                            else
                                objNew.section_fee_manual_receipt = false;

                            if (dr["sims_fee_split_flag"].ToString().Equals("Y"))
                                objNew.section_fee_split_flag = true;
                            else
                                objNew.section_fee_split_flag = false;

                            fee_list.Add(objNew);

                            //total = fee_list.Count;
                            //skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }


        [Route("GetPageLoadValues")]
        public HttpResponseMessage GetPageLoadValues()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCategoryNames(),PARAMETERS  :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCategoryNames"));

            //List<Sims036> fee_list = new List<Sims036>();


            Dictionary<string, List<Sims036>> fee_list_dictionary = new Dictionary<string, List<Sims036>>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                       new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Curriculum_Names", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.section_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.transport_fee_category_name = dr["sims_fee_category_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Catagory", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Description", fee_list);
                    }
                    dr.Close();

                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                      new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_frequency = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_frequency_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_Frequency", fee_list);
                    }
                    dr.Close();


                    dr = db.ExecuteStoreProcedure("sims_section_fee",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                         });
                    if (dr.HasRows)
                    {
                        List<Sims036> fee_list = new List<Sims036>();
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code_type = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_code_type_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                        fee_list_dictionary.Add("Fee_code_type", fee_list);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, fee_list_dictionary);
            }
            catch (Exception e)
            {
                Log.Error(e);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list_dictionary);
            }
        }


        [Route("getAllFeeCategoryNames")]
        public HttpResponseMessage getAllFeeCategoryNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCategoryNames(),PARAMETERS  :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCategoryNames"));

            List<Sims036> fee_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.section_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.transport_fee_category_name = dr["sims_fee_category_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {
                Log.Error(e);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("getAllFeeType")]
        public HttpResponseMessage getAllFeeType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeType"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllFeeFrequency")]
        public HttpResponseMessage getAllFeeFrequency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeFrequency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeFrequency"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_frequency = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_frequency_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllFeeCodeType")]
        public HttpResponseMessage getAllFeeCodeType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCodeType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCodeType"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code_type = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_code_type_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllRemainingFeeCodeType")]
        public HttpResponseMessage getAllRemainingFeeCodeType(string cur_code, string academic_year, string grade_code, string section_code, string fee_category_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRemainingFeeCodeType(),PARAMETERS ::CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECATEGORYCODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllRemainingFeeCodeType", cur_code, academic_year, grade_code, section_code, fee_category_code));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Y"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@gradecode", grade_code),
                            new SqlParameter("@sectioncode", section_code),
                            new SqlParameter("@feecategorycode", fee_category_code),
                       });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        //For SectionFeeCopy Methods

        [Route("getAllGradeSectionName")]
        public HttpResponseMessage getAllRemainingFeeCodeType(string cur_code, string academic_year, string section_code, string category_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRemainingFeeCodeType(),PARAMETERS ::CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECATEGORYCODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllRemainingFeeCodeType", cur_code, academic_year, section_code, category_code));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@sectioncodes", section_code),
                            new SqlParameter("@categorycode", category_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_section_code_to = dr["sims_section_code"].ToString();
                            objNew.section_fee_section_name_to = dr["sims_section_name_en"].ToString();
                            objNew.section_fee_grade_code_to = dr["sims_grade_code"].ToString();
                            objNew.section_fee_grade_name_to = dr["sims_grade_name_en"].ToString();
                            objNew.overwrite = dr["FeeDefined"].ToString().Equals("A") ? true : false;
                            fee_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllSectionFeeNumbersForUpdate")]
        public HttpResponseMessage getAllSectionFeeNumbersForUpdate(string cur_code, string academic_year, string grade_code, string section_code, string fee_category_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionFeeNumbersForUpdate(),PARAMETERS ::CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECATEGORYCODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllSectionFeeNumbersForUpdate", cur_code, academic_year, grade_code, section_code, fee_category_code));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@gradecode", grade_code),
                            new SqlParameter("@sectioncode", section_code),
                            new SqlParameter("@feecategorycode", fee_category_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllSectionFees")]
        public HttpResponseMessage getAllSectionFees()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllSectionFees"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_number = dr["sims_fee_number"].ToString();
                            objNew.section_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.section_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.section_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            objNew.section_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.section_fee_category = dr["sims_fee_category"].ToString();
                            objNew.section_fee_code = dr["sims_fee_code"].ToString();
                            objNew.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getIfFeesExists")]
        public HttpResponseMessage getIfFeesExists(string cur_code, string academic_year, string grade_code, string section_code, string fee_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getIfFeesExists(),PARAMETERS :: CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getIfFeesExists", cur_code, academic_year, grade_code, section_code, fee_code));

            bool ifexists = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@gradecode", grade_code),
                            new SqlParameter("@sectioncode", section_code),
                            new SqlParameter("@sims_fee_code", fee_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ifexists = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, ifexists);

            }
            return Request.CreateResponse(HttpStatusCode.OK, ifexists);
        }



        #region   (section fee new table structure)


        [Route("InsertSectionFeeDetailsNew")]
        public HttpResponseMessage InsertSectionFeeDetailsNew(List<Sims036> simsobjlist)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSectionFeeDetails(),PARAMETERS :: OPR {2}";
            //Log.Debug(string.Format(debug, "STUDENT", "CUDSectionFeeDetails"));

            //  Sims036 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims036>(data);
            //   List<Sims036> mylist = new List<Sims036>();

            bool result = false;
            try
            {
                if (simsobjlist != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        SqlDataReader dr;
                        db.Open();
                        foreach (Sims036 simsobj in simsobjlist)
                        {
                            dr = db.ExecuteStoreProcedure("[dbo].[sims_section_fee_new]",
                               new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                           // new SqlParameter("@sims_fee_number", feenumber),//simsobj.section_fee_number);
                            new SqlParameter("@sims_fee_cur_code", simsobj.section_fee_cur_code_name),
                            new SqlParameter("@sims_fee_academic_year", simsobj.section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj.section_fee_grade_name),
                            new SqlParameter("@sims_fee_section_code", simsobj.section_fee_section_name),
                            new SqlParameter("@sims_fee_category", simsobj.section_fee_category_name),
                            new SqlParameter("@sims_fee_code", simsobj.section_fee_code),
                            new SqlParameter("@sims_fee_code_type", !string.IsNullOrEmpty(simsobj.section_fee_code_type)?simsobj.section_fee_code_type:"O"),
                            new SqlParameter("@sims_fee_refundable", simsobj.section_fee_refundable==true?'Y':'N'),
                            new SqlParameter("@sims_fee_manual_receipt", simsobj.section_fee_manual_receipt==true?'Y':'N'),
                            new SqlParameter("@sims_fee_frequency", simsobj.section_fee_frequency),
                            new SqlParameter("@sims_fee_mandatory", simsobj.section_fee_mandatory==true?'Y':'N'),
                            new SqlParameter("@sims_fee_amount", simsobj.section_fee_amount),
                            new SqlParameter("@sims_fee_installment_mode", simsobj.section_fee_installment_mode==true?'Y':'N'),
                            new SqlParameter("@sims_fee_installment_min_amount", simsobj.section_fee_installment_min_amount),
                            new SqlParameter("@sims_fee_period_code	", simsobj.sims_fee_period_code),
                            new SqlParameter("@sims_fee_period_amt	", simsobj.sims_fee_period_amt),

                            new SqlParameter("@sims_fee_status", simsobj.section_fee_status==true?'A':'I'),
                            new SqlParameter("@sims_fee_split_flag",simsobj.section_fee_split_flag==true?'Y':'N'),
                         });

                            if (dr.RecordsAffected > 0)
                            {
                                result = true;
                                dr.Close();
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDSectionFeeDetailsnewNew")]
        public HttpResponseMessage CUDSectionFeeDetailsnewNew(List<Sims036> simsobj1)
        {
            List<Sims036> obj = new List<Sims036>();
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims036 simsobj in simsobj1)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_section_fee_new]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                            new SqlParameter("@sims_fee_cur_code", simsobj.section_fee_cur_code),
                            new SqlParameter("@sims_fee_academic_year", simsobj.section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj.section_fee_grade_code),
                            new SqlParameter("@sims_fee_section_code", simsobj.section_fee_section_code),
                            new SqlParameter("@sims_fee_category", simsobj.section_fee_category),
                            new SqlParameter("@sims_fee_code", simsobj.section_fee_code),
                            new SqlParameter("@sims_fee_code_type", !string.IsNullOrEmpty(simsobj.section_fee_code_type)?simsobj.section_fee_code_type:"O"),
                            new SqlParameter("@sims_fee_manual_receipt", simsobj.section_fee_manual_receipt==true?'Y':'N'),
                            new SqlParameter("@sims_fee_frequency", simsobj.section_fee_frequency),
                            new SqlParameter("@sims_fee_mandatory", simsobj.section_fee_mandatory==true?'Y':'N'),
                            new SqlParameter("@sims_fee_amount", simsobj.section_fee_amount),
                            new SqlParameter("@sims_fee_installment_mode", simsobj.section_fee_installment_mode==true?'Y':'N'),
                            new SqlParameter("@sims_fee_installment_min_amount", simsobj.section_fee_installment_min_amount),
                            new SqlParameter("@sims_fee_period_code", simsobj.sims_fee_period_code),
                            new SqlParameter("@sims_fee_period_amt", simsobj.sims_fee_period_amt),

                            new SqlParameter("@sims_fee_status", simsobj.section_fee_status==true?'A':'I'),
                            new SqlParameter("@sims_fee_split_flag",simsobj.section_fee_split_flag==true?'Y':'N'),
                            new SqlParameter("@sims_fee_refundable", simsobj.section_fee_refundable==true?'Y':'N'),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                        dr.Close();

                    }

                    if (result == true)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims_section_fee",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_fee_cur_code", simsobj1[0].section_fee_cur_code),
                            new SqlParameter("@sims_fee_academic_year", simsobj1[0].section_fee_academic_year),
                            new SqlParameter("@sims_fee_grade_code", simsobj1[0].section_fee_grade_code),
                            new SqlParameter("@sims_fee_section_code", simsobj1[0].section_fee_section_code),
                            new SqlParameter("@sims_fee_code", simsobj1[0].section_fee_code)
                    });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                Sims036 b = new Sims036();
                                b.section_fee_amount = dr1["section_fee_amount"].ToString();
                                b.full_name = dr1["student_name"].ToString();
                                b.sims_fee_new_amt = dr1["sims_fee_new_amt"].ToString();
                                obj.Add(b);
                            }
                        }
                    }
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }


        [Route("getSectionFeeNew")]
        public HttpResponseMessage getSectionFeeNew(string cur_name, string academic_year, string grade_name, string section_name, string fee_category)//, int pageIndex, int PageSize
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFee(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5},FEECATEGORY{6}";
            //Log.Debug(string.Format(debug, "STUDENT", "getSectionFee"));

            List<Sims036> fee_list = new List<Sims036>();
            //int total = 0, skip = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_section_fee_new]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'H'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                                new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                                new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                                new SqlParameter("@sims_fee_number", ""),
                                new SqlParameter("@sims_fee_cur_code", cur_name),
                                new SqlParameter("@sims_fee_academic_year", academic_year),
                                new SqlParameter("@sims_fee_grade_code", grade_name),
                                new SqlParameter("@sims_fee_section_code", section_name),
                                new SqlParameter("@sims_fee_category", fee_category),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.section_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.section_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            objNew.section_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.section_fee_category = dr["sims_fee_category"].ToString();
                            objNew.section_fee_code = dr["sims_fee_code"].ToString();
                            objNew.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            objNew.section_fee_code_type = dr["sims_fee_code_type"].ToString();
                            objNew.sims_term_start_date = db.UIDDMMYYYYformat(dr["term_start_date"].ToString());
                            objNew.sims_month_code = dr["month_code"].ToString();
                            if (dr["sims_fee_code_type"].ToString().Equals("M"))
                            {
                                using (DBConnection db11 = new DBConnection())
                                {
                                    db11.Open();
                                    List<Sims039> subject_list1 = new List<Sims039>();
                                    SqlDataReader dr11 = db11.ExecuteStoreProcedure("[dbo].[sims_section_fee_new]", new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'R'),
                                    new SqlParameter("@section_feenumber", objNew.section_fee_code),
                                });
                                    if (dr11.HasRows)
                                    {
                                        while (dr11.Read())
                                        {
                                            objNew.section_fee_amount = dr11["sims_membership_fee_amount"].ToString();
                                            if (dr11["sims_membership_fee_installment_mode"].ToString().Equals("Y"))
                                                objNew.section_fee_installment_mode = true;
                                            else
                                                objNew.section_fee_refundable = false;
                                            objNew.section_fee_installment_min_amount = dr11["sims_membership_fee_installment_min_amount"].ToString();
                                            if (dr11["sims_membership_fee_status"].ToString().Equals("A"))
                                                objNew.section_fee_status = true;
                                            else
                                                objNew.section_fee_status = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                objNew.section_fee_amount = dr["sims_fee_amount"].ToString();
                                if (dr["sims_fee_installment_mode"].ToString().Equals("Y"))
                                    objNew.section_fee_installment_mode = true;
                                else
                                    objNew.section_fee_installment_mode = false;
                                objNew.section_fee_installment_min_amount = dr["sims_fee_installment_min_amount"].ToString();
                                if (dr["sims_fee_status"].ToString().Equals("A"))
                                    objNew.section_fee_status = true;
                                else
                                    objNew.section_fee_status = false;

                                if (dr["sims_fee_refundable"].ToString().Equals("Y"))
                                    objNew.section_fee_refundable = true;
                                else
                                    objNew.section_fee_refundable = false;


                                objNew.section_fee_period1 = dr["sims_fee_period1"].ToString();
                                objNew.section_fee_period2 = dr["sims_fee_period2"].ToString();
                                objNew.section_fee_period3 = dr["sims_fee_period3"].ToString();
                                objNew.section_fee_period4 = dr["sims_fee_period4"].ToString();
                                objNew.section_fee_period5 = dr["sims_fee_period5"].ToString();
                                objNew.section_fee_period6 = dr["sims_fee_period6"].ToString();
                                objNew.section_fee_period7 = dr["sims_fee_period7"].ToString();
                                objNew.section_fee_period8 = dr["sims_fee_period8"].ToString();
                                objNew.section_fee_period9 = dr["sims_fee_period9"].ToString();
                                objNew.section_fee_period10 = dr["sims_fee_period10"].ToString();
                                objNew.section_fee_period11 = dr["sims_fee_period11"].ToString();
                                objNew.section_fee_period12 = dr["sims_fee_period12"].ToString();
                            }
                            objNew.section_fee_code_type_name = dr["FeeCode"].ToString();
                            objNew.section_fee_frequency = dr["sims_fee_frequency"].ToString();
                            objNew.section_fee_frequency_name = dr["FeeFrequency"].ToString();
                            if (dr["sims_fee_mandatory"].ToString().Equals("Y"))
                                objNew.section_fee_mandatory = true;
                            else
                                objNew.section_fee_mandatory = false;

                            if (dr["sims_fee_manual_receipt"].ToString().Equals("Y"))
                                objNew.section_fee_manual_receipt = true;
                            else
                                objNew.section_fee_manual_receipt = false;

                            if (dr["sims_fee_split_flag"].ToString().Equals("Y"))
                                objNew.section_fee_split_flag = true;
                            else
                                objNew.section_fee_split_flag = false;

                            fee_list.Add(objNew);

                            //total = fee_list.Count;
                            //skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list); //.Skip(skip).Take(PageSize).ToList()
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }


        #endregion



    }
}