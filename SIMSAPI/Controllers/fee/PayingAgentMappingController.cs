﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.FEE;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.fee;
namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/PayagentMappingController")]
    public class PayingAgentMappingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getPayingAgentDetails")]
        public HttpResponseMessage getPayingAgentDetails(string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPayingAgentDetails()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Fee/", "getPayingAgentDetails"));

            List<Sim531> concess_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_paying_agent_mapping]",
                        new List<SqlParameter>()
                         {
                    new SqlParameter("@opr", "AG"),
                    new SqlParameter("@a_year", academic_year)

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_paying_agent_description = dr["sims_paying_agent_description"].ToString();
                            simsobj.sims_ledger_code = dr["sims_paying_ldgrctl_code"].ToString();
                            simsobj.sims_ledger_ac_no = dr["sims_paying_agent_slma_ldgr"].ToString();
                            simsobj.sims_paying_agent_number = dr["sims_paying_agent_number"].ToString();
                            concess_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("GetSPAM_Students")]
        public HttpResponseMessage GetSPAM_Students(string cur_code, string a_year, string grade_code, string s_code, string search, string agent_tran_no)
        {
            string agent_number = string.Empty;
            List<Sims531> coll = new List<Sims531>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_paying_agent_mapping]",
                        new List<SqlParameter>()
                         {
               new SqlParameter("@opr", "S"),
               new SqlParameter("@cur_code", cur_code),
               new SqlParameter("@a_year", a_year),
               new SqlParameter("@grade_code", grade_code),
               new SqlParameter("@section_code", s_code),
               new SqlParameter("@search_text", search=="null"?null:search)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims531 obj = new Sims531();
                            obj.count = 0;
                            obj.agent_tran_no = agent_tran_no;
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_class = dr["Class"].ToString();
                            obj.sims_student_name = dr["Student_Name"].ToString();
                            obj = GetSPAM_Students_fee(obj);
                            coll.Add(obj);

                        }
                        dr.Close();
                    }


                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, coll);

        }


        [Route("GetSPAM_Students_fee")]
        public Sims531 GetSPAM_Students_fee(Sims531 a)
        {
            string agent_number = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_student_paying_agent_mapping]",
                       new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "SM"),
                            new SqlParameter("@a_year", a.sims_academic_year),
                            new SqlParameter("@search_text", a.sims_enroll_number),
                            new SqlParameter("@sims_pa_transaction_number", a.agent_tran_no)
                        });
                    if (dr1.HasRows)
                    {
                        a.mapping = new List<Sims531_mapping>();
                        while (dr1.Read())
                        {
                            Sims531_mapping obj_map = new Sims531_mapping();
                            obj_map.sims_pa_transaction_number = dr1["sims_pa_transaction_number"].ToString();
                            obj_map.sims_pa_transaction_line_no = dr1["sims_pa_transaction_line_no"].ToString();
                            obj_map.sims_paying_agent_fee_code = dr1["sims_paying_agent_fee_code"].ToString();
                            obj_map.sims_fee_code_description = dr1["sims_fee_code_description"].ToString();
                            obj_map.sims_enroll_number = dr1["sims_enroll_number"].ToString();
                            obj_map.sims_status = dr1["sims_status"].ToString().Equals("A") ? true : false;
                            obj_map.sims_paying_ldgrctl_code = dr1["sims_paying_ldgrctl_code"].ToString();
                            obj_map.sims_paying_agent_slma_ldgr = dr1["sims_paying_agent_slma_ldgr"].ToString();
                            obj_map.sims_paying_agent_discount_type = dr1["sims_paying_agent_discount_type"].ToString();
                            obj_map.sims_paying_agent_discount_type_dsc = dr1["sims_paying_agent_discount_type_dsc"].ToString();
                            obj_map.sims_paying_agent_discount_value = dr1["sims_paying_agent_discount_value"].ToString();
                            obj_map.sims_academic_year = dr1["sims_paying_agent_academic_year"].ToString();
                            a.mapping.Add(obj_map);

                        }
                    }
                }
            }


            catch (Exception x) { }
            return a;

        }


        [Route("SPAM_Save")]
        public HttpResponseMessage SPAM_Save(List<Sims531_mapping> coll)
        {
            bool res = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims531_mapping map in coll)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_paying_agent_mapping]",
                            new List<SqlParameter>()
                             {
                       new SqlParameter("@opr", "I"),
                       new SqlParameter("@sims_pa_transaction_number", map.sims_pa_transaction_number),
                       new SqlParameter("@sims_pa_transaction_line_no", map.sims_pa_transaction_line_no),
                       new SqlParameter("@a_year", map.sims_academic_year),
                       new SqlParameter("@search_text", map.sims_enroll_number),
                       new SqlParameter("@sims_fee_code", map.sims_paying_agent_fee_code),
                       new SqlParameter("@sims_discount_type", map.sims_paying_agent_discount_type),
                       new SqlParameter("@sims_discount_value", map.sims_paying_agent_discount_value),
                       new SqlParameter("@slma_ldgrctl_code", map.sims_paying_ldgrctl_code),
                       new SqlParameter("@slma_acno", map.sims_paying_agent_slma_ldgr),
                       new SqlParameter("@sims_status", map.sims_status == true?"A":"I")
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            res = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }


        [Route("getPayingAgentDetails")]
        public HttpResponseMessage getPayingAgentDetails()
        {
            string agent_number = string.Empty;
            List<Sims531> coll = new List<Sims531>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_paying_agent_mapping]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "P"),
                          });
                       if (dr.HasRows)
                      {
                        while (dr.Read())
                        {
                            Sims531 obj = new Sims531();
                          
                            obj.fins_appl_form_field_value3 = dr["fins_appl_form_field_value3"].ToString();
                            obj.slma_acno = dr["slma_acno"].ToString();
                            obj.coad_pty_full_name = dr["coad_pty_full_name"].ToString();
                            obj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            obj.student_count = dr["student_count"].ToString();
                            coll.Add(obj);
                        }
                      
                    }


                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, coll);

        }


        //API For Late Fee Master

        [Route("getLateFeeFrequency")]
        public HttpResponseMessage getLateFeeFrequency()
        {
            string agent_number = string.Empty;
            List<Sims531> coll = new List<Sims531>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_late_fee_schedule_proc]",
                    new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", "J"),
                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims531 obj = new Sims531();

                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            obj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                            coll.Add(obj);
                        }

                    }


                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, coll);

        }

        [Route("getLateFeeDetails")]
        public HttpResponseMessage getLateFeeDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPayingAgentDetails()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Fee/", "getPayingAgentDetails"));

            List<Sims531> concess_list = new List<Sims531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_late_fee_schedule_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims531 simsobj = new Sims531();
                            simsobj.sims_late_fee_master_sr_no = dr["sims_late_fee_master_sr_no"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_fee_frequency = dr["sims_fee_frequency"].ToString();
                            simsobj.sims_late_fee_applicable_after_days = dr["sims_late_fee_applicable_after_days"].ToString();
                            simsobj.sims_fee_frequency_name = dr["sims_fee_frequency_name"].ToString();
                            simsobj.sims_late_fee_end_date_flag = dr["sims_late_fee_end_date_flag"].Equals("A") ? true : false;
                            simsobj.sims_late_fee_status = dr["sims_late_fee_status"].Equals("A") ? true : false;
                            concess_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("CUDLateFeeSchedule")]
        public HttpResponseMessage CUDLateFeeSchedule(List<Sims531> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims531 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_late_fee_schedule_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_fee_frequency", simsobj.sims_appl_parameter),
                                new SqlParameter("@sims_late_fee_applicable_after_days", simsobj.sims_late_fee_applicable_after_days),
                                new SqlParameter("@sims_late_fee_master_sr_no", simsobj.sims_late_fee_master_sr_no),
                                

                                new SqlParameter("@sims_late_fee_end_date_flag", simsobj.sims_late_fee_end_date_flag==true?"A":"I"),
                                new SqlParameter("@sims_late_fee_status", simsobj.sims_late_fee_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
        }

            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
