﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Newtonsoft.Json;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/common/Concession")]
    public class StudentFeeConcessionController:ApiController
    {


        [Route("getComnSequenceCode")]
        public HttpResponseMessage getComnSequenceCode()
        {

            string next_comm_number = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'W'),
                                    new SqlParameter("@applcode", CommonStaticClass.Sims017_appl_code),
                                new SqlParameter("@sequence_code", CommonStaticClass.Sims017_sequence_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            next_comm_number = dr["comn_number_sequence"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, next_comm_number);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, next_comm_number);
            }
        }

        [Route("getAcadamicYear")]
        public HttpResponseMessage getAcadamicYear()
        {
            List<simsClass> acayear_list = new List<simsClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'H'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();
                            simsobj.sims_concession_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.academic_year = dr["sims_academic_year_description"].ToString();
                            acayear_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }

        [Route("getAllFeeCode")]
        public HttpResponseMessage getAllFeeCode()
        {

            List<Sims017> fee_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsObj = new Sims017();
                            simsObj.sims_concession_fee_code = dr["sims_fee_code"].ToString();
                            simsObj.sims_concession_fee_code_desc = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("getConcessionType")]
        public HttpResponseMessage getConcessionType()
        {


            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsobj = new Sims017();
                            simsobj.sims_concession_type = dr["sims_appl_form_field_value1"].ToString();
                            concession_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, concession_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
        }

        [Route("getConcessionApplicableOn")]
        public HttpResponseMessage getConcessionApplicableOn()
        {


            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims017_appl_code),
                                new SqlParameter("@sims_concession_applicable_on", CommonStaticClass.Sims017_applicable_on),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsobj = new Sims017();
                            simsobj.sims_concession_applicable_on = dr["sims_appl_form_field_value1"].ToString();
                            concession_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, concession_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
        }

        [Route("getConcessionApplicableTo")]
        public HttpResponseMessage getConcessionApplicableTo()
        {

            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'F'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsobj = new Sims017();
                            simsobj.sims_concession_applicable_to = dr["sims_appl_form_field_value1"].ToString();
                            concession_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, concession_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
        }

        [Route("getConcessionDiscountType1")]
        public HttpResponseMessage getConcessionDiscountType1()
        {

            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims017_appl_code),
                                new SqlParameter("@sims_concession_discount_type", CommonStaticClass.Sims017_discount_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsobj = new Sims017();
                            simsobj.sims_concession_discount_type = dr["sims_appl_form_field_value1"].ToString();
                            concession_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, concession_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
        }

        [Route("CUSimsConcession")]
        public HttpResponseMessage CUDSimsConcession(List<Sims017> data)
        {

                        Message message = new Message();
            int cnt = 1;
            try
            {
                if (data != null)
                {
                    foreach (Sims017 simsobj in data)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                                new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims017_appl_code),
                            new SqlParameter("@sims_concession_type_field", CommonStaticClass.Sims017_concession_type),
                            new SqlParameter("@sims_discount_type_field", CommonStaticClass.Sims017_discount_type),
                            new SqlParameter("@sims_applicable_on_field", CommonStaticClass.Sims017_applicable_on),
                            new SqlParameter("@sims_applicable_to_field", CommonStaticClass.Sims017_applicable_to),
                            new SqlParameter("@comn_sequence_code", CommonStaticClass.Sims017_sequence_code),
                            new SqlParameter("@sims_concession_number", simsobj.sims_concession_number),
                            new SqlParameter("@sims_concession_description", simsobj.sims_concession_description),
                            new SqlParameter("@sims_concession_type", simsobj.sims_concession_type),
                            new SqlParameter("@sims_concession_discount_type", simsobj.sims_concession_discount_type),
                            new SqlParameter("@sims_concession_discount_value", simsobj.sims_concession_discount_value),
                            new SqlParameter("@sims_concession_applicable_on", simsobj.sims_concession_applicable_on),
                            new SqlParameter("@sims_concession_corporate_billing", simsobj.sims_concession_corporate_billing == true?"Y":"N"),
                            new SqlParameter("@sims_concession_academic_year", simsobj.sims_concession_academic_year),
                            new SqlParameter("@sims_concession_applicable_to", simsobj.sims_concession_applicable_to),
                            new SqlParameter("@sims_concession_onward_child", simsobj.sims_concession_onward_child),
                            new SqlParameter("@sims_concession_fee_code", simsobj.sims_concession_fee_code),
                            new SqlParameter("@sims_concession_status", simsobj.sims_concession_status == true?"A":"I"),
                            new SqlParameter("@sims_receivable_acno_flag", simsobj.sims_receivable_acno_flag == true?"Y":"N"),

                            new SqlParameter("@count", cnt),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                if (simsobj.opr.Equals("U"))
                                    message.strMessage = "Concession Updated Sucessfully";
                                else if (simsobj.opr.Equals("I"))
                                    message.strMessage = "Concession  Defined Sucessfully";
                                else if (simsobj.opr.Equals("D"))
                                    message.strMessage = "Concession  Updated Sucessfully";

                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                                cnt = cnt + 1;
                            }
                            else
                            {
                                if (simsobj.opr.Equals("U"))
                                    message.strMessage = "Concession Not Updated";
                                else if (simsobj.opr.Equals("I"))
                                    message.strMessage = "Concession  Not Defined";
                                else if (simsobj.opr.Equals("D"))
                                    message.strMessage = "Concession  Updated Sucessfully";
                                message.systemMessage = string.Empty;
                                message.messageType = MessageType.Success;
                            }

                            dr.Close();
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Defined Concession";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("SimsConcessionDelete")]
        public HttpResponseMessage SimsConcessionDelete(List<Sims017> data)
        {
            Message message = new Message();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    foreach (Sims017 simsobj in data)
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                          new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@sims_concession_number", simsobj.sims_concession_number),
                             new SqlParameter("@sims_concession_fee_code",simsobj.sims_concession_fee_code)
                             });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Concession  Deleted Sucessfully";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        else
                        {
                            message.strMessage = "Concession  Not Deleted";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        dr.Close();
                    }

                }

            }
            catch (Exception x)
            {
                message.strMessage = "Error In Deleting Concession Details";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getAllSimsConcession")]
        public HttpResponseMessage getAllSimsConcession()
        {

            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims017_appl_code),
                                new SqlParameter("@sims_concession_type_field", CommonStaticClass.Sims017_concession_type),
                                new SqlParameter("@sims_discount_type_field", CommonStaticClass.Sims017_discount_type),
                                new SqlParameter("@sims_applicable_on_field", CommonStaticClass.Sims017_applicable_on),
                                new SqlParameter("@sims_applicable_to_field", CommonStaticClass.Sims017_applicable_to),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str = dr["sims_concession_number"].ToString();
                            string academic_year = dr["sims_concession_academic_year"].ToString();

                            var v = (from p in concession_list where p.sims_concession_number == str && p.sims_concession_academic_year==academic_year select p);
                            if (v.Count() == 0)
                            {
                                Sims017 simsobj = new Sims017();
                                simsobj.list = new List<SimsList017>();
                                SimsList017 obj = new SimsList017();
                                simsobj.sims_concession_number = dr["sims_concession_number"].ToString();
                                simsobj.sims_concession_description = dr["sims_concession_description"].ToString();
                                simsobj.sims_concession_type = dr["sims_concession_type"].ToString();
                                simsobj.sims_concession_discount_type = dr["sims_concession_discount_type"].ToString();
                                simsobj.sims_concession_discount_value = dr["sims_concession_discount_value"].ToString();
                                simsobj.sims_concession_applicable_on = dr["sims_concession_applicable_on"].ToString();
                                simsobj.sims_concession_corporate_billing = dr["sims_concession_corporate_billing"].ToString().Equals("Y") ? true : false;
                                simsobj.sims_concession_academic_year = dr["sims_concession_academic_year"].ToString();
                                simsobj.sims_concession_applicable_to = dr["sims_concession_applicable_to"].ToString();
                                simsobj.sims_concession_onward_child = int.Parse(dr["sims_concession_onward_child"].ToString());
                                simsobj.sims_concession_fee_code = dr["sims_concession_fee_code"].ToString();
                                simsobj.sims_concession_fee_code_desc = dr["sims_concession_fee_desc"].ToString();
                                simsobj.sims_concession_status = dr["sims_concession_status"].ToString().Equals("A") ? true : false;
                                simsobj.sims_receivable_acno_flag = dr["sims_receivable_acno_flag"].ToString().Equals("Y") ? true : false;
                                obj.sims_concession_fee_code1 = dr["sims_concession_fee_code"].ToString();

                                try
                                {
                                    simsobj.sims_concession_academic_year_description = dr["sims_concession_academic_year_description"].ToString();

                                }
                                catch (Exception e)
                                { 
                                }
                                simsobj.list.Add(obj);
                                concession_list.Add(simsobj);
                            }
                            else
                            {
                                SimsList017 obj = new SimsList017();
                                obj.sims_concession_fee_code1 = dr["sims_concession_fee_code"].ToString();
                                v.ElementAt(0).list.Add(obj);
                            }

                        }
                    }
                }

            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, concession_list);
        }

        [Route("getcheckSimsConcession")]
        public HttpResponseMessage getcheckSimsConcession(string concession_number)
        {
            bool inserted = false;
            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                      new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_concession_number", concession_number),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            inserted = true;

                        }
                    }
                }

            }
            catch (Exception e)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getcheckstatus")]
        public HttpResponseMessage getcheckstatus(string conce_no)
        {
            List<Sims017> acayear_list = new List<Sims017>();
            Sims017 simsobj = new Sims017();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                                new SqlParameter("@sims_concession_number",conce_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            acayear_list.Add(simsobj);
                        }
                    }
                    else
                    {
                        simsobj.sims_status = false;
                        acayear_list.Add(simsobj);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }


    }
}