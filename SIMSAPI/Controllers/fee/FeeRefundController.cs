﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/Feerefund")]
    public class FeeRefundController : ApiController
    {

        #region fee Refund
        [Route("Get_sims550_Curriculum")]
        public HttpResponseMessage Get_sims550_Curriculum()
        {
            List<Sims550> lstEmp = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "C")});
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.cur_code = dr["sims_cur_code"].ToString();
                            obj.cur_desc = dr["sims_cur_short_name_en"].ToString();
                            lstEmp.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("Get_sims550_academic")]
        public HttpResponseMessage Get_sims550_academic(string cur)
        {
            List<Sims550> lstEmp = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                new SqlParameter("@cur_code", cur)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.academic_year = dr["sims_academic_year"].ToString();
                            obj.academic_year_name = dr["sims_academic_year_description"].ToString();
                            lstEmp.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("Get_sims550_payment_mode")]
        public HttpResponseMessage Get_sims550_payment_mode()
        {
            List<Sims550> lstEmp = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "P")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.payment_mode = dr["sims_appl_form_field_value1"].ToString();
                            obj.payment_mode_code = dr["sims_appl_parameter"].ToString();
                            lstEmp.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("Get_sims550_bank")]
        public HttpResponseMessage Get_sims550_bank()
        {
            List<Sims550> lstEmp = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.bank_code = dr["pb_bank_code"].ToString();
                            obj.bank_name = dr["pb_bank_name"].ToString();
                            lstEmp.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("Get_sims550_report_url")]
        public HttpResponseMessage Get_sims550_report_url()
        {
            string lstEmp = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "D")});
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lstEmp = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("GetGradeSection")]
        public HttpResponseMessage GetGradeSection(string cur, string academic)
        {
            List<Sims550> Lst = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "G"),
                new SqlParameter("@cur_code", cur),
                new SqlParameter("@academic_year", academic)

                  });
                    while (dr.Read())
                    {
                        Sims550 simsobj = new Sims550();
                        simsobj.grade_code = dr["sims_grade_code"].ToString();
                        simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                        simsobj.section_code = dr["sims_Section_code"].ToString();
                        simsobj.section_name = dr["sims_section_name_en"].ToString();
                        Lst.Add(simsobj);
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Lst);

        }

        [Route("StudentDetailsRefound")]
        public HttpResponseMessage StudentDetailsRefound(Sims550 data)
        {
            List<Sims550> Lst = new List<Sims550>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@grade_section", data.grade_code+data.section_code),
                new SqlParameter("@cur_code", data.cur_code),
                new SqlParameter("@academic_year", data.academic_year),
                new SqlParameter("@enroll_number", data.enroll_number),
                new SqlParameter("@parent_number", data.parentcode)
                         });
                    while (dr.Read())
                    {
                        Sims550 simsobj = new Sims550();
                        simsobj.academic_year = dr["sims_academic_year"].ToString();
                        simsobj.academic_year_name = dr["sims_academic_year_description"].ToString();
                        simsobj.enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.student_name = dr["stud_name"].ToString();
                        simsobj.grade_code = dr["sims_grade_code"].ToString();
                        simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                        simsobj.section_code = dr["sims_section_code"].ToString();
                        simsobj.section_name = dr["sims_section_name_en"].ToString();
                        simsobj.fee_code = dr["sims_fee_code"].ToString();
                        simsobj.fee_desc = dr["sims_fee_code_description"].ToString();
                        simsobj.cur_code = dr["sims_cur_code"].ToString();
                        simsobj.cur_desc = dr["sims_cur_short_name_en"].ToString();
                        //simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                        simsobj.amount = decimal.Round(decimal.Parse(dr["sims_refund_amount"].ToString()), 2).ToString();
                        simsobj.amount_used = decimal.Round(decimal.Parse(dr["sims_refund_amount_used"].ToString()), 2).ToString();
                        simsobj.total_amount = decimal.Round(decimal.Parse(dr["total_amount"].ToString()), 2);
                        simsobj.remark = dr["sims_remarks"].ToString();

                        Lst.Add(simsobj);
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, Lst);

        }

        [Route("UpdateStudentDetailsRefound")]
        public HttpResponseMessage UpdateStudentDetailsRefound(Sims550 item)
        {
            string Flag = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
             new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", "I"),
                new SqlParameter("@grade_section", item.grade_code + item.section_code),
                new SqlParameter("@cur_code", item.cur_code),
                new SqlParameter("@academic_year", item.academic_year),
                new SqlParameter("@enroll_number", item.enroll_number),
                new SqlParameter("@fee_code", item.fee_code),
                new SqlParameter("@payment_mode", item.payment_mode_code),
                new SqlParameter("@dt_date", db.DBYYYYMMDDformat(item.payment_date)),
                new SqlParameter("@remark", item.remark),
                new SqlParameter("@bank_code", item.bank_code),
                new SqlParameter("@chk_number", item.chk_no),
                new SqlParameter("@chk_date", db.DBYYYYMMDDformat(item.chk_Date))
               });
                    if (dr.Read())
                    {
                        Flag = dr["doc_no"].ToString();
                    }
                    dr.Close();
                }
            }

            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, Flag);

        }

        [Route("getAdjustFeeDetailsStudentRefound")]
        public HttpResponseMessage getAdjustFeeDetailsStudentRefound(string enroll, string cur_code, string aca_year)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
             new List<SqlParameter>() 
                         { 
                 new SqlParameter("@opr", "F"),
                new SqlParameter("@cur_code", cur_code),
                new SqlParameter("@enroll_number", enroll),
                new SqlParameter("@academic_year",aca_year)
                         });

                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_id = dr["id"].ToString();
                        simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                        simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                        simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                        simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                        simsobj.dd_fee_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_orignal_exp_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                        simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount_temp = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_concession_amount_used = "0";

                        if (dr["installment_mode"].ToString().ToLower() == "y")
                            simsobj.Installment_mode = true;
                        else
                            simsobj.Installment_mode = false;

                        simsobj.Installment_mode_chk = false;
                        simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                        simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();
                        mod_list.Add(simsobj);
                    }

                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("AdjustmentRefundInsertToFee")]
        public HttpResponseMessage UpdateStudentDetailsRefoundadjustfee(Sims550 item)
        {
            bool Flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Fee_refund_proc",
                    new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", "SI"),
                new SqlParameter("@grade_section", item.grade_code + item.section_code),
                new SqlParameter("@cur_code", item.cur_code),
                new SqlParameter("@academic_year", item.academic_year),
                new SqlParameter("@enroll_number", item.enroll_number),
                new SqlParameter("@fee_code", item.fee_code),
                new SqlParameter("@payment_mode", item.payment_mode_code),
                new SqlParameter("@dt_date", db.DBYYYYMMDDformat(item.payment_date)),
                new SqlParameter("@remark", item.remark),
                new SqlParameter("@bank_code", item.bank_code),
                new SqlParameter("@chk_number", item.chk_no),
                new SqlParameter("@chk_date", db.DBYYYYMMDDformat(item.chk_Date))
               });
                    if (dr.RecordsAffected > 0)
                    {
                        Flag = true;
                    }
                    dr.Close();


                }
            }


            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, Flag);

        }


        [Route("adjustmentRefundInsertToFee_Document")]
        public HttpResponseMessage RefundInsertToFee_Document(sims043 fdsimsobj)
        {

            string res = "false";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "II"),
                new SqlParameter("@doc_no", null),
                    new SqlParameter("@doc_date",db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@dt_code", "1"),
                new SqlParameter("@doc_total_amount", fdsimsobj.doc_total_amount),
                new SqlParameter("@doc_status", "1"),
                new SqlParameter("@enroll_number", fdsimsobj.doc_enroll_no),
                new SqlParameter("@doc_other_charge_desc", fdsimsobj.doc_other_charge_desc=="null"?null:fdsimsobj.doc_other_charge_desc),
                new SqlParameter("@doc_other_charge_amount", fdsimsobj.doc_other_charge_amount=="null"?null:fdsimsobj.doc_other_charge_amount),
                new SqlParameter("@doc_discount_pct", fdsimsobj.doc_discount_pct=="null"?null:fdsimsobj.doc_discount_pct),
                new SqlParameter("@doc_discount_amount", fdsimsobj.doc_discount_amount=="null"?null:fdsimsobj.doc_discount_amount),
                new SqlParameter("@doc_ref_code", null),
                new SqlParameter("@doc_narration", fdsimsobj.doc_narration=="null"?null:fdsimsobj.doc_narration),
                    new SqlParameter("@creation_date", db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name),//std_fee_student_name used as Created by User
                new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year)//new field added
               });
                    if (dr.RecordsAffected > 0)

                        while (dr.Read())
                        {
                            res = dr["doc_no"].ToString();
                        }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("AdjustmentRefundInsertToFee_Document_Details")]
        public HttpResponseMessage RefundInsertToFee_Document_Details(List<sims043> fddsimsobjlst)
        {
            string res = string.Empty;
            string doc_no = "";
            bool inserted = false;
            int a1 = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    for (int i = 0; i < fddsimsobjlst.Count; i++)
                    {
                        sims043 fddsimsobj = fddsimsobjlst[i];
                        if (fddsimsobj.sims_fee_collection_mode.Equals("M"))
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                       new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "ID"),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        new SqlParameter("@dd_line_no", (i + 1).ToString()),
                        new SqlParameter("@dt_code","4"),
                        new SqlParameter("@dd_fee_number", fddsimsobj.dd_fee_number),
                        new SqlParameter("@dd_fee_code", fddsimsobj.PayFeeCode),
                        new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@dd_fee_amount_discounted", fddsimsobj.PayDisAmount),
                            new SqlParameter("@dd_fee_amount_final",fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@dd_fee_payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@dd_fee_cheque_number", fddsimsobj.chequeDDNo),
                            new SqlParameter("@dd_fee_cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)=="N/A"?null:db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@dd_fee_cheque_bank_code", fddsimsobj.BankName),
                        new SqlParameter("@dd_fee_tt_ref_code", fddsimsobj.TransactionID),
                        //Parameter to Get Fee_Number
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@sims_fee_category", "01"),
                        //Fee Agent 
                        new SqlParameter("@slma_ldgrctl_code", fddsimsobj.slma_ldgrctl_code=="null"?null:fddsimsobj.slma_ldgrctl_code),
                        new SqlParameter("@slma_acno", fddsimsobj.slma_acno=="null"?null:fddsimsobj.slma_acno ),
                        new SqlParameter("@slma_ldgrctl_year", fddsimsobj.slma_ldgrctl_year=="null"?null: fddsimsobj.slma_ldgrctl_year),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@doc_paying_agent_transaction_no",fddsimsobj.doc_paying_agent_transaction_no=="null"?null:fddsimsobj.doc_paying_agent_transaction_no),
					    new SqlParameter("@doc_paying_agent_transaction_lineno",fddsimsobj.doc_paying_agent_transaction_lineno=="null"?null:fddsimsobj.doc_paying_agent_transaction_lineno),
                        //for Credit Card
                        new SqlParameter("@dd_fee_credit_card_code",fddsimsobj.dd_fee_credit_card_code=="null"?null:fddsimsobj.dd_fee_credit_card_code),
					    new SqlParameter("@dd_fee_credit_card_auth_code",fddsimsobj.dd_fee_credit_card_auth_code=="null"?null:fddsimsobj.dd_fee_credit_card_auth_code)
                         });
                            res = dr.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;
                            if (dr.RecordsAffected > 0)
                            {
                                a1 = a1 + 1;
                                inserted = true;

                                dr.Close();

                                SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                          new List<SqlParameter>()
                              {
                                new SqlParameter("@opr", "SU"),
                                new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                                new SqlParameter("@dd_fee_number", fddsimsobj.PayFeeCode.ToString()),
                                new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                                new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                                new SqlParameter("@dd_fee_grade_code",fddsimsobj.std_fee_grade_code),
                                new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                                new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                                new SqlParameter("@dd_fee_amount_final", fddsimsobj.PayAmount)

                        });
                                if (dr1.RecordsAffected > 0)
                                {
                                    inserted = true;

                                    if (inserted == true)
                                    {
                                        doc_no = fddsimsobjlst[0].doc_no;
                                    }
                                }
                                dr1.Close();



                            }

                            dr.Close();
                        }


                        else if (fddsimsobj.sims_fee_collection_mode.Equals("T"))
                        {

                            SqlDataReader dr3 = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                                new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "T"),
                        new SqlParameter("@fee_term", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@enrollno", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@fee_number", fddsimsobj.PayFeeCode.ToString()),
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        //   new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@doc_discount_amount", fddsimsobj.PayDisAmount),
                            new SqlParameter("@dd_fee_amount_final", fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@Payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@cheque_no", fddsimsobj.chequeDDNo),
                        new SqlParameter("@cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@bank_code", fddsimsobj.BankName),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@term_amount", fddsimsobj.PayAmount)
                         });
                            res = dr3.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;

                            if (dr3.RecordsAffected > 0)
                            {
                                a1 = a1 + 1;
                                inserted = true;
                                dr3.Close();
                           SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                          new List<SqlParameter>()
                              {
                                new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                                new SqlParameter("@dd_fee_number", fddsimsobj.PayFeeCode.ToString()),
                                new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                                new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                                new SqlParameter("@dd_fee_grade_code",fddsimsobj.std_fee_grade_code),
                                new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                                new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                                new SqlParameter("@dd_fee_amount_final", fddsimsobj.PayAmount)

                        });
                                if (dr1.RecordsAffected > 0)
                                {
                                    inserted = true;

                                    if (inserted == true)
                                    {
                                        doc_no = fddsimsobjlst[0].doc_no;
                                    }
                                }
                                dr1.Close();

                            }
                        }

                    }

                    if (inserted == true)
                    {
                        a1 = a1 + 1;
                        try {
                             SqlDataReader dr4 = db.ExecuteStoreProcedure("sims.[Fee_refund_proc]",
                                    new List<SqlParameter>()
                             {
                        new SqlParameter("@opr", "TT"),
                         new SqlParameter("@doc_no", fddsimsobjlst[0].doc_no),
                        new SqlParameter("@dd_fee_amount",fddsimsobjlst[0].std_fee_amount),
                        new SqlParameter("@enroll_number",fddsimsobjlst[0].FeeEnrollNo),
                        new SqlParameter("@dd_line_no",a1),
                        new SqlParameter("@dd_fee_cur_code", fddsimsobjlst[0].std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobjlst[0].std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobjlst[0].std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobjlst[0].std_fee_section_code),
                            });

                                dr4.Close();
                               
                            
                           
                        }
                        catch(Exception ee)
                        {

                        }
                        
                        
                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, doc_no);
        }




        #endregion
    }
}