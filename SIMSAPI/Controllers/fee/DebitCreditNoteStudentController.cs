﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using System;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/DebitCreditNote")]
    public class DebitCreditNoteStudentController : ApiController
    {
        [Route("GetAcademic_year")]
        public HttpResponseMessage GetAcademic_year(string curCode)
        {
            List<Sims179> list = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                             new SqlParameter("@sims_cur_code",curCode),
                         });

                    while (dr.Read())
                    {
                        Sims179 y = new Sims179();
                        y.sims_academic_year = dr["sims_academic_year"].ToString();
                        y.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                        list.Add(y);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetdebitcreditAllStudentFee")]
        public HttpResponseMessage GetdebitcreditAllStudentFee(string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {

            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_Debit_Cretid_Note_Student_Proc]",
               new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@sims_grade_code", gc=="null"?null:gc),
                new SqlParameter("@sims_section_code", sc=="null"?null:sc),
                new SqlParameter("@sims_academic_year", ay=="null"?null:ay),
                new SqlParameter("@sims_cur_code", cc=="null"?null:cc),
                new SqlParameter("@search_text", search_stud=="null"?null:search_stud),
                new SqlParameter("@search_flag", search_flag=="null"?null:search_flag)
                        });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                        simsobj.std_fee_student_name = dr["Name"].ToString();
                        simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                        simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                        simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                        simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                        simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total_Fee"].ToString()), 2).ToString();
                        simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["Total_Paid"].ToString()), 2).ToString();

                        if (dr["sims_fee_status"].ToString().Equals("A"))
                            simsobj.std_fee_status = true;
                        else
                            simsobj.std_fee_status = false;
                        simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                        simsobj.parent_email_id = dr["parent_email_id"].ToString();
                        simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                        simsobj.student_status = dr["studen_status"].ToString().Equals("A") ? true : false;
                        simsobj.fee_posting_status = dr["fee_posting_status"].ToString();
                        if (simsobj.student_status != true)
                        {
                            simsobj.color = "#F6081A";
                        }
                        else
                        {
                            simsobj.color = "#000";
                        }

                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getAllFeeDetailsStudent")]
        public HttpResponseMessage getAllFeeDetailsStudent(string enroll, string cur_code, string aca_year, string Fee_code)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
             new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "F"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_enroll_number", enroll),
                new SqlParameter("@sims_academic_year",aca_year),
                new SqlParameter("@sims_fee_code",Fee_code)
                         });

                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_id = dr["id"].ToString();
                        simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                        simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                        simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                        simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                        simsobj.dd_fee_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_orignal_exp_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                        simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount_temp = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_concession_amount_used = "0";

                        if (dr["installment_mode"].ToString().ToLower() == "y")
                            simsobj.Installment_mode = true;
                        else
                            simsobj.Installment_mode = false;

                        simsobj.Installment_mode_chk = false;
                        simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                        simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();
                        mod_list.Add(simsobj);
                    }

                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDDebitCreditNoteFee_Document")]
        public HttpResponseMessage CUDDebitCreditNoteFee_Document(sims043 fdsimsobj)
        {

            string res = "false";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "II"),
                new SqlParameter("@doc_no", null),
                new SqlParameter("@doc_date",db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@dt_code", "9"),
                new SqlParameter("@doc_total_amount", fdsimsobj.doc_total_amount),
                new SqlParameter("@doc_status", "1"),
                new SqlParameter("@enroll_number", fdsimsobj.doc_enroll_no),
                new SqlParameter("@doc_narration", fdsimsobj.doc_narration=="null"?null:fdsimsobj.doc_narration),
                new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name),
                new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year),
                new SqlParameter("@bill_date", db.DBYYYYMMDDformat(fdsimsobj.bill_date)),
                new SqlParameter("@doc_code", fdsimsobj.doc_code),
                new SqlParameter("@doc_type", fdsimsobj.doc_type),
                new SqlParameter("@adjust_fee_code", fdsimsobj.adjust_fee_code),

               });
                    if (dr.RecordsAffected > 0)

                        while (dr.Read())
                        {
                            res = dr["doc_no"].ToString();
                        }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CUDDebitCreditNoteFee_Document_Details")]
        public HttpResponseMessage CUDDebitCreditNoteFee_Document_Details(List<sims043> fddsimsobjlst)
        {
            bool inserted = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    for (int i = 0; i < fddsimsobjlst.Count; i++)
                    {
                        sims043 fddsimsobj = fddsimsobjlst[i];

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                          {
                        new SqlParameter("@opr", "ID"),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        new SqlParameter("@dd_line_no", (i + 1).ToString()),
                        new SqlParameter("@dd_fee_number", fddsimsobj.dd_fee_number),
                        new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@dd_fee_amount_1", fddsimsobj.FeeExpected),
                        new SqlParameter("@doc_narration", fddsimsobj.doc_narration=="null"?null:fddsimsobj.doc_narration),
                        new SqlParameter("@doc_academic_year", fddsimsobj.std_fee_academic_year),
                        
                          });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();

                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetSimsFeeType")]
        public HttpResponseMessage GetSimsFeeType(string cc, string ay, string gd, string sc, string sims_enroll_number)
        {
            List<Sims022> list = new List<Sims022>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                            new SqlParameter("@sims_academic_year",ay),
                            new SqlParameter("@sims_grade_code", gd),
                             new SqlParameter("@sims_section_code", sc),
                             new SqlParameter("@sims_cur_code", cc),
                             new SqlParameter("@sims_enroll_number", sims_enroll_number),
                         });

                    while (dr.Read())
                    {
                        Sims022 simsobj = new Sims022();
                        simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                        simsobj.sims_fee_code_desc = dr["sims_fee_code_description"].ToString();
                        simsobj.sims_fee_category_code = dr["sims_fee_category_code"].ToString();
                        simsobj.sims_fee_code_type = dr["sims_fee_code_type"].ToString();
                        simsobj.sims_fee_frequency = dr["sims_fee_frequency"].ToString();
                        simsobj.sims_fee_amount = dr["sims_fee_amount"].ToString();
                        simsobj.period_code = dr["period_code"].ToString();
                        simsobj.period_desc = dr["period_desc"].ToString();
                        list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("getAllFeeDetailsStudentforApproval")]
        public HttpResponseMessage getAllFeeDetailsStudentforApproval(string cur_code, string aca_year, string grade, string section,string search)
        {
            List<debit_creadit_note> mod_list = new List<debit_creadit_note>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
             new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "SA"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_academic_year",aca_year),
                new SqlParameter("@sims_section_code",section),
                new SqlParameter("@search_text",search)

                         });

                    while (dr.Read())
                    {
                        debit_creadit_note simsobj = new debit_creadit_note();
                        simsobj.fee_list = new List<sims043>();

                        sims043 simsobj1 = new sims043();
                        string str = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_cur_code = dr["sims_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["doc_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_section_code"].ToString();
                        simsobj.std_fee_student_name = dr["StudFullName"].ToString();
                        simsobj.doc_total_amount = decimal.Round(decimal.Parse(dr["doc_total_amount"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                        simsobj.std_fee_class_name = dr["Class"].ToString();
                        simsobj.doc_no = dr["doc_no"].ToString();
                        simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj1.std_fee_number = dr["dd_fee_number"].ToString();
                        simsobj1.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj1.std_fee_child_period_No = dr["dd_fee_period_code"].ToString();
                        simsobj1.dd_fee_amount = decimal.Round(decimal.Parse(dr["dd_fee_amount"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj1.doc_date = dr["doc_date"].ToString();
                        simsobj1.doc_no = dr["doc_no"].ToString();
                        simsobj.doc_narration = dr["doc_narration"].ToString();
                        var v = (from p in mod_list where p.std_fee_enroll_number == str select p);
                        if (v.Count() == 0)
                        {
                            simsobj.fee_list.Add(simsobj1);
                            mod_list.Add(simsobj);

                        }
                        else
                        {
                            v.ElementAt(0).fee_list.Add(simsobj1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CUDDebitCreditNoteFee_Approval_Details")]
        public HttpResponseMessage CUDDebitCreditNoteFee_Approval_Details(List<debit_creadit_note> fddsimsobjlst)
        {
            bool inserted = false;
            string str = "";
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    for (int i = 0; i < fddsimsobjlst.Count; i++)
                    {
                        debit_creadit_note fdsimsobj = fddsimsobjlst[i];

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                          {
                        new SqlParameter("@opr", "AI"),
                new SqlParameter("@doc_no", null),
                new SqlParameter("@doc_date",db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@dt_code", "9"),
                new SqlParameter("@doc_total_amount", fdsimsobj.doc_total_amount),
                new SqlParameter("@doc_status", "1"),
                new SqlParameter("@enroll_number", fdsimsobj.std_fee_enroll_number),
                new SqlParameter("@doc_narration", fdsimsobj.doc_narration=="null"?null:fdsimsobj.doc_narration),
                new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name),
                new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year),
                new SqlParameter("@sims_grade_code", fdsimsobj.std_fee_grade_code),
                new SqlParameter("@sims_section_code", fdsimsobj.std_fee_section_code),
                new SqlParameter("@dd_fee_code", fdsimsobj.std_fee_code),
                new SqlParameter("@sims_cur_code", fdsimsobj.std_fee_cur_code),
                new SqlParameter("@doc_no1",fdsimsobj.doc_no)

                          });
                        if (dr.RecordsAffected>0)
                        {
                            while(dr.Read())
                            {
                                str = dr["doc_no"].ToString();
                            }
                            inserted = true;
                            dr.Close();
                            for (int j = 0; j < fdsimsobj.fee_list.Count; j++)
                            {
                                int dr1 = db.ExecuteStoreProcedureforInsert("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                            new List<SqlParameter>()
                              {
                        new SqlParameter("@opr", "AD"),
                        new SqlParameter("@doc_no", str),
                        new SqlParameter("@dd_line_no", (j + 1).ToString()),
                        new SqlParameter("@dd_fee_number", fdsimsobj.fee_list[j].std_fee_number),
                        new SqlParameter("@dd_fee_period_code", fdsimsobj.fee_list[j].std_fee_child_period_No),
                        new SqlParameter("@dd_fee_amount", fdsimsobj.fee_list[j].dd_fee_amount),
                        //new SqlParameter("@doc_narration", fdsimsobj.fee_list[i].doc_narration=="null"?null:fdsimsobj.fee_list[i].doc_narration),
                        new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year),
                        new SqlParameter("@enroll_number", fdsimsobj.std_fee_enroll_number),
                        new SqlParameter("@doc_no1",fdsimsobj.doc_no),
                        new SqlParameter("@sims_grade_code", fdsimsobj.std_fee_grade_code),
                        new SqlParameter("@sims_section_code", fdsimsobj.std_fee_section_code),
                        new SqlParameter("@dd_fee_code", fdsimsobj.fee_list[j].std_fee_code),
                        new SqlParameter("@sims_cur_code", fdsimsobj.std_fee_cur_code),
                        new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name)


                              });
                                if (dr1 > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                                
                            }
                            
                        }

                    }
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getischeck")]
        public HttpResponseMessage getischeck(string user)
        {
            bool str = true;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@user",user),
                         });

                    while (dr.Read())
                    {
                        str = dr["user1"].ToString().Equals("Y") ? false : true;
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, str);

        }

        [Route("getparameter")]
        public HttpResponseMessage getparameter()
        {
            List<Sims022> list = new List<Sims022>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Debit_Cretid_Note_Student_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "K"),
                         });

                    while (dr.Read())
                    {
                        Sims022 simsobj = new Sims022();
                        simsobj.gldc_doc_code = dr["gldc_doc_code"].ToString();
                        simsobj.gldc_doc_name = dr["gldc_doc_name"].ToString();
                        simsobj.doc_type = dr["doc_type"].ToString();
                        simsobj.fee_code = dr["fee_code"].ToString();
                        list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

    }
}