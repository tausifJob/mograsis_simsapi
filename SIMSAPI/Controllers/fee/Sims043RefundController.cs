﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/studentfeerefund")]
    public class Sims043RefundController : ApiController
    {

        #region refund-fees



        [Route("GetReceipt")]
        public HttpResponseMessage GetReceipt()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            string reportName = "Sims.SIMR51";
            lst.Add(new SqlParameter("@OPR", "FR"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_proc]", lst);
                    if (dr.Read())
                    {
                        reportName = dr[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, reportName);
        }



        [Route("GetSimsSearchOptions")]
        public HttpResponseMessage GetSimsSearchOptions()
        {
            List<sims043> list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFee_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "O"),
                         });

                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.search_by_code = dr["sims_appl_parameter"].ToString();
                        simsobj.search_by_name = dr["sims_appl_form_field_value1"].ToString();
                        list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetSimsPaymentMode")]
        public HttpResponseMessage GetSimsPaymentMode()
        {
            List<sims043> list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                         });

                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.paymentMode_code = dr["sims_appl_parameter"].ToString();
                        simsobj.paymentMode = dr["sims_appl_form_field_value1"].ToString();
                        list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }


        [Route("getConcessionDiscountType")]
        public HttpResponseMessage getConcessionDiscountType()
        {
            List<Sims017> concession_list = new List<Sims017>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_concession",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims017_appl_code),
                                new SqlParameter("@sims_concession_discount_type", CommonStaticClass.Sims017_discount_type),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims017 simsobj = new Sims017();
                            simsobj.sims_concession_discount_type = dr["sims_appl_form_field_value1"].ToString();
                            concession_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, concession_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, concession_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, concession_list);
        }


        [Route("GetSimsFeeType")]
        public HttpResponseMessage GetSimsFeeType()
        {
            List<Sims022> list = new List<Sims022>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section_fee",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                         });

                    while (dr.Read())
                    {
                        Sims022 simsobj = new Sims022();
                        simsobj.sims_fee_code = dr["sims_appl_parameter"].ToString();
                        simsobj.sims_fee_code_desc = dr["sims_appl_form_field_value1"].ToString();
                        list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        [Route("GetAgentList")]
        public HttpResponseMessage GetAgentList(string enroll_number, string a_year)
        {
            List<sims043> agtLst = new List<sims043>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims_student_fee]",
                        new List<SqlParameter>() 
                         { 
               
                new SqlParameter("@opr", "G"),
                new SqlParameter("@enrollno", enroll_number),
                new SqlParameter("@academic_year", a_year)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.slma_ldgrctl_year = dr["sims_paying_agent_academic_year"].ToString();
                            simsobj.slma_acno = dr["sims_paying_agent_slma_ldgr"].ToString();
                            simsobj.slma_ldgrctl_code = dr["sims_paying_ldgrctl_code"].ToString();
                            simsobj.coad_pty_full_name = dr["sims_paying_agent_description"].ToString();
                            agtLst.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, agtLst);

        }

        [Route("GetAgentListDetails")]
        public HttpResponseMessage GetAgentListDetails(string enroll_number, string a_year)
        {
            List<sims043> agtLst = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims_student_fee]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "H"),
                new SqlParameter("@enrollno", enroll_number),
                new SqlParameter("@academic_year", a_year)
 });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.slma_ldgrctl_year = dr["slma_ldgrctl_year"].ToString();
                        simsobj.slma_acno = dr["slma_acno"].ToString();
                        simsobj.slma_ldgrctl_code = dr["slma_ldgrctl_code"].ToString();
                        simsobj.coad_pty_full_name = dr["coad_pty_full_name"].ToString();
                        agtLst.Add(simsobj);
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, agtLst);

        }

        [Route("GetAllBankNames")]
        public HttpResponseMessage GetAllBankNames()
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 obj = new sims043();
                            obj.bank_code = dr["pb_bank_code"].ToString();
                            obj.bank_name = dr["pb_bank_name"].ToString();
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("FeeDiscountTypes")]
        public HttpResponseMessage FeeDiscountTypes(sims043 obj)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetFeeTypes",
             new List<SqlParameter>() 
                     {
                new SqlParameter("@opr", "O"),
                new SqlParameter("@feeType", "O"),
                new SqlParameter("@cur_code", obj.std_fee_cur_code),
                new SqlParameter("@academic_year", obj.std_fee_academic_year),
                new SqlParameter("@grade_code", obj.std_fee_grade_code.ToString()),
                new SqlParameter("@section_code", obj.std_fee_section_code.ToString()),
                new SqlParameter("@enrollNo", obj.FeeEnrollNo.ToString())

                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_discount_type = dr["sims_fee_code_description"].ToString();
                            simsobj.std_fee_discount_type_code = dr["sims_fee_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("AllGetConcessionFee")]
        public HttpResponseMessage AllGetConcessionFee(sims043 obj)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetFeeTypes",
             new List<SqlParameter>() 
                     {
                new SqlParameter("@opr", "A"),
                new SqlParameter("@feeType", "a"),
                new SqlParameter("@cur_code", "a"),
                new SqlParameter("@academic_year", obj.std_fee_academic_year),
                new SqlParameter("@grade_code", "a"),
                new SqlParameter("@section_code", "a"),
                new SqlParameter("@enrollNo", obj.FeeEnrollNo)
              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.fee_concession_number = dr["sims_concession_number"].ToString();
                            simsobj.fee_concession_description = dr["sims_concession_description"].ToString();
                            simsobj.sims_concession_discount_type = dr["sims_concession_discount_type"].ToString();
                            simsobj.sims_concession_discount_value = dr["sims_concession_discount_value"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAllConcessionFee")]
        public HttpResponseMessage GetAllConcessionFee(sims043 obj)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader d = db.ExecuteStoreProcedure("GetFeeTypes",
             new List<SqlParameter>() 
                     {
                new SqlParameter("@opr", "D"),
                new SqlParameter("@feeType", "a"),
                new SqlParameter("@cur_code", "a"),
                new SqlParameter("@academic_year", obj.std_fee_academic_year),
                new SqlParameter("@grade_code", "a"),
                new SqlParameter("@section_code", "a"),
                new SqlParameter("@enrollNo", obj.FeeEnrollNo)
                     });
                    if (d.HasRows)
                    {
                        while (d.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.fee_concession_number = d["sims_concession_number"].ToString();
                            simsobj.fee_concession_description = d["sims_concession_description"].ToString();
                            simsobj.fee_concession_type = d["sims_concession_type"].ToString();
                            simsobj.fee_concession_discount_type = d["sims_concession_discount_type"].ToString();
                            simsobj.fee_concession_discount_value = d["sims_concession_discount_value"].ToString();
                            simsobj.fee_concession_fee_code = d["sims_concession_fee_code"].ToString();
                            simsobj.fee_concession_academic_year = d["sims_fee_academic_year"].ToString();
                            simsobj.fee_concession_enroll_number = d["sims_enroll_number"].ToString();
                            simsobj.fee_concession_fee_code_description = d["sims_fee_code_description"].ToString();
                            simsobj.fee_concession_dis_amt_cal = "0";
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDocRef_code")]
        public HttpResponseMessage GetDocRef_code()
        {
            string ref_code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_document",
             new List<SqlParameter>() 
                     {
                
                new SqlParameter("@opr", "R")
              });
                    if (dr.Read())
                    {
                        ref_code = dr["Reference"].ToString();
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, ref_code);

        }

        [Route("GetStudentRefundFeeNumberDetails")]
        public HttpResponseMessage GetStudentRefundFeeNumberDetails(string enroll, string cur_code, string aca_year)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "S"),
                new SqlParameter("@cur_code", cur_code),
                new SqlParameter("@sims_enroll_number", enroll),
                new SqlParameter("@academic_year",aca_year)
                 });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_id = dr["id"].ToString();
                        simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                        simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                        simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                        simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                        simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_orignal_exp_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                        simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount_temp = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_concession_amount_used = "0";

                        if (dr["installment_mode"].ToString().ToLower() == "y")
                            simsobj.Installment_mode = true;
                        else
                            simsobj.Installment_mode = false;

                        simsobj.Installment_mode_chk = false;
                        simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                        simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();


                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("RefundInsertToFee_Document")]
        public HttpResponseMessage RefundInsertToFee_Document(sims043 fdsimsobj)
        {

            string res = "false";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "D"),
                new SqlParameter("@doc_no", null),
                    new SqlParameter("@doc_date",db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@dt_code", "1"),
                new SqlParameter("@doc_total_amount", fdsimsobj.doc_total_amount),
                new SqlParameter("@doc_status", "1"),
                new SqlParameter("@enroll_number", fdsimsobj.doc_enroll_no),
                new SqlParameter("@doc_other_charge_desc", fdsimsobj.doc_other_charge_desc=="null"?null:fdsimsobj.doc_other_charge_desc),
                new SqlParameter("@doc_other_charge_amount", fdsimsobj.doc_other_charge_amount=="null"?null:fdsimsobj.doc_other_charge_amount),
                new SqlParameter("@doc_discount_pct", fdsimsobj.doc_discount_pct=="null"?null:fdsimsobj.doc_discount_pct),
                new SqlParameter("@doc_discount_amount", fdsimsobj.doc_discount_amount=="null"?null:fdsimsobj.doc_discount_amount),
                new SqlParameter("@doc_ref_code", null),
                new SqlParameter("@doc_narration", fdsimsobj.doc_narration=="null"?null:fdsimsobj.doc_narration),
                    new SqlParameter("@creation_date", db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name),//std_fee_student_name used as Created by User
                new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year)//new field added
               });
                    if (dr.RecordsAffected > 0)

                        while (dr.Read())
                        {
                            res = dr["doc_no"].ToString();
                        }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("RefundInsertToFee_Document_Details")]
        public HttpResponseMessage RefundInsertToFee_Document_Details(List<sims043> fddsimsobjlst)
        {
            string res = string.Empty;
            bool inserted = false;
            for (int i = 0; i < fddsimsobjlst.Count; i++)
            {
                sims043 fddsimsobj = fddsimsobjlst[i];
                try
                {
                    if (fddsimsobj.sims_fee_collection_mode.Equals("M"))
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
                       new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "DD"),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        new SqlParameter("@dd_line_no", (i + 1).ToString()),
                        //new SqlParameter("@dd_line_no", fddsimsobj.std_fee_id.ToString()),
                        new SqlParameter("@dd_fee_number", fddsimsobj.dd_fee_number),
                        new SqlParameter("@dd_fee_code", fddsimsobj.PayFeeCode),
                        new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@dd_fee_amount_discounted", fddsimsobj.PayDisAmount),
                        new SqlParameter("@dd_fee_amount_final",fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@dd_fee_payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@dd_fee_cheque_number", fddsimsobj.chequeDDNo),
                        new SqlParameter("@dd_fee_cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)=="N/A"?null:db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@dd_fee_cheque_bank_code", fddsimsobj.BankName),
                        new SqlParameter("@dd_fee_tt_ref_code", fddsimsobj.TransactionID),
                        //Parameter to Get Fee_Number
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@sims_fee_category", "01"),
                        //Fee Agent 
                        new SqlParameter("@slma_ldgrctl_code", fddsimsobj.slma_ldgrctl_code=="null"?null:fddsimsobj.slma_ldgrctl_code),
                        new SqlParameter("@slma_acno", fddsimsobj.slma_acno=="null"?null:fddsimsobj.slma_acno ),
                        new SqlParameter("@slma_ldgrctl_year", fddsimsobj.slma_ldgrctl_year=="null"?null: fddsimsobj.slma_ldgrctl_year),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@doc_paying_agent_transaction_no",fddsimsobj.doc_paying_agent_transaction_no=="null"?null:fddsimsobj.doc_paying_agent_transaction_no),
					    new SqlParameter("@doc_paying_agent_transaction_lineno",fddsimsobj.doc_paying_agent_transaction_lineno=="null"?null:fddsimsobj.doc_paying_agent_transaction_lineno),
                        //for Credit Card
                        new SqlParameter("@dd_fee_credit_card_code",fddsimsobj.dd_fee_credit_card_code=="null"?null:fddsimsobj.dd_fee_credit_card_code),
					    new SqlParameter("@dd_fee_credit_card_auth_code",fddsimsobj.dd_fee_credit_card_auth_code=="null"?null:fddsimsobj.dd_fee_credit_card_auth_code)
                         });
                            res = dr.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;
                            if (dr.RecordsAffected > 0)
                            {
                                dr.Close();
                                try
                                {
                                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
                                   new List<SqlParameter>() 
                               { 
                                new SqlParameter("@opr", "SU"),
                                new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                                new SqlParameter("@dd_fee_number", fddsimsobj.PayFeeCode.ToString()),
                                new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                                new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                                new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                                new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                                new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                                new SqlParameter("@dd_fee_amount_final", fddsimsobj.PayAmount)
                                 
                         });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                    dr1.Close();
                                }
                                catch (Exception x)
                                {
                                    inserted = false;
                                }
                            }

                            db.Dispose();
                        }

                    }
                    else if (fddsimsobj.sims_fee_collection_mode.Equals("T"))
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr3 = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
                                new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "T"),
                        new SqlParameter("@fee_term", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@enrollno", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@fee_number", fddsimsobj.PayFeeCode.ToString()),
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        //   new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@doc_discount_amount", fddsimsobj.PayDisAmount),
                            new SqlParameter("@dd_fee_amount_final", fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@Payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@cheque_no", fddsimsobj.chequeDDNo),
                        new SqlParameter("@cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@bank_code", fddsimsobj.BankName),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@term_amount", fddsimsobj.PayAmount)
                         });
                            res = dr3.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;
                            dr3.Close();
                        }
                    }
                }
                catch (Exception x)
                {
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllStudentFee")]
        public HttpResponseMessage GetAllStudentFee(string minval, string maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {

            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GetStudentFee_proc",
               new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@min_val", null),
                new SqlParameter("@max_val", null),
                new SqlParameter("@grade_code", gc=="null"?null:gc),
                new SqlParameter("@section_code", sc=="null"?null:sc),
                new SqlParameter("@academic_year", ay=="null"?null:ay),
                new SqlParameter("@cur_code", cc=="null"?null:cc),
                new SqlParameter("@search_text", search_stud=="null"?null:search_stud),
                new SqlParameter("@search_flag", search_flag=="null"?null:search_flag)
                        });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                        simsobj.std_fee_student_name = dr["Name"].ToString();
                        simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                        simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                        simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                        simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                        simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                        simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                        simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                        if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                            simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                        else
                            simsobj.std_fee_last_payment_date = string.Empty;

                        if (dr["sims_fee_status"].ToString().Equals("A"))
                            simsobj.std_fee_status = true;
                        else
                            simsobj.std_fee_status = false;
                        simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                        simsobj.parent_email_id = dr["parent_email_id"].ToString();
                        simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                        simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                        simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                        simsobj.student_attendance_per = dr["StudentAttendance"].ToString();

                        simsobj.student_status = dr["studen_status"].ToString().Equals("A") ? true : false;

                        if (simsobj.student_status != true)
                        {
                            simsobj.color = "#F6081A";
                        }
                        else
                        {
                            simsobj.color = "#000";
                        }

                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        #endregion

        #region (student fee refund new table structure )

        [Route("GetStudentRefundFeeNumberDetailsNew")]
        public HttpResponseMessage GetStudentRefundFeeNumberDetailsNew(string enroll, string cur_code, string aca_year)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_New_proc]",
                        new List<SqlParameter>()
                         {

                new SqlParameter("@opr", "S"),
                new SqlParameter("@cur_code", cur_code),
                new SqlParameter("@sims_enroll_number", enroll),
                new SqlParameter("@academic_year",aca_year)
                 });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_id = dr["id"].ToString();
                        simsobj.feeTypeGroup = dr["feeGroup"].ToString();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        //simsobj.std_fee_number = dr["sims_fee_number"].ToString();
                        simsobj.std_fee_type = dr["sims_fee_code_description"].ToString();
                        simsobj.std_fee_code = dr["sims_fee_code"].ToString();
                        simsobj.std_fee_child_period = dr["sims_fee_period"].ToString();
                        simsobj.std_fee_child_period_No = dr["periodNo"].ToString();
                        simsobj.std_fee_child_exp_amount = decimal.Round(decimal.Parse(dr["expected_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_orignal_exp_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_concession_amount = decimal.Round(decimal.Parse(dr["conc_fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_child_paid_amount = decimal.Parse(dr["paid_Fee"].ToString()).ToString();
                        simsobj.std_fee_child_remaining_amount = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount = decimal.Round(simsobj.std_fee_child_remaining_amount, 2, MidpointRounding.ToEven);
                        simsobj.std_fee_child_paying_amount_temp = decimal.Round(decimal.Parse(dr["bal_Fee"].ToString()), 2, MidpointRounding.ToEven).ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_concession_amount_used = "0";

                        if (dr["installment_mode"].ToString().ToLower() == "y")
                            simsobj.Installment_mode = true;
                        else
                            simsobj.Installment_mode = false;

                        simsobj.Installment_mode_chk = false;
                        simsobj.Installment_amnt = dr["installment_min_amount"].ToString();
                        simsobj.sims_fee_collection_mode = dr["Collection_mode"].ToString();


                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("RefundInsertToFee_DocumentNew")]
        public HttpResponseMessage RefundInsertToFee_DocumentNew(sims043 fdsimsobj)
        {

            string res = "false";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_New_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "D"),
                new SqlParameter("@doc_no", null),
                    new SqlParameter("@doc_date",db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@dt_code", "1"),
                new SqlParameter("@doc_total_amount", fdsimsobj.doc_total_amount),
                new SqlParameter("@doc_status", "1"),
                new SqlParameter("@enroll_number", fdsimsobj.doc_enroll_no),
                new SqlParameter("@doc_other_charge_desc", fdsimsobj.doc_other_charge_desc=="null"?null:fdsimsobj.doc_other_charge_desc),
                new SqlParameter("@doc_other_charge_amount", fdsimsobj.doc_other_charge_amount=="null"?null:fdsimsobj.doc_other_charge_amount),
                new SqlParameter("@doc_discount_pct", fdsimsobj.doc_discount_pct=="null"?null:fdsimsobj.doc_discount_pct),
                new SqlParameter("@doc_discount_amount", fdsimsobj.doc_discount_amount=="null"?null:fdsimsobj.doc_discount_amount),
                new SqlParameter("@doc_ref_code", null),
                new SqlParameter("@doc_narration", fdsimsobj.doc_narration=="null"?null:fdsimsobj.doc_narration),
                    new SqlParameter("@creation_date", db.DBYYYYMMDDformat(fdsimsobj.doc_date)),
                new SqlParameter("@creation_user", fdsimsobj.std_fee_student_name),//std_fee_student_name used as Created by User
                new SqlParameter("@doc_academic_year", fdsimsobj.std_fee_academic_year)//new field added
               });
                    if (dr.RecordsAffected > 0)

                        while (dr.Read())
                        {
                            res = dr["doc_no"].ToString();
                        }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("RefundInsertToFee_Document_DetailsNew")]
        public HttpResponseMessage RefundInsertToFee_Document_DetailsNew(List<sims043> fddsimsobjlst)
        {
            string res = string.Empty;
            bool inserted = false;
            for (int i = 0; i < fddsimsobjlst.Count; i++)
            {
                sims043 fddsimsobj = fddsimsobjlst[i];
                try
                {
                    if (fddsimsobj.sims_fee_collection_mode.Equals("M"))
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_New_proc]",
                       new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", "DD"),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        new SqlParameter("@dd_line_no", (i + 1).ToString()),
                        //new SqlParameter("@dd_line_no", fddsimsobj.std_fee_id.ToString()),
                        new SqlParameter("@dd_fee_number", fddsimsobj.dd_fee_number),
                        new SqlParameter("@dd_fee_code", fddsimsobj.PayFeeCode),
                        new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@dd_fee_amount_discounted", fddsimsobj.PayDisAmount),
                        new SqlParameter("@dd_fee_amount_final",fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@dd_fee_payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@dd_fee_cheque_number", fddsimsobj.chequeDDNo),
                        new SqlParameter("@dd_fee_cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)=="N/A"?null:db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@dd_fee_cheque_bank_code", fddsimsobj.BankName),
                        new SqlParameter("@dd_fee_tt_ref_code", fddsimsobj.TransactionID),
                        //Parameter to Get Fee_Number
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@sims_fee_category", "01"),
                        //Fee Agent 
                        new SqlParameter("@slma_ldgrctl_code", fddsimsobj.slma_ldgrctl_code=="null"?null:fddsimsobj.slma_ldgrctl_code),
                        new SqlParameter("@slma_acno", fddsimsobj.slma_acno=="null"?null:fddsimsobj.slma_acno ),
                        new SqlParameter("@slma_ldgrctl_year", fddsimsobj.slma_ldgrctl_year=="null"?null: fddsimsobj.slma_ldgrctl_year),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@doc_paying_agent_transaction_no",fddsimsobj.doc_paying_agent_transaction_no=="null"?null:fddsimsobj.doc_paying_agent_transaction_no),
                        new SqlParameter("@doc_paying_agent_transaction_lineno",fddsimsobj.doc_paying_agent_transaction_lineno=="null"?null:fddsimsobj.doc_paying_agent_transaction_lineno),
                        //for Credit Card
                        new SqlParameter("@dd_fee_credit_card_code",fddsimsobj.dd_fee_credit_card_code=="null"?null:fddsimsobj.dd_fee_credit_card_code),
                        new SqlParameter("@dd_fee_credit_card_auth_code",fddsimsobj.dd_fee_credit_card_auth_code=="null"?null:fddsimsobj.dd_fee_credit_card_auth_code)
                         });
                            res = dr.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;
                            if (dr.RecordsAffected > 0)
                            {
                                dr.Close();
                                try
                                {
                                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_New_proc]",
                                   new List<SqlParameter>()
                               {
                                new SqlParameter("@opr", "SU"),
                                new SqlParameter("@enroll_number", fddsimsobj.FeeEnrollNo),
                                new SqlParameter("@dd_fee_number", fddsimsobj.PayFeeCode.ToString()),
                                new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                                new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                                new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                                new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                                new SqlParameter("@dd_fee_period_code", fddsimsobj.PayDisPeriodNo),
                                new SqlParameter("@dd_fee_amount_final", fddsimsobj.PayAmount)

                         });
                                    if (dr1.RecordsAffected > 0)
                                    {
                                        inserted = true;
                                    }
                                    dr1.Close();
                                }
                                catch (Exception x)
                                {
                                    inserted = false;
                                }
                            }

                            db.Dispose();
                        }

                    }
                    else if (fddsimsobj.sims_fee_collection_mode.Equals("T"))
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr3 = db.ExecuteStoreProcedure("[sims].[GetStudentFeeDataRefund_Temp_New_proc]",
                                new List<SqlParameter>()
                         {
                        new SqlParameter("@opr", "T"),
                        new SqlParameter("@fee_term", fddsimsobj.PayDisPeriodNo),
                        new SqlParameter("@enrollno", fddsimsobj.FeeEnrollNo),
                        new SqlParameter("@fee_number", fddsimsobj.PayFeeCode.ToString()),
                        new SqlParameter("@dd_fee_cur_code", fddsimsobj.std_fee_cur_code),
                        new SqlParameter("@dd_fee_academic_year", fddsimsobj.std_fee_academic_year),
                        new SqlParameter("@dd_fee_grade_code", fddsimsobj.std_fee_grade_code),
                        new SqlParameter("@dd_fee_section_code", fddsimsobj.std_fee_section_code),
                        new SqlParameter("@doc_no", fddsimsobj.doc_no),
                        //   new SqlParameter("@dd_fee_amount", fddsimsobj.PayAmount),
                        new SqlParameter("@doc_discount_amount", fddsimsobj.PayDisAmount),
                            new SqlParameter("@dd_fee_amount_final", fddsimsobj.IsMulti_Cheque_payment?fddsimsobj.PayAmount:(decimal.Parse(fddsimsobj.PayAmount) - decimal.Parse(fddsimsobj.PayDisAmount)) + ""),
                        new SqlParameter("@Payment_mode", fddsimsobj.paymentMode),
                        new SqlParameter("@cheque_no", fddsimsobj.chequeDDNo),
                        new SqlParameter("@cheque_date",db.DBYYYYMMDDformat(fddsimsobj.chequeDDDate)),
                        new SqlParameter("@bank_code", fddsimsobj.BankName),
                        new SqlParameter("@isOtherFee", fddsimsobj.isOtherFee==true?"Y":"N"),
                        new SqlParameter("@term_amount", fddsimsobj.PayAmount)
                         });
                            res = dr3.RecordsAffected > 0 ? fddsimsobj.doc_no : string.Empty;
                            dr3.Close();
                        }
                    }
                }
                catch (Exception x)
                {
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetAllStudentFeeNew")]
        public HttpResponseMessage GetAllStudentFeeNew(string minval, string maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {

            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GetStudentFee_New_proc]",
               new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "S"),
                new SqlParameter("@min_val", null),
                new SqlParameter("@max_val", null),
                new SqlParameter("@grade_code", gc=="null"?null:gc),
                new SqlParameter("@section_code", sc=="null"?null:sc),
                new SqlParameter("@academic_year", ay=="null"?null:ay),
                new SqlParameter("@cur_code", cc=="null"?null:cc),
                new SqlParameter("@search_text", search_stud=="null"?null:search_stud),
                new SqlParameter("@search_flag", search_flag=="null"?null:search_flag)
                        });
                    while (dr.Read())
                    {
                        sims043 simsobj = new sims043();
                        simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                        simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                        simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                        simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                        simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                        simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                        simsobj.std_fee_student_name = dr["Name"].ToString();
                        simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                        simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                        simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                        simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                        simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                        simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                        simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                        if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                            simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                        else
                            simsobj.std_fee_last_payment_date = string.Empty;

                        if (dr["sims_fee_status"].ToString().Equals("A"))
                            simsobj.std_fee_status = true;
                        else
                            simsobj.std_fee_status = false;
                        simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                        simsobj.parent_email_id = dr["parent_email_id"].ToString();
                        simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                        simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                        simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                        simsobj.student_attendance_per = dr["StudentAttendance"].ToString();

                        simsobj.student_status = dr["studen_status"].ToString().Equals("A") ? true : false;

                        if (simsobj.student_status != true)
                        {
                            simsobj.color = "#F6081A";
                        }
                        else
                        {
                            simsobj.color = "#000";
                        }

                        mod_list.Add(simsobj);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }



        #endregion


    }
}