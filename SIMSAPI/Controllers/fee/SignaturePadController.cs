﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/signature")]
    public class SignaturePadController : ApiController
    {


        [Route("InsertSignatureDetails")]
        public HttpResponseMessage InsertSignatureDetails(signaturepad data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_singnature_pad_details_proc]",
                    new List<SqlParameter>()
                     {
                        new SqlParameter("@opr", "I"),
                        new SqlParameter("@academic_year",data.academic_year),
			            new SqlParameter("@parent_id",data.parent_id),
			            new SqlParameter("@student_id",data.student_id),
			            new SqlParameter("@signature_details",data.signature_details),
                        new SqlParameter("@signature_img_path",data.signature_img_path),
			            new SqlParameter("@takenby_user_name",data.takenby_user_name),                                
                     });
                    if (ins > 0)
                    {
                        insert = true;

                    }
                    else
                    {
                        insert = false;

                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}



