﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.FEE;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using System.Collections;
using System.IO;
using System.Web;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/Fee/SFS")]
    [BasicAuthentication]
    public class SFCController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        #region SiblingConcession
        [Route("GetAllConcession")]
        public HttpResponseMessage GetAllConcession(string cur_code, string ac_year)
        {
            List<simsSiblingConcession> mod_list = new List<simsSiblingConcession>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_sibling_concession_transaction",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@CURCODE", cur_code),
                                new SqlParameter("@ACADEMIC", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsSiblingConcession objNew = new simsSiblingConcession();
                            objNew.sims_concession_number = dr["sims_concession_number"].ToString();
                            objNew.sims_concession_desc = dr["sims_concession_description"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("getStudentSiblingConcession")]
        public HttpResponseMessage GetAllStudentFee(string cc, string ay, string gc, string sc, string concession_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));

            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;

            List<simsSiblingConcession> mod_list = new List<simsSiblingConcession>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_sibling_concession_transaction",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                
                                new SqlParameter("@CURCODE", cc),
                                new SqlParameter("@ACADEMIC", ay),
                                new SqlParameter("@GRADE", gc),
                                new SqlParameter("@SECTION", sc),
                                new SqlParameter("@concession_no", concession_no),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsSiblingConcession simsobj = new simsSiblingConcession();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_section_code"].ToString();
                            simsobj.std_fee_Class = dr["Class"].ToString();
                            simsobj.std_fee_student_name = dr["Student_Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.std_fee_sibling_count = dr["sibling_count"].ToString();
                            simsobj.std_fee_Sibling_Names = dr["Sibling_Names"].ToString();
                            simsobj.std_fee_Concession_status = dr["Concession_status"].ToString();
                            simsobj.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            if (simsobj.std_fee_Concession_status.Equals("P"))
                                simsobj.std_fee_status = false;
                            else if (simsobj.std_fee_Concession_status.Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_selected_status = false;

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("StudentSiblingConcession")]
        public HttpResponseMessage StudentSiblingConcession(string concession_no, string from_date, string to_date, string created_user, List<simsSiblingConcession> mod_list)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            bool res = false;
            try
            {
                foreach (simsSiblingConcession obj in mod_list)
                {
                    if (obj.std_fee_selected_status)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_sibling_concession_transaction",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@CURCODE", obj.std_fee_cur_code),
                                new SqlParameter("@ACADEMIC", obj.std_fee_academic_year),
                                new SqlParameter("@ENROLL", obj.std_fee_enroll_number),
                                new SqlParameter("@concession_no", concession_no),
                                new SqlParameter("@concession_from_date", db.DBYYYYMMDDformat(from_date)),
                                new SqlParameter("@concession_to_date", db.DBYYYYMMDDformat(to_date)),
                                new SqlParameter("@created_by", created_user),
                         });
                            res = dr.RecordsAffected > 0 ? true : false;

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }

        [Route("StudentSiblingConcessionremv")]
        public HttpResponseMessage StudentSiblingConcessionremv(string concession_no, simsSiblingConcession obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            bool res = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_sibling_concession_transaction",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'D'),
                                new SqlParameter("@CURCODE", obj.std_fee_cur_code),
                                new SqlParameter("@ACADEMIC", obj.std_fee_academic_year),
                                new SqlParameter("@ENROLL", obj.std_fee_enroll_number),
                                new SqlParameter("@concession_no", concession_no)
                         });
                    res = dr.RecordsAffected > 0 ? true : false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }


        #endregion


        #region admissionQuota

        [Route("getQuotaCur")]
        public HttpResponseMessage getQuotaCur()
        {

            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SC")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_q_cur_code = dr["sims_cur_code"].ToString();
                            simobj.sims_q_cur_name = dr["sims_cur_short_name_en"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getQuotaAcademic")]
        public HttpResponseMessage getQuotaAcademic(string cur_code)
        {

            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SA"),
                            new SqlParameter("@CURCODE", cur_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_q_academic_year = dr["sims_academic_year"].ToString();
                            simobj.sims_q_academic_desc = dr["sims_academic_year_description"].ToString();
                            simobj.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getQuotaForUser")]
        public HttpResponseMessage getQuotaForUser(string cur_code, string academic_year, string user_name)
        {

            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SQ"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@USERNAME", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_q_cur_code = dr["sims_q_cur_code"].ToString();
                            simobj.sims_q_academic_year = dr["sims_q_academic_year"].ToString();
                            simobj.sims_q_sr_no = dr["sims_q_sr_no"].ToString();
                            simobj.sims_quota_id = dr["sims_quota_id"].ToString();
                            simobj.sims_quota_desc = dr["sims_quota_desc"].ToString();
                            simobj.sims_q_user_name = dr["sims_q_user_name"].ToString();
                            simobj.total_quota_strength = dr["total_quota_strength"].ToString();
                            simobj.total_recommended_seats = dr["total_recommended_seats"].ToString();
                            simobj.balance_recommended_seats = (int.Parse(simobj.total_quota_strength) - int.Parse(simobj.total_recommended_seats)).ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getQuotaGrades")]
        public HttpResponseMessage getQuotaGrades(string cur_code, string academic_year, string user_name, string quotaid)
        {
            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SG"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@USERNAME", user_name),
                            new SqlParameter("@QUOTAID", quotaid)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_q_grade_code = dr["sims_q_grade_code"].ToString();
                            simobj.sims_q_grade_desc = dr["sims_grade_name_en"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getQuotaAdmissions")]
        public HttpResponseMessage getQuotaAdmissions(string cur_code, string academic_year, string user_name, string quotaid, string grade_code, string search)
        {
            grade_code = !string.IsNullOrEmpty(grade_code) ? grade_code : null;
            search = !string.IsNullOrEmpty(search) ? search : null;
            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AD"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@USERNAME", user_name),
                            new SqlParameter("@QUOTAID", quotaid),
                            new SqlParameter("@GRADE", grade_code),
                            new SqlParameter("@SEARCH", search),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            simobj.sims_q_cur_code = dr["sims_admission_cur_code"].ToString();
                            simobj.sims_q_academic_year = dr["sims_admission_academic_year"].ToString();
                            simobj.sims_q_grade_code = dr["sims_admission_grade_code"].ToString();
                            simobj.sims_q_grade_desc = dr["sims_grade_name_en"].ToString();
                            simobj.sims_student_name = dr["Student_Name"].ToString();
                            simobj.sims_admission_enroll_number = dr["sims_admission_sibling_enroll_number"].ToString();
                            simobj.sims_admission_parent_Name = dr["Parent_Name"].ToString();
                            simobj.sims_admission_employee_code = dr["sims_admission_employee_code"].ToString();
                            simobj.sims_q_remark = string.Empty;
                            simobj.sims_admission_recommend_status = false;
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("QuotaAdmissions")]
        public HttpResponseMessage QuotaAdmissions(string cur_code, string academic_year, string user_name, string quotaid, List<admisison_quota> admissions)
        {

            try
            {
                foreach (admisison_quota obj in admissions)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AR"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@admission_number", obj.sims_admission_number),
                            new SqlParameter("@GRADE", obj.sims_q_grade_code),
                            new SqlParameter("@USERNAME", user_name),
                            new SqlParameter("@QUOTAID", quotaid),
                            new SqlParameter("@remark", obj.sims_q_remark),


                         });

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, false);
        }

        [Route("UnRecommendAdmissions")]
        public HttpResponseMessage UnRecommendAdmissions(string cur_code, string academic_year, string user_name, string admissionsno)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "RR"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@admission_number", admissionsno),
                            new SqlParameter("@USERNAME", user_name),
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, false);
        }


        [Route("getRecommedAdmissions")]
        public HttpResponseMessage getRecommedAdmissions(string cur_code, string academic_year, string user_name)
        {
            List<admisison_quota> ItemMaster_list = new List<admisison_quota>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_recommendation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "VR"),
                            new SqlParameter("@CURCODE", cur_code),
                            new SqlParameter("@ACADEMICYEAR", academic_year),
                            new SqlParameter("@USERNAME", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admisison_quota simobj = new admisison_quota();
                            simobj.sims_admission_number = dr["sims_admission_number"].ToString();
                            simobj.sims_quota_desc = dr["sims_quota_desc"].ToString();
                            simobj.sims_student_name = dr["Student_Name"].ToString();
                            simobj.sims_admission_enroll_number = dr["sims_admission_sibling_enroll_number"].ToString();
                            simobj.sims_admission_parent_Name = dr["Parent_Name"].ToString();
                            simobj.sims_q_grade_desc = dr["sims_grade_name_en"].ToString();
                            simobj.sims_q_remark = dr["sims_q_remark"].ToString();
                            if (dr["recommend_status"].ToString().Equals("Y"))
                            {
                                simobj.sims_admission_done_status = true;
                                simobj.sims_admission_recommend_status = true;
                            }
                            else
                            {
                                simobj.sims_admission_done_status = false;
                                simobj.sims_admission_recommend_status = false;

                            }
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }


        #endregion


        #region Invoice

        [Route("GetAllCurName")]
        public HttpResponseMessage GetAllCurName()
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }



        [Route("GetAllTotalStudent")]
        public HttpResponseMessage GetAllTotalStudent(string acyear)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_preview_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'J'),
                                new SqlParameter("@academic_year", acyear),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();

                            simsobj.student_count = dr["student_count"].ToString();
                            
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }



        [Route("GetAllAcademicYears")]
        public HttpResponseMessage GetAllAcademicYears(string cur_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_academic_year_start_date =db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            objNew.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            objNew.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllGrades")]
        public HttpResponseMessage GetAllGrades(string cur_code, string ac_year)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAllSections")]
        public HttpResponseMessage GetAllSections(string cur_code, string ac_year, string g_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_section",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                                new SqlParameter("@sims_g_code", g_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getStudent_Fees")]
        public HttpResponseMessage GetAllStudentFee(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_mother_name = string.Format("{0}({1})", dr["mother_name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            simsobj.is_parent_employee = dr["is_parent_employee"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_fee_defined = dr["is_adv_fee_defined"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_section_defined = dr["is_adv_section_defined"].ToString().Equals("A") ? true : false;
                            simsobj.adv_fee_year = dr["adv_fee_year"].ToString();
                            simsobj.is_adv_grade = dr["is_adv_grade"].ToString();
                            simsobj.is_adv_section = dr["is_adv_section"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        /*Get Student Fee - Transport Fee */
        [Route("getStudentFeesTransport")]
        public HttpResponseMessage getStudentFeesTransport(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_transport",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            simsobj.is_parent_employee = dr["is_parent_employee"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_fee_defined = dr["is_adv_fee_defined"].ToString().Equals("A") ? true : false;
                            simsobj.is_adv_section_defined = dr["is_adv_section_defined"].ToString().Equals("A") ? true : false;
                            simsobj.adv_fee_year = dr["adv_fee_year"].ToString();
                            simsobj.is_adv_grade = dr["is_adv_grade"].ToString();
                            simsobj.is_adv_section = dr["is_adv_section"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("DefineAdvFee")]
        public HttpResponseMessage DefineAdvFee(string cur_code, string ay, string grade, string section, string enroll)
        {
            string result = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "AF"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@search_text", enroll),
                                new SqlParameter("@grade_code", grade),
                                new SqlParameter("@section_code", section)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = dr["adv_status"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        /*Transport Fees*/
        [Route("getStudent_Transport_Fees")]
        public HttpResponseMessage GetAllStudentTransportFee(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_transport",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Active";
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetPreviousReceipts")]
        public HttpResponseMessage GetPreviousReceipts(string ay, string enroll)
        {
            ArrayList coll = new ArrayList();
            List<sims043_previous> mod_list = new List<sims043_previous>();
            List<sims043_previous> dist_docs = new List<sims043_previous>();
            coll.Add(mod_list);
            coll.Add(dist_docs);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_preview_fee_receipt_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@enroll", enroll)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.Receipt_Type = dr["Receipt_Type"].ToString();
                            simsobj.doc_status_code = dr["doc_status_code"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.dd_fee_amount = dr["dd_fee_amount"].ToString();
                            simsobj.total_amount = dr["total_amount"].ToString();
                            //simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            //simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());

                            simsobj.creation_date = (dr["creation_date"].ToString());
                            simsobj.doc_date = (dr["doc_date"].ToString());
                            simsobj.creation_user = dr["creation_user"].ToString();
                            if (dr["doc_status_code"].Equals("3"))
                            {
                                simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                            }
                            else
                            {
                                simsobj.dd_fee_amount_discounted = dr["dd_fee_amount_discounted"].ToString();
                                simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            }
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.ChequeStatus_Desc = dr["ChequeStatus_Desc"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;


                            mod_list.Add(simsobj);
                        }
                        foreach (sims043_previous a in mod_list)
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = a.doc_no;
                            simsobj.Receipt_Type = a.Receipt_Type;
                            simsobj.doc_status_code = a.doc_status_code;
                            simsobj.doc_status = a.doc_status;
                            //simsobj.creation_date = db.UIDDMMYYYYformat(a.creation_date.ToString());
                            //simsobj.doc_date = db.UIDDMMYYYYformat(a.doc_date.ToString());

                            simsobj.creation_date = (a.creation_date.ToString());
                            simsobj.doc_date = (a.doc_date.ToString());
                            //  simsobj.total_amount = mod_list.Where(xx => xx.doc_no == a.doc_no).Sum(xx => decimal.Parse(xx.total_amount)).ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;
                            int cnt = dist_docs.Where(xx => xx.doc_no == simsobj.doc_no).Count();
                            if (cnt == 0)
                                dist_docs.Add(simsobj);
                        }
                        foreach (sims043_previous simsobj in dist_docs)
                        {
                            simsobj.total_amount = mod_list.Where(xx => xx.doc_no == simsobj.doc_no).Sum(xx => decimal.Parse(xx.dd_fee_amount_final)).ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, coll);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
        }


        [Route("GetPreviousReceipts_NEW")]
        public HttpResponseMessage GetPreviousReceipts_NEW(string ay, string enroll)
        {
            ArrayList coll = new ArrayList();
            List<sims043_previous> mod_list = new List<sims043_previous>();
          
            coll.Add(mod_list);
         
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_preview_fee_receipt_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@enroll", enroll)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_status_code = dr["doc_status_code"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.dd_fee_amount = dr["dd_fee_amount"].ToString();
                            simsobj.total_amount = dr["total_amount"].ToString();
                            simsobj.creation_user = dr["creation_user"].ToString();
                            simsobj.creation_date = (dr["creation_date"].ToString());
                            simsobj.chequeColor_Desc = dr["chequeColor_Desc"].ToString();

                            simsobj.doc_date = (dr["doc_date"].ToString());
                            if (dr["doc_status_code"].Equals("3"))
                            {
                                simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                            }
                            else
                            {
                                simsobj.dd_fee_amount_discounted = dr["dd_fee_amount_discounted"].ToString();
                                simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            }

                            try {
                                if (dr["dt_code"].Equals("4"))
                                {
                                    simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                    simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                                }
                            
                            }
                            catch (Exception ex) { }

                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.ChequeStatus_Desc = dr["ChequeStatus_Desc"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;


                            mod_list.Add(simsobj);
                        }
                        
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, coll);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
        }




        /*GetPreviousDetailsFor Transport*/
        [Route("GetTransportPreviousReceipts")]
        public HttpResponseMessage GetTransportPreviousReceipts(string ay, string enroll)
        {
            ArrayList coll = new ArrayList();
            List<sims043_previous> mod_list = new List<sims043_previous>();
            List<sims043_previous> dist_docs = new List<sims043_previous>();
            coll.Add(mod_list);
            coll.Add(dist_docs);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_transport_fee_TRA1",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "PD"),
                                new SqlParameter("@ACADEMIC", ay),
                                new SqlParameter("@ENROLL", enroll)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.Receipt_Type = dr["Receipt_Type"].ToString();
                            simsobj.doc_status_code = dr["doc_status_code"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            simsobj.dd_fee_amount = dr["dd_fee_amount"].ToString();
                            simsobj.total_amount = dr["total_amount"].ToString();
                            simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());

                            simsobj.creation_user = dr["creation_user"].ToString();
                            if (dr["doc_status_code"].Equals("3"))
                            {
                                simsobj.dd_fee_amount_discounted = (decimal.Parse(dr["dd_fee_amount_discounted"].ToString()) * -1).ToString();
                                simsobj.dd_fee_amount_final = (decimal.Parse(dr["dd_fee_amount_final"].ToString()) * -1).ToString();
                            }
                            else
                            {
                                simsobj.dd_fee_amount_discounted = dr["dd_fee_amount_discounted"].ToString();
                                simsobj.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            }
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.ChequeStatus_Desc = dr["ChequeStatus_Desc"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;


                            mod_list.Add(simsobj);
                        }
                        foreach (sims043_previous a in mod_list)
                        {
                            sims043_previous simsobj = new sims043_previous();
                            simsobj.doc_no = a.doc_no;
                            simsobj.Receipt_Type = a.Receipt_Type;
                            simsobj.doc_status_code = a.doc_status_code;
                            simsobj.doc_status = a.doc_status;
                            simsobj.creation_date = db.UIDDMMYYYYformat(a.creation_date.ToString());
                            simsobj.doc_date = db.UIDDMMYYYYformat(a.doc_date.ToString());
                            //  simsobj.total_amount = mod_list.Where(xx => xx.doc_no == a.doc_no).Sum(xx => decimal.Parse(xx.total_amount)).ToString();
                            simsobj.isChecked = false;
                            simsobj.isexpanded = false;

                            int cnt = dist_docs.Where(xx => xx.doc_no == simsobj.doc_no).Count();
                            if (cnt == 0)
                                dist_docs.Add(simsobj);
                        }
                        foreach (sims043_previous simsobj in dist_docs)
                        {
                            simsobj.total_amount = mod_list.Where(xx => xx.doc_no == simsobj.doc_no).Sum(xx => decimal.Parse(xx.dd_fee_amount_final)).ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, coll);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
        }


        [Route("getStudent_FeesPA")]
        public HttpResponseMessage getStudent_FeesPA(int minval, int maxval, string cc, string ay, string gc, string sc, string search_stud, string search_flag)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            minval = 0;
            maxval = 0;
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            search_flag = string.IsNullOrEmpty(search_flag) ? null : search_flag;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee_PA",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@min_val", minval),
                                new SqlParameter("@max_val", maxval),
                                new SqlParameter("@cur_code", cc),
                                new SqlParameter("@academic_year", ay),
                                new SqlParameter("@grade_code", gc),
                                new SqlParameter("@section_code", sc),
                                new SqlParameter("@search_text", search_stud),
                                new SqlParameter("@search_flag", search_flag)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_fee_section_code"].ToString();
                            simsobj.std_fee_Class = string.Format("{0}({1})", dr["sims_grade_name"].ToString(), dr["sims_section_name"].ToString());
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.std_fee_section_name = dr["sims_section_name"].ToString();
                            simsobj.std_fee_parent_id = dr["Parent_Id"].ToString();
                            simsobj.std_fee_parent_name = string.Format("{0}({1})", dr["Parent_Name"].ToString(), simsobj.std_fee_parent_id);
                            simsobj.std_fee_amount = decimal.Round(decimal.Parse(dr["Total Fee"].ToString()), 2).ToString();
                            simsobj.std_TotalPaid = decimal.Round(decimal.Parse(dr["PaidFees"].ToString()), 2).ToString();
                            simsobj.std_BalanceFee = decimal.Round(decimal.Parse(dr["BalanceFees"].ToString()), 2).ToString();
                            if (string.IsNullOrEmpty(dr["last_payment_date"].ToString()) == false)
                                simsobj.std_fee_last_payment_date = db.UIDDMMYYYYformat(dr["last_payment_date"].ToString());
                            else
                                simsobj.std_fee_last_payment_date = "NA";
                            if (string.IsNullOrEmpty(dr["student_attendance_last_date"].ToString()) == false)
                                simsobj.student_attendance_last_date = db.UIDDMMYYYYformat(dr["student_attendance_last_date"].ToString());
                            else
                                simsobj.student_attendance_last_date = string.Empty;

                            if (dr["sims_fee_status"].ToString().Equals("A"))
                                simsobj.std_fee_status = true;
                            else
                                simsobj.std_fee_status = false;
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.parent_email_id = dr["parent_email_id"].ToString();
                            simsobj.parent_mobile_no = dr["parent_mobile_no"].ToString();
                            simsobj.fees_paid_email_btn = dr["fees_paid_email_btn"].ToString();
                            simsobj.fees_paid_sms_btn = dr["fees_paid_sms_btn"].ToString();
                            simsobj.student_attendance_per = dr["StudentAttendance"].ToString();
                            simsobj.sims_student_academic_status_code = dr["sims_student_academic_status"].ToString();
                            simsobj.sims_student_academic_status = dr["sims_student_academic_status"].ToString().Equals("C") ? "Cancelled" : "Acative";
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("getSeachOptions")]
        public HttpResponseMessage getSeachOptions()
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetStudentFee",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'O'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 objNew = new sims043();
                            objNew.sims_search_code = dr["sims_appl_parameter"].ToString();
                            objNew.sims_search_code_name = dr["sims_appl_form_field_value1"].ToString();
                            objNew.sims_search_code_status = dr["sims_appl_form_field_value3"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("Getsims541_InvoiceReportUrl")]
        public HttpResponseMessage Getsims541_InvoiceReportUrl()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            string url = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "U")
                            });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            url = dr["sims_appl_form_field_value1"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, url);

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, url); }
        }


        [Route("Getsims541_InvoiceMode")]
        public HttpResponseMessage Getsims541_InvoiceMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            List<sims541> coll = new List<sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "M")
                            });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_invoce_mode = dr["sims_appl_form_field_value1"].ToString();
                            obj.sims_invoce_mode_code = dr["sims_appl_parameter"].ToString();
                            coll.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, coll);

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
        }

        [Route("Getsims541_FrequencyValue")]
        public HttpResponseMessage Getsims541_FrequencyValue(string cur_code, string academic_year, string freq_flag)
        {
            List<sims541> coll = new List<sims541>();
            try
            {
                // if (freq_flag.ToUpper() == "T")//Get Terms
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                            new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "T"),
                                   new SqlParameter("@cur_code", cur_code),
                                   new SqlParameter("@academic_year", academic_year)
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                sims541 obj = new sims541();
                                obj.sims_invoce_frq_name = dr["sims_fee_term_desc_en"].ToString();
                                obj.sims_invoce_frq_name_value = dr["sims_fee_term_code"].ToString();
                                coll.Add(obj);
                            }
                        }
                    }

                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("Getsims541_CreationOnFee")]
        public HttpResponseMessage Getsims541_CreationOnFee()
        {
            string data = string.Empty;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                        new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "C")
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        data = dr["sims_appl_parameter"].ToString();
                    }
                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, data); }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [Route("Getsims541_FeeTypes")]
        public HttpResponseMessage Getsims541_FeeTypes(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                        new List<SqlParameter>() 
                         { 
                                   new SqlParameter("@opr", "F"),
                                   new SqlParameter("@cur_code", cur_code),
                                   new SqlParameter("@academic_year", academic_year),
                                   new SqlParameter("@grade_code", grade_code),
                                   new SqlParameter("@section_code", section_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_fee_code = dr["sims_fee_code"].ToString();
                            obj.sims_fee_code_desc = dr["sims_fee_code_description"].ToString();
                            coll.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("Getsims541_InvoiceStudents")]
        public HttpResponseMessage Getsims541_InvoiceStudents(string cur_code, string academic_year, string grade_code, string section_code, string term_code, string search, string fee_code_search, string isPayingAgent = null, string acc_ctl = null, string acc_no = null)
        {
            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                        new List<SqlParameter>() 
                            { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@academic_year", academic_year),
                            new SqlParameter("@grade_code",string.IsNullOrEmpty(grade_code)?null:grade_code ),
                            new SqlParameter("@section_code",string.IsNullOrEmpty(section_code)?null:section_code),
                            new SqlParameter("@isPayingAgent", isPayingAgent),
                            new SqlParameter("@slma_ldgrctl_code", acc_ctl),
                            new SqlParameter("@slma_acno", acc_no),
                            new SqlParameter("@term_code", term_code),
                            new SqlParameter("@search", string.IsNullOrEmpty(search)?null:search) ,
                            new SqlParameter("@fee_code_search", string.IsNullOrEmpty(fee_code_search)?null:fee_code_search),
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 obj = new sims541();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_name = dr["StudentName"].ToString();
                            obj.sims_parent_name = dr["ParentName"].ToString();
                            obj.sims_student_class = dr["Class"].ToString();
                            obj.sims_paid_amount = "0";
                            obj.sims_invoce_mode = dr["invoice_mode"].ToString();
                            obj.sims_period_code = dr["period_code"].ToString();
                            obj.sims_invoice_status = false;
                            if (dr["Invoice_Status"].ToString() == "Y")
                                obj.sims_invoice_student_status = true;
                            else
                                obj.sims_invoice_student_status = false;
                            coll.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            { return Request.CreateResponse(HttpStatusCode.OK, coll); }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("GenerateInvoiceForStudents")]
        public HttpResponseMessage GenerateInvoiceForStudents(string term_code, string invoice_mode, string payingAgentFlag, string fee_code_search, List<sims541> temp)
        {
            var obj = (from x in temp where x.sims_invoice_status == true && x.sims_invoice_student_status == false select x).ToList();
            for (int i = 0; i < obj.Count; i++)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document",
                                new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", payingAgentFlag.ToUpper() == "Y"?"X":"I"),
                                new SqlParameter("@cur_code", obj[i].sims_cur_code),
                                new SqlParameter("@academic_year", obj[i].sims_academic_year),
                                new SqlParameter("@grade_code", obj[i].sims_grade_code),
                                new SqlParameter("@section_code", obj[i].sims_section_code),
                                new SqlParameter("@isPayingAgent", payingAgentFlag),
                                new SqlParameter("@term_code", term_code),
                                new SqlParameter("@invoice_mode", invoice_mode),
                                new SqlParameter("@enroll_number", obj[i].sims_enroll_number),
                                new SqlParameter("@fee_code_search", string.IsNullOrEmpty(fee_code_search)?null:fee_code_search)
                            });

                        int r = dr.RecordsAffected;
                    }
                }
                catch (Exception x)
                {
                    { return Request.CreateResponse(HttpStatusCode.OK, false); }

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("SearchInvoice")]
        public HttpResponseMessage SearchInvoice(string search_code, string cur_code, string academic_year, string grade_code, string section_code)// sims541 obj)
        {
            search_code = string.IsNullOrEmpty(search_code) ? null : search_code;
            cur_code = string.IsNullOrEmpty(cur_code) ? null : cur_code;
            academic_year = string.IsNullOrEmpty(academic_year) ? null : academic_year;
            grade_code = string.IsNullOrEmpty(grade_code) ? null : grade_code;
            section_code = string.IsNullOrEmpty(section_code) ? null : section_code;

            List<sims541> coll = new List<sims541>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document_details",
                            new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "S"),
                                    new SqlParameter("@Search", search_code),
                                    new SqlParameter("@start_date", null),
                                    new SqlParameter("@end_date", null),
                                    new SqlParameter("@cur_code", cur_code),
                                    new SqlParameter("@academic_year", academic_year),
                                    new SqlParameter("@grade_code", grade_code),
                                    new SqlParameter("@section_code", section_code)
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims541 t_obj = new sims541();
                            t_obj.in_no = dr["in_no"].ToString();
                            //t_obj.in_date = DateTime.Parse(dr["in_date"].ToString()).ToShortDateString();
                            t_obj.in_date = db.UIDDMMYYYYformat(dr["in_date"].ToString());
                            t_obj.in_status = dr["in_status"].ToString();
                            t_obj.in_status_code = dr["InStatus"].ToString();
                            t_obj.sims_invoice_amount = dr["in_total_amount"].ToString();
                            t_obj.sims_enroll_number = dr["enroll_number"].ToString();
                            t_obj.sims_student_name = string.Format("{0}-{1}", t_obj.sims_enroll_number, dr["StudentName"].ToString());
                            t_obj.sims_parent_name = dr["Father_Name"].ToString();
                            t_obj.sims_academic_year = dr["in_academic_year"].ToString(); // obj.sims_academic_year;
                            t_obj.sims_cur_code = dr["sims_cur_code"].ToString(); //obj.sims_cur_code;
                            t_obj.sims_grade_code = dr["sims_grade_code"].ToString(); //obj.sims_grade_code;
                            t_obj.sims_section_code = dr["sims_section_code"].ToString(); //obj.sims_section_code;
                            t_obj.sims_period_code = dr["in_reference_no"].ToString();
                            t_obj.sims_invoce_mode = dr["Invoice_mode"].ToString();
                            t_obj.sims_student_class = dr["Class"].ToString();
                            t_obj.sims_invoice_status = false;
                            coll.Add(t_obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, coll);
            }
            return Request.CreateResponse(HttpStatusCode.OK, coll);
        }

        [Route("updateInvoices")]
        public HttpResponseMessage updateInvoices(List<string> invoice_nos)
        {
            string invoicenos = string.Empty;
            if (invoice_nos.Count > 0)
                invoicenos = string.Join(",", invoice_nos);

            try
            {
                foreach (string x in invoice_nos)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_invoice_document_details",
                                new List<SqlParameter>() 
                            { 
                                    new SqlParameter("@opr", "U"),
                                    new SqlParameter("@Search", "5"),
                                    new SqlParameter("@inv_no", x.ToString())
                            });
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.NotModified, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        #endregion

        #region DefaulterEmail

        [Route("GetDefaulterSection")]
        public HttpResponseMessage GetDefaulterSection(string cur_code, string ac_year, string g_code)
        {
            List<simsClass> mod_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SC"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", ac_year),
                                new SqlParameter("@grade_code", g_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_section_code = dr["sims_section_code"].ToString();
                            objNew.sims_section_name = dr["sims_section_name_en"].ToString();
                            objNew.sims_sectiom_promote_code = dr["sims_section_promote_code"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetDefaulterfeeTypes")]
        public HttpResponseMessage GetDefaulterfeeTypes()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FT")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetDefaulterviewList")]
        public HttpResponseMessage GetDefaulterviewList()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "VL")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_view_list_code = dr["comn_appl_parameter"].ToString();
                            objNew.sims_view_list_description = dr["comn_appl_form_field_value1"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetDefaulterviewMode")]
        public HttpResponseMessage GetDefaulterviewMode()
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "VM")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_view_mode_code = dr["param_type_code"].ToString();
                            objNew.sims_view_mode_description = dr["param_type_desc"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        //[Route("GetDefaulterEmailMessage")]
        //public HttpResponseMessage GetDefaulterEmailMessage()
        //{
        //    string email_default_template = string.Empty;
        //    string ims_msg_sr_no=string.Empty;
        //    string sims_msg_subject = string.Empty;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
        //                new List<SqlParameter>() 
        //                 { 
        //                        new SqlParameter("@opr", "F"),
                                
                           
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {

                            
        //                    ims_msg_sr_no = dr["ims_msg_sr_no"].ToString();
        //                    sims_msg_subject = dr["sims_msg_subject"].ToString();
        //                    email_default_template = dr["sims_msg_body"].ToString();

        //                }
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
        //    }
        //}



        [Route("GetDefaulterEmailMessage")]
        public HttpResponseMessage GetDefaulterEmailMessage()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetDefaulterEmailMessage(),PARAMETERS :: NO";


            List<defaulter1> emailtemp = new List<defaulter1>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'F'),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {


                            defaulter1 simsobj = new defaulter1();

                            simsobj.email_default_template = dr["sims_msg_body"].ToString();
                            simsobj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            simsobj.sims_msg_subject = dr["sims_msg_subject"].ToString();

                            emailtemp.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, emailtemp);

            }
            return Request.CreateResponse(HttpStatusCode.OK, emailtemp);
        }






        [Route("GetDefaulterTerms")]
        public HttpResponseMessage GetDefaulterTerms(string cur_code, string academic_year)
        {
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year",academic_year),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_term_code = dr["sims_Fee_term_code"].ToString();
                            objNew.sims_term_description = dr["sims_fee_term_desc_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("DefaultersList")]
        public HttpResponseMessage DefaultersList(sims_defaulter obj)
        {
            List<sims_defaulter>[] arr = new List<sims_defaulter>[2];
            List<sims_defaulter> mod_list = new List<sims_defaulter>();
            List<sims_defaulter> summary = new List<sims_defaulter>();
            arr[0] = new List<sims_defaulter>();
            arr[0] = mod_list;
            arr[1] = new List<sims_defaulter>();
            arr[1] = summary;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SS"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@grade_code", obj.sims_grade_code),
                                new SqlParameter("@section_code", obj.sims_section_code),
                                new SqlParameter("@fee_types", obj.sims_fee_code),
                                new SqlParameter("@view_list_opt", obj.sims_view_list_code),
                                new SqlParameter("@amount_param", string.IsNullOrEmpty(obj.sims_amount)?null:obj.sims_amount),
                                new SqlParameter("@search", string.IsNullOrEmpty(obj.sims_search)?null:obj.sims_search),
                                new SqlParameter("@view_mode_opt", string.IsNullOrEmpty(obj.sims_view_mode_code)?null:obj.sims_view_mode_code),
                                new SqlParameter("@term_code", obj.sims_term_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //    sims_defaulter objNew = new sims_defaulter();
                            //    objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            //    objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //    objNew.sims_fee_paid = dr["sims_fee_paid"].ToString();
                            //    objNew.student_name = dr["student_name"].ToString();
                            //    objNew.grade_name = dr["grade_name"].ToString();
                            //    objNew.section_name = dr["section_name"].ToString();
                            //    objNew.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            //    objNew.sims_father_name = dr["father_name"].ToString();
                            //    objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //    objNew.sims_paid_amount = dr["sims_fee_paid"].ToString();
                            //    objNew.sims_balance_amount = (decimal.Parse(objNew.sims_fee_amount) - decimal.Parse(objNew.sims_paid_amount)).ToString();
                            //    objNew.sims_fee_type_desc = dr["sims_fee_type_desc"].ToString();
                            //    objNew.sims_fee_amount_total = dr["sims_fee_amount_total"].ToString();
                            //    objNew.sims_paid_amount_total = dr["sims_fee_paid_total"].ToString();
                            //    objNew.sims_balance_amount_total = (decimal.Parse(objNew.sims_fee_amount_total) - decimal.Parse(objNew.sims_paid_amount_total)).ToString();
                            //    objNew.parent_email = dr["parent_email"].ToString();
                            //    objNew.isSelected = false;
                            //    mod_list.Add(objNew);
                            sims_defaulter objNew = new sims_defaulter();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            //objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //objNew.sims_fee_paid = dr["sims_fee_paid"].ToString();
                            objNew.student_name = dr["student_name"].ToString();
                            objNew.grade_name = dr["grade_name"].ToString();
                            objNew.section_name = dr["section_name"].ToString();
                            objNew.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            objNew.sims_father_name = dr["father_name"].ToString();
                            //objNew.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            //objNew.sims_paid_amount = dr["sims_fee_paid"].ToString();
                            objNew.sims_balance_amount = dr["sims_balance_amount"].ToString();//(decimal.Parse(objNew.sims_fee_amount) - decimal.Parse(objNew.sims_paid_amount)).ToString();
                            objNew.sims_fee_type_desc = dr["sims_fee_type_desc"].ToString();
                            //objNew.sims_fee_amount_total = dr["sims_fee_amount_total"].ToString();
                            //objNew.sims_paid_amount_total = dr["sims_fee_paid_total"].ToString();
                            objNew.sims_balance_amount_total = dr["sims_fee_amount_total"].ToString(); //(decimal.Parse(objNew.sims_fee_amount_total) - decimal.Parse(objNew.sims_paid_amount_total)).ToString();
                            objNew.parent_email = dr["parent_email"].ToString();
                            objNew.parent_mobile = dr["parent_mobile"].ToString();
                            objNew.isSelected = false;
                            mod_list.Add(objNew);
                        }
                    }
                    foreach (sims_defaulter t in mod_list)
                    {
                        sims_defaulter to = new sims_defaulter();
                        to.sims_enroll_number = t.sims_enroll_number;
                        to.student_name = t.student_name;
                        to.grade_name = t.grade_name;
                        to.section_name = t.section_name;
                        to.sims_sibling_parent_number = t.sims_sibling_parent_number;
                        to.sims_father_name = t.sims_father_name;
                        to.sims_balance_amount_total = t.sims_balance_amount_total;
                        to.parent_email = t.parent_email;
                        to.parent_mobile = t.parent_mobile;

                        int cnt = summary.Where(x => x.sims_enroll_number == t.sims_enroll_number).Count();
                        if (cnt == 0)
                            summary.Add(to);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, arr);
            }
        }

        [Route("getDefaulterListNEw")]
        public HttpResponseMessage getDefaulterListNEw(string cur_code, string academic_year, string grade_code, string section, string feetype, string academic_status, string doc_date, string min_value,string max_value)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));

            List<Defaulter_LIst_new> mod_list = new List<Defaulter_LIst_new>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_to_Defaulter]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@acad_year", academic_year),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code", section),
                                new SqlParameter("@fee_code", feetype),
                                new SqlParameter("@acad_status", academic_status),
                                new SqlParameter("@doc_date", db.DBYYYYMMDDformat(doc_date)),
                                new SqlParameter("@min_value",min_value),
                                new SqlParameter("@max_value",max_value),
                                
                                

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Defaulter_LIst_new simsobj = new Defaulter_LIst_new();


                            simsobj.sims_grade_name_en= dr["sims_grade_name_en"].ToString();
                            simsobj.section_name= dr["section_name"].ToString();
                            simsobj.sims_enroll_number= dr["sims_enroll_number"].ToString();
						    simsobj.student_name= dr["student_name"].ToString();
                            simsobj.sims_parent_number= dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name= dr["sims_parent_name"].ToString();
                            simsobj.sims_fee_period_expected= dr["sims_fee_period_expected"].ToString();
							simsobj.sims_fee_period_paid= dr["sims_fee_period_paid"].ToString();
                            simsobj.final_total= dr["final_total"].ToString();
                            simsobj.sims_parent_contact= dr["sims_parent_contact"].ToString();
                            simsobj.sims_parent_email = dr["sims_parent_email"].ToString();

                          
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

         



        [Route("getDefaulterListNEw1")]
        public HttpResponseMessage getDefaulterListNEw1(string cur_code, string academic_year, string grade_code, string section, string feetype, string academic_status, string doc_date, string min_value, string max_value)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));

            List<Defaulter_LIst_new> mod_list = new List<Defaulter_LIst_new>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Percentage_count]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@acad_year", academic_year),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code", section),
                                new SqlParameter("@fee_code", feetype),
                                new SqlParameter("@acad_status", academic_status),
                                new SqlParameter("@doc_date", db.DBYYYYMMDDformat(doc_date)),
                                new SqlParameter("@min_value",min_value),
                                new SqlParameter("@max_value",max_value),
                                
                                

                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Defaulter_LIst_new simsobj = new Defaulter_LIst_new();


                            simsobj.sims_grade_name_en= dr["sims_grade_name_en"].ToString();
                            simsobj.section_name= dr["section_name"].ToString();
                            simsobj.sims_enroll_number= dr["sims_enroll_number"].ToString();
						    simsobj.student_name= dr["student_name"].ToString();
                            simsobj.sims_parent_number= dr["sims_parent_number"].ToString();
                            simsobj.sims_parent_name= dr["sims_parent_name"].ToString();
                            simsobj.sims_fee_period_expected= dr["sims_fee_period_expected"].ToString();
							simsobj.sims_fee_period_paid= dr["sims_fee_period_paid"].ToString();
                            simsobj.final_total= dr["final_total"].ToString();
                            simsobj.sims_parent_contact= dr["sims_parent_contact"].ToString();
                            simsobj.sims_parent_email = dr["sims_parent_email"].ToString();

                            simsobj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            simsobj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            simsobj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();

                          
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }





        [Route("getAllPercentage")]
        public HttpResponseMessage getAllPercentage()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));

            List<Defaulter_LIst_new> mod_list = new List<Defaulter_LIst_new>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_to_Defaulter]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "K")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Defaulter_LIst_new simsobj = new Defaulter_LIst_new();
                            simsobj.sims_appl_parameter= dr["sims_appl_parameter"].ToString();
                            simsobj.min_max_value= dr["min_value"].ToString()+'-'+ dr["max_value"].ToString();
                           
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }





        [Route("emailtoDefaulters")]
        public HttpResponseMessage emailtoDefaulters(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@email_user_code", obj.sims_sibling_parent_number),
                                new SqlParameter("@recepient_id", obj.parent_email),
                                new SqlParameter("@email_subject", obj.email_subject),
                                new SqlParameter("@email_message", obj.email_message)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }




        [Route("emailtoDefaulters_SISO")]
        public HttpResponseMessage emailtoDefaulters_SISO(List<sims_defaulter> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_defaulter obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@cur_code", obj.sims_cur_code),
                                new SqlParameter("@academic_year", obj.sims_academic_year),
                                new SqlParameter("@email_user_code", obj.sims_parent_number),
                                new SqlParameter("@recepient_id", obj.sims_parent_email),
                                new SqlParameter("@email_subject", obj.email_subject),
                                new SqlParameter("@email_message", obj.email_message)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);

        }





        //SMS Methods
        [Route("GetDefaulterSMSMessage")]
        public HttpResponseMessage GetDefaulterSMSMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        //ALert Methods
        [Route("GetDefaulterAlertMessage")]
        public HttpResponseMessage GetDefaulterAlertMessage()
        {
            string email_default_template = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_defaulter_rpt",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "B"),
                                
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email_default_template = dr["sims_msg_body"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, email_default_template);
            }
        }

        #endregion

        #region late_fee_rules

        [Route("GetRuleAllCurName")]
        public HttpResponseMessage GetRuleAllCurName()
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule simsobj = new sims_late_fee_rule();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetRuleAllAcademicYears")]
        public HttpResponseMessage GetRuleAllAcademicYears(string cur_code)
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule objNew = new sims_late_fee_rule();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_academic_year_start_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            objNew.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            objNew.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetLateRules")]
        public HttpResponseMessage GetLateRules()
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SR")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule objNew = new sims_late_fee_rule();
                            objNew.sims_late_rule_code = dr["sims_late_rule_code"].ToString();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_late_rule_description = dr["sims_late_rule_description"].ToString();
                            objNew.sims_fee_code = dr["sims_fee_code"].ToString();
                            objNew.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            objNew.sims_fee_term_code = dr["sims_fee_term_code"].ToString();
                            objNew.sims_fee_term_desc_en = dr["sims_fee_term_desc_en"].ToString();
                            objNew.sims_late_rule_status = dr["sims_late_rule_status"].ToString().Equals("A") ? true : false;

                            mod_list.Add(objNew);
                        }
                        dr.Close();
                    }
                    //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims_late_fee_rule obj in mod_list)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                            { 
                            new SqlParameter("@opr", "SD"),
                            new SqlParameter("@rule_code", obj.sims_late_rule_code)
                            });
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                sims_late_fee_rule_details d = new sims_late_fee_rule_details();
                                d.sims_late_rule_code = dr1["sims_late_rule_code"].ToString();
                                d.sims_fee_code = dr1["sims_late_rule_fee_code"].ToString();
                                d.sims_fee_code_description = dr1["sims_fee_code_description"].ToString();
                                d.sims_late_rule_details_status = dr1["sims_late_rule_fee_code_status"].ToString().Equals("A") ? true : false;
                                obj.details.Add(d);
                            }
                            dr1.Close();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetLateRulesTerm")]
        public HttpResponseMessage GetLateRulesTerm(string cur, string aca)
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "T"),
                new SqlParameter("@cur_Code", cur),
                new SqlParameter("@academic_year", aca),
                
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule obj = new sims_late_fee_rule();
                            obj.sims_fee_term_code = dr["sims_term_code"].ToString();
                            obj.sims_fee_term_desc_en = dr["sims_term_desc_en"].ToString();
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetLateRulesFeeType")]
        public HttpResponseMessage GetLateRulesFeeType()
        {
            List<sims_late_fee_rule_details> mod_list = new List<sims_late_fee_rule_details>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                        new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "F")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule_details obj = new sims_late_fee_rule_details();
                            obj.sims_fee_code = dr["sims_fee_code"].ToString();
                            obj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            obj.sims_late_rule_details_status = false;
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDrules")]
        public HttpResponseMessage CURDrules(sims_late_fee_rule inpt)
        {
            bool res = false;
            string rule_Code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                        new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "IR"),
                        new SqlParameter("@cur_Code", inpt.sims_cur_code),
                        new SqlParameter("@academic_year", inpt.sims_academic_year),
                        new SqlParameter("@rule_description", inpt.sims_late_rule_description),
                        new SqlParameter("@fee_code", inpt.sims_fee_code),
                        new SqlParameter("@term_code", inpt.sims_fee_term_code),
                        new SqlParameter("@rule_status", inpt.sims_late_rule_status==true?"A":"I")
                        });
                    if (dr.RecordsAffected > 0)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            rule_Code = dr["rule_Code"].ToString();
                        }
                        res = true;
                        dr.Close();
                    }
                }
                if (!string.IsNullOrEmpty(rule_Code) && res)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                            new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "RD"),
                        new SqlParameter("@rule_code", rule_Code),
                        new SqlParameter("@fee_code", inpt.sims_fee_code_list),
                        });
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("updaterules")]
        public HttpResponseMessage updaterules(sims_late_fee_rule inpt)
        {
            bool res = false;
            string rule_Code = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                        new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "UR"),
                        new SqlParameter("@cur_Code", inpt.sims_cur_code),
                        new SqlParameter("@academic_year", inpt.sims_academic_year),
                        new SqlParameter("@rule_description", inpt.sims_late_rule_description),
                        new SqlParameter("@rule_code", inpt.sims_late_rule_code),
                        new SqlParameter("@fee_code", inpt.sims_fee_code),
                        new SqlParameter("@rule_status", inpt.sims_late_rule_status==true?"A":"I")
                        });
                    if (dr.RecordsAffected > 0)
                    {
                        res = true;
                        dr.Close();
                    }
                }
                if (res)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                            new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "RD"),
                        new SqlParameter("@rule_code", inpt.sims_late_rule_code),
                        new SqlParameter("@fee_code", inpt.sims_fee_code_list),
                        });
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("CURDdeleterules")]
        public HttpResponseMessage CURDdeleterules(List<sims_late_fee_rule> obj)
        {
            bool res = false;
            string rule_Code = string.Empty;
            try
            {
                foreach (sims_late_fee_rule inpt in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_late_rule_fee_master]",
                            new List<SqlParameter>() 
                         { 
                        new SqlParameter("@opr", "DR"),
                        new SqlParameter("@cur_Code", inpt.sims_cur_code),
                        new SqlParameter("@academic_year", inpt.sims_academic_year),
                        new SqlParameter("@rule_code", inpt.sims_late_rule_code)
                        });
                        if (dr.RecordsAffected > 0)
                        {
                            res = true;
                            dr.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("GetLateFeeeStudents")]
        public HttpResponseMessage GetLateFeeeStudents(string cc, string ay, string gc, string sc, string term)
        {
            List<sims_late_fee_students> mod_list = new List<sims_late_fee_students>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_student_late_fee_rule_details]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@OPR", "S"),
                new SqlParameter("@curcode", cc),
                new SqlParameter("@acyyear", ay),
                new SqlParameter("@grade", gc),
                new SqlParameter("@section", sc),
                new SqlParameter("@term", term),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_students obj = new sims_late_fee_students();
                            obj.sims_late_rule_code = dr["sims_late_rule_code"].ToString();
                            obj.sims_cur_code = dr["sims_fee_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_fee_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_fee_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_fee_section_code"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_fee_term_code = dr["term_code"].ToString();
                            obj.sims_late_fee_amount = dr["sims_late_rule_amount"].ToString();
                            obj.sims_late_rule_description = dr["sims_late_rule_description"].ToString();
                            obj.sims_student_name = dr["sims_student_name"].ToString();
                            obj.sims_fee_amount_transpfer_code = dr["sims_fee_amount_transpfer_code"].ToString();
                            obj.sims_late_rule_status = false;
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("CRUDLateFeeStudents")]
        public HttpResponseMessage CRUDLateFeeStudents(List<sims_late_fee_students> mod_list)
        {
            bool res = true;
            try
            {
                foreach (sims_late_fee_students obj in mod_list)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_student_late_fee_rule_details]",
                            new List<SqlParameter>() 
                         { 
                           new SqlParameter("@OPR", "I"),
                           new SqlParameter("@curcode", obj.sims_cur_code),
                           new SqlParameter("@acyyear", obj.sims_academic_year),
                           new SqlParameter("@grade", obj.sims_grade_code),
                           new SqlParameter("@section", obj.sims_section_code),
                           new SqlParameter("@sims_enroll_number", obj.sims_enroll_number),
                           new SqlParameter("@sims_rule_code", obj.sims_late_rule_code),
                           new SqlParameter("@sims_late_rule_fee_code", obj.sims_fee_amount_transpfer_code),
                           new SqlParameter("@sims_late_rule_amount", obj.sims_late_fee_amount),
                           new SqlParameter("@sims_status", obj.sims_late_rule_status==true?"A":"I"),
                           new SqlParameter("@term", obj.sims_fee_term_code),

                           
                        });
                        int r = dr.RecordsAffected;
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        /*Late Formula*/
        [Route("GetLateFormula")]
        public HttpResponseMessage GetLateFormula()
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SF")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule objNew = new sims_late_fee_rule();
                            objNew.sims_late_rule_code = dr["sims_late_rule_code"].ToString();
                            objNew.sims_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            objNew.sims_late_rule_description = dr["sims_late_rule_description"].ToString();
                            objNew.sims_fee_term_code = dr["sims_fee_term_code"].ToString();
                            objNew.sims_fee_term_desc_en = dr["sims_fee_term_desc_en"].ToString();
                            objNew.sims_late_rule_status = dr["sims_late_rule_status"].ToString().Equals("A") ? true : false;
                            objNew.sims_late_rule_min_days = dr["sims_late_rule_min_days"].ToString();
                            objNew.sims_late_rule_max_days = dr["sims_late_rule_max_days"].ToString();
                            objNew.sims_late_rule_formula_amount = dr["sims_late_rule_formula_amount"].ToString();
                            objNew.sims_late_rule_per_day_flag = dr["sims_late_rule_per_day_flag"].ToString().Equals("Y") ? true : false;
                            objNew.sims_late_rule_formula_status = dr["sims_late_rule_formula_status"].ToString().Equals("A") ? true : false;
                            objNew.sims_late_rule_formula_sr = dr["sims_late_rule_formula_sr"].ToString();
                            mod_list.Add(objNew);
                        }
                        dr.Close();
                    }
                    //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetLateRulesForFormula")]
        public HttpResponseMessage GetLateRulesForFormula(string cur_code, string ac, string term_code)
        {
            List<sims_late_fee_rule> mod_list = new List<sims_late_fee_rule>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FR"),
                                new SqlParameter("@cur_Code", cur_code),
                                new SqlParameter("@academic_year", ac),
                                new SqlParameter("@term_code", term_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_late_fee_rule objNew = new sims_late_fee_rule();
                            objNew.sims_late_rule_code = dr["sims_late_rule_code"].ToString();
                            objNew.sims_late_rule_description = dr["sims_late_rule_description"].ToString();
                            mod_list.Add(objNew);
                        }
                        dr.Close();
                    }
                    //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("DefineLateRulesFormula")]
        public HttpResponseMessage DefineLateRulesFormula(sims_late_fee_rule obj)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FI"),
                                new SqlParameter("@rule_code",obj.sims_late_rule_code ),
                                new SqlParameter("@rule_min_days", obj.sims_late_rule_min_days),
                                new SqlParameter("@rule_max_days", obj.sims_late_rule_max_days),
                                new SqlParameter("@rule_formula_amount", obj.sims_late_rule_formula_amount),
                                new SqlParameter("@per_day_flag", obj.sims_late_rule_per_day_flag==true?"Y":"N"),
                                new SqlParameter("@formula_status", obj.sims_late_rule_formula_status==true?"A":"I"),
                         });
                    res = dr.RecordsAffected > 0 ? true : false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        [Route("updateLateRulesFormula")]
        public HttpResponseMessage updateRulesFormula(sims_late_fee_rule obj)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FU"),
                                new SqlParameter("@rule_code",obj.sims_late_rule_code ),
                                new SqlParameter("@rule_min_days", obj.sims_late_rule_min_days),
                                new SqlParameter("@rule_max_days", obj.sims_late_rule_max_days),
                                new SqlParameter("@rule_formula_amount", obj.sims_late_rule_formula_amount),
                                new SqlParameter("@per_day_flag", obj.sims_late_rule_per_day_flag==true?"Y":"N"),
                                new SqlParameter("@formula_status", obj.sims_late_rule_formula_status==true?"A":"I"),
                                new SqlParameter("@rule_formula_sr", obj.sims_late_rule_formula_sr),
                         });
                    res = dr.RecordsAffected > 0 ? true : false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        [Route("deleteLateRulesFormula")]
        public HttpResponseMessage deleteRulesFormula(List<sims_late_fee_rule> objs)
        {
            bool res = false;
            try
            {
                foreach (sims_late_fee_rule obj in objs)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_late_rule_fee_master",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FD"),
                                new SqlParameter("@rule_code",obj.sims_late_rule_code ),
                                new SqlParameter("@rule_formula_sr", obj.sims_late_rule_formula_sr)
                         });
                        res = dr.RecordsAffected > 0 ? true : false;
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        #endregion


        #region vacnacy
        [Route("getvacancyEmpData")]
        public HttpResponseMessage getEmpData()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));

            List<Pers099_Vacancy> code_list = new List<Pers099_Vacancy>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetEMPData_Temp", new List<SqlParameter>());
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_Vacancy obj = new Pers099_Vacancy();
                            //obj.em_Company_name = dr["Company_Name"].ToString();
                            //obj.em_Company_Code = dr["Company_Code"].ToString();
                            //obj.em_Company_Short_Name = dr["Company_ShortName"].ToString();

                            obj.em_Designation_Code = dr["Designation_Code"].ToString();
                            obj.em_Designation_name = dr["Designation_Desc"].ToString();
                            obj.em_Designation_Company_Code = dr["Designation_Company_code"].ToString();

                            obj.em_Grade_Code = dr["Grade_Code"].ToString();
                            obj.em_Grade_Company_Code = dr["Grade_Company_code"].ToString();
                            obj.em_Grade_name = dr["Grade_Desc"].ToString();

                            obj.em_Dept_Code = dr["Department_Code"].ToString();
                            obj.em_Dept_Company_Code = dr["Department_Company_Code"].ToString();
                            obj.em_Dept_name = dr["Department_Name"].ToString();
                            obj.em_Dept_Short_Name = dr["Department_Short_Name"].ToString();

                            obj.em_Staff_Type_name = dr["Staff_Type_Value"].ToString();
                            obj.em_Staff_Type_Code = dr["Staff_Type_Code"].ToString();

                            obj.em_Religion_Code = dr["sims_religion_code"].ToString();
                            obj.em_Religion_name = dr["sims_religion_name_en"].ToString();

                            obj.em_Sex_name = dr["sims_appl_form_field_value1_gender"].ToString();
                            obj.em_Sex_Code = dr["sims_appl_parameter_gender"].ToString();

                            obj.em_Salutation_name = dr["sims_appl_form_field_value1_salut"].ToString();
                            obj.em_Salutation_Code = dr["sims_appl_parameter_salut"].ToString();

                            //obj.em_Country_name = dr["sims_country_name_en"].ToString();
                            //obj.em_Country_Code = dr["sims_country_code"].ToString();

                            obj.em_Nation_Code = dr["sims_nationality_code"].ToString();
                            obj.em_Nation_name = dr["sims_nationality_name_en"].ToString();

                            //obj.em_Dest_Code = dr["Pays_Destination_Code"].ToString();
                            //obj.em_Dest_name = dr["Pays_Destination_Desc"].ToString();

                            obj.em_Marital_Status_name = dr["sims_appl_form_field_value1_Married_Status"].ToString();
                            obj.em_Marital_Status_Code = dr["sims_appl_parameter_Married_Status"].ToString();

                            obj.em_blood_group_name = dr["sims_appl_form_field_value1_blood"].ToString();
                            obj.em_blood_group_code = dr["sims_appl_parameter_blood"].ToString();

                            obj.em_ethnicity_name = dr["ethnicity_name_en"].ToString();
                            obj.em_ethnicity_code = dr["ethnicity_code"].ToString();


                            obj.em_Bank_Code = dr["bank_code"].ToString();
                            obj.em_Bank_name = dr["bank_name"].ToString();

                            obj.em_visa_type_code = dr["visatype_code"].ToString();
                            obj.em_visa_type = dr["visatype_name"].ToString();

                            obj.em_service_status_code = dr["servicestatus_code"].ToString();
                            obj.em_service_status = dr["servicestatus_name"].ToString();

                            obj.em_secret_question_code = dr["secretquestion_code"].ToString();
                            obj.em_secret_question = dr["secretquestion"].ToString();

                            obj.em_Country_Code = dr["sims_country_code"].ToString();
                            obj.em_Country_name = dr["sims_country_name_en"].ToString();

                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getvacancy")]
        public HttpResponseMessage getvacancy()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));

            List<Pers099_Vacancy> code_list = new List<Pers099_Vacancy>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_employee_vacancy_application", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "V")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_Vacancy obj = new Pers099_Vacancy();
                            obj.sims_vacancy_code = dr["pays_vacancy_id"].ToString();
                            obj.sims_vacancy_name = dr["pays_vacancy_name"].ToString();
                            obj.em_Dept_Code = dr["pays_vacancy_dept_code"].ToString();
                            obj.em_Designation_Code = dr["pays_vacancy_profile_desg_code"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getqualificationLevels")]
        public HttpResponseMessage getqualificationLevels()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));

            List<Pers099_Vacancy> code_list = new List<Pers099_Vacancy>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_employee_vacancy_application", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "E")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_Vacancy obj = new Pers099_Vacancy();
                            obj.pays_qualification_level = dr["sims_appl_parameter"].ToString();
                            obj.pays_qualification_level_name = dr["sims_appl_form_field_value1"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getqualification")]
        public HttpResponseMessage getqualification(string level_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));

            List<Pers099_Vacancy> code_list = new List<Pers099_Vacancy>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_employee_vacancy_application", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "Q"),
                        new SqlParameter("@pays_qualification_level", level_code),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_Vacancy obj = new Pers099_Vacancy();
                            obj.pays_qualification_code = dr["pays_qualification_code"].ToString();
                            obj.pays_qualification_name = dr["pays_qualification_desc"].ToString();
                            code_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }


        [Route("vacancyapp")]
        public HttpResponseMessage vacancyapp(Pers099_Vacancy obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmpData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getEmpData"));
            string application_id = "", application_pwd = "";

            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_employee_vacancy_application", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "I"),
                        new SqlParameter("@em_vacancy_id", obj.sims_vacancy_code),
                        new SqlParameter("@em_desg_code", obj.em_Designation_Code),
                        new SqlParameter("@em_nation_code",  obj.em_Nation_Code),
                        new SqlParameter("@em_salutation_code",  obj.em_Salutation_Code),
                        new SqlParameter("@em_first_name", obj.em_first_name),
                        new SqlParameter("@em_middle_name", obj.em_middle_name),
                        new SqlParameter("@em_last_name", obj.em_last_name),
                        new SqlParameter("@em_family_name", obj.em_family_name),
                        new SqlParameter("@em_name_ot", obj.em_name_ot),
                        new SqlParameter("@em_date_of_birth", db.DBYYYYMMDDformat(obj.em_date_of_birth.ToString())),
                        new SqlParameter("@em_sex_code", obj.em_Sex_Code),
                        new SqlParameter("@em_marital_code", obj.em_Marital_Status_Code),
                        new SqlParameter("@em_religion_code", obj.em_Religion_Code),
                        new SqlParameter("@em_ethnicity_code", obj.em_ethnicity_code),
                        new SqlParameter("@em_appartment_number", obj.em_apartment_number),
                        new SqlParameter("@em_building_number", obj.em_building_number),
                        new SqlParameter("@em_street_number", obj.em_street_number),
                        new SqlParameter("@em_area_number", obj.em_area_number),
                        new SqlParameter("@em_summary_address", obj.em_summary_address),
                        new SqlParameter("@em_city", obj.em_City_name),
                        new SqlParameter("@em_state", obj.em_State_name),
                        new SqlParameter("@em_country_code", obj.em_Country_Code),
                        new SqlParameter("@em_phone", obj.em_phone),
                        new SqlParameter("@em_mobile", obj.em_mobile),
                        new SqlParameter("@em_email", obj.em_email),
                        new SqlParameter("@em_fax", obj.em_fax),
                        new SqlParameter("@em_po_box", obj.em_post),
                        new SqlParameter("@em_passport_number", obj.em_passport_no),
                        new SqlParameter("@em_passport_issue_date", db.DBYYYYMMDDformat(obj.em_passport_issue_date)),
                        new SqlParameter("@em_passport_expiry_date", db.DBYYYYMMDDformat(obj.em_passport_exp_date)),
                        new SqlParameter("@em_passport_issuing_authority", obj.em_passport_issuing_authority),
                        new SqlParameter("@em_passport_issue_place", obj.em_passport_issue_place),
                        new SqlParameter("@em_visa_number", obj.em_visa_no),
                        new SqlParameter("@em_visa_issue_date", db.DBYYYYMMDDformat(obj.em_visa_issue_date)),
                        new SqlParameter("@em_visa_expiry_date", db.DBYYYYMMDDformat(obj.em_visa_exp_date)),
                        new SqlParameter("@em_visa_issuing_place", obj.em_visa_issuing_place),
                        new SqlParameter("@em_visa_issuing_authority", obj.em_visa_issuing_authority),
                        new SqlParameter("@em_visa_type", obj.em_visa_type),
                        new SqlParameter("@em_national_id", obj.nation_id),
                        new SqlParameter("@em_national_id_issue_date", db.DBYYYYMMDDformat(obj.em_national_id_issue_date)),
                        new SqlParameter("@em_national_id_expiry_date", db.DBYYYYMMDDformat(obj.em_national_id_expiry_date)),
                        new SqlParameter("@em_pan_no", obj.em_pan_no),
                        new SqlParameter("@em_labour_card_no", obj.em_labour_card_no),
                        new SqlParameter("@em_img", obj.em_img),
                        new SqlParameter("@em_summary_address_local_language", obj.em_summary_address_local_language),
                        new SqlParameter("@em_emergency_contact_name1", obj.em_emergency_contact_name1),
                        new SqlParameter("@em_emergency_contact_name2", obj.em_emergency_contact_name2),
                        new SqlParameter("@em_emergency_contact_number1", obj.em_emergency_contact_number1),
                        new SqlParameter("@em_emergency_contact_number2", obj.em_emergency_contact_number2),
                        new SqlParameter("@em_expected_date_of_join", db.DBYYYYMMDDformat(obj.em_date_of_join)),
                        new SqlParameter("@em_joining_ref", obj.em_joining_ref),
                        new SqlParameter("@em_handicap_status", obj.em_handicap_status==true?"Y":"N"),
                        new SqlParameter("@em_blood_group_code", obj.em_blood_group_code),
                        new SqlParameter("@em_doc", obj.em_doc),
                    });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        application_id = dr["application_id"].ToString();
                        application_pwd = dr["Pwd"].ToString();
                    }
                    result = dr.RecordsAffected > 0 ? true : false;
                    dr.Close();
                    dr.Dispose();
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }

            try
            {
                foreach (Pers099_qual q in obj.qual_details)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("pays_employee_vacancy_application", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@em_applicant_id", application_id),
                            new SqlParameter("@pays_qualification_code", q.pays_qualification_code),
                            new SqlParameter("@pays_qualification_year", q.pays_qualification_year),
                            new SqlParameter("@pays_qualification_remark", q.pays_qualification_remark),
                            new SqlParameter("@pays_qualification_emp_status", "A")
                        });
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, application_id + "/" + application_pwd);

        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            //+ HttpContext.Current.Request.Headers["schoolId"].ToString() +
            string path = "~/" + location;
            root = HttpContext.Current.Server.MapPath(path);
            Directory.CreateDirectory(root);
            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        #endregion

        /*FEE CATEGORY CHANGE*/
        [Route("getFCCStudents")]
        public HttpResponseMessage getFCCStudents(string cc, string ay, string gc, string sc, string search_stud)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_category_change",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SL"),
                                new SqlParameter("@CUR", cc),
                                new SqlParameter("@ACADEMIC", ay),
                                new SqlParameter("@GRADE", gc),
                                new SqlParameter("@SECTION", sc),
                                new SqlParameter("@ENROLL", search_stud),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.std_fee_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.std_fee_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.std_fee_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.std_fee_section_code = dr["sims_section_code"].ToString();
                            simsobj.std_fee_student_name = dr["Name"].ToString();
                            simsobj.std_fee_Class = dr["Class"].ToString();
                            simsobj.sims_student_academic_status = dr["student_status"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getStudentFeeCategory")]
        public HttpResponseMessage getStudentFeeCategory(string cc, string ay, string gc, string sc, string search_stud)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
            cc = string.IsNullOrEmpty(cc) ? null : cc;
            ay = string.IsNullOrEmpty(ay) ? null : ay;
            gc = string.IsNullOrEmpty(gc) ? null : gc;
            sc = string.IsNullOrEmpty(sc) ? null : sc;
            search_stud = string.IsNullOrEmpty(search_stud) ? null : search_stud;
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_category_change",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "SC"),
                                new SqlParameter("@CUR", cc),
                                new SqlParameter("@ACADEMIC", ay),
                                new SqlParameter("@GRADE", gc),
                                new SqlParameter("@SECTION", sc),
                                new SqlParameter("@ENROLL", search_stud),
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_category_code = dr["sims_fee_category"].ToString();
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("getAllFeeCategory")]
        public HttpResponseMessage getAllFeeCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStudent_Fees()";
            Log.Debug(string.Format(debug, "FEES", "GetAllStudentFee"));
          
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_category_change",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "FC")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_category_code = dr["sims_fee_category"].ToString();
                            simsobj.std_fee_category_name = dr["sims_fee_category_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("changeCategory")]
        public HttpResponseMessage changeCategory(string oldCate, string newCat, sims043 obj)
        {
            string result=string.Empty;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_fee_category_change",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@CUR", obj.std_fee_cur_code),
                                new SqlParameter("@ACADEMIC", obj.std_fee_academic_year),
                                new SqlParameter("@GRADE", obj.std_fee_grade_code),
                                new SqlParameter("@SECTION", obj.std_fee_section_code),
                                new SqlParameter("@ENROLL", obj.std_fee_enroll_number),
                                new SqlParameter("@OLDCATEGORY", oldCate),
                                new SqlParameter("@NEWCATEGORY", newCat)
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        result = dr["Result"].ToString();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception e)
            {
                result = "ERROR";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }


        /* MOgra Version*/


        [Route("GetProjectNames")]
        public HttpResponseMessage GetProjectNames()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "PN") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_project_code = dr["sims_appl_parameter"].ToString();
                            pn.lic_project_name = dr["sims_appl_form_field_value1"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetProjectModulesApplications")]
        public HttpResponseMessage GetProjectModulesApplications()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "CM") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.comn_mod_code = dr["comn_mod_code"].ToString();
                            pn.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                new List<SqlParameter>() { new SqlParameter("@opr", "CMA") ,
                                new SqlParameter("@appl_mod_code", pn.comn_mod_code)});
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_version_applications ao = new Mogra_version_applications();
                                        ao.comn_appl_code = dr1["comn_appl_code"].ToString();
                                        ao.comn_appl_name = dr1["comn_appl_name_en"].ToString();
                                        ao.comn_mod_code = dr["comn_mod_code"].ToString();
                                        ao.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                                        pn.comn_applications.Add(ao);
                                    }
                                }
                                dr1.Close();
                                dr1.Dispose();
                            }
                            lst.Add(pn);
                        }
                        dr.Close();
                        dr.Dispose();
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("Getdevelopers")]
        public HttpResponseMessage Getdevelopers()
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() { new SqlParameter("@opr", "DN") });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_developed_by = dr["lic_developed_by"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("GetMograVersionsDetails")]
        public HttpResponseMessage GetMograVersionsDetails(string project_code)
        {
            List<Mogra_Version> lst = new List<Mogra_Version>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "MV"),
                        new SqlParameter("@lic_project_code", project_code),
                        
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mogra_Version pn = new Mogra_Version();
                            pn.lic_changeset_no = dr["lic_changeset_no"].ToString();
                            pn.lic_project_code = dr["lic_project_code"].ToString();
                            pn.lic_project_name = dr["lic_project_name"].ToString();
                            pn.lic_developed_by = dr["lic_developed_by"].ToString();
                            pn.lic_developed_date = dr["lic_developed_date"].ToString();
                            pn.lic_approved_by = dr["lic_approved_by"].ToString();
                            pn.lic_deployed_by = dr["lic_deployed_by"].ToString();
                            pn.lic_deployed_date = dr["lic_deployed_date"].ToString();
                            pn.lic_deployed_status = dr["lic_deployed_status"].ToString();
                            pn.lic_creation_date = dr["lic_creation_date"].ToString();
                            pn.lic_approved_date = dr["lic_approved_date"].ToString();
                            pn.lic_version_number = dr["lic_version_number"].ToString();
                            pn.lic_release_number = dr["lic_release_number"].ToString();
                            pn.lic_patch_number = dr["lic_patch_number"].ToString();
                            if (pn.mogra_details == null)
                                pn.mogra_details = new List<Mogra_Version_Details>();

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                new List<SqlParameter>() {
                                        new SqlParameter("@opr", "MVS"),
                                        new SqlParameter("@lic_changeset_no", pn.lic_changeset_no),
                                });
                                if (dr1.HasRows)
                                {
                                    while (dr1.Read())
                                    {
                                        Mogra_Version_Details vd = new Mogra_Version_Details();
                                        vd.lic_changeset_no = pn.lic_changeset_no;
                                        vd.lic_school_code = dr1["lic_school_code"].ToString();
                                        vd.lic_school_name = dr1["lic_school_name"].ToString();
                                        vd.lic_version_date = dr1["lic_version_date"].ToString();
                                        vd.lic_version_number = dr1["lic_version_number"].ToString();
                                        vd.lic_release_number = dr1["lic_release_number"].ToString();
                                        vd.lic_patch_number = dr1["lic_patch_number"].ToString();
                                        vd.lic_notes = dr1["lic_notes"].ToString().Equals("A") ? true : false;
                                        vd.lic_version_status = dr1["lic_version_status"].ToString().Equals("A") ? true : false;
                                        vd.lic_test_case = dr1["lic_test_case"].ToString().Equals("A") ? true : false;
                                        vd.lic_document = dr1["lic_document"].ToString().Equals("A") ? true : false;
                                        vd.lic_remark = dr1["lic_remark"].ToString();
                                        vd.lic_sp_name = dr1["lic_sp_name"].ToString();
                                        vd.lic_sp_remark = dr1["lic_sp_remark"].ToString();
                                        vd.lic_approve_by = dr1["lic_approve_by"].ToString();
                                        vd.lic_approve_date = dr1["lic_approve_date"].ToString();
                                        vd.lic_deployed = dr1["lic_deployed"].ToString();
                                        vd.lic_appl_code = dr1["lic_appl_code"].ToString();
                                        vd.comn_appl_name = dr1["comn_appl_name_en"].ToString();
                                        vd.comn_mod_name_e = dr1["comn_mod_name_en"].ToString();
                                        vd.comn_mod_code = dr1["comn_mod_code"].ToString();
                                        vd.lic_changeset_no = dr1["lic_changeset_no"].ToString();
                                        vd.is_new_flag = "N";
                                        pn.mogra_details.Add(vd);
                                    }
                                }
                                lst.Add(pn);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUD_SaveMograVersionsDetails")]
        public HttpResponseMessage CUD_SaveMograVersionsDetails(string opr, Mogra_Version obj)
        {
            string res = "true";
            try
            {
                if (opr.Equals("I"))
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                        new List<SqlParameter>() {
                        new SqlParameter("@opr", "IMV"),
                        new SqlParameter("@lic_changeset_no", obj.lic_changeset_no),
                        new SqlParameter("@lic_project_code", obj.lic_project_code),
                        new SqlParameter("@lic_developed_by", obj.lic_developed_by),
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            foreach (Mogra_Version_Details vd in obj.mogra_details)
                            {
                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                                    new List<SqlParameter>() {
                            new SqlParameter("@opr", "IMD"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                                }
                            }
                        }
                        else
                        {
                            res = "exists";
                        }
                    }
                }
                else
                {
                    foreach (Mogra_Version_Details vd in obj.mogra_details)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                            new List<SqlParameter>() {
                            new SqlParameter("@opr", vd.is_new_flag.Equals("Y")?"IMD":"IMU"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            { res = "false"; }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("CUD_MograVersionsDetails")]
        public HttpResponseMessage CUD_MograVersionsDetails(Mogra_Version_Details vd)
        {
            string res = "true";
            try
            {

                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_mogra_verion_details_proc",
                    new List<SqlParameter>() {
                            new SqlParameter("@opr", "DMD"),
                            new SqlParameter("@lic_school_code", vd.lic_school_code),
                            new SqlParameter("@lic_changeset_no", vd.lic_changeset_no),
                            new SqlParameter("@lic_test_case", vd.lic_test_case),
                            new SqlParameter("@lic_document", vd.lic_document),
                            new SqlParameter("@lic_remark", vd.lic_remark),
                            new SqlParameter("@lic_sp_name", vd.lic_sp_name),
                            new SqlParameter("@lic_sp_remark", vd.lic_sp_remark),
                            new SqlParameter("@lic_approve_by", vd.lic_approve_by),
                            new SqlParameter("@lic_approve_date", vd.lic_approve_date),
                            new SqlParameter("@lic_deployed", vd.lic_deployed),
                            new SqlParameter("@lic_appl_code", vd.lic_appl_code),
                            });
                }
            }
            catch (Exception ex)
            { res = "false"; }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }
    }
}