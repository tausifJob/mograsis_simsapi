﻿using log4net;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/ManualFeePosting")]
    [BasicAuthentication]
    public class ManualFeePostingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region API FOR MANUAL FEE POSTING...

        [Route("getRecordPosting")]
        public HttpResponseMessage getRecordPosting(string cur_name, string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();
                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            FinnObj.expected_fee = dr["expected_fee"].ToString();
                            FinnObj.discount = dr["discount"].ToString();
                            FinnObj.fee_paid = dr["fee_paid"].ToString();
                            FinnObj.total = dr["Total"].ToString();
                            FinnObj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            FinnObj.fee_status_desc = dr["fee_status_desc"].ToString();
                            FinnObj.doc_date = dr["doc_date"].ToString();
                            FinnObj.sims_enroll_number = i.ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getRecordReversal")]
        public HttpResponseMessage getRecordReversal(string cur_name, string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "N"),
                              new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();
                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            FinnObj.expected_fee = dr["expected_fee"].ToString();
                            FinnObj.discount = dr["discount"].ToString();
                            FinnObj.fee_paid = dr["fee_paid"].ToString();
                            FinnObj.total = dr["Total"].ToString();
                            FinnObj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            FinnObj.fee_status_desc = dr["fee_status_desc"].ToString();
                            FinnObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            FinnObj.sims_enroll_numberR = i.ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDfeeposting")]
        public HttpResponseMessage CUDfeeposting(List<Finn225> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (Finn225 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[posting_fee_receipt_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@current_year", obj.sims_academic_year),
                            //new SqlParameter("@doc_date",db.UIDDMMYYYYformat(obj.doc_date)),
                            new SqlParameter("@doc_date",db.DBYYYYMMDDformat(obj.doc_date)),
                        });

                        //if (dr.Read())
                        //{
                        //    st = dr["error"].ToString() + dr["date_error"].ToString();
                        //    msg.strMessage = st;
                        //}

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            msg.strMessage = "Fee Poted Successfully...";
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDfeereversal")]
        public HttpResponseMessage CUDfeereversal(List<Finn225> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (Finn225 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[posting_reverse_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@current_year", obj.sims_academic_year),
                            //new SqlParameter("@doc_date",db.UIDDMMYYYYformat(obj.doc_date)),
                            new SqlParameter("@doc_date",db.DBYYYYMMDDformat(obj.doc_date)),
                        });

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    msg.strMessage = st;
                        //}

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            msg.strMessage = "Fee Revesal Successfully...";
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("getSumerry")]
        public HttpResponseMessage getSumerry()
        {
            List<Sims179> lstCuri = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "M"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            sequence.TotalCount = dr["TotalCount"].ToString();
                            lstCuri.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuri);
        }
        #endregion

        #region API FOR MANUAL INVENTORY FEE POSTING...

        [Route("getRecordPostinginventory")]
        public HttpResponseMessage getRecordPostinginventory(string cur_name, string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "Q"),
                              new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();
                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            FinnObj.expected_fee = dr["expected_fee"].ToString();
                            FinnObj.discount = dr["discount"].ToString();
                            FinnObj.fee_paid = dr["fee_paid"].ToString();
                            FinnObj.total = dr["Total"].ToString();
                            FinnObj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            FinnObj.fee_status_desc = dr["fee_status_desc"].ToString();
                            FinnObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            FinnObj.sims_enroll_number = i.ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getRecordReversalinventory")]
        public HttpResponseMessage getRecordReversalinventory(string cur_name, string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "P"),
                              new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();
                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            FinnObj.expected_fee = dr["expected_fee"].ToString();
                            FinnObj.discount = dr["discount"].ToString();
                            FinnObj.fee_paid = dr["fee_paid"].ToString();
                            FinnObj.total = dr["Total"].ToString();
                            FinnObj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            FinnObj.fee_status_desc = dr["fee_status_desc"].ToString();
                            FinnObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            FinnObj.sims_enroll_numberR = i.ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDfeepostinginventory")]
        public HttpResponseMessage CUDfeepostinginventory(List<Finn225> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (Finn225 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[posting_invs_receipts_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@current_year", obj.sims_academic_year),//
                            new SqlParameter("@doc_date",db.DBYYYYMMDDformat(obj.doc_date)),
                            //new SqlParameter("@doc_date",db.UIDDMMYYYYformat(obj.doc_date)),
                        });

                        //if (dr.Read())
                        //{
                        //    st = dr["error"].ToString() + dr["date_error"].ToString();
                        //    msg.strMessage = st;
                        //}

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            msg.strMessage = "Fee Poted Successfully...";
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDfeereversalinventory")]
        public HttpResponseMessage CUDfeereversalinventory(List<Finn225> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                foreach (Finn225 obj in data)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[posting_invs_reverse_receipts_proc]",
                            new List<SqlParameter>() { 
                            new SqlParameter("@current_year", obj.sims_academic_year),
                            new SqlParameter("@doc_date",db.DBYYYYMMDDformat(obj.doc_date)),
                            //new SqlParameter("@doc_date",db.UIDDMMYYYYformat(obj.doc_date)),
                        });

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    msg.strMessage = st;
                        //}

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                            msg.strMessage = "Fee Revesal Successfully...";
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            Finn225 FinnObj = new Finn225();
                            FinnObj.sims_cur_code = dr["com_code"].ToString();
                            FinnObj.sims_cur_name = dr["comn_name"].ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@sims_cur_code",curCode)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["fins_appl_form_field_value1"].ToString();
                            sequence.sims_academic_year_desc = dr["fins_appl_form_field_value3"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        #endregion

        #region API FOR MANUAL FEE POSTING NEW...
        [Route("getMapping")]
        public HttpResponseMessage getMapping(string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "X"),
                              //new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();

                            FinnObj.sims_fee_code = dr["sims_fee_code"].ToString();

                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }




        

        [Route("getRecordPosting_new")]
        public HttpResponseMessage getRecordPosting_new(string academic_year, string from_date, string to_date)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllBanks(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllBanks"));

            List<Finn225> mod_list = new List<Finn225>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (from_date == "undefined" || from_date == "" || from_date == null)
                        from_date = null;
                    if (to_date == "undefined" || to_date == "" || to_date == null)
                        to_date = null;
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_document_document_detail_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "Z"),
                              //new SqlParameter("@sims_cur_code", cur_name),
                              new SqlParameter("@sims_academic_year", academic_year),
                              new SqlParameter("@sims_from_date", db.DBYYYYMMDDformat(from_date)),
                              new SqlParameter("@sims_to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        int i = 0;
                        while (dr.Read())
                        {
                            i = i + 1;
                            Finn225 FinnObj = new Finn225();
                            FinnObj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());

                        //    FinnObj.sims_cur_code = dr["sims_cur_code"].ToString();
                        //    FinnObj.sims_cur_name = dr["sims_cur_name"].ToString();

                            FinnObj.sims_fee_code = dr["sims_fee_code"].ToString();
                            FinnObj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();

                            FinnObj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();

                           // FinnObj.fee_status_desc = dr["fee_status_desc"].ToString();
                            FinnObj.sims_fee_amount = dr["sims_fee_amount"].ToString();
                            FinnObj.fins_dr_acno = dr["fins_dr_acno"].ToString();
                            FinnObj.fins_cr_no = dr["fins_cr_no"].ToString();

                            FinnObj.total_fee_amount = dr["total_fee_amount"].ToString();
                            FinnObj.sims_enroll_number = i.ToString();
                            mod_list.Add(FinnObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        #endregion
    }
}