﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/concession")]
    public class ConcessionApprovalController:ApiController
    {


        [Route("allsims013_Students")]
        public HttpResponseMessage allsims013_Students(string concession_number,sims013 simsobj)
        {
            List<sims013> lstEmp = new List<sims013>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_concession_transaction_proc]",
                        new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "B"),
                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),
                new SqlParameter("@sims_concession_number", concession_number),
                new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims013 obj = new sims013();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_student_name = dr["Student_Full_Name"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            obj.sims_concession_number = dr["sims_concession_number"].ToString();
                            obj.sims_concession_description = dr["sims_concession_description"].ToString();
                            obj.sims_concession_fee_code = dr["sims_concession_fee_code"].ToString();
                            obj.sims_transaction_number = dr["sims_transaction_number"].ToString();
                            obj.sims_Expected_fee = dr["Expected_fee"].ToString();
                            obj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            obj.sims_Concession_amount = dr["Concession_amount"].ToString();
                            obj.sims_parent_number = dr["sims_parent_number"].ToString();
                            obj.parent_name = dr["parent_name"].ToString();
                            obj.sims_student_employee_comp_code_flag = dr["sims_student_employee_comp_code_flag"].ToString().Equals("A") ? true : false;
                            obj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_status"].ToString()) && dr["sims_status"].ToString() == "A")
                                obj.sims_status = false;
                            else
                                obj.sims_status = false;
                            lstEmp.Add(obj);



                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("getConcession_type")]
        public HttpResponseMessage getConcession_type(string sims_fee_academic_year)
        {
            List<sims013> lstEmp = new List<sims013>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_concession_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                new SqlParameter("@sims_fee_academic_year", sims_fee_academic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims013 obj = new sims013();
                            obj.sims_concession_number = dr["sims_concession_number"].ToString();
                            obj.sims_concession_description = dr["sims_concession_description"].ToString();
                            lstEmp.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lstEmp);

        }

        [Route("CUD_sims013_Students")]
        public HttpResponseMessage CUD_sims013_Students(List<sims013> obj)
        {
            bool inserted = false;
            List<sims013> lstEmp = new List<sims013>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims013 simsobj in obj)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.[sims_concession_transaction_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", 'C'),
                    new SqlParameter("@sims_transaction_number", simsobj.sims_transaction_number),
                    new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                    new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),
                    new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                    new SqlParameter("@sims_concession_number", simsobj.sims_concession_number),
                    new SqlParameter("@sims_created_by", simsobj.sims_concession_fee_code),
                    new SqlParameter("@sims_status", simsobj.sims_status == true?"A":"I"),
                    new SqlParameter("@sims_appl_code", simsobj.sims_created_by),
                    new SqlParameter("@sims_concession_from_date",db.DBYYYYMMDDformat(simsobj.sims_concession_from_date)),
                    new SqlParameter("@sims_concession_to_date",db.DBYYYYMMDDformat(simsobj.sims_concession_to_date)),
                    new SqlParameter("@sims_concession_term_code",simsobj.sims_concession_term)

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                       
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }
    }
}