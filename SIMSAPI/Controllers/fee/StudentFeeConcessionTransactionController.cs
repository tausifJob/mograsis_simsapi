﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Newtonsoft.Json;
using SIMSAPI.Models.SIMS.simsClass;
namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/common/ConcessionTransaction")]
    public class StudentFeeConcessionTransactionController : ApiController
    {
        
        [Route("getAllConcessionCode")]
        public HttpResponseMessage getAllConcessionCode(string academic_year)
        {


            List<Sims018> code_list = new List<Sims018>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_fee_academic_year",academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims018 simsObj = new Sims018();
                            simsObj.sims_concession_number = dr["sims_concession_number"].ToString();
                            simsObj.sims_concession_description = dr["sims_concession_description"].ToString();
                            code_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }

        [Route("getAllSimsConcessionTransaction")]
        public HttpResponseMessage getAllSimsConcessionTransaction()
        {

            List<Sims018> transaction_list = new List<Sims018>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims018 simsobj = new Sims018();

                            simsobj.sims_transaction_number = dr["sims_transaction_number"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_short = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_fee_academic_year_desc = dr["sims_fee_academic_year"].ToString();
                            simsobj.sims_fee_academic_year = dr["academic_year"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_full_name_en = dr["Student_Full_Name"].ToString();
                            simsobj.sims_concession_description = dr["sims_concession_description"].ToString();
                            simsobj.sims_concession_number = dr["sims_concession_number"].ToString();
                            //simsobj.sims_concession_from_date = Convert.ToDateTime(dr["sims_academic_year_start_date"]).ToString("dd-MM-yyyy");
                            simsobj.sims_concession_from_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            simsobj.sims_concession_to_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            //simsobj.sims_concession_to_date = Convert.ToDateTime(dr["sims_academic_year_end_date"]).ToString("dd-MM-yyyy");
                            simsobj.sims_created_by = dr["sims_created_by"].ToString();
                            simsobj.sims_created_date = db.UIDDMMYYYYformat(dr["sims_created_date"].ToString());
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.parent_name = dr["parent_name"].ToString();
                            simsobj.sims_student_employee_comp_code = dr["sims_student_employee_comp_code"].ToString();
                            simsobj.sims_student_employee_comp_code_flag = dr["sims_student_employee_comp_code_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_status1 = dr["sims_status"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString() == "Active" || dr["sims_status"].ToString() == "Approved" ? true : false;

                            transaction_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, transaction_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, transaction_list);

        }

        [Route("CUConcessionTransaction")]
        public HttpResponseMessage CUDConcessionTransaction(List<Sims018> obj)
        {

            string inserted = "";

            int cnt = 2;
            try
            {

                foreach (Sims018 simsobj in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                            new List<SqlParameter>() 

                                 { 
                                    new SqlParameter("@opr", simsobj.opr),
                                    new SqlParameter("@sims_transaction_number", simsobj.sims_transaction_number),
                                    new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                    new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),
                                    new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                    new SqlParameter("@sims_concession_number", simsobj.sims_concession_number),
                                    new SqlParameter("@sims_concession_from_date",  db.DBYYYYMMDDformat(simsobj.sims_concession_from_date)),
                                    new SqlParameter("@sims_concession_to_date",db.DBYYYYMMDDformat(simsobj.sims_concession_to_date)),
                                    new SqlParameter("@sims_created_by",  simsobj.sims_created_by),
                                    new SqlParameter("@sims_created_date",  db.DBYYYYMMDDformat(simsobj.sims_created_date)),
                                    new SqlParameter("@sims_status", simsobj.sims_status == true?"A":"I"),
                                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims018_appl_code),
                                    new SqlParameter("@comn_sequence_code", CommonStaticClass.Sims018_sequence_code),
                                    new SqlParameter("@count", cnt),
                                 });
                        if (dr.Read())
                        {

                            inserted = dr["student_fee"].ToString();

                        }
                    }
                }


            }
            catch (Exception x)
            {
                inserted = x.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateConcessionTransaction")]
        public HttpResponseMessage UpdateConcessionTransaction(string data, string opr)
        {
            Sims018 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims018>(data);

            Message message = new Message();
            int cnt = 2;
            try
            {
                if (simsobj != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                            new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr", opr),
                                    new SqlParameter("@sims_transaction_number", simsobj.sims_transaction_number), 
                                    new SqlParameter("@sims_fee_academic_year", simsobj.sims_fee_academic_year),

                                    new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                    new SqlParameter("@sims_concession_number", simsobj.sims_concession_number),
                                    new SqlParameter("@sims_concession_from_date",  db.DBYYYYMMDDformat(simsobj.sims_concession_from_date)),
                                    new SqlParameter("@sims_concession_to_date",  db.DBYYYYMMDDformat(simsobj.sims_concession_to_date)),
                                    new SqlParameter("@sims_created_by", simsobj.sims_created_by),
                                    new SqlParameter("@sims_created_date",  db.DBYYYYMMDDformat(simsobj.sims_created_date)),
                                    new SqlParameter("@sims_status", simsobj.sims_status == true?"A":"I"),
                                    new SqlParameter("@sims_appl_code", CommonStaticClass.Sims018_appl_code),
                                    new SqlParameter("@comn_sequence_code", CommonStaticClass.Sims018_sequence_code),
                                    new SqlParameter("@count", cnt),
                                 });
                        if (dr.RecordsAffected > 0)
                        {
                            if (opr.Equals("U"))
                                message.strMessage = "Concession Transaction Updated Successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {
                            if (opr.Equals("U"))
                                message.strMessage = "Concession Transaction Not Updated";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }

                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (opr.Equals("U"))
                    message.strMessage = "Error In Updating Concession Transaction";
                else if (opr.Equals("I"))
                    message.strMessage = "Error In Inserting Concession Transaction";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("SimsConcessionTransactionDelete")]
        public HttpResponseMessage SimsConcessionTransactionDelete(List<Sims018> data)
        {
          
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        
                        db.Open();

                        foreach(Sims018 simsobj in data)
                        {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'D'),
                                new SqlParameter("@sims_transaction_number", simsobj.sims_transaction_number),
                                new SqlParameter("@sims_cur_code", ""),
                                new SqlParameter("@sims_fee_academic_year", ""),
                                new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (simsobj.opr.Equals("D"))
                                message.strMessage = "Concession Transaction  Deleted Sucessfully";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                            else
                        {
                            if (simsobj.opr.Equals("D"))
                                message.strMessage = "Concession Transaction  Not Deleted";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        dr.Close();
                    }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (data[0].opr.Equals("D"))
                    message.strMessage = "Error In Deleting Concession Transaction";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getComnSequenceCode")]
        public HttpResponseMessage getComnSequenceCode()  //string applcode, string comn_sequence_code
        {
          
            string next_comm_number = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_concession",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'W'),
                                new SqlParameter("@applcode", CommonStaticClass.Sims018_appl_code),
                                new SqlParameter("@sequence_code", CommonStaticClass.Sims018_sequence_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            next_comm_number = dr["comn_number_sequence"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, next_comm_number);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, next_comm_number);
            }
        }

        [Route("getAllCurName")]
        public HttpResponseMessage getAllCurName()
        {


            List<simsClass> cur_list = new List<simsClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();

                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            cur_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, cur_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, cur_list);
            }
        }

        [Route("getAllACAYearCurCode")]
        public HttpResponseMessage getAllACAYearCurCode(string curcode)
        {
            
            List<simsClass> acayear_list = new List<simsClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@sims_cur_code", curcode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_academic_year_start_date"].ToString()))
                                //objNew.sims_academic_year_start_date = DateTime.Parse(dr["sims_academic_year_start_date"].ToString()).ToShortDateString();
                                objNew.sims_academic_year_start_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                                //objNew.sims_academic_year_start_date =  dr["sims_academic_year_start_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_academic_year_end_date"].ToString()))
                                //objNew.sims_academic_year_end_date = DateTime.Parse(dr["sims_academic_year_end_date"].ToString()).ToShortDateString();
                                objNew.sims_academic_year_end_date = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                                //objNew.sims_academic_year_end_date =  dr["sims_academic_year_end_date"].ToString();
                            acayear_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }


         [Route("getAllConcessionDates")]
        public HttpResponseMessage getAllConcessionDates(string academic_year)
        {


            List<Sims018> code_list = new List<Sims018>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Z'),
                                new SqlParameter("@sims_fee_academic_year",academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims018 simsObj = new Sims018();
                            //simsObj.sims_concession_from_date = Convert.ToDateTime(dr["sims_academic_year_start_date"]).ToString("dd-MM-yyyy");
                            simsObj.sims_concession_from_date = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            //simsObj.sims_concession_to_date = Convert.ToDateTime(dr["sims_academic_year_end_date"]).ToString("dd-MM-yyyy");
                            simsObj.sims_concession_to_date =  db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            code_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }


         [Route("getAllConcessionDates")]
         public HttpResponseMessage getAllConcessionDates(Sims018 Simsobj)
         {


             bool inserted = false;

             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                         new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'Y'),
                                new SqlParameter("@sims_concession_number",Simsobj.sims_concession_number),
                                 new SqlParameter("@sims_fee_academic_year",Simsobj.sims_concession_academic_year),
                                  new SqlParameter("@sims_cur_code",Simsobj.sims_cur_short),
                                  new SqlParameter("@sims_enroll_no",Simsobj.sims_enroll_number)

                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             inserted = true;
                         }
                     }
                 }
                
             }
             catch (Exception e)
             {

                 return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }


        [Route("getSearchStudentconcession")]
        public HttpResponseMessage getSearchStudentconcession(string data,string concession_number)
        {
             
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[search_concession_student_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@student_middle_name", comnobj.search_std_middle_name),
                            new SqlParameter("@student_last_name", comnobj.search_std_last_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                            new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@academic_year", comnobj.sims_academic_year),
                            new SqlParameter("@sims_student_ea_number", comnobj.sims_student_ea_number),
                            new SqlParameter("@user_name", comnobj.search_parent_emp_id),
                            new SqlParameter("@sims_std_transport_bus", comnobj.search_std_transport_bus),
                            new SqlParameter("@concession_number",concession_number)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_sname_in_arabic = dr["Name_in_Arabic"].ToString();
                            objNew.s_class = dr["Class"].ToString();
                            objNew.s_parent_id = dr["Parent_Id"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            // objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_student_enroll_number"].ToString();
                            objNew.Teacher_Type = dr["Name_in_English"].ToString();
                            objNew.search_std_image = dr["StudentImg"].ToString();
                            objNew.name = objNew.s_sname_in_english;
                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            try
                            {
                                //objNew.search_mother_first_name = dr["mother_FirstName"].ToString();
                                //objNew.search_mother_last_name = dr["mother_LastName"].ToString();
                                //objNew.search_mother_midd_name = dr["mother_MiddleName"].ToString();
                                objNew.mother_name = dr["mother_name"].ToString();
                                objNew.mother_email_id = dr["mother_email_id"].ToString();
                                objNew.mother_mobile_no = dr["mother_Mobile"].ToString();


                                //objNew.search_guardian_first_name = dr["guardian_FirstName"].ToString();
                                //objNew.search_guardian_last_name = dr["guardian_LastName"].ToString();
                                //objNew.search_guardian_midd_name = dr["guardian_MiddleName"].ToString();
                                objNew.guardian_name = dr["guardian_name"].ToString();
                                objNew.guardian_email_id = dr["guardian_email_id"].ToString();
                                objNew.guardian_mobile_no = dr["guardian_Mobile"].ToString();
                            }
                            catch (Exception ex) { }
                            objNew.sims_parent_emp_id = dr["EmpName"].ToString();
                            objNew.s_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.section_code = dr["sims_section_code"].ToString();
                            objNew.status_name = dr["status_name"].ToString();
                            objNew.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            objNew.cancelled_date = dr["cancelled_date"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                 
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSiblingConcession")]
        public HttpResponseMessage getSiblingConcession(string cur_code, string acad_year, string grade_code, string section_code, string search, string sims_concession_number)
        {

            CommonUserControlClass comnobj = new CommonUserControlClass();
           

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            if (search == "undefined")
            {
                search = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Y'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@section_code", section_code),    
                            new SqlParameter("@search",search),
                             new SqlParameter("@sims_concession_number",sims_concession_number),

                      });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            objNew.student_name = dr["student_name"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            objNew.sims_parent_number = dr["sims_parent_number"].ToString();
                            objNew.parent_name = dr["parent_name"].ToString();
                            
                            search_list.Add(objNew);

                           
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }


        [Route("getAllConcessionCodeSibling")]
        public HttpResponseMessage getAllConcessionCodeSibling(string academic_year)
        {


            List<Sims018> code_list = new List<Sims018>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_concession_transaction_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'K'),
                                new SqlParameter("@sims_fee_academic_year",academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims018 simsObj = new Sims018();
                            simsObj.sims_concession_number = dr["sims_concession_number"].ToString();
                            simsObj.sims_concession_description = dr["sims_concession_description"].ToString();
                            code_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, code_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, code_list);
            }
        }



    }
}