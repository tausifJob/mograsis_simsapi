﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers
{
    [RoutePrefix("api/FeeTerm")]
    public class FeeTermController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        

        [Route("getFeeTerm")]
        public HttpResponseMessage getFeeTerm()
        {
            List<Sims533> result = new List<Sims533>();
            List<SqlParameter> lst = new List<SqlParameter>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_term_Proc]",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims533 obj = new Sims533();


                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_term_code = dr["sims_fee_term_code"].ToString();
                            obj.sims_term_desc_en = dr["sims_fee_term_desc_en"].ToString();
                            obj.sims_term_desc_ar = dr["sims_fee_term_desc_ar"].ToString();
                            obj.sims_term_desc_fr = dr["sims_fee_term_desc_fr"].ToString();
                            obj.sims_term_desc_ot = dr["sims_fee_term_desc_ot"].ToString();
                            obj.sims_term_start_date = db.UIDDMMYYYYformat(dr["sims_fee_term_start_date"].ToString());
                            obj.sims_term_end_date = db.UIDDMMYYYYformat(dr["sims_fee_term_end_date"].ToString());
                            obj.sims_term_status = dr["sims_fee_term_status"].ToString() == "A";
                            obj.sims_term_cur_short_name_en = dr["curname"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("FeeTermCUD")]
        public HttpResponseMessage FeeTermCUD(List<Sims533> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims533 simsobj in data)
                    {
                        
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_fee_term_Proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                          new SqlParameter("@sims_fee_term_code", simsobj.sims_term_code),
                          new SqlParameter("@sims_fee_term_desc_en",simsobj.sims_term_desc_en),
                          new SqlParameter ("@sims_fee_term_desc_ar",simsobj.sims_term_desc_ar),
                          new SqlParameter ("@sims_fee_term_desc_fr",simsobj.sims_term_desc_fr),
                          new SqlParameter ("@sims_fee_term_desc_ot",simsobj.sims_term_desc_ot),
                          new SqlParameter ("@sims_fee_term_start_date",db.DBYYYYMMDDformat(simsobj.sims_term_start_date)),
                          new SqlParameter ("@sims_fee_term_end_date",db.DBYYYYMMDDformat(simsobj.sims_term_end_date)),

                          new SqlParameter("@sims_fee_term_status",simsobj.sims_term_status==true?"A":"I")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAllTerms")]
        public HttpResponseMessage getAllTerms(string curCode)
        {

            Sims533 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims533>(curCode);

            List<Sims533> lstTerms = new List<Sims533>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_term_Proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "R"),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims533 sequence = new Sims533();
                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            sequence.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            sequence.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                            sequence.sims_term_desc_fr = dr["sims_term_desc_fr"].ToString();
                            sequence.sims_term_desc_ot = dr["sims_term_desc_ot"].ToString();
                            sequence.sims_term_status = dr["sims_term_status"].ToString() == "A";
                            //sequence.sims_term_start_date = Convert.ToDateTime(dr["sims_term_start_date"]).ToString("dd-MM-yyyy");
                            sequence.sims_term_start_date = db.UIDDMMYYYYformat(dr["sims_term_start_date"].ToString());
                            //sequence.sims_term_end_date = Convert.ToDateTime(dr["sims_term_end_date"]).ToString("dd-MM-yyyy");
                            sequence.sims_term_end_date = db.UIDDMMYYYYformat(dr["sims_term_end_date"].ToString());
                        lstTerms.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstTerms);
        }


        [Route("getAllStudentInvoice")]
        public HttpResponseMessage getAllStudentInvoice()
        {


            List<student_inv> lstTerms = new List<student_inv>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Student_invoice]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S"),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_inv inv = new student_inv();

                            inv.sims_enroll_number= dr["sims_enroll_number"].ToString();
			                inv.sims_fee_cur_code= dr["sims_fee_cur_code"].ToString();
			                inv.sims_fee_academic_year= dr["sims_fee_academic_year"].ToString();
                            inv.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            inv.student_name = dr["student_name"].ToString();
                            inv.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
			                inv.sims_fee_number= dr["sims_fee_number"].ToString();
			                inv.sims_fee_code= dr["sims_fee_code"].ToString();
			                inv.sims_fee_grade_code= dr["sims_fee_grade_code"].ToString();
			                inv.sims_fee_section_code= dr["sims_fee_section_code"].ToString();
			                inv.expected_amount= dr["expected_amount"].ToString();
			                inv.posted_amount= dr["posted_amount"].ToString();
			                inv.difference_amount= dr["difference_amount"].ToString();
			                inv.sims_fee_period_term= dr["sims_fee_period_term"].ToString();
			                inv.concession= dr["concession"].ToString();
			                inv.sims_grade_name_en= dr["sims_grade_name_en"].ToString();
			                inv.sims_fee_code_description= dr["sims_fee_code_description"].ToString();
			                inv.advance_revenue_acno= dr["advance_revenue_acno"].ToString();
			                inv.fins_revenue_acno= dr["fins_revenue_acno"].ToString();
			                inv.fins_receivable_acno= dr["fins_receivable_acno"].ToString();
			                inv.fins_discount_acno= dr["fins_discount_acno"].ToString();
                        //    inv.expected_amount = dr["expected_amount"].ToString();
                            lstTerms.Add(inv);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstTerms);
        }

        [Route("getAllStudentInvoiceNew")]
        public HttpResponseMessage getAllStudentInvoiceNew(string sims_cur_code, string sims_academic_year, string fins_comp_code, string finance_year, string fee_type,string doc_from_date,
            string doc_to_date,string user)
        {


            List<student_inv> lstTerms = new List<student_inv>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Student_invoice]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S"),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                            new SqlParameter("@sims_academic_year", sims_academic_year),
                            new SqlParameter("@fins_comp_code", fins_comp_code),
                            new SqlParameter("@finance_year", finance_year),
                            new SqlParameter("@sims_fee_type", fee_type),
                            new SqlParameter("@doc_from_date", db.DBYYYYMMDDformat(doc_from_date)),
                            new SqlParameter("@doc_to_date", db.DBYYYYMMDDformat(doc_to_date)),
                            new SqlParameter("@user", user),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_inv inv = new student_inv();

                            inv.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            inv.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            inv.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            inv.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            inv.student_name = dr["student_name"].ToString();
                            inv.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            inv.sims_fee_number = dr["sims_fee_number"].ToString();
                            inv.sims_fee_code = dr["sims_fee_code"].ToString();
                            inv.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            inv.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            inv.expected_amount = dr["expected_amount"].ToString();
                            inv.posted_amount = dr["posted_amount"].ToString();
                            inv.difference_amount = dr["difference_amount"].ToString();
                            inv.sims_fee_period_term = dr["sims_fee_period_term"].ToString();
                            inv.concession = dr["concession"].ToString();
                            inv.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            inv.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            inv.advance_revenue_acno = dr["advance_revenue_acno"].ToString();
                            inv.fins_revenue_acno = dr["fins_revenue_acno"].ToString();
                            inv.fins_receivable_acno = dr["fins_receivable_acno"].ToString();
                            inv.fins_discount_acno = dr["fins_discount_acno"].ToString();
                            //    inv.expected_amount = dr["expected_amount"].ToString();
                            lstTerms.Add(inv);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstTerms);
        }

        [Route("getAcademicYearNew")]
        public HttpResponseMessage getAcademicYearNew(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Student_invoice]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("Student_invoice_generate")]
        public HttpResponseMessage Student_invoice_generate(List<student_inv> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_inv simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[posting_student_invoice]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@comp_code",simsobj.sims_fee_cur_code),
                          new SqlParameter("@fyear",simsobj.sims_fee_academic_year),
                          new SqlParameter("@posting_date", db.DBYYYYMMDDformat(simsobj.inv_date)),
                          new SqlParameter("@enroll_fee_code",simsobj.enroll_no_Feecode),
                          new SqlParameter("@user_name",simsobj.user),
                          new SqlParameter("@doc_code",simsobj.doc_code),
                          new SqlParameter("@doc_date",simsobj.doc_date),
                          new SqlParameter("@finance_year",simsobj.finance_year),
                          new SqlParameter("@fins_comp_code",simsobj.fins_comp_code),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Student_invoice_generate_ASIS")]
        public HttpResponseMessage Student_invoice_generate_ASIS(List<student_inv> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (student_inv simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[posting_student_invoice]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@comp_code", simsobj.fins_comp_code),
                          new SqlParameter("@fyear",simsobj.finance_year),
                          new SqlParameter("@posting_date", db.DBYYYYMMDDformat(simsobj.inv_date)),
                          new SqlParameter("@enroll_fee_code",simsobj.enroll_no_Feecode),
                          new SqlParameter("@user_name",simsobj.user),
                          new SqlParameter("@doc_code",simsobj.doc_code),
                      //    new SqlParameter("@doc_date",simsobj.doc_date),
                          new SqlParameter("@sims_academic_year",simsobj.sims_fee_academic_year),
                          new SqlParameter("@sims_cur_code",  simsobj.sims_fee_cur_code),

                           new SqlParameter("@doc_from_date",db.DBYYYYMMDDformat(simsobj.doc_from_date)),
                            new SqlParameter("@doc_to_date",db.DBYYYYMMDDformat(simsobj.doc_to_date))
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}