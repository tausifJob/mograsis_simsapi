﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/blackList")]
    public class BlackListController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("getblacklist")]
        public HttpResponseMessage getblacklist(string cur_code, string academic_year, string grade_code, string section_code)
        {
            List<Sims_blak_list> result = new List<Sims_blak_list>();
            List<SqlParameter> lst = new List<SqlParameter>();
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_black_list_Proc]",
                     new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims_blak_list obj = new Sims_blak_list();
                            obj.comn_user_name = dr["comn_user_name"].ToString();
                            obj.comn_user_status = dr["comn_user_status"].ToString().Equals("A") ? true : false; 
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.StudentName = dr["StudentName"].ToString();
                            obj.Class = dr["Class"].ToString();
                            obj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            obj.Balance = dr["Balance"].ToString();

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("UpdateBlackList")]
        public HttpResponseMessage UpdateBlackList(List<Sims_blak_list> lst_simsobj)
        {            
            bool inserted = false;            
            try
            {
                if (lst_simsobj != null)
                {

                    using (DBConnection db = new DBConnection())
                    {

                        db.Open();

                        // SqlDataReader dr;
                        foreach (Sims_blak_list simsobj in lst_simsobj)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_black_list_Proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                                new SqlParameter("@sims_sibling_parent_number", simsobj.sims_sibling_parent_number),
                                new SqlParameter("@comn_user_status", simsobj.comn_user_status==true?"A":"I")
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}
