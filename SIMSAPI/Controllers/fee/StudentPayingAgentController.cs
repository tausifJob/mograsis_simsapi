﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Models.FEE;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.StudentPayingAgentController
{
    [RoutePrefix("api/Payagent")]
    [BasicAuthentication]
    public class StudentPayingAgentController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        [Route("getAllPayAgent")]
        public HttpResponseMessage getAllPayingAgent()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllPayingAgent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllPayingAgent"));

            List<Sim531> goaltarget_list = new List<Sim531>();
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_paying_agent_number = dr["sims_paying_agent_number"].ToString();
                            //simsobj.sims_ledger_ac_no = dr["slma_acno"].ToString();
                            simsobj.sims_paying_agent_discount_value = dr["sims_paying_agent_discount_value"].ToString();
                            simsobj.sims_paying_agent_description = dr["sims_paying_agent_description"].ToString();
                            simsobj.sims_paying_agent_slma_ldgr = dr["sims_paying_agent_slma_ldgr"].ToString();
                            simsobj.sims_paying_agent_discount_type = dr["sims_paying_agent_discount_type"].ToString();
                            simsobj.discount_description = dr["dis_description"].ToString();
                            //simsobj.sims_paying_agent_corporate_billing = dr["sims_paying_agent_corporate_billing"].ToString();
                            simsobj.sims_paying_agent_academic_year = dr["sims_paying_agent_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_paying_agent_fee_code = dr["sims_paying_agent_fee_code"].ToString();
                           // simsobj.sims_paying_created_date = Convert.ToDateTime(dr["sims_paying_created_date"]).ToString("dd-MM-yyyy");
                            simsobj.sims_paying_created_date = db.UIDDMMYYYYformat(dr["sims_paying_created_date"].ToString());
                            //simsobj.sims_paying_created_date = dr["sims_paying_created_date"].ToString();
                            simsobj.sims_paying_ldgrctl_code = dr["sims_paying_ldgrctl_code"].ToString();
                            simsobj.fee_description = dr["fee_description"].ToString();
                            simsobj.sims_paying_agent_corporate_billing = dr["sims_paying_agent_corporate_billing"].Equals ("Y") ? true : false;
                            simsobj.sims_paying_agent_receivable_acno_flag = dr["sims_paying_agent_receivable_acno_flag"].Equals("Y") ? true : false;

                            
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<Sim531> lstModules = new List<Sim531>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Z")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 sequence = new Sim531();
                            sequence.sims_sip_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_sip_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getConcessionType")]
        public HttpResponseMessage getConcessionType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getConcessionType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getConcessionType"));

            List<Sim531> goal_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_concession_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_concession_desc = dr["sims_appl_form_field_value1"].ToString(); 
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getDiscountType")]
        public HttpResponseMessage getDiscountType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDiscountType()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getDiscountType"));

            List<Sim531> goal_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_discount_type = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_discount_desc = dr["sims_appl_form_field_value1"].ToString();
                            goal_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, goal_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getConcessApplicableOn")]
        public HttpResponseMessage getConcessApplicableOn()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getConcessApplicableOn()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getConcessApplicableOn"));

            List<Sim531> concess_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            concess_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getAgentNameType")]
        public HttpResponseMessage getAgentNameType(string comp_code,string finance_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getConcessApplicableOn()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getConcessApplicableOn"));

            List<Sim531> concess_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@slma_comp_code", comp_code),
                            new SqlParameter("@sims_academic_year", finance_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_ledger_code = dr["slma_ldgrctl_code"].ToString();
                            simsobj.sims_paying_agent_name_plus_ledger_code = dr["coad_pty_full_name"].ToString() + "/" + dr["slma_ldgrctl_code"].ToString();
                           
                            simsobj.sims_paying_agent_name = dr["coad_pty_full_name"].ToString();
                            simsobj.sims_ledger_ac_no = dr["slma_acno"].ToString();
                            simsobj.sims_paying_agent_name_plus_acno = dr["coad_pty_full_name"].ToString() + "/" + dr["slma_acno"].ToString();
                            
                            concess_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, concess_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("getPayAgentNumber")]
        public HttpResponseMessage getPayAgentNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPayAgentNumber()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getPayAgentNumber"));

            List<Sim531> srno_list = new List<Sim531>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_paying_agent_new_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim531 simsobj = new Sim531();
                            simsobj.sims_paying_agent_number = dr["pay_age_no"].ToString();
                            srno_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, srno_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        [Route("CUDStudentPayingAgent")]
        public HttpResponseMessage CUDStudentPayingAgent(List<Sim531> data)
        {
            
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim531 simsobj in data)
                    {
                        
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_paying_agent_new_proc]",
                            new List<SqlParameter>() 
                         { 

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_paying_agent_number", simsobj.sims_paying_agent_number),
                                new SqlParameter("@sims_paying_agent_description",simsobj.sims_paying_agent_description),
                                new SqlParameter("@sims_paying_agent_slma_ldgr", simsobj.sims_ledger_ac_no),
                                new SqlParameter("@sims_paying_agent_discount_type", simsobj.sims_discount_type),
                                new SqlParameter("@sims_paying_agent_discount_value",simsobj.sims_paying_agent_discount_value),
                                new SqlParameter("@sims_academic_year",simsobj.sims_sip_academic_year),
                                new SqlParameter("@sims_paying_agent_fee_code",simsobj.sims_fee_code),
                                new SqlParameter("@sims_paying_agent_create_date", db.DBYYYYMMDDformat(simsobj.sims_paying_created_date)),
                                new SqlParameter("@sims_paying_ldgrctl_code",simsobj.sims_ledger_code),
                                new SqlParameter("@sims_paying_agent_corporate_billing",simsobj.sims_paying_agent_corporate_billing==true?"Y":"N"),
                                new SqlParameter("@sims_paying_agent_receivable_acno_flag",simsobj.sims_paying_agent_receivable_acno_flag==true ? "Y" : "N")

                    });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}




