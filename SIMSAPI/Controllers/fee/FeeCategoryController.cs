﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/FeeCategory")]
    public class Sim021Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getFeeCategory")]
        public HttpResponseMessage getFeeCategory()
        {
            List<Sims021> feeCategory = new List<Sims021>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims021 simsobj = new Sims021();
                            simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.sims_fee_category_desc = dr["sims_fee_category_description"].ToString();
                            simsobj.sims_category_status = dr["sims_fee_category_status"].ToString().Equals("A") ? true : false;
                            feeCategory.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, feeCategory);
        }

        [Route("getFeeCategoryByIndex")]
        public HttpResponseMessage getFeeCategoryByIndex()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Sims021> result = new List<Sims021>();
            lst.Add(new SqlParameter("@opr", "S"));
            //int total = 0, skip = 0;
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_category_proc]", lst);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims021 simsobj = new Sims021();
                            simsobj.sims_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.sims_fee_category_desc = dr["sims_fee_category_description"].ToString();
                            simsobj.sims_category_status = dr["sims_fee_category_status"].ToString().Equals("A") ? true : false;
                            result.Add(simsobj);

                            // total = result.Count;

                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [Route("updateInsertDeleteFeeCategory")]
        public HttpResponseMessage updateInsertDeleteFeeCategory(List<Sims021> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFeeType()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/FEE/", "FeeCategory"));
            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims021 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_fee_category_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_fee_category", simsobj.sims_fee_category),
                            new SqlParameter("@sims_fee_category_desc", simsobj.sims_fee_category_desc),
                            new SqlParameter("@sims_fee_category_status", simsobj.sims_category_status==true?"A":"I")
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CheckCatCode")]
        public HttpResponseMessage CheckCatCode(string feecategory)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckCatCode(),PARAMETERS :: FEECATEGORY{2}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckCatCode", feecategory));

            bool ifexists = false;
            List<Sims022> feeType = new List<Sims022>();
            Sims022 simsobj = new Sims022();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_category_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                 new SqlParameter("@sims_fee_category", feecategory)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {
                    simsobj.status = true;
                    simsobj.strMessage = "Fee Category Already Exists";
                    feeType.Add(simsobj);
                }
                else
                {
                    simsobj.status = false;
                    simsobj.strMessage = "No Records Found";
                    feeType.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
