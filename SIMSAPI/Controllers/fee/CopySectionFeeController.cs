﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/SectionFees")]
    [BasicAuthentication]
    public class CopySectionFeeController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCurrentAdvanceAY")]
        public HttpResponseMessage getCurrentAdvanceAY(string curcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurrentAdvanceAY(),PARAMETERS :: CURCODE{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getCurrentAdvanceAY", curcode));

            List<Sims036> acayear_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'P'),
                                new SqlParameter("@sims_cur_code", curcode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            acayear_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acayear_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acayear_list);
            }
        }

        [Route("getAllFeeCategoryNames")]
        public HttpResponseMessage getAllFeeCategoryNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCategoryNames(),PARAMETERS  :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCategoryNames"));

            List<Sims036> fee_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'Q'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_category = dr["sims_fee_category"].ToString();
                            simsobj.section_fee_category_name = dr["sims_fee_category_description"].ToString();
                            simsobj.transport_fee_category_name = dr["sims_fee_category_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {
                Log.Error(e);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("CUDSectionFeeDetails")]
        public HttpResponseMessage CUDSectionFeeDetails(string fromstr, string tostr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSectionFeeDetails(),PARAMETERS ::fromstr{2}, tostr {3}";
            Log.Debug(string.Format(debug, "STUDENT", "CUDSectionFeeDetails", fromstr, tostr));
            string isoverwrite = "false";
            copy_section_fee from = Newtonsoft.Json.JsonConvert.DeserializeObject<copy_section_fee>(fromstr);
            copy_section_fee to = Newtonsoft.Json.JsonConvert.DeserializeObject<copy_section_fee>(tostr);
            Message message = new Message();
            try
            {
                if (from != null)
                {
                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_section_fee_proc", new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_fee_cur_code", from.sims_cur_code),
                            new SqlParameter("@sims_fee_academic_year", from.sims_academic_year),
                            new SqlParameter("@sims_fee_grade_code", from.sims_grade_code),
                            new SqlParameter("@sims_fee_section_code", from.sims_section_code),
                            new SqlParameter("@sims_fee_category", from.sims_fee_cat),
                            new SqlParameter("@sims_fee_code", from.sims_fee_code),
                            new SqlParameter("@sims_fee_cur_code_to", to.sims_cur_code),
                            new SqlParameter("@sims_fee_academic_year_to", to.sims_academic_year),
                            new SqlParameter("@sims_fee_grade_code_to", to.sims_grade_code),
                            new SqlParameter("@sims_fee_section_code_to", to.sims_section_code),

                         });

                        if (dr1.Read())
                        {
                            string msg = "Fee Copied Sucessfully ";
                            if (isoverwrite == "false")
                            {

                                string str = dr1[0].ToString();
                                string cnt = dr1[1].ToString();
                                if (cnt == "0")
                                {
                                    msg = "Fee Not Copied";
                                }

                                if (!string.IsNullOrEmpty(str))
                                {
                                    msg = str + "Fees Already Exist";
                                }

                                if (cnt != "0" && !string.IsNullOrEmpty(str))
                                {
                                    msg = "Fee Copied Sucessfully " + str + " " + "Fees Already Exist ";
                                }
                                message.strMessage = msg;
                            }
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        //else
                        //{
                        //    message.strMessage = "Fee Copied Sucessfully";
                        //    message.systemMessage = string.Empty;
                        //    message.messageType = MessageType.Success;
                        //}
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Fee Details";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Copying Fee";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUDSectionFeeDetailsoverwrite")]
        public HttpResponseMessage CUDSectionFeeDetailsoverwrite(List<copy_section_fee> data)
        {

            string inserted = "";
            try
            {

                using (DBConnection db1 = new DBConnection())
                {
                    db1.Open();
                    foreach (copy_section_fee from in data)
                    {
                        SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_section_fee_proc", new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_fee_cur_code", from.sims_cur_code),
                            new SqlParameter("@sims_fee_academic_year", from.sims_academic_year),
                            new SqlParameter("@sims_fee_grade_code", from.sims_grade_code),
                            new SqlParameter("@sims_fee_section_code", from.sims_section_code),
                            new SqlParameter("@sims_fee_category", from.sims_fee_cat),
                            new SqlParameter("@sims_fee_code", from.sims_fee_code),
                            new SqlParameter("@sims_fee_cur_code_to", from.sims_fee_cur_code_to),
                            new SqlParameter("@sims_fee_academic_year_to", from.sims_fee_academic_year_to),
                            new SqlParameter("@sims_fee_grade_code_to", from.sims_fee_grade_code_to),
                            new SqlParameter("@sims_fee_section_code_to", from.sims_fee_section_code_to),

                         });

                        if (dr1.RecordsAffected > 0)
                        {
                            inserted = "Section Fee Updated Successfully";
                        }
                        else if (dr1.Read())
                        {
                            inserted = dr1["student_fee"].ToString();
                        }
                        else
                        {
                            inserted = "Section Fee And Student Fee Not Updated";
                        }
                        dr1.Close();
                    }
                }
            }
            catch (Exception x)
            {
                inserted = "Section Fee And Student Fee Not Updated";
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getSectionFeeByIndex")]
        public HttpResponseMessage getSectionFeeByIndex(string cur_name, string academic_year, string grade_name, string section_name, string fee_category)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFeeByIndex(),PARAMETERS :: CURNAME{2},ACAYEAR{3},GRADENAME{4},SECTIONNAME{5},FEECATEGORY{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getSectionFeeByIndex", cur_name, academic_year, grade_name, section_name, fee_category));

            List<Sims036> fee_list = new List<Sims036>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                                new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                                new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                                new SqlParameter("@sims_fee_number", ""),
                                new SqlParameter("@sims_fee_cur_code", cur_name),
                                new SqlParameter("@sims_fee_academic_year", academic_year),
                                new SqlParameter("@sims_fee_grade_code", grade_name),
                                new SqlParameter("@sims_fee_section_code", section_name),
                                new SqlParameter("@sims_fee_category", fee_category),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_number = dr["sims_fee_number"].ToString();
                            objNew.section_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.section_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.section_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            objNew.section_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.section_fee_category = dr["sims_fee_category"].ToString();
                            objNew.section_fee_code = dr["sims_fee_code"].ToString();
                            objNew.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            objNew.section_fee_code_type = dr["sims_fee_code_type"].ToString();
                            if (dr["sims_fee_code_type"].ToString().Equals("M"))
                            {
                                using (DBConnection db11 = new DBConnection())
                                {
                                    db11.Open();
                                    List<Sims039> subject_list1 = new List<Sims039>();
                                    SqlDataReader dr11 = db11.ExecuteStoreProcedure("sims.sims_section_fee_proc", new List<SqlParameter>()
                                {
                                    new SqlParameter("@opr", 'R'),
                                    new SqlParameter("@section_feenumber", objNew.section_fee_number),
                                });
                                    if (dr11.HasRows)
                                    {
                                        while (dr11.Read())
                                        {
                                            objNew.section_fee_amount = dr11["sims_membership_fee_amount"].ToString();
                                            if (dr11["sims_membership_fee_installment_mode"].ToString().Equals("Y"))
                                                objNew.section_fee_installment_mode = true;
                                            else
                                                objNew.section_fee_refundable = false;
                                            objNew.section_fee_installment_min_amount = dr11["sims_membership_fee_installment_min_amount"].ToString();
                                            if (dr11["sims_membership_fee_status"].ToString().Equals("A"))
                                                objNew.section_fee_status = true;
                                            else
                                                objNew.section_fee_status = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                objNew.section_fee_amount = dr["sims_fee_amount"].ToString();
                                if (dr["sims_fee_installment_mode"].ToString().Equals("Y"))
                                    objNew.section_fee_installment_mode = true;
                                else
                                    objNew.section_fee_installment_mode = false;
                                objNew.section_fee_installment_min_amount = dr["sims_fee_installment_min_amount"].ToString();
                                if (dr["sims_fee_status"].ToString().Equals("A"))
                                    objNew.section_fee_status = true;
                                else
                                    objNew.section_fee_status = false;
                                objNew.section_fee_period1 = dr["sims_fee_period1"].ToString();
                                objNew.section_fee_period2 = dr["sims_fee_period2"].ToString();
                                objNew.section_fee_period3 = dr["sims_fee_period3"].ToString();
                                objNew.section_fee_period4 = dr["sims_fee_period4"].ToString();
                                objNew.section_fee_period5 = dr["sims_fee_period5"].ToString();
                                objNew.section_fee_period6 = dr["sims_fee_period6"].ToString();
                                objNew.section_fee_period7 = dr["sims_fee_period7"].ToString();
                                objNew.section_fee_period8 = dr["sims_fee_period8"].ToString();
                                objNew.section_fee_period9 = dr["sims_fee_period9"].ToString();
                                objNew.section_fee_period10 = dr["sims_fee_period10"].ToString();
                                objNew.section_fee_period11 = dr["sims_fee_period11"].ToString();
                                objNew.section_fee_period12 = dr["sims_fee_period12"].ToString();
                            }
                            objNew.section_fee_code_type_name = dr["FeeCode"].ToString();
                            if (dr["sims_fee_refundable"].ToString().Equals("Y"))
                                objNew.section_fee_refundable = true;
                            else
                                objNew.section_fee_refundable = false;
                            objNew.section_fee_frequency = dr["sims_fee_frequency"].ToString();
                            objNew.section_fee_frequency_name = dr["FeeFrequency"].ToString();
                            if (dr["sims_fee_mandatory"].ToString().Equals("Y"))
                                objNew.section_fee_mandatory = true;
                            else
                                objNew.section_fee_mandatory = false;

                            if (dr["sims_fee_manual_receipt"].ToString().Equals("Y"))
                                objNew.section_fee_manual_receipt = true;
                            else
                                objNew.section_fee_manual_receipt = false;

                            if (dr["sims_fee_split_flag"].ToString().Equals("Y"))
                                objNew.section_fee_split_flag = true;
                            else
                                objNew.section_fee_split_flag = false;

                            fee_list.Add(objNew);


                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("getAllFeeType")]
        public HttpResponseMessage getAllFeeType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeType"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllFeeFrequency")]
        public HttpResponseMessage getAllFeeFrequency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeFrequency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeFrequency"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_frequency = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_frequency_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllFeeCodeType")]
        public HttpResponseMessage getAllFeeCodeType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeCodeType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeCodeType"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code_type = dr["sims_appl_parameter"].ToString();
                            simsobj.section_fee_code_type_name = dr["sims_appl_form_field_value1"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllRemainingFeeCodeType")]
        public HttpResponseMessage getAllRemainingFeeCodeType(string cur_code, string academic_year, string grade_code, string section_code, string fee_category_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRemainingFeeCodeType(),PARAMETERS ::CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECATEGORYCODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllRemainingFeeCodeType", cur_code, academic_year, grade_code, section_code, fee_category_code));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Y"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@gradecode", grade_code),
                            new SqlParameter("@sectioncode", section_code),
                            new SqlParameter("@feecategorycode", fee_category_code),
                       });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        //For SectionFeeCopy Methods

        [Route("getAllGradeSectionNameByIndex")]
        public HttpResponseMessage getAllGradeSectionNameByIndex(string cur_code, string academic_year, string category_code, string section_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradeSectionNameByIndex(),PARAMETERS ::CURCODE{2},ACAYEAR{3},FEECATEGORYCODE{4},SECTIOCCODE{5}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllGradeSectionNameByIndex", cur_code, academic_year, category_code, section_code));
            int total = 0, skip = 0;
            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@sectioncodes",  section_code),
                            new SqlParameter("@categorycode", category_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_section_code_to = dr["sims_section_code"].ToString();
                            objNew.section_fee_section_name_to = dr["sims_section_name_en"].ToString();
                            objNew.section_fee_grade_code_to = dr["sims_grade_code"].ToString();
                            objNew.section_fee_grade_name_to = dr["sims_grade_name_en"].ToString();
                            objNew.overwrite = dr["FeeDefined"].ToString().Equals("A") ? true : false;
                            fee_list.Add(objNew);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllSectionFeeNumbersForUpdate")]
        public HttpResponseMessage getAllSectionFeeNumbersForUpdate(string cur_code, string academic_year, string grade_code, string section_code, string fee_category_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionFeeNumbersForUpdate(),PARAMETERS ::CURCODE{2},ACAYEAR{3},GRADECODE{4},SECTIONCODE{5},FEECATEGORYCODE{6}";
            Log.Debug(string.Format(debug, "STUDENT", "getAllSectionFeeNumbersForUpdate", cur_code, academic_year, grade_code, section_code, fee_category_code));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@curcode", cur_code),
                            new SqlParameter("@academicyear", academic_year),
                            new SqlParameter("@gradecode", grade_code),
                            new SqlParameter("@sectioncode", section_code),
                            new SqlParameter("@feecategorycode", fee_category_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 simsobj = new Sims036();
                            simsobj.section_fee_number = dr["sims_fee_number"].ToString();
                            simsobj.section_fee_code = dr["sims_fee_code"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getAllSectionFees")]
        public HttpResponseMessage getAllSectionFees()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionFees(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllSectionFees"));

            List<Sims036> fee_list = new List<Sims036>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.Sims036_sims_appl_code),
                            new SqlParameter("@sims_appl_form_field_fee_code_type", CommonStaticClass.Sims036_sims_appl_form_field_fee_code_type),
                            new SqlParameter("@sims_appl_form_field_fee_frequency", CommonStaticClass.Sims036_sims_appl_form_field_fee_frequency),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims036 objNew = new Sims036();
                            objNew.section_fee_number = dr["sims_fee_number"].ToString();
                            objNew.section_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            objNew.section_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            objNew.section_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            objNew.section_fee_section_code = dr["sims_fee_section_code"].ToString();
                            objNew.section_fee_category = dr["sims_fee_category"].ToString();
                            objNew.section_fee_code = dr["sims_fee_code"].ToString();
                            objNew.section_fee_code_desctiption = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }
    }
}