﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/FeeType")]
    [BasicAuthentication]
    public class Sim022Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getFeeType")]
        public HttpResponseMessage getFeeType()
        {

            List<Sims022> feeType = new List<Sims022>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_type_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims022 simsobj = new Sims022();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_desc = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_code_status = dr["sims_fee_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.fee_recievable_staus = dr["sims_fee_code_receivable_flag"].ToString().Equals("Y") ? true : false;
                            simsobj.fee_applicable_status = dr["sims_fee_code_late_fee_applicable_flag"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_fee_code_overpaid = dr["sims_fee_code_overpaid"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_section_ob_fee_code = dr["sims_section_ob_fee_code"].ToString();
                            feeType.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getFeeTypeByIndex")]
        public HttpResponseMessage getFeeTypeByIndex()
        {

            List<Sims022> feeType = new List<Sims022>();
            Message message = new Message();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_type_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims022 simsobj = new Sims022();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_desc = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_code_status = dr["sims_fee_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.fee_recievable_staus = dr["sims_fee_code_receivable_flag"].ToString().Equals("Y") ? true : false;
                            simsobj.fee_applicable_status = dr["sims_fee_code_late_fee_applicable_flag"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_fee_code_overpaid = dr["sims_fee_code_overpaid"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_section_ob_fee_code = dr["sims_section_ob_fee_code"].ToString();
                            simsobj.sims_fee_code_vat_percentage = dr["sims_fee_code_vat_percentage"].ToString();
                            try
                            {
                                simsobj.sims_openingbalfee_desc = dr["openingbalfee_desc"].ToString();
                                
                            }
                            catch(Exception ex)
                            {
                            }
                            try
                            {
                                simsobj.sims_fee_Exampted = dr["vat_exempted_flag"].ToString().Equals("Y") ? true : false;
                            }
                            catch (Exception ex)
                            {
                            }
                            feeType.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, feeType);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("CUDFeeType")]
        public HttpResponseMessage CUDFeeType(List<Sims022> data)
        {

            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims022 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_fee_type_proc]",
                                new List<SqlParameter>() 
                            { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
                                new SqlParameter("@sims_fee_code_desc", simsobj.sims_fee_code_desc),
                                new SqlParameter("@sims_section_ob_fee_code", simsobj.sims_section_ob_fee_code),
                                new SqlParameter("@sims_fee_code_status", simsobj.sims_fee_code_status==true?"A":"I"),
                                new SqlParameter("@sims_fee_recievable_status", simsobj.fee_recievable_staus==true?"Y":"N"),
                                new SqlParameter("@sims_late_applicable_status", simsobj.fee_applicable_status==true?"Y":"N"),
                                new SqlParameter("@sims_fee_code_overpaid",simsobj.sims_fee_code_overpaid==true?"Y":"N"),
                                new SqlParameter("@sims_fee_code_vat_percentage", simsobj.sims_fee_code_vat_percentage),
                                new SqlParameter("@vat_exempted_flag", simsobj.sims_fee_Exampted.Equals(true)?"Y":"N"),

                        });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //[Route("CUDFeeType")]
        //public HttpResponseMessage CUDFeeType(Sims022 simsobj)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDFeeType()PARAMETERS ::No";
        //    Log.Debug(string.Format(debug, "ERP/FEE/", "FEETYPE"));

        //   // Sims022 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims022>(data);
        //    Message message = new Message();
        //    try
        //    {
        //        if (simsobj != null)
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                db.Open();
        //                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_type_proc]",
        //                    new List<SqlParameter>() 
        //                 { 
        //                   new SqlParameter("@opr", simsobj.opr),
        //                    new SqlParameter("@sims_fee_code", simsobj.sims_fee_code),
        //                    new SqlParameter("@sims_fee_code_desc", simsobj.sims_fee_code_desc),
        //                    new SqlParameter("@sims_fee_code_status", simsobj.sims_fee_code_status==true?"A":"I")
        //                 });
        //                if (dr.RecordsAffected > 0)
        //                {
        //                    if (simsobj.opr.Equals("U"))
        //                        message.strMessage = "Fee Type Information Updated Sucessfully";
        //                    else if (simsobj.opr.Equals("I"))
        //                        message.strMessage = "Fee Type Information Added Sucessfully";
        //                    else if (simsobj.opr.Equals("D"))
        //                        message.strMessage = "Fee Type Information Deleted Sucessfully";
        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }
        //                if (dr.RecordsAffected < 0)
        //                {
        //                    message.strMessage = "Record Already Mapped.Can't be Deleted";
        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, message);
        //            }
        //        }
        //        else
        //        {
        //            message.strMessage = "Error In Parsing Information!!";
        //            message.messageType = MessageType.Error;
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        if (simsobj.opr.Equals("U"))
        //            message.strMessage = "Error In Updating Fee Type Information";
        //        else if (simsobj.opr.Equals("I"))
        //            message.strMessage = "Error In Adding Fee Type Information";
        //        else if (simsobj.opr.Equals("D"))
        //            message.strMessage = "Error In Deleting Fee Type Information";
        //        message.systemMessage = x.Message;
        //        return Request.CreateResponse(HttpStatusCode.OK, message);
        //    }
        //}

        [Route("CheckTypeCode")]
        public HttpResponseMessage CheckTypeCode(string feecode)
        {

            bool ifexists = false;
            List<Sims022> feeType = new List<Sims022>();
            Sims022 simsobj = new Sims022();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_type_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                 new SqlParameter("@sims_fee_code", feecode)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {

                    simsobj.status = true;
                    simsobj.strMessage = "Fee Code Already Exists";
                    feeType.Add(simsobj);
                }
                else
                {
                    simsobj.status = false;
                    simsobj.strMessage = "No Records Found";
                    feeType.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj);
            }
        }
    }
}