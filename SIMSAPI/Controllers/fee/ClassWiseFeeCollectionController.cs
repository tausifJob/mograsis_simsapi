﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/Classwisefeecollection")]
    public class ClassWiseFeeCollectionController : ApiController
    {


        #region sims550

        [Route("SimsFeeType1")]
        public HttpResponseMessage SimsFeeType1(sims043 obj)
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetFeeTypes",
             new List<SqlParameter>() 
                     {
                new SqlParameter("@opr", "O"),
                new SqlParameter("@feeType", "O"),
                new SqlParameter("@cur_code", obj.std_fee_cur_code),
                new SqlParameter("@academic_year", obj.std_fee_academic_year),
                new SqlParameter("@grade_code", obj.std_fee_grade_code.ToString()),
                new SqlParameter("@section_code", obj.std_fee_section_code.ToString()),
                new SqlParameter("@enrollNo", obj.FeeEnrollNo.ToString())

                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 simsobj = new sims043();
                            simsobj.std_fee_discount_type = dr["sims_fee_code_description"].ToString();
                            simsobj.std_fee_discount_type_code = dr["sims_fee_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        public string AutoGenerate_Fee_Doc_Number(string year)
        {
            string next_comm_number = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[class_wise_fee_collection_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N"),
                new SqlParameter("@academic_year", year),

                });
                    if (dr.Read())
                    {
                        next_comm_number = dr["comn_number_sequence"].ToString();
                    }
                }
            }
            catch (Exception e)
            {
                return next_comm_number;
            }
            return next_comm_number;
        }

        [Route("Getsims550_TermsAcademicYear")]
        public HttpResponseMessage Getsims550_TermsAcademicYear(string curiculum, string academicyear)
        {
            List<Sims550> AcademicYear = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[class_wise_fee_collection_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "A"),
                new SqlParameter("@cur_code",curiculum),
                new SqlParameter("@academic_year" , academicyear)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.sims_term_code = dr["sims_fee_term_code"].ToString();
                            obj.sims_term_desc_en = dr["sims_fee_term_desc_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }


        [Route("Get_sims550Fees")]
        public HttpResponseMessage Get_sims550Fees(string curcode, string ayear, string gradecode, string sectioncode, string termcode)
        {

            List<Sims550> AcademicYear = new List<Sims550>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[class_wise_fee_collection_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "S"),
                new SqlParameter("@cur_code", curcode),
                new SqlParameter("@academic_year", ayear),
                new SqlParameter("@grade_code", gradecode),
                new SqlParameter("@section_code", sectioncode),
                new SqlParameter("@term_code", termcode)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550 obj = new Sims550();
                            obj.sims_cur_code = dr["sims_fee_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_fee_academic_year"].ToString();
                            obj.sims_grade_code = dr["sims_fee_grade_code"].ToString();
                            obj.sims_section_code = dr["sims_fee_section_code"].ToString();
                            obj.sims_term_code = dr["sims_fee_term_code"].ToString();
                            obj.sims_student_Name = dr["StudentName"].ToString();
                            obj.sims_expected_amt = dr["bal_Fee"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_term_code = termcode;
                            obj.sims_student_check_status = false;
                            //obj=FeesDetailsGet_Sims550(obj);
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);

        }

        [Route("FeesDetailsGet_Sims550")]
        public HttpResponseMessage FeesDetailsGet_Sims550(Sims550 obj)
        {

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[class_wise_fee_collection_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "SD"),
                new SqlParameter("@cur_code", obj.sims_cur_code),
                new SqlParameter("@academic_year", obj.sims_academic_year),
                new SqlParameter("@grade_code", obj.sims_grade_code),
                new SqlParameter("@section_code", obj.sims_section_code),
                new SqlParameter("@term_code", obj.sims_term_code),
                new SqlParameter("@search_enroll", obj.sims_enroll_number)
              });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims550_details details = new Sims550_details();
                            details.sims_fee_code = dr["sims_fee_code"].ToString();
                            details.sims_cur_code = dr["sims_fee_cur_code"].ToString();
                            details.sims_academic_year = dr["sims_fee_academic_year"].ToString();
                            details.sims_grade_code = dr["sims_fee_grade_code"].ToString();
                            details.sims_section_code = dr["sims_fee_section_code"].ToString();
                            details.sims_term_code = dr["sims_fee_term_code"].ToString();
                            details.sims_fee_description = dr["sims_fee_code_description"].ToString();
                            details.sims_fee_number = dr["sims_fee_number"].ToString();
                            details.sims_expected_amt = dr["expected_Fee"].ToString();
                            details.sims_paid_amt = dr["paid_Fee"].ToString();
                            details.sims_balance_amt = dr["bal_Fee"].ToString();
                            details.sims_enroll_number = obj.sims_enroll_number;
                            details.isOtherFee = false;
                            details.disabled1 = true;
                            obj.fee_details.Add(details);

                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }


        [Route("update_sims550Fees")]
        public HttpResponseMessage update_Sims550Fees(List<Sims550> data)
        {
            bool value = false;
            string Doc_number = string.Empty;
            List<Sims550_details> lst = new List<Sims550_details>();
            for (int i = 0; i < data.Count; i++)
            {
                Sims550 obj = data[i];
                Doc_number = AutoGenerate_Fee_Doc_Number(obj.sims_academic_year);
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_fee_document",
                            new List<SqlParameter>() 
                         { 
                  
                    new SqlParameter("@opr", "I"),
                    new SqlParameter("@doc_no", Doc_number),
                    new SqlParameter("@doc_date",null),
                    new SqlParameter("@dt_code", "1"),
                    new SqlParameter("@doc_total_amount", obj.sims_expected_amt),
                    new SqlParameter("@doc_status", "1"),
                    new SqlParameter("@enroll_number", obj.sims_enroll_number),
                    new SqlParameter("@doc_other_charge_desc", "0".ToString()),
                    new SqlParameter("@doc_other_charge_amount", "0".ToString()),
                    new SqlParameter("@doc_discount_pct", "0".ToString()),
                    new SqlParameter("@doc_discount_amount", "0".ToString()),
                    new SqlParameter("@doc_ref_code", string.Empty),
                    new SqlParameter("@doc_narration", string.Empty),
                    new SqlParameter("@creation_date",null),
                    new SqlParameter("@creation_user", string.Empty),
                    new SqlParameter("@doc_academic_year", obj.sims_academic_year)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            value = true;
                        }
                        dr.Close();
                        foreach (Sims550_details x in obj.fee_details)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("sims_student_fee",
                                new List<SqlParameter>() 
                         { 
                            
                            new SqlParameter("@opr", "T"),
                            //new SqlParameter("@fee_term", x.sims_term_code),
                            //new SqlParameter("@enrollno", x.sims_enroll_number),
                            //new SqlParameter("@fee_number", x.sims_fee_number),
                            //new SqlParameter("@cur_code", x.sims_cur_code),
                            //new SqlParameter("@academic_year", x.sims_academic_year),
                            //new SqlParameter("@grade_code", x.sims_grade_code),
                            //new SqlParameter("@section_code", x.sims_section_code),
                            //new SqlParameter("@doc_no", Doc_number),
                            //new SqlParameter("@doc_discount_amount", "0".ToString()),
                            //new SqlParameter("@dd_fee_amount_final", x.sims_paid_amt.ToString()),
                            //new SqlParameter("@Payment_mode", x.Payment_mode),
                            //new SqlParameter("@cheque_no", x.sims_cheque_no),
                            //new SqlParameter("@cheque_date", x.sims_cheque_date),
                            //new SqlParameter("@bank_code", x.sims_bank_name),
                            //new SqlParameter("@term_amount", x.sims_expected_amt),
                            //new SqlParameter("@isOtherFee", x.isOtherFee == true ? "Y" : "N")

                            new SqlParameter("@fee_term", x.sims_term_code),
                            new SqlParameter("@enrollno",x.sims_enroll_number),
                            new SqlParameter("@fee_number", x.sims_fee_code.ToString()),
                            new SqlParameter("@cur_code", x.sims_cur_code),
                            new SqlParameter("@academic_year", x.sims_academic_year),
                            new SqlParameter("@grade_code", x.sims_grade_code),
                            new SqlParameter("@section_code", x.sims_section_code),
                            new SqlParameter("@doc_no", Doc_number),
                            new SqlParameter("@dd_fee_amount", x.sims_expected_amt),
                            new SqlParameter("@doc_discount_amount",  "0.00"),
                            new SqlParameter("@dd_fee_amount_final", (decimal.Parse(x.sims_expected_amt)) + ""),
                            new SqlParameter("@Payment_mode", x.Payment_mode),
                            new SqlParameter("@cheque_no", x.sims_cheque_no),
                            new SqlParameter("@cheque_date", db.DBYYYYMMDDformat(x.sims_cheque_date)),
                            new SqlParameter("@bank_code", x.sims_bank_name),
                             new SqlParameter("@isOtherFee", x.isOtherFee == true ? "Y" : "N"),
                            new SqlParameter("@term_amount", x.sims_expected_amt)

                             });
                            if (dr1.RecordsAffected > 0)
                            {
                                Sims550_details obj1 = new Sims550_details();
                                obj1.sims_enroll_number = x.sims_enroll_number;
                                obj1.Doc_number = Doc_number;
                                obj1.sims_expected_amt = x.sims_expected_amt;
                                obj1.inserted = true;
                                lst.Add(obj1);

                            }
                            dr1.Close();
                        }
                    }
                }

                catch (Exception)
                {
                    value = false;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("GetAllBankNames")]
        public HttpResponseMessage GetAllBankNames()
        {
            List<sims043> mod_list = new List<sims043>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[GetStudentFeeDataRefund_Temp_proc]",
             new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims043 obj = new sims043();
                            obj.bank_code = dr["pb_bank_code"].ToString();
                            obj.bank_name = dr["pb_bank_name"].ToString();
                            mod_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }
        #endregion
    }
}