﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.fee
{
    [RoutePrefix("api/ClassWiseFeeReceipt")]
    public class ClassWiseFeeReceiptController : ApiController
    {



        //[Route("getacademic_year")]
        //public HttpResponseMessage getacademic_year()
        //{
        //    List<fee_recipt> com_list = new List<fee_recipt>();
        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr","A")

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    fee_recipt simsobj = new fee_recipt();
        //                    simsobj.academic_year = dr["sims_academic_year"].ToString();
        //                    simsobj.academic_year_name = dr["sims_academic_year_description"].ToString();
        //                    simsobj.academic_year_status = dr["sims_academic_year_status"].ToString();
        //                    com_list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, com_list);

        //}


        [Route("getAcademicYearClass")]
        public HttpResponseMessage getAcademicYearClass()
        {
            List<fee_recipt> list = new List<fee_recipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fee_recipt simsobj = new fee_recipt();
                            simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.academic_year_name = dr["sims_academic_year_description"].ToString();
                            simsobj.academic_year_status = dr["sims_academic_year_status"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("get_grades")]
        public HttpResponseMessage get_grades( string acayr)
        {
            List<fee_recipt> com_list = new List<fee_recipt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                            new SqlParameter("@academic_year",acayr)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fee_recipt simsobj = new fee_recipt();
                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("get_section")]
        public HttpResponseMessage get_section(string acayr, string grade)
        {
            List<fee_recipt> com_list = new List<fee_recipt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","H"),
                            new SqlParameter("@academic_year",acayr),
                            new SqlParameter("@grade_code",grade)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fee_recipt simsobj = new fee_recipt();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            com_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }


        [Route("getAllRecordsFeeReceipt")]
        public HttpResponseMessage getAllRecordsFeeReceipt(string acayr,string from_date, string to_date,string grade,string section, string search)
        {
            List<fee_recipt> com_list = new List<fee_recipt>();
            Message message = new Message();
            try
            {
                if (grade == "undefined")
                {
                    grade = null;
                    from_date = null;
                }
                if (section == "undefined")
                {
                    section = null;
                }
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@academic_year",acayr),
                            new SqlParameter("@grade_code",grade),
                            new SqlParameter("@section_code",section),
                            new SqlParameter("@search",search),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fee_recipt simsobj = new fee_recipt();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.grade_name = dr["grade_name"].ToString()+"-"+dr["section_name"].ToString();
                            //simsobj.section_name = dr["section_name"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.doc_status = dr["doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getAllRecordsFeeReceiptNew")]
        public HttpResponseMessage getAllRecordsFeeReceiptNew(string from_date, string to_date, string search, string doc_no)
        {
            List<Sim615> com_list = new List<Sim615>();
            Message message = new Message();
            try
            {
                if (from_date == "undefined-undefined-undefined" || from_date == "" || from_date == "null")
                {
                    from_date = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_receipt_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@search",search),
                            new SqlParameter("@doc_no",doc_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_academic_year = dr["doc_academic_year"].ToString();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.creation_date = db.UIDDMMYYYYformat(dr["creation_date"].ToString());
                            //if (!string.IsNullOrEmpty(dr["doc_date"].ToString()))
                            //  simsobj.doc_date = DateTime.Parse(dr["doc_date"].ToString()).ToShortDateString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.grand_total = dr["grand_total"].ToString();
                            simsobj.Doc_status = dr["Doc_status"].ToString();
                            com_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, com_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("DocDetailsGet")]
        public HttpResponseMessage DocDetailsGet(Sim615 search)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_fee_receipt_proc_New]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@doc_no",search.doc_no),
                            new SqlParameter("@academic_year",search.doc_academic_year)
                         });
                    ds.DataSetName = "DS";
                    object o = ds;
                    return Request.CreateResponse(HttpStatusCode.OK, o);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("UpdateDoc")]
        public HttpResponseMessage UpdateDoc(List<Dictionary<string, string>> sf)
        {
            object o = null;
            string z = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            List<SqlParameter> sp = new List<SqlParameter>();
            try
            {
                int cnt = 0;
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var v in sf)
                    {
                        sp.Clear();
                        foreach (var p in v.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = v[p];
                            z = v["val"];
                            sp.Add(pr1);
                        }
                        SqlParameter pr0 = new SqlParameter();
                        pr0.ParameterName = "@OPR";
                        pr0.Value = "U";
                        sp.Add(pr0);

                        int r = db.ExecuteStoreProcedureforInsert("sims.sims_fee_receipt_proc_New", sp);
                        cnt = cnt + r;
                    }
                    if (cnt > 0)
                    {
                        using (DBConnection db1 = new DBConnection())
                        {
                            db1.Open();
                            SqlDataReader dr = db1.ExecuteStoreProcedure("sims.sims_fee_receipt_proc_New",
                               new List<SqlParameter>()
                               {
                                new SqlParameter("@opr","F"),
                                new SqlParameter("@val",z)
                               });
                            if (dr.Read())
                            {
                                o = dr[0].ToString();
                                s = HttpStatusCode.OK;
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                //s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("Cashier")]
        public HttpResponseMessage Cashier()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.cashier_code = dr["creation_user"].ToString();
                            simsobj.cashier_name = dr["emp_name"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("PaymentMode")]
        public HttpResponseMessage PaymentMode()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","P")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.payment_mode_code = dr["sims_appl_parameter"].ToString();
                            simsobj.payment_mode_name = dr["sims_appl_form_field_value1"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("FeeType")]
        public HttpResponseMessage FeeType()
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","F")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);

        }

        [Route("FeeCollection1")]
        public HttpResponseMessage FeeCollection1(Sim615 abc)
        {
            List<Sim615> com_list = new List<Sim615>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_fee_collection_summary_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(abc.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(abc.to_date)),
                            new SqlParameter("@Cashier",abc.cashier_code),
                            new SqlParameter("@payment_mode",abc.payment_mode_code),
                              new SqlParameter("@search",abc.enroll_number)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim615 simsobj = new Sim615();
                            simsobj.doc_no = dr["doc_no"].ToString();
                            simsobj.doc_date = db.UIDDMMYYYYformat(dr["doc_date"].ToString());
                            simsobj.enroll_number = dr["enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.doc_reference_no = dr["doc_reference_no"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            simsobj.cheque_number = dr["cheque_number"].ToString();
                            simsobj.dd_fee_cheque_date = db.UIDDMMYYYYformat(dr["dd_fee_cheque_date"].ToString());
                            simsobj.bank_name = dr["bank_name"].ToString();
                            simsobj.fee_paid = dr["fee_paid"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("FeeCollection")]
        public HttpResponseMessage FeeCollection(Sim615 abc)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[sims_fee_collection_summary_proc]",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(abc.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(abc.to_date)),
                            new SqlParameter("@Cashier",abc.cashier_code),
                            new SqlParameter("@payment_mode",abc.payment_mode_code),
                              new SqlParameter("@search",abc.enroll_number)
                         });

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
        }


    }
}