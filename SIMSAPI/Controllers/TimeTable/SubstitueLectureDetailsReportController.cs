﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.TimeTable
{
    [RoutePrefix("api/lecturedetails")]
    public class SubstitueLectureDetailsReportController : ApiController
    {
        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            sequence.sims_academic_year_status = dr["sims_academic_year_status"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getbell_code")]
        public HttpResponseMessage getbell_code(string academic_year)
        {
            List<SLDR01> lstModules = new List<SLDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_SubstitueLectureDetailsReportproc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@aca_year",academic_year)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLDR01 sequence = new SLDR01();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllteacher")]
        public HttpResponseMessage getAllteacher(string academic_year,string bell_code)
        {
            List<SLDR01> lstModules = new List<SLDR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_SubstitueLectureDetailsReportproc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@aca_year",academic_year),
                            new SqlParameter("@bell_code",bell_code)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLDR01 sequence = new SLDR01();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            sequence.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSubstitueLectureDetailsReport")]
        public HttpResponseMessage getSubstitueLectureDetailsReport( string mom_start_date, string mom_end_date, string bell_code, string teacher_code,string report_status)
        {
            List<SLDR01> lstmeetingcode = new List<SLDR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_SubstitueLectureDetailsReportproc]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@st_date", db.DBYYYYMMDDformat(mom_start_date)),
                                     new SqlParameter("@end_date", db.DBYYYYMMDDformat(mom_end_date)),
                                     new SqlParameter("@bell_code",bell_code),
                                     new SqlParameter("@teacher_code", teacher_code),
                                     new SqlParameter("@status", report_status),



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLDR01 sequence = new SLDR01();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.current_teacher_code = dr["current_teacher_code"].ToString();
                            sequence.current_emp_code = dr["current_emp_code"].ToString();
                            sequence.current_teacher_name = dr["current_teacher_name"].ToString();
                            sequence.current_subject_name = dr["current_subject_name"].ToString();
                            sequence.sub_emp_code = dr["sub_emp_code"].ToString();
                            sequence.sub_teacher_code = dr["sub_teacher_code"].ToString();
                            sequence.sub_teacher_name = dr["sub_teacher_name"].ToString();
                            sequence.sub_subject_name = dr["sub_subject_name"].ToString();
                            sequence.day_date = dr["day_date"].ToString();
                            sequence.sims_bell_substitution_date = dr["sims_bell_substitution_date"].ToString();
                            sequence.time =dr["time"].ToString();
                            sequence.sims_bell_slot_desc = dr["sims_bell_slot_desc"].ToString();
                            sequence.class1 = dr["class1"].ToString();
                            lstmeetingcode.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstmeetingcode);
        }


        [Route("getSubstitutionfromDetails")]
        public HttpResponseMessage getSubstitutionfromDetails(string bell_code, string cur_teacher, string sub_teacher,  string date)
        {
            List<SLDR01> lstmeetingcode = new List<SLDR01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_SubstitueLectureDetailsReportproc]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "C"),
                                     new SqlParameter("@bell_code", bell_code),
                                     new SqlParameter("@teacher_code", cur_teacher),
                                     new SqlParameter("@subst_teacher",sub_teacher),
                                     new SqlParameter("@st_date", date),
                                     



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLDR01 sequence = new SLDR01();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.current_teacher_code = dr["current_teacher_code"].ToString();
                            sequence.current_emp_code = dr["current_emp_code"].ToString();
                            sequence.current_teacher_name = dr["current_teacher_name"].ToString();
                            sequence.current_subject_name = dr["current_subject_name"].ToString();
                            sequence.sub_emp_code = dr["sub_emp_code"].ToString();
                            sequence.sub_teacher_code = dr["sub_teacher_code"].ToString();
                            sequence.sub_teacher_name = dr["sub_teacher_name"].ToString();
                            sequence.sub_subject_name = dr["sub_subject_name"].ToString();
                            sequence.day_date = dr["day_date"].ToString();
                            sequence.sims_bell_substitution_date = dr["sims_bell_substitution_date"].ToString();
                            sequence.time = dr["time"].ToString();
                            sequence.sims_bell_slot_desc = dr["sims_bell_slot_desc"].ToString();
                            sequence.class1 = dr["class1"].ToString();
                            sequence.remark = dr["remark"].ToString();
                            sequence.approved_by = dr["approved_by"].ToString();
                            lstmeetingcode.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstmeetingcode);
        }


    }

}