﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.BellConfig
{
    [RoutePrefix("api/PreferenceController")]
    public class PreferenceController : ApiController
    {

        [Route("ImportGrades")]
        public HttpResponseMessage ImportGrades(string aca_year, string bell_code)
        {
            List<GradeSecObj> grade_list = new List<GradeSecObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'G'),
                                new SqlParameter("@aca_year",aca_year),
                                new SqlParameter("@bell_code",bell_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GradeSecObj simsobj = new GradeSecObj();
                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("DaySlotBreakDetail")]
        public HttpResponseMessage DaySlotBreakDetail(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_preference]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }



        [Route("fetchAllSubject")]
        public HttpResponseMessage fetchAllSubject(Dictionary<string, string> sf)
        {
            List<SubjectObj> sub_list = new List<SubjectObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubjectObj simsobj = new SubjectObj();
                            simsobj.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_color = dr["sims_subject_color"].ToString();
                            simsobj.sims_subject_type = dr["sims_subject_type"].ToString();
                            // simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_subject_preference_code = dr["sims_subject_preference_code"].ToString();
                            sub_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sub_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("fetchAllTeacher")]
        public HttpResponseMessage fetchAllTeacher(Dictionary<string, string> sf)
        {
            List<TeachertObj> teacher_list = new List<TeachertObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TeachertObj simsobj = new TeachertObj();
                            simsobj.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.teacher_name = dr["sims_teacher_name"].ToString();
                            teacher_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, teacher_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("SaveTeacherPreference")]
        public HttpResponseMessage SaveTeacherPreference(List<PreObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@aca_year",data[0].aca_year),
                        new SqlParameter("@bell_code",data[0].bell_code)
                    });

                    foreach (PreObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'I'),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@bell_code",obj.bell_code),
                                new SqlParameter("@grade_code",obj.grade_code),
                                new SqlParameter("@sec_code",obj.sec_code),
                                new SqlParameter("@day_code",obj.day_code),
                                new SqlParameter("@slot_code",obj.slot_code),
                                new SqlParameter("@teacher_code",obj.teacher_code),
                                new SqlParameter("@preference",obj.preference)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("SaveSubjectPreference")]
        public HttpResponseMessage SaveSubjectPreference(List<PreObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (PreObj obj in data)
                    {
                        int inst = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@aca_year",obj.aca_year),
                            new SqlParameter("@bell_code",obj.bell_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                            new SqlParameter("@sec_code",obj.sec_code),
                            new SqlParameter("@day_code",obj.day_code),
                            new SqlParameter("@slot_code",obj.slot_code),
                            new SqlParameter("@sub_code",obj.sub_code),
                        });

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'C'),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@bell_code",obj.bell_code),
                                new SqlParameter("@grade_code",obj.grade_code),
                                new SqlParameter("@sec_code",obj.sec_code),
                                new SqlParameter("@day_code",obj.day_code),
                                new SqlParameter("@slot_code",obj.slot_code),
                                new SqlParameter("@sub_code",obj.sub_code),
                                new SqlParameter("@preference",obj.preference)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("SubjectAllocationPreference")]
        public HttpResponseMessage SubjectAllocationPreference(List<PreObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int del = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "SD"),
                                new SqlParameter("@cur_code",data[0].cur_code),
                                new SqlParameter("@aca_year",data[0].aca_year),
                                new SqlParameter("@bell_code",data[0].bell_code),
                                new SqlParameter("@grade_code",data[0].grade_code),
                                new SqlParameter("@sec_code",data[0].sec_code),
                                new SqlParameter("@sub_code",data[0].sub_code)
                        });

                    foreach (PreObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "SI"),
                                new SqlParameter("@cur_code",obj.cur_code),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@bell_code",obj.bell_code),
                                new SqlParameter("@grade_code",obj.grade_code),
                                new SqlParameter("@sec_code",obj.sec_code),
                                new SqlParameter("@day_code",obj.day_code),
                                new SqlParameter("@slot_code",obj.slot_code),
                                new SqlParameter("@sub_code",obj.sub_code),
                                new SqlParameter("@preference",obj.preference)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("RemovePreference")]
        public HttpResponseMessage RemovePreference(Dictionary<string, string> sf)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("saveRules")]
        public HttpResponseMessage saveRules(Dictionary<string, string> sf)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("fetchSubjectPreference")]
        public HttpResponseMessage fetchSubjectPreference(Dictionary<string, string> sf)
        {
            List<PreObj> subpre_list = new List<PreObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PreObj simsobj = new PreObj();
                            simsobj.sub_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sub_color = dr["sims_subject_color"].ToString();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sec_code = dr["sims_bell_section_code"].ToString();
                            simsobj.sub_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.slot_code = dr["sims_bell_slot_code"].ToString();
                            simsobj.preference = dr["slot_level_pre"].ToString();

                            subpre_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, subpre_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("fetchTeacherPreference")]
        public HttpResponseMessage fetchTeacherPreference(Dictionary<string, string> sf)
        {
            List<PreObj> teacherpre_list = new List<PreObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PreObj simsobj = new PreObj();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sec_code = dr["sims_bell_section_code"].ToString();
                            simsobj.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.slot_code = dr["sims_bell_slot_code"].ToString();
                            simsobj.preference = dr["sims_bell_preference_code"].ToString();
                            teacherpre_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, teacherpre_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }



        [Route("TimeTableRules")]
        public HttpResponseMessage TimeTableRules(Dictionary<string, string> sf)
        {
            List<PreObj> teacherpre_list = new List<PreObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PreObj simsobj = new PreObj();
                            simsobj.sims_bell_appl_form_field = dr["sims_bell_appl_form_field"].ToString();
                            simsobj.sims_bell_appl_parameter = dr["sims_bell_appl_parameter"].ToString();

                            teacherpre_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, teacherpre_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }


        [Route("schoolLevelSubPref")]
        public HttpResponseMessage schoolLevelSubPref(List<PreObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (PreObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'N'),
                                new SqlParameter("@sub_code",obj.sub_code),
                                new SqlParameter("@preference",obj.preference)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("TeacherAllocationPreference")]
        public HttpResponseMessage TeacherAllocationPreference(List<PreObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int del = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "BB"),
                                new SqlParameter("@aca_year",data[0].aca_year),
                                new SqlParameter("@bell_code",data[0].bell_code),
                                new SqlParameter("@teacher_code",data[0].teacher_code),
                                new SqlParameter("@cur_code",data[0].cur_code)

                        });

                    foreach (PreObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_preference]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "AB"),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@bell_code",obj.bell_code),
                                new SqlParameter("@cur_code",obj.cur_code),
                                new SqlParameter("@teacher_code",obj.teacher_code),
                                new SqlParameter("@day_code",obj.day_code),
                                new SqlParameter("@slot_code",obj.slot_code),
                                new SqlParameter("@preference",obj.preference)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("savedTeacherAlloPreference")]
        public HttpResponseMessage savedTeacherAlloPreference(Dictionary<string, string> sf)
        {
            List<PreObj> subpre_list = new List<PreObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_preference]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            PreObj simsobj = new PreObj();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.slot_code = dr["sims_bell_slot_code"].ToString();
                            simsobj.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.preference = dr["sims_bell_preference_code"].ToString();

                            subpre_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, subpre_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

    }
}

