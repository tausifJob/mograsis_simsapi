﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.BellConfig
{
    [RoutePrefix("api/BellConfig")]
    public class BellConfigController : ApiController
    {

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {

            List<AcaObj> acaYear = new List<AcaObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_config]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'A')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AcaObj simsobj = new AcaObj();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();
                            simsobj.aca_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.aca_year_status = dr["sims_academic_year_status"].ToString();
                            acaYear.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acaYear);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acaYear);
            }
        }

        [Route("getAllGradesSec")]
        public HttpResponseMessage getAllGradesSec(string aca_year)
        {

            List<GradeSecObj> grade_list = new List<GradeSecObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_config]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'N'),
                                new SqlParameter("@aca_year",aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GradeSecObj simsobj = new GradeSecObj();
                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllGradesSecNew")]
        public HttpResponseMessage getAllGradesSecNew(string aca_year, string cur_code)
        {
            //string cur_code = string.Empty, str = string.Empty;
                List<GradeSecObj> mod_list = new List<GradeSecObj>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_config]",
                         new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'N'),
                                new SqlParameter("@aca_year",aca_year),
                                new SqlParameter("@cur_code",cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            GradeSecObj simsobj = new GradeSecObj();
                            str1 = dr["sims_grade_code"].ToString();
                            var v = from p in mod_list where p.grade_code == str1 select p;

                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();

                            simsobj.section = new List<SectionObj>();

                            SectionObj h = new SectionObj();

                            h.sims_section_code = dr["sims_section_code"].ToString();
                            h.sims_section_name = dr["sims_section_name_en"].ToString();
                            h.status = dr["sims_bell_status"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.section.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).section.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }


        [Route("BellCongifDataUpdate")]
        public HttpResponseMessage BellCongifDataUpdate(char cTemp, String bell_code, String aca_year, String cur_code , List<BellConfigObj> data)
        {
            bool inserted = true;
            List<BellGradeObj> returndata = new List<BellGradeObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_config]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'C'),
                                new SqlParameter("@aca_year",aca_year),
                                new SqlParameter("@bell_desc",data[0].bell_name),
                                new SqlParameter("@bell_code",bell_code)
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                BellGradeObj obj = new BellGradeObj();
                                
                                bell_code = dr["sims_bell_code"].ToString();
                                obj.curcode = dr["sims_cur_code"].ToString();
                                obj.bell_code = dr["sims_bell_code"].ToString();
                                obj.bell_name = dr["sims_bell_desc"].ToString();
                                obj.aca_year = dr["sims_bell_academic_year"].ToString();
                                obj.aca_year_desc = dr["sims_academic_year_description"].ToString();
                                obj.bell_status = dr["sims_bell_status"].ToString();
                                returndata.Add(obj);
                            }
                        }
                        dr.Close();
                    cTemp = 'U';
                    foreach (BellGradeObj simsobj in data[0].bellGrade)
                    {
                        foreach (SectionObj obj in simsobj.section)
                        {
                            using (DBConnection db1 = new DBConnection())
                            {
                                int ins = db1.ExecuteStoreProcedureforInsert("[sims].[sims_bell_config]",
                                new List<SqlParameter>()
                                {
                                  new SqlParameter("@opr",'D'),
                                  new SqlParameter("@aca_year",aca_year),
                                  new SqlParameter("@bell_code",bell_code),
                                  new SqlParameter("@cur_code",cur_code),
                                  new SqlParameter("@grade_code",simsobj.grade_code),
                                  new SqlParameter("@sec_code",obj.sims_section_code),
                                  new SqlParameter("@status",obj.status)
                                });

                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                                cTemp = 'I';
                            }
                        }
                    }
                    cTemp = 'U';
                    foreach (BellDayObj simsobj in data[0].bellDay)
                    {
                        foreach (BellLectureObj obj in simsobj.lecture)
                        {
                            using (DBConnection db2 = new DBConnection())
                            {
                                int ins = db2.ExecuteStoreProcedureforInsert("[sims].[sims_bell_config]",
                                new List<SqlParameter>()
                            {
                              new SqlParameter("@opr",'O'),
                              new SqlParameter("@cTemp",cTemp),
                              new SqlParameter("@aca_year",aca_year),
                              new SqlParameter("@bell_code",bell_code),
                              new SqlParameter("@day_code",simsobj.day_code),
                              new SqlParameter("@isBreak",obj.isBreak),
                              new SqlParameter("@slot_code",obj.slot_code),
                              new SqlParameter("@start_time",obj.from_time),
                              new SqlParameter("@end_time",obj.to_time),
                              new SqlParameter("@slot_desc",obj.lect_desc)

		                    });
                                if (ins > 0)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                                cTemp = 'I';
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, returndata);
        }


        [Route("getBellData")]
        public HttpResponseMessage getBellData()
        {

            List<BellObj> oBell = new List<BellObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_config", new List<SqlParameter>() { 
                        new SqlParameter("@OPR",'E')
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            BellObj simsobj = new BellObj();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            simsobj.aca_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.bell_name = dr["sims_bell_desc"].ToString();
                            simsobj.curcode = dr["sims_cur_code"].ToString();
                            simsobj.curname = dr["sims_cur_short_name_en"].ToString();
                            oBell.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, oBell);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, oBell);
            }
        }

        [Route("getSavedBellDetailData")]
        public HttpResponseMessage getSavedBellData(String aca_year, String bell_code)
        {

            List<BellDayObj> oBell = new List<BellDayObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_config", new List<SqlParameter>() {
                        new SqlParameter("@opr",'F'),
                        new SqlParameter("@aca_year",aca_year),
                        new SqlParameter("@bell_code",bell_code),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            BellDayObj simsobj = new BellDayObj();
                            str1 = dr["sims_bell_day_code"].ToString();
                            var v = from p in oBell where p.day_code == str1 select p;
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.lecture = new List<BellLectureObj>();

                            BellLectureObj h = new BellLectureObj();
                            h.slot_code = dr["sims_bell_slot_code"].ToString();
                            h.from_time = dr["sims_bell_start_time"].ToString();
                            h.to_time = dr["sims_bell_end_time"].ToString();
                            h.isBreak = dr["sims_bell_break"].ToString();
                            h.lect_desc = dr["sims_bell_slot_desc"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.lecture.Add(h);
                                oBell.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).lecture.Add(h);

                            }
                        }
                      
                    }

                   
                }
                return Request.CreateResponse(HttpStatusCode.OK, oBell);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, oBell);
            }
        }


        [Route("getSavedBellSecData")]
        public HttpResponseMessage getSavedBellSecData(String aca_year, String bell_code, String cur_code)
        {
            //string cur_code = string.Empty, str = string.Empty;
            List<GradeSecObj> mod_list = new List<GradeSecObj>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_config", new List<SqlParameter>() {
                        new SqlParameter("@opr",'G'),
                        new SqlParameter("@aca_year",aca_year),
                        new SqlParameter("@bell_code",bell_code),
                        new SqlParameter("@cur_code",cur_code),
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            GradeSecObj simsobj = new GradeSecObj();
                            str1 = dr["sims_grade_code"].ToString();
                            var v = from p in mod_list where p.grade_code == str1 select p;

                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();

                            simsobj.section = new List<SectionObj>();

                            SectionObj h = new SectionObj();

                            h.sims_section_code = dr["sims_section_code"].ToString();
                            h.sims_section_name = dr["sims_section_name_en"].ToString();
                            h.status = dr["sims_bell_status"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.section.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).section.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        } // End of getSavedBellSecData


        [Route("CUDBellDetailSectionData")]
        public HttpResponseMessage CUDBellDetailSectionData(string aca_year, string bell_code)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_bell_config", new List<SqlParameter>() { 
                        new SqlParameter("@opr",'B'),
                        new SqlParameter("@aca_year",aca_year),
                        new SqlParameter("@bell_code",bell_code),
                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, true);
            }
        }

        public string bell_name { get; set; }

        public string curr_code { get; set; }

        public string curcode { get; set; }
    }
}

