﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.TimeTable
{
    [RoutePrefix("api/TimeTableNew")]
    public class TimeTableNewdController : ApiController
    {
        [Route("TeachersLecture")]
        public HttpResponseMessage TeachersLecture(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.TeachersLecture",sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TeachersLecture_DMC")]
        public HttpResponseMessage TeachersLecture_DMC(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.TeachersLecture_DMC", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CUDTeachersLecture")]
        public HttpResponseMessage CUDTeachersLecture(char cTemp,List<teacherLecture> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (teacherLecture simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[insertTeacherLecture]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@sims_bell_sr_no",simsobj.sims_bell_sr_no),
                          new SqlParameter("@cur_code",simsobj.cur_code),
                          new SqlParameter("@teacher_code",simsobj.teacher_code),
                          new SqlParameter("@grade_code",simsobj.grade_code),
                          new SqlParameter("@section_code",simsobj.section_code),
                          new SqlParameter("@subject_code",simsobj.subject_code),
                          new SqlParameter("@day_code",simsobj.day_code),
                          new SqlParameter("@slot_code",simsobj.slot_code),
                          new SqlParameter("@aca_year",simsobj.aca_year)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        
        [Route("CUDTeachersLecture_DMC")]
        public HttpResponseMessage CUDTeachersLecture_DMC(char cTemp,string term_code, List<teacherLecture> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            bool inserted = false;
            try
            {
                if(term_code == "undefined" || term_code == "")
                    term_code = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (teacherLecture simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[insertTeacherLecture_DMC]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@sims_bell_sr_no",simsobj.sims_bell_sr_no),
                          new SqlParameter("@cur_code",simsobj.cur_code),
                          new SqlParameter("@teacher_code",simsobj.teacher_code),
                          new SqlParameter("@grade_code",simsobj.grade_code),
                          new SqlParameter("@section_code",simsobj.section_code),
                          new SqlParameter("@subject_code",simsobj.subject_code),
                          new SqlParameter("@day_code",simsobj.day_code),
                          new SqlParameter("@slot_code",simsobj.slot_code),
                          new SqlParameter("@aca_year",simsobj.aca_year),
                          new SqlParameter("@term_code",term_code)
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("TimeTableImport")]
        public HttpResponseMessage TimeTableImport(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[TimeTableData]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }


    }
}
