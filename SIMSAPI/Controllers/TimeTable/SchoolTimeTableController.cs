﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.SchoolTimeTable
{
    [RoutePrefix("api/SchoolTimeTable")]
    public class SchoolTimeTableController : ApiController
    {

        [Route("TimeTableCommon")]
        public HttpResponseMessage TimeTableCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[TimeTableData]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }



        [Route("TimeTableInitial")]
        public HttpResponseMessage TimeTableInitial(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[TimeTableData]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TimeTableImport")]
        public HttpResponseMessage TimeTableImport(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[TimeTableData]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TimeTableData")]
        public HttpResponseMessage TimeTableData(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<tTimeTableObj> oTimeTable = new List<tTimeTableObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_timetable_data]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "", str2 = "";
                            tTimeTableObj simsobj = new tTimeTableObj();
                            str1 = dr["sims_bell_grade_code"].ToString();
                            str2 = dr["sims_bell_section_code"].ToString();
                            var v = from p in oTimeTable where (p.grade_code == str1 && p.sec_code == str2) select p;
                            simsobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.sec_code = dr["sims_bell_section_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sec_name = dr["sims_section_name_en"].ToString();
                            simsobj.gradeSec = new List<ttGradeSecObj>();

                            ttGradeSecObj h = new ttGradeSecObj();
                            h.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            h.subject_code = dr["sims_bell_subject_code"].ToString();
                            h.day_code = dr["sims_bell_day_code"].ToString();
                            h.slot_code = dr["sims_bell_slot_code"].ToString();
                            h.teacher_name = dr["sims_teacher_name"].ToString();
                            h.sub_name = dr["sims_subject_name_en"].ToString();
                            h.sub_color = dr["sims_subject_color"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.gradeSec.Add(h);
                                oTimeTable.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).gradeSec.Add(h);

                            }
                        }
                        s = HttpStatusCode.OK;
                        o = oTimeTable;
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TimeTableConflictData")]
        public HttpResponseMessage TimeTableConflictData(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<tTimeTableObj> cTimeTable = new List<tTimeTableObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_timetable_conflict_data]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "", str2 = "";
                            tTimeTableObj simscdobj = new tTimeTableObj();
                            str1 = dr["sims_bell_grade_code"].ToString();
                            str2 = dr["sims_bell_section_code"].ToString();
                            var v = from p in cTimeTable where (p.grade_code == str1 && p.sec_code == str2) select p;

                            simscdobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simscdobj.sec_code = dr["sims_bell_section_code"].ToString();
                            simscdobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simscdobj.sec_name = dr["sims_section_name_en"].ToString();
                            simscdobj.gradeSec = new List<ttGradeSecObj>();

                            ttGradeSecObj cdata = new ttGradeSecObj();
                            cdata.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            cdata.subject_code = dr["sims_bell_subject_code"].ToString();
                            cdata.day_code = dr["sims_bell_day_code"].ToString();
                            cdata.slot_code = dr["sims_bell_slot_code"].ToString();
                            cdata.teacher_name = dr["sims_teacher_name"].ToString();
                            cdata.sub_name = dr["sims_subject_name_en"].ToString();
                            cdata.sub_color = dr["sims_subject_color"].ToString();

                            if (v.Count() == 0)
                            {
                                simscdobj.gradeSec.Add(cdata);
                                cTimeTable.Add(simscdobj);
                            }
                            else
                            {
                                v.ElementAt(0).gradeSec.Add(cdata);

                            }
                        }
                        s = HttpStatusCode.OK;
                        o = cTimeTable;
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TimeTableDatasave")]
        public HttpResponseMessage TimeTableDatasave(List<TimeTableObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@AcaYear",data[0].aca_year),
                        new SqlParameter("@bellCode",data[0].bell_code),
                    });

                    foreach (TimeTableObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@AcaYear",obj.aca_year),
                            new SqlParameter("@bellCode",obj.bell_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                            new SqlParameter("@sec_code",obj.sec_code),
                            new SqlParameter("@day_code",obj.day_code),
                            new SqlParameter("@slot_code",obj.slot_code),
                            new SqlParameter("@slot_room",obj.slot_room),
                            new SqlParameter("@subject_code",obj.subject_code),
                            new SqlParameter("@slot_group",obj.slot_group),
                            new SqlParameter("@teacher_code",obj.teacher_code)
		                });
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("TimeTableHistorysave")]
        public HttpResponseMessage TimeTableHistorysave(string ttVersion, string type, List<TimeTableObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'F'),
                        new SqlParameter("@AcaYear",data[0].aca_year),
                        new SqlParameter("@bellCode",data[0].bell_code),
                        new SqlParameter("@version",ttVersion) 
                    });

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        foreach (TimeTableObj obj in data)
                        {


                            int ins = db1.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'H'),
                            new SqlParameter("@AcaYear",obj.aca_year),
                            new SqlParameter("@bellCode",obj.bell_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                            new SqlParameter("@sec_code",obj.sec_code),
                            new SqlParameter("@day_code",obj.day_code),
                            new SqlParameter("@slot_code",obj.slot_code),
                            new SqlParameter("@subject_code",obj.subject_code),
                            new SqlParameter("@teacher_code",obj.teacher_code),
                            new SqlParameter("@slot_group",obj.slot_group),
                            new SqlParameter("@slot_room",obj.slot_room),
                            new SqlParameter("@version",ttVersion) 
		                });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ttVersion);
        }


        [Route("TimeTableIterationsave")]
        public HttpResponseMessage TimeTableIterationsave(string iNumber, List<TimeTableObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'U'),
                        new SqlParameter("@AcaYear",data[0].aca_year),
                        new SqlParameter("@bellCode",data[0].bell_code),
                        new SqlParameter("@cur_code",data[0].cur_code)
                    });

                    using (DBConnection db1 = new DBConnection())
                    {
                        db1.Open();

                        foreach (TimeTableObj obj in data)
                        {


                            int ins = db1.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'V'),
                            new SqlParameter("@cur_code",obj.cur_code),
                            new SqlParameter("@AcaYear",obj.aca_year),
                            new SqlParameter("@bellCode",obj.bell_code),
                            new SqlParameter("@grade_code",obj.grade_code),
                            new SqlParameter("@sec_code",obj.sec_code),
                            new SqlParameter("@day_code",obj.day_code),
                            new SqlParameter("@slot_code",obj.slot_code),
                            new SqlParameter("@subject_code",obj.subject_code),
                            new SqlParameter("@teacher_code",obj.teacher_code),
                            new SqlParameter("@slot_group",obj.slot_group),
                            new SqlParameter("@slot_room",obj.slot_room),
                            new SqlParameter("@version",iNumber) 
                        });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, iNumber);
        }

        [Route("TimeTableHistoryVersionData")]
        public HttpResponseMessage TimeTableHistoryVersionData(string aca_year, string bell_code)
        {

            List<Versions> oVersion = new List<Versions>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.TimeTableData",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'J'),
                                new SqlParameter("@AcaYear",aca_year),
                                new SqlParameter("@bellCode",bell_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Versions simsobj = new Versions();
                            simsobj.version_name = dr["sims_bell_version_name"].ToString();
                            //simsobj.version_update = dr["sims_bell_schedule_update"].ToString();
                            oVersion.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, oVersion);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("TimeTableHistoryDataRetrieval")]
        public HttpResponseMessage TimeTableHistoryDataRetrieval(string aca_year, string bell_code, string ttVersion, string cur_code)
        {
            List<TimeTableObj> oTimeTable = new List<TimeTableObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.TimeTableData",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", 'K'),
                                new SqlParameter("@AcaYear",aca_year),
                                new SqlParameter("@bellCode",bell_code),
                                new SqlParameter("@version",ttVersion),
                                new SqlParameter("@cur_code",cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TimeTableObj simsobj = new TimeTableObj();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            //simsobj.cur_code = dr["sims_bell_cur_code"].ToString();
                            simsobj.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.section_code = dr["sims_bell_section_code"].ToString().Trim();
                            simsobj.subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.slot_code = dr["sims_bell_slot_code"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.slot_room = dr["sims_bell_slot_room"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.subject_color = dr["sims_subject_color"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            oTimeTable.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, oTimeTable);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("TimeTableHistoryDelete")]
        public HttpResponseMessage TimeTableHistoryDelete(string aca_year, string bell_code, string ttVersion)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[TimeTableData]", new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'F'),
                            new SqlParameter("@AcaYear",aca_year),
                            new SqlParameter("@bellCode",bell_code),
                            new SqlParameter("@version",ttVersion) 
                        });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("TimeTableViewData")]
        public HttpResponseMessage TimeTableViewData(Dictionary<string, string> sf)
        {

            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            List<TimeTableObj> oTimeTable = new List<TimeTableObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[TimeTableData]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TimeTableObj simsobj = new TimeTableObj();
                            simsobj.aca_year = dr["sims_bell_academic_year"].ToString();
                            simsobj.teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.grade_code = dr["sims_bell_grade_code"].ToString();
                            simsobj.section_code = dr["sims_bell_section_code"].ToString().Trim();
                            simsobj.subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.day_code = dr["sims_bell_day_code"].ToString();
                            simsobj.slot_code = dr["sims_bell_slot_code"].ToString();
                            simsobj.bell_code = dr["sims_bell_code"].ToString();
                            simsobj.slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.subject_color = dr["sims_subject_color"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            oTimeTable.Add(simsobj);
                        }
                        s = HttpStatusCode.OK;
                        o = oTimeTable;
                    }
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("TimeTableDataNew")]
        public HttpResponseMessage TimeTableDataNew(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;

            List<tTimeTableGenObj> simsttobj = new List<tTimeTableGenObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();

                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                        DataSet ds = db.ExecuteStoreProcedureDS("sims.TimeTableData", sp);
                        ds.DataSetName = "res";
                        o = ds;
                        s = HttpStatusCode.OK;

                    }
                }
            }
            catch (Exception ex)
            {
                s = HttpStatusCode.InternalServerError;
                return Request.CreateResponse(s, ex);
            }
            return Request.CreateResponse(s, o);
        }

        [Route("checkSubjectPreference")]
        public HttpResponseMessage checkSubjectPreference(Dictionary<string, string> sf)
        {
            List<PreObj> oTimeTable = new List<PreObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[TimeTableData]", sp);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("TimeTableView")]
        public HttpResponseMessage TimeTableView(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[TimeTableData]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

    }
}