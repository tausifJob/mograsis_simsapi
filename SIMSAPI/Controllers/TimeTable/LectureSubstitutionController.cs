﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.LectureSubstitution
{
    [RoutePrefix("api/LectureSubstitution")]
    public class LectureSubstitutionController : ApiController
    {
        [Route("LectureSubstitutionCommon")]
        public HttpResponseMessage LectureSubstitutionCommon(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_lecturesubstitution]", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("fetchAbsentTeacher")]
        public HttpResponseMessage fetchAbsentTeacher(string aca_year, string bell_code, string absentdate)
        {
            List<LectureSubObj> teacher_list = new List<LectureSubObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_lecturesubstitution]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'B'),
                                new SqlParameter("@aca_year",aca_year),
                                new SqlParameter("@bell_code",bell_code),
                                new SqlParameter("@filter_date",db.DBYYYYMMDDformat(absentdate))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LectureSubObj simsobj = new LectureSubObj();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            teacher_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, teacher_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("fetchFreeTeacher")]
        public HttpResponseMessage fetchFreeTeacher(string aca_year, string bell_code, string day_code, string slot_code, string teacher_code, string absentdate)
        {
            List<LectureSubObj> teacher_list = new List<LectureSubObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_lecturesubstitution]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr",'C'),
                                new SqlParameter("@aca_year",aca_year),
                                new SqlParameter("@bell_code",bell_code),
                                new SqlParameter("@day_code",day_code),
                                new SqlParameter("@slot_code",slot_code),
                                new SqlParameter("@teacher_code",teacher_code),
                                new SqlParameter("@filter_date",db.DBYYYYMMDDformat(absentdate))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LectureSubObj simsobj = new LectureSubObj();
                            simsobj.sims_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_bell_subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                            simsobj.sims_bell_slot_room = dr["sims_bell_slot_room"].ToString();
                            teacher_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, teacher_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("SaveSubjectSubstitution")]
        public HttpResponseMessage SaveSubjectSubstitution(List<LectureSubObj> data)
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int inst = db.ExecuteStoreProcedureforInsert("[sims].[sims_lecturesubstitution]", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", 'D'),
                        new SqlParameter("@aca_year",data[0].aca_year),
                        new SqlParameter("@bell_code",data[0].bell_code),
                        new SqlParameter("@teacher_code",data[0].teacher_code)
                    });

                    foreach (LectureSubObj obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_lecturesubstitution]", new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", 'E'),
                                new SqlParameter("@aca_year",obj.aca_year),
                                new SqlParameter("@bell_code",obj.bell_code),
                                new SqlParameter("@grade_code",obj.grade_code),
                                new SqlParameter("@sec_code",obj.sec_code),
                                new SqlParameter("@day_code",obj.day_code),
                                new SqlParameter("@slot_code",obj.slot_code),
                                new SqlParameter("@slot_group",obj.slot_group),
                                new SqlParameter("@slot_room",obj.slot_room),
                                new SqlParameter("@teacher_code",obj.teacher_code),
                                new SqlParameter("@subject_code",obj.subject_code),
                                new SqlParameter("@filter_date",db.DBYYYYMMDDformat(obj.filter_date)),
                                new SqlParameter("@subs_teacher_code",obj.subs_teacher_code),
                                new SqlParameter("@subs_subject_code",obj.subs_subject_code),
                                new SqlParameter("@substitution_status",obj.substitution_status),
                                new SqlParameter("@substitution_acknowledgement_status",obj.substitution_acknowledgement_status),
                                new SqlParameter("@substitution_remark",obj.substitution_remark)
                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}

