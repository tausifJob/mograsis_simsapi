﻿using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.TimeTable
{
    [RoutePrefix("api/SubjectTeacher")]
    public class SubjectTeacherController : ApiController
    {
        [Route("SubjectTeacherDataImport")]
        public HttpResponseMessage SubjectTeacherDataImport(Dictionary<string, string> sf)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.OK;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    List<SqlParameter> sp = new List<SqlParameter>();
                    sp.Clear();
                    if (sf != null)
                    {
                        foreach (var p in sf.Keys)
                        {
                            SqlParameter pr1 = new SqlParameter();
                            pr1.ParameterName = "@" + p;
                            pr1.Value = sf[p];
                            sp.Add(pr1);
                        }
                    }
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_subject_Teacher", sp);
                    ds.DataSetName = "res";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("ImportAcademicYear")]
        public HttpResponseMessage ImportAcademicYear()
        {

            List<AcaObj> acaYear = new List<AcaObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'A')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AcaObj simsobj = new AcaObj();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();
                            simsobj.aca_year_desc = dr["sims_academic_year_description"].ToString();
                            acaYear.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, acaYear);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, acaYear);
            }
        }

        [Route("ImportAllGrades")]
        public HttpResponseMessage ImportAllGrades(String aca_year)
        {

            List<GradeObj> grade_list = new List<GradeObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'N'),
                                new SqlParameter("@sims_academic_year",aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GradeObj simsobj = new GradeObj();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_bell_code = dr["sims_bell_code"].ToString();
                            simsobj.sims_aca_year = dr["sims_bell_academic_year"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("ImportAllSection")]
        public HttpResponseMessage ImportAllSection(String aca_year, String param)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : sectionCommon_p(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "sectionCommon_p"));

            List<SectionObj> section_list = new List<SectionObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@sims_grade_code",param),
                                new SqlParameter("@sims_academic_year",aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SectionObj simsobj = new SectionObj();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            section_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, section_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, section_list);
            }
        }

        [Route("ImportSubjectGrades")]
        public HttpResponseMessage ImportSubjectGrades(String aca_year, String sub_code, String bell_code)
        {

            List<GradeObj> grade_list = new List<GradeObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'G'),
                                new SqlParameter("@sims_academic_year",aca_year),
                                new SqlParameter("@sims_subject_code",sub_code),
                                new SqlParameter("@bell_code",bell_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GradeObj simsobj = new GradeObj();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_bell_code = dr["sims_bell_code"].ToString();
                            simsobj.sims_aca_year = dr["sims_academic_year"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("ImportSubjectSection")]
        public HttpResponseMessage ImportSubjectSection(String aca_year, String param, String sub_code)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : sectionCommon_p(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "sectionCommon_p"));

            List<SectionObj> section_list = new List<SectionObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@sims_grade_code",param),
                                new SqlParameter("@sims_academic_year",aca_year),
                                new SqlParameter("@sims_subject_code",sub_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SectionObj simsobj = new SectionObj();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            section_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, section_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, section_list);
            }
        }

        [Route("ImportAllSubject")]
        public HttpResponseMessage ImportAllSubject(String aca_year, String grade, String section)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : sectionCommon_p(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "sectionCommon_p"));

            List<SubjectObj> subject_list = new List<SubjectObj>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "H"),
                                new SqlParameter("@sims_grade_code",grade),
                                new SqlParameter("@sims_section_code",section),
                                new SqlParameter("@sims_academic_year",aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubjectObj simsobj = new SubjectObj();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_color = dr["sims_subject_color"].ToString();
                            simsobj.sims_subject_type = dr["sims_subject_type"].ToString();
                            subject_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, subject_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subject_list);
            }
        }

        [Route("SubjectTeacherDataUpdate")]
        public HttpResponseMessage SubjectTeacherDataUpdate(char cTemp, String empID, String aca_year, List<TimeTableObj> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
                bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    if (data.Count == 0)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_Teacher]",
                        new List<SqlParameter>()
                         {  
                                new SqlParameter("@OPR",'D'),
                                new SqlParameter("@EmpID",empID),
                                new SqlParameter("@sims_academic_year",aca_year)
                         });
                    }
                    else
                    {
                        foreach (TimeTableObj simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[insertSubjectTeacher]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",cTemp),
                          new SqlParameter("@aca_year",simsobj.aca_year),
                          new SqlParameter("@teacher_code",simsobj.teacher_code),
                          new SqlParameter("@grade_code",simsobj.grade_code),
                          new SqlParameter("@section_code",simsobj.section_code),
                          new SqlParameter("@subject_code",simsobj.subject_code),
                          new SqlParameter("@day_code",simsobj.day_code),
                          new SqlParameter("@slot_code",simsobj.slot_code),
                          new SqlParameter("@bell_code",simsobj.bell_code)
                        });

                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            cTemp = 'I';
                        }
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bell_copy_proc]",
                       new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",'S'),   
                             new SqlParameter("@emp_id",empID),
                             new SqlParameter("@aca_year",aca_year)
                         });
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }

}