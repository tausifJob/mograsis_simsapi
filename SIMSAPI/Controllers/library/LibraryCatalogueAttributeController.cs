﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryCatalogueAttribute")]
    [BasicAuthentication]
    public class LibraryCatalogueAttributeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllLibraryCatalogueAttribute")]
        public HttpResponseMessage getAllLibraryAttribute()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryCatalogueAttribute(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryCatalogueAttribute"));

            List<Sim117> goaltarget_list = new List<Sim117>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_catalogue_attribute_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim117 simsobj = new Sim117();
                            simsobj.cat_code = dr["cat_code"].ToString();
                            simsobj.att_code = dr["att_code"].ToString();
                            simsobj.sims_library_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_attribute_code = dr["sims_library_attribute_code"].ToString();


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        //GetCatalougeName
        [Route("GetcatalogueName")]
        public HttpResponseMessage GetcatalogueName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetcatalogueName()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Sim117> doc_list = new List<Sim117>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_catalogue_attribute_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim117 simsobj = new Sim117();
                            simsobj.sims_library_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        //GetAttributeCode
        [Route("GetAttributeCode")]
        public HttpResponseMessage GetAttributeCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAttributeCode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetAttributeCode"));

            List<Sim117> doc_list = new List<Sim117>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_catalogue_attribute_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim117 simsobj = new Sim117();
                            simsobj.sims_library_attribute_code = dr["sims_library_attribute_code"].ToString();
                            simsobj.sims_library_attribute_name = dr["sims_library_attribute_name"].ToString();
                           

                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }



        [Route("CUDLibraryCatalogueAttribute")]
        public HttpResponseMessage CUDLibraryCatalogueAttribute(List<Sim117> data)
        {
            bool insert=false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim117 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_catalogue_attribute_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_catalogue_code",simsobj.sims_library_catalogue_code),
                                new SqlParameter("@sims_library_attribute_code", simsobj.sims_library_attribute_code),

                               
                               // new SqlParameter("@sims_library_catalogue_code_old", simsobj.cat_code),
                             //   new SqlParameter("@sims_library_attribute_code_old", simsobj.att_code),
                             

                    });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










