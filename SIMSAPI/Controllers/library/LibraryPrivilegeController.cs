﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryPrivilege")]
    [BasicAuthentication]
    public class LibraryPrivilegeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllLibraryPrivilege")]
        public HttpResponseMessage getAllLibraryCatalogue()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryPrivilege(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryPrivileges"));

            List<Sim127> goaltarget_list = new List<Sim127>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_privilege_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim127 simsobj = new Sim127();
                            simsobj.sims_library_privilege_number = dr["sims_library_privilege_number"].ToString();
                            simsobj.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();

                            simsobj.sims_library_user_group_code = dr["sims_library_user_group_code"].ToString();
                            simsobj.sims_library_membership_type = dr["sims_library_membership_type"].ToString();

                            simsobj.sims_library_user_group_code1 = dr["sims_library_user_group_code1"].ToString();
                            simsobj.sims_library_membership_type1 = dr["sims_library_membership_type1"].ToString();


                            simsobj.sims_library_borrow_limit = dr["sims_library_borrow_limit"].ToString();
                            simsobj.sims_library_allowed_limit = dr["sims_library_allowed_limit"].ToString();
                            simsobj.sims_library_reservation_limit = dr["sims_library_reservation_limit"].ToString();
                            simsobj.sims_library_duration = dr["sims_library_duration"].ToString();

                            simsobj.sims_library_promote_flag = dr["sims_library_promote_flag"].Equals("Y") ? true : false;//Chk box

                            simsobj.sims_library_quota = dr["sims_library_quota"].ToString();
                            simsobj.sims_library_no_issue_after =  db.UIDDMMYYYYformat(dr["sims_library_no_issue_after"].ToString());

                            simsobj.sims_library_auto_renew_flag = dr["sims_library_auto_renew_flag"].Equals("Y") ? true : false;//Chk box

                            simsobj.sims_library_grace_days = dr["sims_library_grace_days"].ToString();

                            simsobj.sims_library_privilege_status = dr["sims_library_privilege_status"].Equals("A") ? true : false;//Chk box
                            simsobj.sims_library_reservation_flag = dr["sims_library_reservation_flag"].Equals("Y") ? true : false;//Chk box

                            simsobj.sims_library_reservation_release_limit = dr["sims_library_reservation_release_limit"].ToString();

                            simsobj.sims_library_issue_limit_days = dr["sims_library_issue_limit_days"].ToString();

                            simsobj.sims_library_no_of_times_issue_limit = dr["sims_library_no_of_times_issue_limit"].ToString();
                            simsobj.sims_library_locking_period = dr["sims_library_locking_period"].ToString();



                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        //GetUserGroupCode
        [Route("GetUserGroupCode")]
        public HttpResponseMessage GetUserGroupCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetUserGroupCode()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Sim127> doc_list = new List<Sim127>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_privilege_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim127 simsobj = new Sim127();
                            simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            simsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        //GetMembershipType
        [Route("GetMembershipType")]
        public HttpResponseMessage GetMembershipType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetMembershipType()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetGLAccountNumber"));

            List<Sim127> doc_list = new List<Sim127>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_privilege_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim127 simsobj = new Sim127();
                            simsobj.sims_library_membership_type_code = dr["sims_library_membership_type_code"].ToString();
                            simsobj.sims_library_membership_type_name = dr["sims_library_membership_type_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDLibraryPrivilege")]
        public HttpResponseMessage CUDLibraryPrivilege(List<Sim127> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim127 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_privilege_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_privilege_number",simsobj.sims_library_privilege_number),
                                new SqlParameter("@sims_library_privilege_name", simsobj.sims_library_privilege_name),

                                new SqlParameter("@sims_library_user_group_code1",simsobj.comn_user_group_code),
                                new SqlParameter("@sims_library_membership_type1", simsobj.sims_library_membership_type_code),

                                new SqlParameter("@sims_library_borrow_limit",simsobj.sims_library_borrow_limit),
                                new SqlParameter("@sims_library_allowed_limit", simsobj.sims_library_allowed_limit),
                                new SqlParameter("@sims_library_reservation_limit",simsobj.sims_library_reservation_limit),
                                new SqlParameter("@sims_library_duration", simsobj.sims_library_duration),

                                new SqlParameter("@sims_library_promote_flag", simsobj.sims_library_promote_flag==true?"Y":"N"),

                                new SqlParameter("@sims_library_quota",simsobj.sims_library_quota),
                                new SqlParameter("@sims_library_no_issue_after",  db.DBYYYYMMDDformat(simsobj.sims_library_no_issue_after)),

                                new SqlParameter("@sims_library_auto_renew_flag", simsobj.sims_library_auto_renew_flag==true?"Y":"N"),

                                new SqlParameter("@sims_library_grace_days", simsobj.sims_library_grace_days),


                                new SqlParameter("@sims_library_privilege_status", simsobj.sims_library_privilege_status==true?"A":"I"),
                                new SqlParameter("@sims_library_reservation_flag", simsobj.sims_library_reservation_flag==true?"Y":"N"),

                                new SqlParameter("@sims_library_reservation_release_limit",simsobj.sims_library_reservation_release_limit),

                                new SqlParameter("@sims_library_issue_limit_days",simsobj.sims_library_issue_limit_days),
                                new SqlParameter("@sims_library_no_of_times_issue_limit",simsobj.sims_library_no_of_times_issue_limit),

                                new SqlParameter("@sims_library_locking_period",simsobj.sims_library_locking_period),

                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










