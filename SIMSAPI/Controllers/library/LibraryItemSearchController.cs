﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemSearch")]
   // [BasicAuthentication]
    public class LibraryItemSearchController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetLibrarySubCategory")]
        public HttpResponseMessage GetLibrarySubCategory(string SubCategory)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_search_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S'),
                       new SqlParameter ("@sims_library_category_code", SubCategory)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims121 simsobj = new Sims121();
                                simsobj.sims_library_item_subcategory = dr["sims_library_subcategory"].ToString();
                                simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("ItemSearch")]
        public HttpResponseMessage ItemSearch(Sims121 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : ItemSearch(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "ItemSearch"));
            //Sims682 comnobj = new Sims682();
            //if (data == "undefined" || data == "\"\"" || data == "[]")
            //{
            //    data = null;
            //}
            //else
            //{
            //    comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims682>(data);
            //}

            //List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            List<Sims121> mod_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_search_proc]",
                 new List<SqlParameter>(){               
                 new SqlParameter("@opr", 'A'),
                 new SqlParameter("@sims_library_item_number",data.sims_library_item_number),
                 new SqlParameter("@sims_library_isbn_number",data.sims_library_isbn_number),
                 new SqlParameter("@sims_library_item_accession_number",data.sims_library_item_accession_number),
                 new SqlParameter("@sims_library_item_title",data.sims_library_item_title),
                 new SqlParameter("@sims_library_item_desc",data.sims_library_item_desc),
                 new SqlParameter("@sims_library_item_category",data.sims_library_item_category),
                 new SqlParameter("@sims_library_item_subcategory",data.sims_library_item_subcategory),
                 new SqlParameter("@sims_library_item_catalogue_code",data.sims_library_item_catalogue_code),
                 new SqlParameter("@sims_library_item_author1",data.sims_library_item_author1),
                 new SqlParameter("@sims_library_item_name_of_publisher",data.sims_library_item_name_of_publisher),
                 new SqlParameter("@sims_library_item_bin_code",data.sims_library_item_bin_code),
                 new SqlParameter("@sims_library_item_status_code",data.sims_library_item_status_code),
                 new SqlParameter("@sims_library_item_bar_code",data.sims_library_item_bar_code),
                 new SqlParameter("@sims_library_item_location_code", data.sims_library_item_location_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();

                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();

                            //simsobj.sims_library_item_catalogue_code = new List<string> ();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();


                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString()).Trim();
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                               simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                                simsobj.sims_library_item_entry_date = db.UIDDMMYYYYformat(dr["sims_library_item_entry_date"].ToString());
                            //simsobj.available_copies = dr["available_copies"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }
    }
}