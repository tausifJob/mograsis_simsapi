﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;
using System.Data;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemVerificationApprove")]
    public class LibraryItemVerificationApproveController : ApiController
    {
        //Library Item Verification Approve
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("GetSims_ItemVerificationStatus")]
        public HttpResponseMessage GetSims_ItemVerificationStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_ItemVerificationStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_ItemVerificationStatus"));

            List<Sims121> goaltarget_list = new List<Sims121>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_verification_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'X'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_approved_status_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_library_item_approved_status_name = dr["sims_appl_form_field_value1"].ToString();
                            
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_RequestDetail")]
        public HttpResponseMessage Get_RequestDetail(string date1, string date2, string acc_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_RequestDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_RequestDetail"));
            Message message = new Message();
            List<Sims121> goaltarget_list = new List<Sims121>();
            //int total = 0, skip = 0;


            if (date1 == "undefined" || date1 == "\"\"")
            {
                date1 = null;
            }
            if (date2 == "undefined" || date2 == "\"\"")
            {
                date2 = null;
            }
            if (acc_no == "undefined" || acc_no == "\"\"")
            {
                acc_no = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_verification_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),                          
                           new SqlParameter("@date1",db.DBYYYYMMDDformat(date1)),
                           new SqlParameter("@date2",db.DBYYYYMMDDformat(date2)),                          
                           new SqlParameter("@sims_library_item_accession_number",acc_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_book_style_name"].ToString();
                            simsobj.sims_library_item_request_status_code = dr["sims_library_item_request_status_code"].ToString();
                            simsobj.sims_library_item_request_status_name = dr["sims_library_item_request_status_name"].ToString();
                            simsobj.sims_library_item_approved_status_code = dr["sims_library_item_approved_status_code"].ToString();
                            simsobj.sims_library_item_approved_status_name = dr["sims_library_item_approved_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            simsobj.sims_library_item_quantity = dr["sims_library_item_quantity"].ToString();
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_supplier = dr["sims_library_item_supplier"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_request_date"].ToString()))
                                simsobj.sims_library_item_request_date = db.UIDDMMYYYYformat(dr["sims_library_item_request_date"].ToString());
                            simsobj.sims_library_item_requested_by = dr["sims_library_item_requested_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_status_approve_date"].ToString()))
                                simsobj.sims_library_item_status_approve_date = db.UIDDMMYYYYformat(dr["sims_library_item_status_approve_date"].ToString());
                            simsobj.sims_library_item_approved_by = dr["sims_library_item_approved_by"].ToString();
                            simsobj.sims_library_item_approved_status = dr["sims_library_item_approved_status"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            catch (Exception x)
            {
                Log.Error(x);
                //return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDLibraryItemVerification")]
        public HttpResponseMessage CUDLibraryItemVerification(List<Sims121> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_item_verification_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),   
                          //new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          //new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          //new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),   
                          //new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          //new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          //new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),   
                          //new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          //new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          //new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          //new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          //new SqlParameter("@sims_library_item_grade_code",simsobj.sims_library_item_grade_code),
                          //new SqlParameter("@sims_library_item_language_code",simsobj.sims_library_item_language_code),
                          //new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          //new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          //new SqlParameter("@sims_library_item_name_of_publisher",simsobj.sims_library_item_name_of_publisher),
                          //new SqlParameter("@sims_library_item_date_of_publication", simsobj.sims_library_item_date_of_publication),
                          //new SqlParameter("@sims_library_item_printing_date", simsobj.sims_library_item_printing_date),
                          //new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          //new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          //new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          //new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_request_status_code", simsobj.sims_library_item_request_status_code),
                          new SqlParameter("@sims_library_item_approved_status_code", simsobj.sims_library_item_approved_status_code),
                          new SqlParameter("@sims_library_item_approved_by", simsobj.sims_library_item_approved_by),
                          new SqlParameter("@sims_library_item_approved_status", simsobj.sims_library_item_approved_status),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_quantity),
                          //new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          //new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          //new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

        }

        //Library Item Verification Request
        [Route("ItemSearch")]
        public HttpResponseMessage ItemSearch(Sims121 obj)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();
            if (obj.sims_library_item_category == "undefined" || obj.sims_library_item_category == "")
            {
                obj.sims_library_item_category = null;
            }
            if (obj.sims_library_item_subcategory == "undefined" || obj.sims_library_item_subcategory == "")
            {
                obj.sims_library_item_subcategory = null;
            }
            if (obj.sims_library_item_accession_number == "undefined" || obj.sims_library_item_accession_number == "")
            {
                obj.sims_library_item_accession_number = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_verification_proc]",
                   new List<SqlParameter>(){               
                 new SqlParameter ("@opr", 'A'),
                 new SqlParameter ("@sims_library_item_category",obj.sims_library_item_category),
                 new SqlParameter ("@sims_library_item_subcategory",obj.sims_library_item_subcategory),
                 new SqlParameter ("@sims_library_item_accession_number",obj.sims_library_item_accession_number),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();


                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();


                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString());
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                                simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                                simsobj.sims_library_item_entry_date = db.UIDDMMYYYYformat(dr["sims_library_item_entry_date"].ToString());
                            simsobj.sims_library_item_supplier = dr["sims_library_item_supplier"].ToString();
                            simsobj.sims_library_year_of_publication = dr["sims_library_year_of_publication"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_status_update_date"].ToString()))
                                simsobj.sims_library_item_status_update_date = db.UIDDMMYYYYformat(dr["sims_library_item_status_update_date"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDLibraryItemVerificationInsert")]
        public HttpResponseMessage CUDLibraryItemVerificationInsert(List<Sims121> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_item_verification_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),   
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),   
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),   
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code",simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code",simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code",simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_name_of_publisher",simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication", db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),
                          new SqlParameter("@sims_library_item_printing_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_request_status_code", simsobj.sims_library_item_approved_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          new SqlParameter("@sims_library_item_requested_by", simsobj.sims_library_item_requested_by),
                          new SqlParameter("@sims_library_item_supplier", simsobj.sims_library_item_supplier),
                          //new SqlParameter("@sims_library_item_approved_by", simsobj.sims_library_item_approved_by),
                          new SqlParameter("@sims_library_item_approved_status", simsobj.sims_library_item_approved_status),
                          new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_quantity),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

        }
    }
}