using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemRequest")]
    public class LibraryItemRequestController : ApiController
    {
        [Route("GetLibraryRequestType")]
        public HttpResponseMessage GetLibraryRequestType()
        {
            List<Sims128> type_list = new List<Sims128>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_request_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'G')
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims128 simsobj = new Sims128();
                            simsobj.sims_library_request_typename = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_library_request_type = dr["sims_appl_parameter"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLibraryCategory")]
        public HttpResponseMessage GetLibraryCategory()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_category",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_category = dr["sims_library_category_code"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLibrarySubCategory")]
        public HttpResponseMessage GetLibrarySubCategory(string SubCategory)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_library_subcategory_proc",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'B'),
                       new SqlParameter ("@sims_library_category_code", SubCategory)
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_subcategory = dr["sims_library_subcategory_code"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLibraryCatelogue")]
        public HttpResponseMessage GetLibraryCatelogue()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_catelogue",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("BookSearch")]
        public HttpResponseMessage BookSearch(Sims121 obj)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                   new List<SqlParameter>(){               
                 new SqlParameter("@opr", 'S'),
                 //new SqlParameter("@sims_appl_code", obj.Sims121_sims_appl_code),
                 //new SqlParameter("@sims_appl_form_field", obj.Sims121_sims_appl_form_field),
                 //new SqlParameter("@sims_library_item_accession_number", obj.sims_library_item_accession_number),
                 //new SqlParameter("@sims_library_item_accession_number_to", obj.sims_library_item_accession_number_to),
                 //new SqlParameter("@sims_library_isbn_number", obj.sims_library_isbn_number),
                 //new SqlParameter("@sims_library_bnn_number", obj.sims_library_bnn_number),
                 //new SqlParameter("@sims_library_item_dewey_number", obj.sims_library_item_dewey_number),
                 //new SqlParameter("@sims_library_item_name", obj.sims_library_item_name),
                 //new SqlParameter("@sims_library_item_title", obj.sims_library_item_title),
                 //new SqlParameter("@sims_library_item_category", obj.sims_library_item_category),
                 //new SqlParameter("@sims_library_item_subcategory", obj.sims_library_item_subcategory),
                 //new SqlParameter("@sims_library_item_catalogue_code", obj.sims_library_item_catalogue_code),
                 //new SqlParameter("@sims_library_item_cur_code", obj.sims_library_item_cur_code),
                 //new SqlParameter("@sims_library_item_cur_level_code", obj.sims_library_item_cur_level_code),
                 //new SqlParameter("@sims_library_item_grade_code", obj.sims_library_item_grade_code),
                 //new SqlParameter("@sims_library_item_language_code", obj.sims_library_item_language_code),
                 //new SqlParameter("@sims_library_item_author1", obj.sims_library_item_author1),
                 //new SqlParameter("@sims_library_item_age_from", obj.sims_library_item_age_from),
                 //new SqlParameter("@sims_library_item_place_of_publication", obj.sims_library_item_place_of_publication),
                 //new SqlParameter("@sims_library_item_name_of_publisher", obj.sims_library_item_name_of_publisher),
                 //new SqlParameter("@sims_library_item_book_style", obj.sims_library_item_book_style),
                 //new SqlParameter("@sims_library_item_status_code", obj.sims_library_item_status_code),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.available_copies = dr["available_copies"].ToString();

                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_nm"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_nm"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetLibraryRequest")]
        public HttpResponseMessage GetLibraryRequest()
        {
            List<Sims128> trans_list = new List<Sims128>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_request_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S'),
                //cmd.Parameters.AddWithValue("@sims_library_request_number", "");
                //cmd.Parameters.AddWithValue("@sims_library_user_code", "");
                //cmd.Parameters.AddWithValue("@sims_library_request_date", "");
                //cmd.Parameters.AddWithValue("@sims_library_request_typename", "");
                //cmd.Parameters.AddWithValue("@sims_library_request_remarks", "");
                //cmd.Parameters.AddWithValue("@sims_library_request_statusname", "");
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims128 simsobj = new Sims128();
                            simsobj.sims_library_request_number = dr["sims_library_request_number"].ToString();
                            simsobj.sims_library_user_code = dr["sims_library_user_code"].ToString();
                            simsobj.sims_library_request_date = db.UIDDMMYYYYformat(dr["sims_library_request_date"].ToString());
                            simsobj.sims_library_request_typename = dr["sims_library_request_type"].ToString();
                            simsobj.sims_library_request_remarks = dr["sims_library_request_remarks"].ToString();
                            simsobj.sims_library_request_statusname = dr["sims_library_request_status"].ToString();
                            trans_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);
        }

        [Route("CUDLibraryRequest")]
        public HttpResponseMessage CUDLibraryRequest(List<Sims121> data)
        {
            Message message = new Message();
            int count = 0;
            string inserted = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        SqlDataReader DR = db.ExecuteStoreProcedure("[sims].[sims_library_request_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_request_number", simsobj.sims_library_request_number),
                                new SqlParameter("@sims_library_user_code", simsobj.sims_library_user_code),
                                new SqlParameter("@sims_library_request_date", db.DBYYYYMMDDformat(simsobj.sims_library_request_date)),
                                new SqlParameter("@sims_library_request_type", simsobj.sims_library_request_type),
                                new SqlParameter("@sims_library_request_remarks", simsobj.sims_library_request_remarks),
                                new SqlParameter("@sims_library_request_status", simsobj.sims_library_request_status),
                                new SqlParameter("@sims_library_request_type1",simsobj.sims_library_request_typename),

                               // new SqlParameter("@sims_library_request_line_number", simsobj.sims_library_request_line_number),
                                new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                                new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                                new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                                new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                                new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                                new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                                new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                                new SqlParameter("@sims_library_item_qty", simsobj.sims_library_item_qty),
                                new SqlParameter("@sims_library_item_amount", simsobj.sims_library_item_purchase_price),
                                new SqlParameter("@sims_library_item_source_hint", simsobj.sims_library_item_source_hint),
                                new SqlParameter("@sims_library_item_status", simsobj.sims_library_request_status),
                                new SqlParameter("@count", count)
                     });
                        if (simsobj.opr == "I")
                        {
                            if (DR.Read())
                            {
                                inserted = DR["sims_library_request_number"].ToString();
                                count = count + 1;
                            }
                        }
                        else
                        {
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                inserted = DR["sims_library_request_number"].ToString();
                                count = count + 1;
                            }
                            else
                            {
                                inserted = DR["sims_library_request_number"].ToString();
                                count = count + 1;
                            }
                        }
                        DR.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }

            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}