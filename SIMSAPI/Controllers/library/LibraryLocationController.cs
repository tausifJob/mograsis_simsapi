﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryLocation")]
    [BasicAuthentication]
    public class LibraryLocationController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllLibraryLocation")]
        public HttpResponseMessage getAllLibraryLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryLocation(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryLocation"));

            List<Sim123> Library_loc_list = new List<Sim123>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_location_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim123 simsobj = new Sim123();
                            simsobj.sims_library_location_code = dr["sims_library_location_code"].ToString();
                            simsobj.sims_library_location_name = dr["sims_library_location_name"].ToString();

                            simsobj.sims_library_location_address1 = dr["sims_library_location_address1"].ToString();
                            simsobj.sims_library_location_address2 = dr["sims_library_location_address2"].ToString();
                            simsobj.sims_library_location_contact_person = dr["sims_library_location_contact_person"].ToString();
                            simsobj.sims_library_location_phone = dr["sims_library_location_phone"].ToString();

                            Library_loc_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Library_loc_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, Library_loc_list);
        }

     

        [Route("CUDLibraryLocation")]
        public HttpResponseMessage CUDLibraryCategory(List<Sim123> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim123 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_location_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_location_code",simsobj.sims_library_location_code),
                                new SqlParameter("@sims_library_location_name",simsobj.sims_library_location_name),
                                new SqlParameter("@sims_library_location_address1", simsobj.sims_library_location_address1),
                                new SqlParameter("@sims_library_location_address2",simsobj.sims_library_location_address2),
                                new SqlParameter("@sims_library_location_contact_person", simsobj.sims_library_location_contact_person),
                                new SqlParameter("@sims_library_location_phone",simsobj.sims_library_location_phone),
                               

                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










