﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryMembershipType")]
    [BasicAuthentication]
    public class LibraryMembershipTypeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllLibraryMembershipType")]
        public HttpResponseMessage getAllLibraryMembershipType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryMembershipType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryMembershipType"));

            List<Sim126> goaltarget_list = new List<Sim126>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_membership_type_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim126 simsobj = new Sim126();
                            simsobj.sims_library_membership_type_code = dr["sims_library_membership_type_code"].ToString();
                            simsobj.sims_library_membership_type_name = dr["sims_library_membership_type_name"].ToString();
                            simsobj.sims_library_membership_type_status = dr["sims_library_membership_type_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDLibraryMembershipType")]
        public HttpResponseMessage CUDLibraryMembershipType(List<Sim126> data)
        {
            Message message = new Message();

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim126 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_membership_type_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_membership_type_code",simsobj.sims_library_membership_type_code),
                                new SqlParameter("@sims_library_membership_type_name", simsobj.sims_library_membership_type_name),
                                new SqlParameter("@sims_library_membership_type_status", simsobj.sims_library_membership_type_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {

                            insert = true;
                            //if (simsobj.opr.Equals("U"))
                            //    message.strMessage = "Record Updated Sucessfully!!";
                            //else if (simsobj.opr.Equals("I"))
                            //    message.strMessage = "Record  Added Sucessfully!!";
                            //else if (simsobj.opr.Equals("D"))
                            //    message.strMessage = "Record  Deleted Sucessfully!!";

                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                        }


                        else
                        {
                            insert = false;
                            //// message.strMessage = "";
                            //message.strMessage = " Record already present";
                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            //// message.messageType = MessageType.Error;
                            //// return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
          //  return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










