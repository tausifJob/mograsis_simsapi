﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryEmpMembership")]
    [BasicAuthentication]
    public class LibraryEmpMembershipController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetEmployeeDetails")]
        public HttpResponseMessage GetEmployeeDetails(string emcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeeDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetEmployeeDetails"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            if (emcode == "undefined" || emcode == "\"\"")
            {
                emcode = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@em_number", emcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.em_sex = dr["em_sex"].ToString();
                            //simsobj.em_login_code = dr["em_login_code"].ToString();
                            //simsobj.sims_user_code = dr["sims_user_code"].ToString();
                            //simsobj.sims_teacher_type = dr["sims_teacher_type"].ToString();
                            simsobj.em_status = dr["em_status"].Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetAllLibraryMembershipType")]
        public HttpResponseMessage GetAllLibraryMembershipType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLibraryMembershipType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllLibraryMembershipType"));
            List<Sim126> goaltarget_list = new List<Sim126>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_membership_type_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'K'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim126 simsobj = new Sim126();
                            simsobj.sims_library_membership_type_code = dr["sims_library_membership_type_code"].ToString();
                            simsobj.sims_library_membership_type_name = dr["sims_library_membership_type_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //[Route("GetAllLibraryPrivilege")]
        //public HttpResponseMessage getAllLibraryPrivilege()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryPrivilege(),PARAMETERS :: NO";
        //    Log.Debug(string.Format(debug, "Common", "getAllLibraryPrivileges"));

        //    List<Sim127> goaltarget_list = new List<Sim127>();
        //    //int total = 0, skip = 0;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_privilege_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                   new SqlParameter("@opr", 'S'),

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sim127 simsobj = new Sim127();
        //                    simsobj.sims_library_privilege_number = dr["sims_library_privilege_number"].ToString();
        //                    simsobj.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();                           

        //                    goaltarget_list.Add(simsobj);

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        //}

        [Route("Updateemp_RenewLibrary")]
        public HttpResponseMessage Updateemp_RenewLibrary(List<Sims503> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims503 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'V'),                                
                                //new SqlParameter("@cur_code", simsobj.cur_code),                                                            
                                new SqlParameter("@em_number", simsobj.sims_employee_code),
                                new SqlParameter("@sims_library_membership_type", simsobj.sims_library_membership_type),                                 
                                new SqlParameter("@LIB_JOIN_DATE", db.DBYYYYMMDDformat(simsobj.sims_library_joining_date)),
                                new SqlParameter("@cur_code", simsobj.cur_code),
                                new SqlParameter("@acad_year", simsobj.academic_year),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("GetStudentDetails")]
        public HttpResponseMessage GetStudentDetails(string enroll)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetEmployeeDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetEmployeeDetails"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            if (enroll == "undefined" || enroll == "\"\"")
            {
                enroll = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Q'),
                           new SqlParameter("@enroll", enroll)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.student_sex = dr["sims_student_gender"].ToString();
                            simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Updatestudent_RenewLibrary")]
        public HttpResponseMessage Updatestudent_RenewLibrary(List<Sims503> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims503 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'X'),                                
                                //new SqlParameter("@cur_code", simsobj.cur_code),                                                            
                                new SqlParameter("@enroll", simsobj.sims_enroll_number),
                                new SqlParameter("@sims_library_membership_type", simsobj.sims_library_membership_type),                                 
                                new SqlParameter("@LIB_JOIN_DATE", db.DBYYYYMMDDformat(simsobj.sims_library_joining_date)),
                                new SqlParameter("@cur_code", simsobj.cur_code),
                                new SqlParameter("@acad_year", simsobj.academic_year),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }

                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}