﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryCatalogue")]
    [BasicAuthentication]
    public class LibraryCatalogueController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getAllLibraryCatalogue")]
        public HttpResponseMessage getAllLibraryCatalogue()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryCatalogue(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryCatalogue"));

            List<Sim119> goaltarget_list = new List<Sim119>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_catelogue_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim119 simsobj = new Sim119();
                            simsobj.sims_library_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_catalogue_status = dr["sims_library_catalogue_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDLibraryCatalogue")]
        public HttpResponseMessage CUDLibraryCatalogue(List<Sim119> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim119 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_catelogue_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_catalogue_code",simsobj.sims_library_catalogue_code),
                                new SqlParameter("@sims_library_catalogue_name", simsobj.sims_library_catalogue_name),
                                new SqlParameter("@sims_library_catalogue_status", simsobj.sims_library_catalogue_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}










