﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;


namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemStyle")]
    [BasicAuthentication]
    public class LibraryItemStyleController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Select
        [Route("getLibraryItemStyle")]
        public HttpResponseMessage getAllLibraryItemStyle()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLibraryItemStyle(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getLibraryItemStyle"));

            List<Sims122> goaltarget_list = new List<Sims122>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_style]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims122 simsobj = new Sims122();
                            simsobj.sims_library_item_style_code = int.Parse(dr["sims_library_item_style_code"].ToString());
                            simsobj.sims_library_item_style_name = dr["sims_library_item_style_name"].ToString();
                            //simsobj.sims_library_attribute_status = dr["sims_library_attribute_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }
        [Route("CUDLibraryItemStyle")]
        public HttpResponseMessage CUDLibraryItemStyle(List<Sims122> data)
        {
          
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims122 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_style]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_item_style_code",simsobj.sims_library_item_style_code),
                                new SqlParameter("@sims_library_item_style_name", simsobj.sims_library_item_style_name),
                               // new SqlParameter("@sims_library_attribute_status", simsobj.sims_library_attribute_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            //if (simsobj.opr.Equals("U"))
                            //    message.strMessage = "Record Updated Sucessfully!!";
                            //else if (simsobj.opr.Equals("I"))
                            //    message.strMessage = "Record  Added Sucessfully!!";
                            //else if (simsobj.opr.Equals("D"))
                            //    message.strMessage = "Record  Deleted Sucessfully!!";

                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            insert = true;
                        }


                        else
                        {
                            // message.strMessage = "";
                            //message.strMessage = "Library Item Style Code is already present";
                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}