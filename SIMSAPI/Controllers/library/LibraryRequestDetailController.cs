﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryRequestDetail")]
    [BasicAuthentication]
    public class LibraryRequestDetailController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetSims_LibraryRequestStatus")]
        public HttpResponseMessage GetSims_LibraryRequestStatus()
        {
            List<Sims129> type_list = new List<Sims129>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_request_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims129 simsobj = new Sims129();
                            simsobj.sims_library_item_statusname = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_library_item_status = dr["sims_appl_parameter"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetSims_LibraryRequestDetail")]
        public HttpResponseMessage GetSims_LibraryRequestDetail(string req_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_LibraryRequestDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_LibraryRequestDetail"));
            Message message = new Message();
            List<Sims129> goaltarget_list = new List<Sims129>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_request_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),
                           new SqlParameter("@sims_library_request_number",req_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims129 simsobj = new Sims129();
                            simsobj.sims_library_request_number = dr["sims_library_request_number"].ToString();
                            simsobj.sims_library_request_line_number = dr["sims_library_request_line_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_request_date"].ToString()))
                                simsobj.sims_library_request_date = db.UIDDMMYYYYformat(dr["sims_library_request_date"].ToString());
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_qty = dr["sims_library_item_qty"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_amount"].ToString()))
                                simsobj.sims_library_item_amount = double.Parse(dr["sims_library_item_amount"].ToString());
                            else
                                simsobj.sims_library_item_amount = 0;
                            simsobj.sims_library_item_source_hint = dr["sims_library_item_source_hint"].ToString();
                            simsobj.sims_library_item_status = dr["sims_library_item_status"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                    else
                    {
                        message.strMessage = "Sorry No Records Found";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            catch (Exception x)
            {
                Log.Error(x);
                //return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("Get_RequestDetail")]
        public HttpResponseMessage Get_RequestDetail(string user_code, string date1, string date2, string data, string reqno)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_RequestDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "Get_RequestDetail"));
            Message message = new Message();
            List<Sims129> goaltarget_list = new List<Sims129>();
            //int total = 0, skip = 0;

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            if (date1 == "undefined" || date1 == "\"\"")
            {
                date1 = null;
            }
            if (date2 == "undefined" || date2 == "\"\"")
            {
                date2 = null;
            }
            if (reqno == "undefined" || reqno == "\"\"")
            {
                reqno = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_request_detail_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@sims_library_user_code",user_code),
                           new SqlParameter("@date1",db.DBYYYYMMDDformat(date1)),
                           new SqlParameter("@date2",db.DBYYYYMMDDformat(date2)),
                           new SqlParameter("@data",data),
                           new SqlParameter("@reqno",reqno)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims129 simsobj = new Sims129();
                            simsobj.sims_library_request_number = dr["sims_library_request_number"].ToString();
                            simsobj.sims_library_request_line_number = dr["sims_library_request_line_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_request_date"].ToString()))
                                simsobj.sims_library_request_date = db.UIDDMMYYYYformat(dr["sims_library_request_date"].ToString());
                            //simsobj.sims_library_request_date = DateTime.Parse(dr["sims_library_request_date"].ToString()).ToShortDateString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            //simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            //simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_qty = dr["sims_library_item_qty"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_amount"].ToString()))
                                simsobj.sims_library_item_amount = double.Parse(dr["sims_library_item_amount"].ToString());
                            else
                                simsobj.sims_library_item_amount = 0;
                            //simsobj.sims_library_item_source_hint = dr["sims_library_item_source_hint"].ToString();
                            simsobj.sims_library_item_status = dr["sims_library_item_status"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                    else
                    {
                        message.strMessage = "Sorry No Records Found";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            catch (Exception x)
            {
                Log.Error(x);
                //return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDLibraryRequestDetail")]
        public HttpResponseMessage CUDLibraryRequestDetail(List<Sims129> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims129 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_request_detail_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_request_number", simsobj.sims_library_request_number),
                                new SqlParameter("@sims_library_request_line_number", simsobj.sims_library_request_line_number),
                                new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                                new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                                new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                                new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                                new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                                new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                                new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                                new SqlParameter("@sims_library_item_qty", simsobj.sims_library_item_qty),
                                new SqlParameter("@sims_library_item_amount", simsobj.sims_library_item_amount),
                                new SqlParameter("@sims_library_item_source_hint", simsobj.sims_library_item_source_hint),
                                new SqlParameter("@sims_library_item_status", simsobj.sims_library_item_status),
                                                                                             
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }


                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
        
    }

}