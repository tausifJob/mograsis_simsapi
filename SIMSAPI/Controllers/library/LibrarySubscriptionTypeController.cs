﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibrarySubscriptionType")]
    [BasicAuthentication]
    public class LibrarySubscriptionTypeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Seelct
        [Route("getLibrarySubscriptionType")]
        public HttpResponseMessage getLibrarySubscriptionType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLibrarySubscriptionType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getLibrarySubscriptionType"));

            List<Sims134> goaltarget_list = new List<Sims134>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_subscription_type]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims134 simsobj = new Sims134();
                            simsobj.sims_library_subscription_code = int.Parse(dr["sims_library_subscription_code"].ToString());
                            simsobj.sims_library_subscription_name = dr["sims_library_subscription_name"].ToString();
                            simsobj.sims_library_subscription_status = dr["sims_library_subscription_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }




        [Route("CUDLibrarySubscriptionType")]
        public HttpResponseMessage CUDLibraryAttribute(List<Sims134> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims134 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_subscription_type]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_subscription_code",simsobj.sims_library_subscription_code),
                                new SqlParameter("@sims_library_subscription_name", simsobj.sims_library_subscription_name),
                                new SqlParameter("@sims_library_subscription_status", simsobj.sims_library_subscription_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {                          
                            insert = true;
                        }

                        else
                        {                          
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {                

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }

}