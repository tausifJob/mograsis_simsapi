﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryUserHistory")]
    [BasicAuthentication]
    public class LibraryUserHistoryController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculumLib")]
        public HttpResponseMessage getCuriculumLib()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAllGradesNewLib")]
        public HttpResponseMessage getAllGradesNewLib(string cur_code, string academic_year, string user)

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[library_user_history_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                                new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGradeNewLib")]
        public HttpResponseMessage getSectionFromGradeNewLib(string cur_code, string grade_code, string academic_year, string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGradeNew(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[library_user_history_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "X"),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@academic_year", academic_year),
                                 new SqlParameter("@grade_code", grade_code),
                                  new SqlParameter ("@emp_code",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }



        [Route("GetAllLibraryStudentHistory")]
        public HttpResponseMessage GetAllLibraryStudentHistory(string enroll, string cur_code, string acad_year, string grade, string section, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLibraryStudentHistory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllLibraryStudentHistory"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            if (enroll == "undefined" || enroll == "\"\"")
            {
                enroll = null;
            }

            if (from_date == "undefined" || from_date == "\"\"")
            {
                from_date = null;
            }

            if (to_date == "undefined" || to_date == "\"\"")
            {
                to_date = null;
            }

            if (grade == "undefined" || grade == "\"\"")
            {
                grade = null;
            }

            if (section == "undefined" || section == "\"\"")
            {
                section = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[library_user_history_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@enroll", enroll),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                            simsobj.sims_library_transaction_line_number = dr["sims_library_transaction_line_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            //simsobj.sims_library_transaction_date = db.UIDDMMYYYYformat(dr["sims_library_transaction_date"].ToString());
                            simsobj.sims_library_item_issue_date = db.UIDDMMYYYYformat(dr["sims_library_item_issue_date"].ToString());
                            simsobj.sims_library_item_expected_return_date = db.UIDDMMYYYYformat(dr["sims_library_item_expected_return_date"].ToString());
                            simsobj.sims_library_item_actual_return_date = db.UIDDMMYYYYformat(dr["sims_library_item_actual_return_date"].ToString());
                            //simsobj.transaction_status = dr["sims_library_item_transaction_status"].Equals("A") ? true : false;
                            simsobj.transaction_status = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.student_enroll = dr["student_enroll"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("GetAllLibraryEmployeeHistory")]
        public HttpResponseMessage GetAllLibraryEmployeeHistory(string emcode, string cur_code, string acad_year, string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllLibraryEmployeeHistory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetAllLibraryEmployeeHistory"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            if (emcode == "undefined" || emcode == "\"\"")
            {
                emcode = null;
            }
            if (from_date == "undefined" || from_date == "\"\"")
            {
                from_date = null;
            }
            if (to_date == "undefined" || to_date == "\"\"")
            {
                to_date = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[library_user_history_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@em_number", emcode),
                           new SqlParameter("@cur_code", cur_code),
                           new SqlParameter("@acad_year", acad_year),
                           new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date))
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                            simsobj.sims_library_transaction_line_number = dr["sims_library_transaction_line_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            //simsobj.sims_library_transaction_date = db.UIDDMMYYYYformat(dr["sims_library_transaction_date"].ToString());
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();                            
                            simsobj.sims_teacher_full_name = dr["sims_library_user_first_name"].ToString() +" "+ dr["sims_library_user_middle_name"].ToString() +" " + dr["sims_library_user_last_name"].ToString();
                            
                            simsobj.sims_library_item_issue_date = db.UIDDMMYYYYformat(dr["sims_library_item_issue_date"].ToString());
                            simsobj.sims_library_item_expected_return_date = db.UIDDMMYYYYformat(dr["sims_library_item_expected_return_date"].ToString());
                            simsobj.sims_library_item_actual_return_date = db.UIDDMMYYYYformat(dr["sims_library_item_actual_return_date"].ToString());
                            //simsobj.transaction_status = dr["sims_library_item_transaction_status"].Equals("A") ? true : false;
                            simsobj.transaction_status = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


    }
}