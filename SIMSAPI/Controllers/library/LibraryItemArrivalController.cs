﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;
using System.Data;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemArrival")]
    public class LibraryItemArrivalController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("GetNewArrivalItems")]
        public HttpResponseMessage GetNewArrivalItems()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_Location(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetNewArrivalItems"));

            List<Sims720> goaltarget_list = new List<Sims720>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_arrival_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims720 simsobj = new Sims720();
                            simsobj.sims_library_item_srl_no = dr["sims_library_item_srl_no"].ToString();
                            simsobj.sims_library_item_transaction_number = dr["sims_library_item_transaction_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();                           
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_from_date"].ToString()))
                                simsobj.sims_library_item_from_date = db.UIDDMMYYYYformat(dr["sims_library_item_from_date"].ToString());
                           // simsobj.sims_library_item_from_date = dr["sims_library_item_from_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_to_date"].ToString()))
                                simsobj.sims_library_item_to_date = db.UIDDMMYYYYformat(dr["sims_library_item_to_date"].ToString());
                           // simsobj.sims_library_item_to_date = dr["sims_library_item_to_date"].ToString();
                            simsobj.sims_library_item_created_by = dr["sims_library_item_created_by"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_created_date"].ToString()))
                                simsobj.sims_library_item_created_date = db.UIDDMMYYYYformat(dr["sims_library_item_created_date"].ToString());
                           // simsobj.sims_library_item_created_date = dr["sims_library_item_created_date"].ToString();
                            simsobj.sims_library_cur_code = dr["sims_library_cur_code"].ToString();
                            simsobj.sims_library_cur_name = dr["sims_library_cur_name"].ToString();
                            simsobj.sims_library_academic_year = dr["sims_library_academic_year"].ToString();
                            simsobj.sims_library_grade_code = dr["sims_library_grade_code"].ToString();
                            simsobj.sims_library_grade_name = dr["sims_library_grade_name"].ToString();
                            simsobj.sims_library_section = dr["sims_library_section"].ToString();
                            simsobj.sims_library_section_name = dr["sims_library_section_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_transaction_date"].ToString()))
                                simsobj.sims_library_item_transaction_date = db.UIDDMMYYYYformat(dr["sims_library_item_transaction_date"].ToString());
                           // simsobj.sims_library_item_transaction_date = dr["sims_library_item_transaction_date"].ToString();
                            simsobj.sims_library_item_status = dr["sims_library_item_status"].Equals("A") ? true : false;
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_author = dr["sims_library_item_author"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDNewArrivalItems")]
        public HttpResponseMessage CUDNewArrivalItems(List<Sims720> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims720 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_item_arrival_proc]",
                        new List<SqlParameter>()
                     {
                             new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_library_item_srl_no", simsobj.sims_library_item_srl_no),
                             new SqlParameter("@sims_library_item_transaction_number", simsobj.sims_library_item_transaction_number),
                             new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                             new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),   
                             new SqlParameter("@sims_library_item_from_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_from_date)),
                             new SqlParameter("@sims_library_item_to_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_to_date)),
                             new SqlParameter("@sims_library_item_created_by", simsobj.sims_library_item_created_by),
                             new SqlParameter("@sims_library_item_created_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_created_date)),
                             new SqlParameter("@sims_library_cur_code",simsobj.sims_library_cur_code),
                             new SqlParameter("@sims_library_academic_year", simsobj.sims_library_academic_year),
                             new SqlParameter("@sims_library_grade_code", simsobj.sims_library_grade_code),
                             new SqlParameter("@sims_library_section", simsobj.sims_library_section),
                             new SqlParameter("@sims_library_item_transaction_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_transaction_date)),
                             new SqlParameter("@sims_library_item_status", simsobj.sims_library_item_status==true? "A":"I"),
                             new SqlParameter("@TransStatus", simsobj.transactionstatus)
                     });
                        if (ins > 0)
                        {                           
                            insert = true;
                        }
                        else
                        {                          
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }
            catch (Exception x)
            {           
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }           
        }
    }
}