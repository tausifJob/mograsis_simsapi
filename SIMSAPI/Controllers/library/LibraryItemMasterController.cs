﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;
using System.Data;
using System.Web;
using System.IO;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryItemMaster")]
  //  [BasicAuthentication]
    public class LibraryItemMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Select
        [Route("GetSims_Location")]
        public HttpResponseMessage GetSims_Location()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_Location(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_Location"));

            List<Sims121> goaltarget_list = new List<Sims121>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_location]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_location_code = dr["sims_library_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_location_name"].ToString();
                            //simsobj.sims_library_attribute_status = dr["sims_library_attribute_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSims_Currency")]
        public HttpResponseMessage GetSims_Currency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_Currency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_Currency"));

            List<Sims121> goaltarget_list = new List<Sims121>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_currency_master]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_curcy_code = dr["excg_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["excg_curcy_desc"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetLibraryCategory")]
        public HttpResponseMessage GetLibraryCategory()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_category",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_category = dr["sims_library_category_code"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

       
        [Route("getalldetailsFeatch")]
        public HttpResponseMessage getalldetailsFeatch(string AccessNo)
        {
            List<Sims121> type_list = new List<Sims121>();           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_item_master_proc",
                   new List<SqlParameter>(){
                      new SqlParameter ("@opr", "GA"),
                      new SqlParameter ("@sims_library_isbn_number", AccessNo),
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();
                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString());
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                                simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                                simsobj.sims_library_item_entry_date = db.UIDDMMYYYYformat(dr["sims_library_item_entry_date"].ToString());
                            simsobj.sims_library_item_supplier = dr["sims_library_item_supplier"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }
        [Route("GetLibrarySubCategory")]
        public HttpResponseMessage GetLibrarySubCategory(string SubCategory)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_subcategory_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'K'),
                       new SqlParameter ("@sims_library_category_code",SubCategory)
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_subcategory = dr["sims_library_subcategory_code"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLibraryCatelogue")]
        public HttpResponseMessage GetLibraryCatelogue()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_catelogue",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLanguageCode")]
        public HttpResponseMessage GetLanguageCode()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_language",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_language_code = dr["sims_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetBookStyle")]
        public HttpResponseMessage GetBookStyle()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_item_style",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_style_code"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetBookStatus")]
        public HttpResponseMessage GetBookStatus()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_request",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'F')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_status_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_appl_form_field_value1"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetBin")]
        public HttpResponseMessage GetBin(string location_code)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'J'), 
                       new SqlParameter("@sims_library_location_code",location_code)
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_bin_code = dr["sims_library_bin_code"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_bin_desc"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetCurcode")]
        public HttpResponseMessage GetCurcode()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_short_name_en"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetCurLevelcode")]
        public HttpResponseMessage GetCurLevelcode(string curlevel)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur_level",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetCurGradecode")]
        public HttpResponseMessage GetCurGradecode(string cur, string level_code)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_cur_level_grade]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'G'),
                       new SqlParameter ("@cur_code",cur),
                       new SqlParameter ("@level_code",level_code)
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["grade_name"].ToString();
                            //simsobj.sims_cur_level_name_en = dr["sims_cur_level_name_en"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }
       
        [Route("CheckAccNo")]
        public HttpResponseMessage CheckAccNo(string AccessNo)
        {
            List<Sims121> type_list = new List<Sims121>();
            bool isexist = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_item_master_proc",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'K'),
                       new SqlParameter ("@acc_no", AccessNo)
                   });
                    if (dr.HasRows)
                    {
                        isexist = true;
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, isexist);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, isexist);
        }

        [Route("ItemSearch_ASD")]
        public HttpResponseMessage ItemSearch_ASD(Sims121 obj)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();
            if (obj.sims_library_item_category == "undefined" || obj.sims_library_item_category == "")
            {
                obj.sims_library_item_category = null;
            }
            if (obj.sims_library_item_subcategory == "undefined" || obj.sims_library_item_subcategory == "")
            {
                obj.sims_library_item_subcategory = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){               
                 new SqlParameter("@opr", 'S'),
                 new SqlParameter ("@sims_library_item_category",obj.sims_library_item_category),
                 new SqlParameter ("@sims_library_item_subcategory",obj.sims_library_item_subcategory),
                
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();


                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();


                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_purchase_price"].ToString()))
                            //{
                            //    simsobj.sims_library_item_purchase_price = dr["sims_library_item_curcy_code"].ToString() + " " + dr["sims_library_item_purchase_price"].ToString(); 
                            //}
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString());
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                               simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                                simsobj.sims_library_item_entry_date = db.UIDDMMYYYYformat(dr["sims_library_item_entry_date"].ToString());
                            simsobj.sims_library_item_supplier = dr["sims_library_item_supplier"].ToString();
                           
                          //  simsobj.sims_library_year_of_publication = dr["sims_library_year_of_publication"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_status_update_date"].ToString()))
                          //      simsobj.sims_library_item_status_update_date = db.UIDDMMYYYYformat(dr["sims_library_item_status_update_date"].ToString()) ;
                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("ItemSearch")]
        public HttpResponseMessage ItemSearch(Sims121 obj)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();
            if (obj.sims_library_item_category == "undefined" || obj.sims_library_item_category == "")
            {
                obj.sims_library_item_category = null;
            }
            if (obj.sims_library_item_subcategory == "undefined" || obj.sims_library_item_subcategory == "")
            {
                obj.sims_library_item_subcategory = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){
                 new SqlParameter("@opr", 'S'),
                 new SqlParameter ("@sims_library_item_category",obj.sims_library_item_category),
                 new SqlParameter ("@sims_library_item_subcategory",obj.sims_library_item_subcategory),

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();


                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();


                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price =  dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString());
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                                simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                                simsobj.sims_library_item_entry_date = db.UIDDMMYYYYformat(dr["sims_library_item_entry_date"].ToString());
                            simsobj.sims_library_item_supplier = dr["sims_library_item_supplier"].ToString();

                            //  simsobj.sims_library_year_of_publication = dr["sims_library_year_of_publication"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_status_update_date"].ToString()))
                            //      simsobj.sims_library_item_status_update_date = db.UIDDMMYYYYformat(dr["sims_library_item_status_update_date"].ToString()) ;
                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDLibraryItemMaster")]
        public HttpResponseMessage CUDLibraryItemMaster(List<Sims121> data)
        {
            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            string opts = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {                         
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode),                       
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),              
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)),               
                     });
                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}


                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (simsobj.opr == "I")
                        {

                            while (DR.Read())
                            {
                                Sims121 obj = new Sims121();
                                obj.accno = DR["sims_library_item_accession_number"].ToString();
                                obj.title = DR["sims_library_item_title"].ToString();
                                
                                count = count + 1;
                                inserted.Add(obj);
                            }
                            //inserted = inserted + "Accession No=" + accno + " " + "Title= " + title + "<br/>";
                        }
                        else if ((simsobj.opr == "U"))
                        {
                            
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        else if ((simsobj.opr == "D"))
                        {
                            opts = simsobj.opr;
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            //return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        DR.Close();
                        //inserted = inserted + "Accession No=" + accno + " " + "Title= "+ title+"<br/>";
                    }
                    if (opts == "D")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, updated);
                    }
                   return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDLibraryItemMasterNEW")]
        public HttpResponseMessage CUDLibraryItemMasterNEW(List<Sims121>[] dataarr)
        {           
            List<Sims121> attribute = dataarr[0];
            List<Sims121> data = dataarr[1];
           
            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {                         
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode),                       
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),              
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)), 
                          new SqlParameter("@sims_library_item_supplier",simsobj.sims_library_item_supplier),
                      //    new SqlParameter("@sims_library_item_status_update_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_status_update_date))
                     });
                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}


                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (simsobj.opr == "I")
                        {

                            while (DR.Read())
                            {
                                Sims121 obj = new Sims121();
                                obj.accno = DR["sims_library_item_accession_number"].ToString();
                                obj.title = DR["sims_library_item_title"].ToString();
                                obj.sims_library_item_number = DR["sims_library_item_number"].ToString();
                                foreach (Sims121 att in attribute)
                                {
                                    using (DBConnection tempdb = new DBConnection())
                                    {
                                        tempdb.Open();
                                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                        SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                        new List<SqlParameter>()
                                            {                         
                                            new SqlParameter("@opr", "UA"),
                                            new SqlParameter("@sims_library_item_number", obj.sims_library_item_number),
                                            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                            });
                                    }
                                }
                                count = count + 1;
                                inserted.Add(obj);
                            }
                            if (inserted.Count==0)
                            {
                                Sims121 obj = new Sims121();
                                int ins = DR.RecordsAffected;
                                if (ins > 0)
                                {                                    
                                      //obj.accno = DR["sims_library_item_accession_number"].ToString();
                                    //obj.title = DR["sims_library_item_title"].ToString();
                                    //obj.sims_library_item_number = DR["sims_library_item_number"].ToString();
                                    obj.insertedFlag = true;
                                    inserted.Add(obj);
                                }
                                else
                                {
                                    obj.insertedFlag = false;
                                    inserted.Add(obj);
                                } 
                            }
                          
                        }
                        else if ((simsobj.opr == "U"))
                        {
                            foreach (Sims121 att in attribute)
                            {
                                using (DBConnection tempdb = new DBConnection())
                                {
                                    tempdb.Open();
                                    //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                    SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                    new List<SqlParameter>()
                                            {                         
                                            new SqlParameter("@opr", "UA"),
                                            new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                                            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                            });
                                }
                            }
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        else if ((simsobj.opr == "D"))
                        {
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        //DR.Close();
                        //inserted = inserted + "Accession No=" + accno + " " + "Title= "+ title+"<br/>";
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDLibraryItemMasterNEWSMFC")]
        public HttpResponseMessage CUDLibraryItemMasterNEWSMFC(List<Sims121>[] dataarr)
        {
            List<Sims121> attribute = dataarr[0];
            List<Sims121> data = dataarr[1];

            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title_temp", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)),
                          new SqlParameter("@sims_library_item_supplier",simsobj.sims_library_item_supplier),
                      //    new SqlParameter("@sims_library_item_status_update_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_status_update_date))
                     });
                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}


                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (simsobj.opr == "I")
                        {

                            while (DR.Read())
                            {
                                Sims121 obj = new Sims121();
                                obj.accno = DR["sims_library_item_accession_number"].ToString();
                                obj.title = DR["sims_library_item_title"].ToString();
                                obj.sims_library_item_number = DR["sims_library_item_number"].ToString();
                                foreach (Sims121 att in attribute)
                                {
                                    using (DBConnection tempdb = new DBConnection())
                                    {
                                        tempdb.Open();
                                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                        SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                        new List<SqlParameter>()
                                            {
                                            new SqlParameter("@opr", "UA"),
                                            new SqlParameter("@sims_library_item_number", obj.sims_library_item_number),
                                            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                            });
                                    }
                                }
                                count = count + 1;
                                inserted.Add(obj);
                            }
                            if (inserted.Count == 0)
                            {
                                Sims121 obj = new Sims121();
                                int ins = DR.RecordsAffected;
                                if (ins > 0)
                                {
                                    //obj.accno = DR["sims_library_item_accession_number"].ToString();
                                    //obj.title = DR["sims_library_item_title"].ToString();
                                    //obj.sims_library_item_number = DR["sims_library_item_number"].ToString();
                                    obj.insertedFlag = true;
                                    inserted.Add(obj);
                                }
                                else
                                {
                                    obj.insertedFlag = false;
                                    inserted.Add(obj);
                                }
                            }

                        }
                        else if ((simsobj.opr == "U"))
                        {
                            foreach (Sims121 att in attribute)
                            {
                                using (DBConnection tempdb = new DBConnection())
                                {
                                    tempdb.Open();
                                    //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                    SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                    new List<SqlParameter>()
                                            {
                                            new SqlParameter("@opr", "UA"),
                                            new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                                            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                            });
                                }
                            }
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        else if ((simsobj.opr == "D"))
                        {
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        //DR.Close();
                        //inserted = inserted + "Accession No=" + accno + " " + "Title= "+ title+"<br/>";
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDLibraryItemMasterNEW_dps")]
        public HttpResponseMessage CUDLibraryItemMasterNEW_dps(List<Sims121>[] dataarr)
        {

            string d = null;
            List<Sims121> attribute = dataarr[0];
            List<Sims121> data = dataarr[1];

            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode),
                          
                          new SqlParameter("@sims_appl_parameter", simsobj.sims_appl_parameter),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)),
                     });
                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}


                        //else
                        //{
                        //    inserted = false;
                        //}
                        if (simsobj.opr == "I")
                        {

                            while (DR.Read())
                            {
                                Sims121 obj = new Sims121();
                                obj.accno = DR["sims_library_item_accession_number"].ToString();
                                obj.title = DR["sims_library_item_title"].ToString();
                               // obj.sims_library_item_number = DR["sims_library_item_number"].ToString();
                                //foreach (Sims121 att in attribute)
                                //{
                                //    using (DBConnection tempdb = new DBConnection())
                                //    {
                                //        tempdb.Open();
                                //        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                //        SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                //        new List<SqlParameter>()
                                //            {
                                //            new SqlParameter("@opr", "UA"),
                                //            new SqlParameter("@sims_library_item_number", obj.sims_library_item_number),
                                //            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                //            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                //            });
                                //    }
                                //}
                                //count = count + 1;
                                 inserted.Add(obj);
                               //  d =  "Accession No=" + obj.accno + " " + "Title= " + obj.title + "<br/>";
                                // message.strMessage = d;

                            }
                            d = string.Empty;
                            foreach (Sims121 obj in inserted)
                            {
                                d = "Accession No:" + obj.accno + " " + "Title: " + obj.title + "<br/>";
                                message.strMessage = message.strMessage+ d;
                                d = string.Empty;
                            }

                        }
                        else if ((simsobj.opr == "U"))
                        {
                            foreach (Sims121 att in attribute)
                            {
                                using (DBConnection tempdb = new DBConnection())
                                {
                                    tempdb.Open();
                                    //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                                    SqlDataReader temp = tempdb.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                                    new List<SqlParameter>()
                                            {
                                            new SqlParameter("@opr", "UA"),
                                            new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                                            new SqlParameter("@sims_library_item_catalogue_code", att.sims_library_attribute_code),
                                            new SqlParameter("@sims_library_attribute_value", att.sims_library_attribute_value)
                                            });
                                }
                            }
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        else if ((simsobj.opr == "D"))
                        {
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        DR.Close();
                        //inserted = inserted + "Accession No:" + accno + " " + "Title: "+ title+"<br/>";
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUDLibraryItemMasterNEW_ads")]
        public HttpResponseMessage CUDLibraryItemMasterNEW_ads(List<Sims121> data)
        {
            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            string opts = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        #region
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {                         
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode), 
                          new SqlParameter("@sims_appl_parameter", simsobj.sims_appl_parameter),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),              
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)),  
                          new SqlParameter("@sims_library_item_supplier",simsobj.sims_library_item_supplier),
                        //  new SqlParameter("@sims_library_year_of_publication",simsobj.sims_library_year_of_publication)
                     });
                        //if (ins > 0)
                        //{
                        //    inserted = true;
                        //}


                        //else
                        //{
                        //    inserted = false;
                        //}
                        #endregion
                        if (simsobj.opr == "I")
                        {

                            while (DR.Read())
                            {
                                Sims121 obj = new Sims121();
                                obj.accno = DR["sims_library_item_accession_number"].ToString();
                                obj.title = DR["sims_library_item_title"].ToString();

                                count = count + 1;
                                inserted.Add(obj);
                            }
                            //inserted = inserted + "Accession No=" + accno + " " + "Title= " + title + "<br/>";
                        }
                        else if ((simsobj.opr == "U"))
                        {

                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        else if ((simsobj.opr == "D"))
                        {
                            opts = simsobj.opr;
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        DR.Close();
                        //inserted = inserted + "Accession No=" + accno + " " + "Title= "+ title+"<br/>";
                    }
                    if (opts == "D")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, inserted);
 
          
        }


        [Route("CUDLibraryItemMasterNEW1")]
        public HttpResponseMessage CUDLibraryItemMasterNEW1(List<Sims121> data)
        {


            
          //  List<Sims121> data = dataarr;

            Message message = new Message();
            bool updated = false;
            int count = 0;
            //string updated = "";
            //string accno = "";
            //string title = "";
            List<Sims121> inserted = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims121 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_item_master_proc]",
                        SqlDataReader DR = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                     {
                          new SqlParameter("@opr", simsobj.opr),
                          new SqlParameter("@opr_sub", simsobj.insert_mode),
                          new SqlParameter("@sims_library_item_number", simsobj.sims_library_item_number),
                          new SqlParameter("@sims_library_item_accession_number", simsobj.sims_library_item_accession_number),
                          new SqlParameter("@sims_library_isbn_number", simsobj.sims_library_isbn_number),
                          new SqlParameter("@sims_library_bnn_number", simsobj.sims_library_bnn_number),
                          new SqlParameter("@sims_library_item_dewey_number", simsobj.sims_library_item_dewey_number),
                          new SqlParameter("@sims_library_item_name", simsobj.sims_library_item_name),
                          new SqlParameter("@sims_library_item_title", simsobj.sims_library_item_title),
                          new SqlParameter("@sims_library_item_desc", simsobj.sims_library_item_desc),
                          new SqlParameter("@sims_library_item_category", simsobj.sims_library_item_category),
                          new SqlParameter("@sims_library_item_subcategory", simsobj.sims_library_item_subcategory),
                          new SqlParameter("@sims_library_item_catalogue_code", simsobj.sims_library_item_catalogue_code),
                          new SqlParameter("@sims_library_item_cur_code", simsobj.sims_library_item_cur_code),
                          new SqlParameter("@sims_library_item_cur_level_code", simsobj.sims_library_item_cur_level_code),
                          new SqlParameter("@sims_library_item_grade_code", simsobj.sims_library_item_grade_code),
                          new SqlParameter("@sims_library_item_language_code", simsobj.sims_library_item_language_code),
                          new SqlParameter("@sims_library_item_remark", simsobj.sims_library_item_remark),
                          new SqlParameter("@sims_library_item_author1", simsobj.sims_library_item_author1),
                          new SqlParameter("@sims_library_item_author2", simsobj.sims_library_item_author2),
                          new SqlParameter("@sims_library_item_author3", simsobj.sims_library_item_author3),
                          new SqlParameter("@sims_library_item_author4", simsobj.sims_library_item_author4),
                          new SqlParameter("@sims_library_item_author5", simsobj.sims_library_item_author5),
                          new SqlParameter("@sims_library_item_age_from", simsobj.sims_library_item_age_from),
                          new SqlParameter("@sims_library_item_age_to", simsobj.sims_library_item_age_to),
                          new SqlParameter("@sims_library_item_edition_statement", simsobj.sims_library_item_edition_statement),
                          new SqlParameter("@sims_library_item_place_of_publication", simsobj.sims_library_item_place_of_publication),
                          new SqlParameter("@sims_library_item_name_of_publisher", simsobj.sims_library_item_name_of_publisher),
                          new SqlParameter("@sims_library_item_date_of_publication",db.DBYYYYMMDDformat(simsobj.sims_library_item_date_of_publication)),
                          new SqlParameter("@sims_library_item_extent_of_item", simsobj.sims_library_item_extent_of_item),
                          new SqlParameter("@sims_library_item_accompanying_item", simsobj.sims_library_item_accompanying_item),
                          new SqlParameter("@sims_library_item_printing_date",db.DBYYYYMMDDformat(simsobj.sims_library_item_printing_date)),
                          new SqlParameter("@sims_library_item_qr_code", simsobj.sims_library_item_qr_code),
                          new SqlParameter("@sims_library_item_bar_code", simsobj.sims_library_item_bar_code),
                          new SqlParameter("@sims_library_item_storage_hint", simsobj.sims_library_item_storage_hint),
                          new SqlParameter("@sims_library_item_purchase_price", simsobj.sims_library_item_purchase_price),
                          new SqlParameter("@sims_library_item_keywords", simsobj.sims_library_item_keywords),
                          new SqlParameter("@sims_library_item_number_of_copies", simsobj.sims_library_item_number_of_copies),
                          new SqlParameter("@sims_library_item_known_as", simsobj.sims_library_item_known_as),
                          new SqlParameter("@sims_library_item_book_style", simsobj.sims_library_item_book_style),
                          new SqlParameter("@sims_library_item_status_code", simsobj.sims_library_item_status_code),
                          new SqlParameter("@sims_library_item_curcy_code", simsobj.sims_library_item_curcy_code),
                          //new SqlParameter("@sims_library_item_quantity", simsobj.sims_library_item_qty),
                          new SqlParameter("@sims_library_item_location_code", simsobj.sims_library_item_location_code),
                          new SqlParameter("@sims_library_item_bin_code", simsobj.sims_library_item_bin_code),
                          new SqlParameter("@sims_library_item_entry_date", db.DBYYYYMMDDformat(simsobj.sims_library_item_entry_date)),
                     });
                         if ((simsobj.opr == "D"))
                        {
                            int ins = DR.RecordsAffected;
                            if (ins > 0)
                            {
                                updated = true;
                            }
                            else
                            {
                                updated = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, updated);
                        }
                        DR.Close();
                        //inserted = inserted + "Accession No=" + accno + " " + "Title= "+ title+"<br/>";
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("GetLibItemAttValues")]
        public HttpResponseMessage GetLibItemAttValues(string opr, string catalogue, string item_code)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", "SA"),
                       new SqlParameter ("@opr_sub", opr.Equals("I")?"N":"O" ),
                       new SqlParameter ("@sims_library_item_catalogue_code", catalogue),
                       new SqlParameter ("@sims_library_item_number", item_code)
                   });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_attribute_code = dr["sims_library_attribute_code"].ToString();
                            simsobj.sims_library_attribute_name = dr["sims_library_attribute_name"].ToString();
                            simsobj.sims_library_attribute_value = dr["sims_library_attribute_value"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetAccessUser")]
        public HttpResponseMessage GetAccessUser(string user_code)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'B'),
                       new SqlParameter ("@code", user_code)
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.accno = dr["access_per"].ToString();
                           
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }


        [Route("Getlibrarywings")]
        public HttpResponseMessage Getlibrarywings()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'H'),
                      
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_appl_form_field_value2 = dr["sims_appl_form_field_value2"].ToString();

                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/Images/LibraryItem/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    //{
                    //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                    //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                    // SAVE THE FILES IN THE FOLDER.
                    //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                    string type = Path.GetFileName(hpf.ContentType);
                    file = filename;// +"." + type;
                    hpf.SaveAs((root + filename));//+ "." + type));
                    fname = Path.GetFileName(filename);
                    iUploadedCnt = iUploadedCnt + 1;
                    //   }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }
    }
}