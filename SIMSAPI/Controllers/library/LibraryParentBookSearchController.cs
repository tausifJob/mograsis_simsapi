﻿using log4net;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.ParentClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryParentBookSearch")]
    [BasicAuthentication]
    public class LibraryParentBookSearchController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[Route("GetLibraryCategory")]
        //public HttpResponseMessage GetLibraryCategory()
        //{
        //    List<Sims121> type_list = new List<Sims121>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_category",
        //           new List<SqlParameter>(){
        //               new SqlParameter ("@opr", 'S')
        //           });


        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims121 simsobj = new Sims121();
        //                    simsobj.sims_library_item_category = dr["sims_library_category_code"].ToString();
        //                    simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
        //                    type_list.Add(simsobj);
        //                }
        //            }
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, type_list);
        //    }
        //    catch (Exception e)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, type_list);
        //}

        //[Route("GetLibrarySubCategory")]
        //public HttpResponseMessage GetLibrarySubCategory(string SubCategory)
        //{
        //    List<Sims121> type_list = new List<Sims121>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_subcategory_proc]",
        //           new List<SqlParameter>(){
        //               new SqlParameter ("@opr", 'K'),
        //               new SqlParameter ("@sims_library_category_code",SubCategory)
        //           });


        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims121 simsobj = new Sims121();
        //                    simsobj.sims_library_item_subcategory = dr["sims_library_subcategory_code"].ToString();
        //                    simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
        //                    type_list.Add(simsobj);
        //                }
        //            }
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, type_list);
        //    }
        //    catch (Exception e)
        //    {
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, type_list);
        //}

        [Route("GetLibraryCategory")]
        public HttpResponseMessage GetLibraryCategory()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_parent_book_search_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'B')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_category = dr["sims_library_category_code"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetLibrarySubCategory")]
        public HttpResponseMessage GetLibrarySubCategory(string SubCategory)
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (var item in SubCategory)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_parent_book_search_proc]",
                       new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'C'),
                       new SqlParameter ("@sims_library_category_code", SubCategory)
                   });


                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims121 simsobj = new Sims121();
                                simsobj.sims_library_item_subcategory = dr["sims_library_subcategory_code"].ToString();
                                simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                                type_list.Add(simsobj);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }



        [Route("GetBookStyle")]
        public HttpResponseMessage GetBookStyle()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_parent_book_search_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'S')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_style_code"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

        [Route("GetBookStatus")]
        public HttpResponseMessage GetBookStatus()
        {
            List<Sims121> type_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_parent_book_search_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'F')
                   });


                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_status_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_appl_form_field_value1"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, type_list);
        }

      
        [Route("CheckAccNo")]
        public HttpResponseMessage CheckAccNo(string AccessNo)
        {
            List<Sims121> type_list = new List<Sims121>();
            bool isexist = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_parent_book_search_proc]",
                   new List<SqlParameter>(){
                       new SqlParameter ("@opr", 'K'),
                       new SqlParameter ("@acc_no", AccessNo)
                   });
                    if (dr.HasRows)
                    {
                        isexist = true;
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, isexist);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, isexist);
        }

        [Route("ItemSearch")]
        public HttpResponseMessage ItemSearch(Sims121 obj)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();
            if (obj.sims_library_item_category == "undefined" || obj.sims_library_item_category == "")
            {
                obj.sims_library_item_category = null;
            }
            if (obj.sims_library_item_subcategory == "undefined" || obj.sims_library_item_subcategory == "")
            {
                obj.sims_library_item_subcategory = null;
            }
            if (obj.sims_library_item_title == "undefined" || obj.sims_library_item_title == "")
            {
                obj.sims_library_item_title = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_item_search_proc]",
                    new List<SqlParameter>(){               
                    new SqlParameter("@opr", 'A'),
                    new SqlParameter ("@sims_library_item_category",obj.sims_library_item_category),
                    new SqlParameter ("@sims_library_item_subcategory",obj.sims_library_item_subcategory),
                    new SqlParameter ("@sims_library_item_title",obj.sims_library_item_title)

                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();


                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();

                           //simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                           
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                            //    simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                           
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            
                            simsobj.sims_library_item_status_code = (dr["sims_library_item_status_code"].ToString());
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                          
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                            //    simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            

                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}