﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryBookReviewApprove")]
    public class LibraryBookReview_ApproveController : ApiController
    {

        [Route("get_grid")]
        public HttpResponseMessage get_grid()
        {

            List<Sims849> lst = new List<Sims849>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_Approve_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                      
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims849 simsobj = new Sims849();

                            simsobj.id = dr["id"].ToString();
                            simsobj.login_id = dr["login_id"].ToString();
                            simsobj.rating = dr["rating"].ToString();
                            simsobj.comment = dr["comment"].ToString();
                            simsobj.accession_number = dr["accession_number"].ToString();
                            simsobj.approve_status = dr["approve_status"].ToString().Equals("A") ? true : false;
                            simsobj.display_cmt_flag = dr["display_cmt_flag"].ToString().Equals("A") ? true : false;

                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            //simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            //simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();

                            //simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString().Equals("A ") ? true : false;

                            //simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            //simsobj.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            //simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            //simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDLibBookReviewApprove")]
        public HttpResponseMessage CUDLibBookReview(List<Sims849> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims849 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Library_Book_review_Approve_PROC]",
                       // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_Approve_PROC]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@approve_status", simsobj.approve_status==true?"A":"I"),
                                new SqlParameter("@display_cmt_flag", simsobj.display_cmt_flag==true?"A":"I"),   
                                  new SqlParameter("@comment", simsobj.comment),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;

                        //if (dr.Read())
                        //{
                        //    st = dr[0].ToString();
                        //    message.strMessage = st;
                         // inserted = false;
                        //}
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

 

    }
}