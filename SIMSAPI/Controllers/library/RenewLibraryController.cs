﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/RenewLibrary")]
    [BasicAuthentication]
    public class RenewLibraryController :ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSearchteacher")]
        public HttpResponseMessage getSearchteacher(string libno, string teacherid, string emno, string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchteacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchteacher"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            //int total = 0, skip = 0;

            if (libno == "undefined" || libno == "\"\"")
            {
                libno = null;
            }
            if (teacherid == "undefined" || teacherid == "\"\"")
            {
                teacherid = null;
            }
            if (emno == "undefined" || emno == "\"\"")
            {
                emno = null;
            }
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'T'),
                           new SqlParameter("@libuser_no", libno),
                           new SqlParameter("@teacher_code", teacherid), 
                           new SqlParameter("@em_number", emno),
                           new SqlParameter("@data", data),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_library_user_number = dr["sims_library_user_number"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_joining_date"].ToString()))
                                simsobj.sims_library_joining_date = dr["sims_library_joining_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_expiry_date"].ToString()))
                                simsobj.sims_library_expiry_date = dr["sims_library_expiry_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_renewed_date"].ToString()))
                                simsobj.sims_library_renewed_date = dr["sims_library_renewed_date"].ToString();

                            simsobj.sims_status = dr["sims_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                    //else
                    //{
                    //    message.strMessage = "No Records Found.";
                    //    message.systemMessage = string.Empty;
                    //    message.messageType = MessageType.Success;
                    //}
                    //return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSearchemployee")]
        public HttpResponseMessage getSearchemployee(string emcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchemployee(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchemployee"));
            Message message = new Message();
            List<Sims503> goaltarget_list = new List<Sims503>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@em_number", emcode),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.em_sex = dr["em_sex"].ToString();
                            //simsobj.em_login_code = dr["em_login_code"].ToString();
                            //simsobj.sims_user_code = dr["sims_user_code"].ToString();
                            //simsobj.sims_teacher_type = dr["sims_teacher_type"].ToString();
                            simsobj.em_status = dr["em_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                    //else
                    //{
                    //    message.strMessage = "No Records Found.";
                    //    message.systemMessage = string.Empty;
                    //    message.messageType = MessageType.Success;
                    //}
                    //return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSearchparent")]
        public HttpResponseMessage getSearchparent(string emcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchparent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSearchparent"));

            List<Sims503> goaltarget_list = new List<Sims503>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'E'),
                           new SqlParameter("@em_number", emcode),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 simsobj = new Sims503();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.emp_name = dr["emp_name"].ToString();
                            simsobj.em_sex = dr["em_sex"].ToString();
                            //simsobj.em_login_code = dr["em_login_code"].ToString();
                            //simsobj.sims_user_code = dr["sims_user_code"].ToString();
                            //simsobj.sims_teacher_type = dr["sims_teacher_type"].ToString();
                            simsobj.em_status = dr["em_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSearchStudent")]
        public HttpResponseMessage getSearchStudent(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudent"));
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            //new SqlParameter("@opr_mem_code", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            //new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                            //new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            //new SqlParameter("@family_name", comnobj.search_std_family_name),
                            //new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            //new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@acad_year", comnobj.sims_academic_year),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_class = dr["sims_grade_name_en"].ToString();
                            objNew.joining_date = dr["sims_library_joining_date"].ToString();
                            objNew.exp_date = dr["sims_library_expiry_date"].ToString();
                            objNew.renew_date = dr["sims_library_renewed_date"].ToString();
                            objNew.user_name = dr["sims_library_user_number"].ToString();
                            objNew.Mobile_no = dr["sims_parent_father_mobile"].ToString();
                            objNew.Mail_id = dr["sims_parent_father_email"].ToString();
                            objNew.status_name = dr["sims_library_status"].ToString();
                            objNew.membership_type = dr["sims_library_membership_type"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getMembertype")]
        public HttpResponseMessage getMembertype()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMembertype(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getMembertype"));

            List<Sims503> search_list = new List<Sims503>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims503 objNew = new Sims503();
                            objNew.sims_library_membership_type_code = dr["sims_library_membership_type_code"].ToString();
                            objNew.sims_library_membership_type_name = dr["sims_library_membership_type_name"].ToString();
                                                
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }


        [Route("Update_RenewLibrary")]
        public HttpResponseMessage Update_RenewLibrary(List<Sims503> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims503 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'I'),                                
                                new SqlParameter("@sims_library_renewed_date", db.DBYYYYMMDDformat(simsobj.sims_library_renewed_date)),                                 
                                new SqlParameter("@sims_stud_Name", simsobj.s_enroll_no), 
                                new SqlParameter("@cur_code", simsobj.cur_code),
                                new SqlParameter("@acad_year",simsobj.academic_year),
                                new SqlParameter("@sims_library_membership_type",simsobj.sims_library_membership_type),
                                new SqlParameter("@param",'0'),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }


                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Updateteacher_RenewLibrary")]
        public HttpResponseMessage Updateteacher_RenewLibrary(List<Sims503> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims503 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[Sims_Renew_Library_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'N'),                                
                                new SqlParameter("@sims_library_renewed_date", simsobj.sims_library_renewed_date),                                 
                                new SqlParameter("@em_number", simsobj.sims_employee_code),                                 
                                new SqlParameter("@param",'0'),
                     });
                        if (ins > 0)
                        {
                            inserted = true;
                        }


                        else
                        {
                            inserted = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }

            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        
    }
}

