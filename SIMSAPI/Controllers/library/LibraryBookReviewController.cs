﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryBookReview")]
    public class LibraryBookReviewController : ApiController
    {
        [Route("get_accessionbyList")]
        public HttpResponseMessage get_accessionbyList()
        {

            List<Sims845> lst = new List<Sims845>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims845 simsobj = new Sims845();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();


                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("get_bookdata")]
        public HttpResponseMessage get_bookdata(string str)
        {

            List<Sims845> lst = new List<Sims845>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'G'),
                           new SqlParameter("@sims_library_item_accession_number",str)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims845 simsobj = new Sims845();

                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString().Equals("A ") ? true : false;

                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            lst.Add(simsobj);

                            simsobj.login_id = dr["login_id"].ToString();
                            simsobj.rating = dr["rating"].ToString();
                            simsobj.comment = dr["comment"].ToString();

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("get_grid")]
        public HttpResponseMessage get_grid(string str)
        {

            List<Sims845> lst = new List<Sims845>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_PROC]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                           new SqlParameter("@sims_library_item_accession_number",str)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims845 simsobj = new Sims845();

                            simsobj.id = dr["id"].ToString();
                            simsobj.login_id = dr["login_id"].ToString();
                            simsobj.rating = dr["rating"].ToString();
                            simsobj.comment = dr["comment"].ToString();
                            simsobj.accession_number = dr["accession_number"].ToString();
                            //simsobj.create_on_date = dr["create_on_date"].ToString();

                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            //simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            //simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();

                            //simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString().Equals("A ") ? true : false;

                            //simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            //simsobj.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            //simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            //simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("CUDLibBookReview")]
        public HttpResponseMessage CUDLibBookReview(List<Sims845> data)
        {
            Message message = new Message();
             
            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims845 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_Library_Book_review_PROC]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_Library_Book_review_PROC]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@login_id", simsobj.login_id),
                                new SqlParameter("@rating", simsobj.rating),   
                                new SqlParameter("@comment", simsobj.comment),
                                new SqlParameter("@accession_number", simsobj.accession_number),

                              // new SqlParameter("@create_on_date", simsobj.create_on_date),
                              
                               
                     });


                        //if (ins > 0)
                        //{
                        //    insert = true;
                        //}


                        //else
                        //{
                        //    insert = false;
                        //}
                        //a = 1;

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            //  inserted = false;
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
    }
}