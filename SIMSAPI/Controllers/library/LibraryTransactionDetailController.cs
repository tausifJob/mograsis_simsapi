﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
     [RoutePrefix("api/LibraryTransactionDetail")]
    [BasicAuthentication]
    public class LibraryTransactionDetailController: ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetLibrary_TxnType")]
        public HttpResponseMessage GetLibrary_TxnType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetLibrary_TxnType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetLibrary_TxnType"));

            List<SimsLib> goaltarget_list = new List<SimsLib>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_transaction_type]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsLib simsobj = new SimsLib();
                            simsobj.StudentTransType = dr["sims_library_transaction_type_code"].ToString();
                            simsobj.StudentTransTypeDesc = dr["sims_library_transaction_type_name"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSims_BookDetails")]
        public HttpResponseMessage GetSims_BookDetails(string req_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_BookDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_BookDetails"));
            Message message = new Message();
            List<Sims121> goaltarget_list = new List<Sims121>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_item_master_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),
                           new SqlParameter("@sims_library_item_number",req_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = DateTime.Parse(dr["sims_library_item_date_of_publication"].ToString()).ToShortDateString();
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = DateTime.Parse(dr["sims_library_item_printing_date"].ToString()).ToShortDateString();
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            //simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            //simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            //simsobj.sims_library_item_qty = dr["sims_library_item_quantity"].ToString();
                            //simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            //simsobj.sims_library_item_location_nm = dr["sims_library_item_location_nm"].ToString();
                            //simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_nm"].ToString();
                            //simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                            //    simsobj.sims_library_item_entry_date = DateTime.Parse(dr["sims_library_item_entry_date"].ToString()).ToShortDateString();
                            //simsobj.available_copies = dr["available_copies"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                    else
                    {
                        message.strMessage = "Sorry No Records Found";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            catch (Exception x)
            {
                Log.Error(x);
                // return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetAllLibStudent")]
        public HttpResponseMessage GetAllLibStudent(string cur, string academic, string grade, string section, string enroll)
        {
            //CommonUserControlClass comnobj = new CommonUserControlClass();
            List<SimsLib> lst = new List<SimsLib>();

            if (grade == "undefined" || grade == "\"\"")
            {
                grade = null;
            }
            if (section == "undefined" || section == "\"\"")
            {
                section = null;
            }
            if (enroll == "undefined" || enroll == "\"\"")
            {
                enroll = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[BookBankTrans_proc]",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "A"),
                new SqlParameter("@StudCurr", cur),
                new SqlParameter("@StudAcademicYear",academic),
                new SqlParameter("@StudGrade",grade),
                new SqlParameter("@StudSection",section),
                new SqlParameter("@StudEnroll",enroll)
                });
                    while (dr.Read())
                    {
                        SimsLib obj = new SimsLib();
                        obj.StudentEnroll = dr["sims_enroll_number"].ToString();
                        obj.StudentName = dr["StudName"].ToString();
                        int i;
                        int.TryParse(dr["BorrowLimit"].ToString(), out i);
                        int.TryParse(dr["AllowedLimit"].ToString(), out i);
                        obj.StudentLibBorrowLimit = i;
                        obj.StudentAllowedLimit = i;
                        obj.StudentLibUserNumber = dr["sims_library_user_number"].ToString();
                        obj.StudentLibMemStartDate = dr["sims_library_joining_date"].ToString();
                        obj.StudentLibMemExpiryDate = dr["sims_library_expiry_date"].ToString();
                        obj.StudentBookItemNumbers = dr["CurrentItems"].ToString();
                        int.TryParse(dr["BooksTaken"].ToString(), out i);
                        obj.StudentBooksTaken = i;
                        obj.StudentBalance = (obj.StudentLibBorrowLimit - i);
                        int.TryParse(dr["GraceDays"].ToString(), out i);
                        obj.StudentGraceDays = i;
                        obj.IsValid = true;
                        obj.StudentBookQty = "1";
                        obj.IsChanged = false;
                        obj.StudentBookIssueDate = db.UIDDMMYYYYformat(dr["issue_date"].ToString());
                        obj.StudentCurr = cur;
                        obj.StudentAcademicYear = academic;
                        obj.StudentGrade = grade;
                        obj.StudentSection = section;
                        obj.bookcount = dr["BooksTaken"].ToString();
                        //Transaction Details
                        //obj.Txn_no = dr["Txn_no"].ToString();
                        //obj.Item_no = dr["Item_no"].ToString();
                        //obj.Item_name = dr["Item_name"].ToString();
                        //obj.Issue_date = dr["Issue_date"].ToString();
                        //obj.Return_date = dr["Return_date"].ToString();
                        //obj.Txn_status = dr["Txn_status"].ToString();
                        lst.Add(obj);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("GetAllHomeRoomStudent")]
        public HttpResponseMessage GetAllHomeRoomStudent(string cur, string academic, string home)
        {
            //CommonUserControlClass comnobj = new CommonUserControlClass();
            List<SimsLib> lst = new List<SimsLib>();

            if (home == "undefined" || home == "\"\"")
            {
                home = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].BookBankTrans_proc]",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "Q"),
                new SqlParameter("@StudCurr", cur),
                new SqlParameter("@StudAcademicYear",academic),
                new SqlParameter("@HomeRoom",home),
                
                });
                    while (dr.Read())
                    {
                        SimsLib obj = new SimsLib();
                        obj.StudentEnroll = dr["sims_batch_enroll_number"].ToString();
                        obj.StudentName = dr["StudName"].ToString();
                        int i;
                        int.TryParse(dr["BorrowLimit"].ToString(), out i);
                        int.TryParse(dr["AllowedLimit"].ToString(), out i);
                        obj.StudentLibBorrowLimit = i;
                        obj.StudentAllowedLimit = i;
                        obj.StudentLibUserNumber = dr["sims_library_user_number"].ToString();
                        obj.StudentLibMemStartDate = dr["sims_library_joining_date"].ToString();
                        obj.StudentLibMemExpiryDate = dr["sims_library_expiry_date"].ToString();
                        obj.StudentBookItemNumbers = dr["CurrentItems"].ToString();
                        int.TryParse(dr["BooksTaken"].ToString(), out i);
                        obj.StudentBooksTaken = i;
                        obj.StudentBalance = (obj.StudentLibBorrowLimit - i);
                        int.TryParse(dr["GraceDays"].ToString(), out i);
                        obj.StudentGraceDays = i;
                        obj.IsValid = true;
                        obj.StudentBookQty = "1";
                        obj.IsChanged = false;
                        obj.StudentBookIssueDate = DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year;
                        obj.StudentCurr = cur;
                        obj.StudentAcademicYear = academic;
                        // obj.StudentGrade = grade;
                        // obj.StudentSection = section;

                        //Transaction Details
                        //obj.Txn_no = dr["Txn_no"].ToString();
                        //obj.Item_no = dr["Item_no"].ToString();
                        //obj.Item_name = dr["Item_name"].ToString();
                        //obj.Issue_date = dr["Issue_date"].ToString();
                        //obj.Return_date = dr["Return_date"].ToString();
                        //obj.Txn_status = dr["Txn_status"].ToString();
                        lst.Add(obj);
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("ItemSearch")]
        public HttpResponseMessage ItemSearch(string data)
        {
            Message message = new Message();
            List<Sims121> mod_list = new List<Sims121>();

            Sims121 obj = new Sims121();
            //List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims121>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                   new List<SqlParameter>(){               
                 new SqlParameter("@opr", 'S'),
                 //new SqlParameter("@sims_appl_code", obj.Sims121_sims_appl_code),
                 //new SqlParameter("@sims_appl_form_field", obj.Sims121_sims_appl_form_field),
                 new SqlParameter("@sims_library_item_accession_number", obj.sims_library_item_accession_number),
                 new SqlParameter("@sims_library_item_accession_number_to", obj.sims_library_item_accession_number_to),
                 new SqlParameter("@sims_library_isbn_number", obj.sims_library_isbn_number),
                 new SqlParameter("@sims_library_bnn_number", obj.sims_library_bnn_number),
                 new SqlParameter("@sims_library_item_dewey_number", obj.sims_library_item_dewey_number),
                 new SqlParameter("@sims_library_item_name", obj.sims_library_item_name),
                 new SqlParameter("@sims_library_item_title", obj.sims_library_item_title),
                 new SqlParameter("@sims_library_item_category", obj.sims_library_item_category),
                 new SqlParameter("@sims_library_item_subcategory", obj.sims_library_item_subcategory),
                 new SqlParameter("@sims_library_item_catalogue_code", obj.sims_library_item_catalogue_code),
                 new SqlParameter("@sims_library_item_cur_code", obj.sims_library_item_cur_code),
                 new SqlParameter("@sims_library_item_cur_level_code", obj.sims_library_item_cur_level_code),
                 new SqlParameter("@sims_library_item_grade_code", obj.sims_library_item_grade_code),
                 new SqlParameter("@sims_library_item_language_code", obj.sims_library_item_language_code),
                 new SqlParameter("@sims_library_item_author1", obj.sims_library_item_author1),
                 new SqlParameter("@sims_library_item_age_from", obj.sims_library_item_age_from),
                 new SqlParameter("@sims_library_item_place_of_publication", obj.sims_library_item_place_of_publication),
                 new SqlParameter("@sims_library_item_name_of_publisher", obj.sims_library_item_name_of_publisher),
                 new SqlParameter("@sims_library_item_book_style", obj.sims_library_item_book_style),
                 new SqlParameter("@sims_library_item_status_code", obj.sims_library_item_status_code),
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();

                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = DateTime.Parse(dr["sims_library_item_date_of_publication"].ToString()).ToShortDateString();
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = DateTime.Parse(dr["sims_library_item_printing_date"].ToString()).ToShortDateString();
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            //simsobj.sims_library_item_curcy_code = dr["sims_library_item_curcy_code"].ToString();
                            //simsobj.sims_library_item_curcy_name = dr["sims_library_item_curcy_name"].ToString();
                            //  if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_quantity"].ToString()))
                            // simsobj.sims_library_item_qty = int.Parse(dr["sims_library_item_quantity"].ToString());
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            //simsobj.sims_library_item_location_nm = dr["sims_library_item_location_name"].ToString();
                            //simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_name"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            //if (!string.IsNullOrEmpty(dr["sims_library_item_entry_date"].ToString()))
                            //    simsobj.sims_library_item_entry_date = DateTime.Parse(dr["sims_library_item_entry_date"].ToString()).ToShortDateString();
                            //simsobj.available_copies = dr["available_copies"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        //[Route("InsertTransactionDetails")]
        //public HttpResponseMessage InsertTransactionDetails(List<sims136Detail> lst)
        //{
        //   // int i = 0;
        //    Message message = new Message();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //             foreach (sims136Detail ob in lst)
        //                 {
        //                     int ins = db.ExecuteStoreProcedureforInsert("[sims_library_transaction_detail]",
        //                new List<SqlParameter>()
        //                 {                         

        //           //new SqlParameter.Clear();
        //            new SqlParameter("@opr", "M"),
        //            new SqlParameter("@sims_library_transaction_number", ob.sims_library_transaction_number),
        //            new SqlParameter("@sims_library_transaction_line_number", ob.sims_library_transaction_line_number),
        //            new SqlParameter("@sims_library_item_number", ob.sims_library_item_number),
        //            new SqlParameter("@sims_library_item_qty", "1"),  //ccccc
        //            new SqlParameter("@sims_library_item_expected_return_date", ob.sims_library_item_expected_return_date),
        //            new SqlParameter("@sims_library_item_rate", ""),  //ccccc
        //            new SqlParameter("@sims_library_item_line_total", ""),   //cccccc
        //            new SqlParameter("@sims_library_item_transaction_status", "I"),   //cccccc
        //            //i = i + cmd.ExecuteNonQuery();

        //            });
        //                     if (ins > 0)
        //                     {
        //                         if (ob.opr.Equals("U"))
        //                             message.strMessage = "Record Updated Sucessfully!!";
        //                         else if (ob.opr.Equals("M"))
        //                             message.strMessage = "Record  Added Sucessfully!!";
        //                         else if (ob.opr.Equals("D"))
        //                             message.strMessage = "Record  Deleted Sucessfully!!";

        //                         message.systemMessage = string.Empty;
        //                         message.messageType = MessageType.Success;
        //                     }


        //                     else
        //                     {
        //                         // message.strMessage = "";
        //                         message.strMessage = "";
        //                         message.systemMessage = string.Empty;
        //                         message.messageType = MessageType.Success;
        //                         // message.messageType = MessageType.Error;
        //                         // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //                     }
        //        }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex);
        //        return Request.CreateResponse(HttpStatusCode.OK, lst);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, lst);
        //}

        [Route("MakeTransaction")]
        public HttpResponseMessage MakeTransaction(List<SimsTrans> studData)
        {
            Message message = new Message();
            string st = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SimsTrans item in studData)
                    {
                        // int ins = db.ExecuteStoreProcedureforInsert("[BookBankTrans]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[BookBankTrans_proc]",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@CREATED_BY", item.CREATED_BY),
                            new SqlParameter("@ISSUE_DATE", db.DBYYYYMMDDformat(item.ISSUE_DATE)),
                            new SqlParameter("@ITEM_NO", item.sims_library_item_number),
                            new SqlParameter("@ITEM_PRICE", item.sims_library_item_purchase_price),
                            new SqlParameter("@ITEM_QTY", item.ITEM_QTY),
                            new SqlParameter("@L_User_No", item.L_User_No),
                            new SqlParameter("@REMARK", item.REMARK),
                            new SqlParameter("@RETURN_DATE", db.DBYYYYMMDDformat(item.RETURN_DATE)),
                            new SqlParameter("@TransStatus", item.TransStatus),
                            new SqlParameter("@TransType", item.TransType),
                           
                            });

                        //if (ins > 0)
                        //{
                        //    //SimsLib sl = new SimsLib() { StudentAcademicYear = studData[0].StudAcademicYear, StudentCurr = studData[0].StudCurr, StudentGrade = studData[0].StudGrade, StudentSection = studData[0].StudSection };
                        //    message.strMessage = "Book Issued Sucessfully!!!";
                        //    message.systemMessage = string.Empty;
                        //    message.messageType = MessageType.Success;
                        //}
                        //else
                        //{
                        //    message.strMessage = "Books Not Issued";
                        //    message.systemMessage = string.Empty;
                        //    message.messageType = MessageType.Success;
                        //}

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            //  inserted = false;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            // inserted = true;
                        }
                        else
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("GetBorrwList")]
        public HttpResponseMessage GetBorrwList(string lib_no)
        {
            List<SimsTrans> lst = new List<SimsTrans>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[BookBankTrans_proc]",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "B"),
                new SqlParameter("@L_User_No", lib_no)
                         });
                    while (dr.Read())
                    {
                        SimsTrans obj = new SimsTrans();
                        obj.CREATED_BY = dr["sims_library_transaction_created_by"].ToString();
                        obj.ISSUE_DATE = db.UIDDMMYYYYformat(dr["IssueDate"].ToString());
                        obj.ITEM_Desc = dr["Item_Desc"].ToString();
                        obj.ITEM_NO = dr["sims_library_item_number"].ToString();
                        obj.ITEM_QTY = dr["sims_library_item_qty"].ToString();
                        obj.L_User_No = dr["sims_library_transaction_user_code"].ToString();
                        obj.REMARK = dr["sims_library_transaction_remarks"].ToString();
                        obj.RETURN_DATE = db.UIDDMMYYYYformat(dr["ExpRetDate"].ToString());
                        obj.TransLineNo = dr["sims_library_transaction_line_number"].ToString();
                        obj.TransNo = dr["sims_library_transaction_number"].ToString();
                        obj.TransStatus = dr["sims_library_item_transaction_status"].ToString();
                        obj.TransType = dr["sims_library_transaction_type"].ToString();
                        obj.TransTypeDesc = dr["TT"].ToString();
                        obj.StudSection = dr["sims_library_item_rate"].ToString();
                        //obj.count = dr["count"].ToString();
                        lst.Add(obj);
                    }
                }
            }

            catch (Exception ex)
            {
                // con.Close();
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("StudentTrans")]
        public HttpResponseMessage StudentTrans(List<SimsTrans> obj)
        {
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SimsTrans item in obj)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[BookBankTrans_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@TransStatus", 'R'),                       
                            new SqlParameter("@REMARK", item.REMARK),
                            new SqlParameter("@L_User_No", item.L_User_No),
                            new SqlParameter("@TransNo", item.TransNo),
                            new SqlParameter("@TransLineNo", item.TransLineNo),
                            //new SqlParameter("@REIssue", item.opr),
                            new SqlParameter("@ITEM_NO", item.ITEM_NO),
                            new SqlParameter("@ITEM_QTY", item.ITEM_QTY),
                            //new SqlParameter("@ISSUE_DATE",item.RETURN_DATE1),
                            new SqlParameter("@GraceDays", item.StudGrade),
                            new SqlParameter("@CREATED_BY", item.CREATED_BY),
                            new SqlParameter("@ITEM_PRICE", item.StudSection), 
                            new SqlParameter("@TransType", item.TransType),
                         });

                        if (ins > 0)
                        {
                            //SimsTrans st = new SimsTrans();
                            //st.L_User_No = obj[0].L_User_No;                           
                            message.strMessage = "Books Returned Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {
                            message.strMessage = "Books Not Returned!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("StudentTransReissue")]
        public HttpResponseMessage StudentTransReissue(List<SimsTrans> obj)
        {
            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SimsTrans item in obj)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[BookBankTrans_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'R'),
                            new SqlParameter("@TransStatus", 'R'),                       
                            new SqlParameter("@REMARK", item.REMARK),
                            new SqlParameter("@L_User_No", item.L_User_No),
                            new SqlParameter("@TransNo", item.TransNo),
                            new SqlParameter("@TransLineNo", item.TransLineNo),
                            new SqlParameter("@REIssue", item.opr),
                            new SqlParameter("@ITEM_NO", item.ITEM_NO),
                            new SqlParameter("@ITEM_QTY", item.ITEM_QTY),
                            //new SqlParameter("@ISSUE_DATE",item.RETURN_DATE1),
                            new SqlParameter("@GraceDays", item.StudGrade),
                            new SqlParameter("@CREATED_BY", item.CREATED_BY),
                            new SqlParameter("@ITEM_PRICE", item.StudSection),                       
                         });

                        if (ins > 0)
                        {
                            //SimsTrans st = new SimsTrans();
                            //st.L_User_No = obj[0].L_User_No;                           
                            message.strMessage = "Books Re-Issued Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        else
                        {
                            message.strMessage = "Books Not Re-Issued!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("GetIssueBorrwList")]
        public HttpResponseMessage GetIssueBorrwList(string txn_no, string libcode)
        {
            List<SimsTrans> lst = new List<SimsTrans>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[BookBankTrans_proc]",
                       new List<SqlParameter>()
                         {
                new SqlParameter("@opr", "K"),
                new SqlParameter("@TransType", txn_no), 
                new SqlParameter("@libcode", libcode)
                         });
                    while (dr.Read())
                    {
                        SimsTrans obj = new SimsTrans();
                        obj.CREATED_BY = dr["sims_library_transaction_created_by"].ToString();
                        try
                        {
                            obj.ISSUE_DATE = db.UIDDMMYYYYformat(dr["IssueDate"].ToString());
                        }
                        catch (Exception)
                        {
                        }
                        obj.ITEM_Desc = dr["Item_Desc"].ToString();
                        obj.ITEM_NO = dr["sims_library_item_number"].ToString();
                        obj.ITEM_QTY = dr["sims_library_item_qty"].ToString();
                        obj.L_User_No = dr["sims_library_transaction_user_code"].ToString();
                        obj.bks_remark = dr["sims_library_transaction_remarks"].ToString();
                        //  obj.returN_DATE = dr["sims_library_transaction_remarks"].ToString();

                        try
                        {
                            obj.return_date = db.UIDDMMYYYYformat(dr["ExpRetDate"].ToString());
                        }
                        catch (Exception)
                        {
                        }
                        obj.TransLineNo = dr["sims_library_transaction_line_number"].ToString();
                        obj.TransNo = dr["sims_library_transaction_number"].ToString();
                        obj.TransStatus = dr["sims_library_transaction_status"].ToString();
                        obj.TransType = dr["sims_library_transaction_type"].ToString();
                        obj.TransTypeDesc = dr["TT"].ToString();
                        //obj.StudSection = dr["sims_library_item_rate"].ToString();
                        lst.Add(obj);
                    }
                }
            }

            catch (Exception ex)
            {
                // con.Close();
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("GetBookNameDetail")]
        public HttpResponseMessage GetBookNameDetail(string item_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetBookNameDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetLibrary_TxnType"));

            List<SimsLib> goaltarget_list = new List<SimsLib>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[BookBankTrans_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'M'),
                           new SqlParameter("@item_no",item_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsLib simsobj = new SimsLib();
                            simsobj.StudentTransType = dr["sims_library_transaction_type_code"].ToString();
                            simsobj.StudentTransTypeDesc = dr["sims_library_transaction_type_name"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }
        [Route("GetBookNameAdd")]
        public HttpResponseMessage GetBookNameAdd(string sims_library_item_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetBookNameDetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetLibrary_TxnType"));

            List<SimsLib> goaltarget_list = new List<SimsLib>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[BookBankTrans_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'Z'),
                           new SqlParameter("@sims_library_item_number ",sims_library_item_number )
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimsLib simsobj = new SimsLib();
                            simsobj.studentLibraryItem = dr["sims_library_item_number"].ToString();
                            simsobj.studentLibraryNumber = dr["sims_library_item_accession_number"].ToString();
                            simsobj.studentLibraryDesc = dr["sims_library_item_desc"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

     }
}