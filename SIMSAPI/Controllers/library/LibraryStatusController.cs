﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
//using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryStatus")]
    [BasicAuthentication]
    public class LibraryStatusController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //Select
        [Route("getLibraryStatus")]
        public HttpResponseMessage getLibraryStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLibraryStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getLibraryStatus"));

            List<Sims130> goaltarget_list = new List<Sims130>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_library_status]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims130 simsobj = new Sims130();
                            simsobj.sims_library_status_code = dr["sims_library_status_code"].ToString();
                            simsobj.sims_library_status_name = dr["sims_library_status_name"].ToString();
                            //simsobj.sims_library_attribute_status = dr["sims_library_attribute_status"].Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDLibraryStatus")]
        public HttpResponseMessage CUDLibraryStatus(List<Sims130> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims130 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_library_status]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_status_code", simsobj.sims_library_status_code),
                                new SqlParameter("@sims_library_status_name", simsobj.sims_library_status_name),
                               // new SqlParameter("@sims_library_attribute_status", simsobj.sims_library_attribute_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            //if (simsobj.opr.Equals("U"))
                            //    message.strMessage = "Record Updated Sucessfully!!";
                            //else if (simsobj.opr.Equals("I"))
                            //    message.strMessage = "Record  Added Sucessfully!!";
                            //else if (simsobj.opr.Equals("D"))
                            //    message.strMessage = "Record  Deleted Sucessfully!!";

                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            insert = true;
                        }


                        else
                        {
                            // message.strMessage = "";
                            //message.strMessage = "Library Status Code is already present";
                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }


}

