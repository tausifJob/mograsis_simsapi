﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibTransaction")]
    [BasicAuthentication]
    public class LibraryTransactionController : ApiController
    {
        #region LibraryTransaction
        [Route("GetLibraryDetails")]
        public HttpResponseMessage GetLibraryDetails(string Search_Code)
        {
            List<sims135> lstsimsobj = new List<sims135>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@Search_code", Search_Code),
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        {
                            sims135 simsobj = new sims135();
                            simsobj.sims_library_user_number = dr["sims_library_user_number"].ToString();
                            simsobj.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                            simsobj.sims_library_borrow_limit = dr["sims_library_borrow_limit"].ToString();
                            simsobj.sims_library_allowed_limit = dr["sims_library_allowed_limit"].ToString();
                            simsobj.sims_library_reservation_limit = dr["sims_library_reservation_limit"].ToString();
                            simsobj.sims_library_duration = dr["sims_library_duration"].ToString();
                            simsobj.sims_library_promote_flag = dr["sims_library_promote_flag"].ToString();
                            simsobj.sims_library_quota = dr["sims_library_quota"].ToString();
                            simsobj.sims_library_no_issue_after = dr["sims_library_no_issue_after"].ToString();
                            simsobj.sims_library_auto_renew_flag = dr["sims_library_auto_renew_flag"].ToString();
                            simsobj.sims_library_grace_days = dr["sims_library_grace_days"].ToString();
                            simsobj.sims_library_privilege_status = dr["sims_library_privilege_status"].ToString();
                            simsobj.sims_library_reservation_flag = dr["sims_library_reservation_flag"].ToString();
                            simsobj.sims_library_reservation_release_limit = dr["sims_library_reservation_release_limit"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_user_code = dr["sims_user_code"].ToString();
                            simsobj.sims_library_joining_date = dr["sims_library_joining_date"].ToString();
                            simsobj.sims_library_expiry_date = dr["sims_library_expiry_date"].ToString();
                            if (dr["sims_library_status"].ToString().ToUpper() == "A")
                                simsobj.sims_library_status = true;
                            else
                                simsobj.sims_library_status = false;
                            simsobj.sims_library_StudentName = dr["StudentName"].ToString();
                            simsobj.sims_library_TeacherName = dr["TeacherName"].ToString();
                            simsobj.sims_library_LibraryGuestName = dr["LibraryGuestName"].ToString();
                            simsobj.sims_library_user_photo = dr["user_photo"].ToString();
                            simsobj.sims_library_class = dr["grade"].ToString() + "-" + dr["section"].ToString();
                            lstsimsobj.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
            }
        }

        [Route("GetLibraryTransactionDetails")]
        public HttpResponseMessage GetLibraryTransactionDetails(string Search_Code)
       {
            
            
            
           List<sims136Detail> lstsimsobj = new List<sims136Detail>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@Search_code", Search_Code),
                            new SqlParameter("@Transaction_Type", "01")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims136Detail simsobj = new sims136Detail();
                            simsobj.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                            simsobj.sims_library_transaction_type = dr["sims_library_transaction_type"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_transaction_line_number = dr["sims_library_transaction_line_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_library_item_expected_return_date"].ToString()) == false)
                                simsobj.sims_library_item_expected_return_date = dr["sims_library_item_expected_return_date"].ToString();
                            if (string.IsNullOrEmpty(dr["sims_library_transaction_date"].ToString()) == false)
                                simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_user_number = dr["sims_library_user_number"].ToString();
                            simsobj.isNew = false;//User This Flag For Delete Also
                            simsobj.isReturn = true;
                            simsobj.isReIssue = true;
                            lstsimsobj.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
            }
        }


        [Route("GetBookSearch")]
        public HttpResponseMessage GetBookSearch(string Search_Code, int graceDays, string seldate)
        {
           // DateTime sdate = DateTime.Parse(seldate.ToString());
            List<sims136Detail> lstsimsobj = new List<sims136Detail>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@Search_code", Search_Code),
                            new SqlParameter("@Transaction_Type", "01")
                         });
                    if (dr.Read())
                    {
                        //while (dr.Read())
                        //{
                        sims136Detail simsobj = new sims136Detail();
                        simsobj.sims_library_transaction_number = string.Empty;
                        simsobj.sims_library_transaction_type = "01";//for Issue
                        simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                        simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                        simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                        simsobj.sims_library_transaction_line_number = string.Empty;
                        simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                        simsobj.sims_library_item_expected_return_date = db.UIDDMMYYYYformat(DateTime.Parse(db.DBYYYYMMDDformat(seldate)).AddDays(graceDays).ToShortDateString()).ToString(); //Need To calculate UIDDMMYYYYformat
                        simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                        simsobj.sims_library_user_number = string.Empty;//need to update
                        simsobj.isNew = true;//User This Flag For Delete Also
                        simsobj.isReturn = false;
                        simsobj.isReIssue = false;
                        simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                        simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                        simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                        //simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString(); //DateTime.Now.ToShortDateString();
                        simsobj.sims_library_transaction_date = seldate;
                        simsobj.user_detail = dr["user_detail"].ToString();
                        lstsimsobj.Add(simsobj);
                        //}
                    }
                    dr.Close();                    
                }
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);

            }
        }

        [Route("BookReIssue")]
        public HttpResponseMessage BookReIssue(int days, sims136Detail simsobj)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@Search_code", simsobj.sims_library_transaction_number.ToString()),
                            new SqlParameter("@Transaction_Type", ""),
                            new SqlParameter("@sims_library_transaction_date",db.DBYYYYMMDDformat(simsobj.sims_library_transaction_date)),
                            new SqlParameter("@item_number", simsobj.sims_library_item_number.ToString()),
                            new SqlParameter("@libDays", days.ToString()),
                            new SqlParameter("@ulineno", simsobj.sims_library_transaction_line_number.ToString())
                         });
                    int t = dr.RecordsAffected;
                    if (t >= 1)
                        res = true;
                    else
                        res = false;
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("BookReturn")]
        public HttpResponseMessage BookReturn(sims136Detail simsobj)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@Search_code", simsobj.sims_library_transaction_number.ToString()),
                            new SqlParameter("@Transaction_Type", ""),
                            new SqlParameter("@sims_library_transaction_date", db.DBYYYYMMDDformat(simsobj.sims_library_transaction_date)),
                            new SqlParameter("@item_number", simsobj.sims_library_item_number.ToString()),
                         });
                    int t = dr.RecordsAffected;
                    if (t >= 1)
                        res = true;
                    else
                        res = false;
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        [Route("Insert_NewBooks")]
        public HttpResponseMessage Insert_NewBooks(sims136Detail simsobj)
        {
            string res = "false";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@Search_code", string.Empty),
                            new SqlParameter("@sims_library_transaction_user_code", simsobj.sims_library_user_number),
                            new SqlParameter("@sims_library_transaction_date", db.DBYYYYMMDDformat(simsobj.sims_library_transaction_date)),
                            new SqlParameter("@sims_library_transaction_type", simsobj.sims_library_transaction_type),
                            new SqlParameter("@sims_library_transaction_remarks", simsobj.sims_library_transaction_remarks),
                            new SqlParameter("@sims_library_transaction_total", simsobj.sims_library_transaction_total),
                            new SqlParameter("@sims_library_transaction_created_by", simsobj.sims_library_transaction_created_by),
                            new SqlParameter("@sims_library_transaction_status", "A"),
                         });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        res = dr["TransNo"].ToString();

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
        }

        [Route("InsertTransactionDetails")]
        public HttpResponseMessage InsertTransactionDetails(List<sims136Detail> lst)
        {
            int i = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims136Detail ob in lst)
                    {
                     //   string date = string.Format("{0}-{1}-{2}", DateTime.Parse(ob.sims_library_item_expected_return_date).Year.ToString(), DateTime.Parse(ob.sims_library_item_expected_return_date).Month.ToString(), DateTime.Parse(ob.sims_library_item_expected_return_date).Day.ToString());
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction_detail"
                        , new List<SqlParameter>()
                        {
                        new SqlParameter("@opr", "M"),
                        new SqlParameter("@sims_library_transaction_number", ob.sims_library_transaction_number),
                        new SqlParameter("@sims_library_transaction_line_number", ob.sims_library_transaction_line_number),
                        new SqlParameter("@sims_library_item_number", ob.sims_library_item_number),
                        new SqlParameter("@sims_library_item_qty", "1"),  //ccccc
                        new SqlParameter("@sims_library_item_issue_date", db.DBYYYYMMDDformat(ob.sims_library_transaction_date)),
                        new SqlParameter("@sims_library_item_expected_return_date", db.DBYYYYMMDDformat(ob.sims_library_item_expected_return_date)),
                        new SqlParameter("@sims_library_item_rate", ""),  //ccccc
                        new SqlParameter("@sims_library_item_line_total", ""),    //cccccc
                        new SqlParameter("@sims_library_item_transaction_status", "I"),   //cccccc
                        });
                        i += dr.RecordsAffected;
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, i.ToString());
            }
            return Request.CreateResponse(HttpStatusCode.OK, i.ToString());
        }
       
        //[Route("Insert_NewBooks")]
        //public HttpResponseMessage Insert_NewBooks(sims136Detail simsobj)
        //{
        //    string res = "false";
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", "I"),
        //                    new SqlParameter("@Search_code", string.Empty),
        //                    new SqlParameter("@sims_library_transaction_user_code", simsobj.sims_library_user_number),
        //                    new SqlParameter("@sims_library_transaction_date", DateTime.Parse(simsobj.sims_library_transaction_date).ToShortDateString()),
        //                    new SqlParameter("@sims_library_transaction_type", simsobj.sims_library_transaction_type),
        //                    new SqlParameter("@sims_library_transaction_remarks", simsobj.sims_library_transaction_remarks),
        //                    new SqlParameter("@sims_library_transaction_total", simsobj.sims_library_transaction_total),
        //                    new SqlParameter("@sims_library_transaction_created_by", simsobj.sims_library_transaction_created_by),
        //                    new SqlParameter("@sims_library_transaction_status", "A"),
        //                 });
        //            if (dr.HasRows)
        //            {
        //                dr.Read();
        //                res = dr["TransNo"].ToString();

        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, res);
        //    }
        //    catch (Exception x)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, res);
        //    }
        //}

        //[Route("InsertTransactionDetails")]
        //public HttpResponseMessage InsertTransactionDetails(List<sims136Detail> lst)
        //{
        //    int i = 0;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            List<SqlParameter> param = new List<SqlParameter>();
        //            foreach (sims136Detail ob in lst)
        //            {
        //                param = new List<SqlParameter>();
        //                param.Add(new SqlParameter("@opr", "M"));
        //                param.Add(new SqlParameter("@sims_library_transaction_number", ob.sims_library_transaction_number));
        //                param.Add(new SqlParameter("@sims_library_transaction_line_number", ob.sims_library_transaction_line_number));
        //                param.Add(new SqlParameter("@sims_library_item_number", ob.sims_library_item_number));
        //                param.Add(new SqlParameter("@sims_library_item_qty", "1"));  //ccccc
        //                param.Add(new SqlParameter("@sims_library_item_expected_return_date", ob.sims_library_item_expected_return_date));
        //                param.Add(new SqlParameter("@sims_library_item_rate", ""));  //ccccc
        //                param.Add(new SqlParameter("@sims_library_item_line_total", ""));    //cccccc
        //                param.Add(new SqlParameter("@sims_library_item_transaction_status", "I"));   //cccccc
        //                SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction_detail", param);
        //                i += dr.RecordsAffected;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, i.ToString());
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, i.ToString());
        //}

        [Route("GetBookSearchHistoryDetails")]
        public HttpResponseMessage GetBookSearchHistoryDetails(string Search_Code)
        {
            List<sims136Detail> lstsimsobj = new List<sims136Detail>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H"),
                            new SqlParameter("@Search_code", Search_Code),
                            new SqlParameter("@Transaction_Type", "01")
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims136Detail simsobj = new sims136Detail();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                            simsobj.sims_library_item_issue_date = dr["sims_library_item_issue_date"].ToString();
                            simsobj.sims_library_item_actual_return_date = dr["sims_library_item_actual_return_date"].ToString();
                            //simsobj.sims_library_item_expected_return_date = dr["sims_library_item_actual_return_date"].ToString();
                            simsobj.sims_library_user_name = dr["userName"].ToString();
                            simsobj.sims_library_user_number = dr["sims_library_transaction_user_code"].ToString();
                            simsobj.sims_library_item_status = dr["Staus_Desc"].ToString();
                            lstsimsobj.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);

        }

        [Route("GetMemberSearchHistoryDetails")]
        public HttpResponseMessage GetMemberSearchHistoryDetails(string Search_Code)
        {
            List<sims136Detail> lstsimsobj = new List<sims136Detail>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@Search_code", Search_Code)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims136Detail simsobj = new sims136Detail();
                            simsobj.sims_library_item_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                            simsobj.sims_library_item_issue_date = dr["transaction_type_date"].ToString();
                            simsobj.sims_library_item_status = dr["Staus_Desc"].ToString();
                            lstsimsobj.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);

        }
        #endregion


        #region BOOK SEARCH

        [Route("booksearchdetails")]
        public HttpResponseMessage booksearchdetails(Sims121 obj)
        {

            List<Sims121> mod_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                      new SqlParameter("@opr", 'S'),
                //new SqlParameter("@sims_appl_code",commonStaticClass.Sims121_sims_appl_code),
                //new SqlParameter("@sims_appl_form_field",commonStaticClass.Sims121_sims_appl_form_field),
                new SqlParameter("@sims_library_item_accession_number",obj.sims_library_item_accession_number),
                new SqlParameter("@sims_library_item_accession_number_to",obj.sims_library_item_accession_number_to),
                new SqlParameter("@sims_library_isbn_number",obj.sims_library_isbn_number),
                new SqlParameter("@sims_library_bnn_number",obj.sims_library_bnn_number),
                new SqlParameter("@sims_library_item_dewey_number",obj.sims_library_item_dewey_number),
                new SqlParameter("@sims_library_item_name",obj.sims_library_item_name),
                new SqlParameter("@sims_library_item_title",obj.sims_library_item_title),
                new SqlParameter("@sims_library_item_category",obj.sims_library_item_category),
                new SqlParameter("@sims_library_item_subcategory",obj.sims_library_item_subcategory),
                new SqlParameter("@sims_library_item_catalogue_code",obj.sims_library_item_catalogue_code),
                new SqlParameter("@sims_library_item_cur_code",obj.sims_library_item_cur_code),
                new SqlParameter("@sims_library_item_cur_level_code",obj.sims_library_item_cur_level_code),
                new SqlParameter("@sims_library_item_grade_code",obj.sims_library_item_grade_code),
                new SqlParameter("@sims_library_item_language_code",obj.sims_library_item_language_code),
                new SqlParameter("@sims_library_item_author1",obj.sims_library_item_author1),
                new SqlParameter("@sims_library_item_age_from",obj.sims_library_item_age_from),
                new SqlParameter("@sims_library_item_place_of_publication",obj.sims_library_item_place_of_publication),
                new SqlParameter("@sims_library_item_name_of_publisher",obj.sims_library_item_name_of_publisher),
                new SqlParameter("@sims_library_item_book_style",obj.sims_library_item_book_style),
                new SqlParameter("@sims_library_item_status_code",obj.sims_library_item_status_code),
                new SqlParameter("@sims_library_item_barcode",obj.sims_library_item_barcode),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simsobj = new Sims121();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category = dr["sims_library_item_category"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            simsobj.sims_library_item_subcategory = dr["sims_library_item_subcategory"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            simsobj.sims_library_item_cur_code = dr["sims_library_item_cur_code"].ToString();
                            simsobj.sims_library_item_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.sims_library_item_cur_level_code = dr["sims_library_item_cur_level_code"].ToString();
                            simsobj.sims_library_item_cur_level_name = dr["sims_cur_level_name_en"].ToString();
                            simsobj.sims_library_item_catalogue_code = dr["sims_library_item_catalogue_code"].ToString();
                            simsobj.sims_library_item_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            simsobj.sims_library_item_grade_code = dr["sims_library_item_grade_code"].ToString();
                            simsobj.sims_library_item_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_library_item_language_code = dr["sims_library_item_language_code"].ToString();
                            simsobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            simsobj.sims_library_item_book_style = dr["sims_library_item_book_style"].ToString();
                            simsobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            simsobj.sims_library_item_author2 = dr["sims_library_item_author2"].ToString();
                            simsobj.sims_library_item_author3 = dr["sims_library_item_author3"].ToString();
                            simsobj.sims_library_item_author4 = dr["sims_library_item_author4"].ToString();
                            simsobj.sims_library_item_author5 = dr["sims_library_item_author5"].ToString();
                            simsobj.sims_library_item_age_from = dr["sims_library_item_age_from"].ToString();
                            simsobj.sims_library_item_age_to = dr["sims_library_item_age_to"].ToString();
                            simsobj.sims_library_item_edition_statement = dr["sims_library_item_edition_statement"].ToString();
                            simsobj.sims_library_item_place_of_publication = dr["sims_library_item_place_of_publication"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_date_of_publication"].ToString()))
                                simsobj.sims_library_item_date_of_publication = db.UIDDMMYYYYformat(dr["sims_library_item_date_of_publication"].ToString());
                            simsobj.sims_library_item_extent_of_item = dr["sims_library_item_extent_of_item"].ToString();
                            simsobj.sims_library_item_accompanying_item = dr["sims_library_item_accompanying_item"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_printing_date"].ToString()))
                                simsobj.sims_library_item_printing_date = db.UIDDMMYYYYformat(dr["sims_library_item_printing_date"].ToString());
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_storage_hint = dr["sims_library_item_storage_hint"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_keywords = dr["sims_library_item_keywords"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_known_as = dr["sims_library_item_known_as"].ToString();
                            simsobj.sims_library_item_status_code = dr["sims_library_item_status_code"].ToString();
                            simsobj.sims_library_item_status_name = dr["sims_library_item_status_name"].ToString();
                            simsobj.available_copies = dr["available_copies"].ToString();
                            simsobj.sims_library_item_location_code = dr["sims_library_item_location_code"].ToString();
                            simsobj.sims_library_item_location_nm = dr["sims_library_item_location_nm"].ToString();
                            simsobj.sims_library_item_bin_nm = dr["sims_library_item_bin_nm"].ToString();
                            simsobj.sims_library_item_bin_code = dr["sims_library_item_bin_code"].ToString();
                            //My Change...
                            simsobj.sims_library_item_entry_date = dr["sims_library_item_entry_date"].ToString();
                            simsobj.items_check_status = false;
                            simsobj.sims_library_category_item_issue = dr["sims_library_category_item_issue"].Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getBSBookStyle")]
        public HttpResponseMessage getBSBookStyle()
        {

            List<Sims121> ItemMaster_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "BS")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simobj = new Sims121();
                            simobj.sims_library_item_book_style = dr["sims_library_item_style_code"].ToString();
                            simobj.sims_library_item_book_style_name = dr["sims_library_item_style_name"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getBSLanguage")]
        public HttpResponseMessage getBSLanguage()
        {

            List<Sims121> ItemMaster_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "L")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simobj = new Sims121();
                            simobj.sims_library_item_language_code = dr["sims_language_code"].ToString();
                            simobj.sims_library_item_language_name = dr["sims_language_name_en"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getBSCategory")]
        public HttpResponseMessage getBSCategory()
        {

            List<Sims121> ItemMaster_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simobj = new Sims121();
                            simobj.sims_library_item_category = dr["sims_library_category_code"].ToString();
                            simobj.sims_library_item_category_name = dr["sims_library_category_name"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        [Route("getBSSubCategory")]
        public HttpResponseMessage getBSSubCategory(string cat_Code)
        {

            List<Sims121> ItemMaster_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SC"),
                            new SqlParameter("@search",cat_Code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simobj = new Sims121();
                            simobj.sims_library_item_category = dr["sims_library_subcategory_code"].ToString();
                            simobj.sims_library_item_category_name = dr["sims_library_subcategory_name"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }


        [Route("getBSCur")]
        public HttpResponseMessage getBSCur()
        {

            List<Sims121> ItemMaster_list = new List<Sims121>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_book_search_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "CU")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims121 simobj = new Sims121();
                            simobj.sims_library_item_cur_code = dr["sims_cur_code"].ToString();
                            simobj.sims_library_item_cur_name = dr["sims_cur_short_name_en"].ToString();
                            ItemMaster_list.Add(simobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ItemMaster_list);
        }

        #endregion

        #region Return Book SEARCH
        [Route("UseritemSearch")]
        public HttpResponseMessage UseritemSearch(sims136Detail obj)
        {
           List<sims136Detail> lstsimsobj = new List<sims136Detail>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                            new SqlParameter("@sims_student_enroll_number",obj.sims_student_enroll_number),
                            new SqlParameter("@studname",obj.studentname),
                            new SqlParameter("@sims_library_item_accession_number",obj.sims_library_item_accession_number),
                            new SqlParameter("@sims_library_isbn_number",obj.sims_library_isbn_number),
                            new SqlParameter("@sims_library_item_name",obj.sims_library_item_name),
                            new SqlParameter("@sims_library_item_title",obj.sims_library_item_title),
                            new SqlParameter("@sims_library_item_bar_code",obj.sims_library_item_bar_code)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims136Detail simsobj = new sims136Detail();
                            simsobj.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                            simsobj.sims_library_transaction_line_number = dr["sims_library_transaction_line_number"].ToString();
                            simsobj.sims_library_transaction_user_code = dr["sims_library_transaction_user_code"].ToString();
                            simsobj.sims_library_transaction_date=dr["sims_library_transaction_date"].ToString();
                            simsobj.sims_library_transaction_status=dr["sims_library_transaction_status"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_library_item_issue_date"].ToString()))
                                simsobj.sims_library_item_issue_date = db.UIDDMMYYYYformat(dr["sims_library_item_issue_date"].ToString());
                            if (!string.IsNullOrEmpty(dr["sims_library_item_expected_return_date"].ToString()))
                                simsobj.sims_library_item_expected_return_date = db.UIDDMMYYYYformat(dr["sims_library_item_expected_return_date"].ToString());
                            simsobj.sims_student_enroll_number=dr["sims_enroll_number"].ToString();
                            simsobj.sims_library_item_number=dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number=dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.studentname=dr["studname"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            lstsimsobj.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstsimsobj);
        }

        [Route("Returnitem")]
        public HttpResponseMessage Returnitem(List<sims136Detail> lst)
        {
            bool res = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims136Detail simsobj in lst)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_library_transaction",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "K"),
                            new SqlParameter("@TransNo", simsobj.sims_library_transaction_number),
                            new SqlParameter("@Transaction_Type", "02"),
                            new SqlParameter("@REMARK", simsobj.sims_library_transaction_remarks),
                            new SqlParameter("@CREATED_BY", simsobj.sims_library_transaction_created_by),
                            new SqlParameter("@TransLineNo", simsobj.sims_library_transaction_line_number),
                            new SqlParameter("@item_number", simsobj.sims_library_item_number),
                         });
                        int t = dr.RecordsAffected;
                        if (t >= 1)
                            res = true;
                        else
                            res = false;
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                res = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }
        #endregion

    }
}