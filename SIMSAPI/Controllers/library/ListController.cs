﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.list

{

        [RoutePrefix("api/List")]
        [BasicAuthentication]
        public class ListController : ApiController
        {
            private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


            //Seelct
            [Route("getAllList")]
            public HttpResponseMessage getAllList()
            {
                string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllList(),PARAMETERS :: NO";
                Log.Debug(string.Format(debug, "Common", "getAllList"));

                List<Sim148> goaltarget_list = new List<Sim148>();
                //int total = 0, skip = 0;
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_list_proc]",
                            new List<SqlParameter>()
                             {
                           new SqlParameter("@opr", 'S'),

                             });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sim148 simsobj = new Sim148();
                                simsobj.sims_list_code = dr["sims_list_code"].ToString();
                                simsobj.sims_list_name = dr["sims_list_name"].ToString();
                                simsobj.sims_list_description = dr["sims_list_description"].ToString();
                                simsobj.sims_list_status = dr["sims_list_status"].Equals("A") ? true : false;

                                goaltarget_list.Add(simsobj);

                            }
                        }
                    }
                }
                catch (Exception x)
                {
                    Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

                }
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }




            [Route("CUDList")]
            public HttpResponseMessage CUDList(List<Sim148> data)
            {
                bool insert = false;
                Message message = new Message();
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sim148 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_list_proc]",
                            new List<SqlParameter>()
                         {

                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_list_code",simsobj.sims_list_code),
                                 new SqlParameter("@sims_list_name",simsobj.sims_list_name),
                                 new SqlParameter("@sims_list_description",simsobj.sims_list_description),
                                 new SqlParameter("@sims_list_status", simsobj.sims_list_status==true?"A":"I"),
                        });
                            if (ins > 0)
                            {
                                insert = true;
                            }


                            else
                            {
                                insert = false;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, insert);
                    }

                }


                catch (Exception x)
                {

                    return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                }

            }


        }


    }











