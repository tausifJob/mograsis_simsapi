﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryBookset")]
    public class LibraryBooksetController : ApiController
    {

        [Route("get_bookSetDetail")]
        public HttpResponseMessage get_listsection()
        {

            List<Sims693> lst = new List<Sims693>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_bookset_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims693 simsobj = new Sims693();

                            simsobj.sims_library_bookset_number = dr["sims_library_bookset_number"].ToString();
                            simsobj.sims_library_bookset_name = dr["sims_library_bookset_name"].ToString();
                            simsobj.sims_library_bookset_book_qty = dr["sims_library_bookset_book_qty"].ToString();
                            simsobj.sims_library_bookset_status = dr["sims_library_bookset_status"].ToString().Equals("1") ? true : false;

                            lst.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }


        [Route("CUDLibraryBookset")]
        public HttpResponseMessage CUDListSection(List<Sims693> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Sims693 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_bookset_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                 //new SqlParameter("@id", simsobj.id),
                                new SqlParameter("@sims_library_bookset_number", simsobj.sims_library_bookset_number),
                                new SqlParameter("@sims_library_bookset_name", simsobj.sims_library_bookset_name),
                                new SqlParameter("@sims_library_bookset_book_qty", simsobj.sims_library_bookset_book_qty),   
                                 new SqlParameter("@sims_library_bookset_status", simsobj.sims_library_bookset_status==true?"1":"0"), 
                           
                                

                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
    }
}