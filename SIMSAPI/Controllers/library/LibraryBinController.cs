﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryBin")]
    public class LibraryBinController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("GetSims_LibraryBin")]
        public HttpResponseMessage GetSims_LibraryBin()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLibraryStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_LibraryBin"));

            List<Sims116> goaltarget_list = new List<Sims116>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_bin_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims116 simsobj = new Sims116();
                            simsobj.sims_library_bin_code = int.Parse(dr["sims_library_bin_code"].ToString());
                            simsobj.sims_library_bin_desc = dr["sims_library_bin_desc"].ToString();
                            simsobj.sims_library_location_code = dr["sims_library_location_code"].ToString();
                            simsobj.sims_library_location_name = dr["sims_library_location_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("GetSims_LibraryLocation")]
        public HttpResponseMessage GetSims_LibraryLocation()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSims_LibraryLocation(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_LibraryLocation"));

            List<Sims116> goaltarget_list = new List<Sims116>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_bin_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'L'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims116 simsobj = new Sims116();
                            

                            simsobj.sims_library_location_code = dr["sims_library_location_code"].ToString();
                            simsobj.sims_library_location_name = dr["sims_library_location_name"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDLibraryBin")]
        public HttpResponseMessage CUDLibraryBin(List<Sims116> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims116 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_bin_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_bin_code", simsobj.sims_library_bin_code),
                                new SqlParameter("@sims_library_bin_desc", simsobj.sims_library_bin_desc),   
                                new SqlParameter("@sims_library_location_code", simsobj.sims_library_location_code),
                     });
                        if (ins > 0)
                        {
                            //if (simsobj.opr.Equals("U"))
                            //    message.strMessage = "Record Updated Sucessfully!!";
                            //else if (simsobj.opr.Equals("I"))
                            //    message.strMessage = "Record  Added Sucessfully!!";
                            //else if (simsobj.opr.Equals("D"))
                            //    message.strMessage = "Record  Deleted Sucessfully!!";

                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            insert = true;
                        }


                        else
                        {
                            // message.strMessage = "";
                            //message.strMessage = "Library Bin Code is already present";
                            //message.systemMessage = string.Empty;
                            //message.messageType = MessageType.Success;
                            // message.messageType = MessageType.Error;
                            // return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("GetSims_LibraryBin_pearl")]
        public HttpResponseMessage GetSims_LibraryBin_pearl()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLibraryStatus(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetSims_LibraryBin"));

            List<Sims116> goaltarget_list = new List<Sims116>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_bin_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims116 simsobj = new Sims116();
                            simsobj.sims_library_bin_code = int.Parse(dr["sims_library_bin_code"].ToString());
                            simsobj.sims_library_bin_desc = dr["sims_library_bin_desc"].ToString();
                            simsobj.sims_library_location_code = dr["sims_library_location_code"].ToString();
                            simsobj.sims_library_location_name = dr["sims_library_location_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        [Route("CUDLibraryBin_pear")]
        public HttpResponseMessage CUDLibraryBin_pear(List<Sims116> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims116 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_bin_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_bin_code", simsobj.sims_library_bin_code),
                                new SqlParameter("@sims_library_bin_desc", simsobj.sims_library_bin_desc),
                                new SqlParameter("@sims_library_location_code", simsobj.sims_library_location_code),

                     });
                        if (ins > 0)
                        {
                           
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }   

}