﻿using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryFee")]
    public class LibraryFeeController : ApiController
    {
        [Route("getLibraryFee")]
        public HttpResponseMessage getLibraryFee()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getLibraryFee()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims120> mod_list = new List<Sims120>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_fee_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "S"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims120 simsobj = new Sims120();
                            simsobj.sims_library_fee_number = dr["sims_library_fee_number"].ToString();
                            simsobj.sims_library_privilege_number = dr["sims_library_privilege_number"].ToString();
                            simsobj.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                            simsobj.sims_library_fee_name = dr["sims_library_fee_name"].ToString();
                            simsobj.sims_library_fee_deposit_amount = dr["sims_library_fee_deposit_amount"].ToString();
                            simsobj.sims_library_fee_amount = dr["sims_library_fee_amount"].ToString();
                            simsobj.sims_library_fee_manual_receipt_number = dr["sims_library_fee_manual_receipt_number"].ToString();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            //simsobj.sims_library_fee_frequency = dr["sims_library_fee_frequency"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_library_fee_frequency"].ToString();
                            if (dr["sims_library_fee_installment_mode"].ToString().Equals("Y"))
                                simsobj.sims_library_fee_installment_mode = true;
                            else
                                simsobj.sims_library_fee_installment_mode = false;
                            simsobj.sims_library_fee_installment_minimum_amount = dr["sims_library_fee_installment_minimum_amount"].ToString();
                            simsobj.sims_library_fee_period1 = dr["sims_library_fee_period1"].ToString();
                            simsobj.sims_library_fee_period2 = dr["sims_library_fee_period2"].ToString();
                            simsobj.sims_library_fee_period3 = dr["sims_library_fee_period3"].ToString();
                            simsobj.sims_library_fee_period4 = dr["sims_library_fee_period4"].ToString();
                            simsobj.sims_library_fee_period5 = dr["sims_library_fee_period5"].ToString();
                            simsobj.sims_library_fee_period6 = dr["sims_library_fee_period6"].ToString();
                            simsobj.sims_library_fee_period7 = dr["sims_library_fee_period7"].ToString();
                            simsobj.sims_library_fee_period8 = dr["sims_library_fee_period8"].ToString();
                            simsobj.sims_library_fee_period9 = dr["sims_library_fee_period9"].ToString();
                            simsobj.sims_library_fee_period10 = dr["sims_library_fee_period10"].ToString();
                            simsobj.sims_library_fee_period11 = dr["sims_library_fee_period11"].ToString();
                            simsobj.sims_library_fee_period12 = dr["sims_library_fee_period12"].ToString();
                            if (dr["sims_library_fee_status"].ToString().Equals("A"))
                                simsobj.sims_library_fee_status = true;
                            else
                                simsobj.sims_library_fee_status = false;
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAllLibraryPrivilege")]
        public HttpResponseMessage getAllLibraryPrivilege()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getLibraryFee()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims120> mod_list = new List<Sims120>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_fee_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "G"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims120 simsobj = new Sims120();
                            simsobj.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                            simsobj.sims_library_privilege_number = dr["sims_library_privilege_number"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAllFeeMode")]
        public HttpResponseMessage getAllFeeMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getLibraryFee()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims120> mod_list = new List<Sims120>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_fee_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "N"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims120 simsobj = new Sims120();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getFeeNo")]
        public HttpResponseMessage getFeeNo()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getLibraryFee()PARAMETERS ::NA";
            //Log.Debug(string.Format(debug, "SETUP", "ERP/Grade"));
            List<Sims120> mod_list = new List<Sims120>();
            string groupcode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_fee_proc]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "R"),
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims120 simsobj = new Sims120();
                            simsobj.sims_library_fee_number = dr["sims_library_fee_number"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CUDLibraryFee")]
        public HttpResponseMessage CUDLibraryFee(List<Sims120> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims120 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_fee_proc]",
                                new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_library_fee_number", simsobj.sims_library_fee_number),
                            new SqlParameter("@sims_library_privilege_number", simsobj.sims_library_privilege_number),
                            new SqlParameter("@sims_library_fee_name", simsobj.sims_library_fee_name),

                            new SqlParameter("@sims_library_fee_deposit_amount", simsobj.sims_library_fee_deposit_amount),
                            new SqlParameter("@sims_library_fee_amount", simsobj.sims_library_fee_amount),
                            new SqlParameter("@sims_library_fee_manual_receipt_number", simsobj.sims_library_fee_manual_receipt_number),
                            new SqlParameter("@sims_library_fee_frequency_name", simsobj.sims_appl_form_field_value1),
                            
                            new SqlParameter("@sims_library_fee_installment_minimum_amount", simsobj.sims_library_fee_installment_minimum_amount),
                            new SqlParameter("@sims_library_fee_period1", simsobj.sims_library_fee_period1),
                            new SqlParameter("@sims_library_fee_period2", simsobj.sims_library_fee_period2),
                            new SqlParameter("@sims_library_fee_period3", simsobj.sims_library_fee_period3),
                            new SqlParameter("@sims_library_fee_period4", simsobj.sims_library_fee_period4),
                            new SqlParameter("@sims_library_fee_period5", simsobj.sims_library_fee_period5),
                            new SqlParameter("@sims_library_fee_period6", simsobj.sims_library_fee_period6),
                            new SqlParameter("@sims_library_fee_period7", simsobj.sims_library_fee_period7),
                            new SqlParameter("@sims_library_fee_period8", simsobj.sims_library_fee_period8),
                            new SqlParameter("@sims_library_fee_period9", simsobj.sims_library_fee_period9),
                            new SqlParameter("@sims_library_fee_period10", simsobj.sims_library_fee_period10),
                            new SqlParameter("@sims_library_fee_period11", simsobj.sims_library_fee_period11),
                            new SqlParameter("@sims_library_fee_period12", simsobj.sims_library_fee_period12),
            
                            new SqlParameter("@sims_library_fee_installment_mode",simsobj.sims_library_fee_installment_mode==true?"Y":"N"),
                            new SqlParameter("@sims_library_fee_status",simsobj.sims_library_fee_status==true?"A":"I"),

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

    }
}