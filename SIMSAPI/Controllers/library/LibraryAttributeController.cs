﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.LibraryClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.STUDENT.sibling_details_mode;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibraryAttribute")]
    [BasicAuthentication]
    public class LibraryAttributeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllLibraryAttribute")]
        public HttpResponseMessage getAllLibraryAttribute()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryAttribute(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryAttribute"));

            List<Sim115> goaltarget_list = new List<Sim115>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_attribute_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim115 simsobj = new Sim115();
                            simsobj.sims_library_attribute_code = dr["sims_library_attribute_code"].ToString();
                            simsobj.sims_library_attribute_name = dr["sims_library_attribute_name"].ToString();
                            simsobj.sims_library_attribute_status = dr["sims_library_attribute_status"].Equals("A")?true:false;

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDLibraryAttribute")]
        public HttpResponseMessage CUDLibraryAttribute(List<Sim115> data)
        {
            bool insert = false;
            
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim115 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_attribute_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_attribute_code",simsobj.sims_library_attribute_code),
                                new SqlParameter("@sims_library_attribute_name", simsobj.sims_library_attribute_name),
                                new SqlParameter("@sims_library_attribute_status", simsobj.sims_library_attribute_status==true?"A":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                           
                        }
                        else
                        {
                            insert = false;
                            
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

        
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getAllDuedetail")]
        public HttpResponseMessage getAllDuedetail(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDuedetail(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllDuedetail"));
            object obj = new object();
            List<Sim115> goaltarget_list = new List<Sim115>();

            Sim115 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sim115>(data);

            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_library_attribute_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'J'),
                           new SqlParameter("@sims_cur_code", sf.cur_code),
                           new SqlParameter("@sims_academic_year", sf.academic_year),
                           new SqlParameter("@sims_grade_code", sf.grade),
                           new SqlParameter("@sims_section_code", sf.section),
                         });
                    obj = ds;


                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getshow")]
        public HttpResponseMessage getshow()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sibling_details",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_appl_parameter"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getload_data")]
        public HttpResponseMessage getload_data(String show_report, String cur_code, String acad_year, String student_count, String search)
        {
            List<SIB10> stud_d = new List<SIB10>();
            try
            {

                if (search == "" || search == "undefined")
                {
                    search = null;
                }

                if (student_count == "" || student_count == "undefined")
                {
                    student_count = null;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sibling_details",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                             new SqlParameter("@show_report", show_report),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", acad_year),
                             new SqlParameter("@student_count", student_count),
                             new SqlParameter("@search", search)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SIB10 sequence = new SIB10();
                            sequence.sr_no = dr["sr_no"].ToString();
                            sequence.parent_name = dr["father_name"].ToString();
                            sequence.sibling_no = dr["sims_student_count"].ToString();
                            sequence.enroll_no = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.stud_class = dr["sims_grade_name_en"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getstud_ledge_data")]
        public HttpResponseMessage getstud_ledge_data(string cur_code, string acad_year, string grade_code, string section_code, string search)
        {
            List<FEELEG> stud_d = new List<FEELEG>();
            if (search == "undefined")
            {
                search = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[student_fee_ledger_report_app]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", acad_year),
                             new SqlParameter("@grade_code",grade_code),
                             new SqlParameter("@section_code", section_code),
                             new SqlParameter("@user_number", search)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FEELEG sequence = new FEELEG();
                            sequence.parent_no = dr["sims_sibling_parent_number"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            sequence.enroll_no = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("getfamily_stat")]
        public HttpResponseMessage getstud_ledge_data(string cur_code, string acad_year, string acc_no)
        {
            List<FAMSTAT> stud_d = new List<FAMSTAT>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[student_fee_ledger_report_app]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", acad_year),
                             new SqlParameter("@sltr_slmast_acno",acc_no)
                         });
                    if (dr.HasRows)
                    {
                        int index = 0;
                        while (dr.Read())
                        {
                            FAMSTAT sequence = new FAMSTAT();
                            sequence.date = dr["sims_doc_date"].ToString();
                            sequence.doc = dr["doc_no_new"].ToString();
                            sequence.type = dr["fee_description"].ToString();
                            sequence.description = dr["details"].ToString();
                            sequence.sims_paid_fee = dr["sims_paid_fee"].ToString();
                            sequence.sims_expected_fee = dr["sims_expected_fee"].ToString();

                            if (index == 0)
                            {

                                sequence.balance = (decimal.Parse(sequence.sims_paid_fee) - decimal.Parse(sequence.sims_expected_fee)).ToString();
                                //    if (decimal.Parse(sequence.balance) < 0)
                                //    {
                                //        sequence.balance = (decimal.Parse(sequence.balance) * -1).ToString();
                                //    }
                                //    else
                                //    {
                                //        sequence.balance = (decimal.Parse(sequence.balance)).ToString();
                                //    }
                            }
                            else
                            {
                                sequence.balance = ((decimal.Parse(stud_d[index - 1].balance)) + ((decimal.Parse(sequence.sims_paid_fee) - decimal.Parse(sequence.sims_expected_fee)))).ToString();
                                //    if (decimal.Parse(sequence.sims_paid_fee) <= 0)
                                //    {
                                //        sequence.balance = ((decimal.Parse(stud_d[index - 1].balance)) + decimal.Parse(sequence.sims_expected_fee)).ToString();
                                //    }
                                //    else
                                //    {
                                //        sequence.balance = ((decimal.Parse(stud_d[index - 1].balance)) - decimal.Parse(sequence.sims_paid_fee)).ToString();
                                //    }
                            }
                            index++;
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("insert_demo")]
        public HttpResponseMessage insert_demo(List<INDMO> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (INDMO simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[insert_data_demo]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@name",simsobj.name),
                                new SqlParameter("@pass", simsobj.pass)
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("get_inserted_data")]
        public HttpResponseMessage getstud_ledge_data()
        {
            List<INDMO> stud_d = new List<INDMO>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[insert_data_demo]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            INDMO sequence = new INDMO();
                            sequence.name = dr["name"].ToString();
                            sequence.pass = dr["pass"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("get_emp_salary")]
        public HttpResponseMessage get_emp_salary(string year, string user)
        {
            List<EMPSAL> stud_d = new List<EMPSAL>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[employee_salary_view]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@year_month", year),
                             new SqlParameter("@empid",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EMPSAL sequence = new EMPSAL();
                            sequence.paycode = dr["sd_pay_code"].ToString();
                            sequence.amount = dr["sd_amount"].ToString();
                            sequence.earn = dr["pays_appl_form_field_value1"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("get_emp_sal_month")]
        public HttpResponseMessage get_emp_sal_month()
        {
            List<EMPSAL> stud_d = new List<EMPSAL>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[employee_salary_view]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EMPSAL sequence = new EMPSAL();
                            sequence.sd_year_month = dr["sd_year_month"].ToString();
                            sequence.month = dr["month_name"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("get_emp_sal_year")]
        public HttpResponseMessage get_emp_sal_year()
        {
            List<EMPSAL> stud_d = new List<EMPSAL>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[employee_salary_view]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EMPSAL sequence = new EMPSAL();
                            sequence.year_code = dr["sims_academic_year"].ToString();
                            sequence.year = dr["sims_academic_year_description"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("get_emp_attendance")]
        public HttpResponseMessage get_emp_sal_year(string year, string user)
        {
            List<EMPSAL> stud_d = new List<EMPSAL>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[employee_salary_view]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "W"),
                              new SqlParameter("@year_month", year),
                             new SqlParameter("@empid",user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EMPSAL sequence = new EMPSAL();
                            sequence.absents = dr["absents"].ToString();
                            sequence.presents = dr["presents"].ToString();
                            sequence.holidays = dr["holidays"].ToString();
                            sequence.worddays = dr["worddays"].ToString();
                            sequence.leaves = dr["leaves"].ToString();
                            stud_d.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, stud_d);
        }

        [Route("GetCategory")]
        public HttpResponseMessage GetCategory()
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            simsobj.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSubCategory")]
        public HttpResponseMessage GetSubCategory()
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSubCategoryByCategory")]
        public HttpResponseMessage GetSubCategoryByCategory(string category)
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "SS"),
                           new SqlParameter("@category", category),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetCatalogue")]
        public HttpResponseMessage GetCatalogue()
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_library_catalogue_code = dr["sims_library_catalogue_code"].ToString();
                            simsobj.sims_library_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetLibStatus")]
        public HttpResponseMessage GetLibStatus()
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'D'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getLibraryReport")]
        public HttpResponseMessage GetLibraryReport(string category, string subcategory, string catalogue, string status)
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[LibraryReportApp]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@category", category),
                           new SqlParameter("@subcategory", subcategory),
                           new SqlParameter("@catalogue", catalogue),
                           new SqlParameter("@lib_status", status)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            simsobj.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            simsobj.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            simsobj.sims_library_bnn_number = dr["sims_library_bnn_number"].ToString();
                            simsobj.sims_library_item_dewey_number = dr["sims_library_item_dewey_number"].ToString();
                            simsobj.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            simsobj.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            simsobj.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            simsobj.sims_library_item_category_name = dr["sims_library_item_category_name"].ToString();
                            simsobj.sims_library_item_subcategory_name = dr["sims_library_item_subcategory_name"].ToString();
                            simsobj.sims_library_catelogue_name = dr["sims_library_catelogue_name"].ToString();
                            simsobj.sims_library_item_remark = dr["sims_library_item_remark"].ToString();
                            simsobj.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            simsobj.sims_library_item_date_of_publication = dr["sims_library_item_date_of_publication"].ToString();
                            simsobj.sims_library_item_qr_code = dr["sims_library_item_qr_code"].ToString();
                            simsobj.sims_library_item_bar_code = dr["sims_library_item_bar_code"].ToString();
                            simsobj.sims_library_item_purchase_price = dr["sims_library_item_purchase_price"].ToString();
                            simsobj.sims_library_item_number_of_copies = dr["sims_library_item_number_of_copies"].ToString();
                            simsobj.sims_library_item_quantity = dr["sims_library_item_quantity"].ToString();
                            simsobj.sims_library_item_entry_date = dr["sims_library_item_entry_date"].ToString();
                            simsobj.sims_library_item_location_name = dr["sims_library_item_location_name"].ToString();
                            simsobj.sims_library_bin_name = dr["sims_library_bin_name"].ToString();
                            simsobj.member_name = dr["member_name"].ToString();
                            simsobj.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                            simsobj.sims_library_item_expected_return_date = dr["sims_library_item_expected_return_date"].ToString();
                            simsobj.sims_library_item_actual_return_date = dr["sims_library_item_actual_return_date"].ToString();
                            simsobj.transaction_status = dr["transaction_status"].ToString();
                            simsobj.issue_return_status = dr["issue_return_status"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getMobileUsageDetailsbyuser")]
        public HttpResponseMessage getMobileUsageDetailsbyuser(string from_date, string to_date, string comn_user_name)
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            if (from_date == "undefined")
            {
                from_date = null;
            }

            if (to_date == "undefined")
            {
                to_date = null;
            }
            if (from_date == "undefined" && to_date == "undefined")
            {
                from_date = null;
                to_date = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.mobileAppUsage_proc",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@comn_user_name", comn_user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_user_full_name = dr["comn_user_full_name"].ToString();
                            simsobj.comn_user_device_model = dr["comn_user_device_model"].ToString();
                            simsobj.comn_audit_user_appl_code = dr["comn_audit_user_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            simsobj.comn_audit_end_time = dr["comn_audit_end_time"].ToString();
                            simsobj.user_id_code = dr["user_id_code"].ToString();
                            simsobj.user_mobile_number = dr["user_mobile_number"].ToString();
                            simsobj.user_email_address = dr["user_email_address"].ToString();
                            simsobj.comn_user_device_status = dr["comn_user_device_status"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getMobileUsageDetails")]
        public HttpResponseMessage getMobileUsageDetails(string from_date, string to_date)
        {
            List<LIBCAT> goaltarget_list = new List<LIBCAT>();
            if (from_date == "undefined")
            {
                from_date = null;
            }

            if (to_date == "undefined")
            {
                to_date = null;
            }   
            if (from_date == "undefined" && to_date == "undefined")
            {
                from_date = null;
                to_date = null;
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.mobileAppUsage_proc",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),
                           new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBCAT simsobj = new LIBCAT();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_user_full_name = dr["comn_user_full_name"].ToString();
                         //   simsobj.comn_user_device_model = dr["comn_user_device_model"].ToString();
                         //   simsobj.comn_audit_user_appl_code = dr["comn_audit_user_appl_code"].ToString();
                        //    simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_audit_start_time = dr["First_accessed"].ToString();
                            simsobj.comn_audit_end_time = dr["last_accessed"].ToString();
                            simsobj.user_id_code = dr["user_id_code"].ToString();
                            simsobj.user_mobile_number = dr["user_mobile_number"].ToString();
                            simsobj.user_email_address = dr["user_email_address"].ToString();
                            simsobj.no_of_views = dr["no_of_views"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("gethealthreport")]
        public HttpResponseMessage gethealthreport(string cur_name, string academic_year, string grd_code, string sec_code, string student_en)
        {
            if (student_en == "undefined" || student_en == "" || student_en == null)
                student_en = null;
            List<HealthReportModel> goaltarget_list = new List<HealthReportModel>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_healthReport",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                           new SqlParameter("@sims_cur_code", cur_name),
                           new SqlParameter("@sims_academic_year", academic_year),
                           new SqlParameter("@sims_grade_code", grd_code),
                           new SqlParameter("@sims_section_code", sec_code),
                           new SqlParameter("@sims_enrollment_number", student_en)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HealthReportModel simsobj = new HealthReportModel();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_student_passport_middle_name_en = dr["sims_student_passport_middle_name_en"].ToString();
                            simsobj.sims_student_passport_last_name_en = dr["sims_student_passport_last_name_en"].ToString();
                            simsobj.sims_student_family_name_en = dr["sims_student_family_name_en"].ToString();
                            simsobj.sims_student_nickname = dr["sims_student_nickname"].ToString();
                            simsobj.sims_student_passport_first_name_ot = dr["sims_student_passport_first_name_ot"].ToString();
                            simsobj.sims_student_passport_middle_name_ot = dr["sims_student_passport_middle_name_ot"].ToString();
                            simsobj.sims_student_passport_last_name_ot = dr["sims_student_passport_last_name_ot"].ToString();
                            simsobj.sims_student_family_name_ot = dr["sims_student_family_name_ot"].ToString();
                            simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                            simsobj.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            simsobj.sims_blood_group = dr["sims_blood_group"].ToString();
                            simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_health_card_number = dr["sims_health_card_number"].ToString();
                            simsobj.sims_health_card_issue_date = dr["sims_health_card_issue_date"].ToString();
                            simsobj.sims_health_card_expiry_date = dr["sims_health_card_expiry_date"].ToString();
                            simsobj.sims_health_card_issuing_authority = dr["sims_health_card_issuing_authority"].ToString();
                            simsobj.sims_blood_group_code = dr["sims_blood_group_code"].ToString();
                            simsobj.sims_health_restriction_status = dr["sims_health_restriction_status"].ToString();
                            simsobj.sims_health_restriction_desc = dr["sims_health_restriction_desc"].ToString();
                            simsobj.sims_disability_status = dr["sims_disability_status"].ToString();
                            simsobj.sims_disability_desc = dr["sims_disability_desc"].ToString();
                            simsobj.sims_medication_status = dr["sims_medication_status"].ToString();
                            simsobj.sims_medication_desc = dr["sims_medication_desc"].ToString();
                            simsobj.sims_health_other_status = dr["sims_health_other_status"].ToString();
                            simsobj.sims_health_other_desc = dr["sims_health_other_desc"].ToString();
                            simsobj.sims_health_hearing_status = dr["sims_health_hearing_status"].ToString();
                            simsobj.sims_health_hearing_desc = dr["sims_health_hearing_desc"].ToString();
                            simsobj.sims_health_vision_status = dr["sims_health_vision_status"].ToString();
                            simsobj.sims_health_vision_desc = dr["sims_health_vision_desc"].ToString();
                            simsobj.sims_regular_doctor_name = dr["sims_regular_doctor_name"].ToString();
                            simsobj.sims_regular_doctor_phone = dr["sims_regular_doctor_phone"].ToString();
                            simsobj.sims_regular_hospital_name = dr["sims_regular_hospital_name"].ToString();
                            simsobj.sims_regular_hospital_phone = dr["sims_regular_hospital_phone"].ToString();

                            simsobj.sims_height = dr["sims_height"].ToString();
                            simsobj.sims_wieght = dr["sims_wieght"].ToString();
                            simsobj.sims_teeth = dr["sims_teeth"].ToString();
                            simsobj.sims_health_other_disability_status = dr["sims_health_other_disability_status"].ToString();
                            simsobj.sims_health_other_disability_desc = dr["sims_health_other_disability_desc"].ToString();
                            simsobj.sims_health_bmi = dr["sims_health_bmi"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();

                            simsobj.sims_medical_consent_health_records = dr["sims_medical_consent_health_records"].ToString();
                            simsobj.sims_medical_consent_immunization = dr["sims_medical_consent_immunization"].ToString();
                            simsobj.sims_medical_consent_emergency_treatment = dr["sims_medical_consent_emergency_treatment"].ToString();
                            simsobj.sims_medical_consent_medical_treatment = dr["sims_medical_consent_medical_treatment"].ToString();
                            simsobj.sims_medical_consent_over_the_counter_medicine = dr["sims_medical_consent_over_the_counter_medicine"].ToString();
                            simsobj.sims_medical_consent_medicine_allowed = dr["sims_medical_consent_medicine_allowed"].ToString();
                            simsobj.sims_medical_consent_remark = dr["sims_medical_consent_remark"].ToString();
                            simsobj.sims_medical_consent_applicable_from_acad_year = dr["sims_medical_consent_applicable_from_acad_year"].ToString();
                            simsobj.sims_medical_consent_updated_on = dr["sims_medical_consent_updated_on"].ToString();
                            simsobj.sims_medical_consent_approved_by = dr["sims_medical_consent_approved_by"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getSlotDay")]
        public HttpResponseMessage getSlotDay(string AcaYear, string bellCode)
        {
            //if (student_en == "undefined" || student_en == "" || student_en == null)
            //    student_en = null;
            List<HealthReportModel> goaltarget_list = new List<HealthReportModel>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[TimeTableData]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", "AA"),
                           new SqlParameter("@AcaYear", AcaYear),
                           new SqlParameter("@bellCode", bellCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            HealthReportModel simsobj = new HealthReportModel();
                            simsobj.start_day = dr["start_day"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }   
    }
}










