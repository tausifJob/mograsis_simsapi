﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.library
{
    [RoutePrefix("api/LibrarySubcategory")]
    [BasicAuthentication]
    public class LibrarySubcategoryController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("autoIncrementLibrary")]
        public HttpResponseMessage autoIncrementLibrary()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryNumber(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "LibraryCategory", "getAllLibraryNumber"));

            List<Sim131> goaltarget_list = new List<Sim131>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_subcategory_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim131 simsobj = new Sim131();
                            simsobj.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }
        //Select
        [Route("getAllLibrarySubCategory")]
        public HttpResponseMessage getAllLibrarySubCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibrarySubCategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibrarySubCategory"));

            List<Sim131> goaltarget_list = new List<Sim131>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_subcategory_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim131 simsobj = new Sim131();
                            simsobj.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            simsobj.catagoryname = dr["catagoryname"].ToString();
                            simsobj.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            simsobj.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //Main catagory
        [Route("GetcategaryCodeName")]
        public HttpResponseMessage GetcategaryCodeName()

        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetcategaryCodeName()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetcategaryCodeName"));

            List<Sim131> doc_list = new List<Sim131>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_library_subcategory_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim131 simsobj = new Sim131();
                           
                            simsobj.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            simsobj.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("CUDLibrarySubCategory")]
        public HttpResponseMessage CUDLibrarySubCategory(Sim131 data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //foreach (Sim131 simsobj in data)
                    // {
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_subcategory_proc]",
                    new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", data.opr),
                                new SqlParameter("@sims_library_category_code",data.sims_library_category_code),
                                new SqlParameter("@sims_library_subcategory_code",data.sims_library_subcategory_code),
                                new SqlParameter("@sims_library_subcategory_name", data.sims_library_subcategory_name),

                     });
                    if (ins > 0)
                    {
                        insert = true;
                    }
                    else
                    {
                        insert = false;
                    }
                    // }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }
            catch (Exception x)
            {
               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }



        [Route("DLibrarySubCategory")]
        public HttpResponseMessage DLibrarySubCategory(List<Sim131> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {   
                    db.Open();
                    foreach (Sim131 simsobj in data)
                     {
                             int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_library_subcategory_proc]",
                             new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_library_category_code",simsobj.sims_library_category_code),
                                new SqlParameter("@sims_library_subcategory_code",simsobj.sims_library_subcategory_code),
                                new SqlParameter("@sims_library_subcategory_name", simsobj.sims_library_subcategory_name),

                     });
                    if (ins > 0)
                    {
                        insert = true;
                    }
                    else
                    {
                        insert = false;
                    }
                     }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }
            catch (Exception x)
            {
                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}










