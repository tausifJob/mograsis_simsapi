﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;

namespace SIMSAPI.Controllers.VehicleConsumption
{
    [RoutePrefix("api/vehicle")]
    public class VehicleConsumptionController : ApiController
    {

        //  public int sp_call_att_codes = 0;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
      

        /// vehicle consumption///

        [Route("VehicleConsumption")]
        public HttpResponseMessage VehicleConsumption(List<ExcelData> data)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertVehicleConsumption(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "Employee", "InsertVehicleConsumption", opr));
            int res = 0;
            string status = "";
            List<ExcelData> list = new List<ExcelData>();

            Message message = new Message();
            try
            {
                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        foreach (var simsobj in data)
                        {

                            db.Open();

                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_trans_consumption_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@sup_location_code",simsobj.sup_location_code),
                                new SqlParameter("@vehicle_code",simsobj.vehicle_code),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sale_time",simsobj.sale_time),
                                new SqlParameter("@unit_price",simsobj.unit_price),
                                new SqlParameter("@sale_qty",simsobj.sale_qty),
                                new SqlParameter("@total_amount",simsobj.Total_Amount),
                                new SqlParameter("@sale_period",simsobj.sale_period),
                                new SqlParameter("@driver_code",simsobj.driver_code),
                         });

                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    ExcelData obj = new ExcelData();
                                    obj.sup_code = dr["sup_name"].ToString();
                                    if (dr["sup_flag"].ToString() == "Y")
                                        obj.sup_code_flag = true;
                                    else
                                        obj.sup_code_flag = false;
                                    obj.sup_location_code = dr["sup_loc"].ToString();
                                    if (dr["sup_loc_flag"].ToString() == "Y")
                                        obj.sup_location_code_flag = true;
                                    else
                                        obj.sup_location_code_flag = false;
                                    obj.vehicle_code = dr["vehicle_name"].ToString();
                                    if (dr["vehicle_flag"].ToString() == "Y")
                                        obj.vehicle_code_flag = true;
                                    else
                                        obj.vehicle_code_flag = false;
                                    obj.im_inv_no = dr["item_name"].ToString();
                                    if (dr["item_flag"].ToString() == "Y")
                                        obj.im_inv_no_flag = true;
                                    else
                                        obj.im_inv_no_flag = false;
                                    //obj.driver_code = dr["driver_name"].ToString();
                                    //if (dr["driver_flag"].ToString() == "Y")
                                    //    obj.driver_code_flag = true;
                                    //else
                                    //    obj.driver_code_flag = false;
                                    if (!string.IsNullOrEmpty(dr["sale_time"].ToString()))
                                        obj.sale_time = DateTime.Parse(dr["sale_time"].ToString());
                                    obj.unit_price = dr["unit_price"].ToString();
                                    obj.sale_qty = dr["sale_qty"].ToString();
                                    obj.sale_period = dr["sale_period"].ToString();
                                    obj.Total_Amount = dr["total_amount"].ToString();
                                    list.Add(obj);
                                }
                            }
                            dr.Close();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);

                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
                }
            }
            catch (Exception x)
            {
                res = 0;
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [Route("InsertVehicleConsumption")]
        public HttpResponseMessage InsertVehicleConsumption(List<ExcelData> data)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (ExcelData simsobj in data)
                    {

                        dr = db.ExecuteStoreProcedure("[sims].[sims_trans_consumption_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","I"),
                                new SqlParameter("@sup_code",simsobj.sup_code),
                                new SqlParameter("@sup_location_code",simsobj.sup_location_code),
                                new SqlParameter("@vehicle_code",simsobj.vehicle_code),
                                new SqlParameter("@im_inv_no",simsobj.im_inv_no),
                                new SqlParameter("@sale_time",simsobj.sale_time),
                                new SqlParameter("@unit_price",simsobj.unit_price),
                                new SqlParameter("@sale_qty",simsobj.sale_qty),
                                new SqlParameter("@sale_period",simsobj.sale_period)
                                //new SqlParameter("@driver_code",simsobj.driver_code),

                         });
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                        dr.Close();
                    }
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        //////end vechicle consumption/////
        /// <returns></returns>

       

        public object opr { get; set; }
    }
}