﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COLLABORATION;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi
{
     [RoutePrefix("api/cirulardate")]
    public class CircularReportByDateController : ApiController
    {

           

               [Route("getdate")]
               public HttpResponseMessage getdate(string cur_code, string acad_year, string st_date, string end_date)
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_cir_rep_by_date",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                             new SqlParameter("@st_date",db.DBYYYYMMDDformat(st_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.sims_circular_number = dr["sims_circular_number"].ToString();
                                   sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                                   sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                                   sequence.sims_circular_date = dr["sims_circular_date"].ToString();
                                   sequence.sims_circular_publish_date = dr["sims_circular_publish_date"].ToString();
                                   sequence.sims_circular_expiry_date = dr["sims_circular_expiry_date"].ToString();
                                   sequence.sims_circular_title = dr["sims_circular_title"].ToString();
                                   sequence.sims_circular_short_desc = dr["sims_circular_short_desc"].ToString();
                                   sequence.sims_circular_desc = dr["sims_circular_desc"].ToString();
                                   sequence.sims_circular_file_path1 = dr["sims_circular_file_path1"].ToString();
                                   sequence.sims_circular_file_path2 = dr["sims_circular_file_path2"].ToString();
                                   sequence.sims_circular_file_path3 = dr["sims_circular_file_path3"].ToString();
                                   sequence.sims_circular_type = dr["sims_circular_type"].ToString();
                                   sequence.sims_circular_category = dr["sims_circular_category"].ToString();
                                   sequence.sims_circular_created_user_code = dr["sims_circular_created_user_code"].ToString();
                                   sequence.sims_circular_display_order = dr["sims_circular_display_order"].ToString();
                                   sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                                   sequence.publish_month = dr["publish_month"].ToString();
                                   sequence.circular_status = dr["circular_status"].ToString();
                                   sequence.sims_circular_category_desc = dr["sims_circular_category_desc"].ToString();
                                   sequence.sims_circular_type_desc = dr["sims_circular_type_desc"].ToString();
                                   sequence.sims_circular_created_by = dr["sims_circular_created_by"].ToString();
                                   sequence.circular_active_for = dr["circular_active_for"].ToString();

                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }

               [Route("getcircularbyuser")]
               public HttpResponseMessage getcircularbyuser(string cur_code, string from_date, string to_date, string group_code, string status)
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_rep_by_user",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@group_code",group_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@status",status),

                            
                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.sims_circular_number = dr["sims_circular_number"].ToString();
                                   sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                                   sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                                   sequence.sims_circular_date = dr["sims_circular_date"].ToString();
                                   sequence.group_name = dr["group_name"].ToString();
                                   sequence.sims_circular_user_name = dr["sims_circular_user_name"].ToString();
                                   sequence.sims_circular_title = dr["sims_circular_title"].ToString();
                                   sequence.name = dr["name"].ToString();
                                   sequence.circular_status = dr["circular_status"].ToString();
                                   sequence.sims_circular_remark = dr["sims_circular_remark"].ToString();
                                   sequence.circular_ack = dr["circular_ack"].ToString();
                                   sequence.sims_circular_status = dr["sims_circular_status"].ToString();
                                   sequence.sims_circular_user_group_code = dr["sims_circular_user_group_code"].ToString();


                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }

               [Route("getuser")]
               public HttpResponseMessage getuser()
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_rep_by_user",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                            
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                                   sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();


                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }

   
               [Route("getstatus")]
               public HttpResponseMessage getstatus()
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_circular_rep_by_user",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "L"),
                            
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                                   sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();


                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }


               [Route("getCommunicationData")]
               public HttpResponseMessage getCommunicationData(string cur_code, string acad_year , string grade, string section, string from_date,string to_date,string status,string search,string	group)
               {
                  
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       if (from_date == "null")
                       {
                           from_date = null;
                       }
                       if (to_date == "null")
                       {
                           to_date = null;
                       }
                       if (search == "undefined")
                       {
                           search = null;
                       }

                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[communication_report_new_ABPS]",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade",grade),
                            new SqlParameter("@section",section),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@status",status),
                            new SqlParameter("@search",search),
                            new SqlParameter("@group",group)

                         
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                                   sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                                   sequence.sims_comm_number = dr["sims_comm_number"].ToString();
                                   sequence.sims_comm_tran_number = dr["sims_comm_tran_number"].ToString();
                                   sequence.sims_comm_date = dr["sims_comm_date"].ToString();
                                   sequence.sims_comm_tran_date = dr["sims_comm_tran_date"].ToString();
                                   sequence.sims_comm_category = dr["sims_comm_category"].ToString();
                                   sequence.sims_comm_user_code = dr["sims_comm_user_code"].ToString();
                                   sequence.sims_subject_id = dr["sims_subject_id"].ToString();
                                   sequence.sims_comm_subject = dr["sims_comm_subject"].ToString();
                                   sequence.sender_id = dr["sender_id"].ToString();
                                   sequence.rece_id = dr["rece_id"].ToString();
                                   sequence.sims_comm_message = dr["sims_comm_message"].ToString();
                                   sequence.Initial_sender_name = dr["Initial_sender_name"].ToString();
                                   sequence.Initial_receiver_name = dr["Initial_receiver_name"].ToString();
                                   sequence.sender_name = dr["sender_name"].ToString();
                                   sequence.receiver_name = dr["receiver_name"].ToString();
                                   sequence.initial_send = dr["initial_send"].ToString();
                                   sequence.initial_receive = dr["initial_receive"].ToString();
                                   sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                                   sequence.sims_section_code = dr["sims_section_code"].ToString();
                                   sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                   sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                   sequence.student_name = dr["student_name"].ToString();
                                   sequence.status = dr["status"].ToString();
                                   
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }



               [Route("getstatusCommunicationABPS")]
               public HttpResponseMessage getstatus_Communication_ABPS()
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[communication_report_new_ABPS]",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                                   sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();


                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }


               [Route("getCommunicationUserABPS")]
               public HttpResponseMessage getCommunicationUserABPS()
               {
                   List<CommonCollaboration> lstModules = new List<CommonCollaboration>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[communication_report_new_ABPS]",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonCollaboration sequence = new CommonCollaboration();
                                   sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                                   sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }
   
     
     
     
     }




}