﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.HealthCommonClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.FinnCommonReport;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.ReportApi.health
{
     [RoutePrefix("api/StudentHealthReport")]
    public class StudentHealthReportController : ApiController
    {
         [Route("GetStudentDetails")]
         public HttpResponseMessage GetStudentDetails(string cur_code, string acad_year, string grade, string section)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
             //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

             List<StudRpt> doc_list = new List<StudRpt>();

             Message message = new Message();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_health_student_report_proc",
                         new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                         });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             StudRpt simsobj = new StudRpt();
                             simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                             simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                             simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                             simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             simsobj.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                             simsobj.student_name = dr["student_name"].ToString();
                             simsobj.sims_susceptibility_desc = dr["sims_susceptibility_desc"].ToString();
                             simsobj.sims_complaint_desc = dr["sims_complaint_desc"].ToString();
                             simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                             simsobj.medicines = dr["medicines"].ToString();
                             simsobj.sims_medical_visit_number = dr["sims_medical_visit_number"].ToString();
                             
                             doc_list.Add(simsobj);
                         }
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                     }
                     else
                         return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                 }
             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 message.strMessage = "No Records Found";
                 message.messageType = MessageType.Error;
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
             }

             //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
         }
    }
}