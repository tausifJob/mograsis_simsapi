﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;
using System.Net;



namespace SIMSAPI.Controllers.ReportApi.health
{
       [RoutePrefix("api/StudentHealth")]
       public class StudentHealthRptController : ApiController
    {
            [Route("getstuhealth")]

           public HttpResponseMessage getstuhealth(string cur_code, string acad_year, string grade_code, string section_code, string bmi_status)
          {
              List<CommonHealth> lstModules = new List<CommonHealth>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_health",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),                                                      
                            new SqlParameter("@bmi_status",bmi_status)                            
                          
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonHealth sequence = new CommonHealth();
                              sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                              sequence.student_name = dr["student_name"].ToString();
                              sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                              sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                              sequence.sims_height = dr["sims_height"].ToString();
                              sequence.sims_wieght = dr["sims_wieght"].ToString();
                              sequence.sims_health_bmi = dr["sims_health_bmi"].ToString();
                          
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }


            [Route("getbmistatus")]
          public HttpResponseMessage getbmistatus()
          {
              List<CommonHealth> lstModules = new List<CommonHealth>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_health",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                            
                         }
                             );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonHealth sequence = new CommonHealth();
                              sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                              sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }
    }
}