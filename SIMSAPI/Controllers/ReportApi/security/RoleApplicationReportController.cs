﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.security
{
   
        
    [RoutePrefix("api/RoleApplication")]
    
    public class  RoleApplicationReportController : ApiController
    {
        [Route("getroleappl")]
        public HttpResponseMessage getroleappl(string cur_code, string role_code)
        {
            List<CommonSecurity> lstModules = new List<CommonSecurity>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_role_application",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@role_code",role_code)                    

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonSecurity sequence = new CommonSecurity();
                            sequence.comn_appl_code = dr["comn_appl_code"].ToString();
                            sequence.comn_role_name = dr["comn_role_name"].ToString();
                            sequence.comn_role_code = dr["comn_role_code"].ToString();
                            sequence.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                           

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getrolecode")]
        public HttpResponseMessage getrolecode()
        {
            List<CommonSecurity> lstModules = new List<CommonSecurity>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_role_application",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "X"),
                           
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonSecurity sequence = new CommonSecurity();
                            sequence.comn_role_code = dr["comn_role_code"].ToString();
                            sequence.comn_role_name = dr["comn_role_name"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getroleassign")]
         public HttpResponseMessage getroleassign(string cur_code, string grp_code)
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_role_assignment",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@grp_code",grp_code)
                            

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                             sequence.appName = dr["AppName"].ToString();
                             sequence.comn_user_code = dr["comn_user_code"].ToString();
                             sequence.comn_user_name = dr["comn_user_name"].ToString();
                             sequence.comn_appl_user_name = dr["comn_appl_user_name"].ToString();
                             sequence.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                             sequence.appl_assigned_user_name = dr["appl_assigned_user_name"].ToString();
                             sequence.appl_assigned_user_id = dr["appl_assigned_user_id"].ToString();
                             sequence.comn_user_appl_assigned_by_user_code = dr["comn_user_appl_assigned_by_user_code"].ToString();
                             sequence.comn_user_appl_assigned_date = dr["comn_user_appl_assigned_date"].ToString();



                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getcodegroup")]
         public HttpResponseMessage getcodegroup()
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_role_assignment",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                             sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getstatusdetails")]
         public HttpResponseMessage getstatusdetails(string cur_code, string user_type, string user_status)
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_status_reports",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@user_type",user_type),
                            new SqlParameter("@user_status",user_status)


                            

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             sequence.comn_user_emp_code = dr["comn_user_emp_code"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             sequence.name = dr["NAME"].ToString();
                           


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getusertype")]
         public HttpResponseMessage getusertype()
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_status_reports",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                             sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getuserstatus")]
         public HttpResponseMessage getuserstatus()
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_status_reports",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getlogdetails")]
         public HttpResponseMessage getlogdetails(string cur_code, string group_code, string from_date, string to_date, string login)
         {
             
             List<CommonSecurity> lstModules = new List<CommonSecurity>();

             if (from_date == "undefined" || to_date == "undefined" || from_date == "" || to_date == "")
                         {
                         from_date=null;
                         to_date=null;
                         }
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_login_details",
                         new List<SqlParameter>() 
                         
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@group_code",group_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@login",login)


                            

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_code = dr["comn_user_code"].ToString();
                             sequence.comn_user_name = dr["comn_user_name"].ToString();
                             sequence.comn_user_alias = dr["comn_user_alias"].ToString();
                             sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                             sequence.comn_user_last_login = dr["comn_user_last_login"].ToString();
                             sequence.group_name = dr["group_name"].ToString();
                             sequence.name = dr["name"].ToString();


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getusergroup")]
         public HttpResponseMessage getusergroup()
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_login_details",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "K"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                             sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getlogindetail")]
         public HttpResponseMessage getlogindetail()
         {
             List<CommonSecurity> lstModules = new List<CommonSecurity>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_user_login_details",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "I"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonSecurity sequence = new CommonSecurity();
                             sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
    }
}