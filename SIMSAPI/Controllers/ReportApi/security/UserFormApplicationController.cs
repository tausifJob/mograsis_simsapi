﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;
namespace SIMSAPI.Controllers.ReportApi.security
{
    [RoutePrefix("api/CommonUserFormApplication")]
    public class UserFormApplicationController : ApiController
    {

       


        [Route("postUserFormApp")]
        public HttpResponseMessage postUserFormApp(CommonSecurity itsr)
        {

            if (itsr.search == "undefined"||itsr.search =="")
            {
               itsr.search = null;
            }

            List<CommonSecurity> lstModules = new List<CommonSecurity>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_form_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@module_code",itsr.module_code),
                            new SqlParameter("@app_type",itsr.app_type),
                            new SqlParameter("@app_code",itsr.app_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(itsr.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(itsr.to_date)),
                            new SqlParameter("@search",itsr.search)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonSecurity sequence = new CommonSecurity();
                            sequence.comn_user_code = dr["comn_user_code"].ToString();
                            sequence.comn_user_name = dr["comn_user_name"].ToString();
                            sequence.comn_user_last_login = dr["comn_user_last_login"].ToString();
                            sequence.application_type = dr["application_type"].ToString();
                            sequence.user_count = dr["user_count"].ToString();
                            sequence.comn_role_name = dr["comn_role_name"].ToString();
                            sequence.comn_user_appl_assigned_date = dr["comn_user_appl_assigned_date"].ToString();
                            sequence.assign_by = dr["assign_by"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }





        [Route("getUserFormAppHyperLink")]
        public HttpResponseMessage getUserFormAppHyperLink(string user_code, string module_code, string app_code, string app_type, string from_date, string to_date)
        {
            List<CommonSecurity> lstModules = new List<CommonSecurity>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_form_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@user_code",user_code),
                            new SqlParameter("@module_code",module_code),
                            new SqlParameter("@app_code", app_code),
                            new SqlParameter("@app_type", app_type),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            
                           
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonSecurity sequence = new CommonSecurity();
                            //sequence.comn_user_code = dr["comn_user_code"].ToString();
                            //sequence.comn_user_name = dr["comn_user_name"].ToString();
                            sequence.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            sequence.comn_audit_end_time = dr["comn_audit_end_time"].ToString();
                            sequence.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                              lstModules.Add(sequence);
                           
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



       

        [Route("getAppName")]
        public HttpResponseMessage getAppName(string module_code)
        {
            List<CommonSecurity> lstModules = new List<CommonSecurity>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_form_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                            new SqlParameter("@module_code",module_code)
                           
                        }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonSecurity sequence = new CommonSecurity();
                            sequence.sims_appl_code = dr["comn_appl_code"].ToString();
                            sequence.sims_appl_name = dr["comn_appl_name_en"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



    }
}