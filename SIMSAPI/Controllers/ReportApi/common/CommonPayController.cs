﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi
{
     [RoutePrefix("api/payroll")]
    public class CommonPayController : ApiController
    {
         [Route("getSalaryIncRpt")]
         public HttpResponseMessage getSalaryIncRpt(string company_code, string dept, string designation)
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Salary_Increment_Report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@company_code",company_code),
                              new SqlParameter("@dept",dept),
                              new SqlParameter("@designation",designation)
                        }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.em_login_code = dr["em_login_code"].ToString();
                             sequence.emp_name = dr["emp_name"].ToString();
                             sequence.sd_year_month = dr["sd_year_month"].ToString();
                             sequence.cp_desc = dr["cp_desc"].ToString();
                             sequence.sd_amount = dr["sd_amount"].ToString();
                             sequence.inc_month = dr["inc_month"].ToString();
                             sequence.pai_increment_type = dr["pai_increment_type"].ToString();
                             sequence.inc_amt = dr["inc_amt"].ToString();
                             lstModules.Add(sequence);





                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getSalaryIncRptDept")]
         public HttpResponseMessage getSalaryIncRptDept(string company_code)
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Salary_Increment_Report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@company_code", company_code)
                            
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                             sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                              lstModules.Add(sequence);

                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getSalaryIncRptDesignation")]
         public HttpResponseMessage getSalaryIncRptDesignation(string company_code)
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Salary_Increment_Report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                             new SqlParameter("@company_code",company_code)
                            
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.dg_code = dr["dg_code"].ToString();
                             sequence.dg_desc = dr["dg_desc"].ToString();	
                             lstModules.Add(sequence);

                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


         [Route("getAirFareReport")]
         public HttpResponseMessage getAirFareReport(string company_code, string year, string month)
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Air_Fare_report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@company_code",company_code),
                              new SqlParameter("@year",year),
                              new SqlParameter("@month",month)
                        }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.afepd_em_number = dr["afepd_em_number"].ToString();
                             sequence.ename = dr["ename"].ToString();
                             sequence.gr_desc = dr["gr_desc"].ToString();
                             sequence.ds_name = dr["ds_name"].ToString();
                             sequence.month_en = dr["month_en"].ToString();
                             sequence.afepd_unit_rate = dr["afepd_unit_rate"].ToString();
                             lstModules.Add(sequence);




                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


         [Route("getAirFareReportYear")]
         public HttpResponseMessage getAirFareReportYear()
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Air_Fare_report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                            
                        }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                             sequence.current_year = dr["current_year"].ToString();
                             lstModules.Add(sequence);


                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


         [Route("getAirFareReportMonth")]
         public HttpResponseMessage getAirFareReportMonth(string year)
         {
             List<ComnPay> lstModules = new List<ComnPay>();


             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Air_Fare_report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("year",year)
                            
                        }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnPay sequence = new ComnPay();
                             sequence.paysheet_month = dr["paysheet_month"].ToString();
                             sequence.paysheet_month_name = dr["paysheet_month_name"].ToString();
                             lstModules.Add(sequence);	


                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }







    }
}