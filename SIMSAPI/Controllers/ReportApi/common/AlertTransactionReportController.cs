﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.common
{
    [RoutePrefix("api/alertreport")]
    public class AlertTransactionReportController : ApiController
    {
        [Route("getalerttransactionReport")]
        public HttpResponseMessage getalerttransactionReport(string mom_start_date, string mom_end_date, string doc_search)
        {
            List<ATRC01> alertdetail = new List<ATRC01>();
            if (doc_search == "undefined" || doc_search == "")
            {
                doc_search = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_comm_AlertTransactionReport",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),
                                     new SqlParameter("@end_date", db.DBYYYYMMDDformat(mom_end_date)),
                                     new SqlParameter("@search",doc_search),



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ATRC01 sequence = new ATRC01();
                            sequence.comn_alert_user_code = dr["comn_alert_user_code"].ToString();
                            sequence.comn_alert_date = dr["comn_alert_date"].ToString();
                            sequence.comn_alert_message = dr["comn_alert_message"].ToString();
                            sequence.priority = dr["priority"].ToString();
                            sequence.comn_alert_type_code = dr["comn_alert_type_code"].ToString();
                            sequence.user_name = dr["user_name"].ToString();
                            sequence.comn_alert_status = dr["comn_alert_status"].ToString();
                            alertdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, alertdetail);
        }

    }
}






