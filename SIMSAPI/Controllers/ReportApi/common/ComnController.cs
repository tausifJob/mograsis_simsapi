﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ComnFile;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi
{
     [RoutePrefix("api/getComnFile")]
    public class ComnController:ApiController
    {


         [Route("getAlert")]
         public HttpResponseMessage getAlert(string cur_code, string from_date, string to_date)

         {

             //var from = "";
             //var to = "";
             //if (from_date == "null")
             //{
             //    from = null;
             //}

             //if (to_date == "null")
             //{
             //    to = null;
             //}

             List<ComnClassFile> lstModules = new List<ComnClassFile>();
             
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Comn_Alert]",
                         new List<SqlParameter>() 

                         { 
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date))  
                           
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnClassFile sequence = new ComnClassFile();
                             sequence.comn_alert_user_code = dr["comn_alert_user_code"].ToString();
                             sequence.user_name = dr["user_name"].ToString();
                             sequence.comn_alert_message = dr["comn_alert_message"].ToString();
                             sequence.priority = dr["priority"].ToString();
                             sequence.comn_alert_date = dr["comn_alert_date"].ToString();
                             sequence.comn_alert_status = dr["comn_alert_status"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getTeacherObservation_ASD")]
         public HttpResponseMessage getTeacherObservation_ASD(string cur_code, string acad_year, string grade, string section, string subject, string from_date, string to_date)
         {


             if (subject == "undefined")
             {
                 subject = null;
             }
             if (grade == "undefined")
             {
                 grade = null;
             }
             if (section == "undefined")
             {
                 section = null;
             }
             if (from_date == "undefined")
             {
                 from_date = null;
             }
             if (to_date == "undefined")
             {
                 to_date = null;
             }

             List<ComnClassFile> teacher_data = new List<ComnClassFile>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_observation_rpt]",
                         new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'S'),   
                           new SqlParameter("@cur_code",cur_code),
                           new SqlParameter("@acad_year",acad_year),
                           new SqlParameter("@grade",grade),
                           new SqlParameter("@section",section),
                           new SqlParameter("@subject",subject),
                           new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                                   
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnClassFile sequence = new ComnClassFile();
                             sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             //sequence.sims_drop_down_display_code = dr["sims_drop_down_display_code"].ToString();
                             sequence.sims_lesson_subject_code = dr["sims_lesson_subject_code"].ToString();
                             sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                             sequence.observation = dr["observation"].ToString();
                             sequence.outstanding = dr["outstanding"].ToString();
                             sequence.very_good = dr["very_good"].ToString();
                             sequence.acceptable = dr["acceptable"].ToString();
                             sequence.good = dr["good"].ToString();
                             sequence.very_weak = dr["very_weak"].ToString();
                             sequence.weak = dr["weak"].ToString();

                             teacher_data.Add(sequence);

                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, teacher_data);
         }

         [Route("getTeacherObservationLink_ASD")]
         public HttpResponseMessage getTeacherObservationLink_ASD(string sims_cur_code, string sims_academic_year,string grade, string section,string from_date, string to_date, string sims_lesson_subject_code, string display_code)
         {
             
             if (grade == "undefined")
             {
                 grade = null;
             }
             if (section == "undefined")
             {
                 section = null;
             }
             if (from_date == "undefined")
             {
                 from_date = null;
             }
             if (to_date == "undefined")
             {
                 to_date = null;
             }
       
             List<ComnClassFile> teacher_data = new List<ComnClassFile>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_teacher_observation_rpt]",
                         new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'A'),   
                           new SqlParameter("@cur_code",sims_cur_code),
                           new SqlParameter("@acad_year",sims_academic_year),
                            new SqlParameter("@grade",grade),
                           new SqlParameter("@section",section),
                           new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                           new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                           new SqlParameter("@subject",sims_lesson_subject_code),
                           new SqlParameter("@display_code",display_code),
                                   
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             ComnClassFile sequence = new ComnClassFile();
                             sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                             sequence.sims_lesson_creation_date = dr["sims_lesson_creation_date"].ToString();
                             sequence.sims_comment_description = dr["sims_comment_description"].ToString();
                             sequence.sims_lesson_created_by = dr["sims_lesson_created_by"].ToString();
                             sequence.sims_lesson_created_for = dr["sims_lesson_created_for"].ToString();
                             sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                             sequence.sims_drop_down_display_text = dr["sims_drop_down_display_text"].ToString();
                             sequence.sims_drop_down_display_code = dr["sims_drop_down_display_code"].ToString();

                             teacher_data.Add(sequence);

                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, teacher_data);
         }






         public object sims_cur_code { get; set; }

         public object sims_academic_year { get; set; }

         public object sims_lesson_subject_code { get; set; }

         public object sims_lesson_creation_date { get; set; }
    }
}