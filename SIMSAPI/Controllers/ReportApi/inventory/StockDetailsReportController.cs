﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi.inventory
{
          [RoutePrefix("api/StockDetailsReport")]
    
    public class  StockDetailsReportController : ApiController
    {
        [Route("getstock")]
              public HttpResponseMessage getstock(string cur_code, string cat, string subcat)
        {
            List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_stock_details",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@cat",cat),
                            new SqlParameter("@subcat",subcat)                    


                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InventoryCommonReport sequence = new InventoryCommonReport();
                            sequence.im_item_code = dr["im_item_code"].ToString();
                            sequence.im_inv_no = dr["im_inv_no"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.loc_code = dr["loc_code"].ToString();
                            sequence.loc_name = dr["loc_name"].ToString();
                            sequence.il_cur_qty = dr["il_cur_qty"].ToString();                           

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getcat")]
        public HttpResponseMessage getcat()
        {
            List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_stock_details",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                           
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InventoryCommonReport sequence = new InventoryCommonReport();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

         [Route("getsubcat")]
         public HttpResponseMessage getsubcat(string cat)
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_stock_details",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@cat", cat)                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.pc_code = dr["pc_code"].ToString();
                             sequence.pc_desc = dr["pc_desc"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


         [Route("getrequest")]
         public HttpResponseMessage getrequest(string dname, string rt, string rs, string from_date, string to_date)
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             if (from_date == "undefined" || to_date == "undefined" || from_date == "" || to_date == "")
             {
                 from_date = null;
                 to_date = null;
             }
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_request_detail_summery",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@dname",dname),
                            new SqlParameter("@rt",rt),
                            new SqlParameter("@rs",rs),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))


                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.dep_name = dr["dep_name"].ToString();
                             sequence.req_no = dr["req_no"].ToString();
                             sequence.req_type = dr["req_type"].ToString();
                             sequence.rd_item_desc = dr["rd_item_desc"].ToString();
                             sequence.req_date = dr["req_date"].ToString();
                             sequence.sm_name = dr["sm_name"].ToString();
                             sequence.req_status = dr["req_status"].ToString();

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getdepartment")]
         public HttpResponseMessage getdepartment()
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_request_detail_summery",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "K"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.dep_name = dr["dep_name"].ToString();
                             sequence.dep_code = dr["dep_code"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getreptype")]
         public HttpResponseMessage getreptype()
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_request_detail_summery",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.invs_appl_parameter = dr["invs_appl_parameter"].ToString();
                             sequence.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getstatusp")]
         public HttpResponseMessage getstatusp()
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_request_detail_summery",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "R"),
                                                       
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.invs_appl_parameter = dr["invs_appl_parameter"].ToString();
                             sequence.invs_appl_form_field_value1 = dr["invs_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

       

         [Route("getsales")]
         public HttpResponseMessage getsales(string icom_code, string dep_code, string from_date, string to_date)
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_sales_document_list",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"), 
                             new SqlParameter("@icom_code",icom_code),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.dep_name = dr["dep_name"].ToString();
                             sequence.doc_prov_no = dr["doc_prov_no"].ToString();
                             sequence.doc_date = dr["doc_date"].ToString();
                             sequence.dt_desc = dr["dt_desc"].ToString();
                             sequence.sal_name = dr["sal_name"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.sm_name = dr["sm_name"].ToString();
                             sequence.doc_total_amount = dr["doc_total_amount"].ToString();
                             sequence.status = dr["status"].ToString();
                             

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

               [Route("getcomp")]
         public HttpResponseMessage getcomp()
         {
             List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_sales_document_list",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             InventoryCommonReport sequence = new InventoryCommonReport();
                             sequence.comp_code = dr["comp_code"].ToString();
                             sequence.comp_name = dr["comp_name"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


               [Route("getdepart")]
               public HttpResponseMessage getdepart(string icom_code)
               {
                   List<InventoryCommonReport> lstModules = new List<InventoryCommonReport>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_sales_document_list",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Y"),
                             new SqlParameter("@icom_code", icom_code),
                           
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   InventoryCommonReport sequence = new InventoryCommonReport();
                                   sequence.dep_code = dr["dep_code"].ToString();
                                   sequence.dep_name = dr["dep_name"].ToString();
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }
    }

    }


