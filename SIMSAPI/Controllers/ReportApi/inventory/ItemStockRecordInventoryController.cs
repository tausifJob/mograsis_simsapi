﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ComnInvData;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi.inventory
{
    [RoutePrefix("api/Inv")]
    public class ItemStockRecordInventoryController : ApiController
    {
        [Route("getItemReorderList")]
        public HttpResponseMessage getItemReorderList(string cur_code, string category, string subcategory, string dep_code)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_reorder_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@category",category),
                            new SqlParameter("@subcategory",subcategory),
                            new SqlParameter("@dep_code",dep_code)          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.im_inv_no = dr["im_inv_no"].ToString();
                            sequence.im_item_code = dr["im_item_code"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.Category = dr["Category"].ToString();
                            sequence.SubCategory = dr["SubCategory"].ToString();
                            sequence.im_approximate_lead_time = dr["im_approximate_lead_time"].ToString();
                            sequence.im_average_month_demand = dr["im_average_month_demand"].ToString();
                            sequence.im_reorder_level = dr["im_reorder_level"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemReorderListCategory")]
        public HttpResponseMessage getItemReorderListCategory()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_reorder_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_category_group = dr["pc_category_group"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemReorderListSubCategory")]
        public HttpResponseMessage getItemReorderListSubCategory(string category)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_reorder_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@category",category),
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_category_group = dr["pc_category_group"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemReorderListDept")]
        public HttpResponseMessage getItemReorderListDept()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_reorder_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                           
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_code = dr["dep_code"].ToString();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.com_code = dr["com_code"].ToString();
                            lstModules.Add(sequence);
                            		

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemStockReport")]
        public HttpResponseMessage getItemStockReport(string comp_code, string cat, string subcat, string department)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_Stock_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@cat",cat),
                            new SqlParameter("@subcat",subcat),
                            new SqlParameter("@department",department)          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.im_inv_no = dr["im_inv_no"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.id_yob_qty = dr["id_yob_qty"].ToString();
                            sequence.im_malc_rate = dr["im_malc_rate"].ToString();
                            sequence.ad_qty = dr["ad_qty"].ToString();
                             sequence.dd_qty = dr["dd_qty"].ToString();
                             sequence.toatl_value = dr["toatl_value"].ToString();
                             sequence.toatl_value_date = dr["toatl_value_date"].ToString();
                             sequence.net_balance = dr["net_balance"].ToString();
                             sequence.net_value = dr["net_value"].ToString();
                             lstModules.Add(sequence);
                            
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemStockCategory")]
        public HttpResponseMessage getItemStockCategory()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_Stock_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getItemStockSubategory")]
        public HttpResponseMessage getItemStockSubategory( string cat)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_Stock_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                             new SqlParameter("@cat", cat),
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getItemStockDept")]
        public HttpResponseMessage getItemStockDept(string comp_code)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Item_Stock_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                             new SqlParameter("@comp_code",comp_code),
                           
                              
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_code = dr["dep_code"].ToString();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.com_code = dr["com_code"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getCancelledSaleDocumentList")]
        public HttpResponseMessage getCancelledSaleDocumentList(string comp_code, string dep_code, string from_date, string to_date)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            { 
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_cancelled_sells_document_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.doc_prov_no = dr["doc_prov_no"].ToString();
                            sequence.doc_date = dr["doc_date"].ToString();
                            sequence.dt_desc = dr["dt_desc"].ToString();
                            sequence.sal_name = dr["sal_name"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sm_name = dr["sm_name"].ToString();
                            sequence.doc_total_amount = dr["doc_total_amount"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getCancelledSaleDocumentListDept")]
        public HttpResponseMessage getCancelledSaleDocumentListDept(string comp_code)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_cancelled_sells_document_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp_code",comp_code),
                                   
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_code = dr["dep_code"].ToString();
                            sequence.dep_name = dr["dep_name"].ToString();
                            
                            lstModules.Add(sequence);
                            	

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSalesReportByItem")]
        public HttpResponseMessage getSalesReportByItem(string from_date, string to_date, string View_as)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Sales_report_By_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                             new SqlParameter("@View_as",View_as),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.im_item_code = dr["im_item_code"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.name = dr["name"].ToString();
                            sequence.book_status = dr["book_status"].ToString();
                            sequence.im_malc_rate = dr["im_malc_rate"].ToString();
                            sequence.dd_sell_price = dr["dd_sell_price"].ToString();
                            sequence.dd_qty = dr["dd_qty"].ToString();
                            lstModules.Add(sequence);
                            
                       }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getCostingSheetSummary")]
        public HttpResponseMessage getCostingSheetSummary(string from_date, string to_date)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Inv_Costing_Sheet_Summary]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.cs_no = dr["cs_no"].ToString();
                            sequence.cs_prov_date = dr["cs_prov_date"].ToString();
                            sequence.supplier = dr["supplier"].ToString();
                            sequence.gross = dr["gross"].ToString();
                            sequence.od_total_discount = dr["od_total_discount"].ToString();
                            sequence.od_total_expense = dr["od_total_expense"].ToString();
                            sequence.cs_landed_cost = dr["cs_landed_cost"].ToString();
                           lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSalesDocumentListSalesman")]
        public HttpResponseMessage getSalesDocumentListSalesman(string comp_code, string dep_code, string from_date, string to_date, string sm_code)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_sales_document_list_salesman]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@dep_code",dep_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)) ,
                             new SqlParameter("@sm_code",sm_code)
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.doc_prov_no = dr["doc_prov_no"].ToString();
                            sequence.doc_date = dr["doc_date"].ToString();
                            sequence.dt_desc = dr["dt_desc"].ToString();
                            sequence.sal_name = dr["sal_name"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sm_name = dr["sm_name"].ToString();
                            sequence.doc_total_amount = dr["doc_total_amount"].ToString();
                            sequence.status = dr["status"].ToString();
                            sequence.doc_reference_prov_no = dr["doc_reference_prov_no"].ToString();
                            sequence.doc_status = dr["doc_status"].ToString();
                            lstModules.Add(sequence);
                          






                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getSalesDocumentListSalesmanDEPT")]
        public HttpResponseMessage getSalesDocumentListSalesmanDEPT(string comp_code)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_sales_document_list_salesman]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp_code",comp_code),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.dep_code = dr["dep_code"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSalesDocumentListSalesmanCode")]
        public HttpResponseMessage getSalesDocumentListSalesmanCode()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_sales_document_list_salesman]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.sm_code = dr["sm_code"].ToString();
                            sequence.sm_name = dr["sm_name"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStockAgingReport")]
        public HttpResponseMessage getStockAgingReport(string comp_code, string Date, string cat, string subcat,string View_As)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_stock_aging_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@cat",cat),
                            new SqlParameter("@subcat",subcat),
                            new SqlParameter("@Date",db.DBYYYYMMDDformat(Date)),
                            new SqlParameter("@View_As",View_As)          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.item = dr["item"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.zero_thirty = dr["zero_thirty"].ToString();
                            sequence.zero_thirty_P = dr["zero_thirty_P"].ToString();
                            sequence.thirtyone_sixty = dr["thirtyone_sixty"].ToString();
                            sequence.thirtyone_sixty_P = dr["thirtyone_sixty_P"].ToString();
                            sequence.sixtyone_ninty = dr["sixtyone_ninty"].ToString();
                            sequence.sixtyone_ninty_P = dr["sixtyone_ninty_P"].ToString();
                            sequence.nintyone_onetwenty = dr["nintyone_onetwenty"].ToString();
                            sequence.nintyone_onetwenty_P = dr["nintyone_onetwenty_P"].ToString();
                            sequence.onetwenty_plus = dr["onetwenty_plus"].ToString();
                            sequence.onetwenty_plus_P = dr["onetwenty_plus_P"].ToString();
                            sequence.total = dr["total"].ToString();
                            lstModules.Add(sequence);
                            
                           

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getStockAgingReportCat")]
        public HttpResponseMessage getStockAgingReportCat()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_stock_aging_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            lstModules.Add(sequence);
                            		


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStockAgingReportSubCat")]
        public HttpResponseMessage getStockAgingReportSubCat(string cat)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_stock_aging_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@cat", cat)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getInventoryVoluationReport")]
        public HttpResponseMessage getInventoryVoluationReport(string cur_code, string cat, string subcat, string dept)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_inventory_voluation_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@cat",cat),
                            new SqlParameter("@subcat",subcat),
                            new SqlParameter("@dept",dept)          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.Category = dr["Category"].ToString();
                            sequence.SubCategory = dr["SubCategory"].ToString();
                            sequence.im_item_code = dr["im_item_code"].ToString();
                            sequence.im_desc = dr["im_desc"].ToString();
                            sequence.current_qty = dr["current_qty"].ToString();
                            sequence.current_wac_cost = dr["current_wac_cost"].ToString();
                            sequence.current_value = dr["current_value"].ToString();
                            sequence.current_sell_price = dr["current_sell_price"].ToString();
                            sequence.sale_value = dr["sale_value"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getInventoryVoluationReportCat")]
        public HttpResponseMessage getInventoryVoluationReportCat()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_inventory_voluation_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getInventoryVoluationReportSubCat")]
        public HttpResponseMessage getgetInventoryVoluationReportSubCat(string cat)
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_inventory_voluation_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@cat", cat)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.pc_code = dr["pc_code"].ToString();
                            sequence.pc_parent_code = dr["pc_parent_code"].ToString();
                            sequence.pc_desc = dr["pc_desc"].ToString();
                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getInventoryVoluationReportDept")]
        public HttpResponseMessage getInventoryVoluationReportDept()
        {
            List<CommonINV> lstModules = new List<CommonINV>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_inv_inventory_voluation_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                                               
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV sequence = new CommonINV();
                            sequence.dep_code = dr["dep_code"].ToString();
                            sequence.dep_name = dr["dep_name"].ToString();
                            sequence.com_code = dr["com_code"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        //service material status report

        [Route("getAlldataservice")]
        public HttpResponseMessage getAlldataservice(string req_type, string req_status, string from_date, string to_date)
        {

            List<CommonINV> grade_list = new List<CommonINV>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_service_and_material_status]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "Z"),
                               new SqlParameter("@req_type", req_type),
                               new SqlParameter("@status", req_status),
                               new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),
                               new SqlParameter("@to_date", db.DBYYYYMMDDformat(to_date))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV simsobj = new CommonINV();
                            simsobj.doc_prov_no = dr["doc_prov_no"].ToString();
                            simsobj.item_name = dr["item_name"].ToString();
                            simsobj.doc_prov_date = db.UIDDMMYYYYformat(dr["doc_prov_date"].ToString());
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.comn_requestors_name = dr["comn_requestors_name"].ToString();
                            simsobj.doc_approval_status = dr["doc_approval_status"].ToString();
                            simsobj.id_cur_qty = dr["id_cur_qty"].ToString();
                            simsobj.request_type = dr["reuest_type"].ToString();
                            simsobj.status_desc = dr["Status_desc"].ToString();
                            simsobj.dd_qty = dr["dd_qty"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getrequesttypeapi")]
        public HttpResponseMessage getrequesttypeapi()
        {

            List<CommonINV> grade_list = new List<CommonINV>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_service_and_material_status]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T"),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV simsobj = new CommonINV();
                            simsobj.req_type = dr["req_type"].ToString();
                            simsobj.req_desc = dr["req_desc"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getrequeststatusapi")]
        public HttpResponseMessage getrequeststatusapi()
        {

            List<CommonINV> grade_list = new List<CommonINV>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_service_and_material_status]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonINV simsobj = new CommonINV();
                            simsobj.req_status = dr["req_status"].ToString();
                            simsobj.req_desc = dr["req_desc"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }





    }
}