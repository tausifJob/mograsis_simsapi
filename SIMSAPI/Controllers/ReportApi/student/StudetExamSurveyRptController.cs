﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;
using log4net;
using SIMSAPI.Controllers.ReportApi.student;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/StudentServeyQue")]
    public class StudentExamServey : ApiController
    {
       

        [Route("postStudentServeyQuestions")]
        public HttpResponseMessage postStudentServeyQuestions(SLRQ01 obj)
        {
            List<SLRQ01> lstModules = new List<SLRQ01>();

            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[student_exam_servey_rpt]",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@q_code",obj.q_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(obj.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(obj.to_date)),
                           
                         });

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ds);

            }

            return Request.CreateResponse(HttpStatusCode.OK, ds);

        }






        [Route("getQuestionsCode")]
        public HttpResponseMessage getQuestionsCode()
        {
            List<SLRQ01> lstModules = new List<SLRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[student_exam_servey_rpt]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T")                            
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLRQ01 sequence = new SLRQ01();
                            sequence.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            sequence.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
                            lstModules.Add(sequence);

                            	
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

       
    }
}


