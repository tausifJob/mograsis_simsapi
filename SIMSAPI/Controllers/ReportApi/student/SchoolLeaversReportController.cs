﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS.simsClass;

using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;
using log4net;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/SchoolLeaver")]
        public class SchoolLeaversReportController : ApiController
        {
            [Route("getSchoolLeaversdetails")]
            public HttpResponseMessage getSchoolLeaversdetails(string cur_code, string acad_year, string grade_code, string section_code,string from_date,string to_date)
            {
                List<SLRQ01> lstModules = new List<SLRQ01>();
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_SchoolLeaversReport",
                            new List<SqlParameter>()
                             {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))

                             });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                            SLRQ01 sequence = new SLRQ01();
                                sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                                sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                                sequence.sims_student_academic_year = dr["sims_student_academic_year"].ToString();
                                sequence.sims_parent_login_code = dr["sims_parent_login_code"].ToString();
                                sequence.student_name = dr["student_name"].ToString();
                                sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                sequence.leaving_date = dr["leaving_date"].ToString();
                                sequence.oversease = dr["oversease"].ToString();
                                sequence.school = dr["school"].ToString();
                                sequence.sims_student_remark = dr["sims_student_remark"].ToString();
                                sequence.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                                lstModules.Add(sequence);
                            }
                        }
                    }
                }
                catch (Exception x) { }
                return Request.CreateResponse(HttpStatusCode.OK, lstModules);
            }

        [Route("getdashboarddetails")]
        public HttpResponseMessage getdashboarddetails(string cur_code, string acad_year)
        {
            List<SLRQ01> lstModules = new List<SLRQ01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_SchoolLeaversReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "R"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SLRQ01 sequence = new SLRQ01();
                            sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.sims_student_academic_year = dr["sims_student_academic_year"].ToString();
                            sequence.student_left = dr["student_left"].ToString();
                            sequence.appearing = dr["appearing"].ToString();                            
                            lstModules.Add(sequence);                            
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
       //     Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_SchoolLeaversReport",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
              //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_SchoolLeaversReport",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            sequence.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            try
                            {
                                sequence.sims_academic_year_start_date = dr["sims_academic_year_start_date"].ToString();
                                sequence.sims_academic_year_end_date = dr["sims_academic_year_end_date"].ToString();
                            }
                            catch (Exception x)
                            {


                            }
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getQuestionsCode")]
        public HttpResponseMessage getQuestionsCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            //     Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[student_exam_servey_rpt]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_questionbank_code = dr["sims_questionbank_code"].ToString();
                            simsobj.sims_questionbank_subject = dr["sims_questionbank_subject"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }



        //[Route("postStudentServeyQuestions")]
        //public HttpResponseMessage postStudentServeyQuestions(Sims028 obj)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
        //    //     Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

        //    List<Sims028> grade_list = new List<Sims028>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[student_exam_servey_rpt]",
        //                new List<SqlParameter>()
        //                 {
        //                        new SqlParameter("@opr","S"),
        //                    new SqlParameter("@q_code",obj.q_code),
        //                    new SqlParameter("@from_date",db.DBYYYYMMDDformat(obj.from_date)),
        //                    new SqlParameter("@to_date",db.DBYYYYMMDDformat(obj.to_date)),
                           
                                
        //                 });
                    
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        //    }
        //    catch (Exception e)
        //    {
        //        //  Log.Error(e);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
        //    }
        //}



        [Route("postStudentServeyQuestions")]
        public HttpResponseMessage postStudentServeyQuestions(Sims028 obj)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[sims].[student_exam_servey_rpt]",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","S"),
                            new SqlParameter("@q_code",obj.q_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(obj.from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(obj.to_date)),
                           
                         });

                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ds);

            }

            return Request.CreateResponse(HttpStatusCode.OK, ds);

        }














    }
    }


