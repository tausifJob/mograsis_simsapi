﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.STUDENT;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi.student
{
      [RoutePrefix("api/StudentListBy")]
    public class StudentListByESISNoReportController : ApiController
    {
          [Route("getlistbyesisno")]

          public HttpResponseMessage getlistbyesisno(string cur_code, string acad_year, string grade_code, string section_code, string assign_status)
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_list_by_ESIS_no",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),                                                      
                            new SqlParameter("@assign_status",assign_status)                            
                          
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                              sequence.student_name = dr["student_name"].ToString();
                              sequence.grade_name = dr["grade_name"].ToString();
                              sequence.section_name = dr["section_name"].ToString();
                              sequence.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                          
                              // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }


          //[Route("getsort")]
          //public HttpResponseMessage getsort()
          //{
          //    List<CommonStudent> lstModules = new List<CommonStudent>();
          //    try
          //    {
          //        using (DBConnection db = new DBConnection())
          //        {
          //            db.Open();
          //            SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_list_by_ESIS_no",
          //                  new List<SqlParameter>() 
          //               { 
          //                  new SqlParameter("@opr", "J"),
                            
          //               }
          //                   );
          //            if (dr.HasRows)
          //            {
          //                while (dr.Read())
          //                {
          //                    CommonStudent sequence = new CommonStudent();
          //                    sequence.sims_appl_parameter= dr["sims_appl_parameter"].ToString();
          //                    sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
          //                    lstModules.Add(sequence);
          //                }
          //            }
          //        }
          //    }
          //    catch (Exception x) { }
          //    return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          //}
          [Route("getview")]
          public HttpResponseMessage getview()
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_list_by_ESIS_no",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "L"),
                            
                         }
                             );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                              sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }
    }
}