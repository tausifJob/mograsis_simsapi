﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.STUDENT;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi
{
    [RoutePrefix("api/promotestudlist")]
    public class promoted_stud_listcontroller : ApiController
    {
        [Route("getpromotelist")]
        public HttpResponseMessage getpromotelist(string view_mode, string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stu_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_view_mode",view_mode),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_academic_year",acad_year),
                            new SqlParameter("@sims_grade_code",grade_code),
                            new SqlParameter("@sims_section_code",section_code)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.current_year_grade_code = dr["current_year_grade_name_en"].ToString();
                            sequence.current_year_section_code = dr["current_year_section_code"].ToString();
                            sequence.current_year_section_name_en = dr["current_year_section_name_en"].ToString();
                            sequence.next_year_grade_code = dr["next_year_grade_code"].ToString();
                            sequence.next_year_grade_name_en = dr["next_year_grade_name_en"].ToString();
                            sequence.next_year_section_name_en = dr["next_year_section_name_en"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.sims_parent_mobile = dr["sims_parent_mobile"].ToString();
                            sequence.parent_name = dr["parent_name"].ToString();
                            sequence.parent_mail = dr["parent_mail"].ToString();
                            lstModules.Add(sequence);

                            
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getcanceladmissionsummary")]
        public HttpResponseMessage getcanceladmissionsummary(string cur_code, string acad_year, string grade_code, string section_code, string report_view)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_cancel_admission_summary]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@report_view",report_view)
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_allocation_status = dr["sims_allocation_status"].ToString();
                            sequence.sims_roll_number = dr["sims_roll_number"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                            sequence.sims_certificate_number = dr["sims_certificate_number"].ToString();
                            sequence.sims_certificate_date_of_leaving = dr["sims_certificate_date_of_leaving"].ToString();
                            sequence.sims_certificate_reason_of_leaving = dr["sims_certificate_reason_of_leaving"].ToString();
                            lstModules.Add(sequence);


                            									

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getcanceladmissionsummaryView")]
        public HttpResponseMessage getcanceladmissionsummaryView()
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_cancel_admission_summary]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W")
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.comn_appl_form_field = dr["comn_appl_form_field"].ToString();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                           
                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getstudentlistbyage")]
        public HttpResponseMessage getstudentlistbyage(string cur_code, string acad_year, string grade_code, string section_code, string max_age, string min_age )
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            if (max_age == "undefined" || max_age == "")
            {
                max_age = null;
            }

            if (min_age == "undefined" || min_age == "")
            {
                min_age = null;
            }
                

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_agewise_detail]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@max_age",max_age),
                            new SqlParameter("@min_age",min_age)
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.gender = dr["gender"].ToString();
                            sequence.sims_student_age = dr["sims_student_age"].ToString();
                            sequence.sims_student_dob = dr["sims_student_dob"].ToString();
                            sequence.year = dr["year"].ToString();
                           
                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getstudentlistbygradesection")]
        public HttpResponseMessage getstudentlistbygradesection(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
           

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_admission_list_by_grade_section]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                           
                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getstudentlistbyethinicity")]
        public HttpResponseMessage getstudentlistbyethinicity(string cur_code, string acad_year, string grade_code, string section_code, string thnicity_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_list_by_ethinicity]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@thnicity_code",thnicity_code),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();

                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getethinicity")]
        public HttpResponseMessage getethinicity()
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_list_by_ethinicity]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W")
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_ethnicity_code = dr["sims_ethnicity_code"].ToString();
                            sequence.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();
                            

                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getstudentimmigrationdetails")]
        public HttpResponseMessage getstudentimmigrationdetails(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_student_imigration_detail]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence. pass_exp = dr["pass_exp"].ToString();
                            sequence.nash_exp = dr["nash_exp"].ToString();
                            sequence.visa_exp = dr["visa_exp"].ToString();
                            sequence.sims_student_passport_issue_date1 = dr["sims_student_passport_issue_date1"].ToString();
                            sequence.sims_student_national_id_issue_date1 = dr["sims_student_national_id_issue_date1"].ToString();
                            sequence.sims_student_visa_issue_date1 = dr["sims_student_visa_issue_date1"].ToString();
                            sequence.sims_student_passport_number = dr["sims_student_passport_number"].ToString();
                            sequence.sims_student_visa_number = dr["sims_student_visa_number"].ToString();
                            sequence.sims_student_national_id = dr["sims_student_national_id"].ToString();


                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getStudentListByTeacher")]
        public HttpResponseMessage getStudentListByTeacher(string cur_code, string acad_year)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_list_by_teacher]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year)
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_employee_code = dr["sims_employee_code"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.subject_name = dr["subject_name"].ToString();
                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getStudentContactReport")]
        public HttpResponseMessage getStudentContactReport(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_contact_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                             new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.gender = dr["gender"].ToString();
                            sequence.sims_house_name = dr["sims_house_name"].ToString();
                            sequence.second_lan = dr["second_lan"].ToString();
                            sequence.third_lan = dr["third_lan"].ToString();
                            sequence.bus_in = dr["bus_in"].ToString();
                            sequence.bus_out = dr["bus_out"].ToString();
                            lstModules.Add(sequence);
                            
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getStudentMappingByTeacher")]//cur_code,acad_year,board_code ,exam_code,teacher_code 
        public HttpResponseMessage getStudentMappingByTeacher(string cur_code, string acad_year, string board_code, string exam_code ,string teacher_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_exam_number_mapping_by_teacher]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                             new SqlParameter("@board_code",board_code),
                            new SqlParameter("@exam_code",exam_code),
                             new SqlParameter("@teacher_code",teacher_code)
                            
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_class_teacher_code = dr["sims_class_teacher_code"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            sequence.sims_board_exam_enroll_number = dr["sims_board_exam_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                             lstModules.Add(sequence);


                          }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getBoardCode")]
        public HttpResponseMessage getBoardCode()
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_exam_number_mapping_by_teacher]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           

                            
                            }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_board_code = dr["sims_board_code"].ToString();
                            sequence.sims_board_short_name = dr["sims_board_short_name"].ToString();
                       
                            lstModules.Add(sequence);
                            	


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getExamCode")]
        public HttpResponseMessage getExamCode(string board_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_exam_number_mapping_by_teacher]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@board_code",board_code)
                           

                            
                            }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_board_exam_code = dr["sims_board_exam_code"].ToString();
                            sequence.sims_board_exam_short_name = dr["sims_board_exam_short_name"].ToString();
                            


                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getTeacherCode")]
        public HttpResponseMessage getTeacherCode()
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_stud_exam_number_mapping_by_teacher]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                           

                            
                            }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_class_teacher_code = dr["sims_class_teacher_code"].ToString();
                            sequence.sims_employee_code = dr["sims_employee_code"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            

	



                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getReRegisteredReport")]
        public HttpResponseMessage getReRegisteredReport(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Stud_Re_registerd_student_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                           
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.next_grade = dr["next_grade"].ToString();
                            sequence.next_section = dr["next_section"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.student_status = dr["student_status"].ToString();
                            
                            lstModules.Add(sequence);
                            

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getStudentListByGradeNew")]
        public HttpResponseMessage getStudentListByGradeNew(string cur_code, string acad_year, string grade_code, string section_code, string status)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Stud_Student_List_By_Grade_new]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@status",status)
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sibling_detil = dr["sibling_detil"].ToString();
                            sequence.sims_student_dob = dr["sims_student_dob"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.BusNo = dr["BusNo"].ToString();
                            sequence.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                             lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




        [Route("getGradeByNewStatus")]
        public HttpResponseMessage getGradeByNewStatus()
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Stud_Student_List_By_Grade_new]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                           

                            lstModules.Add(sequence);




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getClassTeacherListRpt")]
        public HttpResponseMessage getClassTeacherListRpt(string cur_code, string acad_year, string subject_status)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Class_Teacher_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@subject_status",subject_status)
                          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_employee_code = dr["sims_employee_code"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                           
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getClassTeacherListRptSubjectStatus")]
        public HttpResponseMessage getClassTeacherListRptSubjectStatus()
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Pay_Class_Teacher_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                            
                          
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.code = dr["code"].ToString();
                            sequence.descr = dr["descr"].ToString();
                            lstModules.Add(sequence);
                            	
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAgendaReport")]
        public HttpResponseMessage getAgendaReport(string cur_code, string acad_year, string grade_code, string section_code, string agenda_date, string end_date, string subject_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_sims_agenda_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@agenda_date",db.DBYYYYMMDDformat(agenda_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@subject_code",subject_code),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_agenda_name = dr["sims_agenda_name"].ToString();
                            sequence.teacher_name = dr["teacher_name"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAgendaReportSubject")]
        public HttpResponseMessage getAgendaReportSubject(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_sims_agenda_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                          
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                           sequence.sims_subject_code = dr["sims_subject_code"].ToString();
                            sequence.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                                 lstModules.Add(sequence);







                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getstudendocumentstatus")]
        public HttpResponseMessage getstudendocumentstatus(string cur_code, string acad_year, string grade_code, string verfiy, string status, string doc_code)
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_document_status]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@verfiy",verfiy),
                             new SqlParameter("@status",status),
                             new SqlParameter("@doc_code",doc_code)
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_admission_doc_admission_number = dr["sims_admission_doc_admission_number"].ToString();
                            sequence.Student_Name = dr["Student_Name"].ToString();
                            sequence.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            sequence.doc_status = dr["doc_status"].ToString();
                             lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getstudendocumentstatusVerify")]
        public HttpResponseMessage getstudendocumentstatusVerify()
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_document_status]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.code = dr["code"].ToString();
                            sequence.disc = dr["disc"].ToString();
                            
                            lstModules.Add(sequence);







                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getstudendocumentstatusStatus")]
        public HttpResponseMessage getstudendocumentstatusStatus()
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_document_status]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.code = dr["code"].ToString();
                            sequence.disc = dr["disc"].ToString();

                            lstModules.Add(sequence);







                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getstudendocumentstatusDoc")]
        public HttpResponseMessage getstudendocumentstatusDoc()
        {
            List<Common_Student> lstModules = new List<Common_Student>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_student_document_status]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Common_Student sequence = new Common_Student();
                            sequence.sims_criteria_code = dr["sims_criteria_code"].ToString();
                            sequence.sims_criteria_name_en = dr["sims_criteria_name_en"].ToString();
                            



                            lstModules.Add(sequence);







                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




    }


}


                           




	
	
	




                           


