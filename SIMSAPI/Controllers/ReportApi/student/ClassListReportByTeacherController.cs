﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.STUDENT;
using System.Data;
namespace SIMSAPI.Controllers.ReportApi.student
{
    [RoutePrefix("api/classlist")]
    public class ClassListReportByTeacherController : ApiController
    {
        [Route("getlistbyteacher")]
        public HttpResponseMessage getlistbyteacher(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_class_list_by_teacher",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_student_nationality_code = dr["sims_student_nationality_code"].ToString();
                            sequence.sims_student_dob = dr["sims_student_dob"].ToString();
                            sequence.nationality_name = dr["nationality_name"].ToString();
                            sequence.sims_student_religion_code = dr["sims_student_religion_code"].ToString();
                            sequence.std_religion = dr["std_religion"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            sequence.teacher_name = dr["teacher_name"].ToString();
                            sequence.sibling_detil = dr["sibling_detil"].ToString();
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getnew_cancelled_student_list")]
        public HttpResponseMessage getnew_cancelled_student_list(string cur_code, string acad_year, string grade_code, string section_code, string from_date, string to_date, string report_param)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
           {
              using (DBConnection db = new DBConnection())
                {
                  db.Open();
                  SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_new_admission_cancellation_std_list",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@report_param", report_param)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_student_date = dr["sims_student_date"].ToString();
                            sequence.sims_student_remark = dr["sims_student_remark"].ToString();
                            sequence.privious_school = dr["privious_school"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.sims_parent_login_code = dr["sims_parent_login_code"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
           }
           catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getreport_parameter")]
        public HttpResponseMessage getreport_parameter()
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_new_admission_cancellation_std_list",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getlistbygrade")]
        public HttpResponseMessage getlistbygrade(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_by_grade",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)  
                                                 
                          
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.gender = dr["gender"].ToString();
                            sequence.sims_student_family_name_en = dr["student_family_name_en"].ToString();
                            sequence.nationality_name = dr["nationality_name"].ToString();
                            sequence.std_religion = dr["std_religion"].ToString();
                            sequence.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();
                            sequence.sims_house_name = dr["sims_house_name"].ToString();

                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();

                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getlistbysubject")]
        public HttpResponseMessage getlistbysubject(string acad_year, string grade_code, string section_code)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_subject_teacher_list ",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)  
                                                 
                          
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_bell_academic_year = dr["sims_bell_academic_year"].ToString();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.bell_name = dr["bell_name"].ToString();
                            sequence.sims_bell_grade_code = dr["sims_bell_grade_code"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_bell_section_code = dr["sims_bell_section_code"].ToString();
                            sequence.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            sequence.teacher_name = dr["teacher_name"].ToString();

                            sequence.employee_code = dr["employee_code"].ToString();

                            sequence.sims_bell_subject_code = dr["sims_bell_subject_code"].ToString();
                            sequence.subject_name = dr["subject_name"].ToString();



                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getonlinelist")]
        public HttpResponseMessage getonlinelist(string cur_code, string acad_year, string grade_code,  string admn_status, string st_date, string end_date )
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_online_admission_std_list",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@admn_status",admn_status),
                             new SqlParameter("@st_date",db.DBYYYYMMDDformat(st_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date))
                         

                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.sims_admission_number = dr["sims_admission_number"].ToString();
                            sequence.sims_admission_date = dr["sims_admission_date1"].ToString();
                            sequence.student_name_en = dr["student_name_en"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.gender = dr["gender"].ToString();
                            sequence.fee_paid_status = dr["fee_paid_status"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            sequence.father_mobile = dr["father_mobile"].ToString();
                            sequence.father_email = dr["father_email"].ToString();
                            sequence.sims_admission_dob = dr["sims_admission_dob1"].ToString();
                            sequence.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                            sequence.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            sequence.sibling_name = dr["sibling_name"].ToString();

                            sequence.sibling_detil = dr["sibling_grade_name"].ToString();
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
            [Route("getstatus_parameter")]
        public HttpResponseMessage getstatus_parameter()
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_online_admission_std_list",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getstudentlist")]
        public HttpResponseMessage getstudentlist(string cur_code, string acad_year, string grade, string section, string status)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_details",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade",grade),
                            new SqlParameter("@section",section),                            
                            new SqlParameter("@status",status),                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_student_gender = dr["sims_student_gender"].ToString();
                            sequence.sims_student_dob = dr["sims_student_dob"].ToString();
                            sequence.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            sequence.sims_student_ea_status = dr["sims_student_ea_status"].ToString();
                            sequence.religion = dr["religion"].ToString();
                            sequence.sims_student_commence_date = dr["sims_student_commence_date"].ToString();
                            sequence.parent_email = dr["parent_email"].ToString();
                            sequence.parent_mobile = dr["parent_mobile"].ToString();
                            sequence.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                           
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getstudent_status")]
        public HttpResponseMessage getstudent_status()
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_details",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




          [Route("getcancelledlist")]
        public HttpResponseMessage getcancelledlist(string cur_code, string from_date, string to_date)
        {
            List<CommonStudent> lstModules = new List<CommonStudent>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_cancelled_student_report",
                        new List<SqlParameter>() 
                         {  new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))                  
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonStudent sequence = new CommonStudent();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                            sequence.sims_student_academic_status = dr["sims_student_academic_status"].ToString();
                            sequence.sims_student_academic_year = dr["sims_student_academic_year"].ToString();
                            sequence.sims_student_remark = dr["sims_student_remark"].ToString();
                            sequence.sims_student_status_updation_date = dr["sims_student_status_updation_date"].ToString();
                            sequence.sims_student_financial_status = dr["sims_student_financial_status"].ToString();
                            sequence.sims_student_status_updation_user_code = dr["sims_student_status_updation_user_code"].ToString();
                            sequence.cancel_date = dr["cancel_date"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class1 = dr["class"].ToString();
                            sequence.status1 = dr["status1"].ToString();
                            sequence.name = dr["name"].ToString();
                          
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


          [Route("getadmissionstatus")]
          public HttpResponseMessage getadmissionstatus(string cur_code, string from, string to)
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_cancelled_admission_status",
                          new List<SqlParameter>() 
                         {  new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@from",db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@to",db.DBYYYYMMDDformat(to))                  
                           
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                              sequence.sims_sr_no = dr["sims_sr_no"].ToString();
                              sequence.sims_finn_clr_status = dr["sims_finn_clr_status"].ToString();
                              sequence.sims_lib_clr_status = dr["sims_lib_clr_status"].ToString();
                              sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                              sequence.sims_clr_emp_status = dr["sims_clr_emp_status"].ToString();
                              sequence.student_Name = dr["Student_Name"].ToString();
                              sequence.employee_name = dr["employee_name"].ToString();
                             
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }
          [Route("getparents")]
          public HttpResponseMessage getparents()
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_parents_not_assigned_to_students",
                          new List<SqlParameter>() 
                         {  new SqlParameter("@opr", "S"),
                            
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.sims_parent_number = dr["sims_parent_number"].ToString();
                              sequence.father_name = dr["father_name"].ToString();
                              sequence.mother_name = dr["mother_name"].ToString();
                              sequence.guardian_name = dr["guardian_name"].ToString();
                             
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }

          [Route("getsibling")]
          public HttpResponseMessage getsibling(string cur_code, string acad_year, string show_report)
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_sibling_report",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@show_report",show_report),
                           
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.parent_name = dr["parent_name"].ToString();
                              sequence.staff_type = dr["staff_type"].ToString();
                              sequence.sims_student_count = dr["sims_student_count"].ToString();
                              sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                              sequence.student_name = dr["student_name"].ToString();
                              sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                              sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }


              [Route("getshow")]
          public HttpResponseMessage getshow()
          {
              List<CommonStudent> lstModules = new List<CommonStudent>();
              try
              {
                  using (DBConnection db = new DBConnection())
                  {
                      db.Open();
                      SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_sibling_report",
                          new List<SqlParameter>() 
                         {  new SqlParameter("@opr", "W"),
                            
                         }
                           );
                      if (dr.HasRows)
                      {
                          while (dr.Read())
                          {
                              CommonStudent sequence = new CommonStudent();
                              sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                              sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             
                             
                              lstModules.Add(sequence);
                          }
                      }
                  }
              }
              catch (Exception x) { }
              return Request.CreateResponse(HttpStatusCode.OK, lstModules);
          }


              [Route("getHouse")]
              public HttpResponseMessage getHouse(string cur_code, string acad_year, string grade_code, string section_code, string age_cal_date, string dob_less_than, string dob_greater)
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_house_list_report",
                              new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@age_cal_date",db.DBYYYYMMDDformat(age_cal_date)),
                            new SqlParameter("@dob_less_than",db.DBYYYYMMDDformat(dob_less_than)),
                            new SqlParameter("@dob_greater",db.DBYYYYMMDDformat(dob_greater)) 
                           
                           

                           
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.student_name = dr["student_name"].ToString();
                                  sequence.age_calculate = dr["age_calculate"].ToString();
                                  sequence.sims_student_dob = dr["sims_student_dob"].ToString();
                                  sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                  sequence.house_name = dr["house_name"].ToString();
                                  sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                  sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();

                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }

              [Route("gethomeroom")]
              public HttpResponseMessage gethomeroom(string cur_code, string acad_year, string grade_code, string section_code, string homeroom, string batch_code)
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_by_homeroom",
                              new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@homeroom",homeroom),
                            new SqlParameter("@batch_code",batch_code)
                           
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.sims_batch_enroll_number = dr["sims_batch_enroll_number"].ToString();
                                  sequence.student_name = dr["student_name"].ToString();
                                  sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                  sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                  sequence.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                                  sequence.sims_batch_name = dr["sims_batch_name"].ToString();

                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }


              [Route("getroom")]
              public HttpResponseMessage getroom(string cur_code, string acad_year)
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  { 
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_by_homeroom",
                              new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "K"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year)
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                                  sequence.sims_homeroom_name = dr["sims_homeroom_name"].ToString();


                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }
              [Route("getbatch")]
              public HttpResponseMessage getbatch(string cur_code, string acad_year, string homeroom)
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_students_by_homeroom",
                              new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "J"),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@acad_year", acad_year),
                             new SqlParameter("@homeroom", homeroom)
                            
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.sims_batch_code = dr["sims_batch_code"].ToString();
                                  sequence.sims_batch_name = dr["sims_batch_name"].ToString();


                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }
              [Route("getprospects")]
              public HttpResponseMessage getprospects(string cur_code, string acad_year, string grade_code, string pros_status, string st_date, string end_date)
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_prospects_details",
                              new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@pros_status",pros_status),
                             new SqlParameter("@st_date",db.DBYYYYMMDDformat(st_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)) 
                           
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.sims_admission_number = dr["sims_admission_number"].ToString();
                                  sequence.sims_pros_date_created = dr["sims_pros_date_created"].ToString();
                                  sequence.student_name_en = dr["student_name_en"].ToString();
                                  sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                  sequence.father_name = dr["father_name"].ToString();
                                  sequence.mother_name = dr["mother_name"].ToString();
                                  sequence.sims_pros_primary_contact_pref = dr["sims_pros_primary_contact_pref"].ToString();
                                  sequence.mother_mobile = dr["mother_mobile"].ToString();
                                  sequence.father_mobile = dr["father_mobile"].ToString();
                                  sequence.guardian_mobile = dr["guardian_mobile"].ToString();
                                  sequence.guardian_email = dr["guardian_email"].ToString();
                                  sequence.mother_email = dr["mother_email"].ToString();
                                  sequence.sims_pros_dob = dr["sims_pros_dob"].ToString();
                                  sequence.father_email= dr["father_email"].ToString();
                                  sequence.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                                  sequence.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                                
                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }


              [Route("getastatus")]
              public HttpResponseMessage getastatus()
              {
                  List<CommonStudent> lstModules = new List<CommonStudent>();
                  try
                  {
                      using (DBConnection db = new DBConnection())
                      {
                          db.Open();
                          SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_prospects_details",
                              new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "T"),
                  
                         }
                               );
                          if (dr.HasRows)
                          {
                              while (dr.Read())
                              {
                                  CommonStudent sequence = new CommonStudent();
                                  sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                                  sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();


                                  lstModules.Add(sequence);
                              }
                          }
                      }
                  }
                  catch (Exception x) { }
                  return Request.CreateResponse(HttpStatusCode.OK, lstModules);
              }
    }
}


