﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FINNANCE;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.finance
{
        [RoutePrefix("api/ChequeFinanceDetails")]
    public class ChequeDetailReportController: ApiController
    {
        [Route("getfinance")]
            public HttpResponseMessage getfinance(string comp_code, string doc_no)
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_cheque_detail_report",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                             new SqlParameter("@doc_no",doc_no)
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.pb_bank_name = dr["pb_bank_name"].ToString();
                            sequence.pc_cheque_no = dr["pc_cheque_no"].ToString();
                            sequence.cheque_date = dr["Cheque_date"].ToString();
                            sequence.enroll_no = dr["enroll_no"].ToString();
                            sequence.doc_date = dr["doc_date"].ToString();
                            sequence.doc_ref_code = dr["doc_ref_code"].ToString();
                            sequence.amount = dr["Amount"].ToString();
                           
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getcompany")]
        public HttpResponseMessage getcompany()
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_cheque_detail_report",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getmaster")]
        public HttpResponseMessage getmaster(string cat, string start, string to)
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_assest_master",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cat",cat),
                              new SqlParameter("@start",db.DBYYYYMMDDformat(start)),
                             new SqlParameter("@to",db.DBYYYYMMDDformat(to))
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())       
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.gam_item_no = dr["gam_item_no"].ToString();
                            sequence.gam_desc = dr["gam_desc_1"].ToString();
                            sequence.gam_des = dr["gam_desc_2"].ToString();
                            sequence.gal_type_desc = dr["gal_type_desc"].ToString();
                            sequence.sims_location_desc = dr["sims_location_desc"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.sup_name = dr["sup_name"].ToString();
                            sequence.gam_invoice_no = dr["gam_invoice_no"].ToString();
                            sequence.gam_receipt_date = dr["gam_receipt_date"].ToString();
                            sequence.gam_entry_date = dr["gam_entry_date"].ToString();
                            sequence.gam_quantity = dr["gam_quantity"].ToString();
                            sequence.gam_order_no = dr["gam_order_no"].ToString();
                            sequence.gam_invoice_amount = dr["gam_invoice_amount"].ToString();
                            sequence.gam_mth_deprn = dr["gam_mth_deprn"].ToString();
                            sequence.gam_cum_deprn = dr["gam_cum_deprn"].ToString();
                            sequence.gam_book_value = dr["gam_book_value"].ToString();
                            sequence.gam_deprn_percent = dr["gam_deprn_percent"].ToString();
                            sequence.gam_status = dr["gam_status"].ToString();
                     
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getcate")]
        public HttpResponseMessage getcate()
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_assest_master",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.gal_asst_type = dr["gal_asst_type"].ToString();
                            sequence.gal_type_desc = dr["gal_type_desc"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
            [Route("getglsum")]
        public HttpResponseMessage getglsum(string comp_code, string dept_code, string acct_code, string from_date, string to_date, string start_date)
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_gl_summary",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                             new SqlParameter("@dept_code",dept_code),
                              new SqlParameter("@acct_code",acct_code),
                              new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                             new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                             new SqlParameter("@start_date",db.DBYYYYMMDDformat(start_date))
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())  
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.doc_no = dr["doc_no"].ToString();
                            sequence.gltr_pstng_date = dr["gltr_pstng_date"].ToString();
                            sequence.gltr_doc_narr = dr["gltr_doc_narr"].ToString();
                            sequence.debit_amt = dr["debit_amt"].ToString();
                            sequence.credit_amt = dr["credit_amt"].ToString();
                           
                     
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
            [Route("getcom")]
            public HttpResponseMessage getcom()
            {
                List<ComnFinance> lstModules = new List<ComnFinance>();
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_gl_summary",
                              new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                            
                         }
                               );
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ComnFinance sequence = new ComnFinance();
                                sequence.comp_code = dr["comp_code"].ToString();
                                sequence.comp_name = dr["comp_name"].ToString();
                                lstModules.Add(sequence);
                            }
                        }
                    }
                }
                catch (Exception x) { }
                return Request.CreateResponse(HttpStatusCode.OK, lstModules);
            }

        [Route("getdepe")]
            public HttpResponseMessage getdepe(string comp_code)
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_gl_summary",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@comp_code", comp_code),
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getacc")]
        public HttpResponseMessage getacc(string comp_code, string dept_code, string from_date)
        {
            List<ComnFinance> lstModules = new List<ComnFinance>();
            if (from_date == "undefined"  || from_date == "" )
            {
                from_date = null;
                
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_gl_summary",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                             new SqlParameter("@comp_code", comp_code),
                              new SqlParameter("@dept_code", dept_code),
                               new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date))
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ComnFinance sequence = new ComnFinance();
                            sequence.glma_acct_code = dr["glma_acct_code"].ToString();
                            sequence.glma_acct_name = dr["glma_acct_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        }
  
       
        }
