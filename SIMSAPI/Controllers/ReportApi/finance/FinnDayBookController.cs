﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FinnDay;
using System.Data;
namespace SIMSAPI.Controllers.ReportApi.finance
{
    [RoutePrefix("api/getFinnDayBook")]
    public class FinnDayBookController : ApiController
    {
        [Route("getFinnDayBookReport")]
        public HttpResponseMessage getFinnDayBookReport(string comp_code, string fin_year, string start_date, string end_date,string doc_code )
        {
            
            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_finn_day_book]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@fin_year",fin_year),
                            new SqlParameter("@start_date",db.DBYYYYMMDDformat(start_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@doc_code",doc_code)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            sequence.gltr_pstng_date = dr["gltr_pstng_date"].ToString();
                            sequence.gltr_doc_code = dr["gltr_doc_code"].ToString();
                            sequence.gltr_acct_code = dr["gltr_acct_code"].ToString();
                            sequence.gltr_acct_name = dr["gltr_acct_name"].ToString();
                            sequence.gltr_dept_no = dr["gltr_dept_no"].ToString();
                            sequence.gltr_doc_narr = dr["gltr_doc_narr"].ToString();
                            //sequence.coce_cost_centre_code = dr["coce_cost_centre_code"].ToString();
                            //sequence.coce_cost_centre_name = dr["coce_cost_centre_name"].ToString();
                            sequence.centre = dr["centre"].ToString();
                            
                            sequence.gltr_tran_amt_dr = dr["gltr_tran_amt_dr"].ToString();
                            sequence.gltr_tran_amt_cr = dr["gltr_tran_amt_cr"].ToString();
                           

                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

      

        [Route("getFinnDayBookReportFinnYear")]
        public HttpResponseMessage getFinnDayBookReportFinnYear(string comp_code)
        {
            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_finn_day_book]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp_code",comp_code),
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            //sequence.year_description = dr["year_description"].ToString();
                            sequence.glma_year = dr["glma_year"].ToString();
                           
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getFinnDayBookReportDocCode")]
        public HttpResponseMessage getFinnDayBookReportDocCode(string comp_code, string fin_year)
        {
            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_finn_day_book]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@fin_year",fin_year),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            sequence.gldc_doc_code = dr["gldc_doc_code"].ToString();
                            sequence.gldc_doc_name = dr["gldc_doc_name"].ToString();
                            
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getAssetLitralReport")]
        public HttpResponseMessage getAssetLitralReport(string comp_code, string fin_year, string cat)
        {
            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_finn_asset_litral_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@fin_year",fin_year),
                              new SqlParameter("@cat",cat),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            sequence.comp_short_name = dr["comp_short_name"].ToString();
                            sequence.rec_type_name = dr["rec_type_name"].ToString();
                            sequence.gal_type_desc=dr["gal_type_desc"].ToString();
                             sequence.gal_asset_acno = dr["gal_asset_acno"].ToString();
                            sequence.asset_acno_name = dr["asset_acno_name"].ToString();
                            sequence.gal_deprn_acno = dr["gal_deprn_acno"].ToString();
                                sequence.depr_acno_name = dr["depr_acno_name"].ToString();
                            sequence.gal_deprn_percent=dr["gal_deprn_percent"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getAssetLitralReportCat")]
        public HttpResponseMessage getAssetLitralReportCat()
        {
            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_finn_asset_litral_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            sequence.gal_asst_type = dr["gal_asst_type"].ToString();
                            sequence.gal_type_desc = dr["gal_type_desc"].ToString();
                           
	

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




        [Route("getBalanceSheetReport")]
        public HttpResponseMessage getBalanceSheetReport(string comp_code, string start_date, string end_date, string exlude_zero)
        {

            List<FinnDayBookCommon> lstModules = new List<FinnDayBookCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[GetPLnBSReport]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),                       
                            new SqlParameter("@start_date",db.DBYYYYMMDDformat(start_date)),
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@exlude_zero",exlude_zero)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FinnDayBookCommon sequence = new FinnDayBookCommon();
                            sequence.group_name= dr["group_name"].ToString();
                            sequence.sched_name = dr["sched_name"].ToString();
                            sequence.glac_name = dr["glac_name"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.glma_op_bal_amt = dr["glma_op_bal_amt"].ToString();
                            sequence.debit = dr["debit"].ToString();
                            sequence.credit = dr["credit"].ToString();
                             sequence.cl_balance = dr["cl_balance"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

      






    }
}