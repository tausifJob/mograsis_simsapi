﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using System.Collections;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FinnCommonReport;
using SIMSAPI.Models.FINNANCE;

namespace SIMSAPI.Controllers.ReportApi.finance
{
    [RoutePrefix("api/recievable_details")]
    public class ReceivableAgingQueryReportController : ApiController
    {
        [Route("getAllagingDetails")]
        public HttpResponseMessage getAllagingDetails(string cur_code, string acad_year, string grade_code, string section_code, string acade_status,string parent_flag)
        {
            List<RAQR01> code_list = new List<RAQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_finn_ReceivableAgingReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@sec_code", section_code),
                            new SqlParameter("@acade_status", acade_status),
                            new SqlParameter("@parent_flag",parent_flag),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RAQR01 finn = new RAQR01();
                            finn.sims_academic_year = dr["sims_academic_year"].ToString();
                            finn.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            finn.student_name = dr["student_name"].ToString();
                            finn.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            finn.no_of_families = dr["no_of_families"].ToString();
                            finn.parent_name = dr["parent_name"].ToString();
                            finn.balance0to1Year = float.Parse(dr["balance0to1Year"].ToString());
                            finn.balance1to2Year = float.Parse(dr["balance1to2Year"].ToString());
                            finn.balance2to3Year = float.Parse(dr["balance2to3Year"].ToString());
                            finn.graterThan3Year= float.Parse(dr["graterThan3Year"].ToString());
                            finn.pdcInHand = float.Parse(dr["pdcInHand"].ToString());
                            finn.pdcDishonored = float.Parse(dr["pdcDishonored"].ToString());
                            finn.expected_amt = float.Parse(dr["expected_amt"].ToString());
                            //finn.net_outstanding = dr["net_outstanding"].ToString();
                            finn.net_outstanding = dr["net_outstanding"].ToString();
                            finn.perc_net_outstanding = dr["perc_net_outstanding"].ToString();
                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

        [Route("getAllagingDetails_new")]
        public HttpResponseMessage getAllagingDetails_new(string cur_code, string acad_year, string acade_status, string parent_flag,string from_date)
        {
            List<RAQR01> code_list = new List<RAQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[finn_ReceivableAgingReport_rpt_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year",acad_year),                            
                            new SqlParameter("@acade_status", acade_status),
                            new SqlParameter("@parent_flag",parent_flag),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RAQR01 finn = new RAQR01();
                            finn.sims_academic_year = dr["sims_academic_year"].ToString();
                            finn.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            finn.student_name = dr["student_name"].ToString();
                            finn.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            finn.no_of_families = dr["no_of_families"].ToString();
                            finn.parent_name = dr["parent_name"].ToString();
                            finn.balance0to1Year = float.Parse(dr["balance0to1Year"].ToString());
                            finn.balance1to2Year = float.Parse(dr["balance1to2Year"].ToString());
                            finn.balance2to3Year = float.Parse(dr["balance2to3Year"].ToString());
                            finn.graterThan3Year = float.Parse(dr["graterThan3Year"].ToString());
                            finn.total_amount = float.Parse(dr["total_amount"].ToString());
                            finn.ob_paid_amount = float.Parse(dr["ob_paid_amount"].ToString());
                            
                            //finn.pdcInHand = float.Parse(dr["pdcInHand"].ToString());
                            //finn.pdcDishonored = float.Parse(dr["pdcDishonored"].ToString());
                            //finn.expected_amt = float.Parse(dr["expected_amt"].ToString());
                            ////finn.net_outstanding = dr["net_outstanding"].ToString();
                            //finn.net_outstanding = dr["net_outstanding"].ToString();
                            //finn.perc_net_outstanding = dr["perc_net_outstanding"].ToString();
                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

        [Route("getbreakdownDetails")]
        public HttpResponseMessage getbreakdownDetails(string cur_code, string acad_year, string grade_code, string section_code, string acade_status)
        {
            List<RAQR01> code_list = new List<RAQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_finn_ReceivableAgingReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@sec_code", section_code),
                           
                            new SqlParameter("@acade_status", acade_status)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RAQR01 finn = new RAQR01();
                            finn.no_of_families = dr["no_of_families"].ToString();
                            finn.expected_amt = float.Parse(dr["expected_amt"].ToString());
                            finn.net_outstanding = dr["net_outstanding"].ToString();
                            finn.greaterthannintyAMT = float.Parse(dr["greaterthannintyAMT"].ToString());
                            finn.seventyFIVE_nintyAMT = float.Parse(dr["seventyFIVE_nintyAMT"].ToString());
                            finn.fifty_seventyfourAMT = float.Parse(dr["fifty_seventyfourAMT"].ToString());
                            finn.lessthanfiftyAMT = float.Parse(dr["lessthanfiftyAMT"].ToString());
                            finn.greaterthannintyFAMITY = dr["greaterthannintyFAMITY"].ToString();
                            finn.seventyFIVE_nintyFAMITY = dr["seventyFIVE_nintyFAMITY"].ToString();
                            finn.fifty_seventyfourFamily = dr["fifty_seventyfourFamily"].ToString();
                            finn.lessthanfiftyFAMITY = dr["lessthanfiftyFAMITY"].ToString();

                            //finn.lessthanfiftyFAMITY = dr["lessthanfiftyFAMITY"].ToString();
                            //finn.greaterthannintyFAMITY = dr["greaterthannintyFAMITY"].ToString();
                            //finn.seventyFIVE_nintyFAMITY = dr["seventyFIVE_nintyFAMITY"].ToString();
                            //finn.fifty_seventyfourFamily = dr["fifty_seventyfourFamily"].ToString();                           
                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

        [Route("getsatusDetails")]
        public HttpResponseMessage getsatusDetails()
        {
            List<RAQR01> code_list = new List<RAQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_finn_ReceivableAgingReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                                                       

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RAQR01 finn = new RAQR01();
                            finn.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            finn.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }


        //According To New Calculation of Ageing

        [Route("getAllagingDetails_newCalc")]
        public HttpResponseMessage getAllagingDetails_newCalc(string cur_code, string acad_year, string acade_status, string parent_flag, string from_date)
        {
            List<RAQR01> code_list = new List<RAQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[finn_ReceivableAgingReport_rpt_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'D'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@acade_status", acade_status),
                            new SqlParameter("@parent_flag",parent_flag),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RAQR01 finn = new RAQR01();
                            finn.sims_academic_year = dr["sims_academic_year"].ToString();
                            finn.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            finn.student_name = dr["student_name"].ToString();
                            finn.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();                            
                            finn.parent_name = dr["parent_name"].ToString();
                            finn.curYearInv = float.Parse(dr["curYearInv"].ToString());
                            finn.prevYearInv = float.Parse(dr["prevYearInv"].ToString());
                            finn.prev_prevYearInv = float.Parse(dr["prev_prevYearInv"].ToString());
                            finn.moreThan3Year = float.Parse(dr["moreThan3Year"].ToString());
                            finn.nFive = float.Parse(dr["nFive"].ToString());
                            finn.total_amount = float.Parse(dr["total_amount"].ToString());
                            finn.pc_amount = float.Parse(dr["pc_amount"].ToString());

                            try {
                                finn.current_year_collection = float.Parse(dr["current_year_collection"].ToString());
                                finn.actual_pc_amount = float.Parse(dr["actual_pc_amount"].ToString());
                                finn.return_check_amt = float.Parse(dr["return_check_amt"].ToString());
                            }
                            catch (Exception e) { }

                            code_list.Add(finn);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }

    }
}