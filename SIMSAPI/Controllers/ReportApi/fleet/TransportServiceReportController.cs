﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Helper;
using SIMSAPI.Models.FleetCommonReport;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.ReportApi.fleet
{
    [RoutePrefix("api/TranServiceRep")]
    public class TransportServiceReportController : ApiController
    {
        [Route("getVehicleNo")]
        public HttpResponseMessage getVehicleNo()
        {
            List<TSRQ01> cheqdetail = new List<TSRQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_TransportServiceReport",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "A"),
                                     


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            TSRQ01 sequence = new TSRQ01();
                            sequence.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            sequence.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            sequence.sims_transport_vehicle_name = dr["sims_transport_vehicle_name"].ToString();
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }

        [Route("getDriverName")]
        public HttpResponseMessage getDriverName(string vehicle_no)
        {
            List<TSRQ01> cheqdetail = new List<TSRQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_TransportServiceReport",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "B"),
                                      new SqlParameter("@vehicle_no",vehicle_no),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TSRQ01 sequence = new TSRQ01();
                            sequence.sims_driver_code = dr["sims_driver_code"].ToString();
                            sequence.sims_driver_name = dr["sims_driver_name"].ToString();
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }



        [Route("getTranServiceRepData")]
        public HttpResponseMessage getTranServiceRepData(string vehicle_no, string driver_name)
        {
            List<TSRQ01> cheqdetail = new List<TSRQ01>();
           
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_TransportServiceReport",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@vehicle_no",vehicle_no),
                                     new SqlParameter("@driver_name",driver_name),
                                      

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TSRQ01 sequence = new TSRQ01();
                            sequence.sims_transport_maintenance_doc_no = dr["sims_transport_maintenance_doc_no"].ToString();
                            sequence.sims_transport_maintenance_vehicle_code = dr["sims_transport_maintenance_vehicle_code"].ToString();
                            sequence.sims_transport_maintenance_date = dr["sims_transport_maintenance_date"].ToString();
                            sequence.sims_transport_maintenance_service_station = dr["sims_transport_maintenance_service_station"].ToString();
                            sequence.sims_transport_maintenance_purpose = dr["sims_transport_maintenance_purpose"].ToString();
                            sequence.sims_transport_maintenance_total_amount = dr["sims_transport_maintenance_total_amount"].ToString();
                            sequence.sims_transport_maintenance_approve_by = dr["sims_transport_maintenance_approve_by"].ToString();
                            sequence.sims_transport_maintenance_driver_code = dr["sims_transport_maintenance_driver_code"].ToString();
                            sequence.sims_transport_maintenance_next_date = dr["sims_transport_maintenance_next_date"].ToString();
                            sequence.sims_transport_maintenance_status = dr["sims_transport_maintenance_status"].ToString();
                            sequence.sims_transport_maintenance_Quantity = dr["sims_transport_maintenance_Quantity"].ToString();
                            sequence.sims_transport_vehicle_name = dr["sims_transport_vehicle_name"].ToString();
                            sequence.sims_transport_maintenance_amount = dr["sims_transport_maintenance_amount"].ToString();
                            sequence.sims_transport_maintenance_desc = dr["sims_transport_maintenance_desc"].ToString();
                            
                                
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }
    }
}