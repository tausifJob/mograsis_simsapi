﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.CommonFleet;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi
{
    [RoutePrefix("api/getFleet")]
    public class FleetController:ApiController
    {


        [Route("getStopByRoute")]// acad_year route_direction  route_code 
        public HttpResponseMessage getStopByRoute(string acad_year, string route_direction, string route_code)
        {
            List<CommonFleetData> lstModules = new List<CommonFleetData>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_stops_by_route]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@route_direction",route_direction),
                            new SqlParameter("@route_code",route_code)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleetData sequence = new CommonFleetData();
                            sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            sequence.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            sequence.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            sequence.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            sequence.sims_transport_stop_landmark = dr["sims_transport_stop_landmark"].ToString();
                            sequence.sims_transport_route_stop_expected_time = dr["sims_transport_route_stop_expected_time"].ToString();
                            sequence.sims_transport_route_stop_waiting_time = dr["sims_transport_route_stop_waiting_time"].ToString();
                              lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getDirection")]
        public HttpResponseMessage getDirection()
        {
            List<CommonFleetData> lstModules = new List<CommonFleetData>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_stops_by_route]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleetData sequence = new CommonFleetData();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            lstModules.Add(sequence);



                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getRoute")]
        public HttpResponseMessage getRoute(string acad_year)
        {
            List<CommonFleetData> lstModules = new List<CommonFleetData>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_stops_by_route]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@acad_year", acad_year)
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleetData sequence = new CommonFleetData();
                            sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            lstModules.Add(sequence);
                            	




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStatus")]
        public HttpResponseMessage getStatus()
        {
            List<CommonFleetData> lstModules = new List<CommonFleetData>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_stops_by_route]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleetData sequence = new CommonFleetData();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            


                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getGenrelReport")]
        public HttpResponseMessage getGenrelReport()
        {
            List<CommonFleetData> lstModules = new List<CommonFleetData>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fleet_stops_by_route]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleetData sequence = new CommonFleetData();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();



                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

    
    }
}