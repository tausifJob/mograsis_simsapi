﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;
using SIMSAPI.Models.CommonFleet;
namespace SIMSAPI.Controllers.ReportApi
{

 
    [RoutePrefix("api/StudentsTransport")]
    
    public class EnrolledStudentsTransportController : ApiController
    {
        [Route("getstudenttranslist")]
        public HttpResponseMessage getstudenttranslist(string cur_code, string acad_year, string grade_code, string section_code, string report_type, string without_effective_upto)
        {
            List<CommonFleet> lstModules = new List<CommonFleet>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_enrolled_students_in_transport",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),                            
                            new SqlParameter("@report_type",report_type),                         
                            new SqlParameter("@without_effective_upto",without_effective_upto)                         

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleet sequence = new CommonFleet();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.address_details = dr["address_details"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.bus_no = dr["bus_no"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getrpt_parameter")]
        public HttpResponseMessage getrpt_parameter()
        {
            List<CommonFleet> lstModules = new List<CommonFleet>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_enrolled_students_in_transport",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "N"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonFleet sequence = new CommonFleet();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

         [Route("getdriverdetails")]
         public HttpResponseMessage getdriverdetails(string acad_year, string driver_type, string Status)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_driver_detail",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@acad_year",acad_year),                            
                            new SqlParameter("@driver_type",driver_type),                         
                            new SqlParameter("@Status",Status) 

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_user_code = dr["sims_user_code"].ToString();
                             sequence.sims_driver_name = dr["sims_driver_name"].ToString();
                             sequence.sims_driver_driving_license_number = dr["sims_driver_driving_license_number"].ToString();
                             sequence.sims_driver_license_expiry_date = dr["sims_driver_license_expiry_date"].ToString();
                             sequence.sims_driver_license_vehicle_categoryname = dr["sims_driver_license_vehicle_categoryname"].ToString();
                             sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                             sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                             sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                             sequence.sims_driver_type = dr["sims_driver_type"].ToString();
                             sequence.sims_driver_type_name = dr["sims_driver_type_name"].ToString();

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getdriver_type")]
         public HttpResponseMessage getdriver_type()
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_driver_detail",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getstatusparameter")]
         public HttpResponseMessage getstatusparameter()
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_driver_detail",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "I"),
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.driver_status = dr["driver_status"].ToString();
                             sequence.status_Desc = dr["Status_Desc"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getattofstudents")]
         public HttpResponseMessage getattofstudents(string cur_code, string acad_year, string grade_code, string section_code, string att_date, string att_code)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_using_transport",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),                            
                            new SqlParameter("@att_date",db.DBYYYYMMDDformat(att_date)),                         
                            new SqlParameter("@att_code",att_code)                         

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_code = dr["sims_section_code"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             sequence.sims_attendance_day_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();
                             sequence.parent_no = dr["parent_no"].ToString();
                             sequence.bus_no = dr["bus_no"].ToString();

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getatt_code")]
         public HttpResponseMessage getatt_code(string cur_code)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_using_transport",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@cur_code",cur_code)
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_attendance_short_desc = dr["sims_attendance_short_desc"].ToString();
                             sequence.sims_attendance_description = dr["sims_attendance_description"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getstudentsbybus")]
         public HttpResponseMessage getstudentsbybus(string opr,string cur_code, string acad_year, string direction, string route_code, string bus, string use_transport, string  date)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_by_bus",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", opr),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@direction",direction),
                            new SqlParameter("@route_code",route_code),                            
                            new SqlParameter("@bus",bus),
                            new SqlParameter("@date",db.DBYYYYMMDDformat(date)),                    
                            new SqlParameter("@use_transport",use_transport)         


                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                             sequence.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                             sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                             sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.cnt = dr["cnt"].ToString();

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getdirection")]
         public HttpResponseMessage getdirection()
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_by_bus",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),
                            //new SqlParameter("@cur_code",cur_code)
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getroutebus")]
         public HttpResponseMessage getroutebus(string acad_year, string direction)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_by_bus",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "O"),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@direction",direction)


                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                             sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getbus")]
         public HttpResponseMessage getbus(string acad_year, string direction, string route_code)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_by_bus",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@direction",direction),
                            new SqlParameter("@route_code",route_code)                            

                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                             sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getreporttype")]
         public HttpResponseMessage getreporttype()
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_student_by_bus",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                            //new SqlParameter("@cur_code",cur_code)
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("gettransportalert")]
         public HttpResponseMessage gettransportalert(string cur_code, string from_date, string to_date)
         {
             List<CommonFleet> lstModules = new List<CommonFleet>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_transport_alert_report",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),                    
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))                    

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             CommonFleet sequence = new CommonFleet();
                             sequence.comn_alert_user_code = dr["comn_alert_user_code"].ToString();
                             sequence.comn_alert_date = dr["comn_alert_date"].ToString();
                             sequence.comn_alert_message = dr["comn_alert_message"].ToString();
                             sequence.priority = dr["priority"].ToString();
                             sequence.comn_alert_type_code = dr["comn_alert_type_code"].ToString();
                             sequence.user_name = dr["user_name"].ToString();
                             sequence.comn_alert_status = dr["comn_alert_status"].ToString();

                             lstModules.Add(sequence);
                         }
                     } 
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
    }
}