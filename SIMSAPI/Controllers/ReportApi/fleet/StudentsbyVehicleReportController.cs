﻿using System;
using System.Collections.Generic;
using SIMSAPI.Helper;
using SIMSAPI.Models.FleetCommonReport;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
namespace SIMSAPI.Controllers.ReportApi.fleet
{
    [RoutePrefix("api/studentslist")]
    public class StudentsbyVehicleReportController :ApiController
    {

        [Route("getRouteName")]
        public HttpResponseMessage getRouteName(string acad_year, string route_direction)
        {
            List<SBVQ01> cheqdetail = new List<SBVQ01>();            
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_Students_by_Vehicle",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "T"),
                                     new SqlParameter("@acad_year",acad_year),                                    
                                     new SqlParameter("@route_direction",route_direction),
                                     

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            SBVQ01 sequence = new SBVQ01();
                            sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();                           
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }

        [Route("getDirection")]
        public HttpResponseMessage getDirection()
        {
            List<SBVQ01> cheqdetail = new List<SBVQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_Students_by_Vehicle",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "R"),
                                     


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBVQ01 sequence = new SBVQ01();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }



        [Route("getStudentBtVehicleList")]
        public HttpResponseMessage getStudentBtVehicleList(string acad_year, string tranport_route, string route_direction, string mom_start_date)
        {
            List<SBVQ01> cheqdetail = new List<SBVQ01>();
            if (mom_start_date == "undefined" || mom_start_date == "")
            {
                mom_start_date = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_Students_by_Vehicle",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@acad_year",acad_year),
                                     new SqlParameter("@transport_route",tranport_route),
                                      new SqlParameter("@route_direction",route_direction),
                                     new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBVQ01 sequence = new SBVQ01();
                            sequence.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            sequence.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            sequence.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            sequence.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            sequence.sims_transport_pickup_point = dr["sims_transport_pickup_point"].ToString();
                            sequence.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            sequence.sims_transport_drop_point = dr["sims_transport_drop_point"].ToString();
                            sequence.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            sequence.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.sims_transport_effective_from = dr["sims_transport_effective_from"].ToString();
                            sequence.sims_transport_effective_upto = dr["sims_transport_effective_upto"].ToString();


                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }

        [Route("getStudentPickupDropPointReport")]
        public HttpResponseMessage getStudentPickupDropPointReport(string cur_code,string acad_year, string grade_code, string section_code, string mom_start_date)
        {
            List<SBVQ01> cheqdetail = new List<SBVQ01>();
            if (mom_start_date == "undefined" || mom_start_date == "")
            {
                mom_start_date = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fleet_Students_by_Vehicle",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "A"),
                                      new SqlParameter("@cur_code",cur_code),
                                     new SqlParameter("@acad_year",acad_year),
                                     new SqlParameter("@grade_code",grade_code),
                                      new SqlParameter("@section_name",section_code),
                                     new SqlParameter("@from_date", db.DBYYYYMMDDformat(mom_start_date)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SBVQ01 sequence = new SBVQ01();
                            sequence.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            sequence.sims_transport_enroll_number = dr["sims_transport_enroll_number"].ToString();
                            sequence.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            sequence.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            sequence.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            sequence.sims_transport_effective_from = dr["sims_transport_effective_from"].ToString();
                            sequence.sims_transport_effective_upto = dr["sims_transport_effective_upto"].ToString();
                            sequence.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            sequence.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            sequence.sims_transport_route_student_status = dr["sims_transport_route_student_status"].ToString();
                            sequence.sims_transport_route_student_stop_lat = dr["sims_transport_route_student_stop_lat"].ToString();
                            sequence.sims_transport_route_student_stop_long = dr["sims_transport_route_student_stop_long"].ToString();
                            sequence.sims_transport_share_location = dr["sims_transport_share_location"].ToString();
                            sequence.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            sequence.pick_up_point = dr["pick_up_point"].ToString();
                            sequence.sims_transport_route_stop_expected_time = dr["sims_transport_route_stop_expected_time"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();

                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            sequence.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();

                            sequence.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            sequence.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();

                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            sequence.drop_point = dr["drop_point"].ToString();
                            sequence.student_name = dr["student_name"].ToString();


                            cheqdetail.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cheqdetail);
        }
    }
}