﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Attendance;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.attendance
{

     [RoutePrefix("api/HolidayAttendance")]
    public class HolidayReportController : ApiController
    {
         //supriya
        [Route("getholidayrep")]
        public HttpResponseMessage getholidayrep(string cur_code, string acad_year, string report_param, string exception_type)
        {
            List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_holiday_rpt",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),                    
                            new SqlParameter("@report_param",report_param),  
                            new SqlParameter("@exception_type",exception_type)                    

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AttendanceCommon sequence = new AttendanceCommon();
                            sequence.sims_calendar_exception_description = dr["sims_calendar_exception_description"].ToString();
                            sequence.exception_type = dr["exception_type"].ToString();
                            sequence.sims_from_date = dr["sims_from_date"].ToString();
                            sequence.sims_to_date = dr["sims_to_date"].ToString();
                            sequence.no_of_days = dr["no_of_days"].ToString();
                           

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getrptparam")]
        public HttpResponseMessage getrptparam()
        {
            List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_holiday_rpt",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                           
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AttendanceCommon sequence = new AttendanceCommon();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getexceptiontype")]
         public HttpResponseMessage getexceptiontype(string report_param)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_holiday_rpt",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                            new SqlParameter("@report_param", report_param),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getattendance")]
         public HttpResponseMessage getattendance(string cur_code, string acad_year, string grade_code, string section_code, string from_date, string to_date)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_att_not_marked",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),                    
                            new SqlParameter("@grade_code",grade_code),  
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)), 
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                             sequence.sims_section_code = dr["sims_section_code"].ToString();
                             sequence.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getvariance")]
         public HttpResponseMessage getvariance(string cur_code, string acad_year, string grade, string section, string from_date, string to_date)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_variance_report_for_teacher",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),                    
                            new SqlParameter("@grade",grade),  
                            new SqlParameter("@section",section),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)), 
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                             sequence.sims_section_code = dr["sims_section_code"].ToString();
                             sequence.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             sequence.grade = dr["grade"].ToString();
                             sequence.section = dr["section"].ToString();
                             sequence.sims_employee_code = dr["sims_employee_code"].ToString();
                             sequence.class_teacher = dr["class_teacher"].ToString();
                             sequence.sims_attendance_day_teacher_code = dr["sims_attendance_day_teacher_code"].ToString();
                             sequence.att_teacher = dr["att_teacher"].ToString();
                             sequence.sims_attendance_day_comment = dr["sims_attendance_day_comment"].ToString();


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getAlertNotification")]
         public HttpResponseMessage getAlertNotification(string cur_code, string acad_year, string grade_code, string section_code, string absent_status, string st_date, string end_date)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_alert_notification",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year), 
                            new SqlParameter("@grade_code",grade_code), 
                            new SqlParameter("@section_code",section_code), 
                            new SqlParameter("@absent_status",absent_status),  
                            new SqlParameter("@st_date",db.DBYYYYMMDDformat(st_date)), 
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date))  

                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.class1 = dr["Class"].ToString();
                             sequence.comn_alert_message = dr["comn_alert_message"].ToString();
                             sequence.sims_attendance_day_comment = dr["sims_attendance_day_comment"].ToString();
                             sequence.comn_alert_date = dr["comn_alert_date"].ToString();
                             sequence.time = dr["TIME"].ToString();
                             sequence.attendance_by = dr["attendance_by"].ToString();
                             sequence.notification_send_by = dr["notification_send_by"].ToString();
                             sequence.emp_id = dr["emp_id"].ToString();


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getabsentstatus")]
         public HttpResponseMessage getabsentstatus()
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_student_alert_notification",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }


         [Route("getFireRegisterReport")]
         public HttpResponseMessage getFireRegisterReport(string sims_cur_code, string sims_acad_year, string sims_grade_code, string sims_section_code, string date)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_fire_register_rpt",
                         new List<SqlParameter>() 
                         { 
                           
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_acad_year",sims_acad_year),                    
                            new SqlParameter("@sims_grade_code",sims_grade_code),  
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@date",db.DBYYYYMMDDformat(date)),                           
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             sequence.sims_attendance_day_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();
                             //sequence.sims_attendance_day_am_attendance_code = dr["sims_attendance_day_am_attendance_code"].ToString();
                             //sequence.sims_attendance_day_pm_attendance_code = dr["sims_attendance_day_pm_attendance_code"].ToString();
                             sequence.sims_attendance_day_comment = dr["sims_attendance_day_comment"].ToString();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         
         [Route("getSlipForStudents")]
         public HttpResponseMessage getSlipForStudents(string sims_cur_code, string sims_acad_year, string sims_grade_code, string sims_section_code, string date, string search, string src_by)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 if (search == "undefined" || search == "" || src_by == "undefined" || src_by == "")
                 {
                     search = null;
                     src_by = null;
                 }

                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_attendance_slip_proc",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code",sims_cur_code),
                            new SqlParameter("@sims_acad_year",sims_acad_year),                    
                            new SqlParameter("@sims_grade_code",sims_grade_code),  
                            new SqlParameter("@sims_section_code",sims_section_code),
                            new SqlParameter("@date",db.DBYYYYMMDDformat(date)),           
                            new SqlParameter("@search",search),  
                            new SqlParameter("@src_by",src_by)
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.student_arabic_name = dr["student_arabic_name"].ToString();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                             sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_code = dr["sims_section_code"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             sequence.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             sequence.sims_slot_code = dr["sims_slot_code"].ToString();
                             sequence.sims_attendance_code = dr["sims_attendance_code"].ToString();
                             sequence.remark = dr["remark"].ToString();
                             sequence.att_time = dr["att_time"].ToString();

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getsearch")]
         public HttpResponseMessage getsearch()
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_attendance_slip_proc",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                             sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getAttendanceRule")]
         public HttpResponseMessage getAttendanceRule(string sims_cur_code)
         {
             string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
             List<Sim156> mod_list = new List<Sim156>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_slip_proc]",
                         new List<SqlParameter>() { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@sims_cur_code", sims_cur_code),
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sim156 simsobj = new Sim156();
                             simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                             simsobj.sims_attendance_description = dr["sims_attendance_description"].ToString();
                             mod_list.Add(simsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, mod_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, mod_list);

         }


         [Route("getSlotRule")]
         public HttpResponseMessage getSlotRule()
         {
             string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getGrade()PARAMETERS ::NA";
             List<Sim156> mod_list = new List<Sim156>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_slip_proc]",
                         new List<SqlParameter>() { 
                            new SqlParameter("@opr", "L"),
                            
                        });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sim156 simsobj = new Sim156();
                             simsobj.sims_bell_slot_code = dr["sims_bell_slot_code"].ToString();
                             simsobj.sims_bell_slot_desc = dr["sims_bell_slot_desc"].ToString();
                             mod_list.Add(simsobj);
                         }
                     }
                 }

             }
             catch (Exception x)
             {
                 // Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.OK, mod_list);
             }

             return Request.CreateResponse(HttpStatusCode.OK, mod_list);

         }

         [Route("InsertSims_leaveSlip")]
         public HttpResponseMessage InsertSims_leaveSlip(AttendanceCommon simsobj)
         {
             string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertSims_leaveSlip()";
             //Log.Debug(string.Format(debug, "PP", "InsertSims_MedicalImmunization"));

             bool inserted = false;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_attendance_slip_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_acad_year", simsobj.sims_acad_year),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_enroll_number", simsobj.sims_enroll_number),
                            new SqlParameter("@date", db.DBYYYYMMDDformat(simsobj.date)),  
                            new SqlParameter("@sims_attendance_code", simsobj.sims_attendance_code),
                            new SqlParameter("@sims_slot_code", simsobj.sims_slot_code),
                            new SqlParameter("@issued_by", simsobj.issued_by),
                            new SqlParameter("@remark", simsobj.remark),
                            new SqlParameter("@att_time", simsobj.att_time),
                          
                        });

                     if (ins > 0)
                     {
                         inserted = true;
                     }
                     else
                     {
                         inserted = false;
                     }

                 }
             }
             catch (Exception x)
             {
                 //Log.Error(x);
                 return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
             }
             return Request.CreateResponse(HttpStatusCode.OK, inserted);
         }


         [Route("getattsum")]
         public HttpResponseMessage getattsum(string cur_code, string acad_year, string grade_code, string section_code, string view_mode, string start_date, string end_date, string attendance_code)
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_attendance_sum",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year), 
                            new SqlParameter("@grade_code",grade_code), 
                            new SqlParameter("@section_code",section_code), 
                            new SqlParameter("@view_mode",view_mode),  
                            new SqlParameter("@start_date",db.DBYYYYMMDDformat(start_date)), 
                            new SqlParameter("@end_date",db.DBYYYYMMDDformat(end_date)),
                            new SqlParameter("@attendance_code",attendance_code)


                         }
                          );
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                             sequence.sims_attendance_date = dr["sims_attendance_date"].ToString();
                             sequence.sims_enrollment_number = dr["sims_enrollment_number"].ToString();
                            

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getrpttype")]
         public HttpResponseMessage getrpttype()
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_attendance_sum",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                             sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getattmode")]
         public HttpResponseMessage getattmode()
         {
             List<AttendanceCommon> lstModules = new List<AttendanceCommon>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_attendance_sum",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                           
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             AttendanceCommon sequence = new AttendanceCommon();
                             sequence.sims_attendance_code = dr["sims_attendance_code"].ToString();
                             sequence.sims_attendance_description = dr["sims_attendance_description"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
    }
}