﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Attendance;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.attendance
{
     [RoutePrefix("api/MeritDemeritForm")]
    public class MeritDemeritController : ApiController
    {

        [Route("getMeritDemerit")]
         public HttpResponseMessage getMeritDemerit(string cur_code, string academic_year, string grade_code, string section_code
            , string mom_start_date, string mom_end_date, string search,string var)
            //, string trans_number
        {
            List<AttendanceCommon> lstCuriculum = new List<AttendanceCommon>();
            try
            {
                if (mom_start_date=="null"){
                    mom_start_date=null;
                }
                if (mom_end_date=="null"){
                    mom_end_date=null;
                }
                  if (search=="null"){
                      search = null;
                }
                  //if (trans_number == "null")
                  //{
                  //    trans_number = null;
                  //}
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_merit_demerit]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'S'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", grade_code+section_code),
                             new SqlParameter("@mom_start_date", db.DBYYYYMMDDformat(mom_start_date)),
                             new SqlParameter("@mom_end_date", db.DBYYYYMMDDformat(mom_end_date)),
                              new SqlParameter("@search", search),
                              new SqlParameter("@var",var)
                             //new SqlParameter("@trans_number", trans_number)
                               
                            // new SqlParameter("@user_number", lecture_count),
                             //new SqlParameter("@value", String.IsNullOrEmpty(lecture_count)?null:lecture_count)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AttendanceCommon sequence = new AttendanceCommon();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                           
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                           
                          
                            try
                            {

                                sequence.demerit_points = dr["demerit_points"].ToString();
                                sequence.merit_points = dr["merit_points"].ToString();
                            }
                            catch (Exception ex) { }
                            

                          
                            try
                            {
                                sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                                sequence.sims_section_code = dr["sims_section_code"].ToString();
                                
                                sequence.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                                sequence.class_name = dr["class_name"].ToString();
                                sequence.sims_detention_name = dr["sims_detention_name"].ToString();
                                sequence.sims_detention_description = dr["sims_detention_description"].ToString();
                                sequence.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                                sequence.sims_detention_point = dr["sims_detention_point"].ToString();
                               
                                sequence.sims_detention_remark = dr["sims_detention_remark"].ToString();

                                sequence.reg_by = dr["reg_by"].ToString();
                                sequence.sims_detention_transaction_number = dr["sims_detention_transaction_number"].ToString();
                                sequence.sims_detention_transaction_created_date = dr["sims_detention_transaction_created_date"].ToString();


                            }
                            catch (Exception ex) { }

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }




        [Route("getMeritDemeritParameter")]
        public HttpResponseMessage getMeritDemeritParameter(string meritdemerit)
        {
            List<AttendanceCommon> lstCuriculum = new List<AttendanceCommon>();
            try
            {
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_merit_demerit]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'A'),
                             new SqlParameter("@meritdemerit", meritdemerit),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AttendanceCommon sequence = new AttendanceCommon();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            

                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getMeritDemeritSubModel")]
        public HttpResponseMessage getMeritDemeritSubModel(string cur_code, string academic_year, string grade_code, string section_code, string search, string date, string trans_number)
        {
            List<AttendanceCommon> lstCuriculum = new List<AttendanceCommon>();
            try
            {
                if (date == "undefined")
                {
                    date = null;
                }
                
                if (search == "null")
                {
                    search = null;
                }

                if (trans_number == "undefined")
                {
                    trans_number = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_merit_demerit]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'B'),
                             new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@academic_year", academic_year),
                             new SqlParameter("@grade_code", grade_code),
                             new SqlParameter("@section_code", grade_code+section_code),
                             new SqlParameter("@search", search),
                             new SqlParameter("@date", db.DBYYYYMMDDformat(date)),
                             new SqlParameter("@trans_number",trans_number),
                             
                               
                            // new SqlParameter("@user_number", lecture_count),
                             //new SqlParameter("@value", String.IsNullOrEmpty(lecture_count)?null:lecture_count)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AttendanceCommon sequence = new AttendanceCommon();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_section_code = dr["sims_section_code"].ToString();
                            sequence.sims_detention_level_code = dr["sims_detention_level_code"].ToString();
                            sequence.sims_detention_desc_code = dr["sims_detention_desc_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class_name = dr["class_name"].ToString();
                            sequence.sims_detention_name = dr["sims_detention_name"].ToString();
                            sequence.sims_detention_description = dr["sims_detention_description"].ToString();
                            sequence.sims_detention_point = dr["sims_detention_point"].ToString();
                            sequence.sims_detention_remark = dr["sims_detention_remark"].ToString();
                            sequence.reg_by = dr["reg_by"].ToString();
                            sequence.sims_detention_transaction_number = dr["sims_detention_transaction_number"].ToString();
                            sequence.sims_detention_transaction_created_date = dr["sims_detention_transaction_created_date"].ToString();


                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


    }
}