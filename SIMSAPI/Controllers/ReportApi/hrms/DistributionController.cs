﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.HRMS;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi
{
    [RoutePrefix("api/hrms")]
    public class DistributionController : ApiController
    {

        [Route("getdistributionofstaffbydesignationgender")]
        public HttpResponseMessage getdistributionofstaffbydesignationgender()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_hrms_distribution_of_staff_designation_gender]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S")
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.staff_type = dr["staff_type"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.gender = dr["gender"].ToString();
                            sequence.asian = dr["asian"].ToString();
                            sequence.europian = dr["europian"].ToString();
                            sequence.african = dr["african"].ToString();
                            sequence.uAE = dr["uAE"].ToString();
                            sequence.american = dr["american"].ToString();
                            sequence.astralia = dr["astralia"].ToString();
                            sequence.other_arab = dr["other_arab"].ToString();
                            sequence.other_gcc = dr["other_gcc"].ToString();
                            sequence.total = dr["total"].ToString();
                            	
                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getImmigration")]
        public HttpResponseMessage getImmigration( string comp_code,string doc_code, string dept_code, string show_expired)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_immigration_document]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@doc_code", doc_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@show_expired",show_expired),
                           
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.col = dr["col"].ToString();
                            sequence.em_issue_date = dr["em_issue_date"].ToString();
                            sequence.em_expiry_date = dr["em_expiry_date"].ToString();
                            sequence.em_visa_type = dr["em_visa_type"].ToString();
                            

                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getImmigrationDept")]
        public HttpResponseMessage getImmigrationDept()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_immigration_document]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V")
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            

                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getImmigrationDoc")]
        public HttpResponseMessage getImmigrationDoc()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_immigration_document]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U")
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            sequence.seq = dr["seq"].ToString();
                            	 	


                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getEmpList")] //  comp_code  dept_code  designation_code  staff_type service_status  
        public HttpResponseMessage getEmpList(string comp_code, string dept_code, string designation_code, string staff_type, string service_status)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Emp_List]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@designation_code", designation_code),
                            new SqlParameter("@staff_type",staff_type),
                            new SqlParameter("@service_status",service_status)
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.service_status = dr["service_status"].ToString();
                            sequence.em_name_ot = dr["em_name_ot"].ToString();
                            sequence.designation_name = dr["designation_name"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.emp_staff_type = dr["emp_staff_type"].ToString();
                            lstModules.Add(sequence);
                          }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getDesignation")]
        public HttpResponseMessage getDesignation(string comp_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Emp_List]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@comp_code",comp_code)
                                                        
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.dg_code = dr["dg_code"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                             lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


   

        [Route("getStaffService")]
        public HttpResponseMessage getStaffService(string opr)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Emp_List]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",opr)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getEmpDept")]
        public HttpResponseMessage getEmpDept(string comp_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Emp_List]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                            new SqlParameter("@comp_code", comp_code)
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();


                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getEmpListSecond")] //  comp_code  dept_code  designation_code  staff_type service_status  
        public HttpResponseMessage getEmpListSecond(string comp_code, string dept_code, string designation_code, string staff_type, string service_status)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Emp_List]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@designation_code", designation_code),
                            new SqlParameter("@staff_type",staff_type),
                            new SqlParameter("@service_status",service_status)
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_dept_code = dr["em_dept_code"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.total = dr["total"].ToString();
                           
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getDistriNationDegi")]  
        public HttpResponseMessage getDistriNationDegi()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Distribution_Nationality_designation]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.staff_type = dr["staff_type"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.gender = dr["gender"].ToString();

                            sequence.afghan = dr["afghan"].ToString();
                            sequence.albanian = dr["albanian"].ToString();
                            sequence.algerian = dr["algerian"].ToString();
                            sequence.american = dr["american"].ToString();
                            sequence.armenian = dr["armenian"].ToString();
                            sequence.aruban = dr["aruban"].ToString();
                            sequence.australian = dr["australian"].ToString();
                            sequence.austrian = dr["austrian"].ToString();
                            sequence.azeri = dr["azeri"].ToString();
                            sequence.bahamian = dr["bahamian"].ToString();
                            sequence.bahraini = dr["bahraini"].ToString();
                            sequence.bangladeshi = dr["bangladeshi"].ToString();
                            sequence.barbadian = dr["barbadian"].ToString();
                            sequence.british = dr["british"].ToString();
                            sequence.egyptian = dr["egyptian"].ToString();
                            sequence.filipino = dr["filipino"].ToString();
                            sequence.icelander = dr["icelander"].ToString();
                            sequence.indian = dr["indian"].ToString();
                            sequence.nepalese = dr["nepalese"].ToString();
                            sequence.pakistani = dr["pakistani"].ToString();
                            sequence.sriLankan = dr["sriLankan"].ToString();
                            sequence.syrian = dr["syrian"].ToString();

                            sequence.total = dr["total"].ToString();
                         
                            


                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getReportParam")]
        public HttpResponseMessage getReportParam()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_staff_strength]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                          	



                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getStrength")]//comp_code , em_staff_type 
        public HttpResponseMessage getStrength(string comp_code,string em_staff_type)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_staff_strength]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@em_staff_type",em_staff_type)
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.description1 = dr["description1"].ToString();
                            sequence.count1 = dr["count1"].ToString();
                            sequence.em_staff_type = dr["em_staff_type"].ToString();
                            sequence.em_company_code = dr["em_company_code"].ToString();
                            

                            




                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getEmployeePunchReport")]
        public HttpResponseMessage getEmployeePunchReport( string comp_code,string show_machine_punch)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Hrms_Empoyee_Punch_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code", comp_code),
                            new SqlParameter("@show_machine_punch", show_machine_punch)
                          
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.dg_code = dr["dg_code"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                             sequence.att_date = dr["att_date"].ToString();
                             sequence.att_punch_flag = dr["att_punch_flag"].ToString();

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getReportFor")]
        public HttpResponseMessage getReportFor()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Hrms_Empoyee_Punch_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            






                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getSubstitutionfromDetails")]
        public HttpResponseMessage getSubstitutionfromDetails(string comp_code,string em_staff_type)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_staff_strength]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "U"),
                                      new SqlParameter("@comp_code", comp_code),
                                     new SqlParameter("@em_staff_type", em_staff_type)
                                    



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.service_status = dr["service_status"].ToString();
                            sequence.em_name_ot = dr["em_name_ot"].ToString();
                            sequence.designation_name = dr["designation_name"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.emp_staff_type = dr["emp_staff_type"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getProbationReportForEmployee")] //  comp_code  acad_year dept_code desg_code
        public HttpResponseMessage getProbationReportForEmployee(string comp_code,string acad_year,string dep_code, string desg_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_probation_report_for_employee]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@dep_code", dep_code),
                            new SqlParameter("@desg_code", desg_code),
                        
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                             sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.em_date_of_join = dr["em_date_of_join"].ToString();
                            sequence.probation_date = dr["probation_date"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getProbationDept")]
        public HttpResponseMessage getProbationDept(string comp_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_probation_report_for_employee]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp_code", comp_code)       
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getProbationDesignation")]
        public HttpResponseMessage getProbationDesignation(string comp_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_probation_report_for_employee]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@comp_code",comp_code)
                                                        
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.dg_code = dr["dg_code"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getDistributionHyperlink")]
        public HttpResponseMessage getDistributionHyperlink(string nation_name,string staff_type,string dg_desc, string gender)
        {
            // var gender_new;
              
            List<CommonHrms> lstModules = new List<CommonHrms>();

              if (gender =="Male"){
                    gender="M";
                }
                else
                {
                gender="F";
                }
            try
            {

                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_hrms_distribution_of_staff_designation_gender]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "T"),
                                      new SqlParameter("@country_name", nation_name),
                                      new SqlParameter("@staff_type",staff_type),
                                      new SqlParameter("@dg_desc", dg_desc),
                                        new SqlParameter("@gender", gender),
                                    
                                    
                                    
                                    
                                    



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.service_status = dr["service_status"].ToString();
                            sequence.em_name_ot = dr["em_name_ot"].ToString();
                            sequence.designation_name = dr["designation_name"].ToString();
                            sequence.dept_name = dr["dept_name"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getIncrementDueReport")] //cur_code  academic_year grade_code  dep
        public HttpResponseMessage getIncrementDueReport(string cur_code, string academic_year, string grade_code, string dep)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_increment_due_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@academic_year", academic_year),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@dep",dep)
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.pai_number = dr["pai_number"].ToString();
                            sequence.ename = dr["ename"].ToString();
                            sequence.department = dr["department"].ToString();
                            sequence.cp_desc = dr["cp_desc"].ToString();
                            sequence.increment_type = dr["increment_type"].ToString();
                            sequence.increment = dr["increment"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getIncrementDueReportDepartment")] //academic_year
        public HttpResponseMessage getIncrementDueReportDepartment(string academic_year)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_increment_due_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                            new SqlParameter("@academic_year", academic_year)
                            
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                          
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAcadyear")] 
        public HttpResponseMessage getAcadyear()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_increment_due_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q")
                            
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            	

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getgradecode")]
        public HttpResponseMessage getgradecode()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_increment_due_report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P")
                                  
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.gr_code = dr["gr_code"].ToString();
                            sequence.gr_desc = dr["gr_desc"].ToString();
                            	
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSalaryRevisionReport")] //cur_code  month  year 
        public HttpResponseMessage getSalaryRevisionReport(string cur_code, string month, string year)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Salary_Revision_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@month", month),
                            new SqlParameter("@year", year)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.ename = dr["ename"].ToString();
                            sequence.pa_effective_from = dr["pa_effective_from"].ToString();
                            sequence.cp_desc = dr["cp_desc"].ToString();
                            sequence.last_sal = dr["last_sal"].ToString();
                            sequence.curr_sal = dr["curr_sal"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSalaryRevisionAcadyear")]
        public HttpResponseMessage getSalaryRevisionAcadyear()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Salary_Revision_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")
                            
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();


                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getSalaryRevisionMonth")]
        public HttpResponseMessage getSalaryRevisionMonth()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_HRMS_Salary_Revision_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U")
                            
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.paysheet_month = dr["paysheet_month"].ToString();
                            sequence.paysheet_month_name = dr["paysheet_month_name"].ToString();



                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getRoleWiseUserList")]
        public HttpResponseMessage getRoleWiseUserList(string comp_code, string role_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();
            
            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_security_role_wise_user_list]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                      new SqlParameter("@comp_code", comp_code),
                                      new SqlParameter("@role_code",role_code),
                                      
                                    
                                    
                                    
                                    



                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.Role_Name = dr["Role_Name"].ToString();
                            sequence.comn_user_name = dr["comn_user_name"].ToString();
                            sequence.NAME = dr["NAME"].ToString();
                            sequence.designation = dr["designation"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getRoleWiseUserListRoleCode")]
        public HttpResponseMessage getRoleWiseUserListRoleCode()
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();

            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_security_role_wise_user_list]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "T")
                                     

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.comn_role_code = dr["comn_role_code"].ToString();
                            sequence.comn_role_name = dr["comn_role_name"].ToString();
                            
                            lstModules.Add(sequence);
                            

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getSupervisorReport")]
        public HttpResponseMessage getSupervisorReport(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();

            try
            {


                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_hrms_supervisor_report]",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@cur_code", cur_code),
                                      new SqlParameter("@acad_year", acad_year),
                                       new SqlParameter("@grade_code", grade_code),
                                        new SqlParameter("@section_code", section_code),
                                      
 
 
 

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_first_name = dr["em_first_name"].ToString();
                            sequence.em_middle_name = dr["em_middle_name"].ToString();
                            sequence.em_last_name = dr["em_last_name"].ToString();
                            
                            


  

                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getEmpExpRpt")] //  comp_code  dept_code  designation_code  
        public HttpResponseMessage getEmpExpRpt(string comp, string dept, string desg, string status)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[employee_exp_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp",comp),
                            new SqlParameter("@dept", dept),
                            new SqlParameter("@desg", desg),
                            new SqlParameter("@status", status)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_emp_full_name = dr["em_emp_full_name"].ToString();
                            sequence.company_name = dr["company_name"].ToString();
                            sequence.total_experience = dr["total_experience"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.total_year = dr["total_year"].ToString();

    
                            lstModules.Add(sequence);



                            						

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getEmpExpRpt_degi")]
        public HttpResponseMessage getEmpExpRpt_degi(string comp)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[employee_exp_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@comp",comp)
                                                        
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.dg_code = dr["dg_code"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getEmpExpRpt_Dept")]
        public HttpResponseMessage getEmpExpRpt_Dept(string comp)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[employee_exp_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                            new SqlParameter("@comp", comp)
                            
                            
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();


                            lstModules.Add(sequence);





                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getEmpExpRptGraph")] //  comp_code  dept_code  designation_code  
        public HttpResponseMessage getEmpExpRptGraph(string comp, string dept, string desg,string status)
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[employee_exp_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@comp",comp),
                            new SqlParameter("@dept", dept),
                            new SqlParameter("@desg", desg),
                            new SqlParameter("@status", status)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.emp_count = dr["emp_count"].ToString();
                            sequence.total_year = dr["total_year"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




        [Route("getEmpStatus")]  
        public HttpResponseMessage getEmpStatus( )
        {
            List<CommonHrms> lstModules = new List<CommonHrms>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[employee_exp_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                            
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonHrms sequence = new CommonHrms();
                            sequence.code = dr["code"].ToString();
                            sequence.emp_desc = dr["emp_desc"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



       
    }




}





