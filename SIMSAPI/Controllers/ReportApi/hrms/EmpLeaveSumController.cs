﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.HRMS;
using System.Data;


namespace SIMSAPI.Controllers.ReportApi.HRMS
{
    
           [RoutePrefix("api/empleavesum")]
 public class EmpLeaveSumController : ApiController

    {

               [Route("getemplist")]
               public HttpResponseMessage getemplist(string code, string from_date, string todate)
               {
                   List<CommonHrms> lstModules = new List<CommonHrms>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_leave_sum",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@code",code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@todate",db.DBYYYYMMDDformat(todate))
                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonHrms sequence = new CommonHrms();
                                   sequence.lt_number = dr["lt_number"].ToString();
                                   sequence.emp_name = dr["Emp_Name"].ToString();
                                   sequence.lt_leave_code = dr["lt_leave_code"].ToString();
                                   sequence.cl_desc = dr["cl_desc"].ToString();
                                   sequence.lt_start_date = dr["lt_start_date"].ToString();
                                   sequence.lt_end_date = dr["lt_end_date"].ToString();
                                   sequence.el_maximum_days = dr["el_maximum_days"].ToString();
                                   sequence.lt_days = dr["lt_days"].ToString();
                                   sequence.lt_balance = dr["lt_balance"].ToString();
                                   sequence.rnk = dr["rnk"].ToString();
                                   sequence.ballance_leave = dr["ballance_leave"].ToString();
                                   sequence.el_maximum_days1 = dr["el_maximum_days1"].ToString();

                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }
      
              [Route("getleave_type")]
               public HttpResponseMessage getleave_type()
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_leave_sum",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.cl_code = dr["cl_code"].ToString();
                               sequence.cl_desc = dr["cl_desc"].ToString();
                              

                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }
             [Route("getattlist")]
               public HttpResponseMessage getattlist(string comp_code, string dept_code, string desg_code, string attendance_code, string from_date, string to_date)
               {
                   List<CommonHrms> lstModules = new List<CommonHrms>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_attendance_summary",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@dept_code",dept_code),
                            new SqlParameter("@desg_code",desg_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@attendance_code",attendance_code),
                            new SqlParameter("@comp_code",comp_code),

                         
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonHrms sequence = new CommonHrms();
                                   sequence.em_login_code = dr["em_login_code"].ToString();
                                   sequence.em_dept_code = dr["em_dept_code"].ToString();
                                   sequence.em_desg_code = dr["em_desg_code"].ToString();
                                   sequence.emp_name = dr["emp_name"].ToString();
                                   sequence.att_date = dr["att_date"].ToString();
                                   sequence.att_absent_flag = dr["att_absent_flag"].ToString();
                                   sequence.department = dr["department"].ToString();
                                   sequence.designation = dr["designation"].ToString();
                                   sequence.att_desc = dr["att_desc"].ToString();
                                  
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }
      
           [Route("getStatus")]
               public HttpResponseMessage getStatus()
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_attendance_summary",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "R"),
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.pays_attendance_code = dr["pays_attendance_code"].ToString();
                               sequence.pays_attendance_description = dr["pays_attendance_description"].ToString();
                              

                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
}
           [Route("getDep")]
           public HttpResponseMessage getDep(string comp_code)
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_attendance_summary",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comp_code",comp_code)
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                               sequence.codp_dept_name = dr["codp_dept_name"].ToString();


                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }

           [Route("getDesignation")]
           public HttpResponseMessage getDesignation(string comp_code)
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_emp_attendance_summary",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "K"),
                            new SqlParameter("@comp_code",comp_code)
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.dg_code = dr["dg_code"].ToString();
                               sequence.dg_desc = dr["dg_desc"].ToString();


                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }


           [Route("getdoclist")]
           public HttpResponseMessage getdoclist(string comp_code, string dept_code, string desg_code, string doc_type, string report_param)
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_emp_doc_report",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@dept_code",dept_code),
                            new SqlParameter("@desg_code",desg_code),
                            new SqlParameter("@doc_type",doc_type),
                            new SqlParameter("@comp_code",comp_code),
                            new SqlParameter("@report_param",report_param),


                         
                         }
                            );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.dept_name = dr["dept_name"].ToString();
                               sequence.designation = dr["designation"].ToString();
                               sequence.pays_doc_desc = dr["pays_doc_desc"].ToString();
                               sequence.em_login_code = dr["em_login_code"].ToString();
                               sequence.emp_name = dr["emp_name"].ToString();
                               sequence.pays_doc_issue_date = dr["pays_doc_issue_date"].ToString();
                               sequence.pays_doc_expiry_date = dr["pays_doc_expiry_date"].ToString();

                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }

           [Route("getDocType")]
           public HttpResponseMessage getDocType()
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_emp_doc_report",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "F"),
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.pays_doc_code = dr["pays_doc_code"].ToString();
                               sequence.pays_doc_desc = dr["pays_doc_desc"].ToString();


                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }

           [Route("getDocStatus")]
           public HttpResponseMessage getDocStatus()
           {
               List<CommonHrms> lstModules = new List<CommonHrms>();
               try
               {
                   using (DBConnection db = new DBConnection())
                   {
                       db.Open();
                       SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_emp_doc_report",
                             new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                            
                         }
                              );
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                               CommonHrms sequence = new CommonHrms();
                               sequence.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                               sequence.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();


                               lstModules.Add(sequence);
                           }
                       }
                   }
               }
               catch (Exception x) { }
               return Request.CreateResponse(HttpStatusCode.OK, lstModules);
           }
           
}
}
   
