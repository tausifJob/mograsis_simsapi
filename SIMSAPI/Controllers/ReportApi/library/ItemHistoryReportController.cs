﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;
using System.Data;

namespace SIMSAPI.Controllers.library
{
               [RoutePrefix("api/libhistory")]

    public class ItemHistoryReportController : ApiController
    {
 

               [Route("gethistory")]
                   public HttpResponseMessage gethistory(string cur_code, string from_date, string to_date)
               {
                   List<CommonLibrary> lstModules = new List<CommonLibrary>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_item_history",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonLibrary sequence = new CommonLibrary();
                                   sequence.sims_library_item_number = dr["sims_library_item_number"].ToString();
                                   sequence.sims_library_item_name = dr["sims_library_item_name"].ToString();
                                   sequence.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                                   sequence.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                                   sequence.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                                   sequence.transaction_status = dr["transaction_status"].ToString();
                                   sequence.sims_login_code = dr["sims_login_code"].ToString();
                                   sequence.member_name = dr["member_name"].ToString();
                                   sequence.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                                   sequence.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();

                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
 }
                   
               [Route("gettrans")]
                   public HttpResponseMessage gettrans(string cur_code, string date, string transaction_status)
               {
                   List<CommonLibrary> lstModules = new List<CommonLibrary>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_Lib_transaction",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@date",db.DBYYYYMMDDformat(date)),
                            new SqlParameter("@transaction_status",@transaction_status),

                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonLibrary sequence = new CommonLibrary();
                                   sequence.sims_library_item_number = dr["sims_library_item_number"].ToString();
                                   sequence.sims_library_transaction_number = dr["sims_library_transaction_number"].ToString();
                                   sequence.sims_library_item_name = dr["sims_library_item_name"].ToString();
                                   sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                                   sequence.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                                   sequence.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                                   sequence.sims_library_item_issue_date = dr["sims_library_item_issue_date"].ToString();
                                   sequence.sims_library_item_expected_return_date = dr["sims_library_item_expected_return_date"].ToString();
                                   sequence.sims_library_item_actual_return_date = dr["sims_library_item_actual_return_date"].ToString();
                                   sequence.sims_library_item_qty = dr["sims_library_item_qty"].ToString();
                                   sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();

                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }

               [Route("getTranStatus")]
               public HttpResponseMessage getTranStatus()
               {
                   List<CommonLibrary> lstModules = new List<CommonLibrary>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_Lib_transaction",
                                 new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H"),
                            
                         }
                                  );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonLibrary sequence = new CommonLibrary();
                                   sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                                   sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }
                      
                   
                   [Route("getMembership")]
               public HttpResponseMessage getMembership(string cur_code, string user_group, string previlige_type)
               {
                   List<CommonLibrary> lstModules = new List<CommonLibrary>();
                   try
                   {
                       using (DBConnection db = new DBConnection())
                       {
                           db.Open();
                           SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_lib_Library_Membership",
                               new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@user_group",user_group),
                            new SqlParameter("@previlige_type",previlige_type),

                          
                         }
                                );
                           if (dr.HasRows)
                           {
                               while (dr.Read())
                               {
                                   CommonLibrary sequence = new CommonLibrary();
                                   sequence.sims_library_user_number = dr["sims_library_user_number"].ToString();
                                   sequence.sims_login_code = dr["sims_login_code"].ToString();
                                   sequence.sims_user_name = dr["sims_user_name"].ToString();
                                   sequence.sims_student_cur_code = dr["sims_student_cur_code"].ToString();
                                   sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                                   sequence.sims_library_user_group_type = dr["sims_library_user_group_type"].ToString();
                                   sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                                   sequence.sims_library_membership_type = dr["sims_library_membership_type"].ToString();
                                   sequence.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                                   sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                                   sequence.grade_name = dr["grade_name"].ToString();
                                   sequence.sims_section_code = dr["sims_section_code"].ToString();
                                   sequence.section_name = dr["section_name"].ToString();
                                   sequence.sims_library_joining_date = dr["sims_library_joining_date"].ToString();
                                   sequence.sims_library_expiry_date = dr["sims_library_expiry_date"].ToString();
                                   sequence.sims_library_renewed_date = dr["sims_library_renewed_date"].ToString();
                                   sequence.sims_library_status = dr["sims_library_status"].ToString();


                                   
                                   lstModules.Add(sequence);
                               }
                           }
                       }
                   }
                   catch (Exception x) { }
                   return Request.CreateResponse(HttpStatusCode.OK, lstModules);
               }

                   [Route("getgroup")]
                   public HttpResponseMessage getgroup()
                   {
                       List<CommonLibrary> lstModules = new List<CommonLibrary>();
                       try
                       {
                           using (DBConnection db = new DBConnection())
                           {
                               db.Open();
                               SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_lib_Library_Membership",
                                     new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),

                            
                         }
                                      );
                               if (dr.HasRows)
                               {
                                   while (dr.Read())
                                   {
                                       CommonLibrary sequence = new CommonLibrary();
                                       sequence.comn_user_group_code = dr["comn_user_group_code"].ToString();
                                       sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                                       lstModules.Add(sequence);
                                   }
                               }
                           }
                       }
                       catch (Exception x) { }
                       return Request.CreateResponse(HttpStatusCode.OK, lstModules);
                   }


                   [Route("getprivilege")]
                   public HttpResponseMessage getprivilege(string cur_code, string user_code)
                   {
                       List<CommonLibrary> lstModules = new List<CommonLibrary>();
                       try
                       {
                           using (DBConnection db = new DBConnection())
                           {
                               db.Open();
                               SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_lib_Library_Membership",
                                     new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@user_code",user_code),

                            
                         }
                                      );
                               if (dr.HasRows)
                               {
                                   while (dr.Read())
                                   {
                                       CommonLibrary sequence = new CommonLibrary();
                                       sequence.sims_library_membership_type = dr["sims_library_membership_type"].ToString();
                                       sequence.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                                       lstModules.Add(sequence);
                                   }
                               }
                           }
                       }
                       catch (Exception x) { }
                       return Request.CreateResponse(HttpStatusCode.OK, lstModules);
                   }
                   [Route("getUserHistory")]
                   public HttpResponseMessage getUserHistory(string cur_code, string from_date, string to_date, string user_group)
                   {
                       List<CommonLibrary> lstModules = new List<CommonLibrary>();
                       try
                       {
                           using (DBConnection db = new DBConnection())
                           {
                               db.Open();
                               SqlDataReader dr = db.ExecuteStoreProcedure("sims.[rpt_for_user_history]",
                                   new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                             new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@user_group",user_group),

                          
                         }
                                    );
                               if (dr.HasRows)
                               {
                                   while (dr.Read())
                                   {
                                       CommonLibrary sequence = new CommonLibrary();
                                       sequence.member_name = dr["member_name"].ToString();
                                       sequence.sims_login_code = dr["sims_login_code"].ToString();
                                       sequence.sims_library_transaction_user_code = dr["sims_library_transaction_user_code"].ToString();
                                       sequence.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                                       sequence.sims_library_transaction_date = dr["sims_library_transaction_date"].ToString();
                                       sequence.sims_library_item_actual_return_date = dr["sims_library_item_actual_return_date"].ToString();
                                       sequence.sims_library_item_issue_date = dr["sims_library_item_issue_date"].ToString();
                                       sequence.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();



                                       lstModules.Add(sequence);
                                   }
                               }
                           }
                       }
                       catch (Exception x) { }
                       return Request.CreateResponse(HttpStatusCode.OK, lstModules);
                   }
                   [Route("gethis")]
                   public HttpResponseMessage gethis()
                   {
                       List<CommonLibrary> lstModules = new List<CommonLibrary>();
                       try
                       {
                           using (DBConnection db = new DBConnection())
                           {
                               db.Open();
                               SqlDataReader dr = db.ExecuteStoreProcedure("sims.[rpt_for_user_history]",
                                     new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                            //new SqlParameter("@cur_code",cur_code),
                            //new SqlParameter("@user_code",user_code),

                            
                         }
                                      );
                               if (dr.HasRows)
                               {
                                   while (dr.Read())
                                   {
                                       CommonLibrary sequence = new CommonLibrary();
                                       sequence.sims_library_user_group_type = dr["sims_library_user_group_type"].ToString();
                                       sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                                       lstModules.Add(sequence);
                                   }
                               }
                           }
                       }
                       catch (Exception x) { }
                       return Request.CreateResponse(HttpStatusCode.OK, lstModules);
                   }

    }
}
    


   
