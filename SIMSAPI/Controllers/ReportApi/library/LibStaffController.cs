﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.CommonLib;
using System.Data;
namespace SIMSAPI.Controllers.ReportApi
{
    [RoutePrefix("api/getLib")]
    public class LibStaffController : ApiController
    {
        [Route("getAvailableBookStock")]//cur_code,item_subcategory,item_category 
        public HttpResponseMessage getAvailableBookStock(string cur_code, string item_category, string item_subcategory)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Available_Book_Stock]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@item_category",item_category),
                            new SqlParameter("@item_subcategory",item_subcategory)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            sequence.sims_library_item_desc = dr["sims_library_item_desc"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            sequence.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            sequence.number_of_copies = dr["number_of_copies"].ToString();
                            sequence.total_return_qty = dr["total_return_qty"].ToString();
                            sequence.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            sequence.total = dr["total"].ToString();
                            
                            
                              lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getCategory")]
        public HttpResponseMessage getCategory()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Available_Book_Stock]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            sequence.sims_library_category_description = dr["sims_library_category_description"].ToString();
                            sequence.sims_library_category_item_issue = dr["sims_library_category_item_issue"].ToString();
                            

                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




        [Route("getSubCategory")]
        public HttpResponseMessage getSubCategory()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Available_Book_Stock]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            sequence.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            sequence.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            


                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getMembershipByGrade")]//cur_code,acad_year,grade_code,section_code,previlige_type ,status
        public HttpResponseMessage getMembershipByGrade(string cur_code, string acad_year, string grade_code, string section_code, string previlige_type, string status)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Membershib_By_Grade]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@previlige_type",previlige_type),
                            new SqlParameter("@status",status)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_user_number = dr["sims_library_user_number"].ToString();
                           // sequence.sims_login_code = dr["sims_login_code"].ToString();
                            sequence.sims_user_name = dr["sims_user_name"].ToString();
                            sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            sequence.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                            sequence.sims_library_joining_date = dr["sims_library_joining_date"].ToString();
                            sequence.sims_library_expiry_date = dr["sims_library_expiry_date"].ToString();
                            sequence.sims_library_renewed_date = dr["sims_library_renewed_date"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getPrivilage")]
        public HttpResponseMessage getPrivilage()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Membershib_By_Grade]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_membership_type = dr["sims_library_membership_type"].ToString();
                            sequence.sims_library_privilege_name = dr["sims_library_privilege_name"].ToString();
                           lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getMembership")]
        public HttpResponseMessage getMembership()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Membershib_By_Grade]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.code1 = dr["code1"].ToString();
                            sequence.descri = dr["descri"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibItem")]//cur_code ,cat_code,location,book_status
        public HttpResponseMessage getLibItem(string cur_code, string cat_code, string location, string book_status)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@cat_code",cat_code),
                            new SqlParameter("@location",location),
                            new SqlParameter("@book_status",book_status),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.lib_location = dr["lib_location"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            sequence.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                            sequence.sims_library_catalogue_name = dr["sims_library_catalogue_name"].ToString();
                            sequence.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            sequence.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            sequence.sims_library_isbn_number = dr["sims_library_isbn_number"].ToString();
                            sequence.sims_library_item_title = dr["sims_library_item_title"].ToString();
                            sequence.sims_library_item_author1 = dr["sims_library_item_author1"].ToString();
                            sequence.sims_library_item_name_of_publisher = dr["sims_library_item_name_of_publisher"].ToString();
                            

                             lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibItemSecond")]//cur_code ,cat_code,location,book_status
        public HttpResponseMessage getLibItemSecond(string cur_code, string cat_code, string location, string book_status)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@cat_code",cat_code),
                            new SqlParameter("@location",location),
                            new SqlParameter("@book_status",book_status),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.lib_location = dr["lib_location"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            sequence.sims_library_item_status = dr["sims_library_item_status"].ToString();
                            sequence.count_value = dr["count_value"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getCat")]
        public HttpResponseMessage getCat()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                            sequence.sims_library_category_description = dr["sims_library_category_description"].ToString();
                            sequence.sims_library_category_item_issue = dr["sims_library_category_item_issue"].ToString();
                            lstModules.Add(sequence);
                            	

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLocation")]
        public HttpResponseMessage getLocation()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_location_code = dr["sims_library_location_code"].ToString();
                            sequence.sims_library_location_name = dr["sims_library_location_name"].ToString();
                            sequence.sims_library_location_address1 = dr["sims_library_location_address1"].ToString();
                            sequence.sims_library_location_address2 = dr["sims_library_location_address2"].ToString();
                            sequence.sims_library_location_contact_person = dr["sims_library_location_contact_person"].ToString();
                            sequence.sims_library_location_phone = dr["sims_library_location_phone"].ToString();
                            	
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getStatus")]
        public HttpResponseMessage getStatus()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Item]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstModules.Add(sequence);
                            	




                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }




        [Route("getLibTransactionReport")]//cur_code,grade_code,section_code,user_type,cat_code,sub_cat_code,trns_type
        public HttpResponseMessage getLibTransactionReport(string cur_code, string grade_code, string section, string user_type, string cat_code, string sub_cat_code, string trns_type, string from_date, string to_date)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Transaction_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section",section),
                            new SqlParameter("@user_type",user_type),
                            new SqlParameter("@cat_code",cat_code),
                            new SqlParameter("@sub_cat_code",sub_cat_code),
                            new SqlParameter("@trns_type",trns_type),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))

                                 
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.trans_date = dr["trans_date"].ToString();
                            sequence.ret_date = dr["ret_date"].ToString();
                            sequence.sims_login_code = dr["sims_login_code"].ToString();
                            sequence.sims_library_transaction_user_code = dr["sims_library_transaction_user_code"].ToString();
                            sequence.member_name = dr["member_name"].ToString();
                            sequence.sims_library_item_name = dr["sims_library_item_name"].ToString();
                            sequence.transaction_status = dr["transaction_status"].ToString();
                            sequence.exp_ret_date = dr["exp_ret_date"].ToString();
                            sequence.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibTransactionCat")]
        public HttpResponseMessage getLibTransactionCat()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Transaction_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_category_code = dr["sims_library_category_code"].ToString();
                            sequence.sims_library_category_name = dr["sims_library_category_name"].ToString();
                           lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibTransactionSubCat")]
        public HttpResponseMessage getLibTransactionSubCat()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Transaction_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_subcategory_code = dr["sims_library_subcategory_code"].ToString();
                            sequence.sims_library_subcategory_name = dr["sims_library_subcategory_name"].ToString();
                             lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibTransactionUserType")]
        public HttpResponseMessage getLibTransactionUserType()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Transaction_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.code = dr["code"].ToString();
                            sequence.name = dr["name"].ToString();
                            lstModules.Add(sequence);


                        }

                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getLibTransactionType")]
        public HttpResponseMessage getLibTransactionType()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_Library_Transaction_Report]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "E"),
                           
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstModules.Add(sequence);


                        }	



                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getNoIssueBook")]
        public HttpResponseMessage getNoIssueBook(string cur_code, string acad_year, string grade_code, string section_code, string from_date, string to_date)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_No_Issue_Book_By_Student]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date))
                             
                            
                                 
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
          
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



        [Route("getDefaulterListStudent")]
        public HttpResponseMessage getDefaulterListStudent(string cur_code ,string from_date,string to_date,string type)
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_student_defaulter_list]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                             new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                            new SqlParameter("@type",type)



                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                             sequence.grade_name = dr["grade_name"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                             sequence.sims_library_item_number = dr["sims_library_item_number"].ToString();
                            sequence.sims_library_item_name = dr["sims_library_item_name"].ToString();
                             sequence.sims_library_item_expected_return_date = dr["sims_library_item_expected_return_date"].ToString();
                             sequence.sims_library_fine_description = dr["sims_library_fine_description"].ToString();
                            sequence.sims_library_item_issue_date = dr["sims_library_item_issue_date"].ToString();
                            sequence.sims_fine_expected_amount = dr["sims_fine_expected_amount"].ToString();
                            sequence.sims_library_item_accession_number = dr["sims_library_item_accession_number"].ToString();
                            sequence.em_designation = dr["em_designation"].ToString();
                            sequence.department = dr["department"].ToString();
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getDefaultertype")]
        public HttpResponseMessage getDefaultertype()
        {
            List<LIBR09> lstModules = new List<LIBR09>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Lib_student_defaulter_list]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")                       

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            LIBR09 sequence = new LIBR09();
                            sequence.sims_library_user_group_type = dr["sims_library_user_group_type"].ToString();
                            sequence.comn_user_group_name = dr["comn_user_group_name"].ToString();                          
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }



    }
}