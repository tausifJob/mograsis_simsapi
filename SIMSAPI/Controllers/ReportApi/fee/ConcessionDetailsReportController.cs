﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.FinnCommonReport;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.ReportApi.fee
{
     [RoutePrefix("api/ConcessionDetailsReport")]
    public class ConcessionDetailsReportController : ApiController
    {
        // GET: ConcessionDetailsReport
       [Route("GetConcession")]
         public HttpResponseMessage GetConcession(string acad_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<FIN01> doc_list = new List<FIN01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_sims_concession_details",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@acad_year", acad_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FIN01 simsobj = new FIN01();
                            simsobj.sims_concession_number = dr["sims_concession_number"].ToString();
                            simsobj.sims_concession_description = dr["sims_concession_description"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

       [Route("GetConcessionDetails")]
       public HttpResponseMessage GetConcessionDetails(string cur_code,string acad_year,string grade,string section,string consessionType)
       {
           string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
           //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

           List<FIN01> doc_list = new List<FIN01>();

           Message message = new Message();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_sims_concession_details",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                            new SqlParameter("@concession_type", consessionType),
                         });
                   if (dr.HasRows)
                   {
                       while (dr.Read())
                       {
                           FIN01 simsobj = new FIN01();
                           simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                           simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                           simsobj.student_name = dr["student_name"].ToString();
                           simsobj.grade_name = dr["grade_name"].ToString();
                           simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                           simsobj.section_name = dr["section_name"].ToString();
                           simsobj.sims_section_code = dr["sims_section_code"].ToString();
                           simsobj.concession_Description = dr["Concession_Description"].ToString();
                           simsobj.sims_concession_discount_value = dr["sims_concession_discount_value"].ToString();
                           simsobj.sims_student_date = dr["sims_student_date"].ToString();
                           simsobj.father_name = dr["father_name"].ToString();
                           simsobj.sims_concession_from_date = dr["sims_concession_from_date"].ToString();
                           simsobj.sims_concession_to_date = dr["sims_concession_to_date"].ToString();
                           doc_list.Add(simsobj);
                       }
                       return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                   }
                   else
                       return Request.CreateResponse(HttpStatusCode.OK, doc_list);
               }
           }
           catch (Exception x)
           {
               // Log.Error(x);
               message.strMessage = "No Records Found";
               message.messageType = MessageType.Error;
               return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
           }

           //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
       }

       [Route("CGDClassFeeStructure")]
       public HttpResponseMessage CGDClassFeeStructure(sims_defaulter obj)
       {
           DataSet ds = new DataSet();
           try
           {
               using (DBConnection db = new DBConnection())
               {
                   db.Open();
                   ds = db.ExecuteStoreProcedureDS("sims.rpt_fees_class_fee_structure_report",
                      new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", obj.sims_cur_code),
                            new SqlParameter("@acad_year", obj.sims_academic_year),
                            new SqlParameter("@grade_code", obj.sims_grade_code),
                            new SqlParameter("@section_code", obj.sims_section_code),
                         });
               }
           }
           catch (Exception x)
           {

               return Request.CreateResponse(HttpStatusCode.OK, ds);

           }

           return Request.CreateResponse(HttpStatusCode.OK, ds);

       }
    }
}