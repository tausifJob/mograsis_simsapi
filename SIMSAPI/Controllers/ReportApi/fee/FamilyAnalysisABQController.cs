﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.FinnCommonReport;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.ReportApi.fee
{
    [RoutePrefix("api/FamilyAnalysisABQ")]
    public class FamilyAnalysisABQController : ApiController
    {
        // GET: FamilyAnalysisABQ
        [Route("GetFamilyAnalysisDetails_New")]
        public HttpResponseMessage GetFamilyAnalysisDetails_New(string acad_year, string from, string rpt_status, string with_pdc, string family_status, string amount_filter, string filter_amount)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            if (filter_amount == "undefined")
            {
                filter_amount = null;
            }
            List<FIN01> doc_list = new List<FIN01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_family_analysis_ABQ]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@rpt_status", rpt_status),
                            new SqlParameter("@with_pdc", with_pdc),
                            new SqlParameter("@family_status", family_status),
                            new SqlParameter("@amount_filter", amount_filter),
                            new SqlParameter("@filter_amount", filter_amount)

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FIN01 simsobj = new FIN01();
                            simsobj.sltr_tran_amt_dr = float.Parse(dr["sltr_tran_amt_dr"].ToString());
                            simsobj.sltr_tran_amt_cr = float.Parse(dr["sltr_tran_amt_cr"].ToString());

                            simsobj.pdc_amount = float.Parse(dr["pdc_amount"].ToString());
                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.net_amt = float.Parse(dr["net_amt"].ToString());
                            simsobj.perc_amt = float.Parse(dr["perc_amt"].ToString());
                            simsobj.parent_contact_number = dr["parent_contact_number"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.class1 = dr["class"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            simsobj.sltr_tran_amt_dr_final = dr["sltr_tran_amt_dr_final"].ToString();
                            simsobj.sltr_tran_amt_cr_final = dr["sltr_tran_amt_cr_final"].ToString();
                            simsobj.net_bal_trial_bal = dr["net_bal_trial_bal"].ToString();

                            try
                            {
                                simsobj.net_perc_exclude_OB = dr["net_perc_exclude_OB"].ToString();
                                simsobj.sims_remark = dr["sims_remark"].ToString();
                                simsobj.fee_status_desc = dr["fee_status_desc"].ToString();

                            }
                            catch (Exception e) { }

                            try
                            {
                                simsobj.return_check_amt = float.Parse(dr["return_check_amt"].ToString());
                            }
                            catch (Exception e) { }

                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


        [Route("GetFamilyAnalysisDetails_New_studentwise")]
        public HttpResponseMessage GetFamilyAnalysisDetails_New_studentwise(string acad_year, string from, string rpt_status, string with_pdc, string sims_parent_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<FIN01> doc_list = new List<FIN01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_family_analysis_ABQ]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr", 'G'),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@from_date", db.DBYYYYMMDDformat(from)),
                            new SqlParameter("@rpt_status", rpt_status),
                            new SqlParameter("@with_pdc", with_pdc),
                            new SqlParameter("@sims_parent_number", sims_parent_number),

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FIN01 simsobj = new FIN01();
                            simsobj.sltr_tran_amt_dr = float.Parse(dr["sltr_tran_amt_dr"].ToString());
                            simsobj.sltr_tran_amt_cr = float.Parse(dr["sltr_tran_amt_cr"].ToString());
                            simsobj.pdc_amount = float.Parse(dr["pdc_amount"].ToString());
                            simsobj.father_name = dr["father_name"].ToString();
                            simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                            simsobj.net_amt = float.Parse(dr["net_amt"].ToString());
                            simsobj.perc_amt = float.Parse(dr["perc_amt"].ToString());
                            simsobj.parent_contact_number = dr["parent_contact_number"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();
                            simsobj.class1 = dr["class"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            simsobj.sltr_tran_amt_dr_final = dr["sltr_tran_amt_dr_final"].ToString();
                            simsobj.sltr_tran_amt_cr_final = dr["sltr_tran_amt_cr_final"].ToString();
                            simsobj.net_bal_trial_bal = dr["net_bal_trial_bal"].ToString();

                            try
                            {
                                 simsobj.sims_remark = dr["sims_remark"].ToString();
                                  simsobj.fee_status_desc = dr["fee_status_desc"].ToString();
                            }
                            catch (Exception e) { }
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
    }
}