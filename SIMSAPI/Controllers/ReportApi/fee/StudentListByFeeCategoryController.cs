﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FEE;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.fee
{
      [RoutePrefix("api/StdFeeCategory")]
    public class StudentListByFeeCategoryController : ApiController
    {

        [Route("getlistbyfee")]
          public HttpResponseMessage getlistbyfee(string cur_code, string acad_year, string grade_code, string section_code, string fee_cate)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_std_list_by_fee_category",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code), 
                              new SqlParameter("@fee_cate",fee_cate) 
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_fee_academic_year = dr["sims_fee_academic_year"].ToString();
                            sequence.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_fee_category = dr["sims_fee_category"].ToString();
                            sequence.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                          
                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getfeecat")]
        public HttpResponseMessage getfeecat()
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_std_list_by_fee_category",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_fee_category = dr["sims_fee_category"].ToString();
                            sequence.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getagent")]
        public HttpResponseMessage getagent(string cur_code, string acad_year)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_paying_agent",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year)
                          
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_paying_agent_number = dr["sims_paying_agent_number"].ToString();
                            sequence.sims_paying_agent_description = dr["sims_paying_agent_description"].ToString();
                            sequence.sims_paying_agent_discount_type = dr["sims_paying_agent_discount_type"].ToString();
                            sequence.discount_type = dr["discount_type"].ToString();
                            sequence.sims_paying_agent_discount_value = dr["sims_paying_agent_discount_value"].ToString();
                            sequence.sims_paying_agent_fee_code = dr["sims_paying_agent_fee_code"].ToString();
                            sequence.sims_paying_ldgrctl_code = dr["sims_paying_ldgrctl_code"].ToString();
                            sequence.fee_type = dr["fee_type"].ToString();
                           

                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getfeedetails")]
        public HttpResponseMessage getfeedetails(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_std_fee_detail_summary",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.class1 = dr["class"].ToString();
                            sequence.debit = dr["debit"].ToString();
                            sequence.credit = dr["credit"].ToString();
                            sequence.pc_amt = dr["pc_amt"].ToString();
                            sequence.net_amount = dr["net_amount"].ToString();


                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getfeedet")]
        public HttpResponseMessage getfeedet(string cur_code, string acad_year, string grade_code, string section_code)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_fee_detail",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.expected_amount = dr["expected_amount"].ToString();
                            sequence.fee_paid = dr["fee_paid"].ToString();
                            sequence.doc_no = dr["doc_no"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.doc_date = dr["doc_date"].ToString();


                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getPayingTransaction")]
        public HttpResponseMessage getPayingTransaction(string cur_code, string acad_year)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_paying_transaction_details",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year)
                         
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_paying_agent_description = dr["sims_paying_agent_description"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.fee_type = dr["fee_type"].ToString();
                            sequence.dd_fee_amount_final = dr["dd_fee_amount_final"].ToString();
                            sequence.doc_status = dr["doc_status"].ToString();
                            sequence.doc_no = dr["doc_no"].ToString();


                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getinvoicedif")]
        public HttpResponseMessage getinvoicedif(string cur_code, string acad_year, string grade_code, string section_code, string status)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_expected_invoice_diff",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                             new SqlParameter("@status",status)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            sequence.name = dr["Name"].ToString();
                            sequence.sims_expected_amount = dr["sims_expected_amount"].ToString();
                            sequence.sims_invoice_amount = dr["sims_invoice_amount"].ToString();
                            sequence.sims_difference = dr["sims_difference"].ToString();


                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
        [Route("getsta")]
        public HttpResponseMessage getsta()
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_expected_invoice_diff",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                            
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.status_desc = dr["status_desc"].ToString();
                            sequence.status = dr["status"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getconcession")]
        public HttpResponseMessage getconcession(string cur_code, string acad_year, string grade_code, string section_code, string concession_type)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_fee_concession_rpt",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code),
                            new SqlParameter("@concession_type",concession_type)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.sims_student_name_en = dr["sims_student_name_en"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            sequence.sims_sibling_parent_number = dr["sims_sibling_parent_number"].ToString();
                            sequence.sims_concession_description = dr["sims_concession_description"].ToString();
                            sequence.sims_fee_code_desc = dr["sims_fee_code_desc"].ToString();
                            sequence.sims_concession_from_date = dr["sims_concession_from_date"].ToString();
                            sequence.sims_concession_to_date = dr["sims_concession_to_date"].ToString();
                            sequence.sims_student_name_en = dr["sims_student_name_en"].ToString();
                            sequence.total_fee = dr["total_fee"].ToString();
                            sequence.concession_amount = dr["concession_amount"].ToString();
                            sequence.sims_concession_approved_by = dr["sims_concession_approved_by"].ToString();
                            

                            // sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getconstype")]
        public HttpResponseMessage getconstype(string acad_year)
        {
            List<FeeCategory> lstModules = new List<FeeCategory>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_fee_concession_rpt",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@acad_year", acad_year)
                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FeeCategory sequence = new FeeCategory();
                            sequence.sims_concession_number = dr["sims_concession_number"].ToString();
                            sequence.sims_concession_description = dr["sims_concession_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


    }

}