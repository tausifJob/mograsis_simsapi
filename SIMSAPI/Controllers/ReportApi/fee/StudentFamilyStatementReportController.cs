﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.FeesCommonReport;
using SIMSAPI.Helper;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.ReportApi.fee
{
    [RoutePrefix("api/family_statement")]
    public class StudentFamilyStatementReportController : ApiController
    {

        [Route("getParent_student_status")]
        public HttpResponseMessage getParent_student_status()
        {
            List<SFSQ01> lstCuriculum = new List<SFSQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_StudentFamilyStatementReport",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SFSQ01 sequence = new SFSQ01();
                            sequence.code = dr["code"].ToString();
                            sequence.desc = dr["desc"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getReport")]
        public HttpResponseMessage getReport()
        {
            List<SFSQ01> lstCuriculum = new List<SFSQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_StudentFamilyStatementReport",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SFSQ01 sequence = new SFSQ01();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getReport_dc")]
        public HttpResponseMessage getReport_dc()
        {
            List<SFSQ01> lstCuriculum = new List<SFSQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_StudentFamilyStatementReport",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SFSQ01 sequence = new SFSQ01();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getStudentParentDetails")]
        public HttpResponseMessage getStudentParentDetails(string cur_code, string acad_year, string parent_flag)
        {
            List<SFSQ01> lstmeetingcode = new List<SFSQ01>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_fee_StudentFamilyStatementReport",
                        new List<SqlParameter>()
                         {
                                     new SqlParameter("@opr", "S"),
                                     new SqlParameter("@cur_code", cur_code),
                                     new SqlParameter("@acad_year", acad_year),
                                     new SqlParameter("@parent_flag",parent_flag),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SFSQ01 sequence = new SFSQ01();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.sims_Section_code = dr["sims_Section_code"].ToString();
                            sequence.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.class_name = dr["class_name"].ToString();
                            sequence.family_id = dr["family_id"].ToString();
                            sequence.father_name = dr["father_name"].ToString();
                            sequence.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                            sequence.mother_name = dr["mother_name"].ToString();
                            sequence.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                            lstmeetingcode.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstmeetingcode);
        }
    }
}