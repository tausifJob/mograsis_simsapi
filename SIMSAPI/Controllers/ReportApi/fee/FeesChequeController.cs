﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FEE;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.fee
{
     [RoutePrefix("api/FeesCheque")]
    public class FeesChequeController : ApiController
    {
         [Route("getFeesChequeSubmissionReport")]
         public HttpResponseMessage getFeesChequeSubmissionReport(string cur_code, string acad_year, string from_date, string to_date, string bank_code)
         {
             List<FeesChequeComn> lstModules = new List<FeesChequeComn>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fees_cheque_submission_report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                             new SqlParameter("@bank_code",bank_code),
                           
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FeesChequeComn sequence = new FeesChequeComn();
                             sequence.pc_sl_acno = dr["pc_sl_acno"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.pc_cheque_no = dr["pc_cheque_no"].ToString();
                             sequence.student_name = dr["student_name"].ToString();
                             sequence.pc_due_date = dr["pc_due_date"].ToString();
                             sequence.pb_bank_name = dr["pb_bank_name"].ToString();
                             sequence.pc_amount = dr["pc_amount"].ToString();
                             sequence.pc_submission_date = dr["pc_submission_date"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getFeesChequeSubmissionReportBank_code")]
         public HttpResponseMessage getFeesChequeSubmissionReportBank_code()
         {
             List<FeesChequeComn> lstModules = new List<FeesChequeComn>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_fees_cheque_submission_report]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T")                         
                           
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FeesChequeComn sequence = new FeesChequeComn();
                             sequence.pb_bank_code = dr["pb_bank_code"].ToString();
                             sequence.pb_bank_name = dr["pb_bank_name"].ToString();
                             

                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

    }
}