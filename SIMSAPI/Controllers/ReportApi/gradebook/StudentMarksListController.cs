﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.GradebookReportCommon;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.ReportApi.gradebook
{
    [RoutePrefix("api/MarksList")]
    public class StudentMarksListController : ApiController
    {
        // GET: StudentMarksList
        [Route("GetSubjects")]
        public HttpResponseMessage GetSubjects(string cur_code, string acad_year, string grade_code,string section,string term,string user)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<GBRQ01> doc_list = new List<GBRQ01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_marks_list_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", acad_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section),
                            new SqlParameter("@sims_gb_term_code", term),
                            new SqlParameter("@user", user),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_gb_number = dr["sims_gb_number"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetCategory")]
        public HttpResponseMessage GetCategory(string cur_code, string acad_year, string grade_code,string section,string sims_gb_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<GBRQ01> doc_list = new List<GBRQ01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_marks_list_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", acad_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section),
                            new SqlParameter("@sims_gb_number", sims_gb_number),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.sims_gb_number = dr["sims_gb_number"].ToString();
                            simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetStatus")]
        public HttpResponseMessage GetStatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<GBRQ01> doc_list = new List<GBRQ01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_marks_list_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ01 simsobj = new GBRQ01();
                            simsobj.name = dr["Name"].ToString();
                            simsobj.code = dr["code"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetStudentsList")]
        public HttpResponseMessage GetStudentsList(string cur_code, string acad_year, string grade_code, string section, string sims_gb_number,string status)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<GBRQ01> categories = new List<GBRQ01>();
            List<GBRQ01> assignment = new List<GBRQ01>();
            List<GBRQ01> student_assignmentmarks = new List<GBRQ01>();

            List<GBRQ01> student_list = new List<GBRQ01>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_marks_list_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", acad_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section),
                            new SqlParameter("@sims_gb_cat_code", sims_gb_number),
                            new SqlParameter("@status", status),
                         });

                    while (dr.Read())
                    {
                        GBRQ01 simsobj = new GBRQ01();
                        simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                        categories.Add(simsobj);
                    }

                    dr.NextResult();

                    while (dr.Read())
                    {
                        GBRQ01 simsobj = new GBRQ01();
                        simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                        simsobj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                        simsobj.cnt_assign = dr["cnt_assign"].ToString();
                        simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                        assignment.Add(simsobj);
                    }

                    dr.NextResult();

                    while (dr.Read())
                        {

                            
                            //Student s = new Student();
                            //s.stud_assign = new List<stud_assign>();

                            string str = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            var v = (from p in student_assignmentmarks where p.sims_gb_cat_assign_enroll_number == str select p);

                            GBRQ01 students = new GBRQ01();
                            students.stud_assign = new List<stud_assign>();

                            students.sims_cur_code = dr["sims_cur_code"].ToString();
                            students.sims_academic_year = dr["sims_academic_year"].ToString();
                            students.sims_grade_code = dr["sims_grade_code"].ToString();
                            students.sims_section_code = dr["sims_section_code"].ToString();
                            students.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            students.student_name = dr["student_name"].ToString();

                            stud_assign assignments = new stud_assign();
                            assignments.sims_gb_number = dr["sims_gb_number"].ToString();
                            assignments.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            assignments.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            assignments.sims_gb_cat_assign_enroll_number = dr["sims_gb_cat_assign_enroll_number"].ToString();
                            assignments.final_mark = dr["final_mark"].ToString();
                            assignments.sims_gb_cat_assign_final_grade = dr["sims_gb_cat_assign_final_grade"].ToString();
                            assignments.final_grade = dr["final_grade"].ToString();
                            assignments.sims_mark_grade_color_code = dr["sims_mark_grade_color_code"].ToString();
                            assignments.sims_gb_cat_assign_status = dr["sims_gb_cat_assign_status"].ToString();
                            assignments.sims_gb_cat_assign_comment = dr["sims_gb_cat_assign_comment"].ToString();
                            assignments.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                            assignments.student_name = dr["student_name"].ToString();
                            assignments.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            assignments.sims_gb_cat_assign_max_score_correct = dr["sims_gb_cat_assign_max_score_correct"].ToString();
                            assignments.sims_gb_remarks = dr["sims_gb_remarks"].ToString();
                            assignments.sims_gb_cat_points_possible = dr["sims_gb_cat_points_possible"].ToString();
                            assignments.sims_subject_type_name_en = dr["sims_subject_type_name_en"].ToString();
                            assignments.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            assignments.category_name = dr["category_name"].ToString();
                            assignments.category_weight = dr["category_weight"].ToString();
                            assignments.sims_gb_name = dr["sims_gb_name"].ToString();
                            assignments.narr_grade_group_name = dr["narr_grade_group_name"].ToString();
                            assignments.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            assignments.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            assignments.sims_gb_cat_assign_include_in_final_grade = dr["sims_gb_cat_assign_include_in_final_grade"].ToString();
                            assignments.sims_gb_cat_assign_weightage = dr["sims_gb_cat_assign_weightage"].ToString();
                            assignments.sims_gb_cat_assign_grade_completed_status = dr["sims_gb_cat_assign_grade_completed_status"].ToString();
                            assignments.sims_gb_grade_scale = dr["sims_gb_grade_scale"].ToString();
                            assignments.sims_gb_cat_assign_date = dr["sims_gb_cat_assign_date"].ToString();
                            assignments.sims_allocation_status = dr["sims_allocation_status"].ToString();
                            assignments.perr = dr["perr"].ToString();
                            assignments.final_marks = dr["final_marks"].ToString();
                            assignments.sims_gb_cat_assign_status_cal = dr["sims_gb_cat_assign_status_cal"].ToString();
                            assignments.tot_cat_weight = dr["tot_cat_weight"].ToString();
                            assignments.per = dr["per"].ToString();

                        if (v.Count() == 0)
                        {
                            students.stud_assign.Add(assignments);
                            student_assignmentmarks.Add(students);
                        }
                        else
                        {
                            v.ElementAt(0).stud_assign.Add(assignments);
                        }
                    }
                       // return Request.CreateResponse(HttpStatusCode.OK, student_assignmentmarks);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            object[] ob = new object[4];
            ob[0] = new object();
            ob[1] = new object();
            ob[2] = new object();
            ob[0] = categories;
            ob[1] = assignment;
            ob[2] = student_assignmentmarks;
            var newList =(from x in student_assignmentmarks select new { Id_user = x.sims_gb_cat_assign_enroll_number } ).Distinct();
            ob[3] = newList;
            return Request.CreateResponse(HttpStatusCode.OK, ob);

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }


    }
}