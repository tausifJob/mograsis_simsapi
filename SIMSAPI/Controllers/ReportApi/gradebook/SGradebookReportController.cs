﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.GradebookReportCommon;
using System.Web.Configuration;

namespace SIMSAPI.Controllers.ReportApi.gradebook
{
    [RoutePrefix("api/SGradebookReportController")]
    public class SGradebookReportController : ApiController
    {
        // GET: SGradebookReport
        [Route("GetSubjects")]
        public HttpResponseMessage GetSubjects(string cur_code,string acad_year,string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<SubjectObj> doc_list = new List<SubjectObj>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_Gradebook_AnalysisAssementReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SubjectObj simsobj = new SubjectObj();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            doc_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetReportDetails")]
        public HttpResponseMessage GetReportDetails(string cur_code, string acad_year, string grade_code,string subject_code,string term_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<GBRQ011> doc_list = new List<GBRQ011>();

            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_Gradebook_AnalysisAssementReport",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", acad_year),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@subject_code", subject_code),
                            new SqlParameter("@term_code", term_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GBRQ011 simsobj = new GBRQ011();
                            string str1 = "";
                            str1 = dr["sims_academic_year_description"].ToString();
                            var v = from p in doc_list where p.sims_academic_year_description == str1 select p;
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.acad_data = new List<GBRQ01>();

                            GBRQ01 list = new GBRQ01();
                            // list.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            list.sims_cur_code = dr["sims_cur_code"].ToString();
                            list.sims_grade_code = dr["sims_grade_code"].ToString();
                            list.no_of_students = dr["no_of_students"].ToString();
                            list.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            list.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            list.sims_gb_term_code = dr["sims_gb_term_code"].ToString();
                            list.sims_gb_subject_code = dr["sims_gb_subject_code"].ToString();
                            list.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            list.grade_avarage = dr["grade_avarage"].ToString();
                            list.below50 = dr["below50"].ToString();
                            list.between50to59 = dr["between50to59"].ToString();
                            list.between60to69 = dr["between60to69"].ToString();
                            list.between70to79 = dr["between70to79"].ToString();
                            list.between80to89 = dr["between80to89"].ToString();
                            list.between90to100 = dr["between90to100"].ToString();
                            

                            if (v.Count() == 0)
                            {
                                simsobj.acad_data.Add(list);
                                doc_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).acad_data.Add(list);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }
    }
}