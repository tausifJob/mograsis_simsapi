﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.CommonSche;
using System.Data;

namespace SIMSAPI.Controllers.ReportApi.scheduling
{
    [RoutePrefix("api/SchedulingExceed")]
    public class SchedulingComnController : ApiController
    {
        [Route("getExceedingWorkLoad")]//cur_code,acad_year,bell,version,param,value
        public HttpResponseMessage getExceedingWorkLoad(string cur_code, string acad_year, string bell, string version, string param, string value)
        {
            List<CommonScheduling> lstModules = new List<CommonScheduling>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Scheduling_Exceeding_Teacher_Workload]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@bell",bell),
                            new SqlParameter("@version",version),
                            new SqlParameter("@param",param),
                            new SqlParameter("@value",value),


                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonScheduling sequence = new CommonScheduling();
                           
                            sequence.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                            sequence.sims_bell_teacher_emp_code = dr["sims_bell_teacher_emp_code"].ToString();
                            sequence.sims_bell_teacher_desc = dr["sims_bell_teacher_desc"].ToString();
                            sequence.section_bell_teacher = dr["section_bell_teacher"].ToString();
                            sequence.cnt = dr["cnt"].ToString();
                            lstModules.Add(sequence);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getBell")]//acad_year
        public HttpResponseMessage getBell(string acad_year)
        {
            List<CommonScheduling> lstModules = new List<CommonScheduling>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Scheduling_Exceeding_Teacher_Workload]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@acad_year",acad_year),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonScheduling sequence = new CommonScheduling();
                            sequence.sims_bell_academic_year = dr["sims_bell_academic_year"].ToString();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.sims_bell_desc = dr["sims_bell_desc"].ToString();
                            sequence.sims_bell_status = dr["sims_bell_status"].ToString();
                            lstModules.Add(sequence);
                            

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getVersion")]//acad_year bell
        public HttpResponseMessage getVersion(string acad_year, string bell)
        {
            List<CommonScheduling> lstModules = new List<CommonScheduling>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Scheduling_Exceeding_Teacher_Workload]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@acad_year",acad_year),
                             new SqlParameter("@bell",bell),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonScheduling sequence = new CommonScheduling();
                            sequence.sims_bell_version_name = dr["sims_bell_version_name"].ToString();
                           
                            lstModules.Add(sequence);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getType")]
        public HttpResponseMessage getType()
        {
            List<CommonScheduling> lstModules = new List<CommonScheduling>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[rpt_Scheduling_Exceeding_Teacher_Workload]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonScheduling sequence = new CommonScheduling();
                            sequence.value = dr["value"].ToString();
                            sequence.desc = dr["desc"].ToString();
                            lstModules.Add(sequence);
                            


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
    }
}