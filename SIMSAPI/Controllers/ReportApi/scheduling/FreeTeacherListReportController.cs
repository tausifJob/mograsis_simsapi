﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.FleetCommonReport;
using System.Data;
namespace SIMSAPI.Controllers.Scheduling

{

 
    [RoutePrefix("api/FreeTeacher")]
    
    public class  FreeTeacherListReportController : ApiController
    {
        [Route("getfreeteacherlist")]
        public HttpResponseMessage getfreeteacherlist(string cur_code, string acad_year, string bell_code, string day, string slot)
        {
            List<FTLQR1> lstModules = new List<FTLQR1>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_free_teacher_list",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@bell_code",bell_code),
                            new SqlParameter("@slot",slot),                         
                            new SqlParameter("@day",day)                         

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FTLQR1 sequence = new FTLQR1();
                            sequence.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            sequence.sims_employee_code = dr["sims_employee_code"].ToString();
                            sequence.teacher_name = dr["teacher_name"].ToString();
                            sequence.qualification = dr["qualification"].ToString();
                           

                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }
         [Route("getbell")]
        public HttpResponseMessage getbell(string acad_year)
        {
            List<FTLQR1> lstModules = new List<FTLQR1>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_free_teacher_list",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Y"),
                            new SqlParameter("@acad_year", acad_year)

                         }
                           );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FTLQR1 sequence = new FTLQR1();
                            sequence.sims_bell_code = dr["sims_bell_code"].ToString();
                            sequence.sims_bell_desc = dr["sims_bell_desc"].ToString();                            
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

         [Route("getday")]
         public HttpResponseMessage getday()
         {
             List<FTLQR1> lstModules = new List<FTLQR1>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_free_teacher_list",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),                            

                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FTLQR1 sequence = new FTLQR1();
                             sequence.sims_bell_day_code = dr["sims_bell_day_code"].ToString();
                             sequence.sims_bell_day_desc = dr["sims_bell_day_desc"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getslot")]
         public HttpResponseMessage getslot()
         {
             List<FTLQR1> lstModules = new List<FTLQR1>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_free_teacher_list",
                           new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Q"),
                            
                         }
                            );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FTLQR1 sequence = new FTLQR1();
                             sequence.sims_bell_slot_group = dr["sims_bell_slot_group"].ToString();
                             sequence.sims_bell_slot_group_desc = dr["sims_bell_slot_group_desc"].ToString();
                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }
         [Route("getsupervisorlist")]
         public HttpResponseMessage getsupervisorlist(string cur_code, string acad_year, string grade_code, string section_code)
         {
             List<FTLQR1> lstModules = new List<FTLQR1>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_supervisor_list",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year),
                            new SqlParameter("@grade_code",grade_code),
                            new SqlParameter("@section_code",section_code)                   
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FTLQR1 sequence = new FTLQR1();
                             sequence.em_login_code = dr["em_login_code"].ToString();
                             sequence.em_first_name = dr["em_first_name"].ToString();
                             sequence.em_middle_name = dr["em_middle_name"].ToString();
                             sequence.em_last_name = dr["em_last_name"].ToString();
                             sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                             sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                             sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                             sequence.sims_section_code = dr["sims_section_code"].ToString();
                             sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                             sequence.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            


                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

         [Route("getlecconflictlist")]
         public HttpResponseMessage getlecconflictlist(string cur_code, string acad_year)
         {
             List<FTLQR1> lstModules = new List<FTLQR1>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_of_lecture_time_conflict",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@acad_year",acad_year)
                                       
                         }
                          );
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             FTLQR1 sequence = new FTLQR1();
                             sequence.grade = dr["grade"].ToString();
                             sequence.bell = dr["bell"].ToString();
                             sequence.day_desc = dr["day_desc"].ToString();
                             sequence.sims_bell_slot_code = dr["sims_bell_slot_code"].ToString();
                             sequence.sims_bell_start_time = dr["sims_bell_start_time"].ToString();
                             sequence.sims_bell_teacher_code = dr["sims_bell_teacher_code"].ToString();
                             sequence.sub = dr["sub"].ToString();
                             sequence.section = dr["section"].ToString();
                           



                             lstModules.Add(sequence);
                         }
                     }
                 }
             }
             catch (Exception x) { }
             return Request.CreateResponse(HttpStatusCode.OK, lstModules);
         }

    }
}