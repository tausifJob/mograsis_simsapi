﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.ReportApi.setup
{
    [RoutePrefix("api/surveystatistic")]
    public class SurveyStatisticQueryReportController : ApiController
    {
        [Route("getAllstatisticDetails")]
        public HttpResponseMessage getAllstatisticDetails(string cur_code,string acad_year)
        {
            List<SSQR01> code_list = new List<SSQR01>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.rpt_survey_statistic_report",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                             new SqlParameter("@cur_code", cur_code),
                              new SqlParameter("@acad_year",acad_year),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SSQR01 sur = new SSQR01();
                            sur.sims_survey_code = dr["sims_survey_code"].ToString();                            
                            sur.sims_survey_acad_year = dr["sims_survey_acad_year"].ToString();
                            sur.sims_survey_subject = dr["sims_survey_subject"].ToString();
                            sur.sims_survey_grade_code = dr["sims_survey_grade_code"].ToString();
                            sur.sims_survey_section_code = dr["sims_survey_section_code"].ToString();
                            sur.user_group = dr["user_group"].ToString();
                            sur.single_choice = dr["single_choice"].ToString();
                            sur.subjective = dr["subjective"].ToString();
                            sur.objective = dr["objective"].ToString();
                            sur.rating = dr["rating"].ToString();
                            sur.true_false = dr["true_false"].ToString();
                            sur.attempt_completed = dr["attempt_completed"].ToString();
                            sur.choice_report_status = dr["choice_report_status"].ToString();
                            sur.text_report_status = dr["text_report_status"].ToString();
                            sur.sims_survey_user_group_code = dr["sims_survey_user_group_code"].ToString();
                            sur.text_report_location = dr["text_report_location"].ToString();
                            sur.choice_report_location = dr["choice_report_location"].ToString();
                            code_list.Add(sur);
                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, code_list);
        }
    }
}