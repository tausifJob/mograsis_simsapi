﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;


namespace SIMSAPI.Controllers.UserAuditController
{
    [RoutePrefix("api/UserAudit")]
    [BasicAuthentication]
    public class UserAuditController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllUserAudit")]
        public HttpResponseMessage getAllGoalTarget()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGoalTarget(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllGoalTarget"));

            List<Com007> goaltarget_list = new List<Com007>();
          
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'), 
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_audit_ip = dr["comn_audit_ip"].ToString();
                            simsobj.comn_audit_dns = dr["comn_audit_dns"].ToString();
                            simsobj.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            simsobj.comn_audit_end_time = dr["comn_audit_end_time"].ToString();
                            simsobj.comn_audit_remark = dr["comn_audit_remark"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }



        [Route("getUserAudit")]
        public HttpResponseMessage getUserAudit(string modulecode, string applcode, string startdate, string enddate, string comn_user_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserAudit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getUserAudit"));

            List<Com007> goaltarget_list = new List<Com007>();
            if (modulecode == "undefined"){
                modulecode = null;
            }
            if(applcode == "undefined"){
                applcode = null;
            }
            if (startdate == "undefined"){
                startdate = null;
            }
            if (enddate == "undefined"){
                enddate = null;
            }

            try
            {
                DBConnection dbTemp = new DBConnection();
                string[] spilit_startdate = System.Text.RegularExpressions.Regex.Split(startdate, "-");
                if(spilit_startdate[0].Count()>=4)
                {
                    startdate= dbTemp.UIDDMMYYYYformat(startdate);
                }
                string[] spilit_enddate = System.Text.RegularExpressions.Regex.Split(enddate, "-");
                if (spilit_enddate[0].Count() >= 4)
                {
                    enddate = dbTemp.UIDDMMYYYYformat(enddate);
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),  
                            new SqlParameter("@comn_mod_code",modulecode),
                            new SqlParameter("@comn_appl_code",applcode),
                            new SqlParameter("@comn_audit_start_time",db.DBYYYYMMDDformat(startdate)),
                            new SqlParameter("@comn_audit_end_time",db.DBYYYYMMDDformat(enddate)),
                            new SqlParameter("@comn_user_name",comn_user_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_audit_ip = dr["comn_audit_ip"].ToString();
                            simsobj.comn_audit_dns = dr["comn_audit_dns"].ToString();
                            simsobj.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            simsobj.comn_audit_end_time = dr["comn_audit_end_time"].ToString();
                            simsobj.comn_audit_remark = dr["comn_audit_remark"].ToString();
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        //[Route("getAcademicYear")]
        //public HttpResponseMessage getAcademicYear()
        //{
        //    List<Ucw242> lstModules = new List<Ucw242>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", "W"),

        //                 }
        //                 );
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Ucw242 sequence = new Ucw242();
        //                    sequence.sims_sip_academic_year = dr["sims_academic_year"].ToString();
        //                    sequence.sims_sip_academic_year_desc = dr["sims_academic_year_description"].ToString();
        //                    lstModules.Add(sequence);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        //}


        [Route("getModuleName")]
        public HttpResponseMessage getModuleName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModuleName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getModuleName"));

            List<Com007> mod_list = new List<Com007>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.sims_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.sims_mod_name = dr["comn_mod_name_en"].ToString(); ;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getApplicationName")]
        public HttpResponseMessage getApplicationName(string modcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getApplicationName()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "getApplicationName"));

            List<Com007> mod_list = new List<Com007>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comn_appl_mod_code",modcode)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.sims_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.sims_appl_name = dr["comn_appl_name_en"].ToString();
                            //simsobj.sims_mod_code = dr["comn_appl_mod_code"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }



            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }


        //[Route("getAutoGenerateCode")]
        //public HttpResponseMessage getAutoGenerateCode()
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateCode()PARAMETERS ::NA";
        //    Log.Debug(string.Format(debug, "ERP/Inventory/", "getAutoGenerateCode"));

        //    List<Ucw242> srno_list = new List<Ucw242>();

        //    Message message = new Message();

        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sip_goal_target_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", 'C'),

        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Ucw242 simsobj = new Ucw242();
        //                    simsobj.sims_sip_goal_target_code = dr["Target_code"].ToString();
        //                    srno_list.Add(simsobj);

        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, srno_list);

        //            }
        //            else
        //                return Request.CreateResponse(HttpStatusCode.OK, dr);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        message.strMessage = "No Records Found";
        //        message.messageType = MessageType.Error;
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //    }

        //    //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        //}


        //[Route("CUDGoalTarget")]
        //public HttpResponseMessage CUDGoalTarget(List<Ucw242> data)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDGoalTarget(),PARAMETERS";
        //    //Log.Debug(string.Format(debug, "MODULE", "CUDDocumentDetails", simsobj));

        //    //Per234 simsobj = data;

        //    Message message = new Message();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            foreach (Ucw242 simsobj in data)
        //            {

        //                int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_sip_goal_target_proc]",
        //                    new List<SqlParameter>() 
        //                 { 
		 
        //                        new SqlParameter("@opr", simsobj.opr),
        //                        new SqlParameter("@sims_sip_academic_year", simsobj.sims_sip_academic_year),
        //                        new SqlParameter("@sims_sip_goal_code", simsobj.sims_sip_goal_code),
        //                        new SqlParameter("@sims_sip_goal_target_code", simsobj.sims_sip_goal_target_code),
        //                        new SqlParameter("@sims_sip_goal_target_desc", simsobj.sims_sip_goal_target_desc),
        //                        new SqlParameter("@sims_sip_goal_target_start_date",simsobj.sims_sip_goal_target_start_date),
        //                        new SqlParameter("@sims_sip_goal_target_end_date",simsobj.sims_sip_goal_target_end_date),
        //                        new SqlParameter("@sims_sip_goal_target_min_point",simsobj.sims_sip_goal_target_min_point),
        //                        new SqlParameter("@sims_sip_goal_target_max_point",simsobj.sims_sip_goal_target_max_point),
        //                        new SqlParameter("@sims_sip_goal_target_status",simsobj.sims_sip_goal_target_status.Equals(true)?"A":"I"),
        //                 });
        //                if (ins > 0)
        //                {
        //                    if (simsobj.opr.Equals("U"))
        //                        message.strMessage = "Goal Target Updated Sucessfully!!";
        //                    else if (simsobj.opr.Equals("I"))
        //                        message.strMessage = "Goal Target Added Sucessfully!!";
        //                    else if (simsobj.opr.Equals("D"))
        //                        message.strMessage = "Goal Target Deleted Sucessfully!!";

        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }


        //                else
        //                {
        //                    message.strMessage = "Error In Parsing Information!!";
        //                    message.messageType = MessageType.Error;
        //                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //                }
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, message);

        //        }
        //    }

        //    catch (Exception x)
        //    {

        //        message.strMessage = "Error In Updating Designation!!";

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, message);
        //}


    }
}




