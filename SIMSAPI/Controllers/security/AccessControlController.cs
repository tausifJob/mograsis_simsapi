﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;


namespace SIMSAPI.Controllers.AccessControlController
{
    [RoutePrefix("api/AccessCriteria")]
    [BasicAuthentication]
    public class AccessControlController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAccessCriteria")]
        public HttpResponseMessage getAccessCriteria()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));

            List<sims_access_criteria> goaltarget_list = new List<sims_access_criteria>();
          
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SA"), 
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_access_criteria simsobj = new sims_access_criteria();
                            simsobj.sims_access_code = dr["sims_access_code"].ToString();
                            simsobj.sims_accesss_name = dr["sims_accesss_name"].ToString();
                            simsobj.sims_access_preference = dr["sims_access_preference"].ToString();
                            simsobj.sims_access_hierarchy_include = dr["sims_access_hierarchy_include"].ToString().Equals("A") ? true : false;
                            simsobj.sims_access_status = dr["sims_access_status"].ToString().Equals("A") ? true : false;

                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("AccessCriteriaCRUD_new")]
        public HttpResponseMessage AccessCriteriaCRUD_new(string opr, sims_access_criteria simsobj)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));
            bool result = false;
            string accesscode = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", opr),
                            new SqlParameter("@access_name", simsobj.sims_accesss_name),
                            new SqlParameter("@access_pref", simsobj.sims_access_preference),
                            new SqlParameter("@hierarchy_include", simsobj.sims_access_hierarchy_include==true?"A":"I"),
                            new SqlParameter("@access_status", simsobj.sims_access_status==true?"A":"I"),
                            new SqlParameter("@access_code", simsobj.sims_access_code)
                         });
                    dr.Read();
                    accesscode = dr["access_code"].ToString();

                    result = dr.RecordsAffected > 0 ? true : false;
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("AccessCriteriaCRUD")]
        public HttpResponseMessage AccessCriteriaCRUD(string opr, Object[] arr)
        {
            
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));
            bool result = false;
            string accesscode = string.Empty;
            try
            {
                List<GradeSecObj> gradesection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GradeSecObj>>(arr[0].ToString());
                sims_access_criteria simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<sims_access_criteria>(arr[1].ToString());
                List<sims_access_criteria_applications> appls = Newtonsoft.Json.JsonConvert.DeserializeObject<List<sims_access_criteria_applications>>(arr[2].ToString());

               
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "DS"),
                            new SqlParameter("@access_code", simsobj.sims_access_code)
                         });

                    result = dr.RecordsAffected > 0 ? true : false;
                }

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", opr),
                            new SqlParameter("@access_name", simsobj.sims_accesss_name),
                            new SqlParameter("@access_pref", simsobj.sims_access_preference),
                            new SqlParameter("@hierarchy_include", simsobj.sims_access_hierarchy_include==true?"A":"I"),
                            new SqlParameter("@access_status", simsobj.sims_access_status==true?"A":"I"),
                            new SqlParameter("@access_code", simsobj.sims_access_code)
                         });
                    dr.Read();
                    accesscode = dr["access_code"].ToString();

                    result = dr.RecordsAffected > 0 ? true : false;
                }

                foreach (GradeSecObj go in gradesection)
                {
                    foreach (SectionObj so in go.section)
                    {
                        if (so.status.Equals("A"))
                        {
                            using (DBConnection db = new DBConnection())
                            {
                                db.Open();
                                SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc", new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@opr", "IS"),
                                    new SqlParameter("@sims_grade_code", go.grade_code),
                                    new SqlParameter("@sims_section_code", so.sims_section_code),
                                    new SqlParameter("@sims_created_by", simsobj.sims_access_created_by),
                                    new SqlParameter("@access_code", accesscode),
                                    new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                    new SqlParameter("@sims_cur_code", simsobj.sims_cur_code)



                                });
                            }
                        }
                    }
                }

                foreach (sims_access_criteria_applications ao in appls)
                {
                    if (ao.sims_appl_status || ao.sims_appl_read_status || ao.sims_appl_write_status || ao.sims_appl_config_status)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc", new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@opr", "IA"),
                                    new SqlParameter("@sims_appl_read_status", ao.sims_appl_read_status==true?"A":"I"),
                                    new SqlParameter("@sims_appl_write_status", ao.sims_appl_write_status==true?"A":"I"),
                                    new SqlParameter("@sims_appl_config_stats", ao.sims_appl_config_status==true?"A":"I"),
                                    new SqlParameter("@sims_status", ao.sims_appl_status==true?"A":"I"),
                                    new SqlParameter("@sims_appl_code", ao.sims_appl_code),
                                    new SqlParameter("@access_code", accesscode)
                                });
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("getAllGradesSecAC")]
        public HttpResponseMessage getAllGradesSecAC(string accesscode, string sims_cur_code, string sims_academic_year)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<GradeSecObj> mod_list = new List<GradeSecObj>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_access_criteria_proc]",
                         new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR","GS"),
                                new SqlParameter("@access_code",accesscode),
                                 new SqlParameter("@sims_cur_code",sims_cur_code),
                                new SqlParameter("@sims_academic_year",sims_academic_year),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            GradeSecObj simsobj = new GradeSecObj();
                            str1 = dr["sims_grade_code"].ToString();
                            var v = from p in mod_list where p.grade_code == str1 select p;

                            simsobj.grade_code = dr["sims_grade_code"].ToString();
                            simsobj.grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.aca_year = dr["sims_academic_year"].ToString();

                            simsobj.section = new List<SectionObj>();

                            SectionObj h = new SectionObj();

                            h.sims_section_code = dr["sims_section_code"].ToString();
                            h.sims_section_name = dr["sims_section_name_en"].ToString();
                            h.status = dr["sims_bell_status"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.section.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).section.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getAllApplicationsAC")]
        public HttpResponseMessage getAllApplicationsAC(string accesscode)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<sims_access_criteria_applications> mod_list = new List<sims_access_criteria_applications>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_access_criteria_proc]",
                         new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR","AL"),
                                new SqlParameter("@access_code",accesscode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_access_criteria_applications simsobj = new sims_access_criteria_applications();
                            simsobj.sims_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.sims_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.sims_access_code = dr["sims_access_code"].ToString();
                            simsobj.sims_appl_read_status = dr["sims_appl_read_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_appl_write_status = dr["sims_appl_write_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_appl_config_status = dr["sims_appl_config_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_appl_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getAccessCriteriaRoleDesig")]
        public HttpResponseMessage getAccessCriteriaRoleDesig(string opt)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));

            List<sims_access_criteria_role_Desg> goaltarget_list = new List<sims_access_criteria_role_Desg>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", opt), 
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_access_criteria_role_Desg simsobj = new sims_access_criteria_role_Desg();
                            simsobj.sims_role_deg_code = dr["accessCode"].ToString();
                            simsobj.sims_role_deg_name = dr["accessName"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getUsersAccessCriteriaRoleDesig")]
        public HttpResponseMessage getUsersAccessCriteriaRoleDesig(string flag, string data,string accesscode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));

            List<sims_access_criteria_role_Desg_user> goaltarget_list = new List<sims_access_criteria_role_Desg_user>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "GUD"), 
                            new SqlParameter("@flag", flag), 
                            new SqlParameter("@Search", data), 
                            new SqlParameter("@access_code", accesscode) 
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sims_access_criteria_role_Desg_user simsobj = new sims_access_criteria_role_Desg_user();
                            simsobj.sims_user_name = dr["comn_user_name"].ToString();
                            simsobj.sims_user_alias = dr["comn_user_alias"].ToString();
                            simsobj.sims_role_deg_code = dr["comn_user_role_id"].ToString();
                            simsobj.sims_role_deg_name = dr["role_name"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("UsersAccessCriteriaRoleDesigCRUD")]
        public HttpResponseMessage UsersAccessCriteriaRoleDesigCRUD(string flag,string data,string access_code, List<sims_access_criteria_role_Desg_user> arr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAccessCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAccessCriteria"));
            bool result = true;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "DUD"), 
                            new SqlParameter("@flag", flag), 
                            new SqlParameter("@Search", data), 
                            new SqlParameter("@access_code", access_code), 
                         });
                }
                foreach (sims_access_criteria_role_Desg_user o in arr)
                {
                    if (o.sims_status == true)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_access_criteria_proc",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "IUD"), 
                            new SqlParameter("@flag", flag), 
                            new SqlParameter("@sims_role_dg_code", o.sims_role_deg_code),
                            new SqlParameter("@user_code", o.sims_user_name), 
                            new SqlParameter("@access_code", access_code), 
                            new SqlParameter("@sims_status", o.sims_status==true?"A":"I"), 
                         });
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, false);

            }
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }


      
    }
}




