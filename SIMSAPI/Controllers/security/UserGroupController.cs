﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.security
{  
    [RoutePrefix("api/UserGroup")]
    public class UserGroupController: ApiController
    {
        [Route("getUserGroup")]
        public HttpResponseMessage getUserGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserGroup(),PARAMETERS :: NO";

            List<Com008> goaltarget_list = new List<Com008>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_group_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com008 simsobj = new Com008();
                            simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            simsobj.comn_user_group_name = dr["comn_user_group_name"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDUserGroup")]
        public HttpResponseMessage CUDUserGroup(List<Com008> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Com008 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[comn_user_group_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@comn_user_group_code",simsobj.comn_user_group_code),
                                new SqlParameter("@comn_user_group_name", simsobj.comn_user_group_name),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}