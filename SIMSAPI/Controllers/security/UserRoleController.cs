﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.SIMS.simsClass;




namespace SIMSAPI.Controllers.security
{
    [RoutePrefix("api/UserRole")]
    public class UserRoleController:ApiController
    {
        [Route("getUserRole")]
        public HttpResponseMessage getUserRole()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserRole(),PARAMETERS :: NO";

            List<Com009> goaltarget_list = new List<Com009>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_role_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'S'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com009 simsobj = new Com009();
                            simsobj.comn_role_code = dr["comn_role_code"].ToString();
                            simsobj.comn_role_name = dr["comn_role_name"].ToString();
                            simsobj.comn_role_status = dr["comn_role_status"].Equals("Y") ? true : false;
                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }


        [Route("CUDUserRole")]
        public HttpResponseMessage CUDUserRole(List<Com009> data)
        {
            Message message = new Message();

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Com009 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[comn_user_role_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@comn_role_code",simsobj.comn_role_code),
                                new SqlParameter("@comn_role_name", simsobj.comn_role_name),
                                new SqlParameter("@comn_role_status", simsobj.comn_role_status==true?"Y":"I"),
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}