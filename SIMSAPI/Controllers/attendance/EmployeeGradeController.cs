﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Models.ERP.hrmsClass;

using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/EmpGrade")]
    public class EmployeeGradeController:ApiController
    {
        [Route("getComapany")]
        public HttpResponseMessage getComapany()
        {
            List<Pers021> lstModules = new List<Pers021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_grade_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers021 sequence = new Pers021();
                            sequence.comp_code = dr["comp_code"].ToString();
                            sequence.comp_short_name = dr["comp_short_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getEmpgradeDetail")]
        public HttpResponseMessage getEmpgradeDetail()
        {
            List<Pers021> LeaveType = new List<Pers021>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_employee_grade_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers021 obj = new Pers021();

                            obj.eg_company_code = dr["eg_company_code"].ToString();
                            obj.eg_grade_code = dr["eg_grade_code"].ToString();
                            obj.eg_grade_desc = dr["eg_grade_desc"].ToString();
                            obj.eg_min_basic = dr["eg_min_basic"].ToString();
                            obj.eg_max_basic = dr["eg_max_basic"].ToString();
                            LeaveType.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, LeaveType);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, LeaveType);
        }

        [Route("EmpGradeCUD")]
        public HttpResponseMessage EmpGradeCUD(Pers021 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[pays_employee_grade_proc]",
                            new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@comp_code", simsobj.eg_company_code),
                                 new SqlParameter("@grade_code", simsobj.eg_grade_code),
                                 new SqlParameter("@grade_desc", simsobj.eg_grade_desc),
                                 new SqlParameter("@min_basic", simsobj.eg_min_basic),
                                 new SqlParameter("@max_basic", simsobj.eg_max_basic)

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}