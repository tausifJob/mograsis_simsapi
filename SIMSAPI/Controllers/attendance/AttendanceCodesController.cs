﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/AttendanceCode")]
    public class AttendanceCodeController : ApiController
    {

        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode()
        {
            List<Sims156> AttendanceCode = new List<Sims156>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_attendance_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims156 obj = new Sims156();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_full_name_en = dr["sims_cur_full_name_en"].ToString();

                            obj.sims_category_code = dr["sims_category_code"].ToString();
                            obj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            obj.sims_attendance_code_numeric = dr["sims_attendance_code_numeric"].ToString();
                            obj.sims_attendance_short_desc = dr["sims_attendance_short_desc"].ToString();
                            obj.sims_attendance_description = dr["sims_attendance_description"].ToString();

                            obj.sims_attendance_present_value = dr["sims_attendance_present_value"].ToString();
                            obj.sims_attendance_absent_value = dr["sims_attendance_absent_value"].ToString();
                            obj.sims_attendance_tardy_value = dr["sims_attendance_tardy_value"].ToString();

                            obj.sims_attendance_type_value = dr["sims_attendance_type_value"].ToString();
                            obj.sims_attendance_color_code = dr["sims_attendance_color_code"].ToString();
                            obj.sims_attendance_status_inactive_date = dr["sims_attendance_status_inactive_date"].ToString();

                            obj.sims_attendance_status = dr["sims_attendance_status"].ToString().Equals("A") ? true : false;
                            obj.sims_attendance_unexcused_flag = dr["sims_attendance_unexcused_flag"].ToString().Equals("Y") ? true : false;
                            obj.sims_attendance_teacher_can_use_flag = dr["sims_attendance_teacher_can_use_flag"].ToString().Equals("Y") ? true : false;

                            AttendanceCode.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, AttendanceCode);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, AttendanceCode);
        }


        [Route("CUDAttendanceCode")]
        public HttpResponseMessage CUDAttendanceCode(List<Sims156> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDAttendanceCode", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Sims156 simsobj in data)
                    {
                        if (simsobj.sims_attendance_status_inactive_date == "")
                            simsobj.sims_attendance_status_inactive_date = null;
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_attendance_code_proc]",
                            new List<SqlParameter>()
                            {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_attendance_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@category_code",simsobj.sims_category_code),
                          new SqlParameter("@attendance_code",simsobj.sims_attendance_code),
                          new SqlParameter("@attendance_code_numeric",simsobj.sims_attendance_code_numeric),
                          new SqlParameter("@attendance_short_desc",simsobj.sims_attendance_short_desc),
                          new SqlParameter("@attendance_description",simsobj.sims_attendance_description),
                          new SqlParameter("@attendance_present_value",simsobj.sims_attendance_present_value),
                          new SqlParameter("@attendance_absent_value",simsobj.sims_attendance_absent_value),
                          new SqlParameter("@attendance_tardy_value",simsobj.sims_attendance_tardy_value),
                          new SqlParameter("@attendance_type_value",simsobj.sims_attendance_type_value),
                          new SqlParameter("@attendance_color_code",simsobj.sims_attendance_color_code),
                          new SqlParameter("@attendance_status_inactive_date",simsobj.sims_attendance_status_inactive_date),

                          new SqlParameter("@attendance_teacher_can_use_flag",simsobj.sims_attendance_teacher_can_use_flag==true?"Y":"N"),
                          new SqlParameter("@attendance_unexcused_flag",simsobj.sims_attendance_unexcused_flag==true?"Y":"N"),
                          new SqlParameter("@attendance_status",simsobj.sims_attendance_status==true?"A":"I"),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("CUDAttendanceCodeIU")]
        public HttpResponseMessage CUDAttendanceCodeIU(Sims156 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "FLEET", "CUDAttendanceCodeIU", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();


                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_attendance_code_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_attendance_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@category_code",simsobj.sims_category_code),
                          new SqlParameter("@attendance_code",simsobj.sims_attendance_code),
                          new SqlParameter("@attendance_code_numeric",simsobj.sims_attendance_code_numeric),
                          new SqlParameter("@attendance_short_desc",simsobj.sims_attendance_short_desc),
                          new SqlParameter("@attendance_description",simsobj.sims_attendance_description),
                          new SqlParameter("@attendance_present_value",simsobj.sims_attendance_present_value),
                          new SqlParameter("@attendance_absent_value",simsobj.sims_attendance_absent_value),
                          new SqlParameter("@attendance_tardy_value",simsobj.sims_attendance_tardy_value),
                          new SqlParameter("@attendance_type_value",simsobj.sims_attendance_type_value),
                          new SqlParameter("@attendance_color_code",simsobj.sims_attendance_color_code),
                          new SqlParameter("@attendance_status_inactive_date",simsobj.sims_attendance_status_inactive_date),

                          new SqlParameter("@attendance_teacher_can_use_flag",simsobj.sims_attendance_teacher_can_use_flag==true?"Y":"N"),
                          new SqlParameter("@attendance_unexcused_flag",simsobj.sims_attendance_unexcused_flag==true?"Y":"N"),
                          new SqlParameter("@attendance_status",simsobj.sims_attendance_status==true?"A":"I"),
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }


                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}