﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.AttendanceClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/classwiseattendance")]
    public class ClassWiseAttendanceController : ApiController
    {
        [Route("getAttendanceCuriculum")]
        public HttpResponseMessage getAttendanceCuriculum()
        {
            List<classwiseattendance> list = new List<classwiseattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            classwiseattendance simsobj = new classwiseattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getSectionNamesCurCodeWise")]
        public HttpResponseMessage getSectionNamesCurCodeWise(string cur_code)
        {
            List<classwiseattendance> list = new List<classwiseattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_attendence_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            classwiseattendance simsobj = new classwiseattendance();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.stuStandard = dr["StuStandard"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getSectionNamesCurCodeWise")]
        public HttpResponseMessage getSectionNamesCurCodeWise(string cur_code, string user)
        {
            List<classwiseattendance> list = new List<classwiseattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_attendence_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@user_name",user),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            classwiseattendance simsobj = new classwiseattendance();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.stuStandard = dr["StuStandard"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("classWiseStudentAttendanceUpdate")]
        public HttpResponseMessage classWiseStudentAttendanceUpdate(List<classwiseattendance> listobj)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (classwiseattendance obj in listobj)
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_attendence_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@cur_code",obj.sims_cur_code),
                            new SqlParameter("@attedance_date", db.DBYYYYMMDDformat(obj.attedance_date)),
                            new SqlParameter("@grade_code",obj.sims_grade_code),
                            new SqlParameter("@section_code",obj.sims_section_code),
                            new SqlParameter("@enrollnolist",obj.Enrollment_list),
                            new SqlParameter("@attendance_code",obj.attendance_code),
                            
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            result = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


    }
}