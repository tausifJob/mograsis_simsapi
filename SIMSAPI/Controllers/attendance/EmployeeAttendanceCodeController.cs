﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/EmployeeAttendanceCode")]
    public class EmployeeAttendanceCodeController : ApiController
    {

        [Route("getEmpAttendanceCode")]
        public HttpResponseMessage getEmpAttendanceCode()
        {
            List<empAttendanceCode> AttendanceCode = new List<empAttendanceCode>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_code_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empAttendanceCode obj = new empAttendanceCode();
                            
                            obj.pays_attendance_code = dr["pays_attendance_code"].ToString();
                            obj.pays_attendance_code_numeric = dr["pays_attendance_code_numeric"].ToString();
                            obj.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                            obj.pays_attendance_description = dr["pays_attendance_description"].ToString();
                            obj.pays_attendance_present_value = dr["pays_attendance_present_value"].ToString();

                            obj.pays_attendance_absent_value = dr["pays_attendance_absent_value"].ToString();
                            obj.pays_attendance_tardy_value = dr["pays_attendance_tardy_value"].ToString();
                            obj.pays_attendance_type_value = dr["pays_attendance_type_value"].ToString();
                            obj.pays_attendance_color_code = dr["pays_attendance_color_code"].ToString();
                            obj.pays_attendance_status_inactive_date = db.UIDDMMYYYYformat(dr["pays_attendance_status_inactive_date"].ToString());
                            obj.pays_attendance_status = dr["pays_attendance_status"].ToString().Equals("A") ? true : false;
                            obj.pays_attendance_unexcused_flag = dr["pays_attendance_unexcused_flag"].ToString().Equals("Y") ? true : false;
                            obj.pays_attendance_teacher_can_use_flag = dr["pays_attendance_teacher_can_use_flag"].ToString().Equals("Y") ? true : false;
                            obj.pays_attendance_leave_code = dr["pays_attendance_leave_code"].ToString();
                            AttendanceCode.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, AttendanceCode);
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, AttendanceCode);
        }


        [Route("CUDEmpAttendanceCode")]
        public HttpResponseMessage CUDEmpAttendanceCode(List<empAttendanceCode> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDAttendanceCode", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (empAttendanceCode simsobj in data)
                    {
                        if (simsobj.pays_attendance_status_inactive_date == "")
                            simsobj.pays_attendance_status_inactive_date = null;
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_code_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simsobj.opr),                         
                                new SqlParameter("@attendance_code",simsobj.pays_attendance_code),
                                new SqlParameter("@attendance_code_numeric",simsobj.pays_attendance_code_numeric),
                                new SqlParameter("@attendance_short_desc",simsobj.pays_attendance_short_desc),
                                new SqlParameter("@attendance_description",simsobj.pays_attendance_description),
                                new SqlParameter("@attendance_present_value",simsobj.pays_attendance_present_value),
                                new SqlParameter("@attendance_absent_value",simsobj.pays_attendance_absent_value),
                                new SqlParameter("@attendance_tardy_value",simsobj.pays_attendance_tardy_value),
                                new SqlParameter("@attendance_type_value",simsobj.pays_attendance_type_value),
                                new SqlParameter("@attendance_color_code",simsobj.pays_attendance_color_code),
                                new SqlParameter("@attendance_status_inactive_date",db.DBYYYYMMDDformat(simsobj.pays_attendance_status_inactive_date)),                        
                                new SqlParameter("@attendance_teacher_can_use_flag",simsobj.pays_attendance_teacher_can_use_flag==true?"Y":"N"),
                                new SqlParameter("@attendance_unexcused_flag",simsobj.pays_attendance_unexcused_flag==true?"Y":"N"),
                                new SqlParameter("@attendance_status",simsobj.pays_attendance_status==true?"A":"I"),
                                new SqlParameter("@pays_attendance_leave_code",simsobj.pays_attendance_leave_code)
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        
    }
}