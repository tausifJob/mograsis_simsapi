﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/empdash")]
    public class LeamsEmpDashboardController : ApiController
    {
        [Route("getEmpcount")]
        public HttpResponseMessage getEmpcount()
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.em_service_Status = dr["em_service_Status"].ToString();
                            sequence.count = dr["count"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEmpGradecount")]
        public HttpResponseMessage getEmpGradecount()
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "N"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.em_grade_code = dr["em_grade_code"].ToString();
                            sequence.gr_desc = dr["gr_desc"].ToString();
                            sequence.count = dr["count"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEMPDetailsbyService")]
        public HttpResponseMessage getEMPDetailsbyService(string servicecode)
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "P"),
                             new SqlParameter("@service_status",servicecode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            sequence.em_sex = dr["em_sex"].ToString();
                            sequence.em_mobile = dr["em_mobile"].ToString();
                            sequence.em_email = dr["em_email"].ToString();

                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.em_left_date = dr["em_left_date"].ToString();
                            sequence.em_left_reason = dr["em_left_reason"].ToString();
                            sequence.em_date_of_join = dr["em_date_of_join"].ToString();

                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEMPDetailsbyGrade")]
        public HttpResponseMessage getEMPDetailsbyGrade(string gradecode)
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "O"),
                             new SqlParameter("@pays_grade_code",gradecode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            sequence.gr_desc = dr["gr_desc"].ToString();
                            sequence.em_mobile = dr["em_mobile"].ToString();
                            sequence.em_email = dr["em_email"].ToString();
                            sequence.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            sequence.em_date_of_join = dr["em_date_of_join"].ToString();

                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEmpVisacount")]
        public HttpResponseMessage getEmpVisacount()
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Q"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.Type = dr["visaType"].ToString();
                            sequence.count = dr["count"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEMPDetailsbyVisa")]
        public HttpResponseMessage getEMPDetailsbyVisa(string visatype)
        {
            List<empdash> dailystatus = new List<empdash>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_emp_dash_leams_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@visatype",visatype),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empdash sequence = new empdash();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.emp_name = dr["emp_name"].ToString();
                            sequence.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            sequence.em_mobile = dr["em_mobile"].ToString();
                            sequence.em_email = dr["em_email"].ToString();
                            try{
                            sequence.em_visa_number = dr["em_visa_number"].ToString();
                            sequence.em_visa_issue_date = dr["em_visa_issue_date"].ToString();
                            sequence.em_visa_expiry_date = dr["em_visa_expiry_date"].ToString();
                            }catch(Exception e){}
                            try
                            {
                                sequence.em_national_id = dr["em_national_id"].ToString();
                                sequence.em_national_id_issue_date = dr["em_national_id_issue_date"].ToString();
                                sequence.em_national_id_expiry_date = dr["em_national_id_expiry_date"].ToString();
                            }
                            catch (Exception e) { }
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        
        
        [Route("getNotPunchedEmployee")]
        public HttpResponseMessage getNotPunchedEmployee(string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "N"),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.punch_date = dr["punch_date"].ToString();
                            sequence.em_punching_id = dr["em_punching_id"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getleaveType")]
        public HttpResponseMessage getleaveType()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.leave_code = dr["pays_appl_parameter"].ToString();
                            sequence.leave_type = dr["pays_appl_form_field_value1"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getleaveDetails")]
        public HttpResponseMessage getleaveDetails(string leavecode, string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "L"),
                             new SqlParameter("@leave_code", leavecode),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.leave_code = dr["lt_leave_code"].ToString();
                            sequence.leave_type = dr["pays_appl_form_field_value1"].ToString();
                            sequence.lt_start_date = dr["lt_start_date"].ToString();
                            sequence.lt_end_date = dr["lt_end_date"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEmpAttendanceCount")]
        public HttpResponseMessage getEmpAttendanceCount(string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.att_date = dr["att_date"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.att_absent_flag = dr["att_absent_flag"].ToString();                            
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Y"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }


    }
}