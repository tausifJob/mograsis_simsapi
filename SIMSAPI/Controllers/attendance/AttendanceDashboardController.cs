﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;

namespace SIMSAPI.Controllers.AttendanceDashboardController
{
    [RoutePrefix("api/attendanceDash")]
    public class AttendanceDashboardController : ApiController
    {
        



        [Route("Get_get_Section_Strength")]
        public HttpResponseMessage Get_get_Section_Strength(string cur_code, string academic, string grade)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "A"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                  new SqlParameter("@sims_grade_code", grade)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_nm = dr["sims_section_name_en"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_std_nm = dr["sims_section_stregth"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("Get_get_total_present")]
        public HttpResponseMessage Get_get_total_present(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
                           
               
                  new SqlParameter("@opr", "S"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                  new SqlParameter("@sims_grade_code", grade),
                  new SqlParameter("@sims_section_code", sec),
                  new SqlParameter("@sims_attendance_date_sd", st)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_nm = dr["sims_section_name_en"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_std_nm = dr["sims_section_stregth"].ToString();
                            a.present = dr["present"].ToString();


                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Get_get_total_present_grade")]
        public HttpResponseMessage Get_get_total_present_grade(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
                           
               
                new SqlParameter("@opr", "F"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", st)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_grade_code = dr["sims_grade_code"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Get_get_all_status")]
        public HttpResponseMessage Get_get_all_status(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "B"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", st)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.present = dr["present"].ToString();
                            a.absent = dr["absent1"].ToString();
                            a.trady = dr["tardy"].ToString();
                            a.tradyExcused = dr["tardyExcuse"].ToString();
                            a.Unmarked = dr["Unmarked"].ToString();
                            a.cnt = dr["stregth"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_att_status_pie")]
        public HttpResponseMessage Get_att_status_pie(string cur_code, string academic, string st, string sec, string grade)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "B"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", st)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash comseq = new attnDash();
                            if (double.Parse(dr["att_code_count"].ToString()) > 0)
                            {
                                comseq.Intensity = double.Parse(dr["att_code_count"].ToString());


                            }
                            else
                                comseq.Intensity = 0;
                            comseq.grade_name_ot = dr["att_code_desc"].ToString();

                            list.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Get_att_student_detail")]
        public HttpResponseMessage Get_att_student_detail(string cur_code, string academic, string st, string sec, string grade, string status)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "E"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", st),
                new SqlParameter("@sims_attendance_day_attendance_code_code", status)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash comseq = new attnDash();
                            comseq.sims_std_nm = dr["stud_Nm"].ToString();
                            comseq.sims_Enroll = dr["sims_enrollment_number"].ToString();
                            comseq.Unmarked = dr["status"].ToString();
                            comseq.sims_attendance_date_sd = dr["sims_attendance_date"].ToString();
                            comseq.sims_grade_nm = dr["class"].ToString();
                            list.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("Get_Section_total_present")]
        public HttpResponseMessage Get_Section_total_present(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "J"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", st)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_nm = dr["sims_section_name_en"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_std_nm = dr["sims_section_stregth"].ToString();
                            a.present = dr["presentCount"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


    }
}