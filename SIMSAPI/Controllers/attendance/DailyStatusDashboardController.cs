﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/dailystatusdash")]
    public class DailyStatusDashboardController : ApiController
    {
        [Route("getdailystatus")]
        public HttpResponseMessage getdailystatus()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.capacity = dr["capacity"].ToString();
                            sequence.intake_student = dr["intake_student"].ToString();
                            sequence.vacancy = dr["vacancy"].ToString();
                            sequence.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            sequence.sims_grade_code = dr["sims_grade_code"].ToString();
                            sequence.present = dr["present"].ToString();
                            sequence.absent = dr["absent"].ToString();
                            sequence.unmark = dr["unmark"].ToString();
                            sequence.registration = dr["registration"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getPunchedEmployee")]
        public HttpResponseMessage getPunchedEmployee(string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "P"),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.punch_date = dr["punch_date"].ToString();
                            sequence.em_punching_id = dr["em_punching_id"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.att_shift1_in = dr["att_shift1_in"].ToString();
                            sequence.att_shift2_out = dr["att_shift2_out"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getNotPunchedEmployee")]
        public HttpResponseMessage getNotPunchedEmployee(string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "N"),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.punch_date = dr["punch_date"].ToString();
                            sequence.em_punching_id = dr["em_punching_id"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getleaveType")]
        public HttpResponseMessage getleaveType()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "T"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.leave_code = dr["pays_appl_parameter"].ToString();
                            sequence.leave_type = dr["pays_appl_form_field_value1"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getleaveDetails")]
        public HttpResponseMessage getleaveDetails(string leavecode, string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "L"),
                             new SqlParameter("@leave_code", leavecode),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.leave_code = dr["lt_leave_code"].ToString();
                            sequence.leave_type = dr["pays_appl_form_field_value1"].ToString();
                            sequence.lt_start_date = dr["lt_start_date"].ToString();
                            sequence.lt_end_date = dr["lt_end_date"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getEmpAttendanceCount")]
        public HttpResponseMessage getEmpAttendanceCount(string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.att_date = dr["att_date"].ToString();
                            sequence.em_login_code = dr["em_login_code"].ToString();
                            sequence.em_name = dr["em_name"].ToString();
                            sequence.att_absent_flag = dr["att_absent_flag"].ToString();                            
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Y"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getAdmissionData")]
        public HttpResponseMessage getAdmissionData(string year, string fromdate, string todate, string subopr)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "O"),
                             new SqlParameter("@subopr", subopr),
                             new SqlParameter("@academic_year", year),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(fromdate)),
                             new SqlParameter("@todate",db.DBYYYYMMDDformat(todate)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.today_date = dr["today_date"].ToString();
                            sequence.sims_admission_date = dr["sims_admission_date"].ToString();
                            sequence.sims_admission_number = dr["sims_admission_number"].ToString();
                            sequence.sims_admission_application_number = dr["sims_admission_application_number"].ToString();
                            sequence.sims_admission_pros_number = dr["sims_admission_pros_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getCancelAdmissionData")]
        public HttpResponseMessage getCancelAdmissionData(string year, string fromdate,string todate,string subopr)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@academic_year", year),
                             new SqlParameter("@subopr", subopr),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(fromdate)),
                             new SqlParameter("@todate",db.DBYYYYMMDDformat(todate)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.today_date = dr["today_date"].ToString();
                            sequence.sims_admission_date = dr["admission_date"].ToString();
                            sequence.sims_admission_number = dr["sims_student_admission_number"].ToString();
                            sequence.sims_admission_application_number = dr["sims_student_enroll_number"].ToString();
                            sequence.sims_admission_pros_number = dr["cancel_date"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getFeePaymentMode")]
        public HttpResponseMessage getFeePaymentMode()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.fee_payment_mode = dr["sims_appl_parameter"].ToString();
                            sequence.fee_payment_mode_desc = dr["sims_appl_form_field_value1"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }


        [Route("getCollectionSummary")]
        public HttpResponseMessage getCollectionSummary(string paymentmode,string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "D"),
                             new SqlParameter("@fee_payment_mode", paymentmode),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.today_date = dr["today_date"].ToString();
                            sequence.dd_fee_payment_mode = dr["dd_fee_payment_mode"].ToString();
                            sequence.dd_fee_payment_mode_desc = dr["dd_fee_payment_mode_desc"].ToString();
                            sequence.fee_amount = dr["fee_amount"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }

        [Route("getCollectionSummaryPaymentModeWise")]
        public HttpResponseMessage getCollectionSummaryPaymentModeWise(string paymentmode,string date)
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "E"),
                             new SqlParameter("@fee_payment_mode", paymentmode),
                             new SqlParameter("@selecteddate",db.DBYYYYMMDDformat(date)),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.today_date = dr["today_date"].ToString();
                            sequence.enroll_number = dr["enroll_number"].ToString();
                            sequence.student_name = dr["student_name"].ToString();
                            sequence.dd_fee_number = dr["dd_fee_number"].ToString();
                            sequence.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            sequence.fee_month = dr["fee_month"].ToString();
                            sequence.fee_paid = dr["fee_paid"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }


        [Route("getCollectionSummaryPDCDetails")]
        public HttpResponseMessage getCollectionSummaryPDCDetails()
        {
            List<DSD001> dailystatus = new List<DSD001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_daily_status_dash_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            DSD001 sequence = new DSD001();
                            sequence.today_date = dr["today_date"].ToString();
                            sequence.enroll_number = dr["pc_name"].ToString();
                            sequence.student_name = dr["pdc_amount"].ToString();
                            dailystatus.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, dailystatus);
        }
    }
}