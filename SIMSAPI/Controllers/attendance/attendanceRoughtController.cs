﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/attendanceRought")]
    public class attendanceRoughtController : ApiController
    {
        //  public int sp_call_att_codes = 0;

        [Route("getAttendanceCuriculum")]
        public HttpResponseMessage getAttendanceCuriculum()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendanceyear")]
        public HttpResponseMessage getAttendanceyear(string cur_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancegrade")]
        public HttpResponseMessage getAttendancegrade(string cur_code, string academic_year, string userName)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", academic_year),
                            new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancesection")]
        public HttpResponseMessage getAttendancesection(string cur_code, string academic_year, string gradeCode, string userName)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", academic_year),
                            new SqlParameter("@bell_grade_code", gradeCode),
                            new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancesectionTWAR")]
        public HttpResponseMessage getAttendancesectionTWAR(string cur_code, string academic_year, string gradeCode, string userName)
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_wise_attendance_proc_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SS"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", academic_year),
                            new SqlParameter("@grade_code", gradeCode),
                            new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancetermTWAR")]
        public HttpResponseMessage getAttendancetermTWAR(string cur_code, string academic_year)
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_wise_attendance_proc_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "ST"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancetableTWAR")]
        public HttpResponseMessage getAttendancetableTWAR(string cur_code, string academic_year, string sectionCode, string termCode)
        {
            List<studentattendance_rpt> list = new List<studentattendance_rpt>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_term_wise_attendance_proc_rpt]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SM"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@acad_year", academic_year),
                            new SqlParameter("@section_code", sectionCode),
                            new SqlParameter("@term_code", termCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance_rpt simsobj = new studentattendance_rpt();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_class_name = dr["class"].ToString();
                            simsobj.sims_student_name = dr["studentName"].ToString();
                            simsobj.sims_present = dr["present"].ToString();
                            simsobj.sims_absent = dr["absent"].ToString();
                            simsobj.sims_absent_excused = dr["absent_excused"].ToString();
                            simsobj.sims_Tardy = dr["Tardy"].ToString();
                            simsobj.sims_Unmarked = dr["Unmarked"].ToString();
                            simsobj.sims_total = dr["total"].ToString();
                            simsobj.sims_TotalAttendanceDays = dr["TotalAttendanceDays"].ToString();
                            simsobj.sims_Percentage = dr["Percentage"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancetype")]
        public HttpResponseMessage getAttendancetype()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Attendance Type' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_attendance_type_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAttendancesubtype")]
        public HttpResponseMessage getAttendancesubtype()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Attendance Sub Type' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_type_details_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_attendance_type_details_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancesort")]
        public HttpResponseMessage getAttendancesort()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_form_field_value2,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Sort Option' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_sort_code = dr["sims_appl_form_field_value2"].ToString();
                            simsobj.sims_attendance_sort_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode(string cur_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_code_sort_name = dr["sims_attendance_short_desc"].ToString();
                            simsobj.sims_attendance_code_name = dr["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                if (dr["colorCode"].ToString().Length != 7)
                                    simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                                else
                                    simsobj.sims_attendance_color = dr["colorCode"].ToString();

                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getStudentAttendance")]
        public HttpResponseMessage getStudentAttendance(string cur_code, string ayear, string grade, string section, string attednacedate)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SS"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(attednacedate)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.sims_student_fname = dr["FirstName"].ToString();
                            simsobj.sims_student_nickname = dr["Nickname"].ToString();
                            simsobj.sims_student_img = dr["studentImg"].ToString();
                            simsobj.sims_student_enroll = dr["sims_enroll_number"].ToString();
                            simsobj.sims_attendance_code = dr["AttendanceCode"].ToString();
                            //if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            //{
                            //    simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                            //}

                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                if (dr["colorCode"].ToString().Length != 7)
                                    simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                                else
                                    simsobj.sims_attendance_color = dr["colorCode"].ToString();

                            }
                            simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                            simsobj.sims_student_bday = dr["StudentBday"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_isholiday = dr["HolidayWeekFlag"].ToString().Equals("Y") ? true : false;
                            simsobj.sims_bus_number = dr["BusNo"].ToString();
                            try
                            {
                                simsobj.sims_attendance_day_comment = dr["sims_attendance_day_comment"].ToString();
                            }
                            catch (Exception ex) { }

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("markStudentAttendance")]
        public HttpResponseMessage markStudentAttendance(string cur_code, string ayear, string grade, string section, string enrollNumber, string att_code, string attednacedate)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MA"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@enrollNo", enrollNumber),
                            new SqlParameter("@sims_attendance_code", att_code),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(attednacedate))

                         });
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("markStudentAttendanceList")]
        public HttpResponseMessage markStudentAttendanceList(List<studentattendance_new> list)
        {
            //List<studentattendance_new> list = new List<studentattendance_new>();

            bool result = false;

            foreach (studentattendance_new obj in list)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MA"),
                            new SqlParameter("@sims_cur_code", obj.cur_code),
                            new SqlParameter("@bell_academic_year", obj.ayear),
                            new SqlParameter("@bell_grade_code", obj.grade),
                            new SqlParameter("@bell_section_code",obj.section),
                            new SqlParameter("@enrollNo",obj. enrollNumber),
                            new SqlParameter("@sims_attendance_code", obj.att_code),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(obj.attednacedate)),
                              new SqlParameter("@user_name", obj.user_name)
                         });
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }

                }
                catch (Exception x) { }
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }



        //Monthly Attendance

        [Route("markAllStudentAttendance")]
        public HttpResponseMessage markAllStudentAttendance(List<studentmonthlyattendance> listobj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (studentmonthlyattendance obj in listobj)
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MA"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@bell_academic_year", obj.sims_academic_year),
                            new SqlParameter("@bell_grade_code", obj.sims_grade_code),
                            new SqlParameter("@bell_section_code", obj.sims_section_code),
                            new SqlParameter("@enrollNo", obj.sims_enroll_number),
                            new SqlParameter("@sims_attendance_code", obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date", obj.attednacedate),
                            new SqlParameter("@user_name", obj.user_name)

                         });

                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        dr.Close();
                    }

                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }



        [Route("getTerms")]
        public HttpResponseMessage getTerms(string cur_code, string ayear)
        {
            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "ST"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_name = dr["sims_term_desc_en"].ToString();

                            if (!string.IsNullOrEmpty(dr["sims_term_start_date"].ToString()))
                            {
                                if (!string.IsNullOrEmpty(dr["sims_term_end_date"].ToString()))
                                {
                                    simsobj.sims_term_start_date = DateTime.Parse(dr["sims_term_start_date"].ToString()).Year.ToString() + "-" + DateTime.Parse(dr["sims_term_start_date"].ToString()).Month.ToString() + "-" + DateTime.Parse(dr["sims_term_start_date"].ToString()).Day.ToString();
                                    simsobj.sims_term_end_date = DateTime.Parse(dr["sims_term_end_date"].ToString()).Year.ToString() + "-" + DateTime.Parse(dr["sims_term_end_date"].ToString()).Month.ToString() + "-" + DateTime.Parse(dr["sims_term_end_date"].ToString()).Day.ToString();
                                    list.Add(simsobj);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getTermDetails")]
        public HttpResponseMessage getTermDetails(string startDate, string endDate)
        {
            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            try
            {

                DateTime dt1 = DateTime.Parse(startDate);
                dt1 = new DateTime(dt1.Year, dt1.Month, 1);

                DateTime dt2 = DateTime.Parse(endDate);

                while (dt1 <= dt2)
                {
                    studentmonthlyattendance obj = new studentmonthlyattendance();
                    obj.sims_month_no = dt1.Month.ToString();
                    obj.sims_month_name = string.Format("{0}-{1}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dt1.Month).ToString(), dt1.Year.ToString());
                    list.Add(obj);
                    dt1 = dt1.AddMonths(1);
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendanceMonthForEMP")]
        public HttpResponseMessage getAttendanceMonthForEMP(string startDate, string endDate)
        {
            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            try
            {

                DateTime dt1 = DateTime.Parse(startDate);
                dt1 = new DateTime(dt1.Year, dt1.Month, 1);

                DateTime dt2 = DateTime.Parse(endDate);

                while (dt1 <= dt2)
                {
                    studentmonthlyattendance obj = new studentmonthlyattendance();
                    obj.sims_month_no = dt1.Month.ToString("d2");
                    //obj.sims_month_no = dt1.Month.ToString();
                    obj.sims_month_name = string.Format("{0}-{1}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dt1.Month).ToString(), dt1.Year.ToString());
                    obj.sims_year = dt1.Year.ToString();
                    list.Add(obj);
                    dt1 = dt1.AddMonths(1);
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        private string Attendance_codes(string attednace_code, string cur_code)
        {
            string color_code = string.Empty;

            List<studentattendance> codes = new List<studentattendance>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_code_sort_name = dr["sims_attendance_short_desc"].ToString();
                            simsobj.sims_attendance_code_name = dr["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                            }
                            codes.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x) { }


            if (!string.IsNullOrEmpty(attednace_code))
            {
                studentattendance obj = (from c in codes where c.sims_attendance_code == attednace_code select c).Single();
                color_code = obj.sims_attendance_color;
            }
            return color_code;


        }


        [Route("getMonthlyAttendance")]
        public HttpResponseMessage getMonthlyAttendance(string cur_code, string ayear, string grade, string section)
        {

            int d = (int)System.DateTime.Now.DayOfWeek;

            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            List<studentattendance> codes = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            studentattendance simsobj1 = new studentattendance();
                            simsobj1.sims_attendance_code = dr1["sims_attendance_code"].ToString();
                            simsobj1.sims_attendance_code_sort_name = dr1["sims_attendance_short_desc"].ToString();
                            simsobj1.sims_attendance_code_name = dr1["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr1["colorCode"].ToString()))
                            {
                                if (dr1["colorCode"].ToString().Length != 7)
                                    simsobj1.sims_attendance_color = string.Format("#{0}", dr1["colorCode"].ToString().Substring(3));
                                else
                                    simsobj1.sims_attendance_color = dr1["colorCode"].ToString();

                            }
                            codes.Add(simsobj1);
                        }
                    }
                    dr1.Close();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AX"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", section),
                            //new SqlParameter("@monthNo", month),
                            //new SqlParameter("@year", year),
                         });

                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.Yearno = dr["Yearno"].ToString();
                            simsobj.monthYear = dr["monthYear"].ToString();
                            //simsobj.attendace_date = dr["dt"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enrollment_number"].ToString();
                            simsobj.Transport = dr["Transport"].ToString();
                            simsobj.BusNo = dr["BusNo"].ToString();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.FirstName = dr["FirstName"].ToString();
                            simsobj.NickName = dr["NickName"].ToString();
                            simsobj.studentDOB = dr["StudentDOB"].ToString();


                            try
                            {
                                if (dr["t"].ToString().Equals("0") || dr["t"].ToString().Equals("I"))
                                    simsobj.t = true;
                                else
                                    simsobj.t = false;
                            }
                            catch (Exception ex) { simsobj.t = false; }
                            simsobj.attendance_day1_code = dr["1"].ToString();
                            simsobj.attendance_day2_code = dr["2"].ToString();
                            simsobj.attendance_day3_code = dr["3"].ToString();
                            simsobj.attendance_day4_code = dr["4"].ToString();
                            simsobj.attendance_day5_code = dr["5"].ToString();
                            simsobj.attendance_day6_code = dr["6"].ToString();
                            simsobj.attendance_day7_code = dr["7"].ToString();
                            simsobj.attendance_day8_code = dr["8"].ToString();
                            simsobj.attendance_day9_code = dr["9"].ToString();
                            simsobj.attendance_day10_code = dr["10"].ToString();
                            simsobj.attendance_day11_code = dr["11"].ToString();
                            simsobj.attendance_day12_code = dr["12"].ToString();
                            simsobj.attendance_day13_code = dr["13"].ToString();
                            simsobj.attendance_day14_code = dr["14"].ToString();
                            simsobj.attendance_day15_code = dr["15"].ToString();
                            simsobj.attendance_day16_code = dr["16"].ToString();
                            simsobj.attendance_day17_code = dr["17"].ToString();
                            simsobj.attendance_day18_code = dr["18"].ToString();
                            simsobj.attendance_day19_code = dr["19"].ToString();
                            simsobj.attendance_day20_code = dr["20"].ToString();
                            simsobj.attendance_day21_code = dr["21"].ToString();
                            simsobj.attendance_day22_code = dr["22"].ToString();
                            simsobj.attendance_day23_code = dr["23"].ToString();
                            simsobj.attendance_day24_code = dr["24"].ToString();
                            simsobj.attendance_day25_code = dr["25"].ToString();
                            simsobj.attendance_day26_code = dr["26"].ToString();
                            simsobj.attendance_day27_code = dr["27"].ToString();
                            simsobj.attendance_day28_code = dr["28"].ToString();
                            simsobj.attendance_day29_code = dr["29"].ToString();
                            simsobj.attendance_day30_code = dr["30"].ToString();
                            simsobj.attendance_day31_code = dr["31"].ToString();





                            if (!string.IsNullOrEmpty(simsobj.attendance_day1_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day1_code select c).Single();
                                simsobj.attendance_day1_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day2_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day2_code select c).Single();
                                simsobj.attendance_day2_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day3_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day3_code select c).Single();
                                simsobj.attendance_day3_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day4_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day4_code select c).Single();
                                simsobj.attendance_day4_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day5_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day5_code select c).Single();
                                simsobj.attendance_day5_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day6_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day6_code select c).Single();
                                simsobj.attendance_day6_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day7_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day7_code select c).Single();
                                simsobj.attendance_day7_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day8_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day8_code select c).Single();
                                simsobj.attendance_day8_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day9_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day9_code select c).Single();
                                simsobj.attendance_day9_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day10_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day10_code select c).Single();
                                simsobj.attendance_day10_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day11_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day11_code select c).Single();
                                simsobj.attendance_day11_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day12_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day12_code select c).Single();
                                simsobj.attendance_day12_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day13_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day13_code select c).Single();
                                simsobj.attendance_day13_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day14_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day14_code select c).Single();
                                simsobj.attendance_day14_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day15_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day15_code select c).Single();
                                simsobj.attendance_day15_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day16_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day16_code select c).Single();
                                simsobj.attendance_day16_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day17_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day17_code select c).Single();
                                simsobj.attendance_day17_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day18_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day18_code select c).Single();
                                simsobj.attendance_day18_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day19_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day19_code select c).Single();
                                simsobj.attendance_day19_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day20_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day20_code select c).Single();
                                simsobj.attendance_day20_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day21_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day21_code select c).Single();
                                simsobj.attendance_day21_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day22_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day22_code select c).Single();
                                simsobj.attendance_day22_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day23_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day23_code select c).Single();
                                simsobj.attendance_day23_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day24_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day24_code select c).Single();
                                simsobj.attendance_day24_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day25_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day25_code select c).Single();
                                simsobj.attendance_day25_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day26_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day26_code select c).Single();
                                simsobj.attendance_day26_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day27_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day27_code select c).Single();
                                simsobj.attendance_day27_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day28_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day28_code select c).Single();
                                simsobj.attendance_day28_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day29_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day29_code select c).Single();
                                simsobj.attendance_day29_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day30_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day30_code select c).Single();
                                simsobj.attendance_day30_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day31_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day31_code select c).Single();
                                simsobj.attendance_day31_color_code = obj.sims_attendance_color;
                            }

                            list.Add(simsobj);
                        }
                    }

                    // int count = list.Count(item => item.attendance_day1_code.Equals("P"));
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        //To Update All Day for a single enroll and single month
        [Route("markStudentAttendanceForMonth")]
        public HttpResponseMessage markStudentAttendanceForMonth(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                            new SqlParameter("@enrollList", obj.sims_enroll_number),
                            new SqlParameter("@att_code", obj.sims_attendance_code),
                            new SqlParameter("@monthNo", obj.monthYear),
                            new SqlParameter("@year", obj.Yearno),
                            new SqlParameter("@user_name",obj.Current_Username),
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("markAllStudentAttendanceDayWise")]
        public HttpResponseMessage markAllStudentAttendanceDayWise(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                           // new SqlParameter("@enrollList", obj.sims_enroll_number),
                            new SqlParameter("@att_code", obj.sims_attendance_code),
                            new SqlParameter("@att_date", obj.attednacedate),
                            new SqlParameter("@user_name",obj.Current_Username),
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        //use in Student Attendance form for mark all student attendance day and section wise
        [Route("markAllStudAttendDay")]
        public HttpResponseMessage markAllStudAttendDay(string cur_code, string ayear, string grade, string section, string att_code, string attednacedate)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MB"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@sims_attendance_code", att_code),
                            new SqlParameter("@bell_attendance_date", attednacedate)
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("getStudentAttendanceNew")]
        public HttpResponseMessage getStudentAttendanceNew(string cur_code, string ayear, string grade, string section, string attednacedate)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SN"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@bell_attendance_date", attednacedate),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.sims_student_fname = dr["FirstName"].ToString();
                            simsobj.sims_student_nickname = dr["Nickname"].ToString();
                            simsobj.sims_student_img = dr["studentImg"].ToString();
                            simsobj.sims_student_enroll = dr["sims_enroll_number"].ToString();
                            simsobj.sims_attendance_code = dr["AttendanceCode"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                            }
                            simsobj.sims_attendance_date = dr["sims_attendance_date"].ToString();
                            simsobj.sims_student_bday = dr["StudentBday"].ToString().Equals("Y") ? true : false;
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("markStudentAttendanceNew")]
        public HttpResponseMessage markStudentAttendanceNew(string cur_code, string ayear, string grade, string section, string enrollNumber, string att_code, string attednacedate)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SD"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@enrollNo", enrollNumber),
                            new SqlParameter("@sims_attendance_code", att_code),
                            new SqlParameter("@bell_attendance_date", attednacedate)

                         });
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("markStudentAttendanceALLNew")]
        public HttpResponseMessage markStudentAttendanceALLNew(string cur_code, string ayear, string grade, string section, string enrollNumber, string att_code, string attednacedate)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MP"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", grade),
                            new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@enrollNo", enrollNumber),
                            new SqlParameter("@sims_attendance_code", att_code),
                            new SqlParameter("@bell_attendance_date", attednacedate)

                         });
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("markAllStudentAttendanceNew")]
        public HttpResponseMessage markAllStudentAttendanceNew(List<studentmonthlyattendance> listobj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (studentmonthlyattendance obj in listobj)
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SD"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@bell_academic_year", obj.sims_academic_year),
                            new SqlParameter("@bell_grade_code", obj.sims_grade_code),
                            new SqlParameter("@bell_section_code", obj.sims_section_code),
                            new SqlParameter("@enrollNo", obj.sims_enroll_number),
                            new SqlParameter("@sims_attendance_code", obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date", obj.attednacedate)

                         });

                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        dr.Close();
                    }

                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("getMonthlyAttendanceNew")]
        public HttpResponseMessage getMonthlyAttendanceNew(string cur_code, string ayear, string grade, string section, string month, string year)
        {

            int d = (int)System.DateTime.Now.DayOfWeek;

            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            List<studentattendance> codes = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            studentattendance simsobj1 = new studentattendance();
                            simsobj1.sims_attendance_code = dr1["sims_attendance_code"].ToString();
                            simsobj1.sims_attendance_code_sort_name = dr1["sims_attendance_short_desc"].ToString();
                            simsobj1.sims_attendance_code_name = dr1["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr1["colorCode"].ToString()))
                            {
                                if (dr1["colorCode"].ToString().Length != 7)
                                    simsobj1.sims_attendance_color = string.Format("#{0}", dr1["colorCode"].ToString().Substring(3));
                                else
                                    simsobj1.sims_attendance_color = dr1["colorCode"].ToString();

                            }
                            codes.Add(simsobj1);
                        }
                    }
                    dr1.Close();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", section),
                            new SqlParameter("@monthNo", month),
                            new SqlParameter("@year", year),
                         });

                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.Yearno = dr["Yearno"].ToString();
                            simsobj.monthYear = dr["monthYear"].ToString();
                            //simsobj.attendace_date = dr["dt"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enrollment_number"].ToString();
                            simsobj.Transport = dr["Transport"].ToString();
                            simsobj.BusNo = dr["BusNo"].ToString();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.FirstName = dr["FirstName"].ToString();
                            simsobj.NickName = dr["NickName"].ToString();
                            try
                            {
                                if (dr["t"].ToString().Equals("0") || dr["t"].ToString().Equals("I"))
                                    simsobj.t = true;
                                else
                                    simsobj.t = false;
                            }
                            catch (Exception ex) { simsobj.t = false; }
                            simsobj.attendance_day1_code = dr["1"].ToString();
                            simsobj.attendance_day2_code = dr["2"].ToString();
                            simsobj.attendance_day3_code = dr["3"].ToString();
                            simsobj.attendance_day4_code = dr["4"].ToString();
                            simsobj.attendance_day5_code = dr["5"].ToString();
                            simsobj.attendance_day6_code = dr["6"].ToString();
                            simsobj.attendance_day7_code = dr["7"].ToString();
                            simsobj.attendance_day8_code = dr["8"].ToString();
                            simsobj.attendance_day9_code = dr["9"].ToString();
                            simsobj.attendance_day10_code = dr["10"].ToString();
                            simsobj.attendance_day11_code = dr["11"].ToString();
                            simsobj.attendance_day12_code = dr["12"].ToString();
                            simsobj.attendance_day13_code = dr["13"].ToString();
                            simsobj.attendance_day14_code = dr["14"].ToString();
                            simsobj.attendance_day15_code = dr["15"].ToString();
                            simsobj.attendance_day16_code = dr["16"].ToString();
                            simsobj.attendance_day17_code = dr["17"].ToString();
                            simsobj.attendance_day18_code = dr["18"].ToString();
                            simsobj.attendance_day19_code = dr["19"].ToString();
                            simsobj.attendance_day20_code = dr["20"].ToString();
                            simsobj.attendance_day21_code = dr["21"].ToString();
                            simsobj.attendance_day22_code = dr["22"].ToString();
                            simsobj.attendance_day23_code = dr["23"].ToString();
                            simsobj.attendance_day24_code = dr["24"].ToString();
                            simsobj.attendance_day25_code = dr["25"].ToString();
                            simsobj.attendance_day26_code = dr["26"].ToString();
                            simsobj.attendance_day27_code = dr["27"].ToString();
                            simsobj.attendance_day28_code = dr["28"].ToString();
                            simsobj.attendance_day29_code = dr["29"].ToString();
                            simsobj.attendance_day30_code = dr["30"].ToString();
                            simsobj.attendance_day31_code = dr["31"].ToString();





                            if (!string.IsNullOrEmpty(simsobj.attendance_day1_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day1_code select c).Single();
                                simsobj.attendance_day1_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day2_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day2_code select c).Single();
                                simsobj.attendance_day2_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day3_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day3_code select c).Single();
                                simsobj.attendance_day3_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day4_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day4_code select c).Single();
                                simsobj.attendance_day4_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day5_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day5_code select c).Single();
                                simsobj.attendance_day5_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day6_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day6_code select c).Single();
                                simsobj.attendance_day6_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day7_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day7_code select c).Single();
                                simsobj.attendance_day7_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day8_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day8_code select c).Single();
                                simsobj.attendance_day8_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day9_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day9_code select c).Single();
                                simsobj.attendance_day9_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day10_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day10_code select c).Single();
                                simsobj.attendance_day10_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day11_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day11_code select c).Single();
                                simsobj.attendance_day11_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day12_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day12_code select c).Single();
                                simsobj.attendance_day12_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day13_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day13_code select c).Single();
                                simsobj.attendance_day13_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day14_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day14_code select c).Single();
                                simsobj.attendance_day14_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day15_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day15_code select c).Single();
                                simsobj.attendance_day15_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day16_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day16_code select c).Single();
                                simsobj.attendance_day16_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day17_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day17_code select c).Single();
                                simsobj.attendance_day17_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day18_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day18_code select c).Single();
                                simsobj.attendance_day18_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day19_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day19_code select c).Single();
                                simsobj.attendance_day19_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day20_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day20_code select c).Single();
                                simsobj.attendance_day20_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day21_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day21_code select c).Single();
                                simsobj.attendance_day21_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day22_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day22_code select c).Single();
                                simsobj.attendance_day22_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day23_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day23_code select c).Single();
                                simsobj.attendance_day23_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day24_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day24_code select c).Single();
                                simsobj.attendance_day24_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day25_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day25_code select c).Single();
                                simsobj.attendance_day25_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day26_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day26_code select c).Single();
                                simsobj.attendance_day26_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day27_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day27_code select c).Single();
                                simsobj.attendance_day27_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day28_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day28_code select c).Single();
                                simsobj.attendance_day28_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day29_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day29_code select c).Single();
                                simsobj.attendance_day29_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day30_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day30_code select c).Single();
                                simsobj.attendance_day30_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day31_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day31_code select c).Single();
                                simsobj.attendance_day31_color_code = obj.sims_attendance_color;
                            }

                            list.Add(simsobj);
                        }
                    }

                    // int count = list.Count(item => item.attendance_day1_code.Equals("P"));
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("markStudentAttendanceForMonthNew")]
        public HttpResponseMessage markStudentAttendanceForMonthNew(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MM"),
                         
                            new SqlParameter("@monthNo", obj.monthYear),
                            new SqlParameter("@year", obj.Yearno),
                           

                              new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@bell_academic_year", obj.sims_academic_year),
                            new SqlParameter("@bell_grade_code", obj.sims_grade_code),
                            new SqlParameter("@bell_section_code",  obj.sims_section_code),
                            new SqlParameter("@enrollNo", obj.sims_enroll_number),
                            new SqlParameter("@sims_attendance_code",obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date",  obj.attednacedate)

                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("markAllStudentAttendanceDayWiseNew")]
        public HttpResponseMessage markAllStudentAttendanceDayWiseNew(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            
                             new SqlParameter("@opr", "MP"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@bell_academic_year", obj.sims_academic_year),
                            new SqlParameter("@bell_grade_code", obj.sims_grade_code),
                            new SqlParameter("@bell_section_code",  obj.sims_section_code),
                          //  new SqlParameter("@enrollNo", enrollNumber),
                            new SqlParameter("@sims_attendance_code",obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date",  obj.attednacedate)
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("getUserRole")]
        public HttpResponseMessage getUserRole(string username, string sims_grade_code, string sims_section_code)
        {
            List<studentmonthlyattendance> mod_list = new List<studentmonthlyattendance>();
            try
            {
                if (sims_grade_code == "undefined" || sims_grade_code == "")
                    sims_grade_code = null;
                if (sims_section_code == "undefined" || sims_section_code == "")
                    sims_section_code = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","SN"),                        
                        new SqlParameter("@username",username),
                        new SqlParameter("@user_name",username),
                        new SqlParameter("@sims_grade_code",sims_grade_code),
                        new SqlParameter("@sims_section_code",sims_section_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.user_status = dr["user_status"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getUserAttendanceTimeStatus")]
        public HttpResponseMessage getUserAttendanceTimeStatus()
        {
            List<studentmonthlyattendance> mod_list = new List<studentmonthlyattendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","SO")                        
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.user_status = dr["status"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getAttendanceCount")]
        public HttpResponseMessage getAttendanceCount(string cur)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendence_detail_by_condition",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code",cur),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_category_code = dr["sims_category_code"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("getAttendanceCount_details")]
        public HttpResponseMessage getAttendanceCount_details(string cur, string academic_year, string grade, string section, string enroll, string count)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendence_detail_by_condition",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code",cur),
                            new SqlParameter("@sims_academic_year",academic_year),
                            new SqlParameter("@sims_grade_code",grade),
                            new SqlParameter("@sims_section_code",section),
                            new SqlParameter("@sims_enroll_number",enroll),
                            new SqlParameter("@sims_attendence_count",count),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_from_date = dr["sims_from_date"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_AttendanceComment")]
        public HttpResponseMessage get_AttendanceComment(string enroll, string date)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                            new SqlParameter("@enrollList", enroll),
                            new SqlParameter("@att_date", db.DBYYYYMMDDformat(date)),
                           
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_day_comment = dr["sims_attendance_day_comment"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("markAttendanceComment_update")]
        public HttpResponseMessage markAttendanceComment_update(string enroll, string date, string comment)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", "C"),
                            
                            new SqlParameter("@enrollList",enroll),
                            new SqlParameter("@att_date",db.DBYYYYMMDDformat(date)),
                            new SqlParameter("@comment", comment),

                           
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancerought")]
        public HttpResponseMessage getAttendancerought(string academic_year)
        {
            List<studentattendancerought> list = new List<studentattendancerought>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                            //new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@bell_academic_year", academic_year)
                            //new SqlParameter("@bell_grade_code", gradeCode),
                            //new SqlParameter("@user_name", userName)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought simsobj = new studentattendancerought();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }






        //New/////Bus//////Attendance///////////////////////////////////////////

        [Route("getRoughtInOut")]
        public HttpResponseMessage getRoughtInOut()
        {
            List<studentattendancerought> list = new List<studentattendancerought>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "J"),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought simsobj = new studentattendancerought();
                            simsobj.sims_rought_parameter = dr["sims_rought_parameter"].ToString();
                            simsobj.sims_rought_field_value1 = dr["sims_rought_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getRoughtPickDrop")]
        public HttpResponseMessage getRoughtPickDrop()
        {
            List<studentattendancerought> list = new List<studentattendancerought>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            //new SqlParameter("@pd_str", str),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought simsobj = new studentattendancerought();
                            simsobj.sims_transport_parameter = dr["sims_transport_parameter"].ToString();
                            simsobj.sims_transport_field_value1 = dr["sims_transport_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAttendancedate")]
        public HttpResponseMessage getAttendancedate()
        {
            List<studentattendancerought> list = new List<studentattendancerought>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bus_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "CD"),
                            //new SqlParameter("@pd_str", str),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought simsobj = new studentattendancerought();
                            simsobj.currentdate = dr["currentdate"].ToString();

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("getBusRoute")]
        public HttpResponseMessage getBusRoute(string caretaker_code)
        {

            List<studentattendancerought> lst = new List<studentattendancerought>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bus_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@caretaker_code", caretaker_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought obj = new studentattendancerought();
                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_route_vehicle_code = dr["sims_transport_route_vehicle_code"].ToString();
                            obj.sims_transport_route_driver_code = dr["sims_transport_route_driver_code"].ToString();
                            obj.sims_transport_route_caretaker_code = dr["sims_transport_route_caretaker_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("getNewBusStudent")]
        public HttpResponseMessage getNewBusStudent(string routecode, string attdate, string daycode, string picdrop)
        {

            List<studentattendancerought> lst = new List<studentattendancerought>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bus_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "SS"),
                             new SqlParameter("@route_code", routecode),
                             new SqlParameter("@attdate",db.DBYYYYMMDDformat(attdate)),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought obj = new studentattendancerought();
                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_date = dr["sims_transport_date"].ToString();
                            obj.sims_transport_enrollment_number = dr["sims_transport_enrollment_number"].ToString();
                            obj.sims_transport_studentname = dr["StudentName"].ToString();
                            obj.sims_transport_firstname = dr["FirstName"].ToString();
                            obj.sims_transport_middlename = dr["MiddleName"].ToString();
                            obj.sims_transport_lastname = dr["LastName"].ToString();
                            obj.sims_emergency_contact_number = dr["sims_student_primary_contact_pref"].ToString();
                            obj.sims_student_img = dr["sims_student_img"].ToString();
                            if (daycode == "AM")
                            {
                                if (picdrop == "P")
                                {
                                    obj.sims_attendance_pickup_drop_time = dr["sims_attendance_am_in_pickup_time"].ToString();
                                    obj.attendance_mark_code = dr["sims_attendance_am_pickup"].ToString();
                                    obj.attendance_day1_color_code = dr["sims_attendance_am_pickup_color"].ToString();

                                }
                                else if (picdrop == "D")
                                {
                                    obj.sims_attendance_pickup_drop_time = dr["sims_attendance_am_in_drop_time"].ToString();
                                    obj.attendance_mark_code = dr["sims_attendance_am_drop"].ToString();
                                    obj.attendance_day1_color_code = dr["sims_attendance_am_drop_color"].ToString();

                                }
                            }
                            if (daycode == "PM")
                            {
                                if (picdrop == "P")
                                {
                                    obj.sims_attendance_pickup_drop_time = dr["sims_attendance_pm_out_pickup_time"].ToString();
                                    obj.attendance_mark_code = dr["sims_attendance_pm_pickup"].ToString();
                                    obj.attendance_day1_color_code = dr["sims_attendance_pm_pickup_color"].ToString();

                                }
                                else if (picdrop == "D")
                                {
                                    obj.sims_attendance_pickup_drop_time = dr["sims_attendance_pm_out_drop_time"].ToString();
                                    obj.attendance_mark_code = dr["sims_attendance_pm_drop"].ToString();
                                    obj.attendance_day1_color_code = dr["sims_attendance_pm_drop_color"].ToString();

                                }
                            }
                            obj.sims_attendance_am_pickup_time_flag = dr["sims_attendance_am_pickup_time_flag"].ToString();
                            obj.sims_attendance_am_drop_time_flag = dr["sims_attendance_am_drop_time_flag"].ToString();
                            obj.sims_attendance_pm_pickup_time_flag = dr["sims_attendance_pm_pickup_time_flag"].ToString();
                            obj.sims_attendance_pm_drop_time_flag = dr["sims_attendance_pm_drop_time_flag"].ToString();
                            
                            obj.sims_attendance_remark = dr["sims_attendance_remark"].ToString();
                            
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            lst.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }



        [Route("markNewBusStudentAttendance123")]
        public HttpResponseMessage markNewBusStudentAttendance123(List<studentattendancerought> data)
        {
            bool result = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (studentattendancerought obj in data)
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", obj.opr),
                            new SqlParameter("@sopr",obj.subopr),
                            new SqlParameter("@ssopr",obj.ssubopr),
                            new SqlParameter("@academic_year", obj.ayear),
                            new SqlParameter("@sims_transport_enrollment_number", obj.sims_transport_enrollment_number),
                            new SqlParameter("@attdate", db.DBYYYYMMDDformat(obj.sims_transport_date)),
                            new SqlParameter("@route_code", obj.sims_transport_route_code),
                            
                            new SqlParameter("@sims_attendance_pickup_drop_time", obj.sims_attendance_pickup_drop_time),
                             
                            new SqlParameter("@caretaker_code",obj.caretaker_code),
                         });

                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        dr.Close();
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }



        //insert and update
        [Route("markNewBusStudentAttendance")]
        public HttpResponseMessage markNewBusStudentAttendance(List<studentattendancerought> data)
        {
            Message message = new Message();
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (studentattendancerought obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_bus_attendance_proc]",
                        new List<SqlParameter>()
                     {
                            new SqlParameter("@opr", obj.opr),
                            new SqlParameter("@sopr",obj.subopr),
                            new SqlParameter("@ssopr",obj.ssubopr),
                            new SqlParameter("@academic_year", obj.ayear),
                            new SqlParameter("@sims_transport_enrollment_number", obj.sims_transport_enrollment_number),
                            new SqlParameter("@attdate",  db.DBYYYYMMDDformat(obj.sims_transport_date)),
                            new SqlParameter("@route_code", obj.sims_transport_route_code),
                            
                            new SqlParameter("@sims_attendance_remark", obj.sims_attendance_remark),
                            new SqlParameter("@sims_attendance_am", obj.attendance_mark_code),
                            new SqlParameter("@sims_attendance_pm", obj.attendance_mark_code),
                             
                            new SqlParameter("@caretaker_code",obj.caretaker_code),
                     });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                        a = 1;
                    }





                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                //message.strMessage = x.Message;
                //message.systemMessage = string.Empty;
                //message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getAttendanceBusCode")]
        public HttpResponseMessage getAttendanceBusCode(string cur_code)
        {
            List<studentattendancerought> list = new List<studentattendancerought>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_bus_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendancerought simsobj = new studentattendancerought();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_code_sort_name = dr["sims_attendance_short_desc"].ToString();
                            simsobj.sims_attendance_code_name = dr["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                if (dr["colorCode"].ToString().Length != 7)
                                    simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                                else
                                    simsobj.sims_attendance_color = dr["colorCode"].ToString();

                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
    }
}