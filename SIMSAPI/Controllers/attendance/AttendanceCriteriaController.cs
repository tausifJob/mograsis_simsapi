﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Attendance")]
    public class Sim541Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        [Route("getAttendanceCriteria")]
        public HttpResponseMessage getAttendanceCriteria(string cur_code,string academicYear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Attendance", "ATTENDANCECRITERIA"));

            List<Sims541> State_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_attendance_criteria",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academicYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_attendance_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_attendance_day_wise_flag = dr["sims_attendance_day_wise_flag"].ToString().Equals("A");
                            simsobj.sims_attendance_slot_wise_flag = dr["sims_attendance_slot_wise_flag"].ToString().Equals("A");
                            simsobj.sims_attendance_am_pm_wise_flag = dr["sims_attendance_am_pm_wise_flag"].ToString().Equals("A");
                            State_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, State_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, State_list);
        }

        [Route("getAttendanceCriteriaByIndex")]
        public HttpResponseMessage getAttendanceCriteriaByIndex(string cur_code, string academicYear, int pageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Attendance", "ATTENDANCECRITERIA"));

            List<Sims541> State_list = new List<Sims541>();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_attendance_criteria",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academicYear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_attendance_cur_code = dr["sims_attendance_cur_code"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_attendance_day_wise_flag = dr["sims_attendance_day_wise_flag"].ToString().Equals("A");
                            simsobj.sims_attendance_slot_wise_flag = dr["sims_attendance_slot_wise_flag"].ToString().Equals("A");
                            simsobj.sims_attendance_am_pm_wise_flag = dr["sims_attendance_am_pm_wise_flag"].ToString().Equals("A");
                            State_list.Add(simsobj);
                            total = State_list.Count;
                            skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, State_list.Skip(skip).Take(PageSize).ToList());
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, State_list);

            }
           // return Request.CreateResponse(HttpStatusCode.OK, State_list);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ATTENDANCE", "ATTENDANCECRITERIA"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail_proc]",
                        new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", 'C'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_attendance_cur_code = dr["sims_cur_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum(),PARAMETERS :: curCode{2}";
            Log.Debug(string.Format(debug, "ATTENDANCE", "ATTENDANCECRITERIA", curCode));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_detail]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@OPR", 'Y'),
                             new SqlParameter("@CURR_CODE",curCode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }
    }
}