﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;

using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.AttendanceClass;


namespace SIMSAPI.Controllers.Finance
{
    [RoutePrefix("api/attendanceMachine")]
    public class AttendanceMachineController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Attendance Machine


        [Route("getAttendaceMachineDetails")]
        public HttpResponseMessage getAttendaceMachineDetails(string sdate, string edate, string punchin, string punchout,string dept_code, string desg_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendaceMachineDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "EMPLOYEE", "getAttendaceMachineDetails"));

            List<att_data> list = new List<att_data>();
            if (dept_code == "undefined" || dept_code == "")
                dept_code = null;
            if (desg_code == "undefined" || desg_code == "")
                desg_code = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[attendance_machine_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sdate",db.DBYYYYMMDDformat(sdate)),
                              new SqlParameter("@edate",db.DBYYYYMMDDformat(edate)),
                              new SqlParameter("@in",punchin),
                              new SqlParameter("@out",punchout),
                              new SqlParameter("@dept_code",dept_code),
                              new SqlParameter("@desg_code",desg_code)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            att_data objNew = new att_data();
                            objNew.att_emp_id = dr["att_emp_id"].ToString();
                            objNew.empname = dr["empname"].ToString();
                            objNew.att_date = db.UIDDMMYYYYformat(dr["att_date"].ToString());
                            objNew.att_shift1_in = dr["Check_IN"].ToString();
                            objNew.att_shift1_out = dr["Check_OUT"].ToString();
                            try
                            {
                                objNew.em_login_code = dr["em_login_code"].ToString();
                                objNew.att_push_flag = dr["att_push_flag"].ToString();
                            }
                            catch(Exception) { }
                            
                            list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("CUDAttendaceMachine")]
        public HttpResponseMessage CUDAttendaceMachine(List<att_data> listatt)
        {
            bool res = false;
            Message message = new Message();
            try
            {
                if (listatt != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (att_data obj in listatt)
                        {
                            SqlDataReader dr;
                            dr = db.ExecuteStoreProcedure("[attendance_machine_proc]",
                              new List<SqlParameter>() 
                             {                              
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@att_emp_id", obj.att_emp_id),
                                new SqlParameter("@att_date", db.DBYYYYMMDDformat(obj.att_date)),
                                new SqlParameter("@att_shift1_in", obj.att_shift1_in),
                                new SqlParameter("@att_shift1_out", obj.att_shift1_out),
                                new SqlParameter("@sdate", db.DBYYYYMMDDformat(obj.att_sdate)),
                                new SqlParameter("@edate", db.DBYYYYMMDDformat(obj.att_edate)),                                                             
                           });
                            if (dr.RecordsAffected > 0)
                            {
                                res = true;
                            }
                            else
                            {
                                res = false;
                            }
                            dr.Close();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, res);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            catch (Exception x)
            {
               // res = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion
    }
}