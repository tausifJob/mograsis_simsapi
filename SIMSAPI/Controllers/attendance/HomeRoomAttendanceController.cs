﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;

using System.Globalization;
using SIMSAPI.Models.ERP.AttendanceClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/HomeRoomAttendance")]    
    public class HomeRoomAttendanceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getAttendanceCuriculum")]
        public HttpResponseMessage getAttendanceCuriculum()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_short_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendanceyear")]
        public HttpResponseMessage getAttendanceyear(string cur_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_academic_year",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        //[Route("getAttendancegrade")]
        //public HttpResponseMessage getAttendancegrade(string cur_code, string academic_year, string userName)
        //{
        //    List<studentattendance> list = new List<studentattendance>();
        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", "G"),
        //                    new SqlParameter("@sims_cur_code", cur_code),
        //                    new SqlParameter("@bell_academic_year", academic_year),
        //                    new SqlParameter("@user_name", userName)
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    studentattendance simsobj = new studentattendance();
        //                    simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
        //                    simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
        //                    list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, list);
        //}

        //[Route("getAttendancesection")]
        //public HttpResponseMessage getAttendancesection(string cur_code, string academic_year, string gradeCode, string userName)
        //{
        //    List<studentattendance> list = new List<studentattendance>();
        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", "T"),
        //                    new SqlParameter("@sims_cur_code", cur_code),
        //                    new SqlParameter("@bell_academic_year", academic_year),
        //                    new SqlParameter("@bell_grade_code", gradeCode),
        //                    new SqlParameter("@user_name", userName)
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    studentattendance simsobj = new studentattendance();
        //                    simsobj.sims_section_code = dr["sims_section_code"].ToString();
        //                    simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
        //                    list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, list);
        //}

        [Route("getTeacherSubject")]
        public HttpResponseMessage getTeacherSubject(string academic_year, string username,string homeroom_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_homeroom_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            //new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@a_year", academic_year),
                            new SqlParameter("@teacher_code", username),
                            new SqlParameter("@homeromm_code", homeroom_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_bell_subject_code = dr["sims_bell_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            //simsobj.sims_batch_code = dr["sims_batch_code"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getHomeRoomBatch")]
        public HttpResponseMessage getHomeRoomBatch(string cur_code, string academic_year, string username)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_homeroom_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@cur_code", cur_code),
                            new SqlParameter("@a_year", academic_year),
                            new SqlParameter("@teacher_code", username)
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.homeroombatch = dr["HomeRoomBatch"].ToString();
                            simsobj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            simsobj.sims_batch_code = dr["sims_batch_code"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancetype")]
        public HttpResponseMessage getAttendancetype()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Attendance Type' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_attendance_type_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAttendancesubtype")]
        public HttpResponseMessage getAttendancesubtype()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Attendance Sub Type' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_type_details_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_attendance_type_details_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAttendancesort")]
        public HttpResponseMessage getAttendancesort()
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_form_field_value2,sims_appl_form_field_value1"),
                            new SqlParameter("@tbl_cond", "sims_appl_form_field='Sort Option' AND sims_appl_code='Sim155'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_sort_code = dr["sims_appl_form_field_value2"].ToString();
                            simsobj.sims_attendance_sort_name = dr["sims_appl_form_field_value1"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode(string cur_code)
        {
            List<studentattendance> list = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_code_sort_name = dr["sims_attendance_short_desc"].ToString();
                            simsobj.sims_attendance_code_name = dr["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                // simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                                simsobj.sims_attendance_color = dr["colorCode"].ToString();
                            }

                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getStudentAttendance")]
        public HttpResponseMessage getStudentAttendance(string home_code, string bat_code, string ayear, string attedancedate, string subject, string username)
        {
            List<studentattendance> list = new List<studentattendance>();
            if (subject == "undefined")
            {
                subject = null;
            }
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_homeroom_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "H"),
                            new SqlParameter("@homeromm_code", home_code),
                            new SqlParameter("@batch_code", bat_code),
                            new SqlParameter("@a_year", ayear),                           
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(attedancedate)),
                            new SqlParameter("@sims_bell_subject_code", subject),
                            new SqlParameter("@teacher_code", username),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.sims_student_fname = dr["FirstName"].ToString();
                            simsobj.sims_student_nickname = dr["Nickname"].ToString();
                            simsobj.sims_student_img = dr["studentImg"].ToString();
                            simsobj.sims_student_enroll = dr["sims_enrollment_number"].ToString();
                            simsobj.sims_attendance_code = dr["sims_attendance_day_attendance_code"].ToString();                            
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                if (dr["colorCode"].ToString().Length != 7)
                                    simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                                else
                                    simsobj.sims_attendance_color = dr["colorCode"].ToString();
                            }
                            simsobj.sims_attendance_date = db.UIDDMMYYYYformat(dr["sims_attendance_date"].ToString());
                            //simsobj.sims_student_bday = dr["StudentBday"].ToString().Equals("Y") ? true : false;
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        //HomeRoom Student Attendance
        [Route("markStudentAttendance")]
        public HttpResponseMessage markStudentAttendance(studentmonthlyattendance obj)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_homeroom_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MA"),
                            new SqlParameter("@cur_code", obj.sims_cur_code),
                            new SqlParameter("@a_year", obj.sims_academic_year),
                            //new SqlParameter("@bell_grade_code", grade),
                            //new SqlParameter("@bell_section_code", section),
                            new SqlParameter("@enrollNo", obj.sims_enroll_number),
                            new SqlParameter("@sims_attendance_code", obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(obj.attednacedate))

                         });
                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        //Monthly Attendance

        [Route("markAllStudentAttendance")]
        public HttpResponseMessage markAllStudentAttendance(List<studentmonthlyattendance> listobj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr;
                    foreach (studentmonthlyattendance obj in listobj)
                    {
                        dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "MA"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@bell_academic_year", obj.sims_academic_year),
                            new SqlParameter("@bell_grade_code", obj.sims_grade_code),
                            new SqlParameter("@bell_section_code", obj.sims_section_code),
                            new SqlParameter("@enrollNo", obj.sims_enroll_number),
                            new SqlParameter("@sims_attendance_code", obj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(obj.attednacedate))

                         });

                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;

                        dr.Close();
                    }

                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("getTerms")]
        public HttpResponseMessage getTerms(string cur_code, string ayear)
        {
            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "ST"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_name = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_term_start_date = DateTime.Parse(dr["sims_term_start_date"].ToString()).Year.ToString() + "-" + DateTime.Parse(dr["sims_term_start_date"].ToString()).Month.ToString() + "-" + DateTime.Parse(dr["sims_term_start_date"].ToString()).Day.ToString();
                            simsobj.sims_term_end_date = DateTime.Parse(dr["sims_term_end_date"].ToString()).Year.ToString() + "-" + DateTime.Parse(dr["sims_term_end_date"].ToString()).Month.ToString() + "-" + DateTime.Parse(dr["sims_term_end_date"].ToString()).Day.ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getTermDetails")]
        public HttpResponseMessage getTermDetails(string startDate, string endDate)
        {
            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            try
            {

                DateTime dt1 = DateTime.Parse(startDate);
                DateTime dt2 = DateTime.Parse(endDate);

                while (dt1.AddMonths(1) < dt2)
                {
                    studentmonthlyattendance obj = new studentmonthlyattendance();
                    obj.sims_month_no = dt1.Month.ToString();
                    obj.sims_month_name = string.Format("{0}-{1}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dt1.Month).ToString(), dt1.Year.ToString());
                    list.Add(obj);
                    dt1 = dt1.AddMonths(1);
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        private string Attendance_codes(string attednace_code, string cur_code)
        {
            string color_code = string.Empty;

            List<studentattendance> codes = new List<studentattendance>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            studentattendance simsobj = new studentattendance();
                            simsobj.sims_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_code_sort_name = dr["sims_attendance_short_desc"].ToString();
                            simsobj.sims_attendance_code_name = dr["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["colorCode"].ToString()))
                            {
                                simsobj.sims_attendance_color = string.Format("#{0}", dr["colorCode"].ToString().Substring(3));
                            }
                            codes.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception x) { }


            if (!string.IsNullOrEmpty(attednace_code))
            {
                studentattendance obj = (from c in codes where c.sims_attendance_code == attednace_code select c).Single();
                color_code = obj.sims_attendance_color;
            }
            return color_code;


        }


        [Route("getMonthlyAttendance")]
        public HttpResponseMessage getMonthlyAttendance(string cur_code, string ayear, string grade, string section, string month, string year)
        {

            int d = (int)System.DateTime.Now.DayOfWeek;

            List<studentmonthlyattendance> list = new List<studentmonthlyattendance>();
            List<studentattendance> codes = new List<studentattendance>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_proc]",
                          new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                            new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            studentattendance simsobj1 = new studentattendance();
                            simsobj1.sims_attendance_code = dr1["sims_attendance_code"].ToString();
                            simsobj1.sims_attendance_code_sort_name = dr1["sims_attendance_short_desc"].ToString();
                            simsobj1.sims_attendance_code_name = dr1["sims_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr1["colorCode"].ToString()))
                            {
                                simsobj1.sims_attendance_color = string.Format("#{0}", dr1["colorCode"].ToString().Substring(3));
                            }
                            codes.Add(simsobj1);
                        }
                    }
                    dr1.Close();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", section),
                            new SqlParameter("@monthNo", month),
                            new SqlParameter("@year", year),
                         });

                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            studentmonthlyattendance simsobj = new studentmonthlyattendance();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.Yearno = dr["Yearno"].ToString();
                            simsobj.monthYear = dr["monthYear"].ToString();
                            //simsobj.attendace_date = dr["dt"].ToString();
                            simsobj.sims_enroll_number = dr["sims_enrollment_number"].ToString();
                            simsobj.Transport = dr["Transport"].ToString();
                            simsobj.BusNo = dr["BusNo"].ToString();
                            simsobj.sims_student_name = dr["StudentName"].ToString();
                            simsobj.FirstName = dr["FirstName"].ToString();
                            simsobj.NickName = dr["NickName"].ToString();
                            simsobj.attendance_day1_code = dr["1"].ToString();
                            simsobj.attendance_day2_code = dr["2"].ToString();
                            simsobj.attendance_day3_code = dr["3"].ToString();
                            simsobj.attendance_day4_code = dr["4"].ToString();
                            simsobj.attendance_day5_code = dr["5"].ToString();
                            simsobj.attendance_day6_code = dr["6"].ToString();
                            simsobj.attendance_day7_code = dr["7"].ToString();
                            simsobj.attendance_day8_code = dr["8"].ToString();
                            simsobj.attendance_day9_code = dr["9"].ToString();
                            simsobj.attendance_day10_code = dr["10"].ToString();
                            simsobj.attendance_day11_code = dr["11"].ToString();
                            simsobj.attendance_day12_code = dr["12"].ToString();
                            simsobj.attendance_day13_code = dr["13"].ToString();
                            simsobj.attendance_day14_code = dr["14"].ToString();
                            simsobj.attendance_day15_code = dr["15"].ToString();
                            simsobj.attendance_day16_code = dr["16"].ToString();
                            simsobj.attendance_day17_code = dr["17"].ToString();
                            simsobj.attendance_day18_code = dr["18"].ToString();
                            simsobj.attendance_day19_code = dr["19"].ToString();
                            simsobj.attendance_day20_code = dr["20"].ToString();
                            simsobj.attendance_day21_code = dr["21"].ToString();
                            simsobj.attendance_day22_code = dr["22"].ToString();
                            simsobj.attendance_day23_code = dr["23"].ToString();
                            simsobj.attendance_day24_code = dr["24"].ToString();
                            simsobj.attendance_day25_code = dr["25"].ToString();
                            simsobj.attendance_day26_code = dr["26"].ToString();
                            simsobj.attendance_day27_code = dr["27"].ToString();
                            simsobj.attendance_day28_code = dr["28"].ToString();
                            simsobj.attendance_day29_code = dr["29"].ToString();
                            simsobj.attendance_day30_code = dr["30"].ToString();
                            simsobj.attendance_day31_code = dr["31"].ToString();





                            if (!string.IsNullOrEmpty(simsobj.attendance_day1_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day1_code select c).Single();
                                simsobj.attendance_day1_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day2_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day2_code select c).Single();
                                simsobj.attendance_day2_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day3_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day3_code select c).Single();
                                simsobj.attendance_day3_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day4_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day4_code select c).Single();
                                simsobj.attendance_day4_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day5_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day5_code select c).Single();
                                simsobj.attendance_day5_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day6_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day6_code select c).Single();
                                simsobj.attendance_day6_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day7_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day7_code select c).Single();
                                simsobj.attendance_day7_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day8_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day8_code select c).Single();
                                simsobj.attendance_day8_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day9_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day9_code select c).Single();
                                simsobj.attendance_day9_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day10_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day10_code select c).Single();
                                simsobj.attendance_day10_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day11_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day11_code select c).Single();
                                simsobj.attendance_day11_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day12_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day12_code select c).Single();
                                simsobj.attendance_day12_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day13_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day13_code select c).Single();
                                simsobj.attendance_day13_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day14_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day14_code select c).Single();
                                simsobj.attendance_day14_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day15_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day15_code select c).Single();
                                simsobj.attendance_day15_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day16_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day16_code select c).Single();
                                simsobj.attendance_day16_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day17_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day17_code select c).Single();
                                simsobj.attendance_day17_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day18_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day18_code select c).Single();
                                simsobj.attendance_day18_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day19_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day19_code select c).Single();
                                simsobj.attendance_day19_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day20_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day20_code select c).Single();
                                simsobj.attendance_day20_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day21_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day21_code select c).Single();
                                simsobj.attendance_day21_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day22_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day22_code select c).Single();
                                simsobj.attendance_day22_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day23_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day23_code select c).Single();
                                simsobj.attendance_day23_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day24_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day24_code select c).Single();
                                simsobj.attendance_day24_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day25_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day25_code select c).Single();
                                simsobj.attendance_day25_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day26_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day26_code select c).Single();
                                simsobj.attendance_day26_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day27_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day27_code select c).Single();
                                simsobj.attendance_day27_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day28_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day28_code select c).Single();
                                simsobj.attendance_day28_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day29_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day29_code select c).Single();
                                simsobj.attendance_day29_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day30_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day30_code select c).Single();
                                simsobj.attendance_day30_color_code = obj.sims_attendance_color;
                            }
                            if (!string.IsNullOrEmpty(simsobj.attendance_day31_code))
                            {
                                studentattendance obj = (from c in codes where c.sims_attendance_code == simsobj.attendance_day31_code select c).Single();
                                simsobj.attendance_day31_color_code = obj.sims_attendance_color;
                            }

                            list.Add(simsobj);
                        }
                    }

                    // int count = list.Count(item => item.attendance_day1_code.Equals("P"));
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        //To Update All Day for a single enroll and single month
        [Route("markStudentAttendanceForMonth")]
        public HttpResponseMessage markStudentAttendanceForMonth(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                            new SqlParameter("@enrollList", obj.sims_enroll_number),
                            new SqlParameter("@att_code", obj.sims_attendance_code),
                            new SqlParameter("@monthNo", obj.monthYear),
                            new SqlParameter("@year", obj.Yearno),
                            new SqlParameter("@user_name",obj.Current_Username),
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("markAllStudentAttendanceDayWise")]
        public HttpResponseMessage markAllStudentAttendanceDayWise(studentmonthlyattendance obj)
        {
            bool result = false;
            //List<studentmonthlyattendance> listobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<studentmonthlyattendance>>(listdata);

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_attendance_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                           // new SqlParameter("@enrollList", obj.sims_enroll_number),
                            new SqlParameter("@att_code", obj.sims_attendance_code),
                            new SqlParameter("@att_date", db.DBYYYYMMDDformat(obj.attednacedate)),
                            new SqlParameter("@user_name",obj.Current_Username),
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;

                    dr.Close();
                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        //HomeRoom All Student Attendance
        //use in Student Attendance form for mark all student attendance day and section wise
        [Route("markAllStudAttendDay")]
        public HttpResponseMessage markAllStudAttendDay(studentattendance attobj)
        {
            bool result = false;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_homeroom_attendance_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "HA"),
                            new SqlParameter("@cur_code", attobj.sims_cur_code),
                            new SqlParameter("@a_year", attobj.sims_academic_year),
                            new SqlParameter("@homeromm_code", attobj.sims_homeroom_code),
                            new SqlParameter("@batch_code", attobj.sims_batch_code),
                            new SqlParameter("@sims_attendance_code", attobj.sims_attendance_code),
                            new SqlParameter("@bell_attendance_date", db.DBYYYYMMDDformat(attobj.attednacedate))
                         });

                    int r = dr.RecordsAffected;
                    result = r > 0 ? true : false;
                }
            }

            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}