﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.messageClass;

using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Newtonsoft.Json;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/emp/attendance")]
    [BasicAuthentication]
    public class EmployeeAttendanceMonthlyController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllEmployeeAttendance")]
        public HttpResponseMessage getAllEmployeeAttendance(string comp_code, string dept_code, string grade_code, string stafftype_code, string designation_code, string att_year, string month_no, string employee_no)
        {

            List<Per313> list = new List<Per313>();
            try
            {
                if (dept_code == "undefined")
                {
                    dept_code = null;
                }
                if (grade_code == "undefined")
                {
                    grade_code = null;
                }
                if (stafftype_code == "undefined")
                {
                    stafftype_code = null;
                }
                if (designation_code == "undefined")
                {
                    designation_code = null;
                }
                if (employee_no == "undefined")
                {
                    employee_no = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code", comp_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@stafftype_code", stafftype_code),
                            new SqlParameter("@designation_code", designation_code),
                            new SqlParameter("@year",att_year),
                            new SqlParameter("@monthNo",month_no),
                            new SqlParameter("@emp_no",employee_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_employee_name = dr["EmployeeName"].ToString();
                            simsobj.att_emp_id = dr["att_emp_id"].ToString();
                            simsobj.Yearno = dr["Yearno"].ToString();
                            simsobj.monthYear = dr["monthYear"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.em_dept_code = dr["em_dept_code"].ToString();
                            simsobj.em_grade_code = dr["em_grade_code"].ToString();
                            simsobj.em_staff_type = dr["em_staff_type"].ToString();
                            simsobj.em = dr["em"].ToString();
                            simsobj.em_desg_code = dr["em_desg_code"].ToString();
                            simsobj.pe_status = dr["pe_status"].ToString();

                            simsobj.pays_attendance_day_1_att_code = dr["1"].ToString();
                            simsobj.pays_attendance_day_2_att_code = dr["2"].ToString();
                            simsobj.pays_attendance_day_3_att_code = dr["3"].ToString();
                            simsobj.pays_attendance_day_4_att_code = dr["4"].ToString();
                            simsobj.pays_attendance_day_5_att_code = dr["5"].ToString();
                            simsobj.pays_attendance_day_6_att_code = dr["6"].ToString();
                            simsobj.pays_attendance_day_7_att_code = dr["7"].ToString();
                            simsobj.pays_attendance_day_8_att_code = dr["8"].ToString();
                            simsobj.pays_attendance_day_9_att_code = dr["9"].ToString();
                            simsobj.pays_attendance_day_10_att_code = dr["10"].ToString();
                            simsobj.pays_attendance_day_11_att_code = dr["11"].ToString();
                            simsobj.pays_attendance_day_12_att_code = dr["12"].ToString();
                            simsobj.pays_attendance_day_13_att_code = dr["13"].ToString();
                            simsobj.pays_attendance_day_14_att_code = dr["14"].ToString();
                            simsobj.pays_attendance_day_15_att_code = dr["15"].ToString();
                            simsobj.pays_attendance_day_16_att_code = dr["16"].ToString();
                            simsobj.pays_attendance_day_17_att_code = dr["17"].ToString();
                            simsobj.pays_attendance_day_18_att_code = dr["18"].ToString();
                            simsobj.pays_attendance_day_19_att_code = dr["19"].ToString();
                            simsobj.pays_attendance_day_20_att_code = dr["20"].ToString();
                            simsobj.pays_attendance_day_21_att_code = dr["21"].ToString();
                            simsobj.pays_attendance_day_22_att_code = dr["22"].ToString();
                            simsobj.pays_attendance_day_23_att_code = dr["23"].ToString();
                            simsobj.pays_attendance_day_24_att_code = dr["24"].ToString();
                            simsobj.pays_attendance_day_25_att_code = dr["25"].ToString();
                            simsobj.pays_attendance_day_26_att_code = dr["26"].ToString();
                            simsobj.pays_attendance_day_27_att_code = dr["27"].ToString();
                            simsobj.pays_attendance_day_28_att_code = dr["28"].ToString();
                            simsobj.pays_attendance_day_29_att_code = dr["29"].ToString();
                            simsobj.pays_attendance_day_30_att_code = dr["30"].ToString();
                            simsobj.pays_attendance_day_31_att_code = dr["31"].ToString();
                            //simsobj.pays_attendance_absent_count = ((simsobj, "A").ToString();
                            //simsobj.pays_attendance_present_count =((simsobj, "P").ToString();
                            //simsobj.pays_attendance_leave_count = (simsobj, "LA").ToString();
                            //simsobj.pays_attendance_CL_count = CountForAttendanceCode(obj, "CL").ToString();
                            //simsobj.pays_attendance_EA_count = CountForAttendanceCode(obj, "EA").ToString();
                            //simsobj.pays_attendance_SL_count = CountForAttendanceCode(obj, "SL").ToString();
                            //int holiday = int.Parse(CountForAttendanceCode(simsobj, "H").ToString());
                            //int Weekend = int.Parse(CountForAttendanceCode(simsobj, "W").ToString());
                            //simsobj.pays_attendance_wd_count = (days - (holiday + Weekend)).ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getAllEmployeeAttendanceRakaag")]
        public HttpResponseMessage getAllEmployeeAttendanceRakaag(string comp_code, string dept_code, string grade_code, string stafftype_code, string designation_code, string att_year, string month_no, string employee_no)
        {

            List<Per313> list = new List<Per313>();
            try
            {
                if (dept_code == "undefined")
                {
                    dept_code = null;
                }
                if (grade_code == "undefined")
                {
                    grade_code = null;
                }
                if (stafftype_code == "undefined")
                {
                    stafftype_code = null;
                }
                if (designation_code == "undefined")
                {
                    designation_code = null;
                }
                if (employee_no == "undefined")
                {
                    employee_no = null;
                }
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_rakaag_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comp_code", comp_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@stafftype_code", stafftype_code),
                            new SqlParameter("@designation_code", designation_code),
                            new SqlParameter("@year",att_year),
                            new SqlParameter("@monthNo",month_no),
                            new SqlParameter("@emp_no",employee_no)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_employee_name = dr["EmployeeName"].ToString();
                            simsobj.att_emp_id = dr["att_emp_id"].ToString();
                            simsobj.Yearno = dr["Yearno"].ToString();
                            simsobj.monthYear = dr["monthYear"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.em_dept_code = dr["em_dept_code"].ToString();
                            simsobj.em_grade_code = dr["em_grade_code"].ToString();
                            simsobj.em_staff_type = dr["em_staff_type"].ToString();
                            simsobj.em = dr["em"].ToString();
                            simsobj.em_desg_code = dr["em_desg_code"].ToString();
                            simsobj.pe_status = dr["pe_status"].ToString();

                            try
                            {
                                string[] atta_1 = dr["1"].ToString().Split('/');
                                simsobj.pays_attendance_day_1_att_code = atta_1[0];
                                simsobj.pays_attendance_day_1_att_exp_in = atta_1[1];
                                simsobj.pays_attendance_day_1_att_punch_in = atta_1[2];
                                simsobj.pays_attendance_day_1_att_exp_out = atta_1[3];
                                simsobj.pays_attendance_day_1_att_punch_out = atta_1[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_1_att_code = dr["1"].ToString();
                            }

                            try
                            {
                                string[] atta_2 = dr["2"].ToString().Split('/');
                                simsobj.pays_attendance_day_2_att_code = atta_2[0];
                                simsobj.pays_attendance_day_2_att_exp_in = atta_2[1];
                                simsobj.pays_attendance_day_2_att_punch_in = atta_2[2];
                                simsobj.pays_attendance_day_2_att_exp_out = atta_2[3];
                                simsobj.pays_attendance_day_2_att_punch_out = atta_2[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_2_att_code = dr["2"].ToString();
                            }

                            try
                            {
                                string[] atta_3 = dr["3"].ToString().Split('/');
                                simsobj.pays_attendance_day_3_att_code = atta_3[0];
                                simsobj.pays_attendance_day_3_att_exp_in = atta_3[1];
                                simsobj.pays_attendance_day_3_att_punch_in = atta_3[2];
                                simsobj.pays_attendance_day_3_att_exp_out = atta_3[3];
                                simsobj.pays_attendance_day_3_att_punch_out = atta_3[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_3_att_code = dr["3"].ToString();
                            }


                            try
                            {
                                string[] atta_4 = dr["4"].ToString().Split('/');
                                simsobj.pays_attendance_day_4_att_code = atta_4[0];
                                simsobj.pays_attendance_day_4_att_exp_in = atta_4[1];
                                simsobj.pays_attendance_day_4_att_punch_in = atta_4[2];
                                simsobj.pays_attendance_day_4_att_exp_out = atta_4[3];
                                simsobj.pays_attendance_day_4_att_punch_out = atta_4[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_4_att_code = dr["4"].ToString();
                            }
                            try
                            {
                                string[] atta_5 = dr["5"].ToString().Split('/');
                                simsobj.pays_attendance_day_5_att_code = atta_5[0];
                                simsobj.pays_attendance_day_5_att_exp_in = atta_5[1];
                                simsobj.pays_attendance_day_5_att_punch_in = atta_5[2];
                                simsobj.pays_attendance_day_5_att_exp_out = atta_5[3];
                                simsobj.pays_attendance_day_5_att_punch_out = atta_5[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_5_att_code = dr["5"].ToString();
                            }
                            try
                            {
                                string[] atta_6 = dr["6"].ToString().Split('/');
                                simsobj.pays_attendance_day_6_att_code = atta_6[0];
                                simsobj.pays_attendance_day_6_att_exp_in = atta_6[1];
                                simsobj.pays_attendance_day_6_att_punch_in = atta_6[2];
                                simsobj.pays_attendance_day_6_att_exp_out = atta_6[3];
                                simsobj.pays_attendance_day_6_att_punch_out = atta_6[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_6_att_code = dr["6"].ToString();                                
                            }
                            try
                            {
                                string[] atta_7 = dr["7"].ToString().Split('/');
                                simsobj.pays_attendance_day_7_att_code =   atta_7[0];
                                simsobj.pays_attendance_day_7_att_exp_in = atta_7[1];
                                simsobj.pays_attendance_day_7_att_punch_in = atta_7[2];
                                simsobj.pays_attendance_day_7_att_exp_out = atta_7[3];
                                simsobj.pays_attendance_day_7_att_punch_out = atta_7[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_7_att_code = dr["7"].ToString();
                            }
                            try
                            {
                                string[] atta_8 = dr["8"].ToString().Split('/');
                                simsobj.pays_attendance_day_8_att_code = atta_8[0];
                                simsobj.pays_attendance_day_8_att_exp_in = atta_8[1];
                                simsobj.pays_attendance_day_8_att_punch_in = atta_8[2];
                                simsobj.pays_attendance_day_8_att_exp_out = atta_8[3];
                                simsobj.pays_attendance_day_8_att_punch_out = atta_8[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_8_att_code = dr["8"].ToString();
                            }
                            try
                            {
                                string[] atta_9 = dr["9"].ToString().Split('/');
                                simsobj.pays_attendance_day_9_att_code = atta_9[0];
                                simsobj.pays_attendance_day_9_att_exp_in = atta_9[1];
                                simsobj.pays_attendance_day_9_att_punch_in = atta_9[2];
                                simsobj.pays_attendance_day_9_att_exp_out = atta_9[3];
                                simsobj.pays_attendance_day_9_att_punch_out = atta_9[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_9_att_code = dr["9"].ToString();
                            }
                            try
                            {
                                string[] atta_10 = dr["10"].ToString().Split('/');
                                simsobj.pays_attendance_day_10_att_code = atta_10[0];
                                simsobj.pays_attendance_day_10_att_exp_in = atta_10[1];
                                simsobj.pays_attendance_day_10_att_punch_in = atta_10[2];
                                simsobj.pays_attendance_day_10_att_exp_out = atta_10[3];
                                simsobj.pays_attendance_day_10_att_punch_out = atta_10[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_10_att_code = dr["10"].ToString();
                            }

                            try
                            {
                                string[] atta_11 = dr["11"].ToString().Split('/');
                                simsobj.pays_attendance_day_11_att_code = atta_11[0];
                                simsobj.pays_attendance_day_11_att_exp_in = atta_11[1];
                                simsobj.pays_attendance_day_11_att_punch_in = atta_11[2];
                                simsobj.pays_attendance_day_11_att_exp_out = atta_11[3];
                                simsobj.pays_attendance_day_11_att_punch_out = atta_11[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_11_att_code = dr["11"].ToString();
                            }

                            try
                            {
                                string[] atta_12 = dr["12"].ToString().Split('/');
                                simsobj.pays_attendance_day_12_att_code = atta_12[0];
                                simsobj.pays_attendance_day_12_att_exp_in = atta_12[1];
                                simsobj.pays_attendance_day_12_att_punch_in = atta_12[2];
                                simsobj.pays_attendance_day_12_att_exp_out = atta_12[3];
                                simsobj.pays_attendance_day_12_att_punch_out = atta_12[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_12_att_code = dr["12"].ToString();
                            }

                            try
                            {
                                string[] atta_13 = dr["13"].ToString().Split('/');
                                simsobj.pays_attendance_day_13_att_code = atta_13[0];
                                simsobj.pays_attendance_day_13_att_exp_in = atta_13[1];
                                simsobj.pays_attendance_day_13_att_punch_in = atta_13[2];
                                simsobj.pays_attendance_day_13_att_exp_out = atta_13[3];
                                simsobj.pays_attendance_day_13_att_punch_out = atta_13[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_13_att_code = dr["13"].ToString();
                            }


                            try
                            {
                                string[] atta_14 = dr["14"].ToString().Split('/');
                                simsobj.pays_attendance_day_14_att_code = atta_14[0];
                                simsobj.pays_attendance_day_14_att_exp_in = atta_14[1];
                                simsobj.pays_attendance_day_14_att_punch_in = atta_14[2];
                                simsobj.pays_attendance_day_14_att_exp_out = atta_14[3];
                                simsobj.pays_attendance_day_14_att_punch_out = atta_14[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_14_att_code = dr["14"].ToString();
                            }


                            try
                            {
                                string[] atta_15 = dr["15"].ToString().Split('/');
                                simsobj.pays_attendance_day_15_att_code = atta_15[0];
                                simsobj.pays_attendance_day_15_att_exp_in = atta_15[1];
                                simsobj.pays_attendance_day_15_att_punch_in = atta_15[2];
                                simsobj.pays_attendance_day_15_att_exp_out = atta_15[3];
                                simsobj.pays_attendance_day_15_att_punch_out = atta_15[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_15_att_code = dr["15"].ToString();
                            }


                            try
                            {
                                string[] atta_16 = dr["16"].ToString().Split('/');
                                simsobj.pays_attendance_day_16_att_code = atta_16[0];
                                simsobj.pays_attendance_day_16_att_exp_in = atta_16[1];
                                simsobj.pays_attendance_day_16_att_punch_in = atta_16[2];
                                simsobj.pays_attendance_day_16_att_exp_out = atta_16[3];
                                simsobj.pays_attendance_day_16_att_punch_out = atta_16[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_16_att_code = dr["16"].ToString();
                            }


                            try
                            {
                                string[] atta_17 = dr["17"].ToString().Split('/');
                                simsobj.pays_attendance_day_17_att_code = atta_17[0];
                                simsobj.pays_attendance_day_17_att_exp_in = atta_17[1];
                                simsobj.pays_attendance_day_17_att_punch_in = atta_17[2];
                                simsobj.pays_attendance_day_17_att_exp_out = atta_17[3];
                                simsobj.pays_attendance_day_17_att_punch_out = atta_17[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_17_att_code = dr["17"].ToString();
                            }


                            try
                            {
                                string[] atta_18 = dr["18"].ToString().Split('/');
                                simsobj.pays_attendance_day_18_att_code = atta_18[0];
                                simsobj.pays_attendance_day_18_att_exp_in = atta_18[1];
                                simsobj.pays_attendance_day_18_att_punch_in = atta_18[2];
                                simsobj.pays_attendance_day_18_att_exp_out = atta_18[3];
                                simsobj.pays_attendance_day_18_att_punch_out = atta_18[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_18_att_code = dr["18"].ToString();
                            }


                            try
                            {
                                string[] atta_19 = dr["19"].ToString().Split('/');
                                simsobj.pays_attendance_day_19_att_code = atta_19[0];
                                simsobj.pays_attendance_day_19_att_exp_in = atta_19[1];
                                simsobj.pays_attendance_day_19_att_punch_in = atta_19[2];
                                simsobj.pays_attendance_day_19_att_exp_out = atta_19[3];
                                simsobj.pays_attendance_day_19_att_punch_out = atta_19[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_19_att_code = dr["19"].ToString();
                            }


                            try
                            {
                                string[] atta_20 = dr["20"].ToString().Split('/');
                                simsobj.pays_attendance_day_20_att_code = atta_20[0];
                                simsobj.pays_attendance_day_20_att_exp_in = atta_20[1];
                                simsobj.pays_attendance_day_20_att_punch_in = atta_20[2];
                                simsobj.pays_attendance_day_20_att_exp_out = atta_20[3];
                                simsobj.pays_attendance_day_20_att_punch_out = atta_20[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_20_att_code = dr["20"].ToString();
                            }


                            try
                            {
                                string[] atta_21 = dr["21"].ToString().Split('/');
                                simsobj.pays_attendance_day_21_att_code = atta_21[0];
                                simsobj.pays_attendance_day_21_att_exp_in = atta_21[1];
                                simsobj.pays_attendance_day_21_att_punch_in = atta_21[2];
                                simsobj.pays_attendance_day_21_att_exp_out = atta_21[3];
                                simsobj.pays_attendance_day_21_att_punch_out = atta_21[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_21_att_code = dr["21"].ToString();
                            }


                            try
                            {
                                string[] atta_22 = dr["22"].ToString().Split('/');
                                simsobj.pays_attendance_day_22_att_code = atta_22[0];
                                simsobj.pays_attendance_day_22_att_exp_in = atta_22[1];
                                simsobj.pays_attendance_day_22_att_punch_in = atta_22[2];
                                simsobj.pays_attendance_day_22_att_exp_out = atta_22[3];
                                simsobj.pays_attendance_day_22_att_punch_out = atta_22[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_22_att_code = dr["22"].ToString();
                            }


                            try
                            {
                                string[] atta_23 = dr["23"].ToString().Split('/');
                                simsobj.pays_attendance_day_23_att_code = atta_23[0];
                                simsobj.pays_attendance_day_23_att_exp_in = atta_23[1];
                                simsobj.pays_attendance_day_23_att_punch_in = atta_23[2];
                                simsobj.pays_attendance_day_23_att_exp_out = atta_23[3];
                                simsobj.pays_attendance_day_23_att_punch_out = atta_23[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_23_att_code = dr["23"].ToString();
                            }

                            try
                            {
                                string[] atta_24 = dr["24"].ToString().Split('/');
                                simsobj.pays_attendance_day_24_att_code = atta_24[0];
                                simsobj.pays_attendance_day_24_att_exp_in = atta_24[1];
                                simsobj.pays_attendance_day_24_att_punch_in = atta_24[2];
                                simsobj.pays_attendance_day_24_att_exp_out = atta_24[3];
                                simsobj.pays_attendance_day_24_att_punch_out = atta_24[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_24_att_code = dr["24"].ToString();
                            }

                            try
                            {
                                string[] atta_25 = dr["25"].ToString().Split('/');
                                simsobj.pays_attendance_day_25_att_code = atta_25[0];
                                simsobj.pays_attendance_day_25_att_exp_in = atta_25[1];
                                simsobj.pays_attendance_day_25_att_punch_in = atta_25[2];
                                simsobj.pays_attendance_day_25_att_exp_out = atta_25[3];
                                simsobj.pays_attendance_day_25_att_punch_out = atta_25[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_25_att_code = dr["25"].ToString();
                            }

                            try
                            {
                                string[] atta_26 = dr["26"].ToString().Split('/');
                                simsobj.pays_attendance_day_26_att_code = atta_26[0];
                                simsobj.pays_attendance_day_26_att_exp_in = atta_26[1];
                                simsobj.pays_attendance_day_26_att_punch_in = atta_26[2];
                                simsobj.pays_attendance_day_26_att_exp_out = atta_26[3];
                                simsobj.pays_attendance_day_26_att_punch_out = atta_26[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_26_att_code = dr["26"].ToString();
                            }

                            try
                            {
                                string[] atta_27 = dr["27"].ToString().Split('/');
                                simsobj.pays_attendance_day_27_att_code = atta_27[0];
                                simsobj.pays_attendance_day_27_att_exp_in = atta_27[1];
                                simsobj.pays_attendance_day_27_att_punch_in = atta_27[2];
                                simsobj.pays_attendance_day_27_att_exp_out = atta_27[3];
                                simsobj.pays_attendance_day_27_att_punch_out = atta_27[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_27_att_code = dr["27"].ToString();
                            }

                            try
                            {
                                string[] atta_28 = dr["28"].ToString().Split('/');
                                simsobj.pays_attendance_day_28_att_code = atta_28[0];
                                simsobj.pays_attendance_day_28_att_exp_in = atta_28[1];
                                simsobj.pays_attendance_day_28_att_punch_in = atta_28[2];
                                simsobj.pays_attendance_day_28_att_exp_out = atta_28[3];
                                simsobj.pays_attendance_day_28_att_punch_out = atta_28[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_28_att_code = dr["28"].ToString();
                            }

                            try
                            {
                                string[] atta_29 = dr["29"].ToString().Split('/');
                                simsobj.pays_attendance_day_29_att_code = atta_29[0];
                                simsobj.pays_attendance_day_29_att_exp_in = atta_29[1];
                                simsobj.pays_attendance_day_29_att_punch_in = atta_29[2];
                                simsobj.pays_attendance_day_29_att_exp_out = atta_29[3];
                                simsobj.pays_attendance_day_29_att_punch_out = atta_29[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_29_att_code = dr["29"].ToString();
                            }

                            try
                            {
                                string[] atta_30 = dr["30"].ToString().Split('/');
                                simsobj.pays_attendance_day_30_att_code = atta_30[0];
                                simsobj.pays_attendance_day_30_att_exp_in = atta_30[1];
                                simsobj.pays_attendance_day_30_att_punch_in = atta_30[2];
                                simsobj.pays_attendance_day_30_att_exp_out = atta_30[3];
                                simsobj.pays_attendance_day_30_att_punch_out = atta_30[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_30_att_code = dr["30"].ToString();
                            }

                            try
                            {
                                string[] atta_31 = dr["31"].ToString().Split('/');
                                simsobj.pays_attendance_day_31_att_code = atta_31[0];
                                simsobj.pays_attendance_day_31_att_exp_in = atta_31[1];
                                simsobj.pays_attendance_day_31_att_punch_in = atta_31[2];
                                simsobj.pays_attendance_day_31_att_exp_out = atta_31[3];
                                simsobj.pays_attendance_day_31_att_punch_out = atta_31[4];
                            }
                            catch (Exception e)
                            {
                                simsobj.pays_attendance_day_31_att_code = dr["31"].ToString();
                            }
                          
                            //simsobj.pays_attendance_day_1_att_code = dr["1"].ToString();

                            //simsobj.pays_attendance_day_2_att_code = dr["2"].ToString();
                            //simsobj.pays_attendance_day_3_att_code = dr["3"].ToString();
                            //simsobj.pays_attendance_day_4_att_code = dr["4"].ToString();
                            //simsobj.pays_attendance_day_5_att_code = dr["5"].ToString();
                            //simsobj.pays_attendance_day_6_att_code = dr["6"].ToString();
                            //simsobj.pays_attendance_day_7_att_code = dr["7"].ToString();
                            //simsobj.pays_attendance_day_8_att_code = dr["8"].ToString();
                            //simsobj.pays_attendance_day_9_att_code = dr["9"].ToString();
                            //simsobj.pays_attendance_day_10_att_code = dr["10"].ToString();
                            //simsobj.pays_attendance_day_11_att_code = dr["11"].ToString();
                            //simsobj.pays_attendance_day_12_att_code = dr["12"].ToString();
                            //simsobj.pays_attendance_day_13_att_code = dr["13"].ToString();
                            //simsobj.pays_attendance_day_14_att_code = dr["14"].ToString();
                            //simsobj.pays_attendance_day_15_att_code = dr["15"].ToString();
                            //simsobj.pays_attendance_day_16_att_code = dr["16"].ToString();
                            //simsobj.pays_attendance_day_17_att_code = dr["17"].ToString();
                            //simsobj.pays_attendance_day_18_att_code = dr["18"].ToString();
                            //simsobj.pays_attendance_day_19_att_code = dr["19"].ToString();
                            //simsobj.pays_attendance_day_20_att_code = dr["20"].ToString();
                            //simsobj.pays_attendance_day_21_att_code = dr["21"].ToString();
                            //simsobj.pays_attendance_day_22_att_code = dr["22"].ToString();
                            //simsobj.pays_attendance_day_23_att_code = dr["23"].ToString();
                            //simsobj.pays_attendance_day_24_att_code = dr["24"].ToString();
                            //simsobj.pays_attendance_day_25_att_code = dr["25"].ToString();
                            //simsobj.pays_attendance_day_26_att_code = dr["26"].ToString();
                            //simsobj.pays_attendance_day_27_att_code = dr["27"].ToString();
                            //simsobj.pays_attendance_day_28_att_code = dr["28"].ToString();
                            //simsobj.pays_attendance_day_29_att_code = dr["29"].ToString();
                            //simsobj.pays_attendance_day_30_att_code = dr["30"].ToString();
                            //simsobj.pays_attendance_day_31_att_code = dr["31"].ToString();

                          
                            //simsobj.pays_attendance_absent_count = ((simsobj, "A").ToString();
                            //simsobj.pays_attendance_present_count =((simsobj, "P").ToString();
                            //simsobj.pays_attendance_leave_count = (simsobj, "LA").ToString();
                            //simsobj.pays_attendance_CL_count = CountForAttendanceCode(obj, "CL").ToString();
                            //simsobj.pays_attendance_EA_count = CountForAttendanceCode(obj, "EA").ToString();
                            //simsobj.pays_attendance_SL_count = CountForAttendanceCode(obj, "SL").ToString();
                            //int holiday = int.Parse(CountForAttendanceCode(simsobj, "H").ToString());
                            //int Weekend = int.Parse(CountForAttendanceCode(simsobj, "W").ToString());
                            //simsobj.pays_attendance_wd_count = (days - (holiday + Weekend)).ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.company_code = dr["co_company_code"].ToString();
                            simsobj.company_name = dr["co_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {

                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode()
        {
            List<Per313> list = new List<Per313>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_attendance_code = dr["pays_attendance_code"].ToString();
                            simsobj.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                            simsobj.pays_attendance_description = dr["pays_attendance_description"].ToString();

                            if (!string.IsNullOrEmpty(dr["pays_attendance_color_code"].ToString()))
                            {
                                if (dr["pays_attendance_color_code"].ToString().Length != 7)
                                    simsobj.pays_attendance_color = string.Format("#{0}", dr["pays_attendance_color_code"].ToString().Substring(3));
                                else
                                    simsobj.pays_attendance_color = dr["pays_attendance_color_code"].ToString();                                
                            }
                            simsobj.pays_attendance_teacher_can_use_flag = dr["pays_attendance_teacher_can_use_flag"].ToString();
                            simsobj.pays_attendance_leave_code = dr["pays_attendance_leave_code"].ToString();

                            //if (!string.IsNullOrEmpty(dr["pays_attendance_color_code"].ToString()))
                            //{
                            //    simsobj.pays_attendance_color = string.Format("#{0}", dr["pays_attendance_color_code"].ToString().Substring(4));
                            //}
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getDeptName")]
        public HttpResponseMessage getDeptName(string comp_code)
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@comp_code",comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getGradeName")]
        public HttpResponseMessage getGradeName()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.gr_code = dr["gr_code"].ToString();
                            simsobj.gr_desc = dr["gr_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getStaffType")]
        public HttpResponseMessage getStaffType()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.staff_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.staff_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDesignation")]
        public HttpResponseMessage getDesignation()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dg_code = dr["dg_code"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getYear")]
        public HttpResponseMessage getYear()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_start_date = DateTime.Parse(dr["sims_academic_year_start_date"].ToString()).ToShortDateString();
                            simsobj.sims_academic_year_end_date = DateTime.Parse(dr["sims_academic_year_end_date"].ToString()).ToShortDateString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getSort")]
        public HttpResponseMessage getSort()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'N'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.sort_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sort_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("InsertEmpAttendance")]
        public HttpResponseMessage InsertEmpAttendance(List<Per313> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per313 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_daily_m_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                //new SqlParameter("@USER_ID", simsobj.att_emp_id),
                                new SqlParameter("@emp_id", simsobj.att_emp_id),
                                new SqlParameter("@att_date", simsobj.att_date),
                                new SqlParameter("@att_flag", simsobj.pays_attendance_code)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("InsertEmpAttendance_Rakaag")]
        public HttpResponseMessage InsertEmpAttendance_Rakaag(List<Per313> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per313 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_daily_m_rakaag_proc]",
                           new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", simsobj.opr),
                                //new SqlParameter("@USER_ID", simsobj.att_emp_id),
                                new SqlParameter("@emp_id", simsobj.att_emp_id),
                                new SqlParameter("@att_date", simsobj.att_date),
                                new SqlParameter("@att_flag", simsobj.pays_attendance_code),
                                new SqlParameter("@att_shift1_in", simsobj.att_shift1_in),
                                new SqlParameter("@att_shift1_out", simsobj.att_shift1_out)
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("MarkAllPreAbEmpAttendance")]
        public HttpResponseMessage MarkAllPreAbEmpAttendance(List<Per313> data)
        {
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per313 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_daily_m_proc]",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@emp_id", simsobj.att_emp_id),
                                new SqlParameter("@att_date", simsobj.att_date),
                                new SqlParameter("@att_flag", simsobj.att_flag)

                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("GetPayCycleDays")]
        public HttpResponseMessage GetPayCycleDays()
        {
            List<Per313> mod_list = new List<Per313>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "AB")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_appl_form_field_value1 = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.pays_appl_parameter = dr["pays_appl_parameter"].ToString();
                            mod_list.Add(simsobj);
                        }                        
                    }                                                           
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception x)
            {                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }            
        }



        [Route("PushAttendanceTime")]
        public HttpResponseMessage PushAttendanceTime(Per313 obj)
        {
            bool inserted = false;
            List<Per313> mod_list = new List<Per313>();
            try
            {
                if (obj.attendance_push_date == "undefined" || obj.attendance_push_date == "")
                    obj.attendance_push_date = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[job_employee_attendance_machine_form]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@attendace_push_date",db.DBYYYYMMDDformat(obj.attendance_push_date))
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }

        [Route("PushAttendanceFlag")]
        public HttpResponseMessage PushAttendanceFlag(Per313 obj)
        {
            bool inserted = false;
            List<Per313> mod_list = new List<Per313>();
            try
            {
                if (obj.attendance_push_date == "undefined" || obj.attendance_push_date == "")
                    obj.attendance_push_date = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[job_employee_attendance_monthly_form]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@attendace_push_date", db.DBYYYYMMDDformat(obj.attendance_push_date))
                    });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
        }


    }
}





















