﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/AttendanceAbsentReport")]
    public class AttendanceAbsentReportController : ApiController
    {
        [Route("CGDstudentattendance")]
        public HttpResponseMessage CGDstudentattendance(sims_defaulter obj)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("sims.sims_studentabsentreport_proc",
                       new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_section_code", obj.sims_section_code),
                            new SqlParameter("@from", db.DBYYYYMMDDformat(obj.from)),
                            new SqlParameter("@to", db.DBYYYYMMDDformat(obj.to))
                         });                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, ds);

            }

            return Request.CreateResponse(HttpStatusCode.OK, ds);

        }
    }
}