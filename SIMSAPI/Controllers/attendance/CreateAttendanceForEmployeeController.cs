﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.ERP.hrmsClass;

namespace SIMSAPI.Controllers.attendance
{
    [RoutePrefix("api/CreateAttendanceForEmployee")]
    [BasicAuthentication]
    public class CreateAttendanceForEmployeeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'K'),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.company_code = dr["co_company_code"].ToString();
                            simsobj.company_name = dr["co_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {

                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAttendanceCode")]
        public HttpResponseMessage getAttendanceCode()
        {
            List<Per313> list = new List<Per313>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "AC"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.pays_attendance_code = dr["pays_attendance_code"].ToString();
                            simsobj.pays_attendance_short_desc = dr["pays_attendance_short_desc"].ToString();
                            simsobj.pays_attendance_description = dr["pays_attendance_description"].ToString();
                            if (!string.IsNullOrEmpty(dr["pays_attendance_color_code"].ToString()))
                            {
                                simsobj.pays_attendance_color = string.Format("#{0}", dr["pays_attendance_color_code"].ToString().Substring(4));
                            }
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getDeptName")]
        public HttpResponseMessage getDeptName(string comp_code)
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@comp_code",comp_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dept_code = dr["codp_dept_no"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getGradeName")]
        public HttpResponseMessage getGradeName()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'F'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.gr_code = dr["gr_code"].ToString();
                            simsobj.gr_desc = dr["gr_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getStaffType")]
        public HttpResponseMessage getStaffType()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'H'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.staff_type_code = dr["sims_appl_parameter"].ToString();
                            simsobj.staff_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getDesignation")]
        public HttpResponseMessage getDesignation()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.dg_code = dr["dg_code"].ToString();
                            simsobj.dg_desc = dr["dg_desc"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getYear")]
        public HttpResponseMessage getYear()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'L'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_start_date = dr["sims_academic_year_start_date"].ToString();
                            simsobj.sims_academic_year_end_date =dr["sims_academic_year_end_date"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getSort")]
        public HttpResponseMessage getSort()
        {

            List<Per313> mod_list = new List<Per313>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'N'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.sort_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sort_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                //message.strMessage = "No Records Found";
                //message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, feeType);
        }

        [Route("getAttendanceInfoOfEmloyee")]
        public HttpResponseMessage getAttendanceInfoOfEmloyee(string company_code, string dept_code,string grade_code, string stafftype_code,string designation_code,string em_login_code, string FromDateDay, string ToDateDay, string attendance_flag)
        {
            List<Per313> list = new List<Per313>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "GP"),
                            new SqlParameter("@comp_code", company_code),
                            new SqlParameter("@dept_code", dept_code),
                            new SqlParameter("@grade_code", grade_code),
                            new SqlParameter("@stafftype_code", stafftype_code),
                            new SqlParameter("@designation_code", designation_code),
                            new SqlParameter("@em_login_code", em_login_code),
                            new SqlParameter("@FromDateDay",FromDateDay),
                            new SqlParameter("@ToDateDay", ToDateDay),
                            new SqlParameter("@attendance_flag",attendance_flag),                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per313 simsobj = new Per313();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.pays_employee_name = dr["employeeName"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) 
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("CreateEmployeeAttendance")]
        public HttpResponseMessage CreateEmployeeAttendance(List<Per313> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Per313 simsobj in data)
                    {
                        //int ins = db.ExecuteStoreProcedureforInsert("[pays].[pays_attendance_daily_m_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_attendance_daily_m_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", "PG"),
                                new SqlParameter("@em_login_code",simsobj.em_login_code),
                                new SqlParameter("@FromDateDay",simsobj.FromDateDay),
                                new SqlParameter("@ToDateDay",simsobj.ToDateDay),
                                 new SqlParameter("@att_absent_flag", simsobj.att_absent_flag),
                               
                     });
                        if (dr.RecordsAffected > 0)
                        {
                            insert = true;
                        }
                        
                        else
                        {
                            insert = false;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}