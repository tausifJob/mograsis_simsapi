﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.AttendanceClass;
using System.Linq;
namespace SIMSAPI.Controllers.SubjectwiseAttendanceController
{
    [RoutePrefix("api/SubjectwiseAttendance")]
    public class SubjectwiseAttendanceController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("Get_get_total_present")]
        public HttpResponseMessage Get_get_total_present(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
                           
               
                  new SqlParameter("@opr", "S"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                  new SqlParameter("@sims_grade_code", grade),
                  new SqlParameter("@sims_section_code", sec),
                  new SqlParameter("@sims_attendance_date_sd", db.DBYYYYMMDDformat(st))
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_nm = dr["sims_section_name_en"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_std_nm = dr["sims_section_stregth"].ToString();
                            a.present = dr["present"].ToString();


                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Get_get_all_status")]
        public HttpResponseMessage Get_get_all_status(string cur_code, string academic, string grade, string sec, string st)
        {
            List<piechart> list = new List<piechart>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "B"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", db.DBYYYYMMDDformat(st))
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            piechart a = new piechart();
                            a.label = dr["att_code_desc"].ToString();
                            //a.absent = dr["absent1"].ToString();
                            //a.trady = dr["tardy"].ToString();
                            //a.tradyExcused = dr["tardyExcuse"].ToString();
                            //a.Unmarked = dr["Unmarked"].ToString();
                            a.color = dr["att_color"].ToString();
                            a.highlight = dr["att_color"].ToString();

                            if (!string.IsNullOrEmpty(dr["att_code_count"].ToString()))
                                a.value = int.Parse(dr["att_code_count"].ToString());
                            else
                                a.value = 0;

                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("Get_get_all_labels")]
        public HttpResponseMessage Get_get_all_labels(string cur_code, string academic, string grade, string sec, string st)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
             
                new SqlParameter("@opr", "C"),
                new SqlParameter("@sims_cur_code", cur_code),
                new SqlParameter("@sims_academic_year", academic),
                new SqlParameter("@sims_grade_code", grade),
                new SqlParameter("@sims_section_code", sec),
                new SqlParameter("@sims_attendance_date_sd", db.DBYYYYMMDDformat(st))
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.present = dr["present"].ToString();
                            a.absent = dr["absent1"].ToString();
                            a.trady = dr["tardy"].ToString();
                            a.tradyExcused = dr["tardyExcuse"].ToString();
                            a.Unmarked = dr["Unmarked"].ToString();
                            a.cnt = dr["stregth"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        //Insert Records

        [Route("UpdateAttednace")]
        public HttpResponseMessage UpdateAttednace(subject_wise_attendance obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_subject_attendnace",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "U"),
                  new SqlParameter("@cur_code", obj.sims_cur_code),
                  new SqlParameter("@year", obj.sims_academic_year),
                  new SqlParameter("@grade", obj.sims_grade_code),
                  new SqlParameter("@section", obj.sims_section_code),
                  new SqlParameter("@sub_code", obj.sims_subject_code),
                  new SqlParameter("@teacher_code", obj.sims_teacher_code),
                  new SqlParameter("@slot_code", obj.sims_attedance_slot),
                  new SqlParameter("@att_code", obj.sims_attedance_code),
                  new SqlParameter("@enrollno", obj.sims_enroll_number),
                  new SqlParameter("@attdate",db.DBYYYYMMDDformat(obj.sims_attedance_date)),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateCommentAttednace")]
        public HttpResponseMessage UpdateCommentAttednace(subject_wise_attendance obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_subject_attendnace",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "C"),
                  new SqlParameter("@cur_code", obj.sims_cur_code),
                  new SqlParameter("@year", obj.sims_academic_year),
                  new SqlParameter("@grade", obj.sims_grade_code),
                  new SqlParameter("@section", obj.sims_section_code),
                  new SqlParameter("@sub_code", obj.sims_subject_code),
                  new SqlParameter("@teacher_code", obj.sims_teacher_code),
                  new SqlParameter("@slot_code", obj.sims_attedance_slot),
                  new SqlParameter("@att_code", obj.sims_attedance_code),
                  new SqlParameter("@enrollno", obj.sims_enroll_number),
                   new SqlParameter("@comment", obj.sims_attendance_day_comment),
                  new SqlParameter("@attdate",db.DBYYYYMMDDformat(obj.sims_attedance_date)),

                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CommentGetAttednace")]
        public HttpResponseMessage CommentGetAttednace(subject_wise_attendance obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            string inserted = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_subject_attendnace",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "G"),
                  new SqlParameter("@cur_code", obj.sims_cur_code),
                  new SqlParameter("@year", obj.sims_academic_year),
                  new SqlParameter("@grade", obj.sims_grade_code),
                  new SqlParameter("@section", obj.sims_section_code),
                  new SqlParameter("@sub_code", obj.sims_subject_code),
                  new SqlParameter("@teacher_code", obj.sims_teacher_code),
                  new SqlParameter("@slot_code", obj.sims_attedance_slot),
                  new SqlParameter("@att_code", obj.sims_attedance_code),
                  new SqlParameter("@enrollno", obj.sims_enroll_number),
                   new SqlParameter("@comment", obj.sims_attendance_day_comment),
                  new SqlParameter("@attdate",db.DBYYYYMMDDformat(obj.sims_attedance_date)),

                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            inserted = dr["comment"].ToString();
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("MarkAllAttednace")]
        public HttpResponseMessage MarkAllAttednace(subject_wise_attendance obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims_subject_attendnace",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "M"),
                  new SqlParameter("@cur_code", obj.sims_cur_code),
                  new SqlParameter("@year", obj.sims_academic_year),
                  new SqlParameter("@grade", obj.sims_grade_code),
                  new SqlParameter("@section", obj.sims_section_code),
                  new SqlParameter("@sub_code", obj.sims_subject_code),
                  new SqlParameter("@teacher_code", obj.sims_teacher_code),
                  new SqlParameter("@slot_code", obj.sims_attedance_slot),
                  new SqlParameter("@att_code", obj.sims_attedance_code),
                 // new SqlParameter("@enrollno", obj.sims_enroll_number),
                  new SqlParameter("@attdate",db.DBYYYYMMDDformat(obj.sims_attedance_date)),

                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        //Get Records

        [Route("GetAttednace_Grade")]
        public HttpResponseMessage GetAttednace_Grade(string cur_code, string a_year, string username)
        {
            List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance",
                        new List<SqlParameter>()
                         {

                 new SqlParameter("@opr", "G"),
                 new SqlParameter("@sims_cur_code", cur_code),
                 new SqlParameter("@bell_academic_year", a_year),
                 new SqlParameter("@user_name", username),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            subject_wise_attendance obj = new subject_wise_attendance();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();

                            house.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("GetAttednace_Section")]
        public HttpResponseMessage GetAttednace_Section(string cur_code, string a_year, string grade_code, string username)
        {

            List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance",
                        new List<SqlParameter>()
                         {
                            
                 new SqlParameter("@opr", "T"),
                 new SqlParameter("@sims_cur_code", cur_code),
                 new SqlParameter("@bell_academic_year", a_year),
                 new SqlParameter("@bell_grade_code", grade_code),
                 new SqlParameter("@user_name", username)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            subject_wise_attendance obj = new subject_wise_attendance();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            house.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }

        [Route("GetAttednace")]
        public HttpResponseMessage GetAttednace(string cur_code, string ayear, string grade, string section, string attednacedate, string sims_teacher_code)
        {
            string str = "";
            string sub = "";

            List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_subject_attendnace",
                        new List<SqlParameter>()
                         {
                            
                 new SqlParameter("@opr", "S"),
                 new SqlParameter("@cur_code", cur_code),
                 new SqlParameter("@year", ayear),
                 new SqlParameter("@grade", grade),
                 new SqlParameter("@section", section),
                 new SqlParameter("@att_date", db.DBYYYYMMDDformat(attednacedate)),
                 new SqlParameter("@teacher_code", sims_teacher_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["enroll_no"].ToString();
                           sub = dr["sims_bell_subject_code"].ToString();

                            var v = (from p in house where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                subject_wise_attendance ob = new subject_wise_attendance();
                                ob.sublist1 = new List<subject1>();
                                ob.sims_cur_code = cur_code;
                                ob.sims_academic_year = ayear;
                                ob.sims_grade_code = grade;
                                ob.sims_section_code = section;
                                ob.sims_attedance_date = attednacedate;
                                ob.sims_enroll_number = str;


                                ob.sims_student_name = dr["StudentName"].ToString();

                                ob.sims_attedance_code = dr["attendance_code"].ToString();
                              //  ob.sims_att_color_code = dr["color_code"].ToString();
                                //if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                //{
                                //    ob.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                //}

                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    if (dr["color_code"].ToString().Length != 7)
                                        ob.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                    else
                                        ob.sims_att_color_code = dr["color_code"].ToString();

                                }

                                subject1 sb = new subject1();
                                sb.sims_subject_name = dr["Subject_Name"].ToString();
                                sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                                sb.sims_attedance_slot = dr["slot_no"].ToString();

                                sb.sims_attedance_code = dr["attendance_code"].ToString();
                               // sb.sims_att_color_code = dr["color_code"].ToString();
                                //if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                //{
                                //    sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                //}

                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    if (dr["color_code"].ToString().Length != 7)
                                        sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                    else
                                        sb.sims_att_color_code = dr["color_code"].ToString();

                                }

                                ob.sublist1.Add(sb);
                                house.Add(ob);
                            }
                            else
                            {
                                subject1 sb = new subject1();
                                sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                                sb.sims_subject_name = dr["Subject_Name"].ToString();
                                sb.sims_attedance_slot = dr["slot_no"].ToString();
                                sb.sims_attedance_code = dr["attendance_code"].ToString();
                                //sb.sims_att_color_code = dr["color_code"].ToString();

                                //if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                //{
                                //    sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                //}


                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    if (dr["color_code"].ToString().Length != 7)
                                        sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                    else
                                        sb.sims_att_color_code = dr["color_code"].ToString();

                                }

                                //sb.sims_status = dr["Status"].Equals(1) ? true : false;
                                v.ElementAt(0).sublist1.Add(sb);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }




        [Route("GetAttednaceNew")]
        public HttpResponseMessage GetAttednaceNew(string cur_code, string ayear, string grade, string section, string attednacedate, string sims_teacher_code)
        {
            string str = "";
            List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_attendnace_proc]",
                        new List<SqlParameter>()
                         {
                            
                 new SqlParameter("@opr", "N"),
                 new SqlParameter("@cur_code", cur_code),
                 new SqlParameter("@year", ayear),
                 new SqlParameter("@grade", grade),
                 new SqlParameter("@section", section),
                 new SqlParameter("@att_date", attednacedate),
                 new SqlParameter("@teacher_code", sims_teacher_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            str = dr["enroll_no"].ToString();
                            var v = (from p in house where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                subject_wise_attendance ob = new subject_wise_attendance();
                                ob.sublist1 = new List<subject1>();
                                ob.sims_cur_code = cur_code;
                                ob.sims_academic_year = ayear;
                                ob.sims_grade_code = grade;
                                ob.sims_section_code = section;
                                ob.sims_attedance_date = attednacedate;
                                ob.sims_enroll_number = str;


                                ob.sims_student_name = dr["StudentName"].ToString();

                                ob.sims_attedance_code = dr["attendance_code"].ToString();
                                //  ob.sims_att_color_code = dr["color_code"].ToString();
                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    ob.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                }

                                subject1 sb = new subject1();
                                sb.sims_subject_name = dr["Subject_Name"].ToString();
                                sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                                sb.sims_attedance_slot = dr["slot_no"].ToString();

                                sb.sims_attedance_code = dr["attendance_code"].ToString();
                                // sb.sims_att_color_code = dr["color_code"].ToString();
                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                }

                                ob.sublist1.Add(sb);
                                house.Add(ob);
                            }
                            else
                            {
                                subject1 sb = new subject1();
                                sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
                                sb.sims_subject_name = dr["Subject_Name"].ToString();
                                sb.sims_attedance_slot = dr["slot_no"].ToString();
                                sb.sims_attedance_code = dr["attendance_code"].ToString();
                                //sb.sims_att_color_code = dr["color_code"].ToString();

                                if (!string.IsNullOrEmpty(dr["color_code"].ToString()))
                                {
                                    sb.sims_att_color_code = string.Format("#{0}", dr["color_code"].ToString().Substring(3));
                                }

                                //sb.sims_status = dr["Status"].Equals(1) ? true : false;
                                v.ElementAt(0).sublist1.Add(sb);
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }



        [Route("UpdateAttednaceNew")]
        public HttpResponseMessage UpdateAttednaceNew(subject_wise_attendance obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UpdateAttednace(),PARAMETERS :: OBJ {2}";
            Log.Debug(string.Format(debug, "PP", "UpdateAttednace", obj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_subject_attendnace_proc]",
                            new List<SqlParameter>()
                            {
                       
                  new SqlParameter("@opr", "Z"),
                  new SqlParameter("@cur_code", obj.sims_cur_code),
                  new SqlParameter("@year", obj.sims_academic_year),
                  new SqlParameter("@grade", obj.sims_grade_code),
                  new SqlParameter("@section", obj.sims_section_code),
                  new SqlParameter("@sub_code", obj.sims_subject_code),
                  new SqlParameter("@teacher_code", obj.sims_teacher_code),
                  new SqlParameter("@slot_code", obj.sims_attedance_slot),
                  new SqlParameter("@att_code", obj.sims_attedance_code),
                  new SqlParameter("@enrollno", obj.sims_enroll_number),
                  new SqlParameter("@attdate", obj.sims_attedance_date),

                            });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        #region admission dash
        [Route("get_Section_Strength")]
        public HttpResponseMessage get_Section_Strength(string cur_code, string academic, string grade, string sd, string ed)
        {
            List<attnDash> list = new List<attnDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_student_attendance_dash",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "S"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                  new SqlParameter("@sims_grade_code", grade)
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            attnDash a = new attnDash();
                            a.sims_section_code = dr["sims_section_code"].ToString();
                            a.sims_section_nm = dr["sims_section_name_en"].ToString();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_std_nm = dr["sims_section_stregth"].ToString();
                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        #endregion


        //[Route("GetAttednace")]
        //public HttpResponseMessage GetAttednace(string cur_code, string ayear, string grade, string section, string attednacedate, string sims_teacher_code)
        //{
        //    string cur_code = string.Empty;
        //    string str = "";
        //    List<subject_wise_attendance> mod_list = new List<subject_wise_attendance>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("sims_subject_attendnace",
        //            new List<SqlParameter>() 
        //                 { 
        //         new SqlParameter("@opr", "S"),
        //         new SqlParameter("@cur_code", cur_code),
        //         new SqlParameter("@year", ayear),
        //         new SqlParameter("@grade", grade),
        //         new SqlParameter("@section", section),
        //         new SqlParameter("@att_date", attednacedate),
        //         new SqlParameter("@teacher_code", sims_teacher_code)

        //                 });
        //            while (dr.Read())
        //            {
        //                Sims089 newobj = new Sims089();
        //                str = dr["enroll_no"].ToString();
        //                var v = (from p in mod_list where p.sims_enroll_number == str select p);

        //                if (v.Count() == 0)
        //                {
        //                    subject_wise_attendance ob = new subject_wise_attendance();
        //                    ob.sublist1 = new List<subject1>();
        //                    ob.sims_cur_code = cur_code;
        //                    ob.sims_academic_year = ayear;
        //                    ob.sims_grade_code = grade;
        //                    ob.sims_section_code = section;
        //                    ob.sims_attedance_date = attednacedate;
        //                    ob.sims_enroll_number = str;


        //                    ob.sims_student_name = dr["StudentName"].ToString();

        //                    ob.sims_attedance_code = dr["attendance_code"].ToString();
        //                    ob.sims_att_color_code = dr["color_code"].ToString();

        //                    subject1 sb = new subject1();
        //                    sb.sims_subject_name = dr["Subject_Name"].ToString();
        //                    sb.sims_subject_code = dr["sims_bell_subject_code"].ToString();
        //                    sb.sims_attedance_slot = dr["slot_no"].ToString();

        //                    ob.sublist1.Add(sb);
        //                    mod_list.Add(ob);
        //                }
        //                else
        //                {
        //                    subject1 sb = new subject1();
        //                    sb.sims_subject_code = dr["sims_subject_code"].ToString();
        //                    sb.sims_subject_name = dr["sims_subject_name_en"].ToString();
        //                    sb.sims_attedance_slot = dr["slot_no"].ToString();

        //                    sb.sims_status = dr["Status"].Equals(1) ? true : false;
        //                    v.ElementAt(0).sublist1.Add(sb);
        //                }
        //                newobj.sims_student_name = dr["sims_enroll_number"].ToString();
        //                newobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //                newobj.sims_grade_code = dr["sims_grade_code"].ToString();
        //                newobj.sims_section_code = dr["sims_section_code"].ToString();
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //        }
        //    }

        //    catch (Exception e)
        //    {
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        //}

        [Route("GetAttednaceGeneration")]
        public HttpResponseMessage AttednaceGeneration(string cur_code, string ayear, string grade, string section, string start_date, string end_date, string sims_teacher_code)
        {
            string str = "";
            bool sub = false;

            List<subject_wise_attendance> house = new List<subject_wise_attendance>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_attendance_update_from_subject_wise_new_proc",
                        new List<SqlParameter>()
                         {

                 //new SqlParameter("@opr", "S"),
                 new SqlParameter("@sims_cur_code", cur_code),
                 new SqlParameter("@sims_academic_year", ayear),
                 new SqlParameter("@sims_grade_code", grade),
                 new SqlParameter("@sims_section_code", section),
                 new SqlParameter("@att_date_start", db.DBYYYYMMDDformat(start_date)),
                 new SqlParameter("@att_date_end", db.DBYYYYMMDDformat(end_date)),
                 new SqlParameter("@teacher_code", sims_teacher_code)
                         }
                         );
                   // if (dr.HasRows)
                    {
                       // while (dr.Read())
                        {

                            if (dr.RecordsAffected > 0) {
                                sub = true;   
                            }
                            
                             
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, sub);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, house);
        }


    }

}