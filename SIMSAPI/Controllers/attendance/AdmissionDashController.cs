﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.AttendanceClass;
using System.Linq;
namespace SIMSAPI.Controllers.AdmissionDashController
{
    [RoutePrefix("api/AdmissionDash")]
    public class AdmissionDashController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


      

        #region admission dash
        [Route("get_Section_Strength")]
        public HttpResponseMessage get_Section_Strength(string cur_code, string academic, string grade, string sd, string ed)
        {
            List<admissionDash> list = new List<admissionDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_dashboard]",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "S"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                 // new SqlParameter("@sims_grade_code", grade)
                  new SqlParameter("@start_date", sd),
                  new SqlParameter("@end_date",ed),
                  new SqlParameter("@sims_grade_code", grade),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionDash a = new admissionDash();
                            a.sims_grade_code = dr["sims_grade_code"].ToString();
                            a.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            a.total_stregth = dr["total_stregth"].ToString();
                            a.allocated = dr["allocated"].ToString();
                            a.confirm = dr["confirm"].ToString();

                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        #endregion

        [Route("get_All_status")]
        public HttpResponseMessage get_All_status(string cur_code, string academic, string grade, string sd, string ed)
        {
            List<piechart> list = new List<piechart>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_dashboard]",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "B"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                   new SqlParameter("@start_date", sd),
                  new SqlParameter("@end_date",ed),
                  new SqlParameter("@sims_grade_code", grade),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            piechart a = new piechart();
                            a.label = dr["name"].ToString();
                          //  a.value = dr["count"].ToString();
                            if (!string.IsNullOrEmpty(dr["count"].ToString()))
                                a.value = int.Parse(dr["count"].ToString());
                            else
                                a.value = 0;

                      

                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_All_Boys_Girls_status")]
        public HttpResponseMessage get_All_Boys_Girls_status(string cur_code, string academic, string grade, string sd, string ed)
        {
            List<admissionDash> list = new List<admissionDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_dashboard]",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "K"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                   new SqlParameter("@start_date", sd),
                  new SqlParameter("@end_date",ed),
                  new SqlParameter("@sims_grade_code", grade),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionDash a = new admissionDash();
                            a.cnt = dr["cnt"].ToString();
                            a.confirm = dr["confirm"].ToString();
                            a.rejected = dr["rejected"].ToString();


                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


        [Route("get_All_status")]
        public HttpResponseMessage get_All_Religion(string cur_code, string academic, string grade, string sd, string ed,string opr)
        {
            List<piechart> list = new List<piechart>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_dashboard]",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", opr),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                    new SqlParameter("@start_date", sd),
                  new SqlParameter("@end_date",ed),
                  new SqlParameter("@sims_grade_code", grade),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            piechart a = new piechart();
                            a.label = dr["sims_religion_name_en"].ToString();
                            if (!string.IsNullOrEmpty(dr["cnt"].ToString()))

                                a.value = int.Parse(dr["cnt"].ToString());
                            else
                                a.value = 0;

                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }



        [Route("get_All_Religion_details")]
        public HttpResponseMessage get_All_Religion_details(string cur_code, string academic, string grade, string sd, string ed, string religion)
        {
            List<admissionDash> list = new List<admissionDash>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_dashboard]",
                        new List<SqlParameter>() 
                         { 
                           
                  new SqlParameter("@opr", "F"),
                  new SqlParameter("@sims_cur_code", cur_code),
                  new SqlParameter("@sims_academic_year", academic),
                  new SqlParameter("@start_date", sd),
                  new SqlParameter("@end_date",ed),
                  new SqlParameter("@sims_grade_code", grade),
                  new SqlParameter("@sims_grade_name_ot", religion),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionDash a = new admissionDash();
                            a.cnt = dr["cnt"].ToString();
                            a.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                           // a.rejected = dr["rejected"].ToString();

                            list.Add(a);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }


    }

}