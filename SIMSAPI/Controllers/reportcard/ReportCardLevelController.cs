﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.gradebookClass;

namespace SIMSAPI.Controllers.reportcard
{
    [RoutePrefix("api/ReportCardLevel")]
    [BasicAuthentication]
    public class ReportCardLevelController : ApiController
    {

        [Route("getReportCardLevel")]
        public HttpResponseMessage getReportCardLevel()
        {
            List<Sims146> list = new List<Sims146>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_level_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           
                            Sims146 simsobj = new Sims146();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_name = dr["cur_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["grade_name"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["section_name"].ToString();
                            simsobj.sims_level_code = dr["sims_level_code"].ToString();
                            simsobj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            simsobj.sims_level_status = dr["sims_report_card_status"].ToString() == "A";
                            list.Add(simsobj);
                           
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateDeleteReportCardLevel")]
        public HttpResponseMessage InsertUpdateDeleteReportCardLevel(List<gradebookClass> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {

                foreach (var simsobj in data)
                    {
                string[] classes = simsobj.sims_section_code.Split(',');

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var item in classes)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_level_proc]",
                        new List<SqlParameter>()
                     {

                              
                                  new SqlParameter("@opr", simsobj.opr),
                                  new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                  new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                  new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                  new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                  new SqlParameter("@sims_level_code", simsobj.sims_level_code),
                                  new SqlParameter("@class_code", item),
                                  new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
                                  new SqlParameter("@sims_level_status", simsobj.sims_level_status ? "A" : "I"),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                    }
                }
                }
            }
            catch (Exception x)
            {
                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
    }
}