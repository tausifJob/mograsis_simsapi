﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.gradebookClass;

namespace SIMSAPI.Controllers.reportcard
{
    [RoutePrefix("api/ReportCard")]
    [BasicAuthentication]
    public class ReportCardController : ApiController
    {
        [Route("getReportCard")]
        public HttpResponseMessage getReportCard()
        {
            List<Sims138> list = new List<Sims138>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims138 obj = new Sims138();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_cur_name = dr["cur_name"].ToString();
                            obj.sims_level_name = dr["sims_level_name"].ToString();
                            obj.sims_level_code = dr["sims_level_code"].ToString();
                            obj.sims_grade_name = dr["grade_name"].ToString();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_section_name = dr["section_name"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            obj.sims_report_card_name = dr["sims_report_card_name"].ToString();

                            obj.sims_report_card_desc = dr["sims_report_card_description"].ToString();
                            obj.sims_report_card_declaration_date = db.UIDDMMYYYYformat(dr["sims_report_card_declaration_date"].ToString());
                            obj.sims_report_card_publish_date = db.UIDDMMYYYYformat(dr["sims_report_card_publishing_date"].ToString());
                            obj.user_name = dr["user_name"].ToString();
                            obj.sims_report_card_created_user = dr["sims_report_card_created_user_code"].ToString();
                            if (dr["sims_report_card_freeze_status"].ToString() == "Y")
                                obj.sims_report_card_freeze_status = true;
                            else
                                obj.sims_report_card_freeze_status = false;
                            obj.sims_report_card_parameter_master_code = dr["sims_report_card_parameter_master_code"].ToString();
                            obj.param_desc = dr["param_desc"].ToString();
                            obj.term_name = dr["report_term"].ToString();
                            obj.sims_report_card_term = dr["sims_report_card_term"].ToString();
                            list.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("InsertUpdateDeleteReportCard")]
        public HttpResponseMessage InsertUpdateDeleteReportCard(List<Sims138> data)
        {
            string status = string.Empty;
            bool insert = false;
            Message message = new Message();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (var simsobj in data)
                    {
                        if (simsobj.sims_report_card_freeze_status == true)
                            status = "Y";
                        else
                            status = "N";

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_report_card_proc]",
                        new List<SqlParameter>()
                            {

                                  new SqlParameter("@opr", simsobj.opr),
                                  new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                  new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                  new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                  new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                                  new SqlParameter("@sims_level_code", simsobj.sims_level_code),
                                  new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_code),
                                  new SqlParameter("@sims_report_card_name", simsobj.sims_report_card_name),
                                  new SqlParameter("@sims_report_card_description", simsobj.sims_report_card_desc),
                                  new SqlParameter("@sims_report_card_declaration_date",db.DBYYYYMMDDformat(simsobj.sims_report_card_declaration_date)),
                                  new SqlParameter("@sims_report_card_publishing_date",db.DBYYYYMMDDformat(simsobj.sims_report_card_publish_date)),
                                  new SqlParameter("@sims_report_card_created_user_code", simsobj.sims_report_card_created_user),
                                  new SqlParameter("@sims_report_card_freeze_status", status),
                                  new SqlParameter("@sims_report_card_parameter_master_code", simsobj.param_desc),
                                  //new SqlParameter("@sims_report_card_parameter_master_code", simsobj.par_desc),
                                  new SqlParameter("@sims_report_card_term", simsobj.term_name),
                               
                            });
                        if (ins > 0)
                        {
                            insert = true;
                        }

                    }



                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getReportCardLevelName")]
        public HttpResponseMessage getReportCardLevelName(string cur_code, string academic_year, string grade_code, string section_name)
        {
            List<Sims138> list = new List<Sims138>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section_name),
                           
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims138 simsobj = new Sims138();
                            simsobj.sims_level_code = dr["sims_level_code"].ToString();
                            simsobj.sims_level_name = dr["sims_report_card_name"].ToString();
                            list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getTermDetails")]
        public HttpResponseMessage getTermDetails(string cur_code, string academic_year)
        {
            List<Sims138> list = new List<Sims138>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "G"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", academic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims138 obj = new Sims138();
                            obj.sims_term_code = dr["sims_term_code"].ToString();
                            obj.term_name = dr["sims_term_desc_en"].ToString();
                            list.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("getParamDesc")]
        public HttpResponseMessage getParamDesc(string cur_code, string academic_year, string grade_code, string section_name)
        {
            List<Sims138> list = new List<Sims138>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_report_card_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@sims_cur_code", cur_code),
                             new SqlParameter("@sims_academic_year", academic_year),
                            new SqlParameter("@sims_grade_code", grade_code),
                            new SqlParameter("@sims_section_code", section_name),
                           
                           
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims138 simsobj = new Sims138();
                            simsobj.sims_report_card_parameter_master_code = dr["sims_report_card_parameter_master_code"].ToString();
                            simsobj.param_desc = dr["sims_report_card_param_description"].ToString();
                            list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

    }
}