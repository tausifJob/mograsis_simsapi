﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Web;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.gradebookClass;
namespace SIMSAPI.Controllers.reportcard
{
    [RoutePrefix("api/ReportCardAllocation")]
    public class ReportCardAllocationController : ApiController
    {
        [Route("Get_report_card_code")]
        public HttpResponseMessage Get_report_card_code(string cur_code, string acadmic_yr, string grade_code, string section, string term)
        {
            List<Sims139> list = new List<Sims139>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "F"),
                            new SqlParameter("@sims_report_card_cur_code", cur_code),
                            new SqlParameter("@sims_report_card_academic_year", acadmic_yr),
                            new SqlParameter("@sims_report_card_grade_code", grade_code),
                            new SqlParameter("@sims_report_card_section_code", section),
                            new SqlParameter("@sims_report_card_term_code", term)
                }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims139 obj = new Sims139();

                            obj.sims_report_card_master_code = dr["sims_level_code"].ToString() + dr["sims_report_card_code"].ToString();
                            obj.sims_report_card_name = dr["level_name"].ToString() + " " + dr["sims_report_card_name"].ToString();
                            obj.sims_level_code = dr["sims_level_code"].ToString();
                            obj.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("Get_Report_card_allocation")]
        public HttpResponseMessage Get_Report_card_allocation(string cur_code, string acadmic_yr, string grade_code, string section, string report_card) //(/*string cur_code, string acadmic_yr, string grade_code, string section, string report_card*/)
        {
            List<Sims139> list = new List<Sims139>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S"),
                                  new SqlParameter("@sims_report_card_cur_code", cur_code),
                                  new SqlParameter("@sims_report_card_academic_year",acadmic_yr ),
                                  new SqlParameter("@sims_report_card_grade_code", grade_code),
                                  new SqlParameter("@sims_report_card_section_code", section),
                                  new SqlParameter("@sims_report_card_code", report_card)
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims139 obj = new Sims139();
                            obj.sims_academic_year = dr["sims_report_card_academic_year"].ToString();
                            obj.sims_cur_code = dr["sims_report_card_cur_code"].ToString();
                            obj.sims_cur_name = dr["sims_report_card_cur_code"].ToString();
                            obj.sims_grade_code = dr["sims_report_card_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_code = dr["sims_report_card_section_code"].ToString();
                            obj.sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_report_card_level = dr["sims_report_card_level_code"].ToString();
                            obj.sims_report_card_level_name = dr["level"].ToString();
                            obj.sims_report_card_master_code = dr["sims_report_card_code"].ToString();
                            obj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            obj.sims_gb_number = dr["sims_gb_number"].ToString();
                            obj.sims_gb_name = dr["sims_gb_name"].ToString();
                            obj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            obj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            obj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            obj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                            obj.sims_report_card_serial_number = dr["sims_report_card_serial_number"].ToString();
                            obj.sims_report_card_allocation = dr["sims_report_card_allocation"].ToString();
                            if (dr["sims_report_card_status"].ToString() == "A")
                            {
                                obj.sims_report_card_status = true;
                            }
                            else
                            {
                                obj.sims_report_card_status = false;
                            }
                            obj.report_select = false;
                            list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }

        [Route("GbListProps")]
        public HttpResponseMessage GbListProps(List<GBRoot> data)
        {
            List<GBRoot> lst = new List<GBRoot>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (GBRoot simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[GRADEBOOK]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@OPR", "PR"),
                             new SqlParameter("@IOPR", simsobj.OPR),
                             new SqlParameter("@ACADEMIC_YEAR", simsobj.academic_year),
                             new SqlParameter("@GRADE_CODE", simsobj.grade_code),
                             new SqlParameter("@SECTION_CODE", simsobj.section_code),
                             new SqlParameter("@GB_TERM_CODE", simsobj.term_code)
                           });
                        if (dr.HasRows)

                        {
                            while (dr.Read())
                            {
                                GBRoot gb = new GBRoot();
                                gb.academic_year = dr["sims_academic_year"].ToString();
                                gb.curr_code = dr["sims_cur_code"].ToString();
                                gb.grade_code = dr["sims_grade_code"].ToString();
                                gb.section_code = dr["sims_section_code"].ToString();
                                gb.sims_gb_name = dr["sims_gb_name"].ToString();
                                gb.sims_gb_number = dr["sims_gb_number"].ToString();
                                gb.term_code = dr["sims_gb_term_code"].ToString();
                                string sts = dr["sims_gb_status"].ToString();
                                gb.Status = (sts == "A" || sts == "1" || sts == "T" || sts == "t" || sts == "a" || sts == "Y" || sts == "y") ? "Black" : "WhiteSmoke";
                                try
                                {
                                    string str = dr[simsobj.PropertyName].ToString();
                                    gb.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                                }
                                catch (Exception)
                                {
                                }
                                lst.Add(gb);
                            }
                            dr.NextResult();
                            while (dr.Read())
                            {
                                GB_Category cat = new GB_Category();
                                cat.Category_code = dr["sims_gb_cat_code"].ToString();
                                cat.Category_name = dr["sims_gb_cat_name"].ToString();
                                cat.sims_gb_number = dr["sims_gb_number"].ToString();
                                string sts = dr["sims_gb_cat_status"].ToString();
                                cat.Status = (sts == "A" || sts == "1" || sts == "T" || sts == "t" || sts == "a" || sts == "Y" || sts == "y") ? "Black" : "WhiteSmoke";
                                try
                                {
                                    string str = dr[simsobj.PropertyName].ToString();
                                    cat.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                                }
                                catch (Exception)
                                {
                                }

                                try
                                {
                                    GBRoot gb = lst.Single(q => q.sims_gb_number == cat.sims_gb_number);
                                    if (gb.Categories == null) gb.Categories = new List<GB_Category>();
                                    gb.Categories.Add(cat);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                          
                            dr.NextResult();
                            while (dr.Read())
                            {

                                CatAssignment assign = new CatAssignment();
                                assign.Assignment_name = dr["sims_gb_cat_assign_name"].ToString();
                                assign.Assignment_number = dr["sims_gb_cat_assign_number"].ToString();
                                assign.sims_gb_cat_number = dr["sims_gb_cat_code"].ToString();
                                assign.sims_gb_number = dr["sims_gb_number"].ToString();
                                try
                                {
                                    string str = dr[simsobj.PropertyName].ToString();
                                    assign.isSelected = (str == "A" || str == "1" || str == "T" || str == "t" || str == "a" || str == "Y" || str == "y");
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    GBRoot gbro = lst.Single(q => q.sims_gb_number == assign.sims_gb_number);
                                    GB_Category cat = gbro.Categories.Single(q => q.Category_code == assign.sims_gb_cat_number);
                                    if (cat.AssignMents == null) cat.AssignMents = new List<CatAssignment>();
                                    cat.AssignMents.Add(assign);
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            try
            {
                foreach (var GB in lst)
                {
                    if (GB.Categories != null)
                    {
                        foreach (var CAT in GB.Categories)
                        {
                            if (CAT.AssignMents != null)
                            {
                                int cnt = CAT.AssignMents.Count;
                                int checkedCount = CAT.AssignMents.Where(q => q.isSelected).Count();
                                CAT.isSelected = (cnt == checkedCount);
                            }
                        }
                        int GBcnt = GB.Categories.Count;
                        int GBcheckedCount = GB.Categories.Where(q => q.isSelected).Count();
                        GB.isSelected = (GBcnt == GBcheckedCount);
                    }
                }
            }
            catch (Exception)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
            // return lst;
        }
       
        [Route("Sims506_Delete_Report_card")]
        public HttpResponseMessage Sims506_Delete_Report_card(List<Sims506> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "Sims506_Insert_Report_Card", simsobj));

            bool Deleted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Sims506 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims_report_card_allocation]",
                            new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr", 'D'),
                                 new SqlParameter("@sims_report_card_cur_code", simsobj.sims_cur_code),
                                 new SqlParameter("@sims_report_card_academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@sims_report_card_grade_code", simsobj.sims_grade_code),
                                 new SqlParameter("@sims_report_card_section_code", simsobj.sims_section_name),
                                 new SqlParameter("@sims_report_card_level_code", simsobj.sims_report_card_level),
                                 new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_code),
                                 new SqlParameter("@sims_gb_number", simsobj.sims_gb_number),
                                 new SqlParameter("@sims_gb_cat_code", simsobj.sims_gb_cat_code),
                                 new SqlParameter("@sims_gb_cat_assign_number", simsobj.sims_gb_cat_assign_number),
                    });
                      
                        if (ins > 0)
                        {
                            Deleted = true;
                        }
                        else
                        {
                            Deleted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Deleted);
        }

        [Route("Sims506_Update_Report_card_allocation")]
        public HttpResponseMessage Sims506_Update_Report_card_allocation(List<Sims506> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "Sims506_Insert_Report_Card", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Sims506 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims_report_card_allocation]",
                            new List<SqlParameter>()
                            {
                                  
                         new SqlParameter("@opr", 'U'),
                         new SqlParameter("@sims_report_card_cur_code", simsobj.sims_cur_code),
                         new SqlParameter("@sims_report_card_academic_year", simsobj.sims_academic_year),
                         new SqlParameter("@sims_report_card_grade_code", simsobj.sims_grade_code),
                         new SqlParameter("@sims_report_card_section_code", simsobj.sims_section_code),
                         new SqlParameter("@sims_report_card_level_code", simsobj.sims_report_card_level),
                         new SqlParameter("@sims_report_card_code", simsobj.sims_report_card_master_code),
                         new SqlParameter("@sims_gb_number", simsobj.sims_gb_number),
                         new SqlParameter("@sims_gb_cat_code", simsobj.sims_gb_cat_code),
                         new SqlParameter("@sims_gb_cat_assign_number", simsobj.sims_gb_cat_assign_number),
                         new SqlParameter("@sims_report_card_serial_number", simsobj.sims_report_card_serial_number),
                         new SqlParameter("@sims_report_card_status", simsobj.sims_report_card_status==true?"A":"I"),
                         new SqlParameter("@sims_report_card_allocation", simsobj.sims_report_card_allocation),
                    });
                      
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("Sims506_Insert_Report_Card")]
        public HttpResponseMessage Sims506_Insert_Report_Card(List<Sims506> data)
        {
            bool inserted = false;            
            //Sims506 obj;
            try
            {
                 string val = "";
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                    foreach (Sims506 obj in data)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                                new List<SqlParameter>()

                                {

                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
                            new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
                            new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
                            new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
                            new SqlParameter("@sims_report_card_level_code", obj.sims_report_card_level),
                            new SqlParameter("@sims_report_card_code", obj.sims_report_card_code),
                            new SqlParameter("@sims_gb_number", obj.sims_gb_number),
                            new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
                            new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number),
                            });
                        if (dr.HasRows)
                        {
                            dr.Close();                           
                            continue;
                        }
                        else
                        {
                            dr.Close();
                            int ins = db.ExecuteStoreProcedureforInsert("sims_report_card_allocation",
                        new List<SqlParameter>()

                           {

                                 new SqlParameter("@opr", 'I'),
                                 new SqlParameter("@sims_report_card_cur_code", obj.sims_cur_code),
                                 new SqlParameter("@sims_report_card_academic_year", obj.sims_academic_year),
                                 new SqlParameter("@sims_report_card_grade_code", obj.sims_grade_code),
                                 new SqlParameter("@sims_report_card_section_code", obj.sims_section_code),
                                 new SqlParameter("@sims_report_card_level_code", obj.sims_report_card_level),
                                 new SqlParameter("@sims_report_card_code", obj.sims_report_card_code),
                                 new SqlParameter("@sims_gb_number",obj.sims_gb_number),
                                 new SqlParameter("@sims_gb_cat_code", obj.sims_gb_cat_code),
                                 new SqlParameter("@sims_gb_cat_assign_number", obj.sims_gb_cat_assign_number),
                                 new SqlParameter("@sims_report_card_serial_number", obj.sims_report_card_serial_number),
                                new SqlParameter("@sims_report_card_status", "A"),

                        });

                            if (ins > 0)
                            {
                                inserted = true;
                            }                         
                            else
                            {
                                inserted = false;
                            }
                            //dr.Close();

                        }
                       

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getInsertMesage")]
        public HttpResponseMessage getInsertMesage()
        {
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("fins_pdc_bills1",
                        new List<SqlParameter>()
                         {
                         new SqlParameter("@opr", "T"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            message.strMessage = dr["msg"].ToString();

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("GetAllterm")]
        public HttpResponseMessage GetAllterm(string cur_code, string academic_year)
        {
            List<Sims179> mod_list = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_report_card_allocation",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","T"),
                        new SqlParameter("@sims_cur_code", cur_code),
                        new SqlParameter("@sims_academic_year", academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 simsobj = new Sims179();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}

   
