﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using EmiratesId.AE.PublicData;
using System.Web;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/nationalid")]
    public class NationalIdCardController : ApiController
    {

        [Route("EIDRecord")]
        public HttpResponseMessage EIDRecord(emirates str)
        {
            try
            {
                List<emirates> doc_list = new List<emirates>();
                emirates obj = new emirates();
                string ef_idn_cn = str.idn_cn;
                string ef_non_mod_data = str.non_mod_data;
                string ef_mod_data = str.mod_data;
                string ef_sign_image = str.sign_image;
                string ef_photo = str.photo;
                string ef_root_cert = str.root_cert;
                string ef_home_address = str.home_address;
                string ef_work_address = str.work_address;
                //string certsPath = "C:/Development/website/sis-branch/SIMSAPI/data_signing_certs";
                string certsPath = HttpContext.Current.Server.MapPath("~/Content/data_signing_certs"); 
                //   string certsPath = "http://oa.mograsys.com/emiratesid/data_signing_certs/";
                bool nonMod = false;
                bool mod = false;
                bool signImage = false;
                bool photo = false;
                bool homeAddress = false;
                bool workAddress = false;

                PublicDataParser parser = null;

                //string[] files = System.IO.Directory.GetFiles(certsPath);

                //foreach (string s in files)
                //{
                //    parser = new PublicDataParser(ef_idn_cn, s);
                //}
                try
                {
                    parser = new PublicDataParser(ef_idn_cn, certsPath);
                    nonMod = parser.parseNonModifiableData(ef_non_mod_data);
                    mod = parser.parseModifiableData(ef_mod_data);
                    photo = parser.parsePhotography(ef_photo);
                    signImage = parser.parseSignatureImage(ef_sign_image);
                    homeAddress = parser.parseHomeAddressData(ef_home_address);
                    workAddress = parser.parseWorkAddressData(ef_work_address);
                    parser.parseRootCertificate(ef_root_cert);
                }
                catch (Exception ex)
                {

                }
                obj.FullName = parser.getFullName();
                obj.IDN = parser.getIdNumber();
                obj.CardNumber = parser.getCardNumber();
                obj.Title = parser.getTitle();
                obj.Nationality = parser.getNationality();
                if (parser.getIssueDate() != null)
                {
                    obj.IssueDate = parser.getIssueDate().Value.ToString("dd-MM-yyyy");
                }
                if (parser.getExpiryDate() != null)
                {
                    obj.ExpiryDate = parser.getExpiryDate().Value.ToString("dd-MM-yyyy");
                }
                obj.IdType = parser.getIdType();
                obj.Sex = parser.getSex();
                if (parser.getDateOfBirth() != null)
                {
                    obj.DoB = parser.getDateOfBirth() == null ? "" : parser.getDateOfBirth().Value.ToString("dd-MM-yyyy");
                }
                obj.FullName_ar = parser.getArabicFullName();
                obj.MaritalStatus = parser.getMaritalStatus();
                obj.Occupation = parser.getOccupation() == null ? "" : parser.getOccupation();
                obj.OccupationField = parser.getOccupationField() == null ? "" : parser.getOccupationField();
                obj.OccupationType = parser.getOccupationType();
                obj.OccupationTypeAr = parser.getOccupationType_ar();
                obj.Title_ar = parser.getArabicTitle();
                obj.Nationality_ar = parser.getArabicNationality();
                obj.MotherName = parser.getMotherFullName() == null ? "" : parser.getMotherFullName();
                obj.MotherName_ar = parser.getMotherFullName_ar() == null ? "" : parser.getMotherFullName_ar();
                obj.FamilyId = parser.getFamilyID();
                obj.HusbandIDN = parser.getHusbandIDN();
                obj.SponsorType = parser.getSponsorType();
                obj.SponsorName = parser.getSponsorName();
                obj.SponsorUnifiedNumber = parser.getSponsorUnifiedNumber();
                obj.ResidencyType = parser.getResidencyType();
                obj.ResidencyNumber = parser.getResidencyNumber();
                obj.ResidencyExpiryDate = parser.getResidencyExpiryDate() == null ? "" : parser.getResidencyExpiryDate().Value.ToString("dd-MM-yyyy");
                obj.companyname = parser.getCompanyName();
                obj.companyname_ar = parser.getCompanyName_ar();

                obj.FlatNo = parser.getHomeAddressFlatNo();
                obj.Street = parser.getHomeAddressStreet();
                obj.Area = parser.getHomeAddressAreaDesc();
                obj.BldgName = parser.getHomeAddressBuildingName();
                obj.City = parser.getHomeAddressCityDesc();
                obj.Email = parser.getHomeAddressEmail();
                obj.ResPhone = parser.getHomeAddressResidentPhoneNo();
                obj.Mobile = parser.getHomeAddressMobilePhoneNo();
                obj.POBox = parser.getHomeAddressPOBox();
                obj.EmirateDesc = parser.getHomeAddressEmirateDesc();
                obj.DateofGraduation = parser.getGraduationDate() == null ? "" : parser.getGraduationDate().Value.ToString("dd-MM-yyyy");
                obj.Degree = parser.getDegreeDesc();
                obj.DegreeAr = parser.getDegreeDesc_ar();
                obj.FieldofStudy = parser.getFieldOfStudy();
                obj.FieldofStudyAr = parser.getFieldOfStudy_ar();
                obj.placeofbirth = parser.getPlaceOfBirth();
                obj.placeofbirthAr = parser.getPlaceOfBirth_ar();
                obj.Placeofstudy = parser.getPlaceOfStudy();
                obj.Placeofstudy_ar = parser.getPlaceOfStudy_ar();
                obj.QualificationLevel = parser.getQualificationLevelDesc();
                obj.QualificationLevelAr = parser.getQualificationLevelDesc_ar();
                obj.SponsorUnifiedNo = parser.getSponsorUnifiedNumber();

                obj.PassportNumber = parser.getPassportNumber();
                obj.PassportCountry = parser.getPassportCountry();
                obj.PassportCountryDesc = parser.getPassportCountryDesc();
                obj.PassportCountryAr = parser.getPassportCountryDesc_ar();
                obj.PassportIssueDate = parser.getPassportIssueDate() == null ? "" : parser.getPassportIssueDate().Value.ToString("dd-MM-yyyy");
                obj.PassportExpiryDate = parser.getPassportExpiryDate() == null ? "" : parser.getPassportExpiryDate().Value.ToString("dd-MM-yyyy");
                obj.PassportType = parser.getPassportType();

                obj.homeaddress = parser.getHomeAddressFlatNo() + "," + parser.getHomeAddressBuildingName() + " , " + parser.getHomeAddressAreaDesc() + " , " + parser.getHomeAddressCityDesc() + " , " + parser.getHomeAddressStreet() + " , " + parser.getHomeAddressResidentPhoneNo() + " , " + parser.getHomeAddressPOBox();
                obj.workaddress = parser.getWorkAddressBuildingName() + " , " + parser.getWorkAddressAreaDesc()
                                    + " , " + parser.getWorkAddressCityDesc() + " , " + parser.getWorkAddressStreet() + " , " + parser.getWorkAddressLandPhoneNo()
                                    + " , " + parser.getWorkAddressPOBox();
                if (parser.getPhotography() != null)
                    obj.Srcphoto = "data:image/jpeg;base64," + Convert.ToBase64String(parser.getPhotography());
                if (parser.getHolderSignatureImage() != null)
                    obj.Srcsign = "data:image/tiff;base64," + Convert.ToBase64String(parser.getHolderSignatureImage());


                doc_list.Add(obj);
                return Request.CreateResponse(HttpStatusCode.OK, doc_list);
            }
            catch (Exception e) {

                return Request.CreateResponse(HttpStatusCode.OK, e);
            }

            
        }


        [Route("InsertEIDCardDetails")]
        public HttpResponseMessage InsertEIDCardDetails(emirates data)
        {
            bool insert = false;
            Message message = new Message();
            emirates obj = new emirates();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    //                    int ins 
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_national_id_card_transaction_details_proc]",
                new List<SqlParameter>()                     {                        new SqlParameter("@opr", "I"),                        new SqlParameter("@eid_card_no",data.CardNumber),			            new SqlParameter("@eid_idn_no",data.IDN),			            new SqlParameter("@eid_idn_type",data.IdType),			            new SqlParameter("@eid_full_name",data.FullName),                        new SqlParameter("@eid_full_name_ar",data.FulllNameAr),			            new SqlParameter("@eid_family_id",data.FamilyId),			            new SqlParameter("@eid_mother_full_name",data.motherfullname),			            new SqlParameter("@eid_mother_full_name_ar",data.motherfullnamear),                        			            new SqlParameter("@eid_dob",db.DBYYYYMMDDformat(data.DoB)),			            new SqlParameter("@eid_gender",data.Sex),			            new SqlParameter("@eid_profile_img",data.photo),			            new SqlParameter("@eid_signature_img",data.sign_image),			            new SqlParameter("@eid_nationality",data.Nationality),			            new SqlParameter("@eid_nationality_ar",data.NatinalityAr),                        new SqlParameter("@eid_marital_status",data.MaritalStatus),			            new SqlParameter("@eid_husband_idn_no",data.HusbandIDN),			            new SqlParameter("@eid_card_issue_date",db.DBYYYYMMDDformat(data.CardIssueDate)),			            new SqlParameter("@eid_card_expiry_date",db.DBYYYYMMDDformat(data.CardExpiryDate)),			            new SqlParameter("@eid_occupation",data.Occupation),			            new SqlParameter("@eid_occupation_field",data.OccupationField),			            new SqlParameter("@eid_occupation_type",data.OccupationType),			            new SqlParameter("@eid_occupation_type_ar",data.OccupationTypeAr),                        new SqlParameter("@eid_residency_no",data.ResidencyNumber),			            new SqlParameter("@eid_residency_type",data.ResidencyType),			            new SqlParameter("@eid_residency_expiry_date",db.DBYYYYMMDDformat(data.ResidencyExpiryDate)),			            new SqlParameter("@eid_sponsor_name",data.SponsorName),			            new SqlParameter("@eid_sponsor_no",data.SponsorNo),			            new SqlParameter("@eid_sponsor_type",data.SponsorType),			            new SqlParameter("@eid_title",data.Title),                        new SqlParameter("@eid_title_ar",data.TitleAr),                                                   			            new SqlParameter("@eid_flat_no",data.FlatNo),			            new SqlParameter("@eid_street",data.Street),			            new SqlParameter("@eid_area",data.Area),			            new SqlParameter("@eid_building_name",data.BldgName),			            new SqlParameter("@eid_city",data.City),			            new SqlParameter("@eid_email",data.Email),			            new SqlParameter("@eid_resident_phone",data.ResPhone),			            new SqlParameter("@eid_mobile",data.Mobile),			            new SqlParameter("@eid_po_box",data.POBox),			            new SqlParameter("@eid_emirates_desc",data.EmirateDesc),			            new SqlParameter("@eid_company_name",data.companyname),			            new SqlParameter("@eid_company_name_ar",data.companyname_ar),			            new SqlParameter("@eid_date_of_graduation",data.DateofGraduation),			            new SqlParameter("@eid_degree",data.Degree),			            new SqlParameter("@eid_degree_ar",data.DegreeAr),                        new SqlParameter("@eid_field_of_study",data.FieldofStudy),			            new SqlParameter("@eid_field_of_study_ar",data.FieldofStudyAr),                        new SqlParameter("@eid_passport_number",data.PassportNumber),			            new SqlParameter("@eid_passport_country",data.PassportCountry),			            new SqlParameter("@eid_passport_country_desc",data.PassportCountryDesc),                        new SqlParameter("@eid_passport_country_desc_ar",data.PassportCountryDescAr),                        new SqlParameter("@eid_passport_type",data.PassportType),			            new SqlParameter("@eid_passport_issue_date",db.DBYYYYMMDDformat(data.PassportIssueDate)),			            new SqlParameter("@eid_passport_expiry_date",db.DBYYYYMMDDformat(data.PassportExpiryDate)),			            new SqlParameter("@eid_place_of_birth",data.placeofbirth),			            new SqlParameter("@eid_place_of_birth_ar",data.placeofbirthar),                        new SqlParameter("@eid_place_of_study",data.Placeofstudy),			            new SqlParameter("@eid_place_of_study_ar",data.Placeofstudyar),                        new SqlParameter("@eid_qualification_level",data.QualificationLevel),			            new SqlParameter("@eid_qualification_level_ar",data.QualificationLevelAr),                        new SqlParameter("@eid_sponsor_unified_no",data.SponsorUnifiedNo),			            new SqlParameter("@eid_work_address",data.workaddress),			            new SqlParameter("@eid_creation_user",data.CreationUser),			            new SqlParameter("@eid_creation_date",null),			            new SqlParameter("@comn_user_name",data.UserName),                                                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            obj.idn_cn = dr["result"].ToString();
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        } 
    }
}










