﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.Security
{
    [RoutePrefix("api/ResetPassword")]
    [BasicAuthentication]
    public class ResetPasswordController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Seelct
        [Route("GetAll_User_Status")]
        public HttpResponseMessage GetAll_User_Status()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLibraryAttribute(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllLibraryAttribute"));

            List<Comn014> goaltarget_list = new List<Comn014>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_reset_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn014 simsobj = new Comn014();
                            simsobj.comn_reset_user_status_code = dr["sims_appl_parameter"].ToString();
                            simsobj.comn_reset_user_status = dr["sims_appl_form_field_value1"].ToString();
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetFillAllData")]
        public HttpResponseMessage GetFillAllData(string user_code)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetFillAllData(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "GetFillAllData"));

            List<Comn014> goaltarget_list = new List<Comn014>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_reset_proc]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'B'),
                           new SqlParameter("@comn_user_code",user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn014 simsobj = new Comn014();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_user_captcha_status = dr["comn_user_captcha_status"].Equals("1") ? true : false; 
                            simsobj.comn_user_email = dr["comn_user_email"].ToString();
                            simsobj.comn_reset_user_status_code = dr["comn_user_status"].ToString();
                            //comn_reset_user_status_code
                            simsobj.comn_user_status_name = dr["comn_user_status_name"].ToString();
                            simsobj.comn_user_expiry_date = db.UIDDMMYYYYformat(dr["comn_user_expiry_date"].ToString());
                            simsobj.comn_user_code = dr["comn_user_code"].ToString();
                            simsobj.comn_user_alias = dr["comn_user_alias"].ToString();
                            simsobj.comn_user_password = dr["comn_user_password"].ToString();
                            simsobj.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            simsobj.comn_user_date_created = dr["comn_user_date_created"].ToString();
                            simsobj.comn_user_last_login = dr["comn_user_last_login"].ToString();
                            simsobj.comn_user_emp_code = dr["comn_user_emp_code"].ToString();
                            simsobj.comn_user_enroll_number = dr["comn_user_enroll_number"].ToString();
                            simsobj.comn_user_phone_number = dr["comn_user_phone_number"].ToString();
                            simsobj.comn_user_secret_question_code = dr["comn_user_secret_question_code"].ToString();
                            simsobj.comn_user_secret_answer = dr["comn_user_secret_answer"].ToString();
                            simsobj.comn_user_remark = dr["comn_user_remark"].ToString();
                            simsobj.comn_user_created_by_user_code = dr["comn_user_created_by_user_code"].ToString();
                            simsobj.comn_user_created_date = db.UIDDMMYYYYformat(dr["comn_user_created_date"].ToString());
                            goaltarget_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDResetPassword")]
        public HttpResponseMessage CUDResetPassword(List<Comn014> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Comn014 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[comn_user_reset_proc]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                               new SqlParameter("@comn_user_status",simsobj.comn_reset_user_status_code),
                                new SqlParameter("@comn_user_new_password",simsobj.comn_user_password),
                                new SqlParameter("@comn_user_captcha_status",simsobj.comn_user_captcha_status.Equals(true) ? 'A' :'I'),
                                new SqlParameter("@comn_user_email", simsobj.comn_user_email),
                                new SqlParameter("@comn_user_expiry_date",simsobj.comn_user_expiry_date),
                                new SqlParameter("@comn_user_code", simsobj.comn_user_code),
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
               return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("CUDfavorite")]
        public HttpResponseMessage CUDfavorite(string app_code,string user_name,string status )
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_user_applicaton_favorite]",

                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", "S"),
                               new SqlParameter("@user_name",user_name),
                                new SqlParameter("@appl_code",app_code),
                                new SqlParameter("@is_favorite",status),
                               
                                
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                 
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

                message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

    }
}










