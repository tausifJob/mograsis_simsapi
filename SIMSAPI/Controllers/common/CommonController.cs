﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.COMMON;
using System.Web.Configuration;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        //Common Menu
        #region commn_menu



        [Route("GetMenu")]
        public HttpResponseMessage GetMenu(string user_code, string module_code)
        {
            Object[] ob = new Object[2];
            List<comomn_menu> appl_types = new List<comomn_menu>();
            List<comomn_menu> applications = new List<comomn_menu>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_menu",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@user_name", user_code),
                            new SqlParameter("@mod_code", module_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comomn_menu simsobj = new comomn_menu();
                            simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            simsobj.comn_appl_type_name = dr["comn_appl_type_name"].ToString();
                            try
                            {
                                simsobj.comn_appl_type_name_ar = dr["sims_appl_form_field_value3"].ToString();
                                simsobj.comn_appl_type_color = dr["sims_appl_form_field_value4"].ToString();

                            }
                            catch (Exception ex) { }
                            appl_types.Add(simsobj);
                        }
                        ob[0] = new object();
                        ob[0] = appl_types;
                        dr.NextResult();
                        while (dr.Read())
                        {
                            comomn_menu simsobj = new comomn_menu();
                            simsobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();
                            simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            simsobj.comn_appl_type_name = dr["comn_appl_type_name"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            simsobj.comn_appl_image = dr["app_icon"].ToString();
                            simsobj.comn_appl_location = dr["comn_appl_location"].ToString();
                            try
                            {
                                simsobj.comn_appl_display_order = dr["comn_appl_display_order"].ToString();
                            }
                            catch (Exception ex) { }

                            applications.Add(simsobj);
                        }
                        ob[1] = new object();
                        ob[1] = applications;

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ob);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ob);
            }
        }

        [Route("GetUserMenu")]
        public HttpResponseMessage GetUserMenu(string user_code)
        {
            Object[] ob = new Object[2];
            List<comomn_menu> appl_types = new List<comomn_menu>();
            List<comomn_menu> applications = new List<comomn_menu>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_menu",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "CM"),
                            new SqlParameter("@user_name", user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comomn_menu simsobj = new comomn_menu();
                            simsobj.comn_appl_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();
                            simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            simsobj.comn_appl_type_name = dr["comn_appl_type_name"].ToString();
                            appl_types.Add(simsobj);
                        }
                        ob[0] = new object();
                        ob[0] = appl_types;
                        dr.NextResult();
                        while (dr.Read())
                        {
                            comomn_menu simsobj = new comomn_menu();
                            simsobj.comn_appl_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_mod_name_ar = dr["comn_mod_name_ar"].ToString();
                            simsobj.comn_mod_color = dr["comn_mod_color"].ToString();
                            simsobj.comn_mod_img = dr["comn_mod_img"].ToString();
                            if (!string.IsNullOrEmpty(dr["appl_type"].ToString()))
                            {
                                List<string> apptype = dr["appl_type"].ToString().Split(',').ToList<string>();
                                foreach (string x in apptype)
                                    simsobj.appl_type.Add(new commn_appl_type { comn_appl_name_en = x, comn_user_appl_code = string.Empty });
                            }
                            applications.Add(simsobj);
                        }
                        ob[1] = new object();
                        ob[1] = applications;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ob);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ob);
            }
        }

        [Route("GetUserNotifications")]
        public HttpResponseMessage GetUserNotifications(string user_code)
        {
            List<common_notifications> userNotifications = new List<common_notifications>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_menu",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "SN"),
                            new SqlParameter("@user_name", user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            common_notifications common = new common_notifications();
                            common.common_notification_type = dr["Message_Type"].ToString();
                            common.common_notification_subject = dr["User_Message"].ToString();
                            common.common_notification_date = dr["Message_Date"].ToString();
                            userNotifications.Add(common);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, userNotifications);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, userNotifications);
            }
        }

        #endregion

        // GET api/<controller>/5
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllComboBoxValues")]
        public HttpResponseMessage getAllComboBoxValues(string academicYear, string routeCode)
        {
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("sims.GetAdmissionData_Temp_proc",
                       new List<SqlParameter>() 
                         { 
                             new SqlParameter("@academic_year", academicYear),
                              new SqlParameter("@route_code", routeCode)
                         });

                }
                return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }


        [Route("getStopLanLogByDirection")]
        public HttpResponseMessage getStopLanLogByDirection(string route_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStopLanLogByDirection(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getStopLanLogByDirection"));

            List<Sims081> Stop_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_student_approved_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "P"),
                                new SqlParameter("@sims_transport_route_code", route_code),
                                new SqlParameter("@sims_transport_academic_year", aca_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            simsobj.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            simsobj.sims_transport_stop_lat = dr["sims_transport_stop_lat"].ToString();
                            simsobj.sims_transport_stop_long = dr["sims_transport_stop_long"].ToString();
                            Stop_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Stop_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Stop_list);
            }
        }


        [Route("getSitingCapacity")]
        public HttpResponseMessage getSitingCapacity(string route_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSitingCapacity(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getSitingCapacity"));

            List<Sims081> Seat_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_student_approved_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "q"),
                                new SqlParameter("@sims_transport_route_code", route_code),
                                new SqlParameter("@sims_transport_academic_year", aca_year),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_vehicle_seating_capacity = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            simsobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();

                            Seat_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Seat_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Seat_list);
            }
        }

        [Route("getRouteCode")]
        public HttpResponseMessage getRouteCode(string academicYear, string routeCode)
        {
            List<parentClass> parentStudents = new List<parentClass>();


            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetAdmissionData_Temp",
                       new List<SqlParameter>() 
                         { 
                             new SqlParameter("@academic_year", academicYear),
                              new SqlParameter("@route_code", routeCode)
                         });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            parentClass pc = new parentClass();

                            pc.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            pc.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();

                            parentStudents.Add(pc);
                        }
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, parentStudents);

        }

        //[Route("getCity")]
        //public HttpResponseMessage getCity(string StateCode)
        //{
        //    List<Sims042> list = new List<Sims042>();

        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@tbl_name", "sims.sims_city"),
        //                    new SqlParameter("@tbl_col_name1", "sims_city_code,sims_city_name_en"),
        //                    new SqlParameter("@tbl_cond", "sims_status=" + "'A'" + "and sims_state_code=" + "'" + StateCode + "'"),
        //                    new SqlParameter("@tbl_ordby", "sims_city_name_en")
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims042 simsobj = new Sims042();
        //                    simsobj.sims_parent_father_city_code = dr["sims_city_code"].ToString();
        //                    simsobj.sims_parent_father_city = dr["sims_city_name_en"].ToString();
        //                    list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, list);
        //}


        [Route("GetComapanyName")]
        public HttpResponseMessage GetComapanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetComapanyName()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetComapanyName"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'A'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.comp_short_name = dr["comp_short_name"].ToString();
                            simsobj.comp_code = dr["comp_code"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("GetYear")]
        public HttpResponseMessage GetYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetYear()PARAMETERS ::NA";
            //  Log.Debug(string.Format(debug, "ERP/Inventory/", "GetYear"));

            List<Fin205> doc_list = new List<Fin205>();

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@opr_mem", '1'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fin205 simsobj = new Fin205();
                            simsobj.fins_finance_year = dr["fins_appl_form_field_value1"].ToString();
                            simsobj.fins_finance_year_desc = dr["fins_appl_form_field_value3"].ToString();
                            doc_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, doc_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, goal_list);
        }

        [Route("getSchoolDetails")]
        public HttpResponseMessage getSchoolDetails()
        {
            //List<common> list = new List<common>();
            License simsobj = new License();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.lic_school_name = dr["lic_school_name"].ToString();

                            simsobj.lic_website_url = dr["lic_website_url"].ToString();

                            simsobj.lic_school_other_name = dr["lic_school_other_name"].ToString();

                            //   string url = simsobj.lic_website_url.Split().First();
                            simsobj.lic_school_logo = dr["lic_school_logo"].ToString();
                            try
                            {
                                int x = simsobj.lic_school_logo.IndexOf("Images");
                                string str = simsobj.lic_school_logo.Substring(0, x).ToString();
                                simsobj.lic_website_url = str;
                            }
                            catch (Exception ex)
                            {
                            }

                            simsobj.lic_lms_url = dr["lic_school_lms_link"].ToString();


                            // list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, simsobj);
        }

        [Route("getSchoolDetailsNew")]
        public HttpResponseMessage getSchoolDetailsNew()
        {
            //List<common> list = new List<common>();
            License simsobj = new License();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.Mogra_License_details_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.lic_school_name = dr["lic_school_name"].ToString();

                            simsobj.lic_website_url = dr["lic_website_url"].ToString();

                            simsobj.lic_school_other_name = dr["lic_school_other_name"].ToString();

                            //   string url = simsobj.lic_website_url.Split().First();
                            simsobj.lic_school_logo = dr["lic_school_logo"].ToString();
                            try
                            {
                                int x = simsobj.lic_school_logo.IndexOf("Images");
                                string str = simsobj.lic_school_logo.Substring(0, x).ToString();
                                simsobj.lic_website_url = str;
                            }
                            catch (Exception ex)
                            {
                            }

                            try {
                                simsobj.lic_school_microsoft_client_id = dr["lic_school_microsoft_client_id"].ToString();
                                simsobj.lic_school_microsoft_login = dr["lic_school_microsoft_login"].ToString();
                                simsobj.lic_school_microsoft_redirect_url = dr["lic_school_microsoft_redirect_url"].ToString();
                            
                            }
                            catch (Exception ex) { }

                            simsobj.lic_lms_url = dr["lic_school_lms_link"].ToString();


                            // list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, simsobj);
        }


        [Route("getUserDetails")]
        public HttpResponseMessage getUserDetails(string uname)
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                DataSet dr = db.ExecuteStoreProcedureDS("GetData",
                    new List<SqlParameter>() { new SqlParameter("@tbl_name", "[comn].[comn_user]"),
                            new SqlParameter("@tbl_col_name1", "*"),
                            new SqlParameter("@tbl_cond","comn_user_name=" + "'" + uname + "'"),
                            });
                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, dr);
            }

        }



        [Route("getAllMedicines")]
        public HttpResponseMessage getAllMedicines(int pageIndex, int PageSize)
        {
            List<Medicine> result = new List<Medicine>();
            List<SqlParameter> lst = new List<SqlParameter>();
            HttpStatusCode msg;
            int total = 0, skip = 0;
            lst.Add(new SqlParameter("@opr", "S"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_medicine", lst);
                    while (dr.Read())
                    {
                        Medicine c = new Medicine();
                        c.Sims_Medicine_Code = dr["sims_medicine_code"].ToString();
                        c.Sims_Medicine_Name = dr["sims_medicine_name"].ToString();
                        c.Sims_Medicine_Type = dr["sims_medicine_type"].ToString();
                        c.Sims_Medicine_ExpiryAlert = dr["sims_medicine_expirey_alert"].ToString();
                        c.Sims_Medicine_BatchNo = dr["sims_medicine_batch_number"].ToString();
                        c.Sims_Medicine_Prod_Date = DateTime.Parse(dr["sims_medicine_production_date"].ToString()).ToShortDateString();
                        c.Sims_Medicine_Exp_Date = DateTime.Parse(dr["sims_medicine_expirey_date"].ToString()).ToShortDateString();
                        c.Sims_Medicine_Type_Name = dr["sims_medicine_typename"].ToString();
                        c.Sims_Medicine_AlertName = dr["sims_medicine_expirey_alertname"].ToString();
                        result.Add(c);
                        total = result.Count;
                        skip = PageSize * (pageIndex - 1);
                    }
                    msg = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                msg = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(msg, result.Skip(skip).Take(PageSize).ToList());
        }

        [Route("getCriteria")]
        public HttpResponseMessage GetAdmissionCriterias(int pageIndex, int PageSize)
        {
            List<AdmissionCriteria> result = new List<AdmissionCriteria>();
            List<SqlParameter> lst = new List<SqlParameter>();
            HttpStatusCode msg;
            int total = 0, skip = 0;
            lst.Add(new SqlParameter("@opr", "S"));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_criteria", lst);
                    while (dr.Read())
                    {
                        AdmissionCriteria c = new AdmissionCriteria();
                        c.Sims_Criteria_code = dr["sims_criteria_code"].ToString();
                        c.Sims_Criteria_Name_en = dr["sims_criteria_name_en"].ToString();
                        //c.Sims_Criteria_Name_ar  =dr[""].ToString();
                        //c.Sims_Criteria_Name_fr  =dr[""].ToString();
                        c.Sims_Criteria_Name_ot = dr["sims_criteria_name_ot"].ToString();
                        c.Sims_Criteria_Type = dr["sims_criteria_type"].ToString();
                        c.Sims_Criteria_Type_Name = dr["sim_criteria_type_name"].ToString();
                        c.Sims_Criteria_status = dr["sims_criteria_status"].ToString();
                        c.Sims_Criteria_Mandatory = dr["sims_criteria_mandatory"].ToString();
                        result.Add(c);
                        total = result.Count;
                        skip = PageSize * (pageIndex - 1);
                    }
                    msg = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                msg = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(msg, result.Skip(skip).Take(PageSize).ToList());
        }

        [Route("CUDCriteria")]
        public HttpResponseMessage GetAdmissionCriterias(string opr, string Data)
        {
            Message message = new Message();
            AdmissionCriteria data = Newtonsoft.Json.JsonConvert.DeserializeObject<AdmissionCriteria>(Data);
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@opr", opr));
            lst.Add(new SqlParameter("@sims_criteria_code", data.Sims_Criteria_code));
            lst.Add(new SqlParameter("@sims_criteria_name_en", data.Sims_Criteria_Name_en));
            lst.Add(new SqlParameter("@sims_criteria_name_ot", data.Sims_Criteria_Name_ot));
            lst.Add(new SqlParameter("@sims_criteria_mandatory", data.Sims_Criteria_Mandatory));
            lst.Add(new SqlParameter("@sims_criteria_type", data.Sims_Criteria_Type));
            lst.Add(new SqlParameter("@sims_criteria_status", data.Sims_Criteria_status));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_admission_criteria", lst);
                    if (dr.RecordsAffected > 0)
                    {
                        if (opr.Equals("U"))
                            message.strMessage = "Application Information Updated Sucessfully!!";
                        else if (opr.Equals("I"))
                            message.strMessage = "Application Information Added Sucessfully!!";
                        else
                            message.strMessage = "Application Information Deleted Sucessfully!!";

                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
            }
            catch (Exception ex)
            {
                if (opr.Equals("U"))
                    message.strMessage = "Error In Updating Application Information !!";
                else if (opr.Equals("I"))
                    message.strMessage = "Error In Inserting Application Information !!";
                else
                    message.strMessage = "Error In Deleting Application Information !!";

                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("ResetPassword")]
        public HttpResponseMessage ResetPassword(string str)
        {
            bool msg = false;
            ResetPassowrd data = Newtonsoft.Json.JsonConvert.DeserializeObject<ResetPassowrd>(str);
            List<SqlParameter> lst = new List<SqlParameter>();
            try
            {
                lst.Add(new SqlParameter("@comn_user_password", data.OldPasscode));
                lst.Add(new SqlParameter("@comn_user_new_password", data.NewPasscode));
                lst.Add(new SqlParameter("@comn_user_name", data.UserName));
                lst.Add(new SqlParameter("@opr", "U"));
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_change_password", lst);
                    if (dr.RecordsAffected > 0)
                        msg = true;
                    else
                        msg = false;
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        [Route("ResetPassword")]
        public HttpResponseMessage ResetPassword(ResetPassowrd str)
        {
            bool msg = false;
            //ResetPassowrd data = Newtonsoft.Json.JsonConvert.DeserializeObject<ResetPassowrd>(str);
            List<SqlParameter> lst = new List<SqlParameter>();
            try
            {
                lst.Add(new SqlParameter("@comn_user_password", str.OldPasscode));
                lst.Add(new SqlParameter("@comn_user_new_password", str.NewPasscode));
                lst.Add(new SqlParameter("@comn_user_name", str.UserName));
                lst.Add(new SqlParameter("@opr", "U"));
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_change_password", lst);
                    if (dr.RecordsAffected > 0)
                        msg = true;
                    else
                        msg = false;
                }
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }



        [Route("GetAllUserRole")]
        public HttpResponseMessage GetAllUserRole()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<UserRole> result = new List<UserRole>();
            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[comn_user_role]", lst);
                try
                {
                    while (dr.Read())
                    {
                        UserRole ur = new UserRole();
                        ur.Comn_User_Role_Code = dr["comn_role_code"].ToString();
                        ur.Comn_User_Role_Name = dr["comn_role_name"].ToString();
                        ur.Comn_User_Role_Status = dr["comn_role_status"].ToString();
                        result.Add(ur);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, result);
                }
            }
        }

        [Route("GetAllIcon")]
        public HttpResponseMessage GetAllIcon(string app_code)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            UserRole ur = new UserRole();
            lst.Add(new SqlParameter("@opr", "B"));
            lst.Add(new SqlParameter("@appl_code", app_code));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_user_applicaton_favorite]", lst);
                try
                {
                    while (dr.Read())
                    {

                        if (int.Parse(dr["help"].ToString()) > 0)
                            ur.help = true;
                        if (int.Parse(dr["video_help"].ToString()) > 0)
                            ur.video_help = true;
                        if (int.Parse(dr["dependancy"].ToString()) > 0)
                            ur.dependancy = true;
                        //  result.Add(ur);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ur);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ur);
                }
            }
        }

        [Route("GetAllUserRoleByIndex")]
        public HttpResponseMessage GetAllUserRoleByIndex(int pageIndex, int PageSize)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<UserRole> result = new List<UserRole>();
            int total = 0, skip = 0;

            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[comn_user_role]", lst);
                try
                {
                    while (dr.Read())
                    {
                        UserRole ur = new UserRole();
                        ur.Comn_User_Role_Code = dr["comn_role_code"].ToString();
                        ur.Comn_User_Role_Name = dr["comn_role_name"].ToString();
                        ur.Comn_User_Role_Status = dr["comn_role_status"].ToString();
                        result.Add(ur);
                        total = result.Count;
                        skip = PageSize * (pageIndex - 1);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result.Skip(skip).Take(PageSize).ToList());
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, result);
                }
            }
        }

        [Route("UserRoleDataOpr")]
        public HttpResponseMessage UserRoleDataOpr(string opr, string str)
        {

            UserRole data = Newtonsoft.Json.JsonConvert.DeserializeObject<UserRole>(str);
            List<SqlParameter> lst = new List<SqlParameter>();

            lst.Add(new SqlParameter("@opr", opr));
            lst.Add(new SqlParameter("@comn_role_code", data.Comn_User_Role_Code));
            lst.Add(new SqlParameter("@comn_role_name", data.Comn_User_Role_Name));
            lst.Add(new SqlParameter("@comn_role_status", data.Comn_User_Role_Status));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("[comn_user_role]", lst);
                if (dr.RecordsAffected > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, dr);
            }
        }

        [Route("GetAllUserGroup")]
        public HttpResponseMessage GetAllUserGroup(int pageIndex, int PageSize)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<UserGroup> result = new List<UserGroup>(); int total = 0, skip = 0;
            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_group", lst);
                try
                {
                    while (dr.Read())
                    {
                        UserGroup ur = new UserGroup();
                        ur.Comn_User_Group_Code = dr["comn_user_group_code"].ToString();
                        ur.Comn_User_Group_Name = dr["comn_user_group_name"].ToString();
                        result.Add(ur);
                        total = result.Count;
                        skip = PageSize * (pageIndex - 1);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result.Skip(skip).Take(PageSize).ToList());
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, result);
                }
            }
        }

        [Route("UserGroupDataOpr")]
        public HttpResponseMessage UserGroupDataOpr(string opr, string str)
        {
            UserGroup data = Newtonsoft.Json.JsonConvert.DeserializeObject<UserGroup>(str);
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@opr", opr));
            lst.Add(new SqlParameter("@comn_user_group_code", data.Comn_User_Group_Code));
            lst.Add(new SqlParameter("@comn_user_group_name", data.Comn_User_Group_Name));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                DataSet dr = db.ExecuteStoreProcedureDS("comn_user_group", lst);
                if (dr.Tables[0].Rows.Count == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, dr);
            }
        }

        [Route("getReligion")]
        public HttpResponseMessage getReligion(int pageIndex, int PageSize)
        {
            List<Sims009> religion = new List<Sims009>();
            int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_religion",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims009 simsobj = new Sims009();
                            simsobj.sims_religion_code = dr["sims_religion_code"].ToString();
                            simsobj.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                            simsobj.sims_religion_name_ot = dr["sims_religion_name_ot"].ToString();
                            simsobj.sims_religion_img = dr["sims_religion_img"].ToString();
                            simsobj.sims_religion_status = dr["sims_religion_status"].ToString().Equals("A") ? true : false;
                            religion.Add(simsobj);
                            total = religion.Count();
                            skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, religion.Skip(skip).Take(PageSize).ToList());
        }

        [Route("updateInsertDeleteReligion")]
        public HttpResponseMessage getUpdateInsertDeleteReligion(string opr, string data)
        {

            Sims009 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims009>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_religion",
                            new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", opr),
                            new SqlParameter("@sims_religion_code", simsobj.sims_religion_code),
                            new SqlParameter("@sims_religion_name_en", simsobj.sims_religion_name_en),
                            new SqlParameter("@sims_religion_name_ot", simsobj.sims_religion_name_ot),
                            new SqlParameter("@sims_religion_img", simsobj.sims_religion_img),
                            new SqlParameter("@sims_religion_status",simsobj.sims_religion_status == true?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (opr.Equals("U"))
                                message.strMessage = "Religion Information Updated Sucessfully!!";
                            else if (opr.Equals("I"))
                                message.strMessage = "Religion Information Added Sucessfully!!";
                            else if (opr.Equals("D"))
                                message.strMessage = "Religion Information Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (opr.Equals("U"))
                    message.strMessage = "Error In Updating Religion Information!!";
                else if (opr.Equals("I"))
                    message.strMessage = "Error In Adding Religion Information!!";
                else if (opr.Equals("D"))
                    message.strMessage = "Error In Deleting Religion Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("GetAllCountries")]
        public HttpResponseMessage GetAllCountries()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Country> result = new List<Country>();
            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims_country", lst);
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Country c = new Country();
                        c.sims_country_code = dr["sims_country_code"].ToString();
                        c.sims_country_name_en = dr["sims_country_name_en"].ToString();
                        c.sims_country_name_ar = dr["sims_country_name_ar"].ToString();
                        c.sims_country_name_fr = dr["sims_country_name_fr"].ToString();
                        c.sims_country_name_ot = dr["sims_country_name_ot"].ToString();
                        c.sims_continent = dr["sims_continent"].ToString();
                        c.sims_region_code = dr["sims_region_code"].ToString();
                        c.sims_region_code_name = dr["sims_region_name_en"].ToString();
                        c.sims_currency_code = dr["sims_currency_code"].ToString();
                        c.sims_currency_code_name = dr["sims_currency_dec"].ToString();
                        c.sims_currency_dec = dr["excg_curcy_desc"].ToString();
                        c.sims_country_img = ""; //dr[""].ToString();
                        c.sims_status = dr["sims_status"].ToString() == "A";
                        result.Add(c);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
        }

        [Route("GetAllLanguagesByIndex")]
        public HttpResponseMessage GetAllLanguagesByIndex(int pageIndex, int PageSize)
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Language> result = new List<Language>();
            int total = 0, skip = 0;
            lst.Add(new SqlParameter("@opr", "S"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_language", lst);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Language c = new Language();
                            c.sims_Language_Code = dr["sims_language_code"].ToString();
                            c.sims_Language_Name_En = dr["sims_language_name_en"].ToString();
                            c.sims_Language_Name_Ar = dr["sims_language_name_ar"].ToString();
                            c.sims_Language_Name_Fr = dr["sims_language_name_fr"].ToString();
                            c.sims_Language_Name_Ot = dr["sims_language_name_ot"].ToString();
                            c.sims_Language_Status = dr["sims_language_status"].ToString();
                            result.Add(c);
                            total = result.Count;
                            skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result.Skip(skip).Take(PageSize).ToList());
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("LanguageDataOpr")]
        public HttpResponseMessage LanguageDataOpr(string opr, string str)
        {
            Language data = Newtonsoft.Json.JsonConvert.DeserializeObject<Language>(str);
            List<SqlParameter> lst = new List<SqlParameter>();
            List<Language> result = new List<Language>();
            Message message = new Message();
            lst.Add(new SqlParameter("@opr", opr));
            lst.Add(new SqlParameter("@sims_language_code", data.sims_Language_Code));
            lst.Add(new SqlParameter("@sims_language_name_en", data.sims_Language_Name_En));
            lst.Add(new SqlParameter("@sims_language_name_ar", data.sims_Language_Name_Ar));
            lst.Add(new SqlParameter("@sims_language_name_fr", data.sims_Language_Name_Fr));
            lst.Add(new SqlParameter("@sims_language_name_ot", data.sims_Language_Name_Ot));
            lst.Add(new SqlParameter("@sims_language_status", data.sims_Language_Status));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_language", lst);
                    if (dr.RecordsAffected > 0)
                    {
                        if (opr.Equals("U"))
                            message.strMessage = "Application Information Updated Sucessfully!!";
                        else if (opr.Equals("I"))
                            message.strMessage = "Application Information Added Sucessfully!!";
                        else
                            message.strMessage = "Application Information Deleted Sucessfully!!";

                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    message.systemMessage = ex.Message;
                    message.messageType = MessageType.Error;
                    message.strMessage = "Error";
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
        }

        [Route("getState")]
        public HttpResponseMessage getState(int pageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getState(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "SETUP", "State"));

            List<Sims055> State_list = new List<Sims055>();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_State",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims055 simsobj = new Sims055();
                            simsobj.sims_state_code = dr["sims_state_code"].ToString();
                            simsobj.sims_country_code = dr["sims_country_code"].ToString();
                            simsobj.sims_country_code_name = dr["sims_country_name_en"].ToString();
                            simsobj.sims_state_name_en = dr["sims_state_name_en"].ToString();
                            simsobj.sims_state_name_ar = dr["sims_state_name_ar"].ToString();
                            simsobj.sims_state_name_fr = dr["sims_state_name_fr"].ToString();
                            simsobj.sims_state_name_ot = dr["sims_state_name_ot"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A");
                            State_list.Add(simsobj);

                            total = State_list.Count();
                            skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, State_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, State_list.Skip(skip).Take(PageSize).ToList());
        }

        [Route("getCountry")]
        public HttpResponseMessage getCountry()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCountry(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "SETUP", "State"));

            List<Sims055> mod_list = new List<Sims055>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "[sims].[sims_country]"),
                            new SqlParameter("@tbl_col_name1", "[sims_country_name_en],[sims_country_code]"),
                            new SqlParameter("@tbl_cond", "[sims_status]='A'"),
                            new SqlParameter("@tbl_ordby", "[sims_country_name_en]")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims055 simsobj = new Sims055();
                            simsobj.sims_country_code_name = dr["sims_country_name_en"].ToString();
                            simsobj.sims_country_code = dr["sims_country_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }




        [Route("getapplicationPdfUrl")]
        public HttpResponseMessage getapplicationPdfUrl(string appl_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCriteriaType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllCriteriaType"));

            string url = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "comn.comn_application"),
                                new SqlParameter("@tbl_col_name1", "comn_appl_help_url"),
                                new SqlParameter("@tbl_cond","comn_appl_location_html=" + "'" + appl_code + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            url = dr["comn_appl_help_url"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, url);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, url);
            }
        }


        [Route("getapplicationVideoUrl")]
        public HttpResponseMessage getapplicationVideoUrl(string appl_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCriteriaType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllCriteriaType"));

            string url = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "comn.comn_application"),
                                new SqlParameter("@tbl_col_name1", "comn_appl_video_url"),
                                new SqlParameter("@tbl_cond","comn_appl_location_html=" + "'" + appl_code + "'"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            url = dr["comn_appl_video_url"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, url);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, url);
            }
        }


        [Route("getCity")]
        public HttpResponseMessage getCity(int pageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCity(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CITY"));

            List<Sims056> city_list = new List<Sims056>();
            int total = 0, skip = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_city",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims056 simsobj = new Sims056();
                            simsobj.city_code = dr["sims_city_code"].ToString();
                            simsobj.state_name = dr["sims_state_name"].ToString();
                            simsobj.city_name_eng = dr["sims_city_name_en"].ToString();
                            simsobj.city_name_ar = dr["sims_city_name_ar"].ToString();
                            simsobj.city_name_fr = dr["sims_city_name_fr"].ToString();
                            simsobj.city_name_ot = dr["sims_city_name_ot"].ToString();
                            simsobj.city_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            city_list.Add(simsobj);

                            total = city_list.Count();
                            skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, city_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, city_list.Skip(skip).Take(PageSize).ToList());
        }

        [Route("GetAllStatesByCountry")]
        public HttpResponseMessage GetAllStatesByCountry(string country_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllStatesByCountry(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "STUDENT", "CITY", country_code));
            List<Sims056> mod_list = new List<Sims056>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_state]"),
                                new SqlParameter("@tbl_col_name1", "[sims_state_name_en],[sims_state_code]"),
                                new SqlParameter("@tbl_cond", "[sims_country_code]=" + country_code + "and [sims_status]='A'"),
                                new SqlParameter("@tbl_ordby", "[sims_state_code]")
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims056 simsobj = new Sims056();
                            simsobj.state_name = dr["sims_state_name_en"].ToString();
                            simsobj.state_code = dr["sims_state_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getClassTeacher")]
        public HttpResponseMessage getClassTeacher(string curCode, string academicYear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getClassTeacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CLASSTEACHER"));

            List<Sims179> State_list = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("PP_sims_class_teacher",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", curCode),
                            new SqlParameter("@sims_academic_year", academicYear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 simsobj = new Sims179();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_teacher_code = dr["sims_class_teacher_code"].ToString();
                            simsobj.sims_class_name = dr["Class"].ToString();
                            simsobj.sims_emp_code = dr["sims_employee_code"].ToString();
                            simsobj.sims_teacher_name = dr["TeacherName"].ToString();
                            simsobj.sims_status = dr["Status"].ToString().Equals("Y") ? true : false;
                            State_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, State_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, State_list);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "S"),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear(string curCode)
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",curCode)
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            sequence.sims_academic_year_status = dr["sims_academic_year_status"].ToString();
                            try
                            {
                                sequence.sims_academic_year_start_date = dr["sims_academic_year_start_date"].ToString();
                                sequence.sims_academic_year_end_date = dr["sims_academic_year_end_date"].ToString();
                            }
                            catch (Exception x)
                            {

                                 
                            }
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "N"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getAllGradesdvps")]
        public HttpResponseMessage getAllGradesdvps()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradesdvps(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "P"),
                               //new SqlParameter("@sims_cur_code", cur_code),
                               //new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }



        [Route("getAllGradesFeePost")]
        public HttpResponseMessage getAllGradesFeePost(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Grades", "getAllGrades"));

            List<simsClass> mod_list = new List<simsClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_grade_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "Z"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", ac_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass objNew = new simsClass();
                            objNew.sims_grade_code = dr["sims_grade_code"].ToString();
                            objNew.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }


        [Route("getSectionFromGrade")]
        public HttpResponseMessage getSectionFromGrade(string cur_code, string grade_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                 new SqlParameter("@sims_grade_code", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getTeacherList")]
        public HttpResponseMessage getTeacherList()
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "sims.sims_teacher"),
                            new SqlParameter("@tbl_col_name1", "sims_teacher_code,sims_teacher_name"),
                            new SqlParameter("@tbl_cond", "sims_status='A'")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            sequence.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getSimsDocMaster")]
        public HttpResponseMessage getSimsDocMaster()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSimsDocMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SIMSDOCMASTER"));

            List<Sims020> doc_list = new List<Sims020>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_doc_master",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "S"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims020 simsobj = new Sims020();
                            simsobj._sims_cur_code = dr["sims_doc_cur_code"].ToString();
                            simsobj._sims_doc_code = dr["sims_doc_code"].ToString();
                            simsobj._sims_cur_cur_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj._sims_doc_create_date = Convert.ToDateTime(dr["sims_doc_create_date"].ToString());
                            simsobj._sims_doc_created_by = dr["sims_doc_created_by"].ToString();
                            simsobj._sims_doc_desc = dr["sims_doc_desc"].ToString();
                            simsobj._sims_mod_name = dr["comn_mod_name_en"].ToString();
                            simsobj._sims_doc_status = dr["sims_doc_status"].ToString() == "A" ? true : false;
                            doc_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, doc_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, doc_list);
        }

        [Route("getModulename")]
        public HttpResponseMessage getModulename()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModulename(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "MODULENAME"));

            List<Sims020> mod_list = new List<Sims020>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "comn.comn_module"),
                                new SqlParameter("@tbl_col_name1", "comn_mod_name_en"),
                                new SqlParameter("@tbl_cond", "comn_mod_status='A'"),
                                new SqlParameter("@tbl_ordby", "comn_mod_name_en")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims020 simsobj = new Sims020();
                            simsobj._sims_mod_name = dr["comn_mod_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("CUDDocMasterDetails")]
        public HttpResponseMessage CUDDocMasterDetails(string opr, string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDDocMasterDetails(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "STUDENT", "DOCMASTER", opr));

            Sims020 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims020>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_doc_master",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", opr),
                                new SqlParameter("@sims_doc_mod_name", simsobj._sims_mod_name),
                                new SqlParameter("@sims_doc_cur_name", simsobj._sims_cur_code),
                                new SqlParameter("@sims_doc_code", simsobj._sims_doc_code),
                                new SqlParameter("@sims_doc_desc", simsobj._sims_doc_desc),
                                new SqlParameter("@sims_doc_create_date", simsobj._sims_doc_create_date),
                                new SqlParameter("@sims_doc_created_by", simsobj._sims_doc_created_by),
                                new SqlParameter("@sims_doc_status", simsobj._sims_doc_status == true?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (opr.Equals("U"))
                                message.strMessage = "DocumentMasterDetails Information Updated Sucessfully!!";
                            else if (opr.Equals("I"))
                                message.strMessage = "DocumentMasterDetails Information Added Sucessfully!!";
                            else if (opr.Equals("D"))
                                message.strMessage = "DocumentMasterDetails Information Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (opr.Equals("U"))
                    message.strMessage = "Error In Updating DocumentMasterDetails Information!!";
                else if (opr.Equals("I"))
                    message.strMessage = "Error In Adding DocumentMasterDetails Information!!";
                else if (opr.Equals("D"))
                    message.strMessage = "Error In Deleting DocumentMasterDetails Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        //Transport All
        //-----------------------------------FOR_BUSWise_Transfer_Transport----------------------------------//

        [Route("getAllrotenameforBuswiseTransfer")]
        public HttpResponseMessage getAllrotenameforBuswiseTransfer(string academic_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllrotenameforBuswiseTransfer(),PARAMETERS :: NO";

            List<Sims085> bus_wise_route_list = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_bus_trasfer_proc]",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "R"),
                                 new SqlParameter("@sims_transport_academic_year", academic_yr),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();

                            bus_wise_route_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_wise_route_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_wise_route_list);
            }
        }

        [Route("getAllDirectionforBuswiseTransfer")]
        public HttpResponseMessage getAllDirectionforBuswiseTransfer(string academic_yr, string route_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDirectionforBuswiseTransfer(),PARAMETERS :: NO";

            List<Sims085> bus_wise_direction_list = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_bus_trasfer_proc]",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "T"),
                                 new SqlParameter("@sims_transport_academic_year", academic_yr),
                                 new SqlParameter("@sims_transport_route_code", route_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();

                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();

                            bus_wise_direction_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_wise_direction_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_wise_direction_list);
            }
        }

        //-----------------------------------FOR_BUSWise_Transfer_Transport----------------------------------//
        [Route("getAllFeeType")]
        public HttpResponseMessage getAllFeeType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllFeeType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllFeeType"));

            List<Sim079> fee_list = new List<Sim079>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'Z'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim079 simsobj = new Sim079();
                            simsobj.sims_fee_code = dr["sims_fee_code"].ToString();
                            simsobj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fee_list);
        }

        [Route("getFeeFrequency")]
        public HttpResponseMessage getFeeFrequency()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFeeFrequency(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getFeeFrequency"));

            List<Sim079> feef_list = new List<Sim079>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim079 simsobj = new Sim079();
                            simsobj.FeeFrequency = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_frequency = dr["sims_appl_parameter"].ToString();
                            feef_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, feef_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, feef_list);
        }

        [Route("getFeeCategory")]
        public HttpResponseMessage getFeeCategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFeeCategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getFeeCategory"));

            List<Sim079> cat_list = new List<Sim079>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_fee_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","H"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim079 simsobj = new Sim079();
                            //simsobj.sims_fee_category_description = dr["sims_appl_form_field_value1"].ToString();
                            //simsobj.sims_transport_category = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            simsobj.sims_transport_category = dr["sims_fee_category"].ToString();
                            cat_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, cat_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, cat_list);
        }

        [Route("getCaretakerCode")]
        public HttpResponseMessage getCaretakerCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCaretakerCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Fleet", "getAllEmpDetails"));

            string caretakercode = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_caretaker_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            caretakercode = dr["em_number"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, caretakercode);

            }
            return Request.CreateResponse(HttpStatusCode.OK, caretakercode);
        }

        [Route("getAllOwnership")]
        public HttpResponseMessage getAllOwnership()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllOwnership(),PARAMETERS :: NO)";
            Log.Debug(string.Format(debug, "FLEET", "getAllOwnership"));

            List<Sims086> ownership_list = new List<Sims086>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_vehicle_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims086 simsobj = new Sims086();
                            simsobj.sims_transport_vehicle_ownership_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_vehicle_ownership = dr["sims_appl_parameter"].ToString();
                            ownership_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ownership_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ownership_list);
            }
        }

        [Route("getAllTransmission")]
        public HttpResponseMessage getAllTransmission()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTransmission(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllTransmission"));

            List<Sims086> tranmission_list = new List<Sims086>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_vehicle_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "B")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims086 simsobj = new Sims086();
                            simsobj.sims_transport_vehicle_transmission_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_vehicle_transmission = dr["sims_appl_parameter"].ToString();
                            tranmission_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, tranmission_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, tranmission_list);
            }
        }

        [Route("getAllTerms")]
        public HttpResponseMessage getAllTerms()
        {
            List<Sims179> lstCuriculum = new List<Sims179>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_term]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_cur_code = dr["sims_cur_code"].ToString();
                            sequence.sims_term_desc_ar = dr["sims_term_desc_ar"].ToString();
                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAllEmp")]
        public HttpResponseMessage getAllEmp()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllEmp(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllEmp"));

            List<Sims087> type_list = new List<Sims087>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_caretaker_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            simsobj.sims_employee_code_name = dr["sims_employee_name"].ToString();
                            simsobj.emp_full_name = dr["emp_full_name"].ToString();
                            simsobj.sims_employee_code = dr["em_number"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }

        [Route("getAllEmpDriver")]
        public HttpResponseMessage getAllEmpDriver()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllEmpDriver(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllEmpDriver"));

            List<Sims087> emp_driver = new List<Sims087>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "E")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            simsobj.sims_employee_code_name = dr["sims_employee_name"].ToString();
                            simsobj.emp_full_name = dr["emp_full_name"].ToString();
                            simsobj.sims_employee_code = dr["em_number"].ToString();
                            emp_driver.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_driver);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_driver);
            }
        }

        [Route("getDriverGender")]
        public HttpResponseMessage getDriverGender()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllTransmission(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getDriverGender"));

            List<Sims088> gender_list = new List<Sims088>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "G")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 simsobj = new Sims088();

                            simsobj.sims_driver_gender_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_driver_gender = dr["sims_appl_parameter"].ToString();
                            gender_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, gender_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, gender_list);
            }
        }

        [Route("getDriverType")]
        public HttpResponseMessage getDriverType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDriverType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getDriverType"));

            List<Sims088> dtype_list = new List<Sims088>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "T")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 simsobj = new Sims088();
                            simsobj.sims_driver_type_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_driver_type = dr["sims_appl_parameter"].ToString();
                            dtype_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, dtype_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, dtype_list);
            }
        }

        [Route("getVehiclecategory")]
        public HttpResponseMessage getVehiclecategory()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehiclecategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getVehiclecategory"));

            List<Sims088> Vehicle_cat = new List<Sims088>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 simsobj = new Sims088();
                            simsobj.sims_driver_license_vehicle_categoryname = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_driver_license_vehicle_category = dr["sims_appl_parameter"].ToString();
                            Vehicle_cat.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Vehicle_cat);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Vehicle_cat);
            }
        }

        [Route("getVehicleMode")]
        public HttpResponseMessage getVehicleMode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehicleMode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getVehicleMode"));

            List<Sims088> vehicle_mode = new List<Sims088>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 simsobj = new Sims088();
                            simsobj.sims_driver_license_vehicle_mode_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_driver_license_vehicle_mode = dr["sims_appl_parameter"].ToString();
                            vehicle_mode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, vehicle_mode);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, vehicle_mode);
            }
        }


        [Route("getVisaType")]
        public HttpResponseMessage getVehiclemode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVisaType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getVisaType"));

            List<Sims088> Visa_Type = new List<Sims088>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "V")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 simsobj = new Sims088();
                            simsobj.sims_driver_visa_type_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_driver_visa_type = dr["sims_appl_parameter"].ToString();
                            Visa_Type.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Visa_Type);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Visa_Type);
            }
        }


        [Route("getRouteDirec")]
        public HttpResponseMessage getRouteDirec()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRouteDirec(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getRouteDirec"));

            List<Sims083> Route_Direct = new List<Sims083>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "R")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 simsobj = new Sims083();
                            simsobj.sims_transport_route_direction_name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_appl_parameter"].ToString();
                            Route_Direct.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Route_Direct);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Route_Direct);
            }
        }

        [Route("getVehicleCode")]
        public HttpResponseMessage getVehicleCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehicleCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getVehicleCode"));

            List<Sims083> VehicleCode = new List<Sims083>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "V")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 simsobj = new Sims083();
                            simsobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            simsobj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            VehicleCode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, VehicleCode);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, VehicleCode);
            }
        }


        [Route("getDriverCode")]
        public HttpResponseMessage getDriverCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDriverCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getDriverCode"));

            List<Sims083> DriverCode = new List<Sims083>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 simsobj = new Sims083();
                            simsobj.sims_driver_name = dr["sims_driver_name"].ToString();
                            simsobj.sims_driver_code = dr["sims_driver_code"].ToString();
                            DriverCode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, DriverCode);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, DriverCode);
            }
        }

        [Route("getCaretakerNames")]
        public HttpResponseMessage getCaretakerNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCaretakerNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getCaretakerNames"));

            List<Sims083> caretakernm = new List<Sims083>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "N")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 simsobj = new Sims083();
                            simsobj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();
                            simsobj.sims_caretaker_code = dr["sims_caretaker_code"].ToString();
                            caretakernm.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, caretakernm);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, caretakernm);
            }
        }

        [Route("getAllAcademicYearstop")]
        public HttpResponseMessage getAllAcademicYearstop()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYearstop(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllAcademicYear"));

            List<Sims084> activeyear = new List<Sims084>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_stop_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims084 simsobj = new Sims084();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            activeyear.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, activeyear);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, activeyear);
            }
        }


        [Route("getAllStopName")]
        public HttpResponseMessage getAllStopName(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllStopName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllStopName"));

            List<Sims085> stopname = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "R"),
                                 new SqlParameter("@sims_academic_year",Acayear)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            simsobj.sims_transport_route_stop_code = dr["sims_transport_stop_code"].ToString();
                            stopname.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, stopname);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, stopname);
            }
        }

        [Route("getAllRouteName")]
        public HttpResponseMessage getAllRouteName(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRouteName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllRouteName"));

            List<Sims085> routename = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C"),
                                 new SqlParameter("@sims_academic_year",Acayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();

                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();

                            routename.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename);
            }
        }

        [Route("getDirectionByRouteName")]
        public HttpResponseMessage getDirectionByRouteName(string route_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDirectionByRouteName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "ALLDIRECTION"));

            List<Sims085> Direct_list = new List<Sims085>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_transport_route_code", route_code),
                                //new SqlParameter("@sims_transport_route_name", route_name),
                                new SqlParameter("@sims_academic_year", aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            Direct_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Direct_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Direct_list);
            }
        }

        //End

        [Route("getAllMedicalExaminationName")]
        public HttpResponseMessage getAllMedicalExaminationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllMedicalExaminationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllMedicalExaminationName"));

            List<Sims093> medical_en_list = new List<Sims093>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_medical_visit_examination]"),
                                new SqlParameter("@tbl_col_name1", "[sims_examination_name],sims_examination_code")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsObj = new Sims093();
                            simsObj.sims_examination_name = dr["sims_examination_name"].ToString();
                            simsObj.sims_examination_code = dr["sims_examination_code"].ToString();
                            medical_en_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, medical_en_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, medical_en_list);
            }
        }

        [Route("getMedicalExaminations")]
        public HttpResponseMessage getMedicalExaminations(int pageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMedicalExaminations(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "MedicalExaminations"));

            List<Sims093> medexamination_list = new List<Sims093>();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_examination",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_visit_number", ""),
                                new SqlParameter("@sims_serial_number", 0),
                                new SqlParameter("@sims_examination_name", ""),
                                new SqlParameter("@sims_examination_value", "")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsobj = new Sims093();
                            simsobj.sims_visit_number = dr["sims_visit_number"].ToString();
                            simsobj.sims_serial_number = int.Parse(dr["sims_serial_number"].ToString());
                            simsobj.sims_examination_name = dr["sims_examination_name"].ToString();
                            simsobj.sims_examination_code = dr["sims_examination_code"].ToString();
                            simsobj.sims_examination_value = dr["sims_examination_value"].ToString();
                            medexamination_list.Add(simsobj);
                            total = medexamination_list.Count;
                            skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, medexamination_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, medexamination_list.Skip(skip).Take(PageSize).ToList());
        }

        [Route("getAutoGenerateMedicalSerialNumber")]
        public HttpResponseMessage getAutoGenerateMedicalSerialNumber()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateMedicalSerialNumber(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateMedicalSerialNumber"));

            string serialnumber = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_medical_examination]"),
                                new SqlParameter("@tbl_col_name1", "max(convert(int,sims_serial_number))+1 as max"),
                                new SqlParameter("@tbl_cond", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            serialnumber = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, serialnumber);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, serialnumber);
            }
        }

        [Route("getAllMedicalVisitNumbers")]
        public HttpResponseMessage getAllMedicalVisitNumbers()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllMedicalVisitNumbers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllMedicalVisitNumbers"));

            List<Sims093> medical_vn_list = new List<Sims093>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@tbl_name", "[sims].[sims_medical_visit]"),
                               new SqlParameter("@tbl_col_name1", "[sims_medical_visit_number]")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims093 simsObj = new Sims093();
                            simsObj.sims_visit_number = dr["sims_medical_visit_number"].ToString();
                            medical_vn_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, medical_vn_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, medical_vn_list);
            }
        }

        [Route("CUDMedicalExaminationDetails")]
        public HttpResponseMessage CUDMedicalExaminationDetails(string data, string opr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDMedicalExaminationDetails(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "STUDENT", "CUDMedicalExaminationDetails", opr));

            Sims093 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims093>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_moderator",
                            new List<SqlParameter>() 
                         {                             
                                new SqlParameter("@opr", opr),
                                new SqlParameter("@sims_visit_number", simsobj.sims_visit_number),
                                new SqlParameter("@sims_serial_number", simsobj.sims_serial_number),
                                new SqlParameter("@sims_examination_name", simsobj.sims_examination_name),
                                new SqlParameter("@sims_examination_value", simsobj.sims_examination_value)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (opr.Equals("U"))
                                message.strMessage = "MedicalExaminationDetails Information Updated Sucessfully!!";
                            else if (opr.Equals("I"))
                                message.strMessage = "MedicalExaminationDetails Information Added Sucessfully!!";
                            else if (opr.Equals("D"))
                                message.strMessage = "MedicalExaminationDetails Information Deleted Sucessfully!!";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (opr.Equals("U"))
                    message.strMessage = "Error In Updating MedicalExaminationDetails Information!!";
                else if (opr.Equals("I"))
                    message.strMessage = "Error In Adding MedicalExaminationDetails Information!!";
                else if (opr.Equals("D"))
                    message.strMessage = "Error In Deleting MedicalExaminationDetails Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CUDMedicalExamination")]
        public HttpResponseMessage CUDConsequenceDetails(List<Sims093> data)
        {
            bool inserted = false;

            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims093 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims_medical_examination]",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_visit_number", simsobj.sims_visit_number),
                                new SqlParameter("@sims_serial_number", simsobj.sims_serial_number),
                                new SqlParameter("@sims_examination_name", simsobj.sims_examination_code),
                                new SqlParameter("@sims_examination_value", simsobj.sims_examination_value)
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                        }
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                inserted = false;

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getMedicalImmunization")]
        public HttpResponseMessage getMedicalImmunization(int pageIndex, int PageSize)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getMedicalImmunization(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getMedicalImmunization"));

            List<Sims094> medimmunization_list = new List<Sims094>();
            int total = 0, skip = 0;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_immunization",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_code = int.Parse(dr["sims_immunization_code"].ToString());
                            simsobj.sims_immunization_age_group_name = dr["sims_immunization_age_group_code"].ToString();
                            simsobj.sims_immunization_desc = dr["sims_immunization_desc"].ToString();
                            simsobj.sims_immunization_dosage1 = dr["sims_immunization_dosage1"].ToString();
                            simsobj.sims_immunization_dosage2 = dr["sims_immunization_dosage2"].ToString();
                            simsobj.sims_immunization_dosage3 = dr["sims_immunization_dosage3"].ToString();
                            simsobj.sims_immunization_dosage4 = dr["sims_immunization_dosage4"].ToString();
                            simsobj.sims_immunization_dosage5 = dr["sims_immunization_dosage5"].ToString();
                            simsobj.sims_immunization_status = dr["sims_immunization_status"].ToString().Equals("A") ? true : false;

                            medimmunization_list.Add(simsobj);
                            total = medimmunization_list.Count;
                            skip = PageSize * (pageIndex - 1);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, medimmunization_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, medimmunization_list.Skip(skip).Take(PageSize).ToList());
        }

        [Route("getAutoGenerateMedicalImmunizationCode")]
        public HttpResponseMessage getAutoGenerateMedicalImmunizationCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateMedicalImmunizationCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateMedicalImmunizationCode"));

            List<Sims094> medimmunization_list = new List<Sims094>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_medical_immunization",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "P"),
                               new SqlParameter("@sims_immunization_code", "")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsobj = new Sims094();
                            simsobj.sims_immunization_code = int.Parse(dr["sims_immunization_code"].ToString());
                            medimmunization_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, medimmunization_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, medimmunization_list);
        }

        [Route("getAllMedicalImmunizationAgeGroupCode")]
        public HttpResponseMessage getAllMedicalImmunizationAgeGroupCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllMedicalImmunizationAgeGroupCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllMedicalImmunizationAgeGroupCode"));

            List<Sims094> groupcode_list = new List<Sims094>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_medical_immunization_age_group]"),
                                new SqlParameter("@tbl_col_name1", "[sims_immunization_age_group_desc]"),
                                new SqlParameter("@tbl_cond", "[sims_immunization_age_group_status]='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims094 simsObj = new Sims094();
                            simsObj.sims_immunization_age_group_name = dr["sims_immunization_age_group_desc"].ToString();
                            groupcode_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, groupcode_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, groupcode_list);
            }
        }

        [Route("getAllCriteriaType")]
        public HttpResponseMessage getAllCriteriaType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCriteriaType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllCriteriaType"));

            List<AdmissionCriteria> type_list = new List<AdmissionCriteria>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_parameter],[sims_appl_form_field_value1]"),
                                new SqlParameter("@tbl_cond", "[sims_appl_code]='"+ CommonStaticClass.Sims012_app_code+"' and [sims_appl_form_field]='"+CommonStaticClass.Sims012_criteria_name+"'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AdmissionCriteria simsobj = new AdmissionCriteria();
                            simsobj.Sims_Criteria_Type = dr["sims_appl_parameter"].ToString();
                            simsobj.Sims_Criteria_Type_Name = dr["sims_appl_form_field_value1"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }

        [Route("getAllMedicineType")]
        public HttpResponseMessage getAllMedicineType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllMedicineType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllMedicineType"));

            List<Medicine> type_list = new List<Medicine>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_parameter]"),
                                new SqlParameter("@tbl_col_name1", "[sims_appl_form_field_value1],[sims_appl_parameter]"),
                                new SqlParameter("@tbl_cond", "[sims_appl_code]='Sim097' AND sims_appl_form_field='Medicine Type'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Medicine simsobj = new Medicine();
                            simsobj.Sims_Medicine_Type_Name = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.Sims_Medicine_Type = dr["sims_appl_parameter"].ToString();
                            type_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, type_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, type_list);
            }
        }


        [Route("getAllModulesTiles")]
        public HttpResponseMessage getAllModulesTiles(string username)
        {
            List<comn_modules> module_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_module",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'J'),
                             new SqlParameter("@flag", 2),
                             new SqlParameter("@UserName", username)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_modules sequence = new comn_modules();
                            sequence.module_code = dr["comn_appl_mod_code"].ToString();
                            sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                            sequence.module_name_ar = dr["comn_mod_name_ar"].ToString();
                            sequence.module_name_fr = dr["comn_mod_name_fr"].ToString();
                            sequence.module_name_ot = dr["comn_mod_name_ot"].ToString();
                            sequence.module_create_date = dr["comn_mod_create_date"].ToString();
                            sequence.module_short_desc = dr["comn_mod_short_description"].ToString();
                            sequence.module_desc = dr["comn_mod_description"].ToString();
                            sequence.module_keywords = dr["comn_mod_keywords"].ToString();
                            sequence.module_status = dr["comn_mod_status"].ToString().Equals("A") ? true : false;
                            sequence.module_version = dr["comn_mod_version"].ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(dr["comn_mod_color"].ToString()))
                                {
                                    if (dr["comn_mod_color"].ToString().Length == 9)
                                        sequence.module_color = "#" + dr["comn_mod_color"].ToString().Substring(2);
                                    else if (dr["comn_mod_color"].ToString().Length == 7)
                                        sequence.module_color = dr["comn_mod_color"].ToString();
                                    else
                                        sequence.module_color = "#993366";
                                }
                                else
                                    sequence.module_color = "#993366";
                            }
                            catch (Exception de)
                            {
                                sequence.module_color = "#993366";
                            }
                            sequence.module_location = dr["comn_mod_location"].ToString();
                            sequence.module_dislpay_order = int.Parse(dr["comn_mod_display_order"].ToString());
                            sequence.module_Image = dr["comn_mod_img"].ToString();

                            module_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, module_list);

        }


        [Route("getAllModules")]
        public HttpResponseMessage getAllModules(string username)
        {
            List<comn_modules> module_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_module",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'J'),
                             new SqlParameter("@flag", 2),
                             new SqlParameter("@UserName", username)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_modules sequence = new comn_modules();
                            sequence.module_code = dr["comn_appl_mod_code"].ToString();
                            sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                            sequence.module_name_ar = dr["comn_mod_name_ar"].ToString();
                            sequence.module_name_fr = dr["comn_mod_name_fr"].ToString();
                            sequence.module_name_ot = dr["comn_mod_name_ot"].ToString();
                            sequence.module_create_date = dr["comn_mod_create_date"].ToString();
                            sequence.module_short_desc = dr["comn_mod_short_description"].ToString();
                            sequence.module_desc = dr["comn_mod_description"].ToString();
                            sequence.module_keywords = dr["comn_mod_keywords"].ToString();
                            sequence.module_status = dr["comn_mod_status"].ToString().Equals("A") ? true : false;
                            sequence.module_version = dr["comn_mod_version"].ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(dr["comn_mod_color"].ToString()))
                                {
                                    if (dr["comn_mod_color"].ToString().Length == 9)
                                        sequence.module_color = "#" + dr["comn_mod_color"].ToString().Substring(2);
                                    else if (dr["comn_mod_color"].ToString().Length == 7)
                                        sequence.module_color = dr["comn_mod_color"].ToString();
                                    else
                                        sequence.module_color = "#993366";
                                }
                                else
                                    sequence.module_color = "#993366";
                            }
                            catch (Exception de)
                            {
                                sequence.module_color = "#993366";
                            }
                            sequence.module_location = dr["comn_mod_location"].ToString();
                            sequence.module_dislpay_order = int.Parse(dr["comn_mod_display_order"].ToString());
                            sequence.module_Image = dr["comn_mod_img"].ToString();

                            List<Comn_parameterstypes> comnparams_list = new System.Collections.Generic.List<Comn_parameterstypes>();

                            //Call 'Group Types'
                            using (DBConnection db_type = new DBConnection())
                            {
                                db_type.Open();
                                SqlDataReader dr_type = db_type.ExecuteStoreProcedure("comn_module", new List<SqlParameter>() 
                                { 
                                        new SqlParameter("@opr", 'Y'),
                                        new SqlParameter("@UserName", username),
                                        new SqlParameter("@comn_mod_code", sequence.module_code),
                                });
                                if (dr_type.HasRows)
                                {
                                    while (dr_type.Read())
                                    {
                                        Comn_parameterstypes comnobj = new Comn_parameterstypes();
                                        comnobj.comn_type = dr_type["sims_appl_parameter"].ToString() + "_" + sequence.module_code;
                                        comnobj.comn_type_code = dr_type["sims_appl_parameter"].ToString();
                                        comnobj.comn_type_name = dr_type["sims_appl_form_field_value1"].ToString();
                                        comnobj.comn_type_name_ar = dr_type["sims_appl_form_field_value3"].ToString();

                                        using (DBConnection db_appl = new DBConnection())
                                        {
                                            db_appl.Open();
                                            List<Comn_applications> comnappl_list = new List<Comn_applications>();
                                            SqlDataReader dr_appl = db_appl.ExecuteStoreProcedure("comn_module", new List<SqlParameter>() 
                                            { 
                                                    new SqlParameter("@opr", 'X'),
                                                    new SqlParameter("@UserName", username),
                                                    new SqlParameter("@comn_mod_code", sequence.module_code),
                                                    new SqlParameter("@appltype", comnobj.comn_type_code),
                                          });
                                            if (dr_appl.HasRows)
                                            {
                                                while (dr_appl.Read())
                                                {
                                                    List<string> lst_loc = new List<string>();
                                                    Comn_applications applobj = new Comn_applications();
                                                    applobj.comn_appl_code = dr_appl["comn_appl_code"].ToString();
                                                    applobj.comn_appl_name_en = dr_appl["comn_appl_name_en"].ToString();
                                                    applobj.comn_appl_name_ar = dr_appl["comn_appl_name_ar"].ToString();

                                                    applobj.comn_appl_tag = dr_appl["comn_appl_tag"].ToString();
                                                    applobj.comn_appl_location = dr_appl["comn_appl_location"].ToString();
                                                    applobj.comn_appl_type = dr_appl["comn_appl_type"].ToString();
                                                    try
                                                    {
                                                        applobj.comn_user_insert = dr_appl["comn_user_insert"].Equals("1") ? true : false;
                                                        applobj.comn_user_update = dr_appl["comn_user_update"].Equals("1") ? true : false;
                                                        applobj.comn_user_delete = dr_appl["comn_user_delete"].Equals("1") ? true : false;
                                                    }
                                                    catch (Exception ex) { }

                                                    //string location = dr_appl["comn_appl_location"].ToString();
                                                    //if (!(string.IsNullOrEmpty(location)))
                                                    //{
                                                    //    lst_loc = location.Split('|').ToList();
                                                    //    applobj.comn_appl_location = lst_loc[0].ToString();
                                                    //    applobj.comn_url = lst_loc[1].ToString();
                                                    //    applobj.comn_controller = lst_loc[2].ToString();
                                                    //}
                                                    comnappl_list.Add(applobj);
                                                }
                                            }
                                            db_appl.Dispose();
                                            comnobj.comn_appl = comnappl_list;
                                        }

                                        comnparams_list.Add(comnobj);
                                    }
                                }
                                db_type.Dispose();
                                sequence.comn_type = comnparams_list;
                            }

                            module_list.Add(sequence);
                        }
                    }

                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, module_list);
        }



        [Route("getAllFaviourate")]
        public HttpResponseMessage getAllFaviourate(string user, string appl_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllAcademicYear"));

            string ss = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_employee_dashboard_erp]",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "D"),
                                 new SqlParameter("@comn_user_name", user),
                                 new SqlParameter("@comn_widget_appl_code", appl_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            ss = dr["comn_user_appl_fav"].ToString();

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
        }

        [Route("getAllAcademicYear")]
        public HttpResponseMessage getAllAcademicYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllAcademicYear"));

            List<Sims084> activeyear = new List<Sims084>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_stop_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "M")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims084 simsobj = new Sims084();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            activeyear.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, activeyear);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, activeyear);
            }
        }

        public HttpResponseMessage getAllAcademicYearDash(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYearDash(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                            new SqlParameter("@sims_cur_code",cur_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getAllAcademicYear_dash")]
        public HttpResponseMessage getAllAcademicYear_dash()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getAllCountryName")]
        public HttpResponseMessage getAllCountryName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCountryName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllCountryName"));

            List<Sim056> countryname = new List<Sim056>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_city_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim056 simsobj = new Sim056();
                            simsobj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            simsobj.sims_country_code = dr["sims_country_code"].ToString();

                            countryname.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, countryname);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, countryname);
            }
        }

        [Route("getStateName")]
        public HttpResponseMessage getStateName(string countrycode)
        {
            List<Sim056> lstModules = new List<Sim056>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_city_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_countrycode",countrycode),

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim056 sequence = new Sim056();
                            sequence.sims_state_code = dr["sims_state_code"].ToString();
                            sequence.sims_state_name_en = dr["sims_state_name_en"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAllRegionName")]
        public HttpResponseMessage getAllRegionName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRegionName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllRegionName"));

            List<Sim002> regionname = new List<Sim002>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_country_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "R"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim002 simsobj = new Sim002();
                            simsobj.sims_region_name_en = dr["sims_region_name_en"].ToString();
                            simsobj.sims_region_code = dr["sims_region_code"].ToString();

                            regionname.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, regionname);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, regionname);
            }
        }


        [Route("getAllCurrencyCode")]
        public HttpResponseMessage getAllCurrencyCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCurrencyCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getAllCurrencyCode"));

            List<Sim002> currencyname = new List<Sim002>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_country_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "C"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim002 simsobj = new Sim002();
                            simsobj.sims_currency_dec = dr["excg_curcy_desc"].ToString();
                            simsobj.sims_currency_code = dr["excg_curcy_code"].ToString();

                            currencyname.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, currencyname);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, currencyname);
            }
        }

        [Route("getAllGradeCode")]
        public HttpResponseMessage getAllGradeCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradeCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "GradeCode", "GradeCode"));

            List<Sim038> mod_list = new List<Sim038>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "N"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();
                            simsobj.screening_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.screening_grade_code_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getAllScreeningGradeCode")]
        public HttpResponseMessage getAllScreeningGradeCode(string cur_code, string acad_yr)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradeCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "GradeCode", "GradeCode"));

            List<Sim038> mod_list = new List<Sim038>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "N"),
                            new SqlParameter("@sims_cur_code",cur_code),
                            new SqlParameter("@sims_academic_year",acad_yr),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim038 simsobj = new Sim038();
                            simsobj.screening_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.screening_grade_code_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        [Route("getCompanyName")]
        public HttpResponseMessage getCompanyName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCompanyName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getCompanyName"));

            List<Per058> company_list = new List<Per058>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.pays_pay_code_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per058 simsobj = new Per058();
                            simsobj.company_name = dr["co_desc"].ToString();
                            simsobj.pc_company_code = dr["co_company_code"].ToString();
                            company_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, company_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, company_list);
        }

        [Route("getDepartmentNameByCompany")]
        public HttpResponseMessage getDepartmentNameByCompany(string comp)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDepartmentNameByCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getDepartmentNameByCompany"));

            List<Pers137> company_list = new List<Pers137>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C"),
                            new SqlParameter("@noh_company_code",comp)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Pers137 simsobj = new Pers137();
                            simsobj.codp_dept_name = dr["codp_dept_name"].ToString();
                            simsobj.noh_dept_code = dr["codp_dept_no"].ToString();

                            company_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, company_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, company_list);
        }

        [Route("getGradeNameByCompany")]
        public HttpResponseMessage getGradeNameByCompany(string comp)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeNameByCompany(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getGradeNameByCompany"));

            List<Pers137> grade_list = new List<Pers137>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_ot_hour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                            new SqlParameter("@noh_company_code",comp)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Pers137 simsobj = new Pers137();
                            simsobj.gr_desc = dr["gr_desc"].ToString();
                            simsobj.noh_grade_code = dr["gr_code"].ToString();

                            grade_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("getDeductionTag")]
        public HttpResponseMessage getDeductionTag()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDeductionTag(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getDeductionTag"));

            List<Per058> deduction = new List<Per058>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_pay_code",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","E"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per058 simsobj = new Per058();
                            simsobj.dedn_tag = dr["pays_appl_form_field_value1"].ToString();
                            simsobj.pc_earn_dedn_tag = dr["pays_appl_parameter"].ToString();
                            deduction.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, deduction);

            }
            return Request.CreateResponse(HttpStatusCode.OK, deduction);
        }

        [Route("getCitiExpTag")]
        public HttpResponseMessage getCitiExpTag()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCitiExpTag(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "FLEET", "getCitiExpTag"));

            List<Per058> CitiExp = new List<Per058>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_pay_code",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per058 simsobj = new Per058();
                            simsobj.pc_citi_exp_tag = dr["pays_appl_form_field_value1"].ToString();

                            CitiExp.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, CitiExp);

            }
            return Request.CreateResponse(HttpStatusCode.OK, CitiExp);
        }

        [Route("getGradeName")]
        public HttpResponseMessage getGradeName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Setup", "getGradeName"));

            List<Per066> grade_code = new List<Per066>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_pay_code_detail",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","G"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per066 simsobj = new Per066();
                            simsobj.pd_grade_code_nm = dr["gr_desc"].ToString();
                            simsobj.pd_grade_code = dr["gr_code"].ToString();

                            grade_code.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grade_code);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_code);
        }

        [Route("getPayName")]
        public HttpResponseMessage getPayName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Setup", "getPayName"));

            List<Per066> grade_code = new List<Per066>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_pay_code_detail",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","P"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Per066 simsobj = new Per066();
                            simsobj.pd_pay_code_nm = dr["cp_desc"].ToString();
                            simsobj.pd_pay_code = dr["cp_code"].ToString();

                            grade_code.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grade_code);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_code);
        }

        [Route("getFormulaCode")]
        public HttpResponseMessage getFormulaCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFormulaCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Setup", "getFormulaCode"));

            List<Per066> formula_code = new List<Per066>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays_pay_code_detail",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","F"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Per066 simsobj = new Per066();
                            simsobj.pd_formula_nm = dr["fm_desc"].ToString();
                            simsobj.pd_formula_code = dr["fm_code"].ToString();

                            formula_code.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, formula_code);

            }
            return Request.CreateResponse(HttpStatusCode.OK, formula_code);
        }

        [Route("getCreditDebit")]
        public HttpResponseMessage getCreditDebit()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCreditDebit(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Setup", "getCreditDebit"));

            List<Per066> cred_list = new List<Per066>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_pay_code_detail_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Per066 simsobj = new Per066();
                            simsobj.pd_credit_acno_nm = dr["acc_name"].ToString();
                            simsobj.pd_credit_acno = dr["acc_code"].ToString();

                            //simsobj.pd_debit_acno_nm = dr["glma_comp_code"].ToString() + dr["glma_dept_no"].ToString() + dr["glma_acct_code"].ToString() + "-" + dr["glma_acct_name"].ToString();
                            //simsobj.pd_debit_acno = dr["glma_acct_code"].ToString();
                            cred_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, cred_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, cred_list);
        }

        [Route("getAllLedger")]
        public HttpResponseMessage getAllLedger()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLedger(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Hrms", "getAllLedger"));

            List<Per131> Ldgr_list = new List<Per131>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_code_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "L"),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per131 simsobj = new Per131();
                            simsobj.ledgerName = dr["sllc_ldgr_name"].ToString();
                            simsobj.lc_ledger_code = dr["sllc_ldgr_code"].ToString();
                            Ldgr_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, Ldgr_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ldgr_list);
            }
        }

        [Route("getAllLoanDesc")]
        public HttpResponseMessage getAllLoanDesc(string Company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllLoanDesc(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Hrms", "getAllLoanDesc"));

            List<Per134> loan_desc = new List<Per134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_register_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "F"),
                                new SqlParameter("@Company_code", Company_code),
                              

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per134 simsobj = new Per134();
                            simsobj.LoanName = dr["lc_desc"].ToString();
                            simsobj.lo_code = dr["lc_code"].ToString();
                            loan_desc.Add(simsobj);
                        }

                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, loan_desc);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, loan_desc);
            }
        }

        [Route("getAllEmpName")]
        public HttpResponseMessage getAllEmpName(string Company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllEmpName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Hrms", "getAllEmpName"));
            //   List<Sim508> list = new List<Sim508>();
            List<Per134> rglist = new List<Per134>();
            List<Per134> Emp_desc = new List<Per134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_register_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@Company_code", Company_code),
                                //new SqlParameter("@Loan_number", Loan_number),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per134 simsobj = new Per134();
                            simsobj.EmployeeName = dr["Name"].ToString();
                            simsobj.lo_number = dr["em_login_code"].ToString();
                            //simsobj.DESIGNATION = dr["DESIGNATION"].ToString();
                            //simsobj.lo_gaunt_desg = dr["em_desg_code"].ToString();
                            //simsobj.lo_gaunt_address1 = dr["em_summary_address"].ToString();
                            //simsobj.em_first_name = dr["em_first_name"].ToString();
                            //simsobj.CompanyName = dr["CompanyName"].ToString();

                            Emp_desc.Add(simsobj);
                        }

                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_desc);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_desc);
            }
        }

        [Route("getGradeDesc")]
        public HttpResponseMessage getGradeDesc(string Company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeDesc(),PARAMETERS :: NO)";


            List<Per130> grade_desc = new List<Per130>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_loan_amount_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),
                             new SqlParameter("@COMPANY_CODE", Company_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per130 simsobj = new Per130();
                            simsobj.la_grade_code = dr["gr_code"].ToString();
                            simsobj.GradeName = dr["gr_desc"].ToString();
                            grade_desc.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_desc);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_desc);
            }
        }




        [Route("Get_Application")]
        public HttpResponseMessage Get_Application(string s)
        {
            List<search_app> mod_list = new List<search_app>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user",
                        new List<SqlParameter>() 
                         { 
                           
                new SqlParameter("@opr", "B"),
                new SqlParameter("@comn_user_name", s)
              
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            search_app comseq = new search_app();
                            comseq.comn_mod_code = dr["comn_mod_code"].ToString();
                            comseq.location = dr["location"].ToString();
                            comseq.name_only = dr["comn_appl_name_en"].ToString();
                            comseq.comn_mod_name_en = dr["comn_mod_name_en"].ToString();

                            comseq.comn_mod_name_ar = dr["comn_mod_name_ar"].ToString();
                            comseq.name_only_ar = dr["comn_appl_name_ar"].ToString();
                            comseq.name_ar = dr["comn_mod_name_ar"].ToString() + "-" + dr["comn_appl_name_ar"].ToString();
                            comseq.name = dr["comn_mod_name_en"].ToString() + "-" + dr["comn_appl_name_en"].ToString();
                            if (!string.IsNullOrEmpty(dr["comn_appl_location"].ToString()))
                                comseq.url = dr["comn_appl_location"].ToString();
                            else
                                comseq.url = "-";
                            comseq.comn_appl_type = dr["comn_appl_type"].ToString();
                            mod_list.Add(comseq);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }




        [Route("getAllModulesArabic")]
        public HttpResponseMessage getAllModulesArabic(string username)
        {
            List<comn_modules> module_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_module",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'B'),
                             new SqlParameter("@flag", 2),
                             new SqlParameter("@UserName", username)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_modules sequence = new comn_modules();
                            sequence.module_code = dr["comn_appl_mod_code"].ToString();
                            sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                            sequence.module_name_ar = dr["comn_mod_name_ar"].ToString();
                            sequence.module_name_fr = dr["comn_mod_name_fr"].ToString();
                            sequence.module_name_ot = dr["comn_mod_name_ot"].ToString();
                            sequence.module_create_date = dr["comn_mod_create_date"].ToString();
                            sequence.module_short_desc = dr["comn_mod_short_description"].ToString();
                            sequence.module_desc = dr["comn_mod_description"].ToString();
                            sequence.module_keywords = dr["comn_mod_keywords"].ToString();
                            sequence.module_status = dr["comn_mod_status"].ToString().Equals("A") ? true : false;
                            sequence.module_version = dr["comn_mod_version"].ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(dr["comn_mod_color"].ToString()))
                                {
                                    if (dr["comn_mod_color"].ToString().Length == 9)
                                        sequence.module_color = "#" + dr["comn_mod_color"].ToString().Substring(2);
                                    else if (dr["comn_mod_color"].ToString().Length == 7)
                                        sequence.module_color = dr["comn_mod_color"].ToString();
                                    else
                                        sequence.module_color = "#993366";
                                }
                                else
                                    sequence.module_color = "#993366";
                            }
                            catch (Exception de)
                            {
                                sequence.module_color = "#993366";
                            }
                            sequence.module_location = dr["comn_mod_location"].ToString();
                            sequence.module_dislpay_order = int.Parse(dr["comn_mod_display_order"].ToString());
                            sequence.module_Image = dr["comn_mod_img"].ToString();

                            List<Comn_parameterstypes> comnparams_list = new System.Collections.Generic.List<Comn_parameterstypes>();

                            //Call 'Group Types'
                            using (DBConnection db_type = new DBConnection())
                            {
                                db_type.Open();
                                SqlDataReader dr_type = db_type.ExecuteStoreProcedure("comn_module", new List<SqlParameter>() 
                                { 
                                        new SqlParameter("@opr", 'A'),
                                        new SqlParameter("@UserName", username),
                                        new SqlParameter("@comn_mod_code", sequence.module_code),
                                });
                                if (dr_type.HasRows)
                                {
                                    while (dr_type.Read())
                                    {
                                        Comn_parameterstypes comnobj = new Comn_parameterstypes();
                                        comnobj.comn_type = dr_type["sims_appl_parameter"].ToString() + "_" + sequence.module_code;
                                        comnobj.comn_type_code = dr_type["sims_appl_parameter"].ToString();
                                        comnobj.comn_type_name = dr_type["sims_appl_form_field_value1"].ToString();

                                        using (DBConnection db_appl = new DBConnection())
                                        {
                                            db_appl.Open();
                                            List<Comn_applications> comnappl_list = new List<Comn_applications>();
                                            SqlDataReader dr_appl = db_appl.ExecuteStoreProcedure("comn_module", new List<SqlParameter>() 
                                            { 
                                                    new SqlParameter("@opr", 'C'),
                                                    new SqlParameter("@UserName", username),
                                                    new SqlParameter("@comn_mod_code", sequence.module_code),
                                                    new SqlParameter("@appltype", comnobj.comn_type_code),
                                          });
                                            if (dr_appl.HasRows)
                                            {
                                                while (dr_appl.Read())
                                                {
                                                    List<string> lst_loc = new List<string>();
                                                    Comn_applications applobj = new Comn_applications();
                                                    applobj.comn_appl_code = dr_appl["comn_appl_code"].ToString();
                                                    applobj.comn_appl_name_en = dr_appl["comn_appl_name_en"].ToString();
                                                    applobj.comn_appl_tag = dr_appl["comn_appl_tag"].ToString();
                                                    applobj.comn_appl_location = dr_appl["comn_appl_location"].ToString();
                                                    applobj.comn_appl_type = dr_appl["comn_appl_type"].ToString();

                                                    //string location = dr_appl["comn_appl_location"].ToString();
                                                    //if (!(string.IsNullOrEmpty(location)))
                                                    //{
                                                    //    lst_loc = location.Split('|').ToList();
                                                    //    applobj.comn_appl_location = lst_loc[0].ToString();
                                                    //    applobj.comn_url = lst_loc[1].ToString();
                                                    //    applobj.comn_controller = lst_loc[2].ToString();
                                                    //}
                                                    comnappl_list.Add(applobj);
                                                }
                                            }
                                            db_appl.Dispose();
                                            comnobj.comn_appl = comnappl_list;
                                        }

                                        comnparams_list.Add(comnobj);
                                    }
                                }
                                db_type.Dispose();
                                sequence.comn_type = comnparams_list;
                            }

                            module_list.Add(sequence);
                        }
                    }

                }

            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, module_list);
        }



        [Route("getAcaYearbyCompCode")]
        public HttpResponseMessage getAcaYearbyCompCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcaYearbyCompCode(),PARAMETERS :: NO)";
            Log.Debug(string.Format(debug, "FLEET", "getAcaYearbyCompCode"));

            List<fins118> fins_academic = new List<fins118>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_get_data_sp",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "B"),
                             new SqlParameter("@opr_mem", "1")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins118 simsobj = new fins118();
                            simsobj.glsc_year = dr["fins_appl_form_field_value3"].ToString();
                            simsobj.glsc_year_code = dr["fins_appl_form_field_value1"].ToString();
                            fins_academic.Add(simsobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fins_academic);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_academic);
            }
        }

        [Route("getFinsCompCode")]
        public HttpResponseMessage getFinsCompCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFinsCompCode(),PARAMETERS :: NO)";
            Log.Debug(string.Format(debug, "FLEET", "getFinsCompCode"));

            List<fins118> fins_comp = new List<fins118>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.fins_get_data_sp",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "A"),
                            // new SqlParameter("@opr_mem", "1")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins118 simsobj = new fins118();
                            simsobj.company_full_name = dr["comp_name"].ToString();
                            simsobj.company_name = dr["comp_short_name"].ToString();
                            simsobj.glsc_comp_code = dr["comp_code"].ToString();
                            fins_comp.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fins_comp);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_comp);
            }
        }


        [Route("getFinsmatch")]
        public HttpResponseMessage getFinsmatch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getFinsmatch(),PARAMETERS :: NO)";
            //  Log.Debug(string.Format(debug, "FLEET", "getFinsmatchCode"));

            List<fins069> fins_match = new List<fins069>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[fins_ledger_control_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "M")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fins069 simsobj = new fins069();
                            simsobj.sllc_match_type_desc = dr["fins_appl_form_field_value1"].ToString();
                            simsobj.sllc_match_type = dr["fins_appl_parameter"].ToString();
                            fins_match.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fins_match);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fins_match);
            }
        }



        [Route("getEmployeeProfileView")]
        public HttpResponseMessage getEmployeeProfileView(string em_login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeeProfileView(),PARAMETERS :: NO)";


            List<Per099> grade_desc = new List<Per099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_profile_view]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@em_login_code", em_login_code),
                             //new SqlParameter("@em_number", em_number)


                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per099 simsobj = new Per099();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.em_company_name = dr["Company_name"].ToString();
                            simsobj.em_company_code = dr["em_company_code"].ToString();
                            simsobj.em_Deptt_Code = dr["em_dept_code"].ToString();
                            simsobj.em_dept_name = dr["dept_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_dept_effect_from"].ToString()))
                                simsobj.em_dept_effect_from = dr["em_dept_effect_from"].ToString();
                            simsobj.em_desg_code = dr["em_desg_code"].ToString();
                            simsobj.em_desg_name = dr["em_desg_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_date_of_join"].ToString()))
                                simsobj.em_date_of_join = dr["em_date_of_join"].ToString();
                            simsobj.em_service_status = dr["ServiceStatus"].ToString();
                            simsobj.em_staff_type = dr["em_staff_type"].ToString();
                            simsobj.em_Staff_Type_Code = dr["em_staff_type"].ToString();
                            //  simsobj.em_Dest_Code = dr["em_dest_code"].ToString();
                            simsobj.em_nation_name = dr["nationality_name"].ToString();
                            simsobj.em_Grade_Code = dr["em_grade_code"].ToString();
                            simsobj.em_grade_name = dr["grade_name"].ToString();
                            simsobj.em_punching_id = dr["em_punching_id"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_grade_effect_from"].ToString()))
                                simsobj.em_grade_effect_from = dr["em_grade_effect_from"].ToString();
                            simsobj.em_Salutation_Code = dr["em_salutation"].ToString();
                            simsobj.em_salutation = dr["salutation"].ToString();
                            simsobj.em_first_name = dr["em_first_name"].ToString();
                            simsobj.em_middle_name = dr["em_middle_name"].ToString();
                            simsobj.em_last_name = dr["em_last_name"].ToString();
                            simsobj.em_full_name = dr["em_full_name"].ToString();
                            simsobj.em_family_name = dr["em_family_name"].ToString();
                            simsobj.em_name_ot = dr["em_name_ot"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_date_of_birth"].ToString()))
                                simsobj.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            simsobj.em_Sex_Code = dr["em_sex"].ToString();
                            simsobj.em_sex = dr["Gender"].ToString();
                            simsobj.em_marital_status = dr["maritialStatus"].ToString();
                            simsobj.em_Marital_Status_Code = dr["em_marital_status"].ToString();
                            if (dr["em_habdicap_status"].ToString() == "A")
                                simsobj.em_handicap_status = true;
                            else
                                simsobj.em_handicap_status = false;
                            simsobj.em_Religion_code = dr["em_religion_code"].ToString();
                            simsobj.em_religion_name = dr["religion_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_last_login"].ToString()))
                                simsobj.em_last_login = dr["em_last_login"].ToString();
                            //   simsobj.em_ethnicity_code = dr["em_ethnicity_code"].ToString();
                            //   simsobj.em_ethnicity_name = dr["ethnicity_name"].ToString();
                            //   simsobj.em_apartment_number = dr["em_appartment_number"].ToString();
                            //   simsobj.em_building_number  = dr["em_building_number"].ToString();
                            //   simsobj.em_street_number   = dr["em_street_number"].ToString();
                            //   simsobj.em_area_number     = dr["em_area_number"].ToString();
                            simsobj.em_summary_address = dr["em_summary_address"].ToString();
                            //    simsobj.em_summary_address_local_language = dr["em_summary_address_local_language"].ToString();
                            simsobj.em_city_code = dr["em_city"].ToString();
                            simsobj.em_city = dr["city_name"].ToString();
                            simsobj.em_state_code = dr["em_state"].ToString();
                            simsobj.em_state = dr["state_name"].ToString();
                            simsobj.em_Country_Code = dr["em_country_code"].ToString();
                            simsobj.em_country = dr["sims_country_name_en"].ToString();
                            //    simsobj.em_Nation_Code   = dr["em_nation_code"].ToString();
                            simsobj.em_service_status_code = dr["em_service_status"].ToString();
                            simsobj.em_phone = dr["em_phone"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_fax = dr["em_fax"].ToString();
                            simsobj.em_po_box = dr["em_po_box"].ToString();
                            simsobj.em_img = dr["em_img"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_dependant_full"].ToString()))
                            //    simsobj.em_dependant_full = Convert.ToInt16(dr["em_dependant_full"].ToString());
                            //if (!string.IsNullOrEmpty(dr["em_dependant_half"].ToString()))
                            //    simsobj.em_dependant_half = Convert.ToInt16(dr["em_dependant_half"].ToString());
                            //if (!string.IsNullOrEmpty(dr["em_dependant_infant"].ToString()))
                            //    simsobj.em_dependant_infant = Convert.ToInt16(dr["em_dependant_infant"].ToString());
                            simsobj.em_emergency_contact_name1 = dr["em_emergency_contact_name1"].ToString();
                            simsobj.em_emergency_contact_name2 = dr["em_emergency_contact_name2"].ToString();
                            simsobj.em_emergency_contact_number1 = dr["em_emergency_contact_number1"].ToString();
                            simsobj.em_emergency_contact_number2 = dr["em_emergency_contact_number2"].ToString();

                            if (dr["em_bank_cash_tag"].ToString() == "C")
                                simsobj.em_bank_cash_tag = dr["em_bank_cash_tag"].ToString();
                            else
                                simsobj.em_bank_cash_tag = dr["em_bank_cash_tag"].ToString();
                            simsobj.em_ledger_ac_no = dr["em_ledger_ac_no"].ToString();
                            simsobj.em_gpf_ac_no = dr["em_gpf_ac_no"].ToString();
                            simsobj.em_gosi_ac_no = dr["em_gosi_ac_ac_no"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_gosi_start_date"].ToString()))
                                simsobj.em_gosi_start_date = dr["em_gosi_start_date"].ToString();
                            //simsobj.em_pan_no = dr["em_pan_no"].ToString();
                            simsobj.em_iban_no = dr["em_iban_no"].ToString();
                            simsobj.em_labour_card_no = dr["en_labour_card_no"].ToString();
                            //simsobj.em_route_code = dr["em_route_code"].ToString();
                            simsobj.em_status = dr["em_status"].ToString();
                            simsobj.em_status_code = dr["em_status_code"].ToString();
                            //if (dr["em_stop_salary_indicator"].ToString() == "Y")
                            //    simsobj.em_stop_salary_indicator = true;
                            //else
                            //    simsobj.em_stop_salary_indicator = false;

                            //if (!string.IsNullOrEmpty(dr["em_leave_resume_date"].ToString()))
                            //    simsobj.em_leave_resume_date = DateTime.Parse(dr["em_leave_resume_date"].ToString());
                            //if (dr["em_leave_tag"].ToString() == "Y")
                            //    simsobj.em_leave_tag = true;
                            //else
                            //    simsobj.em_leave_tag = false;
                            //if (dr["em_citi_exp_tag"].ToString() == "E")
                            //    simsobj.em_citi_exp_tag = true;
                            //else
                            //    simsobj.em_citi_exp_tag = false;
                            simsobj.em_joining_ref = dr["em_joining_ref"].ToString();
                            simsobj.em_bank_code = dr["em_bank_code"].ToString();
                            simsobj.em_bank_name = dr["bank_name"].ToString();
                            simsobj.em_bank_ac_no = dr["em_bank_ac_no"].ToString();
                            simsobj.em_bank_swift_code = dr["em_bank_swift_code"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_left_date"].ToString()))
                            //    simsobj.em_left_date = DateTime.Parse(dr["em_left_date"].ToString());
                            //simsobj.em_left_reason = dr["em_left_reason"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_leave_start_date"].ToString()))
                            //    simsobj.em_leave_start_date = DateTime.Parse(dr["em_leave_start_date"].ToString());
                            //if (!string.IsNullOrEmpty(dr["em_leave_end_date"].ToString()))
                            //    simsobj.em_leave_end_date = DateTime.Parse(dr["em_leave_end_date"].ToString());

                            //if (!string.IsNullOrEmpty(dr["em_cl_resume_date"].ToString()))
                            //    simsobj.em_cl_resume_date = DateTime.Parse(dr["em_cl_resume_date"].ToString());
                            //simsobj.em_leave_resume_ref = dr["em_leave_resume_ref"].ToString();
                            //if (!string.IsNullOrEmpty(dr["em_over_stay_days"].ToString()))
                            //    simsobj.em_over_stay_days = Convert.ToInt16(dr["em_over_stay_days"].ToString());
                            //if (!string.IsNullOrEmpty(dr["em_under_stay_days"].ToString()))
                            //    simsobj.em_under_stay_days = Convert.ToInt16(dr["em_under_stay_days"].ToString());
                            //if (!string.IsNullOrEmpty(dr["em_stop_salary_from"].ToString()))
                            //    simsobj.em_stop_salary_from = dr["em_stop_salary_from"].ToString();
                            //simsobj.em_unpaid_leave = dr["em_unpaid_leave"].ToString();
                            //if (dr["em_punching_status"].ToString() == "A")
                            //    simsobj.em_punching_status = true;
                            //else
                            //    simsobj.em_punching_status = false;

                            simsobj.em_passport_no = dr["em_passport_number"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_issue_date"].ToString()))
                                simsobj.em_passport_issue_date = dr["em_passport_issue_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_remember_expiry_date"].ToString()))
                                simsobj.em_pssport_exp_rem_date = dr["em_passport_remember_expiry_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_expiry_date"].ToString()))
                                simsobj.em_passport_exp_date = dr["em_passport_expiry_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_passport_return_date"].ToString()))
                                simsobj.em_passport_ret_date = dr["em_passport_return_date"].ToString();

                            simsobj.em_passport_issuing_authority = dr["em_passport_issuing_authority"].ToString();
                            simsobj.em_passport_issue_place = dr["em_passport_issue_place"].ToString();
                            simsobj.em_visa_no = dr["em_visa_number"].ToString();
                            simsobj.em_visa_type = dr["em_visa_type"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_issue_date"].ToString()))
                                simsobj.em_visa_issue_date = dr["em_visa_issue_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_visa_expiry_date"].ToString()))
                                simsobj.em_visa_exp_date = dr["em_visa_expiry_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["em_visa_remember_expiry_date"].ToString()))
                                simsobj.em_visa_exp_rem_date = dr["em_visa_remember_expiry_date"].ToString();

                            simsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            simsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            simsobj.em_national_id = dr["em_national_id"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_issue_date"].ToString()))
                                simsobj.em_national_id_issue_date = dr["em_national_id_issue_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_national_id_expiry_date"].ToString()))
                                simsobj.em_national_id_expiry_date = dr["em_national_id_expiry_date"].ToString();

                            //if (!string.IsNullOrEmpty(dr["em_modified_on"].ToString()))
                            //    simsobj.em_modified_on = DateTime.Parse(dr["em_modified_on"].ToString());
                            //simsobj.em_secret_question_code = dr["em_secret_question_code"].ToString();
                            //simsobj.em_secret_answer = dr["em_secret_answer"].ToString();
                            if (dr["em_agreement"].ToString() == "A")
                                simsobj.em_agreement = true;
                            else
                                simsobj.em_agreement = false;

                            if (!string.IsNullOrEmpty(dr["em_agreement_start_date"].ToString()))
                                simsobj.em_agreement_start_date = dr["em_agreement_start_date"].ToString();

                            if (!string.IsNullOrEmpty(dr["em_agreement_exp_date"].ToString()))
                                simsobj.em_agreement_exp_date = dr["em_agreement_exp_date"].ToString();

                            //if (!string.IsNullOrEmpty(dr["em_agreement_exp_rem_date"].ToString()))
                            //    simsobj.em_agreemet_exp_rem_date = DateTime.Parse(dr["em_agreement_exp_rem_date"].ToString());

                            //simsobj.em_blood_group_code = dr["em_blood_group_code"].ToString();
                            //simsobj.em_blood_group_name = dr["em_blood_group_name"].ToString();
                            //simsobj.em_adec_approval_number = dr["em_adec_approval_number"].ToString();
                            //simsobj.em_adec_approval_Date = dr["em_adec_approval_Date"].ToString();
                            //simsobj.em_adec_approved_designation = dr["em_adec_approved_designation"].ToString();
                            //simsobj.em_adec_approved_qualification = dr["em_adec_approved_qualification"].ToString();
                            //simsobj.em_adec_approved_subject = dr["em_adec_approved_subject"].ToString();
                            //simsobj.em_adec_approved_level = dr["em_adec_approved_level"].ToString();
                            //simsobj.em_health_card_number = dr["em_health_card_number"].ToString();
                            //simsobj.em_health_card_issue_date = dr["em_health_card_issue_date"].ToString();
                            //simsobj.em_health_card_expiry_date = dr["em_health_card_expiry_date"].ToString();
                            //simsobj.em_rta_number = dr["em_rta_number"].ToString();
                            //simsobj.em_rta_issue_date = dr["em_rta_issue_date"].ToString();
                            //simsobj.em_rta_expiry_date = dr["em_rta_expiry_date"].ToString();
                            grade_desc.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_desc);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_desc);
            }
        }


        [Route("getEmployeeAttendanceDetails")]
        public HttpResponseMessage getEmployeeAttendanceDetails(string em_login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeeAttendanceDetails(),PARAMETERS :: NO)";


            List<Per099> attendance_details = new List<Per099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_profile_view]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Y"),
                             new SqlParameter("@em_login_code", em_login_code)
                             //new SqlParameter("@em_number", em_number)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Per099 simsobj = new Per099();
                            simsobj.em_attendance_month_name = dr["month_name"].ToString();
                            simsobj.em_attendance_working_days = dr["Working_days"].ToString();
                            simsobj.em_attendance_present_days = dr["present_days"].ToString();
                            simsobj.em_attendance_absent_days = dr["absent_days"].ToString();
                            simsobj.em_attendance_leave_days = dr["leave_days"].ToString();

                            attendance_details.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, attendance_details);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, attendance_details);
            }
        }


        [Route("getEmployeeLeavesDetails")]
        public HttpResponseMessage getEmployeeLeavesDetails(string el_number)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeeLeavesDetails(),PARAMETERS :: NO)";


            List<Per099> leave_details = new List<Per099>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_profile_view]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Z"),
                             new SqlParameter("@em_login_code", el_number)
                            
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Per099 simsobj = new Per099();
                            simsobj.leave_type = dr["leave_type"].ToString();
                            simsobj.remain_days = dr["remain_days"].ToString();
                            simsobj.max_leaves = dr["max_leaves"].ToString();
                            simsobj.taken_leaves = dr["taken_leaves"].ToString();
                            simsobj.extra_leaves = dr["extra_leaves"].ToString();

                            leave_details.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, leave_details);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, leave_details);
            }
        }


        [Route("getEmployeeClassDetails")]
        public HttpResponseMessage getEmployeeClassDetails(string em_login_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEmployeeClassDetails(),PARAMETERS :: NO)";

            List<Per099> leave_details = new List<Per099>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pays_employee_profile_view]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "V"),
                             new SqlParameter("@em_login_code", em_login_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Per099 simsobj = new Per099();
                            simsobj.grade_sec = dr["grade_sec"].ToString();
                            simsobj.subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.lecture_count = dr["sims_bell_lecture_count"].ToString();
                            simsobj.practical_count = dr["sims_bell_lecture_practical_count"].ToString();

                            leave_details.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, leave_details);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, leave_details);
            }
        }


        [Route("Get_ApplicationArabic")]
        public HttpResponseMessage Get_ApplicationArabic(string s)
        {
            List<search_app> mod_list = new List<search_app>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user",
                        new List<SqlParameter>() 
                         { 
                           
                new SqlParameter("@opr", "C"),
                new SqlParameter("@comn_user_name", s)
              
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            search_app comseq = new search_app();

                            comseq.comn_mod_code = dr["comn_mod_code"].ToString();
                            comseq.location = dr["location"].ToString();


                            comseq.name = dr["comn_mod_name_en"].ToString() + "-" + dr["comn_appl_name_en"].ToString();
                            comseq.url = dr["comn_appl_location"].ToString();
                            comseq.comn_appl_type = dr["comn_appl_type"].ToString();

                            mod_list.Add(comseq);


                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("InsertUserAudit")]
        public HttpResponseMessage InsertUserAudit(audit obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertUserAudit()";
            Log.Debug(string.Format(debug, "PP", "InsertUserAudit"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("comn_user_audit",
                        new List<SqlParameter>() 
                         { 
                 new SqlParameter("@opr", obj.opr),
                 new SqlParameter("@comn_audit_user_code",obj.comn_audit_user_code ),
                 new SqlParameter("@comn_appl_name_en", obj.comn_appl_name_en),
                 new SqlParameter("@comn_audit_ip", obj.comn_audit_ip),
                 new SqlParameter("@comn_audit_dns", obj.comn_audit_dns),
                 new SqlParameter("@comn_audit_start_time", obj.comn_audit_start_time),
                 new SqlParameter("@comn_audit_end_time", obj.comn_audit_end_time),
                 new SqlParameter("@comn_audit_remark", obj.comn_audit_remark),
                             
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        ////// module drag drop /////

        [Route("getModulesOrder")]
        public HttpResponseMessage getModulesOrder()
        {
            List<comn_modules> module_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_module_order_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),
                         });
                    while (dr.Read())
                    {
                        comn_modules sequence = new comn_modules();
                        sequence.comn_appl = new List<Comn_applications>();
                        sequence.module_code = dr["comn_mod_code"].ToString();
                        sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                        sequence.module_name_ar = dr["comn_mod_name_ar"].ToString();
                        sequence.module_name_fr = dr["comn_mod_name_fr"].ToString();
                        sequence.module_name_ot = dr["comn_mod_name_ot"].ToString();
                        sequence.module_create_date = dr["comn_mod_create_date"].ToString();
                        sequence.module_short_desc = dr["comn_mod_short_description"].ToString();
                        sequence.module_desc = dr["comn_mod_description"].ToString();
                        sequence.module_keywords = dr["comn_mod_keywords"].ToString();
                        sequence.module_status = dr["comn_mod_status"].ToString().Equals("A") ? true : false;
                        sequence.module_version = dr["comn_mod_version"].ToString();
                        try
                        {
                            if (!string.IsNullOrEmpty(dr["comn_mod_color"].ToString()))
                            {
                                if (dr["comn_mod_color"].ToString().Length == 9)
                                    sequence.module_color = "#" + dr["comn_mod_color"].ToString().Substring(2);
                                else if (dr["comn_mod_color"].ToString().Length == 7)
                                    sequence.module_color = dr["comn_mod_color"].ToString();
                                else
                                    sequence.module_color = "#993366";
                            }
                            else
                                sequence.module_color = "#993366";
                        }
                        catch (Exception de)
                        {
                            sequence.module_color = "#993366";
                        }
                        sequence.module_location = dr["comn_mod_location"].ToString();
                        sequence.module_dislpay_order = int.Parse(dr["comn_mod_display_order"].ToString());
                        sequence.module_Image = dr["comn_mod_img"].ToString();
                        sequence.label = "Show";

                        module_list.Add(sequence);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        Comn_applications applobj = new Comn_applications();
                        applobj.comn_mod_code = dr["comn_appl_mod_code"].ToString();
                        applobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                        applobj.comn_appl_tag = dr["comn_appl_tag"].ToString();
                        applobj.comn_appl_code = dr["comn_appl_code"].ToString();
                        applobj.comn_appl_type = dr["comn_appl_type"].ToString();
                        applobj.comn_appl_status = dr["comn_appl_status"].ToString().Equals("A") ? true : false;
                        var m = from p in module_list where p.module_code == applobj.comn_mod_code select p;
                        if (m.Count() > 0)
                        {
                            m.ElementAt(0).comn_appl.Add(applobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, module_list);
        }

        [Route("updateModule")]
        public HttpResponseMessage updateModule(string modCode, string modDropCode)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_module_order_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'U'),
                            new SqlParameter("@comn_mod_code", modCode),
                            new SqlParameter("@comn_mod_display_order", modDropCode),
                          });

                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();


                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("updateApplication")]
        public HttpResponseMessage updateApplication(string modCode, string appDropCode, string appCode)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_module_order_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'J'),
                            new SqlParameter("@comn_mod_code", modCode),
                            new SqlParameter("@comn_mod_display_order", appDropCode),
                            new SqlParameter("@comn_appl_code",appCode),
                          });

                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();


                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("updateApplType")]
        public HttpResponseMessage updateApplType(string applCode, string applType)
        {

            //List<Pers260> grp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Pers260>>(lst[0].ToString());
            bool Inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_module_order_proc]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@comn_appl_code", applCode),
                            new SqlParameter("@comn_appl_type", applType),
                          });

                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();


                }
            }
            catch (Exception e)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }


        [Route("getAppType")]
        public HttpResponseMessage getAppType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAppType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAppType"));

            List<comn_modules> app_list = new List<comn_modules>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_module_order_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "T"),
                          });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_modules simsobj = new comn_modules();
                            simsobj.sims_appl_code = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_name = dr["sims_appl_form_field_value1"].ToString();
                            app_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, app_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, app_list);
            }
        }

        [Route("Get_Grade_CodebyCuriculum")]
        public HttpResponseMessage Get_Grade_CodebyCuriculum(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Grade_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Grade_Section_Code"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Search",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_mem_code", "G"),
                            new SqlParameter("@cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            //simsobj.section_code = dr["sims_grade_code"].ToString() + dr["sims_section_code"].ToString();
                            //simsobj.section_name = dr["sims_section_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("Get_Section_CodebyCuriculum")]
        public HttpResponseMessage Get_Section_CodebyCuriculum(string cur_code, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Section_CodebyCuriculum"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("Search",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_mem_code", "X"),
                            new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@IGNORED_SECTION_LIST", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            simsobj.grade_section_name = dr["section"].ToString();
                            simsobj.grade_section_code = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getAllDesignationName")]
        public HttpResponseMessage getAllDesignationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDesignationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Designation", "Designation"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.pays_employee_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 objNew = new Com052();
                            objNew.designation = dr["dg_code"].ToString();
                            objNew.designation_name = dr["dg_desc"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getdeptwiseDesignationName")]
        public HttpResponseMessage getdeptwiseDesignationName(string dept_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getdeptwiseDesignationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Designation", "Designation"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pays.pays_employee_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W1"),
                            new SqlParameter("@em_dept_name_er",dept_name),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 objNew = new Com052();
                            objNew.designation = dr["dg_code"].ToString();
                            objNew.designation_name = dr["dg_desc"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        //INVENTORY
        [Route("getDepartmentName")]
        public HttpResponseMessage getDepartmentName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDepartmentName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Department", "Department"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("hrms_get_data_sp",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "D"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 objNew = new Com052();
                            objNew.em_dept_code = dr["codp_dept_no"].ToString();
                            objNew.em_dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(objNew);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }

        [Route("getglmaAccountNames")]
        public HttpResponseMessage getglmaAccountNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getglmaAccountNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Setup", "getglmaAccountNames"));

            List<Inv027> account_list = new List<Inv027>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_purchase_expense_types_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Inv027 simsobj = new Inv027();

                            simsobj.glma_comp_code = dr["glma_comp_code"].ToString();
                            simsobj.glma_dept_no = dr["glma_dept_no"].ToString();
                            simsobj.glma_acct_code = dr["glma_acct_code"].ToString();
                            simsobj.glma_ldgr_code = dr["glma_ldgr_code"].ToString();
                            simsobj.pet_expense_acno = dr["pet_expense_acno"].ToString();
                            simsobj.pet_expense_acname = dr["pet_expense_acname"].ToString();

                            account_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, account_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, account_list);
        }

        [Route("getShipmentNo")]
        public HttpResponseMessage getShipmentNo()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getShipmentNo(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Setup", "getShipmentNo"));

            List<Inv133> shipment_list = new List<Inv133>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_clearance_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Inv133 simsobj = new Inv133();

                            simsobj.sr_shipment_no = dr["sr_shipment_no"].ToString();

                            shipment_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, shipment_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, shipment_list);
        }

        [Route("getCCNoFromCCheques")]
        public HttpResponseMessage getCCNoFromCCheques()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCCNoFromCCheques(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Setup", "getShipmentNo"));

            List<Inv134> CCNo_list = new List<Inv134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_expenses_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","C")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Inv134 simsobj = new Inv134();

                            simsobj.cc_no = dr["cc_no"].ToString();

                            CCNo_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, CCNo_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, CCNo_list);
        }

        [Route("getPetDesc")]
        public HttpResponseMessage getPetDesc()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPetDesc(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "Setup", "getShipmentNo"));

            List<Inv134> Pet_list = new List<Inv134>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[invs].[invs_custom_expenses_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","T")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Inv134 simsobj = new Inv134();

                            simsobj.pet_code = dr["pet_code"].ToString();
                            simsobj.pet_desc = dr["pet_desc"].ToString();

                            Pet_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Pet_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, Pet_list);
        }

        //gloabal search
        [Route("SearchEmployee")]
        public HttpResponseMessage SearchEmployee(string data)
        {

            empSearch obj = new empSearch();

            List<empSearch> Emp_List = new List<empSearch>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<empSearch>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Search]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'E'),
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            empSearch simsobj = new empSearch();
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.dept_name = dr["dept_name"].ToString();
                            simsobj.desg_name = dr["desg_name"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();
                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }

        [Route("getUserAllDetails")]
        public HttpResponseMessage getUserAllDetails(string uname)
        {
            Comn005 UserDet = new Comn005();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_user_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "X"),
                            new SqlParameter("@comn_user_name", uname),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            UserDet.User_Name = dr["comn_user_name"].ToString();
                            UserDet.comn_user_alias = dr["comn_user_alias"].ToString();
                            UserDet.User_Code = dr["comn_user_code"].ToString();
                            UserDet.Email_id = dr["Mail_Id"].ToString();
                            UserDet.Expiry_Date = dr["comn_user_expiry_date"].ToString();
                            UserDet.Group_Code = dr["comn_user_group_code"].ToString();
                            UserDet.Last_Login = dr["comn_user_last_login"].ToString();
                            UserDet.user_image = dr["user_image"].ToString();
                            UserDet.User_password = dr["comn_user_password"].ToString();
                            UserDet.UserAlertCount = dr["AlertCount"].ToString();
                            UserDet.UserCircularCount = dr["CircularCount"].ToString();
                            UserDet.messageCount = dr["MessageCount"].ToString();
                            if (dr["Desig"].ToString() != "?")

                                UserDet.Designation = dr["Desig"].ToString();
                            else
                            {
                                try
                                {
                                    UserDet.Designation = dr["comn_user_group_name"].ToString();
                                    UserDet.UserNewsCount = dr["NewsCount"].ToString();
                                }
                                catch (Exception ex) { }
                            }


                            UserDet.isAdmin = dr["ADMINuser"].ToString().Equals("1");
                            UserDet.comp = dr["comp"].ToString();
                            try
                            {
                                UserDet.lic_school_name = dr["lic_school_name"].ToString();
                                UserDet.lic_school_logo = dr["lic_school_logo"].ToString();

                            }
                            catch (Exception ex) { }

                            string[] str = UserDet.Designation.Split(' ');
                            if (str.Length > 1)
                                UserDet.DesigLtr = str[0][0] + "" + str[1][0];
                            else
                                UserDet.DesigLtr = str[0][0] + "";

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, UserDet);
        }


        //Bellow 3 API for Admission Cancellation.......
        [Route("CUDCancelAdmission")]
        public HttpResponseMessage CUDCancelAdmission(List<Sim999> data)
        {
            bool insert = false;
            Message message = new Message();
            string st = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim999 simsobj in data)
                    {

                        //int ins = db.ExecuteStoreProcedureforInsert("sims.sims_adm_cancellation_clr_config_proc",
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_adm_cancellation_clr_config_proc",
                         new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_clr_emp_id", simsobj.em_number),
                                new SqlParameter("@sims_fee_clr_status", simsobj.sims_fee_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_finn_clr_status", simsobj.sims_finn_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_inv_clr_status", simsobj.sims_inv_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_inci_clr_status", simsobj.sims_inci_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_lib_clr_status", simsobj.sims_lib_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_trans_clr_status", simsobj.sims_trans_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_acad_clr_status", simsobj.sims_acad_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_admin_clr_status", simsobj.sims_admin_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other1_clr_status", simsobj.sims_other1_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other2_clr_status", simsobj.sims_other2_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other3_clr_status", simsobj.sims_other3_clr_status==true?"T":"F"),
                                 new SqlParameter("@sims_clr_emp_status", simsobj.sims_clr_emp_status==true?"A":"I"),
                                  new SqlParameter("@sims_clr_super_user_flag", simsobj.sims_clr_super_user_flag==true?"T":"F"),
                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            message.strMessage = st;
                            //  insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                        dr.Close();


                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_adm_cancellation_clr_config_proc",
                           new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_sr_no", st),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_list", simsobj.sims_grade_list),
                                new SqlParameter("@sims_status", simsobj.sims_clr_emp_status==true?"A":"I"),

                           });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            message.strMessage = st;
                            insert = true;

                        }

                        else
                        {
                            insert = false;
                        }
                        dr1.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getAcademicYearClass")]
        public HttpResponseMessage getAcademicYearClass()
        {
            List<fee_recipt> com_list = new List<fee_recipt>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_class_wise_fee_receipt_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fee_recipt simsobj = new fee_recipt();
                            simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.academic_year_name = dr["sims_academic_year_description"].ToString();
                            simsobj.academic_year_status = dr["sims_academic_year_status"].ToString();
                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("CUDCancelAdmissionshow")]
        public HttpResponseMessage CUDCancelAdmissionshow(List<Sim999> data)
        {
            bool insert = false;
            Message message = new Message();
            string st = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim999 simsobj in data)
                    {
                        SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_adm_cancellation_clr_config_proc",
                           new List<SqlParameter>()
                        {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_sr_no", simsobj.sims_sr_no),
                                new SqlParameter("@sims_clr_emp_id", simsobj.empName),
                                new SqlParameter("@sims_fee_clr_status", simsobj.sims_fee_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_finn_clr_status", simsobj.sims_finn_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_inv_clr_status", simsobj.sims_inv_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_inci_clr_status", simsobj.sims_inci_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_lib_clr_status", simsobj.sims_lib_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_trans_clr_status", simsobj.sims_trans_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_acad_clr_status", simsobj.sims_acad_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_admin_clr_status", simsobj.sims_admin_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other1_clr_status", simsobj.sims_other1_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other2_clr_status", simsobj.sims_other2_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_other3_clr_status", simsobj.sims_other3_clr_status==true?"T":"F"),
                                new SqlParameter("@sims_clr_emp_status", simsobj.sims_clr_emp_status==true?"A":"I"),
                                 new SqlParameter("@sims_clr_super_user_flag", simsobj.sims_clr_super_user_flag==true?"T":"F"),


                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_grade_list", simsobj.sims_grade_list),
                                new SqlParameter("@sims_status", simsobj.sims_clr_emp_status==true?"A":"I"),

                           });

                        if (dr1.Read())
                        {
                            st = dr1[0].ToString();
                            message.strMessage = st;
                            insert = true;

                        }

                        else
                        {
                            insert = false;
                        }
                        dr1.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getAllADMData")]
        public HttpResponseMessage getAllFinancialYear(string Empnum, string sims_academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllBankMaster(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllBankMaster"));

            List<Sim999> goaltarget_list = new List<Sim999>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_adm_cancellation_clr_config_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_clr_emp_id",Empnum),
                            new SqlParameter("@sims_academic_year",sims_academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim999 simsobj = new Sim999();
                            simsobj.sims_clr_emp_id = dr["sims_clr_emp_id"].ToString();
                            simsobj.sims_sr_no = dr["sims_sr_no"].ToString();

                            simsobj.sims_fee_clr_status = dr["sims_fee_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_finn_clr_status = dr["sims_finn_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_inv_clr_status = dr["sims_inv_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_inci_clr_status = dr["sims_inci_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_lib_clr_status = dr["sims_lib_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_trans_clr_status = dr["sims_trans_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_acad_clr_status = dr["sims_acad_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_admin_clr_status = dr["sims_admin_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other1_clr_status = dr["sims_other1_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other2_clr_status = dr["sims_other2_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_other3_clr_status = dr["sims_other3_clr_status"].Equals("T") ? true : false;
                            simsobj.sims_clr_emp_status = dr["sims_clr_emp_status"].Equals("A") ? true : false;
                            simsobj.sims_clr_super_user_flag = dr["sims_clr_super_user_flag"].Equals("T") ? true : false;
                            simsobj.grades = dr["grades"].ToString();
                            simsobj.dstatus = dr["dstatus"].Equals("A") ? true : false;
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();

                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("GetSystemAlerts")]
        public HttpResponseMessage GetSystemAlerts()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetSystemAlerts(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("job_call_all_Alert_SPS",
                        new List<SqlParameter>()
                        {

                        });
                    if (dr.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getUserDetails_pass")]
        public HttpResponseMessage getUserDetails_pass(string uname)
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                DataSet dr = db.ExecuteStoreProcedureDS("GetData",
                    new List<SqlParameter>() { new SqlParameter("@tbl_name", "[comn].[comn_user]"),
                            new SqlParameter("@tbl_col_name1", "*"),
                            new SqlParameter("@tbl_cond","comn_user_name=" + "'" + uname + "'"),
                            });
                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, dr);
            }

        }


        //ExportExcel  method

        [Route("getReportCardLevel")]
        public HttpResponseMessage getReportCardLevel(string academic_year, string term)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "R"),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@sims_term_code", term)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_level_code = dr["sims_level_code"].ToString();
                            simsobj.sims_report_card_level_name = dr["sims_report_card_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getReportCard")]
        public HttpResponseMessage getReportCard(string sims_level_code, string academic_year, string term)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionFromGrade(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_level_code", sims_level_code),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@sims_term_code", term)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_report_card_code = dr["sims_report_card_code"].ToString();
                            simsobj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            simsobj.sims_report_card_description = dr["sims_report_card_description"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getGradebookCategory")]
        public HttpResponseMessage getGradebookCategory(string grade_code, string section_code, string academic_year, string term_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradebookCategory(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "G"),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code", section_code),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@sims_term_code", term_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            simsobj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getGradebookAssignment")]
        public HttpResponseMessage getGradebookAssignment(string grade_code, string section_code, string academic_year, string term_code, string cat_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradebookAssignment(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code", section_code),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@sims_term_code", term_code),
                                 new SqlParameter("@sims_gb_cat_code", cat_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            simsobj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getSubjects")]
        public HttpResponseMessage getSubjects(string grade_code, string section_code, string academic_year, string cur_code, string term_code, string assign_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjects(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "B"),
                                new SqlParameter("@grade_code", grade_code),
                                new SqlParameter("@section_code", section_code),
                                new SqlParameter("@academic_year", academic_year),
                                new SqlParameter("@cur_code", cur_code),
                                new SqlParameter("@sims_term_code", term_code),
                                new SqlParameter("@sims_gb_cat_assign_number", assign_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("GetRCTerm")]
        public HttpResponseMessage GetRCTerm(string curcode, string ayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetRCTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "T"),
                                new SqlParameter("@cur_code", curcode),
                                new SqlParameter("@academic_year", ayear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }

        [Route("getRecords")]
        public HttpResponseMessage getRecords(string ayear, string grade, string section, string cur_code, string report_level_code, string report_card_code, string gb_cat_assign_no, string sub_code, string term_code)
        {
            object[] arr = null;
            List<Sims028> rc = new List<Sims028>();
            List<subject_excel_sheet> lst = new List<subject_excel_sheet>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                   new List<SqlParameter>()
                         {
                                    new SqlParameter("@opr", "S"),
                                     new SqlParameter("@cur_code", cur_code),
                                    new SqlParameter("@academic_year", ayear),
                                    new SqlParameter("@grade_code",grade),
                                    new SqlParameter("@section_code",section),
                                    new SqlParameter("@sims_gb_cat_assign_number",gb_cat_assign_no),
                                    new SqlParameter("@sims_gb_subject_code",sub_code),
                                    new SqlParameter("@sims_term_code",term_code),
                                    new SqlParameter("@sims_level_code",report_level_code),
                                    new SqlParameter("@sims_report_card_code",report_card_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 obj = new Sims028();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.sims_level_code = dr["sims_level_code"].ToString();
                            obj.sims_report_card_name = dr["sims_report_card_name"].ToString();
                            obj.report_name = dr["report_name"].ToString();
                            obj.sims_report_card_description = dr["sims_report_card_description"].ToString();
                            obj.sims_gb_number = dr["sims_gb_number"].ToString();
                            obj.sims_gb_name = dr["sims_gb_name"].ToString();
                            obj.sims_gb_cat_assign_name = dr["sims_gb_cat_assign_name"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            obj.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            obj.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            obj.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();
                            obj.sims_gb_cat_code = dr["sims_gb_cat_code"].ToString();
                            obj.sims_gb_cat_name = dr["sims_gb_cat_name"].ToString();

                            obj.subjects = new List<subject_excel_sheet>();

                            string str = dr["sims_enroll_number"].ToString();
                            subject_excel_sheet obj1 = new subject_excel_sheet();

                            obj1.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            obj1.sims_subject_code = dr["sims_subject_code"].ToString();
                            obj1.sims_gb_cat_assign_mark = dr["sims_gb_cat_assign_mark"].ToString();
                            obj1.sims_gb_cat_assign_max_score = dr["sims_gb_cat_assign_max_score"].ToString();
                            obj1.sims_gb_cat_assign_number = dr["sims_gb_cat_assign_number"].ToString();

                            var v = (from p in rc where p.sims_enroll_number == str select p);

                            if (v.Count() == 0)
                            {
                                obj.subjects.Add(obj1);
                                rc.Add(obj);

                            }
                            else
                            {
                                v.ElementAt(0).subjects.Add(obj1);
                            }

                        }

                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, rc);
        }

        [Route("getGradesTermwise")]
        public HttpResponseMessage getGradesTermwise(string cur_code, string academic_year, string term_code, string level_code, string report_card_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradesTermwise(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "M"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                               new SqlParameter("@sims_term_code", term_code),
                               new SqlParameter("@sims_level_code", level_code),
                               new SqlParameter("@sims_report_card_code", report_card_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionGrdwise")]
        public HttpResponseMessage getSectionGrdwise(string cur_code, string academic_year, string term, string grade_code, string level_code, string report_card_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradesTermwise(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ALLSections"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_export_excel]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "N"),
                               new SqlParameter("@cur_code", cur_code),
                               new SqlParameter("@academic_year", academic_year),
                               new SqlParameter("@sims_term_code", term),
                               new SqlParameter("@grade_code", grade_code),
                               new SqlParameter("@sims_level_code", level_code),
                               new SqlParameter("@sims_report_card_code", report_card_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("CheckExcelRecords")]
        public HttpResponseMessage CheckExcelRecords(List<Sims028> data)
        {
            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims028 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_export_excel",
                           new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@cur_code",simsobj.sims_cur_code),
                                new SqlParameter("@grade_code", simsobj.sims_grade_name),
                                new SqlParameter("@section_code", simsobj.sims_section_name),
                                new SqlParameter("@sims_gb_name", simsobj.sims_gb_name), 
                                new SqlParameter("@sims_gb_cat_code", simsobj.sims_gb_cat_name),
                                new SqlParameter("@sims_gb_cat_assign_name", simsobj.sims_gb_cat_assign_name),                           
                                new SqlParameter("@sims_gb_cat_assign_enroll_number", simsobj.sims_enroll_number),
                                new SqlParameter("@sims_gb_cat_assign_mark",simsobj.sims_gb_cat_assign_mark)
//                                new SqlParameter("@max_score", simsobj.sims_gb_cat_assign_max_score),
                            });

                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims028 obj = new Sims028();
                                obj.ac_year = dr["ac_year"].ToString();
                                if (dr["ac__year_flag"].ToString() == "Y")
                                    obj.ac__year_flag = true;
                                else
                                    obj.ac__year_flag = false;
                                obj.cur_code = dr["cur_code"].ToString();
                                if (dr["cur_code_flag"].ToString() == "Y")
                                    obj.cur_code_flag = true;
                                else
                                    obj.cur_code_flag = false;
                                obj.grade_code = dr["grade_code"].ToString();
                                if (dr["grade_code_flag"].ToString() == "Y")
                                    obj.grade_code_flag = true;
                                else
                                    obj.grade_code_flag = false;
                                obj.section_code = dr["section_code"].ToString();
                                if (dr["section_code_flag"].ToString() == "Y")
                                    obj.section_code_flag = true;
                                else
                                    obj.section_code_flag = false;
                                //obj.gb_number = dr["gb_number"].ToString();
                                //if (dr["gb_number_flag"].ToString() == "Y")
                                //    obj.gb_number_flag = true;
                                //else
                                //    obj.gb_number_flag = false;

                                obj.enroll_number = dr["enroll_number"].ToString();
                                if (dr["enroll_number_flag"].ToString() == "Y")
                                    obj.enroll_number_flag = true;
                                else
                                    obj.enroll_number_flag = false;

                                obj.max_score = dr["max_score"].ToString();
                                if (dr["max_score_flag"].ToString() == "Y")
                                    obj.max_score_flag = true;
                                else
                                    obj.max_score_flag = false;

                                obj.assign_name = dr["assign_number"].ToString();
                                if (dr["assign_name_flag"].ToString() == "Y")
                                    obj.assign_name_flag = true;
                                else
                                    obj.assign_name_flag = false;

                                list.Add(obj);
                            }
                            dr.Close();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }

            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x);

            }
        }

        [Route("Check")]
        public HttpResponseMessage Check(List<Sims028> data)
        {
            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims028 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_export_excel",
                           new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@academic_year", simsobj.sims_academic_year)
                            });

                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                Sims028 obj = new Sims028();
                                obj.ac_year = dr["ac_year"].ToString();
                                list.Add(obj);
                            }
                            dr.Close();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x);
            }
        }

        [Route("UpdateRecords")]
        public HttpResponseMessage UpdateRecords(List<Sims028> data)
        {
            bool updated = false;
            List<Sims028> list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims028 simsobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_export_excel",
                           new List<SqlParameter>()
                            {
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_gb_name", simsobj.sims_gb_name),
                                new SqlParameter("@sims_gb_cat_name", simsobj.sims_gb_cat_name),
                                new SqlParameter("@sims_gb_cat_assign_name",simsobj.sims_gb_cat_assign_name),
                                new SqlParameter("@sims_gb_cat_assign_mark",simsobj.sims_gb_cat_assign_mark),
                                new SqlParameter("@sims_gb_cat_assign_enroll_number",simsobj.sims_enroll_number)

                            });

                        if (dr.RecordsAffected > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                        dr.Close();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x);
            }
        }


        [Route("getdepandacy_detail")]
        public HttpResponseMessage getdepandacy_detail(string appl_code)
        {
            List<comn_depandancy> com_list = new List<comn_depandancy>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "O"),
                             new SqlParameter("@comn_appl_code", appl_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            comn_depandancy simsobj = new comn_depandancy();
                            simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_code_name = dr["comn_appl_code_name"].ToString();
                            simsobj.comn_appl_field_name_en = dr["comn_appl_field_name_en"].ToString();

                            simsobj.comn_field_appl_code = dr["comn_field_appl_code"].ToString();
                            simsobj.comn_appl_field_name = dr["comn_appl_field_name"].ToString();
                            simsobj.comn_show_icon_status = dr["comn_show_icon_status"].ToString();
                            simsobj.comn_appl_field_desc = dr["comn_appl_field_desc"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            simsobj.comn_appl_mod_name = dr["comn_appl_mod_name"].ToString();
                            simsobj.comn_creation_user = dr["comn_creation_user"].ToString();

                            simsobj.comn_appl_creation_date = dr["comn_appl_creation_date"].ToString();
                            simsobj.comn_appl_field_status = dr["comn_appl_field_status"].ToString();
                            simsobj.user_name = dr["user_name"].ToString();
                            simsobj.comn_field_display_order = dr["comn_field_display_order"].ToString();


                            com_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        //exportexcel end

        [Route("getCompanyCurrency")]
        public HttpResponseMessage getCompanyCurrency(string comp_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetRCTerm(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SECTIONFROMGRADE"));

            if (comp_code == "undefined")
            {
                comp_code = null;
            }

            List<Sims0288> list = new List<Sims0288>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[fins_get_data_sp]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "B"),
                                new SqlParameter("@comp_code", comp_code),
                               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims0288 simsobj = new Sims0288();

                            simsobj.comp_code = dr["comp_code"].ToString();
                            simsobj.comp_curcy_code = dr["comp_curcy_code"].ToString();
                            simsobj.comp_curcy_dec = dr["comp_curcy_dec"].ToString();


                            list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, list);
            }
        }


        [Route("getLanguge")]
        public HttpResponseMessage getLanguge(string uname)
        {
            string cnt = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_user_lang_pref_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comn_user_name",uname),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cnt = dr["comn_user_lang_pref"].ToString();

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cnt);
        }


        [Route("updateLanguage")]
        public HttpResponseMessage updateLanguage(string lang, string uname)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_user_lang_pref_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@comn_user_lang", lang),
                            new SqlParameter("@comn_user_name", uname),
                           
                         });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getMenuPref")]
        public HttpResponseMessage getMenuPref(string uname)
        {
            string cnt = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_user_menu_pref_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comn_user_name",uname),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cnt = dr["comn_user_menu_pref"].ToString();

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cnt);
        }



        [Route("updateMenu")]
        public HttpResponseMessage updateMenu(string menu, string uname)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_user_menu_pref_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@comn_user_menu", menu),
                            new SqlParameter("@comn_user_name", uname),
                           
                         });
                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //  Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("getUrls")]
        public HttpResponseMessage getUrls()
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();


                DataSet dr = db.ExecuteStoreProcedureDS("GetData",
                    new List<SqlParameter>() { new SqlParameter("@tbl_name", "sims.sims_parameter"),
                            new SqlParameter("@tbl_col_name1", "sims_appl_parameter,sims_appl_form_field_value1,sims_appl_form_field_value3"),
                            new SqlParameter("@tbl_cond","sims_appl_form_field='Floating Button'" ),
                            });
                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, dr);
            }

        }




        [Route("getlastLogin")]
        public HttpResponseMessage getlastLogin(string uname)
        {
            bool str = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() { new SqlParameter("@tbl_name", "comn.comn_user"),
                            new SqlParameter("@tbl_col_name1", "comn_user_last_login"),
                             new SqlParameter("@tbl_cond","comn_user_name=" + "'" + uname + "'"),
                            });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            if (string.IsNullOrEmpty(dr["comn_user_last_login"].ToString()))
                            {
                                str = true;
                            }
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, str);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, str);

            }

        }


        [Route("getDetails")]
        public HttpResponseMessage getDetails(string uname, string pass, string t, string sc)
        {
            SqlConnection sqlcon = new SqlConnection(WebConfigurationManager.ConnectionStrings[sc].ConnectionString);

            try
            {
                DataSet dr = new DataSet();

                if (sqlcon.State == ConnectionState.Closed) sqlcon.Open();
                var objCom = new SqlCommand("[comn].[get_user_details]", sqlcon);
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandTimeout = 300;
                objCom.Parameters.AddWithValue("@UserName", uname);
                objCom.Parameters.AddWithValue("@pass", pass);
                objCom.Parameters.AddWithValue("@t", t);


                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = objCom;

                da.Fill(dr);

                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr.Tables[0]);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "");

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "");

            }

        }


        [Route("getDetails")]
        public HttpResponseMessage getDetails(string uname, string pass)
        {
            SqlConnection sqlcon = new SqlConnection(WebConfigurationManager.ConnectionStrings["asd"].ConnectionString);

            try
            {
                DataSet dr = new DataSet();

                if (sqlcon.State == ConnectionState.Closed) sqlcon.Open();
                var objCom = new SqlCommand("[comn].[get_user_details]", sqlcon);
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandTimeout = 300;
                objCom.Parameters.AddWithValue("@UserName", uname);
                objCom.Parameters.AddWithValue("@pass", pass);
                // objCom.Parameters.AddWithValue("@t", t);


                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = objCom;

                da.Fill(dr);

                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr.Tables[0]);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "");

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "");

            }

        }



        [Route("gettype")]
        public HttpResponseMessage gettype(string uname)
        {
            string cnt = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[get_user_details]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@UserName",uname),
                           
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cnt = dr["nm"].ToString();

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, cnt);
        }


        [Route("GetAgecompare")]
        public HttpResponseMessage GetAgecompare(string cur_code, string acad_yr, string grade)
        {
            age em = new age();
            List<age> lstAge = new List<age>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Sims_admission_proc]",
                    new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'Y'),
                            new SqlParameter("@sims_admission_cur_code",cur_code),
                            new SqlParameter("@sims_admission_academic_year",acad_yr),
                            new SqlParameter("@sims_admission_grade_code",grade),
                        });
                    if (dr.HasRows)
                    {
                        if (dr.Read())
                        {
                            em.sims_birth_date_from = dr["sims_birth_date_from"].ToString();
                            em.sims_birth_date_to = dr["sims_birth_date_to"].ToString();
                            lstAge.Add(em);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lstAge);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstAge);
        }

        [Route("GetAboutUsData")]
        public HttpResponseMessage GetAboutUsData()
        {
            List<SqlParameter> lst = new List<SqlParameter>();
            List<age> result = new List<age>();
            //lst.Add(new SqlParameter("@opr", "A"));
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[admission_enquiry]",
                        new List<SqlParameter>() { 
                            new SqlParameter("@opr", "E")
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            age c = new age();
                            c.referal_code = dr["sims_appl_parameter"].ToString();
                            c.referal_name = dr["Sims_appl_form_field_value1"].ToString();
                            result.Add(c);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
        }

        [Route("getEngAr")]
        public HttpResponseMessage getEngAr()
        {
            List<enar> lstModules = new List<enar>();

            string uname = string.Empty;
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                    new List<SqlParameter>() { new SqlParameter("@tbl_name", "sims.sims_arabic_english"),
                            new SqlParameter("@tbl_col_name1", "sims_english,sims_arabic_en"),
                          //  new SqlParameter("@tbl_cond","sims_enroll_no="+"'" + enroll + "'" ),
                            });
                while (dr.Read())
                {
                    enar ob = new enar();

                    ob.sims_english = dr["sims_english"].ToString();
                    ob.sims_arabic = dr["sims_arabic_en"].ToString();

                    lstModules.Add(ob);

                }
                return Request.CreateResponse(HttpStatusCode.OK, lstModules);
            }

        }


        [Route("getEngAr")]
        public HttpResponseMessage getEngAr(string appl_code)
        {
            List<enar> lstModules = new List<enar>();

            string uname = string.Empty;
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("sims.get_english_arabic_proc",
                    new List<SqlParameter>() { new SqlParameter("@comn_appl_code", appl_code),
                          //  new SqlParameter("@tbl_col_name1", "sims_english,sims_arabic_en"),
                          //  new SqlParameter("@tbl_cond","sims_enroll_no="+"'" + enroll + "'" ),
                            });
                while (dr.Read())
                {
                    enar ob = new enar();

                    ob.sims_english = dr["sims_english"].ToString();
                    ob.sims_arabic = dr["sims_arabic_en"].ToString();

                    lstModules.Add(ob);

                }
                return Request.CreateResponse(HttpStatusCode.OK, lstModules);
            }

        }



        [Route("getuserpass")]
        public HttpResponseMessage getuserpass(string email_id)
        {
            using (DBConnection db = new DBConnection())
            {
                db.Open();


                DataSet dr = db.ExecuteStoreProcedureDS("GetData",
                    new List<SqlParameter>() { new SqlParameter("@tbl_name", "comn.comn_user"),
                            new SqlParameter("@tbl_col_name1", "comn_user_name,comn_user_password"),
                             new SqlParameter("@tbl_cond","comn_user_email=" + "'" + email_id + "'"),

                            });
                if (dr.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dr.Tables[0]);
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, "");
            }

        }



    }
}



