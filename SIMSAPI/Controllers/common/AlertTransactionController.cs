﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using log4net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.common
{
    [RoutePrefix("api/AlertTransaction")]
    public class AlertTransactionController:ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //select
        [Route("getAlertTransactionDatewise")]
        public HttpResponseMessage getAlertTransactionDatewise(string from_date, string to_date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlertTransactionDatewise(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAlertTransactionDatewise"));

            List<Comn050> mod_list = new List<Comn050>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_alert_transaction_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'P'),
                            new SqlParameter("@from_date",db.DBYYYYMMDDformat(from_date)),
                            new SqlParameter("@to_date",db.DBYYYYMMDDformat(to_date)),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn050 obj = new Comn050();
                            obj.comn_alert_number = dr["comn_alert_number"].ToString();
                            obj.comn_alert_message = dr["comn_alert_message"].ToString();
                            obj.comn_alert_mod_code = dr["comn_alert_mod_code"].ToString();
                            obj.comn_alert_priority_code = dr["comn_alert_priority_code"].ToString();
                            obj.comn_alert_type_code = dr["comn_alert_type_code"].ToString();
                            obj.comn_alert_user_code = dr["comn_alert_user_code"].ToString();

                            obj.user_name = dr["user_name"].ToString();
                            obj.comn_alert_date = db.UIDDMMYYYYformat(dr["comn_alert_date"].ToString());
                            obj.mod_name = dr["comn_mod_name_en"].ToString();
                            obj.alert_type = dr["sims_appl_form_field_value1"].ToString();
                            obj.alert_priority = dr["priority"].ToString();
                            obj.UserCount = 0;

                            if (dr["comn_alert_status"].ToString().Equals("U"))
                            {
                                obj.comn_alert_status = true;

                            }
                            else
                            {

                                obj.comn_alert_status = false;
                            }
                            mod_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getAlertType")]
        public HttpResponseMessage getAlertType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlertType(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAlertType"));

            List<Comn050> goaltarget_list = new List<Comn050>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_alert_transaction_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'T'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn050 obj = new Comn050();
                            obj.alert_type = dr["sims_appl_form_field_value1"].ToString();
                            obj.alert_code = dr["sims_appl_parameter"].ToString();

                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getAlertPriority")]
        public HttpResponseMessage getAlertPriority()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlertPriority(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAlertPriority"));

            List<Comn050> goaltarget_list = new List<Comn050>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_alert_transaction_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'V'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn050 obj = new Comn050();
                            obj.alert_priority = dr["sims_appl_form_field_value1"].ToString();
                            obj.alert_priority_code = dr["sims_appl_parameter"].ToString();


                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("getModuleCode")]
        public HttpResponseMessage getModuleCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModuleCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getModuleCode"));

            List<Comn050> goaltarget_list = new List<Comn050>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_alert_transaction_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'W'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn050 obj = new Comn050();
                            obj.mod_name = dr["comn_mod_name_en"].ToString();
                            obj.mod_code = dr["comn_mod_code"].ToString();

                            goaltarget_list.Add(obj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);
        }

        [Route("CUDAlertTransaction")]
        public HttpResponseMessage CUDAlertTransaction(List<Comn050> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Comn050 obj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_alert_transaction_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@comn_alert_number",obj.comn_alert_number),
                                new SqlParameter("@comn_alert_message", obj.comn_alert_message),

                                new SqlParameter("@comn_alert_mod_code",obj.comn_alert_mod_code),
                                new SqlParameter("@comn_alert_priority_code", obj.comn_alert_priority_code),

                                new SqlParameter("@comn_alert_type_code",obj.comn_alert_type_code),
                                new SqlParameter("@comn_alert_user_code", obj.comn_alert_user_code),
                   
                                new SqlParameter("@comn_alert_date",obj.comn_alert_date),
                                new SqlParameter("@comn_alert_status", "U"),
                                
                                new SqlParameter("@comn_alert_sender_type", obj.comn_alert_sender_type),
                               
                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /*message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
           // return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("Get_user_access_rights")]
        public HttpResponseMessage Get_user_access_rights(string username,string appl_code)
        {
            List<Comn006> mod_list = new List<Comn006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_access_rights",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@comn_user_name", username),
                            //new SqlParameter("@comn_mod_code", mod_code),
                            new SqlParameter("@comn_appl_code", appl_code)

                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn006 comnobj = new Comn006();
                            comnobj.comn_user_code = dr["comn_user_code"].ToString();
                            comnobj.comn_user_name = dr["comn_user_name"].ToString();
                            comnobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            comnobj.comn_user_appl_name = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_role_code = dr["comn_user_role_id"].ToString();

                            if (dr["comn_user_read"].ToString().ToLower() == "1")
                                comnobj.comn_user_read = true;
                            else
                                comnobj.comn_user_read = false;

                            if (dr["comn_user_insert"].ToString().ToLower() == "1")
                                comnobj.comn_user_insert = true;
                            else
                                comnobj.comn_user_insert = false;

                            if (dr["comn_user_update"].ToString().ToLower() == "1")
                                comnobj.comn_user_update = true;
                            else
                                comnobj.comn_user_update = false;

                            if (dr["comn_user_delete"].ToString().ToLower() == "1")
                                comnobj.comn_user_delete = true;
                            else
                                comnobj.comn_user_delete = false;

                            if (dr["comn_user_massupdate"].ToString().ToLower() == "1")
                                comnobj.comn_user_massupdate = true;
                            else
                                comnobj.comn_user_massupdate = false;

                            if (dr["comn_user_admin"].ToString().ToLower() == "1")
                                comnobj.comn_user_admin = true;
                            else
                                comnobj.comn_user_admin = false;

                            if (dr["comn_user_appl_status"].ToString().ToLower() == "a")
                                comnobj.comn_user_appl_status = true;
                            else
                                comnobj.comn_user_appl_status = false;
                            mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

    }
}