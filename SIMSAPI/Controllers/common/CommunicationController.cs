﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.COMMON;
using System.Linq;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/Communication")]
    public class CommunicationController : ApiController
    {
        #region portal_communication

        [Route("GetAllPortalCommunication")]
        public HttpResponseMessage GetAllPortalCommunication(string user_code_from)
        {
            //Object[] ob = new Object[2];
            List<portal_communication> portal_communication = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SS"),
                            new SqlParameter("@user_code_from", user_code_from),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication simsobj = new portal_communication();
                            simsobj.sims_communication_number = dr["sims_comm_number"].ToString();
                            simsobj.sims_subject_name = dr["sims_comm_subject"].ToString();
                            simsobj.sims_subject_id = dr["sims_subject_id"].ToString();
                            simsobj.sims_communication_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
                            simsobj.sims_subject_new_message_count = dr["New_meesages"].ToString();
                            simsobj.sims_subject_status = dr["sims_comm_status"].ToString();
                            simsobj.sims_comm_user_name = dr["Sender_name"].ToString();
                            simsobj.sims_comn_tran_no = dr["sims_comm_tran_number"].ToString();
                            simsobj.sims_comm_message = dr["sims_comm_message"].ToString();
                            simsobj.sims_communication_date1 = dr["sims_comm_date2"].ToString();
                            simsobj.sims_comm_recepient_name = dr["receipient_name"].ToString();
                            simsobj.sims_comm_recepient_name1 = dr["receipient_name1"].ToString();

                            if (simsobj.comnn_histr == null)
                                simsobj.comnn_histr = new List<portal_communication_his>();
                            portal_communication.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
        }

      //[Route("GetAllPortalCommunication")]
      //  public HttpResponseMessage GetAllPortalCommunication(string user_code_from)
      //  {
      //      //Object[] ob = new Object[2];
      //      List<portal_communication> portal_communication = new List<portal_communication>();
      //      try
      //      {
      //          using (DBConnection db = new DBConnection())
      //          {
      //              db.Open();
      //              SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication_abps",
      //                  new List<SqlParameter>()
      //                   {
      //                      new SqlParameter("@opr", "SS"),
      //                      new SqlParameter("@user_code_from", user_code_from),
      //                   });
      //              if (dr.HasRows)
      //              {
      //                  while (dr.Read())
      //                  {
      //                      portal_communication simsobj = new portal_communication();
      //                      simsobj.sims_communication_number = dr["sims_comm_number"].ToString();
      //                      simsobj.sims_subject_name = dr["sims_comm_subject"].ToString();
      //                      simsobj.sims_subject_id = dr["sims_subject_id"].ToString();
      //                      simsobj.sims_communication_date = dr["sims_comm_date"].ToString();
      //                      simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
      //                      simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
      //                      simsobj.sims_subject_new_message_count = dr["New_meesages"].ToString();
      //                      simsobj.sims_subject_status = dr["sims_comm_status"].ToString();
      //                      simsobj.sims_comm_user_name = dr["Sender_name"].ToString();
      //                      simsobj.sims_comn_tran_no = dr["sims_comm_tran_number"].ToString();
      //                      simsobj.sims_comm_message = dr["sims_comm_message"].ToString();
      //                      simsobj.sims_communication_date1 = dr["sims_comm_date2"].ToString();
      //                      simsobj.sims_comm_recepient_name = dr["receipient_name"].ToString();
      //                      simsobj.sims_comm_recepient_name1 = dr["receipient_name1"].ToString();

      //                      if (simsobj.comnn_histr == null)
      //                          simsobj.comnn_histr = new List<portal_communication_his>();
      //                      portal_communication.Add(simsobj);
      //                  }
      //              }
      //          }
      //          return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
      //      }
      //      catch (Exception e)
      //      {
      //          return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
      //      }
      //  }-->

        [Route("GetAllPortalCommunication_inactive")]
        public HttpResponseMessage GetAllPortalCommunication_inactive(string user_code_from)
        {
            //Object[] ob = new Object[2];
            List<portal_communication> portal_communication = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication_abps",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "SS"),
                            new SqlParameter("@user_code_from", user_code_from),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication simsobj = new portal_communication();
                            simsobj.sims_communication_number = dr["sims_comm_number"].ToString();
                            simsobj.sims_subject_name = dr["sims_comm_subject"].ToString();
                            simsobj.sims_subject_id = dr["sims_subject_id"].ToString();
                            simsobj.sims_communication_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
                            simsobj.sims_subject_new_message_count = dr["New_meesages"].ToString();
                            simsobj.sims_subject_status = dr["sims_comm_status"].ToString();
                            simsobj.sims_comm_user_name = dr["Sender_name"].ToString();
                            simsobj.sims_comn_tran_no = dr["sims_comm_tran_number"].ToString();
                            simsobj.sims_comm_message = dr["sims_comm_message"].ToString();
                            simsobj.sims_communication_date1 = dr["sims_comm_date2"].ToString();
                            simsobj.sims_comm_recepient_name = dr["receipient_name"].ToString();
                            simsobj.sims_comm_recepient_name1 = dr["receipient_name1"].ToString();

                            if (simsobj.comnn_histr == null)
                                simsobj.comnn_histr = new List<portal_communication_his>();
                            portal_communication.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
        }


        [Route("GetPortalCommunication")]
        public HttpResponseMessage GetPortalCommunication(string user_code_from, string user_code_to)
        {
            //Object[] ob = new Object[2];
            List<portal_communication> portal_communication = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@user_code_from", user_code_from),
                            new SqlParameter("@user_code_to", user_code_to),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication simsobj = new portal_communication();
                            simsobj.sims_communication_number = dr["sims_comm_number"].ToString();
                            simsobj.sims_subject_name = dr["sims_comm_subject"].ToString();
                            simsobj.sims_subject_id = dr["sims_subject_id"].ToString();
                            simsobj.sims_communication_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
                            simsobj.sims_subject_new_message_count = dr["New_meesages"].ToString();
                            simsobj.sims_subject_status = dr["sims_comm_status"].ToString();

                            if (simsobj.comnn_histr == null)
                                simsobj.comnn_histr = new List<portal_communication_his>();
                            portal_communication.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
        }

        [Route("GetPortalCommunicationHistory")]
        public HttpResponseMessage GetPortalCommunicationHistory(string user_code_from, string user_code_to, string sims_subject_id, string comm_number)
        {
            //Object[] ob = new Object[2];
            List<portal_communication_his> portal_communication = new List<portal_communication_his>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "CH"),
                            new SqlParameter("@user_code_from", user_code_from),
                            new SqlParameter("@user_code_to", user_code_to),
                            new SqlParameter("@sims_subject_id", sims_subject_id),
                            new SqlParameter("@comm_number",comm_number)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication_his simsobj = new portal_communication_his();
                            string str = dr["sims_subject_id"].ToString();
                            simsobj.sims_communication_number = dr["sims_comm_number"].ToString();
                            simsobj.sims_subject_name = dr["sims_comm_message"].ToString();
                            simsobj.sims_subject_name1 = dr["sims_comm_message1"].ToString();
                            simsobj.sims_subject_id = dr["sims_subject_id"].ToString();
                            simsobj.sims_communication_date = dr["sims_comm_date"].ToString();
                            simsobj.sims_comm_sender_id = dr["sims_comm_sender_id"].ToString();
                            simsobj.sims_comm_recepient_id = dr["sims_comm_recepient_id"].ToString();
                            simsobj.sims_subject = dr["sims_comm_subject"].ToString();
                            simsobj.msgdoc = new List<message_doc>();
                            simsobj.sims_message_file_name = dr["sims_comm_message_file_name"].ToString();
                            string file_name = dr["sims_comm_message_file_name"].ToString();
                            var v = (from p in portal_communication where simsobj.sims_subject_id == str select p);

                            if (file_name != null || file_name != "")
                            {
                                message_doc msg = new message_doc();
                                msg.sims_message_file_name = dr["sims_comm_message_file_name"].ToString();
                                if (v.Count() == 0)
                                {
                                    simsobj.msgdoc.Add(msg);

                                }
                                else
                                {
                                    v.ElementAt(0).msgdoc.Add(msg);
                                }
                            }
                            portal_communication.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, portal_communication);
            }
        }

        [Route("PortalCommunication")]
        public HttpResponseMessage PortalCommunication(portal_communication_his simsobj)
        {
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_communication_transaction",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_cur_code", string.Empty),
                            new SqlParameter("@sims_academic_year", string.Empty),
                            new SqlParameter("@sims_comm_number", simsobj.sims_communication_number),
                            new SqlParameter("@sims_comm_user_code", simsobj.sims_comm_sender_id),
                            new SqlParameter("@sims_comm_sender_id", simsobj.sims_comm_sender_id),
                            new SqlParameter("@sims_comm_receipt_id", simsobj.sims_comm_recepient_id),
                            new SqlParameter("@sims_comm_message", simsobj.sims_subject_name),
                            new SqlParameter("@sims_comm_status", "N"),
                            new SqlParameter("@subjectid", simsobj.sims_subject_id)
                         });
                    flag = dr.RecordsAffected > 0 ? true : false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
        }

        [Route("getusersList")]
        public HttpResponseMessage getusersList(string user_name)
        {
            List<portal_communication> users = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "U"),
                            new SqlParameter("@user_code_from", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication obj = new portal_communication();
                            obj.sims_comm_recepient_id = dr["User_Names"].ToString();
                            obj.sims_comm_user_name = dr["comn_user_alias"].ToString();
                            users.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
        }

        //[Route("pstCommunication")]
        //public HttpResponseMessage pstCommunication(string message, string subject, string from_user, List<portal_communication> data)
        //{
        //    bool flag = false;
        //    try
        //    {
        //        foreach (portal_communication user in data)
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                db.Open();
        //                SqlDataReader dr = db.ExecuteStoreProcedure("sims_communication",
        //                    new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", "I"),
        //                    new SqlParameter("@sims_cur_code", string.Empty),
        //                    new SqlParameter("@sims_academic_year", string.Empty),
        //                    new SqlParameter("@sims_comm_user_name", from_user),
        //                    new SqlParameter("@sims_comm_sender_id", from_user),
        //                    new SqlParameter("@sims_comm_recepient_id", user.sims_comm_recepient_id),
        //                    new SqlParameter("@sims_comm_category", string.Empty),
        //                    new SqlParameter("@sims_comm_subject", subject),//Subject
        //                    new SqlParameter("@sims_comm_message", message),//Message
        //                    new SqlParameter("@sims_comm_status", "N"),
        //                 });
        //                flag = dr.RecordsAffected > 0 ? true : false;
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, flag);
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, flag);
        //    }
        //}


        [Route("pstCommunication")]
        public HttpResponseMessage pstCommunication(string message, string subject, string from_user, string file_name, List<portal_communication> data)
        {
            bool flag = false;
            try
            {
                foreach (portal_communication user in data)
                {
                    if (user.sims_is_stud)
                    {
                        user.sims_comm_user_name = user.sims_comm_recepient_id;
                        user.sims_comm_recepient_id = user.parent_id;

                    }
                    else
                    {
                        user.sims_comm_user_name = null;
                    }

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_communication",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_cur_code", string.Empty),
                            new SqlParameter("@sims_academic_year", string.Empty),
                            new SqlParameter("@sims_comm_user_name", user.sims_comm_user_name),
                            new SqlParameter("@sims_comm_sender_id", from_user),
                            new SqlParameter("@sims_comm_recepient_id", user.sims_comm_recepient_id),
                            new SqlParameter("@sims_comm_category", string.Empty),
                            new SqlParameter("@sims_comm_subject", subject),//Subject
                            new SqlParameter("@sims_comm_message",user.sims_comm_message),//Message
                            new SqlParameter("@sims_file_name",file_name),
                            new SqlParameter("@sims_comm_status", "N"),
                         });
                        flag = dr.RecordsAffected > 0 ? true : false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
        }


        [Route("pstCommunication")]
        public HttpResponseMessage pstCommunication(List<portal_communication> data)
        {
            bool flag = false;
            try
            {
                foreach (portal_communication user in data)
                {
                    if (user.sims_is_stud)
                    {
                        user.sims_comm_user_name = user.sims_comm_recepient_id;
                        user.sims_comm_recepient_id = user.parent_id;

                    }
                    else
                    {
                        user.sims_comm_user_name = null;
                    }

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_communication",
                            new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "I"),
                            new SqlParameter("@sims_cur_code", string.Empty),
                            new SqlParameter("@sims_academic_year", string.Empty),
                            new SqlParameter("@sims_comm_user_name", user.sims_comm_user_name),
                            new SqlParameter("@sims_comm_sender_id", user.sims_comm_sender_id),
                            new SqlParameter("@sims_comm_recepient_id", user.sims_comm_recepient_id),
                            new SqlParameter("@sims_comm_category", string.Empty),
                            new SqlParameter("@sims_comm_subject", user.sims_subject_name),//Subject
                            new SqlParameter("@sims_comm_message",user.sims_comm_message),//Message
                            new SqlParameter("@sims_file_name",""),
                            new SqlParameter("@sims_comm_status", "N"),
                         });
                        flag = dr.RecordsAffected > 0 ? true : false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
        }



        [Route("communicationcountupdate")]
        public HttpResponseMessage communicationcountupdate(string communicationID, string subjectID, string user_code_from)
        {
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "UC"),
                            new SqlParameter("@comm_number", communicationID),
                            new SqlParameter("@sims_subject_id", subjectID),
                            new SqlParameter("@user_code_from", user_code_from),


                         });
                    flag = dr.RecordsAffected > 0 ? true : false;
                }

                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
        }

        [Route("closecommunicationsubject")]
        public HttpResponseMessage closecommunicationsubject(string subjectID)
        {
            bool flag = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "CS"),
                            new SqlParameter("@sims_subject_id", subjectID),
                         });
                    flag = dr.RecordsAffected > 0 ? true : false;
                }

                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, flag);
            }
        }

        [Route("getPortalCommunicationCounts")]
        public HttpResponseMessage getPortalCommunicationCounts(string user_code_from)
        {
            //Object[] ob = new Object[2];
            string total_messages = "0";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "TC"),
                            new SqlParameter("@user_code_from", user_code_from),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            total_messages = dr["total_messages"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, total_messages);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, total_messages);
            }
        }

        [Route("getgradesection")]
        public HttpResponseMessage getgradesection(string user_name)
        {
            List<portal_communication> users = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "GS"),
                            new SqlParameter("@user_code_from", user_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication obj = new portal_communication();
                            obj.sims_grade_section_name = dr["Grade_section_name"].ToString();
                            obj.sims_grade_section_code = dr["Grade_section_code"].ToString();
                            users.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
        }

        [Route("getgradesectionwise_parent")]
        public HttpResponseMessage getgradesectionwise_parent(string grade_section,string user_code_from)
        {
            List<portal_communication> users = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "GP"),
                            new SqlParameter("@grade_section_code", grade_section),
                            new SqlParameter("@user_code_from", user_code_from)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication obj = new portal_communication();
                            obj.sims_comm_user_name = dr["sims_parent_father_first_name"].ToString();
                            obj.sims_comm_recepient_id = dr["sims_parent_login_code"].ToString();
                            obj.sims_grade_section_code = dr["Grade_section_code"].ToString();
                            users.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
        }

        [Route("get_parent_details")]
        public HttpResponseMessage get_parent_details()
        {
            List<portal_communication> users = new List<portal_communication>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("portal_commnunication",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "AS"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            portal_communication obj = new portal_communication();
                            obj.sims_comm_user_name = dr["sims_parent_father_first_name"].ToString();
                            obj.sims_full_name = dr["fullName"].ToString();                            
                            obj.sims_comm_recepient_id = dr["sims_parent_login_code"].ToString();
                            obj.sims_grade_section_code = dr["Grade_section_code"].ToString();
                            users.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
        }
        #endregion

    }
}