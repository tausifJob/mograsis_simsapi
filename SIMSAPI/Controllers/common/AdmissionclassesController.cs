﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.common
{
    [RoutePrefix("api/admissionclasses")]
    public class AdmissionclassesController : ApiController
    {

        [Route("GetAllAdmissionClasses")]
        public HttpResponseMessage GetAllAdmissionClasses()
        {
            List<admissionclasses> mod_list = new List<admissionclasses>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_classes_proc]",
                         new List<SqlParameter>() 
                         { 
                   new SqlParameter("@opr","S")
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionclasses simsobj = new admissionclasses();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            simsobj.sims_birth_date_from = db.UIDDMMYYYYformat(dr["sims_birth_date_from"].ToString());
                            simsobj.sims_birth_date_to = db.UIDDMMYYYYformat(dr["sims_birth_date_to"].ToString());
                            simsobj.sims_admission_start_from = db.UIDDMMYYYYformat(dr["sims_admission_start_from"].ToString());
                            simsobj.sims_admission_end = db.UIDDMMYYYYformat(dr["sims_admission_end"].ToString());
                            mod_list.Add(simsobj);
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("CURDAdmissionClasses")]
        public HttpResponseMessage CURDAdmissionClasses(List<admissionclasses> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (admissionclasses obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_classes_proc]",
                             new List<SqlParameter>() 
                         { 
                                new SqlParameter("@OPR", obj.opr),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_term_code", obj.sims_term_code),
                                new SqlParameter("@sims_status", obj.sims_status.Equals(true)?"A":"I"),
                                new SqlParameter("@sims_birth_date_from", db.DBYYYYMMDDformat(obj.sims_birth_date_from)),
                                new SqlParameter("@sims_birth_date_to", db.DBYYYYMMDDformat(obj.sims_birth_date_to)),
                                new SqlParameter("@sims_admission_start_from", db.DBYYYYMMDDformat(obj.sims_admission_start_from)),
                                new SqlParameter("@sims_admission_end", db.DBYYYYMMDDformat(obj.sims_admission_end))
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        dr.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, alert_status);

        }

        [Route("GetAllterm")]
        public HttpResponseMessage GetAllterm(string cur_code, string academic_year)
        {
            List<admissionclasses> mod_list = new List<admissionclasses>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_classes_proc]",
                         new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr","T"),
                    new SqlParameter("@sims_cur_code", cur_code),
                    new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            admissionclasses simsobj = new admissionclasses();
                            simsobj.sims_term_code = dr["sims_term_code"].ToString();
                            simsobj.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }



            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }


        [Route("AllGrades_p")]
        public HttpResponseMessage AllGrades_p(Sims028 pa)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : AllGrades_p(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "AllGrades_p"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_classes_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@OPR",'G'),
                                new SqlParameter("@sims_cur_code", pa.sims_cur_code),
                                new SqlParameter("@sims_academic_year", pa.sims_academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [Route("sectionCommon_p")]
        public HttpResponseMessage sectionCommon_p(Sims028 param)
        {
            //string debug = "MODULE :{0},APPLICATION :{1},METHOD : sectionCommon_p(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "STUDENT", "sectionCommon_p"));

            List<Sims028> grade_list = new List<Sims028>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_admission_classes_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_cur_code", param.sims_cur_code),
                                new SqlParameter("@sims_academic_year", param.sims_academic_year),
                                new SqlParameter("@sims_grade_code",param.sims_grade_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims028 simsobj = new Sims028();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}