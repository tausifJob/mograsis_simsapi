﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;
using SIMSAPI.Models.Common;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/GlobalSearch")]
    [BasicAuthentication]
    public class GlobalSearchController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("getSearchStudent")]
        public HttpResponseMessage getSearchStudent(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudent"));
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@student_middle_name", comnobj.search_std_middle_name),
                            new SqlParameter("@student_last_name", comnobj.search_std_last_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                            new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@academic_year", comnobj.sims_academic_year),  
                            new SqlParameter("@sims_student_ea_number", comnobj.sims_student_ea_number),   
                            new SqlParameter("@user_name", comnobj.search_parent_emp_id),                
                            new SqlParameter("@sims_std_transport_bus", comnobj.search_std_transport_bus),
                            new SqlParameter("@parent_id", comnobj.search_parent_no),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_sname_in_arabic = dr["Name_in_Arabic"].ToString();
                            objNew.s_class = dr["Class"].ToString();
                            try
                            {
                                objNew.teacher_name = dr["teacher_name"].ToString();
                            }
                            catch (Exception ex) { }
                            objNew.s_parent_id = dr["Parent_Id"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            // objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_student_enroll_number"].ToString();
                            objNew.Teacher_Type = dr["Name_in_English"].ToString();
                            objNew.search_std_image = dr["StudentImg"].ToString();
                            objNew.name = objNew.s_sname_in_english;
                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            try
                            {
                                //objNew.search_mother_first_name = dr["mother_FirstName"].ToString();
                                //objNew.search_mother_last_name = dr["mother_LastName"].ToString();
                                //objNew.search_mother_midd_name = dr["mother_MiddleName"].ToString();
                                objNew.mother_name = dr["mother_name"].ToString();
                                objNew.mother_email_id = dr["mother_email_id"].ToString();
                                objNew.mother_mobile_no = dr["mother_Mobile"].ToString();


                                //objNew.search_guardian_first_name = dr["guardian_FirstName"].ToString();
                                //objNew.search_guardian_last_name = dr["guardian_LastName"].ToString();
                                //objNew.search_guardian_midd_name = dr["guardian_MiddleName"].ToString();
                                objNew.guardian_name = dr["guardian_name"].ToString();
                                objNew.guardian_email_id = dr["guardian_email_id"].ToString();
                                objNew.guardian_mobile_no = dr["guardian_Mobile"].ToString();
                               
                            }
                            catch (Exception ex) { }

                            try
                            {
                                objNew.sims_roll_number = int.Parse(new String( dr["sims_roll_number"].ToString().Where(Char.IsDigit).ToArray()));
                            }
                            catch (Exception ex){ }
                            objNew.sims_parent_emp_id = dr["EmpName"].ToString();
                            objNew.s_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.section_code = dr["sims_section_code"].ToString();
                            objNew.status_name = dr["status_name"].ToString();
                            objNew.sims_student_ea_number = dr["sims_student_ea_number"].ToString();
                            objNew.cancelled_date = dr["cancelled_date"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }



        [Route("getSearchStudent1")]
        public HttpResponseMessage getSearchStudent1(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudent"));

            CommonUserControlClass comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.sims_grade_code),
                            new SqlParameter("@section_code", comnobj.sims_section_code),
                            new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@academic_year", comnobj.sims_academic_year),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_sname_in_arabic = dr["Name_in_Arabic"].ToString();
                            objNew.s_class = dr["Class"].ToString();
                            objNew.s_parent_id = dr["Parent_Id"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            // objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_student_enroll_number"].ToString();
                            objNew.Teacher_Type = dr["Name_in_English"].ToString();
                            objNew.search_std_image = dr["StudentImg"].ToString();
                            objNew.name = objNew.s_sname_in_english;
                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            objNew.s_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.section_code = dr["sims_section_code"].ToString();
                            objNew.status_name = dr["status_name"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchStudent1_ahgs")]
        public HttpResponseMessage getSearchStudent1_ahgs(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudent"));

            CommonUserControlClass comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_ahgs_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.sims_grade_code),
                            new SqlParameter("@section_code", comnobj.sims_section_code),
                            new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@academic_year", comnobj.sims_academic_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_sname_in_arabic = dr["Name_in_Arabic"].ToString();
                            objNew.s_class = dr["Class"].ToString();
                            objNew.s_parent_id = dr["Parent_Id"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            // objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_student_enroll_number"].ToString();
                            objNew.Teacher_Type = dr["Name_in_English"].ToString();
                            objNew.search_std_image = dr["StudentImg"].ToString();
                            objNew.name = objNew.s_sname_in_english;
                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            objNew.s_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.section_code = dr["sims_section_code"].ToString();
                            objNew.status_name = dr["status_name"].ToString();
                            objNew.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            objNew.sims_section_name_en = dr["sims_section_name_en"].ToString();

                            try
                            {
                                objNew.sims_student_dob = dr["sims_student_dob"].ToString();
                                objNew.parent_name = dr["parent_name"].ToString();
                                objNew.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                objNew.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                objNew.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                objNew.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                objNew.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                                objNew.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                                objNew.sims_student_date = dr["sims_student_date"].ToString();
                                objNew.sims_student_transport_status = dr["sims_student_transport_status"].ToString();
                                objNew.nationality = dr["nationality"].ToString();
                                objNew.sims_student_gender = dr["sims_student_gender"].ToString();
                                objNew.rfid_card = dr["rfid_card"].ToString();
                                objNew.mother_name = dr["mother_name"].ToString();
                                objNew.sims_transport_grade_code = dr["sims_transport_grade_code"].ToString();
                                objNew.sims_transport_section_code = dr["sims_transport_section_code"].ToString();
                            }
                            catch (Exception) { }

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchStudentUnassigned")]
        public HttpResponseMessage getSearchStudentUnassigned(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchStudentUnassigned(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchStudentUnassigned"));
            CommonUserControlClass comnobj = new CommonUserControlClass();
            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@opr_mem_code", 'S'),
                            new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                            new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                            new SqlParameter("@passport_no", comnobj.search_std_passport_no),
                            new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@std_nationality", comnobj.sims_nationality_name_en),
                            new SqlParameter("@std_national_id", comnobj.std_national_id),
                            new SqlParameter("@cur_code", comnobj.s_cur_code),
                            new SqlParameter("@academic_year", comnobj.sims_academic_year),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            objNew.s_sname_in_arabic = dr["Name_in_Arabic"].ToString();
                            objNew.s_class = dr["Class"].ToString();
                            //    objNew.s_parent_id = dr["Parent_Id"].ToString();
                            //  objNew.s_parent_name = dr["Parent_Name"].ToString();
                            // objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_student_enroll_number"].ToString();
                            objNew.Teacher_Type = dr["Name_in_English"].ToString();
                            //objNew.search_std_image = dr["StudentImg"].ToString();
                            objNew.name = objNew.s_sname_in_english;
                            //objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            //objNew.search_parent_first_name = dr["FirstName"].ToString();
                            //objNew.search_parent_last_name = dr["LastName"].ToString();
                            //objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            // objNew.Mobile_no = dr["Mobile"].ToString();
                            //objNew.Mail_id = dr["Email"].ToString();
                            objNew.s_cur_code = dr["sims_cur_code"].ToString();
                            objNew.sims_academic_year = dr["sims_academic_year"].ToString();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.section_code = dr["sims_section_code"].ToString();
                            //       objNew.status_name = dr["status_name"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }


        [Route("getSearchParentUassigned")]
        public HttpResponseMessage getSearchParentUassigned(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchParent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchParent"));

            CommonUserControlClass comnobj = new CommonUserControlClass();
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                            new SqlParameter("@opr_mem_code", 'P'),
                          //  new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                          //  new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                          //  new SqlParameter("@passport_no", ""),
                           // new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@parent_id", comnobj.search_parent_id),
                            new SqlParameter("@parent_name", comnobj.search_parent_name),
                            new SqlParameter("@parent_mail_id", comnobj.search_parent_email_id),
                            new SqlParameter("@parent_mobile_no", comnobj.search_parent_mobile_no),
                           // new SqlParameter("@teacher_id", comnobj.search_teacher_id),
                          //  new SqlParameter("@teacher_name", comnobj.search_teacher_name),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_parent_id = dr["sims_parent_number"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            objNew.Mail_id = dr["sims_parent_father_email"].ToString();
                            objNew.Mobile_no = dr["sims_parent_father_mobile"].ToString();
                            //   objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            // objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            //objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_parent_number"].ToString();
                            objNew.Teacher_Type = dr["Parent_Name"].ToString();
                            objNew.search_par_image = dr["parentImg"].ToString();

                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            // objNew.s_class = dr["Class"].ToString();

                            objNew.mother_name = dr["mother_name"].ToString();
                            objNew.mother_email_id = dr["sims_parent_mother_email"].ToString();
                            objNew.mother_mobile_no = dr["sims_parent_mother_mobile"].ToString();

                            objNew.guardian_name = dr["guardian_name"].ToString();
                            objNew.guardian_email_id = dr["sims_parent_guardian_email"].ToString();
                            objNew.guardian_mobile_no = dr["sims_parent_guardian_mobile"].ToString();
                            objNew.name = objNew.s_parent_name;
                            objNew.status_name = dr["status_name"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }



        [Route("getSearchParent")]
        public HttpResponseMessage getSearchParent(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchParent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchParent"));

            CommonUserControlClass comnobj = new CommonUserControlClass();
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'P'),
                          //  new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                          //  new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                          //  new SqlParameter("@passport_no", ""),
                           // new SqlParameter("@family_name", comnobj.search_std_family_name),
                            new SqlParameter("@parent_id", comnobj.search_parent_id),
                            new SqlParameter("@parent_name", comnobj.search_parent_name),
                            new SqlParameter("@parent_mail_id", comnobj.search_parent_email_id),
                            new SqlParameter("@parent_mobile_no", comnobj.search_parent_mobile_no),
                            new SqlParameter("@parent_mother_name", comnobj.search_parent_mother_name),
                            new SqlParameter("@parent_mother_mail_id", comnobj.search_parent_mother_email_id),
                            new SqlParameter("@parent_mother_mobile_no", comnobj.search_parent_mother_mobile_no),
                           // new SqlParameter("@teacher_id", comnobj.search_teacher_id),
                          //  new SqlParameter("@teacher_name", comnobj.search_teacher_name),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_parent_id = dr["sims_parent_number"].ToString();
                            objNew.s_parent_name = dr["Parent_Name"].ToString();
                            objNew.Mail_id = dr["sims_parent_father_email"].ToString();
                            objNew.Mobile_no = dr["sims_parent_father_mobile"].ToString();
                            objNew.s_enroll_no = dr["sims_student_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Name_in_English"].ToString();
                            //objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["sims_parent_number"].ToString();
                            objNew.Teacher_Type = dr["Parent_Name"].ToString();
                            objNew.search_par_image = dr["parentImg"].ToString();

                            objNew.search_parent_Summary_address = dr["SummaryAddress"].ToString();
                            objNew.search_parent_first_name = dr["FirstName"].ToString();
                            objNew.search_parent_last_name = dr["LastName"].ToString();
                            objNew.search_parent_midd_name = dr["MiddleName"].ToString();
                            objNew.s_class = dr["Class"].ToString();

                            objNew.mother_name = dr["mother_name"].ToString();
                            objNew.mother_email_id = dr["sims_parent_mother_email"].ToString();
                            objNew.mother_mobile_no = dr["sims_parent_mother_mobile"].ToString();

                            objNew.guardian_name = dr["guardian_name"].ToString();
                            objNew.guardian_email_id = dr["sims_parent_guardian_email"].ToString();
                            objNew.guardian_mobile_no = dr["sims_parent_guardian_mobile"].ToString();
                            objNew.name = objNew.s_parent_name;
                            objNew.status_name = dr["status_name"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchParentSectionWise")]
        public HttpResponseMessage getSearchParentSectionWise(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchParent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchParent"));

            CommonUserControlClass comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'V'),
                          //  new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                          //  new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", comnobj.search_std_grade_name),
                            new SqlParameter("@section_code", comnobj.search_std_section_name),
                          //  new SqlParameter("@passport_no", ""),
                           // new SqlParameter("@family_name", comnobj.search_std_family_name),
                           // new SqlParameter("@parent_id", comnobj.search_parent_id),
                          //  new SqlParameter("@parent_name", comnobj.search_parent_name),
                           // new SqlParameter("@parent_mail_id", comnobj.search_parent_email_id),
                           // new SqlParameter("@parent_mobile_no", comnobj.search_parent_mobile_no),
                           // new SqlParameter("@teacher_id", comnobj.search_teacher_id),
                          //  new SqlParameter("@teacher_name", comnobj.search_teacher_name),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();

                            objNew.s_enroll_no = dr["sims_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Sname"].ToString();
                            objNew.s_parent_id = dr["sims_parent_number"].ToString();
                            objNew.s_parent_name = dr["NAME"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getGradeSectionCode")]
        public HttpResponseMessage getGradeSectionCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeSectionCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getGradeSectionCode"));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'N'),                                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.grade_code = dr["sims_grade_code"].ToString();
                            objNew.grade_name = dr["sims_grade_name_en"].ToString();
                            objNew.section_code = dr["sims_grade_code"].ToString() + dr["sims_section_code"].ToString();
                            objNew.section_name = dr["sims_section_name_en"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getGradeSectionCode")]
        public HttpResponseMessage getGradeSectionCode(string grade, string section)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGradeSectionCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getGradeSectionCode"));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'V'),
                            new SqlParameter("@section_code", section != ""?section:null),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.s_sname_in_english = dr["Sname"].ToString();
                            objNew.s_enroll_no = dr["sims_enroll_number"].ToString();
                            objNew.user_name = dr["sims_parent_number"].ToString();
                            objNew.name = dr["NAME"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            objNew.code = dr["sims_parent_number"].ToString();
                            objNew.name = objNew.s_sname_in_english;

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchTeacher")]
        public HttpResponseMessage getSearchTeacher(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchTeacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchTeacher"));

            CommonUserControlClass comnobj = new CommonUserControlClass();
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'T'),
                            new SqlParameter("@teacher_id", comnobj.search_teacher_id),
                            new SqlParameter("@teacher_name", comnobj.search_teacher_name),
                            new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code_teacher),
                            new SqlParameter("@sims_appl_form_field", CommonStaticClass.teacher_type),               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.user_name = dr["sims_employee_code"].ToString();
                            objNew.Teacher_Name = dr["sims_teacher_name"].ToString();
                            objNew.Teacher_Type = dr["Teacher_Type"].ToString();
                            objNew.Teacher_id = dr["sims_teacher_code"].ToString();
                            objNew.name = objNew.Teacher_Name;
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchUser")]
        public HttpResponseMessage getSearchUser(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchUser(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchUser"));
            CommonUserControlClass comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'U'),
                            new SqlParameter("@grade_code", comnobj.code),
                            new SqlParameter("@user_login_name", comnobj.search_user_name),
                            new SqlParameter("@user_name", comnobj.name),
                            new SqlParameter("@user_mail_id", comnobj.search_user_email_id),   
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.user_code = dr["comn_user_code"].ToString();
                            objNew.user_name = dr["comn_user_name"].ToString();
                            objNew.name = dr["comn_user_alias"].ToString();
                            objNew.user_group_id = dr["comn_user_group_code"].ToString();
                            objNew.search_user_name = dr["comn_user_group_name"].ToString();
                            if (string.IsNullOrEmpty(dr["comn_user_date_created"].ToString()) == false)
                                objNew.user_date_created = DateTime.Parse(dr["comn_user_date_created"].ToString()).ToShortDateString();
                            if (dr["comn_user_captcha_status"].ToString().Equals("1"))
                                objNew.user_captcha_status = true;
                            else
                                objNew.user_captcha_status = false;
                            if (string.IsNullOrEmpty(dr["comn_user_last_login"].ToString()) == false)
                                objNew.user_last_login = DateTime.Parse(dr["comn_user_last_login"].ToString()).ToShortDateString();
                            else
                                objNew.user_last_login = null;

                            objNew.Mail_id = dr["comn_user_email"].ToString();
                            //   if (dr["comn_user_status"].ToString().Equals("A"))
                            objNew.user_status = dr["comn_user_status"].ToString();
                            //   else
                            //     objNew.user_status = false;
                            if (string.IsNullOrEmpty(dr["comn_user_expiry_date"].ToString()) == false)
                                objNew.user_expiry_date = dr["comn_user_expiry_date"].ToString();

                            //objNew.comn_user_phone_number = dr["comn_user_phone_number"].ToString();
                            //objNew.comn_user_secret_question_code = dr["comn_user_secret_question_code"].ToString();
                            //objNew.comn_user_secret_question = dr["sims_appl_form_field_value1"].ToString();
                            //objNew.comn_user_secret_answer = dr["comn_user_secret_answer"].ToString();
                            //objNew.comn_user_status_name = dr["status_name"].ToString();
                            //objNew.comn_user_status = dr["comn_user_status"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }


        [Route("SearchEmployee")]
        public HttpResponseMessage SearchEmployee(string data)
        {

            Sims189 obj = new Sims189();

            List<Sims189> Emp_List = new List<Sims189>();

            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims189>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'E'),
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),
                            new SqlParameter("@teacher_name",obj.em_department),
                            new SqlParameter("@em_desg_code",obj.em_desg_code),
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();

                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_department = dr["dept_name"].ToString();
                            simsobj.em_desg_code = dr["desg_name"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();

                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            simsobj.sims_student_enroll_number = dr["emp_child"].ToString();

                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }


        [Route("SearchEmployeeForNIS")]
        public HttpResponseMessage SearchEmployeeForNIS(string data)
        {

            Sims189 obj = new Sims189();

            List<Sims189> Emp_List = new List<Sims189>();

            if (data == "undefined" || data == "\"\"" || data == "[]")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims189>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'E'),
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),
                            new SqlParameter("@teacher_name",obj.em_department),
                            new SqlParameter("@em_desg_code",obj.em_desg_code),
                            new SqlParameter("@codp_comp_code",obj.company_code),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();

                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number = dr["em_number"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            simsobj.em_department = dr["dept_name"].ToString();
                            simsobj.em_desg_code = dr["desg_name"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();

                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            simsobj.sims_student_enroll_number = dr["emp_child"].ToString();

                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }

        [Route("getSearchFinanceUser")]
        public HttpResponseMessage getSearchFinanceUser(string user_name, string emp_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchFinanceUser(),PARAMETERS :: USERNAME{2},EMPNAME{3}";
            Log.Debug(string.Format(debug, "HRMS", "getSearchFinanceUser", user_name, emp_name));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'F'),
                            new SqlParameter("@enroll", ""),
                            new SqlParameter("@sname", ""),
                            new SqlParameter("@grade_code", ""),
                            new SqlParameter("@section_code", ""),
                            new SqlParameter("@passport_no", ""),
                            new SqlParameter("@family_name", ""),
                            new SqlParameter("@parent_id", ""),
                            new SqlParameter("@parent_name", ""),
                            new SqlParameter("@parent_mail_id", ""),
                            new SqlParameter("@parent_mobile_no", ""),
                            new SqlParameter("@teacher_id", ""),
                            new SqlParameter("@teacher_name", ""),
                            new SqlParameter("@user_name", user_name),
                            new SqlParameter("@user_mail_id",""),
                            new SqlParameter("@emp_name",emp_name),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.user_name = dr["comn_user_name"].ToString();
                            objNew.search_user_name = dr["EmpName"].ToString();
                            objNew.name = objNew.search_user_name;

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchEmployee")]
        public HttpResponseMessage getSearchEmployee(string user_name, string emp_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchEmployee(),PARAMETERS :: USERNAME{2},EMPNAME{3}";
            Log.Debug(string.Format(debug, "HRMS", "getSearchEmployee", user_name, emp_name));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'E'),
                            new SqlParameter("@user_name", user_name),
                            new SqlParameter("@user_mail_id", ""),
                            new SqlParameter("@emp_name", emp_name),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.user_name = dr["em_login_code"].ToString();
                            objNew.code = dr["em_number"].ToString();
                            objNew.name = dr["EmpName"].ToString();
                            objNew.Mobile_no = dr["em_mobile"].ToString();
                            objNew.Mail_id = dr["em_email"].ToString();
                            objNew.status = dr["em_service_status"].ToString();
                            objNew.status_name = dr["em_service_status_name"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchEmployeefilter")]
        public HttpResponseMessage getSearchEmployeefilter(string user_name, string emp_name, string desg)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchEmployeefilter(),PARAMETERS :: USERNAME{2},EMPNAME{3},DESIGNATION{4}";
            Log.Debug(string.Format(debug, "HRMS", "getSearchEmployeefilter", user_name, emp_name, desg));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'M'),
                            new SqlParameter("@user_name", user_name),
                            new SqlParameter("@user_mail_id", ""),
                            new SqlParameter("@emp_name", emp_name),
                            new SqlParameter("@em_desg_code", desg),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.user_name = dr["em_login_code"].ToString();
                            objNew.code = dr["em_number"].ToString();
                            objNew.name = dr["EmpName"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getAllDesignationName")]
        public HttpResponseMessage getAllDesignationName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllDesignationName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getAllDesignationName"));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'A'),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.designation = dr["dg_code"].ToString();
                            objNew.designation_name = dr["dg_desc"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getAllSectionName")]
        public HttpResponseMessage getAllSectionName(string gradecode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllSectionName(),PARAMETERS :: GRADECODE{2}";
            Log.Debug(string.Format(debug, "HRMS", "getAllSectionName", gradecode));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'B'),
                            new SqlParameter("@grade_code", gradecode),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.code = dr["sims_section_code"].ToString();
                            objNew.name = dr["sims_section_name_en"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getAllGradeName")]
        public HttpResponseMessage getAllGradeName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGradeName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getAllGradeName"));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'G'),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.name = dr["sims_grade_name_en"].ToString();
                            objNew.code = dr["sims_grade_code"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getAllNationality")]
        public HttpResponseMessage getAllNationality()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllNationality(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getAllNationality"));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'H'),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.code = dr["sims_nationality_code"].ToString();
                            objNew.name = dr["sims_nationality_name_en"].ToString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchCouncilUser")]
        public HttpResponseMessage getSearchCouncilUser(string academic, string cname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchCouncilUser(),PARAMETERS :: ACADEMIC{2},NAME{3}";
            Log.Debug(string.Format(debug, "HRMS", "getSearchCouncilUser", academic, cname));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'C'),
                            new SqlParameter("@enroll", ""),
                            new SqlParameter("@sname", ""),
                            new SqlParameter("@grade_code", ""),
                            new SqlParameter("@section_code", ""),
                            new SqlParameter("@passport_no", ""),
                            new SqlParameter("@family_name", ""),
                            new SqlParameter("@parent_id", ""),
                            new SqlParameter("@parent_name", ""),
                            new SqlParameter("@parent_mail_id", ""),
                            new SqlParameter("@parent_mobile_no", ""),
                            new SqlParameter("@teacher_id", ""),
                            new SqlParameter("@teacher_name", ""),
                            new SqlParameter("@user_name", ""),
                            new SqlParameter("@user_mail_id", ""),
                            new SqlParameter("@emp_name", cname),
                            new SqlParameter("@academic", academic),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();
                            objNew.code = dr["sims_user_login_code"].ToString();
                            objNew.search_user_name = dr["student"].ToString();
                            objNew.s_class = dr["class"].ToString();
                            objNew.designation = dr["sims_council_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_house_council_enroll_start_date"].ToString()))
                                objNew.Sdate = DateTime.Parse(dr["sims_house_council_enroll_start_date"].ToString()).ToShortDateString();

                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        //For Search Query

        [Route("getUserGroupCode")]
        public HttpResponseMessage getUserGroupCode(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserGroupCode(),PARAMETERS :: USERNAME{2}";
            Log.Debug(string.Format(debug, "HRMS", "getUserGroupCode", username));

            string grpCode = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'J'),
                            new SqlParameter("@user_name", username),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            grpCode = dr["comn_user_group_code"].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grpCode);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grpCode);
        }

        [Route("getAllUserQueries")]
        public HttpResponseMessage getAllUserQueries(string username, string groupcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllUserQueries(),PARAMETERS :: USERNAME{2},GROUPCODE{3}";
            Log.Debug(string.Format(debug, "HRMS", "getAllUserQueries", username, groupcode));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'K'),
                            new SqlParameter("@user_name", username),
                            new SqlParameter("@group_code", groupcode),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass obj = new CommonUserControlClass();
                            obj.search_code = dr["sims_search_code"].ToString();
                            obj.search_query = dr["sims_search_desc"].ToString();
                            search_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getQueryFields")]
        public HttpResponseMessage getQueryFields(string srchcode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getQueryFields(),PARAMETERS :: SEARCHCODE{2}";
            Log.Debug(string.Format(debug, "HRMS", "getQueryFields", srchcode));

            string fieldStr = string.Empty;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@sims_search_code", srchcode),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            fieldStr = dr[0].ToString();
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, fieldStr);

            }
            return Request.CreateResponse(HttpStatusCode.OK, fieldStr);
        }

        [Route("getSearchOutput")]
        public HttpResponseMessage getSearchOutput(string search_code, string fieldstring)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchOutput(),PARAMETERS :: SEARCHCODE{2}";
            Log.Debug(string.Format(debug, "HRMS", "getSearchOutput", search_code));

            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();
            string[] stringSeparators = new string[] { "," };
            string[] Str = fieldstring.Split(stringSeparators, StringSplitOptions.None);

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                            new SqlParameter("@sims_search_code", search_code),
                                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass obj = new CommonUserControlClass();

                            foreach (string item in Str)
                            {
                                try
                                {
                                    if (item.Contains("Curiculum Short Name") == true)
                                        obj.sims_cur_short_name_en = dr["Curiculum Short Name"].ToString();
                                    if (item.Contains("Curiculum Level Name") == true)
                                        obj.sims_cur_level_name_en = dr["Curiculum Level Name"].ToString();
                                    if (item.Contains("Grade Name") == true)
                                        obj.sims_grade_name_en = dr["Grade Name"].ToString();
                                    if (item.Contains("Section Name") == true)
                                        obj.sims_section_name_en = dr["Section Name"].ToString();
                                    if (item.Contains("Student Enroll Number") == true)
                                        obj.sims_student_enroll_number = dr["Student Enroll Number"].ToString();
                                    if (item.Contains("Student_Passport_Name") == true)
                                        obj.sims_student_passport_name_en = dr["Student_Passport_Name"].ToString();
                                    if (item.Contains("Student Nickname") == true)
                                        obj.sims_student_nickname = dr["Student Nickname"].ToString();
                                    if (item.Contains("Student Gender") == true)
                                    {
                                        obj.sims_student_gender = dr["Student Gender"].ToString();
                                        if (obj.sims_student_gender == "F")
                                            obj.sims_student_gender = "Female";
                                        if (obj.sims_student_gender == "M")
                                            obj.sims_student_gender = "Male";
                                    }
                                    if (item.Contains("Student Religion Name") == true)
                                        obj.sims_religion_name_en = dr["Student Religion Name"].ToString();
                                    if (item.Contains("Student Nationality Name") == true)
                                        obj.sims_nationality_name_en = dr["Student Nationality Name"].ToString();
                                    if (item.Contains("Student Remark") == true)
                                        obj.sims_student_remark = dr["Student Remark"].ToString();
                                    if (item.Contains("Student House Name") == true)
                                        obj.sims_house_name = dr["Student House Name"].ToString();
                                    if (item.Contains("Student Father Name") == true)
                                        obj.sims_parent_father_name = dr["Student Father Name"].ToString();
                                    if (item.Contains("Student Mother Name") == true)
                                        obj.sims_parent_mother_name = dr["Student Mother Name"].ToString();
                                    if (item.Contains("Student Guardian Name") == true)
                                        obj.sims_parent_guardian_name = dr["Student Guardian Name"].ToString();
                                    if (item.Contains("Student Father Family Name") == true)
                                        obj.sims_parent_father_family_name = dr["Student Father Family Name"].ToString();
                                    if (item.Contains("Student Mother Family Name") == true)
                                        obj.sims_parent_mother_family_name = dr["Student Mother Family Name"].ToString();
                                    if (item.Contains("Student Guardian Family Name") == true)
                                        obj.sims_parent_guardian_family_name = dr["Student Guardian Family Name"].ToString();
                                    if (item.Contains("Student Father Mobile Number") == true)
                                        obj.sims_parent_father_mobile = dr["Student Father Mobile Number"].ToString();
                                    if (item.Contains("Mother Mobile Number") == true)
                                        obj.sims_parent_mother_mobile = dr["Student Mother Mobile Number"].ToString();
                                    if (item.Contains("Student Guardian Mobile Number") == true)
                                        obj.sims_parent_guardian_mobile = dr["Student Guardian Mobile Number"].ToString();
                                    if (item.Contains("Student Father Email") == true)
                                        obj.sims_parent_father_email = dr["Student Father Email"].ToString();
                                    if (item.Contains("Student Mother Email") == true)
                                        obj.sims_parent_mother_email = dr["Student Mother Email"].ToString();
                                    if (item.Contains("Student Guardian Email") == true)
                                        obj.sims_parent_guardian_email = dr["Student Guardian Email"].ToString();
                                    if (item.Contains("Student Subject Name") == true)
                                        obj.sims_subject_name_en = dr["Student Subject Name"].ToString();
                                }
                                catch (Exception ee)
                                { }
                            }
                            search_list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }

        [Route("getSearchParentSectionWise")]
        public HttpResponseMessage getSearchParentSectionWise(string grade, string section)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchParent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "HRMS", "getSearchParent"));

            //   CommonUserControlClass comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<CommonUserControlClass>(data);
            List<CommonUserControlClass> search_list = new List<CommonUserControlClass>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'V'),
                          //  new SqlParameter("@enroll", comnobj.search_std_enroll_no),
                          //  new SqlParameter("@sname", comnobj.search_std_name),
                            new SqlParameter("@grade_code", grade),
                            new SqlParameter("@section_code", section),
                          //  new SqlParameter("@passport_no", ""),
                           // new SqlParameter("@family_name", comnobj.search_std_family_name),
                           // new SqlParameter("@parent_id", comnobj.search_parent_id),
                          //  new SqlParameter("@parent_name", comnobj.search_parent_name),
                           // new SqlParameter("@parent_mail_id", comnobj.search_parent_email_id),
                           // new SqlParameter("@parent_mobile_no", comnobj.search_parent_mobile_no),
                           // new SqlParameter("@teacher_id", comnobj.search_teacher_id),
                          //  new SqlParameter("@teacher_name", comnobj.search_teacher_name),                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CommonUserControlClass objNew = new CommonUserControlClass();

                            objNew.s_enroll_no = dr["sims_enroll_number"].ToString();
                            objNew.s_sname_in_english = dr["Sname"].ToString();
                            objNew.s_parent_id = dr["sims_parent_number"].ToString();
                            objNew.s_parent_name = dr["NAME"].ToString();
                            objNew.Mail_id = dr["Email"].ToString();
                            objNew.Mobile_no = dr["Mobile"].ToString();
                            search_list.Add(objNew);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, search_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, search_list);
        }


        public class Com052
        {

            public string sims_grade_code { get; set; }

            public string section_code { get; set; }

            public string section_name { get; set; }

            public string sims_grade_name { get; set; }

            public string grade_section_name { get; set; }

            public string grade_section_code { get; set; }

            public string designation { get; set; }

            public string designation_name { get; set; }

            public string em_dept_code { get; set; }

            public string em_dept_name { get; set; }

            public bool sims_father { get; set; }

            public bool sims_mother { get; set; }

            public bool sims_guardian { get; set; }

            public bool sims_active { get; set; }
        }


        [Route("Get_Section_CodebyAll")]
        public HttpResponseMessage Get_Section_CodebyAll(string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : Get_Section_CodebyCuriculum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, " Get_Grade_Section_Code", " Get_Section_CodebyCuriculum"));

            List<Com052> mod_list = new List<Com052>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_mem_code", "X"),
                         //   new SqlParameter("@cur_code", cur_code),
                             new SqlParameter("@IGNORED_SECTION_LIST", grade_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com052 simsobj = new Com052();
                            simsobj.section_code = dr["sims_section_code"].ToString();
                            simsobj.section_name = dr["sims_section_name_en"].ToString();
                            simsobj.grade_section_name = dr["section"].ToString();
                            simsobj.grade_section_code = dr["grade_sec"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        // Get Company
        [Route("GetCompanyNameForShift")]
        public HttpResponseMessage GetCompanyNameForShift()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() {
                            new SqlParameter("tbl_name", "[pays].[pays_company]"),
                            new SqlParameter("tbl_col_name1", "[co_company_code],[co_desc]")
                            
                        }
                    );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();
                            hrmsobj.sh_company_code = dr["co_company_code"].ToString();
                            hrmsobj.sh_company_name = dr["co_desc"].ToString();
                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);

            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        //Get Department from company

        [Route("GetDepartmentName")]
        public HttpResponseMessage GetDepartmentName(string company_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getTimeMarkers(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "ERP", "getCategorize"));

            List<Pers099_new> events = new List<Pers099_new>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[Search_proc]",
                        new List<SqlParameter>() {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@opr_mem_code", "L"),
                            new SqlParameter("@codp_comp_code", company_code)
                        }
                    );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Pers099_new hrmsobj = new Pers099_new();
                            hrmsobj.code = dr["codp_dept_no"].ToString();
                            hrmsobj.em_dept_name = dr["codp_dept_name"].ToString();
                            events.Add(hrmsobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, events);
            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

    }
}