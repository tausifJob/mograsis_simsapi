﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIMSAPI.Models.ERP.messageClass;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using log4net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.common
{
    [RoutePrefix("api/Exceptions")]
    public class ExceptionsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //select
        [Route("getAllExceptionDetails")]
        public HttpResponseMessage getAllExceptionDetails()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllExceptionDetails(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllExceptionDetails"));

            List<Comn012> Exception_Details = new List<Comn012>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_exception_details_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",'S'),
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn012 comnobj = new Comn012();
                            comnobj.ed_exception_id = Int64.Parse(dr["ed_exception_id"].ToString());
                            comnobj.ed_exception_type = dr["ed_exception_type"].ToString();
                            comnobj.ed_appl_name = dr["ed_appl_name"].ToString();
                            comnobj.ed_db_tbl_name = dr["ed_db_tbl_name"].ToString();
                            comnobj.ed_exception_msg = dr["ed_exception_msg"].ToString();
                            comnobj.ed_inner_exception_msg = dr["ed_inner_exception_msg"].ToString();
                            comnobj.ed_stack_strace_msg = dr["ed_stack_strace_msg"].ToString();
                            comnobj.ed_keycode = dr["ed_keycode"].ToString();
                            comnobj.ed_reg_date = dr["ed_reg_date"].ToString();
                            Exception_Details.Add(comnobj);
                        }
                    }
                }

            }
            catch (Exception x)
            {

                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Exception_Details);
            }

            return Request.CreateResponse(HttpStatusCode.OK, Exception_Details);
        }

    }
}