﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/common/UserLangController")]

    public class UserLangController : ApiController
    {
       
        [Route("getAllAcademicYear")]
        public HttpResponseMessage getAllAcademicYear(string cur)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@sims_cur_code",cur),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        [Route("getAllAcademicYear1")]
        public HttpResponseMessage getAllAcademicYear1(string cur)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                             new SqlParameter("@sims_cur_code",cur),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }




        [Route("getuserLang_show")]
        public HttpResponseMessage getuserLang_show()
        {
            List<Comn007> lstseq = new List<Comn007>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn007 comseq = new Comn007();
                            comseq.id = dr["id"].ToString();
                            comseq.comn_application_code = dr["sims_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application_Name"].ToString();

                            comseq.comn_mod_code = dr["sims_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode_Name"].ToString();

                            comseq.sims_english = dr["sims_english"].ToString();
                            comseq.sims_arabic_en = dr["sims_arabic_en"].ToString();
                            comseq.sims_english_en = dr["sims_english_en"].ToString();

                            lstseq.Add(comseq);

                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("getuserLangcode_show")]
        public HttpResponseMessage getuserLangcode_show(string comn_appl_code)
        {
            List<Comn007> lstseq = new List<Comn007>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@comn_appl_code", comn_appl_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn007 comseq = new Comn007();
                            comseq.id = dr["id"].ToString();
                            comseq.comn_application_code = dr["sims_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application_Name"].ToString();

                            comseq.comn_mod_code = dr["sims_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode_Name"].ToString();

                            comseq.sims_english = dr["sims_english"].ToString();
                            comseq.sims_arabic_en = dr["sims_arabic_en"].ToString();
                            comseq.sims_english_en = dr["sims_english_en"].ToString();

                            lstseq.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("userLang_insert")]
        public HttpResponseMessage userLang_insert(List<Comn007> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn007 comc in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),
                            
                        
                         });

                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("userLang_Update")]
        public HttpResponseMessage userLang_Update(List<Comn007> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn007 comc in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("sims.sims_arabic_english_proc",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                             
                            
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("userLang_insert1")]
        public HttpResponseMessage userLang_insert1(List<Comn007> data)
        {
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Comn007 comc in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_arabic_english_proc",
                        new List<SqlParameter>()
                     {

                               new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            //new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),
                              });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

                /* message.strMessage = x.Message;
                 message.systemMessage = string.Empty;
                 message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("userLangTable_Update")]
        public HttpResponseMessage userLangTable_Update(List<Comn007> data)
        {
            Message message = new Message();

            string st = string.Empty;
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    int a = 0;
                    db.Open();
                    foreach (Comn007 comc in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_arabic_english_proc",
                            // SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sen_proc]",
                        new List<SqlParameter>()
                     {
                                new SqlParameter("@opr", 'G'),      
                                new SqlParameter("@comn_appl_code", comc.comn_application_code),
                           new SqlParameter("@id", comc.id),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),

                         });


                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getApplication")]
        public HttpResponseMessage getApplication()
        {
            List<Comn004> lstCuriculum = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_arabic_english_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'Z'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 sequence = new Comn004();
                            sequence.comn_application_code = dr["comn_appl_code"].ToString();
                            sequence.comn_application_name = dr["comn_appl_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getApplication")]
        public HttpResponseMessage getApplication(string modCode)
        {
            if (modCode == "undefined" || modCode == "\"\"")
            {
                modCode = null;
            }
            List<Comn004> lstCuriculum = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_arabic_english_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'C'),
                            new SqlParameter("@com_mode_name",modCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 sequence = new Comn004();
                            sequence.comn_application_code = dr["comn_appl_code"].ToString();
                            sequence.comn_application_name = dr["comn_appl_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

    }
}