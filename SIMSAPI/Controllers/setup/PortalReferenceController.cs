﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/portalrefer")]
    public class PortalReferenceController : ApiController
    {
        static string root = "http://localhost/SIMSAPI/Content/databaseConnectionString/UploadedFiles/";
        [Route("getPortalDetail")]
        public HttpResponseMessage getPortalDetail()
        {
            List<Sims092> portal = new List<Sims092>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_portal_reference_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims092 obj = new Sims092();


                            obj.sims_cur_name = dr["cur_name"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();

                            obj.sims_reference_number = dr["sims_reference_number"].ToString();
                            obj.sims_reference_name = dr["sims_reference_name"].ToString();
                            obj.sims_reference_publish_date =db.UIDDMMYYYYformat(dr["sims_reference_publish_date"].ToString());
                            obj.sims_reference_expiry_date = db.UIDDMMYYYYformat(dr["sims_reference_expiry_date"].ToString());
                           obj.sims_reference_desc = dr["sims_reference_desc"].ToString();
                           // if (!string.IsNullOrEmpty(dr["sims_reference_publish_date"].ToString()))
                           //     obj.sims_reference_publish_date = Convert.ToDateTime(dr["sims_reference_publish_date"].ToString()).ToShortDateString();
                           // if (!string.IsNullOrEmpty(dr["sims_reference_expiry_date"].ToString()))
                           //     obj.sims_reference_expiry_date = Convert.ToDateTime(dr["sims_reference_expiry_date"].ToString()).ToShortDateString();
                            obj.sims_reference_filename = dr["sims_reference_filename"].ToString();

                            obj.sims_reference_status = dr["sims_reference_status"].Equals("A") ? true : false;
                            obj.sims_learn_arabic_status = dr["sims_learn_arabic_status"].Equals("T") ? true : false;
                            obj.sims_reference_video_resource = dr["sims_reference_video_resource"].Equals("Y") ? true : false;


                            //if (dr["sims_reference_status"].ToString() == "A")
                            //    obj.sims_reference_status = true;
                            //else
                            //    obj.sims_reference_status = false;

                            //if (dr["sims_learn_arabic_status"].ToString() == "T")
                            //    obj.sims_learn_arabic_status = true;
                            //else
                            //    obj.sims_learn_arabic_status = false;


                            portal.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, portal);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, portal);
        }

        [Route("PortalCUD")]
        public HttpResponseMessage PortalCUD(List<Sims092> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims092 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_portal_reference_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                 new SqlParameter("@sims_reference_number", simsobj.sims_reference_number),
                                 new SqlParameter("@sims_reference_name", simsobj.sims_reference_name),
                                 new SqlParameter("@sims_reference_desc", simsobj.sims_reference_desc),
                                 new SqlParameter("@sims_reference_publish_date",db.DBYYYYMMDDformat(simsobj.sims_reference_publish_date)),
                                 new SqlParameter("@sims_reference_expiry_date",db.DBYYYYMMDDformat(simsobj.sims_reference_expiry_date)),
                                 new SqlParameter("@sims_reference_filename", simsobj.sims_timetable_filename),
                                 new SqlParameter("@sims_reference_status", simsobj.sims_reference_status==true?"A":"I"),
                                 new SqlParameter("@sims_learn_arabic_status", simsobj.sims_learn_arabic_status==true?"T":"F"),
                                 new SqlParameter("@sims_reference_video_resource", simsobj.sims_reference_video_resource==true?"Y":"N")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getLearnArabic")]
        public HttpResponseMessage getLearnArabic()
        {
            List<Sims092> portal = new List<Sims092>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_portal_reference_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "R")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims092 obj = new Sims092();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_reference_number = dr["sims_reference_number"].ToString();
                            obj.sims_reference_name = dr["sims_reference_name"].ToString();
                            obj.sims_reference_publish_date =db.UIDDMMYYYYformat(dr["sims_reference_publish_date"].ToString());
                            obj.sims_reference_expiry_date = db.UIDDMMYYYYformat(dr["sims_reference_expiry_date"].ToString());
                            obj.sims_reference_filename = dr["sims_reference_filename"].ToString();
                            obj.sims_reference_desc = dr["sims_reference_desc"].ToString();
                            portal.Add(obj);



                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, portal);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, portal);
        }

    }
}




