﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.setupClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Album")]
    [BasicAuthentication]
    public class AlbumController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ACADEMICYEAR"));

            List<Sims078> aca_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "sims.sims_academic_year"),
                                new SqlParameter("@tbl_col_name1", "sims_academic_year,Format(sims_academic_year_start_date,'yyyy-MMM-dd') as sims_academic_year_start_date,Format(sims_academic_year_end_date,'yyyy-MMM-dd') as sims_academic_year_end_date"),
                                new SqlParameter("@tbl_cond", "sims_academic_year_status='C'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_start_date = dr["sims_academic_year_start_date"].ToString();
                            simsobj.sims_academic_end_date = dr["sims_academic_year_end_date"].ToString();
                            aca_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, aca_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, aca_list);
            }
        }

        [Route("getClassList")]
        public HttpResponseMessage getClassList(string sims_cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getClassList(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "CLASSLIST"));

            //  Sims078 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims078>(data);
            Sims078 simsobj = new Sims078();
            List<GRD> grdlst = new List<GRD>();

            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_album_grade_section_right_proc",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_album_cur_code", null),
                                new SqlParameter("@sims_academic_year", null)
                         });
                        if (dr.HasRows)
                        {
                            simsobj.ClassList = new List<GRD>();
                            while (dr.Read())
                            {
                                GRD g = new GRD();
                                g.Sections = new List<SEC>();
                                simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                                simsobj.academic_year = dr["sims_academic_year"].ToString();
                                g.sims_grade_code = dr["sims_grade_code"].ToString();
                                g.sims_grade_name = dr["sims_grade_name_en"].ToString();
                                grdlst.Add(g);
                            }
                        }
                        dr.NextResult();
                        if (dr.HasRows)
                        {
                            SEC s = new SEC();
                            s.sims_gradeCode_s = dr["sims_grade_code"].ToString();
                            s.sims_section_code = dr["sims_section_code"].ToString();
                            s.sims_section_name = dr["sims_section_name_en"].ToString();
                            try
                            {
                                GRD g = grdlst.Single(q => q.sims_grade_code == s.sims_gradeCode_s);
                                g.Sections.Add(s);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        simsobj.ClassList = grdlst;
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);

            }
            return Request.CreateResponse(HttpStatusCode.OK, simsobj);
        }

        [Route("getClassListHtml")]
        public HttpResponseMessage getClassListHtml(string sims_cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ACADEMICYEAR"));

            List<Sims078> aca_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_album_grade_section_right_proc",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "B"),
                                new SqlParameter("@sims_album_cur_code", sims_cur_code),
                                new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            //simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.ClassName = dr["name"].ToString();
                            simsobj.ClassCode = dr["code"].ToString();
                            aca_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, aca_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, aca_list);
            }
        }


        [Route("getPhotoInAlbum")]
        public HttpResponseMessage getPhotoInAlbum(string album_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getPhotoInAlbum(),PARAMETERS :: ALBUMNAME{2}";
            Log.Debug(string.Format(debug, "STUDENT", "PHOTOINALBUM", album_name));

            List<Sims078> photo_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_photo_proc]",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "S"),
                              new SqlParameter("@sims_album_name", album_name)                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            simsobj.sims_album_code = dr["sims_album_code"].ToString();
                            simsobj.sims_album_photo_serial_number = dr["sims_album_photo_serial_number"].ToString();
                            simsobj.sims_album_photo_path = dr["sims_album_photo_path"].ToString();
                            simsobj.sims_album_photo_description = dr["sims_album_photo_description"].ToString();
                            simsobj.sims_album_photo_sell_flag = dr["sims_album_photo_sell_flag"].ToString().Equals("Y");
                            if (!string.IsNullOrEmpty(dr["sims_album_photo_price"].ToString()))
                                simsobj.sims_album_photo_price = Convert.ToDecimal(dr["sims_album_photo_price"].ToString());
                            simsobj.sims_album_photo_status = dr["sims_album_photo_status"].ToString();
                            simsobj.sims_album_photo_id = dr["sims_album_photo_id"].ToString();
                            simsobj.tname = "";
                            try {
                                simsobj.sims_album_photo_url = dr["sims_album_photo_url"].ToString();
                            
                            }
                            catch (Exception ex) { }
                            photo_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, photo_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, photo_list);
        }


        [Route("DeletePhotoInAlbum")]
        public HttpResponseMessage getDeletePhotoInAlbum(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : DeletePhotoInAlbum(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "DELETEPHOTOINALBUM"));

            Sims078 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims078>(data);
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_photo_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "D"),
                                new SqlParameter("@sims_album_photo_id", simsobj.sims_album_photo_id),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_code)
                         });
                        if (dr.RecordsAffected > 0)
                        {

                            message.strMessage = "PhotoDetails Information Deleted Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Deleting Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getAllGroupNames")]
        public HttpResponseMessage getAllGroupNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGroupNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "GROUPNAMES"));

            List<UserGroup> grp_list = new List<UserGroup>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_album_user_group_right",
                        new List<SqlParameter>() 
                         { 
                              new SqlParameter("@opr", "G"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            UserGroup ugobj = new UserGroup();
                            ugobj.user_group_code = dr["comn_user_group_code"].ToString();
                            ugobj.user_group_name = dr["comn_user_group_name"].ToString();
                            grp_list.Add(ugobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grp_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grp_list);
        }

        [Route("getSimAlbumData")]
        public HttpResponseMessage getSimAlbumData(Sims078Html simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSimAlbumData(),PARAMETERS :: OPR{2}";
            Log.Debug(string.Format(debug, "STUDENT", "SIMSALBUMDATA"));

            //Sims078Html simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims078Html>(data);
            Message message = new Message();

            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_code", simsobj.sims_album_code),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_name),
                                new SqlParameter("@sims_album_description", simsobj.sims_album_description),
                                new SqlParameter("@sims_album_publish_date", simsobj.sims_album_publish_date),
                                new SqlParameter("@sims_UserList", simsobj.UserGroups),
                                new SqlParameter("@sims_ClassList", simsobj.Classes),
                                new SqlParameter("@sims_album_expiry_date", simsobj.sims_album_expiry_date),
                                new SqlParameter("@sims_album_visible_to_portal", simsobj.sims_album_visible_to_portal ? "Y" : "N"),
                                new SqlParameter("@sims_album_status", simsobj.sims_album_status ? "A" : "I"),
                                new SqlParameter("@sims_appl_code", "Sim078"),
                                new SqlParameter("@comn_sequence_code","ALC")  
                    });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "AlbumDetails Information Updated Sucessfully!!";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Updating Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getAlbumInfo")]
        public HttpResponseMessage getAlbumInfo(string album_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS :: ALBUMCODE {2}";
            Log.Debug(string.Format(debug, "STUDENT", "ALBUMINFO", album_code));

            List<Sims078> album_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                              new SqlParameter("@sims_album_code", album_code)                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            simsobj.sims_album_name = dr["sims_album_name"].ToString();
                            simsobj.sims_album_photo_path = dr["sims_album_photo_path"].ToString();


                            simsobj.sims_album_code = dr["sims_album_code"].ToString();
                            simsobj.grade = dr["sims_grade_name_en"].ToString() + ":" + dr["sims_section_name_en"].ToString();
                            simsobj.sims_album_description = dr["sims_album_description"].ToString();
                            simsobj.sims_album_expiry_date = db.UIDDMMYYYYformat( dr["sims_album_expiry_date"].ToString());
                            simsobj.sims_album_publish_date =db.UIDDMMYYYYformat( dr["sims_album_publish_date"].ToString());
                            simsobj.sims_album_status = dr["sims_album_status"].ToString() == "A";
                            simsobj.sims_album_visible_to_portal = dr["sims_album_visible_to_portal"].ToString() == "Y";
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.UserGroups = dr["UserGroup"].ToString();
                            simsobj.sims_tile_color = dr["Color"].ToString();
                            simsobj.Classes = dr["Classes"].ToString();
                            simsobj.AlbumCount = dr["AlbumCount"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            album_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, album_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, album_list);
        }




        [Route("InsertSimAlbumDataHTML")]
        public HttpResponseMessage InsertSimAlbumDataHTML(Sims078Html simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSimAlbumDataHTML()";
            Log.Debug(string.Format(debug, "PP", "getSimAlbumDataHTML"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_album_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_code", simsobj.sims_album_code),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_name),
                                new SqlParameter("@sims_album_description", simsobj.sims_album_description),
                                new SqlParameter("@sims_album_publish_date", db.DBYYYYMMDDformat(simsobj.sims_album_publish_date)),
                                new SqlParameter("@sims_UserList", simsobj.UserGroups),
                                new SqlParameter("@sims_ClassList", simsobj.Classes),
                                new SqlParameter("@sims_album_expiry_date", db.DBYYYYMMDDformat(simsobj.sims_album_expiry_date)),
                                new SqlParameter("@sims_album_visible_to_portal", simsobj.sims_album_visible_to_portal ? "Y" : "N"),
                                new SqlParameter("@sims_album_status", simsobj.sims_album_status ? "A" : "I"),
                                new SqlParameter("@sims_appl_code", "Sim078"),
                                new SqlParameter("@comn_sequence_code","ALC")  ,
                                new SqlParameter("@sims_academic_year",simsobj.sims_academic_year)  
                                

                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("CUDSimAlbumDataHTML")]
        public HttpResponseMessage CUDSimAlbumDataHTMLNEw(Sims078Html simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSimAlbumDataHTML()";
            Log.Debug(string.Format(debug, "PP", "getSimAlbumDataHTML"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_album_proc]",
                        new List<SqlParameter>() 
                         { 


                             new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_code", simsobj.sims_album_code),
                                new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_name),
                                new SqlParameter("@sims_album_description", simsobj.sims_album_description),
                                new SqlParameter("@sims_album_publish_date", db.DBYYYYMMDDformat(simsobj.sims_album_publish_date)),
                                new SqlParameter("@sims_UserList", simsobj.UserGroups),
                                new SqlParameter("@sims_ClassList", simsobj.Classes),
                                new SqlParameter("@sims_album_expiry_date", db.DBYYYYMMDDformat( simsobj.sims_album_expiry_date)),
                                new SqlParameter("@sims_album_visible_to_portal", simsobj.sims_album_visible_to_portal ? "Y" : "N"),
                                new SqlParameter("@sims_album_status", simsobj.sims_album_status ? "A" : "I"),
                                new SqlParameter("@sims_appl_code", "Sim078"),
                                new SqlParameter("@comn_sequence_code","ALC")  ,
                                new SqlParameter("@sims_academic_year",simsobj.sims_academic_year)  
                                

                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }




        [Route("getAlbumListHtml")]
        public HttpResponseMessage getAlbumListHtml()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ACADEMICYEAR"));

            List<Sims078> aca_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_album_grade_section_right_proc",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "A"),
                              
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            //simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_album_code = dr["sims_album_code"].ToString();
                            simsobj.sims_album_name = dr["sims_album_name"].ToString();
                            aca_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, aca_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, aca_list);
            }
        }





        [Route("InsertPhotoInAlbumData")]
        public HttpResponseMessage InsertPhotoInAlbumData(Sims078Ht simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertPhotoInAlbumData()";
            Log.Debug(string.Format(debug, "PP", "InsertPhotoInAlbumData"));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_album_photo_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_code),
                                new SqlParameter("@sims_album_photo_serial_number", simsobj.sims_album_photo_serial_number),
                                new SqlParameter("@sims_album_photo_id", simsobj.sims_album_photo_id),
                                new SqlParameter("@sims_album_photo_path", simsobj.sims_album_photo_path),
                                new SqlParameter("@sims_album_photo_description", simsobj.sims_album_photo_description),
                                new SqlParameter("@sims_album_photo_sell_flag", simsobj.sims_album_photo_sell_flag ? "Y" : "N"),
                                new SqlParameter("@sims_album_photo_price", simsobj.sims_album_photo_price),
                                new SqlParameter("@sims_album_photo_status", simsobj.sims_album_photo_status),
                               
                                new SqlParameter("@sims_album_photo_url", simsobj.sims_album_photo_url)

                             
                         });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



        [Route("InsertPhotoInAlbumDataList")]
        public HttpResponseMessage InsertPhotoInAlbumDataList(Sims078Ht simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertPhotoInAlbumData()";
            Log.Debug(string.Format(debug, "PP", "InsertPhotoInAlbumData"));

            bool inserted = false;
            foreach (string imgpath in simsobj.sims_album_photo_path_lst)
            {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_album_photo_proc]",
                            new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_code),
                                new SqlParameter("@sims_album_photo_serial_number", simsobj.sims_album_photo_serial_number),
                                new SqlParameter("@sims_album_photo_id", simsobj.sims_album_photo_id),
                                new SqlParameter("@sims_album_photo_path", imgpath),
                                new SqlParameter("@sims_album_photo_description", simsobj.sims_album_photo_description),
                                new SqlParameter("@sims_album_photo_sell_flag", simsobj.sims_album_photo_sell_flag ? "Y" : "N"),
                                new SqlParameter("@sims_album_photo_price", simsobj.sims_album_photo_price),
                                new SqlParameter("@sims_album_photo_status", simsobj.sims_album_photo_status)
                               
                             
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
                catch (Exception x)
                {
                    //Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("InsertPhotoInAlbumDataListNew")]
        public HttpResponseMessage InsertPhotoInAlbumDataListNew(Sims078Ht simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertPhotoInAlbumData()";
            Log.Debug(string.Format(debug, "PP", "InsertPhotoInAlbumData"));

            bool inserted = false;
           // foreach (string imgpath in simsobj.sims_album_photo_path_lst)
           // {
                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_album_photo_proc]",
                            new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_album_name", simsobj.sims_album_code),
                                new SqlParameter("@sims_album_photo_serial_number", simsobj.sims_album_photo_serial_number),
                                new SqlParameter("@sims_album_photo_id", simsobj.sims_album_photo_id),
                                new SqlParameter("@sims_album_photo_url",  simsobj.sims_album_photo_url),
                                new SqlParameter("@sims_album_photo_description", simsobj.sims_album_photo_description),
                                new SqlParameter("@sims_album_photo_sell_flag", simsobj.sims_album_photo_sell_flag ? "Y" : "N"),
                                new SqlParameter("@sims_album_photo_price", simsobj.sims_album_photo_price),
                                new SqlParameter("@sims_album_photo_status", simsobj.sims_album_photo_status),
                                new SqlParameter("@sims_album_photo_path",  simsobj.sims_album_photo_path)
                                
                         });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
                catch (Exception x)
                {
                    //Log.Error(x);
                    return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                }
           // }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        //Academic Year
        [Route("getAcademicYearAll")]
        public HttpResponseMessage getAcademicYearAll()
        {
            List<Sims078> lstModules = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "P"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 sequence = new Sims078();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }


        [Route("getAlbumInfoAyear")]
        public HttpResponseMessage getAlbumInfoAyear(string academic_year, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAlbumInfo(),PARAMETERS :: ALBUMCODE {2}";
            Log.Debug(string.Format(debug, "STUDENT", "ALBUMINFO", academic_year));

            List<Sims078> album_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_album_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "Q"),
                             new SqlParameter("@sims_academic_year", academic_year),
                             new SqlParameter("@sims_cur_code", cur_code)


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            simsobj.sims_album_name = dr["sims_album_name"].ToString();
                            simsobj.sims_album_photo_path = dr["sims_album_photo_path"].ToString();


                            simsobj.sims_album_code = dr["sims_album_code"].ToString();
                            simsobj.grade = dr["sims_grade_name_en"].ToString() + ":" + dr["sims_section_name_en"].ToString();
                            simsobj.sims_album_description = dr["sims_album_description"].ToString();
                            simsobj.sims_album_expiry_date =db.UIDDMMYYYYformat( dr["sims_album_expiry_date"].ToString());
                            simsobj.sims_album_publish_date =db.UIDDMMYYYYformat( dr["sims_album_publish_date"].ToString());
                            simsobj.sims_album_status = dr["sims_album_status"].ToString() == "A";
                            simsobj.sims_album_visible_to_portal = dr["sims_album_visible_to_portal"].ToString() == "Y";
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.UserGroups = dr["UserGroup"].ToString();
                            simsobj.sims_tile_color = dr["Color"].ToString();
                            simsobj.Classes = dr["Classes"].ToString();
                            simsobj.AlbumCount = dr["AlbumCount"].ToString();
                            album_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, album_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, album_list);
        }



    }
}