﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.Common;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;



namespace SIMSAPI.Controllers.ERP.modules
{
    [RoutePrefix("api/Section/Master")]
    public class SectionMasterController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getGender")]
        public HttpResponseMessage getGender()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : geGender()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            List<Sims035> lstgender = new List<Sims035>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "R"),
                         }
                         );

                    while (dr.Read())
                    {
                        Sims035 simsobj = new Sims035();
                        simsobj.sims_section_gender = dr["sims_appl_form_field_value1"].ToString();
                        simsobj.sims_section_gender_code = dr["sims_appl_parameter"].ToString();
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }

        [Route("getPromoteCode")]
        public HttpResponseMessage getPromoteCode(string cur_code, string academic_year)
        {
            List<Sims035> lstgender = new List<Sims035>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("GetPromoteCode",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@grade_code","Null"),
                            new SqlParameter("@grade_name","Null"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@academic_year",academic_year)
                         }
                         );
                    while (dr.Read())
                    {
                        string str = null;
                        Sims035 simsobj = new Sims035();
                        simsobj.sims_section_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_promote_code = dr["grade"].ToString() + "" + dr["sims_section_code"].ToString();
                        simsobj.sims_section_boys_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_boys_promote_code = dr["grade"].ToString() + "" + dr["sims_section_code"].ToString();
                        simsobj.sims_section_girls_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_girls_promote_code = dr["grade"].ToString() + "" + dr["sims_section_code"].ToString();
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }


        [Route("getPromoteCodeotherschool")]
        public HttpResponseMessage getPromoteCodeotherschool(string cur_code, string academic_year)
        {

            List<Sims035> lstgender = new List<Sims035>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("GetPromoteCode",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@grade_code","Null"),
                            new SqlParameter("@grade_name","Null"),
                            new SqlParameter("@cur_code",cur_code),
                            new SqlParameter("@academic_year",academic_year)
                         }
                         );
                    while (dr.Read())
                    {
                        string str = null;
                        Sims035 simsobj = new Sims035();
                        simsobj.sims_section_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_promote_code = dr["sims_grade_code"].ToString() + "" + dr["sims_section_code"].ToString();
                        simsobj.sims_section_boys_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_boys_promote_code = dr["grade"].ToString() + "" + dr["sims_section_code"].ToString();
                        simsobj.sims_section_girls_promote_desc = dr["sims_grade_name_en"].ToString() + " " + dr["sims_section_name_en"].ToString();
                        simsobj.sims_section_girls_promote_code = dr["grade"].ToString() + "" + dr["sims_section_code"].ToString();
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }


        [Route("getSections")]
        public HttpResponseMessage getSections(string curCode, string academicYear, string grade_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getGrade()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            List<Sims035> lst = new List<Sims035>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code",curCode),
                            new SqlParameter("@sims_academic_year", academicYear),
                            new SqlParameter("@sims_grade_code",grade_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims035 obj = new Sims035();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj._sims_section_name = dr["sims_section_name_en"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.sims_section_stregth = dr["sims_section_stregth"].ToString();
                            obj.sims_section_gender = dr["sims_section_gender"].ToString();
                            obj.sims_section_name_ar = dr["sims_section_name_ar"].ToString();
                            obj.sims_section_name_fr = dr["sims_section_name_fr"].ToString();
                            obj.sims_section_name_ot = dr["sims_section_name_ot"].ToString();
                            obj.sims_section_gender_desc = dr["sims_section_gender_desc"].ToString();
                            obj.sims_section_promote_desc = dr["sims_section_promote_desc"].ToString();
                            obj.sims_section_promote_code = dr["sims_section_promote_code"].ToString();
                            obj.sims_section_boys_promote_desc = dr["sims_section_boys_promote_desc"].ToString();
                            obj.sims_section_boys_promote_code = dr["sims_section_boys_promote_code"].ToString();
                            obj.sims_section_girls_promote_desc = dr["sims_section_girls_promote_desc"].ToString();
                            obj.sims_section_girls_promote_code = dr["sims_section_girls_promote_code"].ToString();

                            obj.count = Convert.ToInt16(dr["count1"].ToString() + Convert.ToInt16(dr["Cnt"].ToString()));
                            obj.grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name = dr["sims_grade_name"].ToString();
                            obj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            obj.sims_section_order_no = dr["sims_section_order_no"].ToString();
                            obj.school_name  = dr["school_name"].ToString();
                            obj.lic_school_code = dr["lic_school_code"].ToString();

                            lst.Add(obj);

                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("CUDSection")]
        public HttpResponseMessage CUDSection(List<Sims035> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDSection()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            bool insert = false;

            // Sims035 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims035>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims035 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_grade_code", simsobj.grade_code),
                            new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                            new SqlParameter("@sims_section_name_en", simsobj.sims_section_name_en),
                            new SqlParameter("@sims_section_name_ar", simsobj.sims_section_name_ar),
                            new SqlParameter("@sims_section_name_fr", simsobj.sims_section_name_fr),
                            new SqlParameter("@sims_section_name_ot", simsobj.sims_section_name_ot),
                            new SqlParameter("@sims_section_strength", simsobj.sims_section_stregth),
                            new SqlParameter("@sims_section_gender", simsobj.sims_section_gender),
                            new SqlParameter("@sims_section_promote_code", simsobj.sims_section_promote_code),
                            new SqlParameter("@sims_section_boys_promote_code", simsobj.sims_section_boys_promote_code),
                            new SqlParameter("@sims_section_girls_promote_code", simsobj.sims_section_girls_promote_code),
                            new SqlParameter("@sims_section_order_no", simsobj.sims_section_order_no),
                            new SqlParameter("@sims_status", simsobj.sims_status==true?"A":"I"),
                            new SqlParameter("@sims_school_code", simsobj.lic_school_code),

                         });
                            if (dr.RecordsAffected > 0)
                            {
                                insert = true;
                            }
                            dr.Close();
                        }
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information!!";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
               /* Log.Error(x);
                message.strMessage = "Error In Deleting Section Information!!";*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getschooltype")]
        public HttpResponseMessage getschooltype()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getschooltype()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            List<Sims035> lstgender = new List<Sims035>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_section_proc]",
                        new List<SqlParameter>() 
                         { 
                               new SqlParameter("@opr", "B"),
                         }
                         );

                    while (dr.Read())
                    {
                        

                        Sims035 simsobj = new Sims035();
                        simsobj.lic_school_code = dr["lic_school_code"].ToString();
                        simsobj.school_name = dr["school_name"].ToString();
                        lstgender.Add(simsobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstgender);
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstgender);
        }

    }
}