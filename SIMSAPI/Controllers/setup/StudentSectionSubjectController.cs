﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.gradebookClass;
using System.Text.RegularExpressions;


namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/StudentSectionSubject")]
    public class StudentSectionSubjectController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("insert_student_section_subject")]
        public HttpResponseMessage insert_student_section_subject(List<Sims089> ob)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insert_student_section_subject(),PARAMETERS :: obj {2},Opr {2}";
            Log.Debug(string.Format(debug, "PP", "insert_student_section_subject", ob));

            Message message = new Message();
            string opr = "";

            List<SqlParameter> lst = new List<SqlParameter>();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    foreach (Sims089 o in ob)
                    {
                        lst.Clear();
                        lst.Add(new SqlParameter("@opr", "P"));
                        lst.Add(new SqlParameter("@sims_cur_code", o.sims_cur_code));
                        lst.Add(new SqlParameter("@sims_academic_year", o.sims_academic_year));
                        lst.Add(new SqlParameter("@sims_grade_code", o.sims_grade_code));
                        lst.Add(new SqlParameter("@sims_section_code", o.sims_section_code));
                        lst.Add(new SqlParameter("@sims_enroll_number", o.sims_student_enroll_number));
                        lst.Add(new SqlParameter("@sims_sub_code", o.sims_subject_name_en));
                        lst.Add(new SqlParameter("@sims_sub_code1", o.sims_subject_code));

                        //lst.Add(new SqlParameter("@opr", "P"));
                        //lst.Add(new SqlParameter("@sims_cur_code", o.sims_cur_code));
                        //lst.Add(new SqlParameter("@sims_academic_year", o.sims_academic_year));
                        //lst.Add(new SqlParameter("@sims_grade_code", o.sims_grade_code));
                        //lst.Add(new SqlParameter("@sims_section_code", o.sims_section_code));
                        //lst.Add(new SqlParameter("@sims_enroll_number", o.sims_student_enroll_number));
                        //if (string.IsNullOrEmpty(o.sims_subject_code))
                        //    lst.Add(new SqlParameter("@sims_sub_code", o.sims_subject_name_en));
                        //if (!string.IsNullOrEmpty(o.sims_subject_code))
                        //    lst.Add(new SqlParameter("@sims_sub_code1", o.sims_subject_code));

                        int ins = db.ExecuteStoreProcedureforInsert("sims.[sims_student_section_subject_proc]", lst);

                        //if (ins > 0)
                        //{

                        //    message.strMessage = "studen section subject Updated Successfully";


                        //}
                        //else
                        //{
                        //    message.strMessage = "studen section subject Not Updated ";
                        //}
                        message.strMessage = "Student Section Subject Updated Successfully";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
            }

            catch (Exception x)
            {
                message.strMessage = "Error In Parsing Fee Details";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("insert_student_section_subject_college")]
        public HttpResponseMessage insert_student_section_subject_college(List<Sims089> ob)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insert_student_section_subject(),PARAMETERS :: obj {2},Opr {2}";
            Log.Debug(string.Format(debug, "PP", "insert_student_section_subject", ob));

            Message message = new Message();
            string opr = "";

            List<SqlParameter> lst = new List<SqlParameter>();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    foreach (Sims089 o in ob)
                    {
                        lst.Clear();
                        lst.Add(new SqlParameter("@opr", "P"));
                        lst.Add(new SqlParameter("@sims_cur_code", o.sims_cur_code));
                        lst.Add(new SqlParameter("@sims_academic_year", o.sims_academic_year));
                        lst.Add(new SqlParameter("@sims_grade_code", o.sims_grade_code));
                        lst.Add(new SqlParameter("@sims_section_code", o.sims_section_code));
                        lst.Add(new SqlParameter("@sims_enroll_number", o.sims_student_enroll_number));
                        lst.Add(new SqlParameter("@sims_sub_code", o.sims_subject_name_en));
                        lst.Add(new SqlParameter("@sims_sub_code1", o.sims_subject_code));
                        lst.Add(new SqlParameter("@sims_section_term", o.sims_section_term));

                        //lst.Add(new SqlParameter("@opr", "P"));
                        //lst.Add(new SqlParameter("@sims_cur_code", o.sims_cur_code));
                        //lst.Add(new SqlParameter("@sims_academic_year", o.sims_academic_year));
                        //lst.Add(new SqlParameter("@sims_grade_code", o.sims_grade_code));
                        //lst.Add(new SqlParameter("@sims_section_code", o.sims_section_code));
                        //lst.Add(new SqlParameter("@sims_enroll_number", o.sims_student_enroll_number));
                        //if (string.IsNullOrEmpty(o.sims_subject_code))
                        //    lst.Add(new SqlParameter("@sims_sub_code", o.sims_subject_name_en));
                        //if (!string.IsNullOrEmpty(o.sims_subject_code))
                        //    lst.Add(new SqlParameter("@sims_sub_code1", o.sims_subject_code));

                        int ins = db.ExecuteStoreProcedureforInsert("sims.[sims_student_section_subject_dmc_proc]", lst);

                        //if (ins > 0)
                        //{

                        //    message.strMessage = "studen section subject Updated Successfully";


                        //}
                        //else
                        //{
                        //    message.strMessage = "studen section subject Not Updated ";
                        //}
                        message.strMessage = "Student Section Subject Updated Successfully";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                    }
                }
            }

            catch (Exception x)
            {
                message.strMessage = "Error In Parsing Fee Details";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getStudentName")]
        public HttpResponseMessage getStudentName(string curcode, string gradecode, string academicyear, string section, string subcode)
        {
            string cur_code = string.Empty;
            string str = "";
            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","Q"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_subject_code", subcode==""?null:subcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.sims_student_enroll_number == str select p);

                            Sims089 ob = new Sims089();
                            ob.sublist = new List<subject>();
                            ob.sims_cur_code = dr["sims_cur_code"].ToString();
                            ob.sims_academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_student_enroll_number = str;
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_student_name = dr["StudentName"].ToString();
                            try {
                                ob.sims_roll_number = dr["sims_roll_number"].ToString();

                           
                                ob.sims_roll_number_new = int.Parse(Regex.Match(dr["sims_roll_number"].ToString(), @"\d+").Value);
                             
                            }
                            catch (Exception ex) { }

                            try
                            {
                                ob.sims_religion_name = dr["sims_student_religion"].ToString();
                            }
                            catch (Exception ex) { }
                            
                            subject sb = new subject();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_status = dr["Status"].Equals(1) ? true : false;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getStudentNameCollege")]
        public HttpResponseMessage getStudentNameCollege(string curcode, string gradecode, string academicyear, string section, string subcode,string  section_term)
        {
            string cur_code = string.Empty;
            string str = "";
            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_dmc_proc]",
                    new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","Q"),
                          new SqlParameter("@sims_cur_code", curcode),
                          new SqlParameter("@sims_academic_year", academicyear),
                          new SqlParameter("@sims_grade_code", gradecode),
                          new SqlParameter("@sims_section_code", section),
                          new SqlParameter("@sims_subject_code", subcode==""?null:subcode),
                          new SqlParameter("@sims_section_term", section_term)
                       });
                     if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.sims_student_enroll_number == str select p);

                            Sims089 ob = new Sims089();
                            ob.sublist = new List<subject>();
                            ob.sims_cur_code = dr["sims_cur_code"].ToString();
                            ob.sims_academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_student_enroll_number = str;
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_student_name = dr["StudentName"].ToString();
                            try
                            {
                                ob.sims_roll_number = dr["sims_roll_number"].ToString();


                                ob.sims_roll_number_new = int.Parse(Regex.Match(dr["sims_roll_number"].ToString(), @"\d+").Value);

                            }
                            catch (Exception ex) { }

                            try
                            {
                                ob.sims_religion_name = dr["sims_student_religion"].ToString();
                            }
                            catch (Exception ex) { }

                            subject sb = new subject();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_status = dr["Status"].Equals(1) ? true : false;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            
        }


        [Route("getSectionsubject_name")]
        public HttpResponseMessage getSectionsubject_name(string curcode, string gradecode, string academicyear, string section, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","Z"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getSectionsubject_nameNew")]
        public HttpResponseMessage getSectionsubject_nameNew(string curcode, string gradecode, string academicyear, string section, string term, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_dmc_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","U"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_subject_term_code", term),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@teacher_code", login_user)
                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("getcheckSectionsubject_name")]
        public HttpResponseMessage getcheckSectionsubject_name(string curcode, string gradecode, string academicyear, string section, string enroll_number, string subject_code)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","C"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_enroll_number", enroll_number),
                new SqlParameter("@sims_subject_code", subject_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_student_name = dr["student_name"].ToString();
                            newobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("getgrade_name")]
        public HttpResponseMessage getgrade_name(string curcode, string academicyear, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","G"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            newobj.grade_code_name = dr["sims_grade_name_en"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getsection_name")]
        public HttpResponseMessage getsection_name(string curcode, string academicyear, string gradecode, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.[sims_student_section_subject_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","M"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                  new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            newobj.sims_section_code = dr["sims_section_code"].ToString();
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetRCGrade")]
        public HttpResponseMessage GetRCGrade(string curcode, string ayear, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_subject_proc",
                    new List<SqlParameter>()
                    {

                        new SqlParameter("@opr", "R"),
                        new SqlParameter("@sims_cur_code", curcode),
                        new SqlParameter("@bell_academic_year", ayear),
                        new SqlParameter("@user_name", teachercode),
                        new SqlParameter("@teacher_code", teachercode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("getTermNew")]
        public HttpResponseMessage getTermNew(string curcode, string academicyear,string sims_grade_code,string sims_section_code)
        {
            List<SecTermsubject> lstCuriculum = new List<SecTermsubject>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_subject_proc",
                    new List<SqlParameter>() 
                     { 
                         new SqlParameter("@opr", "Y"),
                         //new SqlParameter("@sims_term_code", sims_term_code),
                         new SqlParameter("@sims_cur_code", curcode),
                         new SqlParameter("@sims_academic_year", academicyear),
                         new SqlParameter("@sims_grade_code", sims_grade_code),
                         new SqlParameter("@sims_section_code", sims_section_code)
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SecTermsubject sequence = new SecTermsubject();


                            sequence.sims_term_code = dr["sims_term_code"].ToString();
                            sequence.sims_term_desc_en = dr["sims_term_desc_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("GetRCSection")]
        public HttpResponseMessage GetRCSection(string curcode, string ayear, string gradecode, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_student_section_subject_proc",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", gradecode),
                            new SqlParameter("@user_name", teachercode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        public object sims_term_code { get; set; }

        public object term_code { get; set; }
    }
}