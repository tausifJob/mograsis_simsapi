﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Setup
{

    [RoutePrefix("api/CouncilStudentMapping")]
    public class CouncilStudentMappingController:ApiController
    {


        [Route("getAllHouseNameByCur")]
        public HttpResponseMessage getAllHouseNameByCur(string cur_code, string academic_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllHouseNameByCur(),PARAMETERS :: NO";
           // Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims189> house_name = new List<Sims189>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_council_proc]",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "H"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189(); 
                            simsobj.sims_house_name = dr["sims_house_name"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            house_name.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, house_name);
            }
            catch (Exception e)
            {
             //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, house_name);
            }
        }


        [Route("getAllCouncilDetails")]
        public HttpResponseMessage getAllCouncilDetails(string cur_code, string academic_year, string house_cd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllCouncilDetails(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims189> council_list = new List<Sims189>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_council_proc]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "S"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year),
                               new SqlParameter("@sims_house_code", house_cd)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_user_login_code = dr["sims_user_login_code"].ToString();
                            simsobj.old_sims_user_login_code = dr["sims_user_login_code"].ToString();
                            simsobj.sims_stud_name = dr["stud"].ToString();
                            simsobj.sims_emp_name = dr["Emp_name"].ToString();
                            simsobj.sims_council_code = dr["sims_council_code"].ToString();
                            simsobj.sims_council_name = dr["sims_council_name"].ToString();
                            simsobj.sims_house_code = dr["sims_house_code"].ToString();
                            simsobj.sims_house_name = dr["house_name"].ToString();
                            if (!string.IsNullOrEmpty(dr["sims_house_council_enroll_start_date"].ToString()))

                                simsobj.sims_house_council_enroll_start_date = dr["sims_house_council_enroll_start_date"].ToString();
                            else
                                simsobj.sims_house_council_enroll_start_date = DateTime.Now.ToShortDateString();
                            simsobj.sims_house_council_enroll_end_date = dr["sims_house_council_enroll_end_date"].ToString();


                            council_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, council_list);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, council_list);
            }
        }


        [Route("getAllStudentsDetail")]
        public HttpResponseMessage getAllStudentsDetail(string cur_code, string academic_year, string grade_code, string sec_code, string house_cd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllStudentsDetail(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "STUDENT", "ALLGRADES"));

            List<Sims189> student_list = new List<Sims189>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_student_house_council_proc]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "R"),
                               new SqlParameter("@sims_cur_code", cur_code),
                               new SqlParameter("@sims_academic_year", academic_year),
                               new SqlParameter("@sims_grade_code", grade_code),
                               new SqlParameter("@sims_section_code", sec_code),
                               new SqlParameter("@sims_house_code", house_cd)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();
                            simsobj.sims_student_enroll_number = dr["sims_student_enroll_number"].ToString();
                            simsobj.Student_name = dr["Student_name"].ToString();

                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_code_name = dr["sims_grade_code_name"].ToString();

                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_section_code_name = dr["sims_section_code_name"].ToString();
                         
                            simsobj.sims_student_house = dr["sims_student_house"].ToString();
                            simsobj.sims_student_house_name = dr["sims_student_house_name"].ToString();

                            student_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, student_list);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, student_list);
            }
        }


        [Route("CouncilStudentCUD")]
        public HttpResponseMessage CouncilStudentCUD(List<Sims189> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LedgerControlCUD", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims189 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_student_house_council_proc]",
                            new List<SqlParameter>()
                            {
                               new SqlParameter("@opr",simsobj.opr),
                               
                               new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                               new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                               new SqlParameter("@sims_user_login_code", simsobj.sims_user_login_code),
                               new SqlParameter("@sims_house_code", simsobj.sims_house_code),
                               new SqlParameter("@sims_council_code", simsobj.sims_council_code),
                               new SqlParameter("@sims_house_council_enroll_start_date", null)
                              // new SqlParameter("@sims_house_council_enroll_end_date", simsobj.sims_house_council_enroll_end_date),

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("SearchEmployee")]
        public HttpResponseMessage SearchEmployee(string data)
        {

            Sims189 obj = new Sims189();

            List<Sims189> Emp_List = new List<Sims189>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims189>(data);
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Search]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'E'),
                            new SqlParameter("@user_name",obj.em_login_code),
                            new SqlParameter("@emp_name",obj.EmpName),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims189 simsobj = new Sims189();
                            
                            simsobj.em_login_code = dr["em_login_code"].ToString();
                            simsobj.em_number     = dr["em_number"].ToString();
                            simsobj.EmpName       = dr["EmpName"].ToString();
                            simsobj.em_mobile     = dr["em_mobile"].ToString();
                            simsobj.em_email      = dr["em_email"].ToString();
                            simsobj.em_service_status = dr["em_service_status"].ToString();
                            simsobj.em_service_status_name = dr["em_service_status_name"].ToString();
                            Emp_List.Add(simsobj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Emp_List);
            }
            catch (Exception e)
            {
                //   Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Emp_List);
            }
        }

        //[OperationContract]
        //public List<CommonUserControlClass> Get_Search_Employee(string user_name, string emp_name)
        //{
        //    List<CommonUserControlClass> mod_list = new List<CommonUserControlClass>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("Search", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@opr", 'S');
        //        cmd.Parameters.AddWithValue("@opr_mem_code", 'E');
        //        cmd.Parameters.AddWithValue("@user_name", user_name);
        //        cmd.Parameters.AddWithValue("@user_mail_id", "");
        //        cmd.Parameters.AddWithValue("@emp_name", emp_name);
        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            CommonUserControlClass objNew = new CommonUserControlClass();
        //            objNew.user_name = dr["em_login_code"].ToString();
        //            objNew.code = dr["em_number"].ToString();
        //            objNew.name = dr["EmpName"].ToString();
        //            mod_list.Add(objNew);
        //        }
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        return mod_list;
        //    }
        //}


        //[OperationContract]
        //public List<Sims189> GetAllSims_Council_details(string cur_cd, string academic_year, string house_cd)
        //{
        //    List<Sims189> mod_list = new List<Sims189>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@opr", 'S');
        //        cmd.Parameters.AddWithValue("@sims_cur_code", cur_cd);
        //        cmd.Parameters.AddWithValue("@sims_academic_year", academic_year);
        //        cmd.Parameters.AddWithValue("@sims_house_code", house_cd);

        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            Sims189 simsobj = new Sims189();
        //            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
        //            simsobj.sims_user_login_code = dr["sims_user_login_code"].ToString();
        //            simsobj.old_sims_user_login_code = dr["sims_user_login_code"].ToString();
        //            simsobj.sims_stud_name = dr["stud"].ToString();
        //            simsobj.sims_emp_name = dr["Emp_name"].ToString();
        //            simsobj.sims_council_code = dr["sims_council_code"].ToString();
        //            simsobj.sims_council_name = dr["sims_council_name"].ToString();
        //            simsobj.sims_house_code = dr["sims_house_code"].ToString();
        //            if (!string.IsNullOrEmpty(dr["sims_house_council_enroll_start_date"].ToString()))
        //                simsobj.sims_house_council_enroll_start_date = dr["sims_house_council_enroll_start_date"].ToString();
        //            else
        //                simsobj.sims_house_council_enroll_start_date = DateTime.Now.ToShortDateString();
        //            simsobj.sims_house_council_enroll_end_date = dr["sims_house_council_enroll_end_date"].ToString();

        //            mod_list.Add(simsobj);
        //        }
        //        cmd.Dispose();
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return mod_list;
        //    }
        //}
        //[OperationContract]
        //public List<Sims189> GetAll_Council_students()
        //{
        //    List<Sims189> mod_list = new List<Sims189>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@opr", 'D');
        //        //cmd.Parameters.AddWithValue("@sims_user_login_code", user_code);

        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            Sims189 simsobj = new Sims189();
        //            //simsobj.sims_enroll_number = dr["sims_student_enroll_number"].ToString();
        //            simsobj.sims_student_full_name = dr["sims_student_full_name"].ToString();
        //            mod_list.Add(simsobj);
        //        }
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return mod_list;
        //    }
        //}

        //[OperationContract]
        //public List<Sims189> Sims189Get_acadmic_year(string curcode)
        //{
        //    List<Sims189> list = new List<Sims189>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("Getdata", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@tbl_name", "[sims].[sims_academic_year]");
        //        cmd.Parameters.AddWithValue("@tbl_col_name1", "[sims_academic_year],[sims_academic_year_status]");
        //        cmd.Parameters.AddWithValue("@tbl_cond", "[sims_cur_code]='" + curcode + "'");
        //        cmd.Parameters.AddWithValue("@tbl_ordby", "[sims_academic_year]");
        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            Sims189 obj = new Sims189();
        //            obj.sims_academic_year = dr["sims_academic_year"].ToString();
        //            obj.sims_academic_year_status = dr["sims_academic_year_status"].ToString();

        //            list.Add(obj);
        //        }
        //        con.Close();
        //        return list;

        //    }
        //    catch (Exception)
        //    {
        //        con.Close();
        //        return list;
        //    }
        //}

        //[OperationContract]
        //public List<Sims189> GetAll_House_name(string cur_code, string academic_year)
        //{
        //    List<Sims189> mod_list = new List<Sims189>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("GetData", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("tbl_name", "[sims].[sims_house]");
        //        cmd.Parameters.AddWithValue("tbl_col_name1", "[sims_house_code],sims_house_name");
        //        cmd.Parameters.AddWithValue("@tbl_cond", "sims_house_status='A' and sims_cur_code='" + cur_code + "' and sims_cur_year='" + academic_year + "'");
        //        cmd.Parameters.AddWithValue("@tbl_OrdBy", "sims_house_name");
        //        dr = cmd.ExecuteReader();
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                Sims189 simsobj = new Sims189();
        //                simsobj.sims_house_code = dr["sims_house_code"].ToString();
        //                simsobj.sims_house_name = dr["sims_house_name"].ToString();
        //                mod_list.Add(simsobj);
        //            }
        //        }
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return mod_list;
        //    }
        //}


        //[OperationContract]
        //public List<Sims189> GetAllSims_Council_details(string cur_cd, string academic_year, string house_cd)
        //{
        //    List<Sims189> mod_list = new List<Sims189>();
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@opr", 'S');
        //        cmd.Parameters.AddWithValue("@sims_cur_code", cur_cd);
        //        cmd.Parameters.AddWithValue("@sims_academic_year", academic_year);
        //        cmd.Parameters.AddWithValue("@sims_house_code", house_cd);

        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            Sims189 simsobj = new Sims189();
        //            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
        //            simsobj.sims_user_login_code = dr["sims_user_login_code"].ToString();
        //            simsobj.old_sims_user_login_code = dr["sims_user_login_code"].ToString();
        //            simsobj.sims_stud_name = dr["stud"].ToString();
        //            simsobj.sims_emp_name = dr["Emp_name"].ToString();
        //            simsobj.sims_council_code = dr["sims_council_code"].ToString();
        //            simsobj.sims_council_name = dr["sims_council_name"].ToString();
        //            simsobj.sims_house_code = dr["sims_house_code"].ToString();
        //            if (!string.IsNullOrEmpty(dr["sims_house_council_enroll_start_date"].ToString()))
        //                simsobj.sims_house_council_enroll_start_date = dr["sims_house_council_enroll_start_date"].ToString();
        //            else
        //                simsobj.sims_house_council_enroll_start_date = DateTime.Now.ToShortDateString();
        //            simsobj.sims_house_council_enroll_end_date = dr["sims_house_council_enroll_end_date"].ToString();

        //            mod_list.Add(simsobj);
        //        }
        //        cmd.Dispose();
        //        con.Close();
        //        return mod_list;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return mod_list;
        //    }
        //}

        //[OperationContract]
        //public bool Insert_Sims_Student_House_Council(Sims189 simsobj)
        //{
        //    bool Inserted = false;
        //    int ins = 0;
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = con;
        //        cmd.Parameters.AddWithValue("@opr", 'X');
        //        cmd.Parameters.AddWithValue("@sims_cur_code", simsobj.sims_cur_code);
        //        cmd.Parameters.AddWithValue("@sims_academic_year", simsobj.sims_academic_year);
        //        cmd.Parameters.AddWithValue("@sims_council_code", simsobj.sims_council_code);
        //        cmd.Parameters.AddWithValue("@sims_house_code", simsobj.sims_house_code);
        //        if (simsobj.old_sims_user_login_code == "")
        //            cmd.Parameters.AddWithValue("@sims_user_login_code", simsobj.sims_user_login_code);
        //        else
        //            cmd.Parameters.AddWithValue("@sims_user_login_code", simsobj.old_sims_user_login_code);
        //        dr = cmd.ExecuteReader();
        //        if (dr.HasRows)
        //        {
        //            con.Close();
        //            if (con.State == ConnectionState.Closed)
        //                con.Open();
        //            cmd = new SqlCommand("sims_student_house_council", con);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Connection = con;
        //            cmd.Parameters.AddWithValue("@opr", 'U');
        //            cmd.Parameters.AddWithValue("@sims_cur_code", simsobj.sims_cur_code);
        //            cmd.Parameters.AddWithValue("@sims_academic_year", simsobj.sims_academic_year);
        //            cmd.Parameters.AddWithValue("@sims_council_code", simsobj.sims_council_code);
        //            cmd.Parameters.AddWithValue("@sims_house_code", simsobj.sims_house_code);
        //            cmd.Parameters.AddWithValue("@sims_user_login_code", simsobj.sims_user_login_code);
        //            cmd.Parameters.AddWithValue("@old_sims_user_login_code", simsobj.old_sims_user_login_code);
        //            cmd.Parameters.AddWithValue("@sims_house_council_enroll_start_date", simsobj.sims_house_council_enroll_start_date);
        //            cmd.Parameters.AddWithValue("@sims_house_council_enroll_end_date", simsobj.sims_house_council_enroll_end_date);
        //            ins = cmd.ExecuteNonQuery();
        //            if (ins > 0)
        //            {
        //                Inserted = true;
        //            }

        //        }
        //        else
        //        {
        //            con.Close();
        //            if (con.State == ConnectionState.Closed)
        //                con.Open();
        //            cmd = new SqlCommand("sims_student_house_council", con);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Connection = con;
        //            cmd.Parameters.AddWithValue("@opr", 'T');
        //            cmd.Parameters.AddWithValue("@sims_cur_code", simsobj.sims_cur_code);
        //            cmd.Parameters.AddWithValue("@sims_academic_year", simsobj.sims_academic_year);
        //            cmd.Parameters.AddWithValue("@sims_council_code", simsobj.sims_council_code);
        //            cmd.Parameters.AddWithValue("@sims_house_code", simsobj.sims_house_code);
        //            cmd.Parameters.AddWithValue("@sims_user_login_code", simsobj.sims_user_login_code);
        //            ins = cmd.ExecuteNonQuery();
        //            if (ins > 0)
        //            {
        //                Inserted = true;
        //            }
        //        }



        //        con.Close();
        //        return Inserted;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return Inserted;
        //    }

        //}

        //[OperationContract]
        //public bool sims189_chk_user_status(Sims189 simsobj)
        //{
        //    bool Updated = false;
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = con;
        //        cmd.Parameters.AddWithValue("@opr", 'Y');
        //        cmd.Parameters.AddWithValue("@sims_user_login_code", simsobj.sims_user_login_code);
        //        dr = cmd.ExecuteReader();
        //        if (dr.HasRows)
        //        {
        //            Updated = false;
        //        }
        //        else
        //        {
        //            Updated = true;
        //        }


        //        con.Close();
        //        return Updated;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return Updated;
        //    }
        //}


        //[OperationContract]
        //public bool Delete_Sims_Student_House_Council(Sims189 simsobj)
        //{
        //    bool Deleted = false;
        //    try
        //    {
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        cmd = new SqlCommand("sims_student_house_council", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = con;

        //        cmd.Parameters.AddWithValue("@opr", 'D');
        //        cmd.Parameters.AddWithValue("@sims_cur_code", simsobj.sims_cur_code);
        //        cmd.Parameters.AddWithValue("@sims_academic_year", simsobj.sims_academic_year);
        //        cmd.Parameters.AddWithValue("@sims_grade_code", simsobj.sims_grade_code);
        //        cmd.Parameters.AddWithValue("@sims_section_code", simsobj.sims_section_code);
        //        cmd.Parameters.AddWithValue("@sims_enroll_number", simsobj.sims_enroll_number);
        //        cmd.Parameters.AddWithValue("@sims_house_code", simsobj.sims_house_code);
        //        cmd.Parameters.AddWithValue("@sims_council_code", simsobj.sims_council_code);

        //        int del = cmd.ExecuteNonQuery();
        //        if (del > 0)
        //        {
        //            Deleted = true;
        //        }
        //        cmd.Dispose();
        //        con.Close();
        //        return Deleted;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Close();
        //        return Deleted;
        //    }

        //}

    }
}