﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.modules.ApplicationRoleMappingController
{
    [RoutePrefix("api/common/ApplicationRoleMapping")]
    [BasicAuthentication]
    public class ApplicationRoleMappingController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllModules")]
        public HttpResponseMessage getAllModules()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllModules(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllModules"));

            List<string> mod_list = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                         //   new SqlParameter("@opr", 'A'),     
                 new SqlParameter("@tbl_name", "comn.comn_module"),
                 new SqlParameter("@tbl_col_name1", "*"),
                 new SqlParameter("@tbl_cond", " comn.comn_module.comn_mod_status='A'"),
                 new SqlParameter("@tbl_ordby", "comn_mod_name_en,comn_mod_code ")
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            mod_list.Add(dr["comn_mod_name_en"].ToString());
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("GetAllModules")]
        public HttpResponseMessage GetAllModules(string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllModules(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllModules"));

            List<string> mod_list = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetApplicationsByModule",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'M'), 
                            new SqlParameter("@username", username)


                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            mod_list.Add(dr["comn_mod_name_en"].ToString());
                            //  dr["comn_mod_code"].ToString()
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getAllApplications")]
        public HttpResponseMessage getAllApplications(bool flag, string modNames, string rolename)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllApplications(),PARAMETERS :: FLAG{2},MODNAME{3},ROLENAME{4}";
            Log.Debug(string.Format(debug, "Common", "getAllApplications", flag, modNames, rolename));

            List<string> mod_list = new List<string>();

            try
            {
                if (flag == true)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("comn_role_application",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),                         
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                mod_list.Add(dr["comn_appl_name_en"].ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(modNames) == false)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("GetApplicationsByModule",
                                new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),  
                             new SqlParameter("@modname", modNames),                         
                            new SqlParameter("@rolename", rolename), 
                            new SqlParameter("@username", "admin"), 
                        
                         });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    mod_list.Add(dr["comn_appl_name_en"].ToString());
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }




        [Route("getAllApplicationsUser")]
        public HttpResponseMessage getAllApplicationsUser(bool flag, string modNames, string rolename, string username)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllApplications(),PARAMETERS :: FLAG{2},MODNAME{3},ROLENAME{4},User{5}";
            Log.Debug(string.Format(debug, "Common", "getAllApplications", flag, modNames, rolename, username));

            List<application> mod_list = new List<application>();

            try
            {
                if (flag == true)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name","comn.comn_application,comn.comn_module"),
                            new SqlParameter("@tbl_col_name1", "*"),                         
                            new SqlParameter("@tbl_cond", "comn.comn_module.comn_mod_code=comn.comn_application.comn_appl_mod_code AND comn.comn_application.comn_appl_status='A'"),                         
                            new SqlParameter("@tbl_ordby", "comn_appl_name_en")                       
                          
                         });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                application add = new application();
                                add.app_nm = dr["comn_appl_name_en"].ToString();
                                mod_list.Add(add);
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(modNames) == false)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("GetApplicationsByModule",
                                new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S'),  
                             new SqlParameter("@modname", modNames),                         
                            new SqlParameter("@rolename", rolename), 
                            new SqlParameter("@username", username)
                        
                        
                         });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    application add = new application();
                                    if (dr["comn_appl_code"].ToString() == "XXX")
                                        add.app_nm = dr["comn_appl_name_en"].ToString();
                                    else
                                        add.app_nm = dr["comn_appl_name_en"].ToString() + " (" + dr["comn_appl_code"].ToString() + ")";
                                    add.app_Code = dr["comn_appl_code"].ToString();

                                    mod_list.Add(add);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }





        //All Application Based on User Group

        [Route("getUserApplications")]
        public HttpResponseMessage getUserApplications(string roleCode)
        {
            int i = 0;
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserApplications(),PARAMETERS :: ROLECODE{2}";
            Log.Debug(string.Format(debug, "Common", "getUserApplications", roleCode));

            List<string> mod_list = new List<string>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetUserApplictions",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@RoleName", roleCode),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            mod_list.Add(dr["comn_appl_name_en"].ToString());

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getUserApplicationsNew")]
        public HttpResponseMessage getUserApplicationsNew(string roleCode)
        {
            int i = 0;
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserApplications(),PARAMETERS :: ROLECODE{2}";
            Log.Debug(string.Format(debug, "Common", "getUserApplications", roleCode));

            List<application> mod_list = new List<application>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetUserApplictions",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@RoleName", roleCode),                         
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            application app = new application();
                            if (dr["comn_appl_code"].ToString() == "XXX")
                                app.app_nm = dr["comn_appl_name_en"].ToString();
                            else
                            app.app_nm = dr["comn_appl_name_en"].ToString() + " (" + dr["comn_appl_code"].ToString() + ")";
                            app.app_Code = dr["comn_appl_code"].ToString();

                            mod_list.Add(app);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getAllRoleCode")]
        public HttpResponseMessage getAllRoleCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRoleCode(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getAllRoleCode"));

            List<commonClass> mod_list = new List<commonClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                          
               new SqlParameter("@tbl_name", "comn.comn_user_role"),
               new SqlParameter("@tbl_col_name1", "*"),
               new SqlParameter("@tbl_cond", ""),
               new SqlParameter("@tbl_ordby", "[comn_role_name]")

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            commonClass comseq = new commonClass();
                            comseq.comn_role_code = dr["comn_role_name"].ToString();
                            comseq.comn_role_code_o = dr["comn_role_code"].ToString();

                            mod_list.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("DeleteRoleApp")]
        public HttpResponseMessage DeleteRoleApp(string rolecode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : DeleteRoleApp(),PARAMETERS :: ROLECODE{2}";
            Log.Debug(string.Format(debug, "Common", "DeleteRoleApp", rolecode));

            bool ifdeleted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_role_application",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@appCode", ""),
                            new SqlParameter("@sims_appl_form_field", ""),
                            new SqlParameter("@comn_role_code", rolecode),
                            new SqlParameter("@comn_appl_code", ""),
                            new SqlParameter("@opr", "D"),                        
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ifdeleted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, ifdeleted);

            }
            return Request.CreateResponse(HttpStatusCode.OK, ifdeleted);
        }


        public class roleappp
        {
            public string rolenm { get; set; }
            public List<string> app_Code { get; set; }
            public string opr { get; set; }
            public string user_name { get; set; }

        }

        public class application
        {
            public string app_nm { get; set; }
            public string app_Code { get; set; }
        }

        [Route("InsertComn_role_App")]
        public HttpResponseMessage InsertComn_role_App(roleappp obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertComn_role_App(),PARAMETERS :: ROLENAME {2}";
            Log.Debug(string.Format(debug, "COMMON", "InsertComn_role_App", obj));

            // List<string> app_Code = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(app_Code1);
            string rolecode = string.Empty;
            bool ins = false;
            using (DBConnection db = new DBConnection())
            {
                db.Open();
                SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                    new List<SqlParameter>() 
                                     { 
                                            
                    new SqlParameter("@tbl_name", "comn.comn_user_role"),
                    new SqlParameter("@tbl_col_name1", "comn_role_code"),
                    new SqlParameter("@tbl_cond", "comn_role_name='" + obj.rolenm + "'")

                                     });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        rolecode = dr["comn_role_code"].ToString();
                    }
                }
            }


            Message message = new Message();
            try
            {
                if (obj.app_Code.Count > 0)
                {
                    for (int i = 0; i < obj.app_Code.Count; i++)
                    {
                        try
                        {
                            //Application Code

                            string appCode = string.Empty;
                            try
                            {
                                using (DBConnection db = new DBConnection())
                                {
                                    db.Open();
                                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                                        new List<SqlParameter>() 
                                     { 
                                          //  new SqlParameter("@opr",'X'),
                                new SqlParameter("@tbl_name", "comn.comn_application"),
                                new SqlParameter("@tbl_col_name1", "comn_appl_code"),
                                new SqlParameter("@tbl_cond", "comn_appl_name_en='" + obj.app_Code[i].ToString() + "'")
                               
                                     });
                                    if (dr.HasRows)
                                    {
                                        while (dr.Read())
                                        {
                                            appCode = dr["comn_appl_code"].ToString();
                                        }
                                    }
                                }

                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("comn_role_application",
                                        new List<SqlParameter>() 
                                     { 
                                            new SqlParameter("@appCode", CommonStaticClass.appName),
                                            new SqlParameter("@sims_appl_form_field", CommonStaticClass.simsapplformfield1),
                                            new SqlParameter("@comn_role_code", rolecode),
                                            new SqlParameter("@comn_appl_code", appCode),
                                            new SqlParameter("@opr", obj.opr),
                                     });
                                    if (dr1.HasRows)
                                    {
                                        while (dr1.Read())
                                        {
                                            // appCode = dr1["comn_appl_code"].ToString();
                                            ins = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception de)
                            {
                            }
                        }
                        catch (Exception fr)
                        {
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }



        [Route("InsertComn_role_App_new")]
        public HttpResponseMessage InsertComn_role_App_new(roleappp obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertComn_role_App(),PARAMETERS :: ROLENAME {2}";
            Log.Debug(string.Format(debug, "COMMON", "InsertComn_role_App", obj));

            // List<string> app_Code = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(app_Code1);
            string rolecode = string.Empty;
            bool ins = false;

            try
            {
                if (obj.app_Code.Count > 0)
                {
                    for (int i = 0; i < obj.app_Code.Count; i++)
                    {
                        try
                        {
                            //Application Code

                            string appCode = string.Empty;
                            try
                            {

                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("comn_role_application",
                                        new List<SqlParameter>() 
                                     { 
                                            new SqlParameter("@appCode", CommonStaticClass.appName),
                                            new SqlParameter("@sims_appl_form_field", CommonStaticClass.simsapplformfield1),
                                            new SqlParameter("@comn_role_code", obj.rolenm),
                                            new SqlParameter("@comn_appl_code", obj.app_Code[i].ToString()),
                                            new SqlParameter("@opr", obj.opr),
                                            new SqlParameter("@usercode", obj.user_name),

                                     });
                                    if (dr1.HasRows)
                                    {
                                        while (dr1.Read())
                                        {
                                            // appCode = dr1["comn_appl_code"].ToString();
                                            ins = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception de)
                            {
                            }
                        }
                        catch (Exception fr)
                        {
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }


        [Route("InsertComn_role_App_new1")]
        public HttpResponseMessage InsertComn_role_App_new1(roleappp obj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : InsertComn_role_App(),PARAMETERS :: ROLENAME {2}";
            Log.Debug(string.Format(debug, "COMMON", "InsertComn_role_App", obj));

            // List<string> app_Code = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(app_Code1);
            string rolecode = string.Empty;
            bool ins = false;

            try
            {
                if (obj.app_Code.Count > 0)
                {
                    for (int i = 0; i < obj.app_Code.Count; i++)
                    {
                        try
                        {
                            //Application Code

                            string appCode = string.Empty;
                            try
                            {

                                using (DBConnection db1 = new DBConnection())
                                {
                                    db1.Open();
                                    SqlDataReader dr1 = db1.ExecuteStoreProcedure("comn.comn_role_application_proc",
                                        new List<SqlParameter>() 
                                     { 
                                            new SqlParameter("@appCode", CommonStaticClass.appName),
                                            new SqlParameter("@sims_appl_form_field", CommonStaticClass.simsapplformfield1),
                                            new SqlParameter("@comn_role_code", obj.rolenm),
                                            new SqlParameter("@comn_appl_code", obj.app_Code[i].ToString()),
                                            new SqlParameter("@opr", obj.opr),
                                            new SqlParameter("@usercode", obj.user_name),

                                     });
                                    if (dr1.HasRows)
                                    {
                                        while (dr1.Read())
                                        {
                                            // appCode = dr1["comn_appl_code"].ToString();
                                            ins = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception de)
                            {
                            }
                        }
                        catch (Exception fr)
                        {
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, ins);
        }


    }
}