﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.ERP.hrmsClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Controllers.hrms;

using SIMSAPI.Models.ERP.LibraryClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/EmailTransaction")]
    public class EmailCommunicationController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Seelct
        [Route("getSelectInbox")]
        public HttpResponseMessage getSelectInbox()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSelectInbox(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getSelectInbox"));

            List<EmailT> Inbox_list = new List<EmailT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pp_sims_email_transaction]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmailT simsobj = new EmailT();
                            simsobj.sims_enroll_number= dr["sims_enroll_number"].ToString();
                            simsobj.sims_email_number = dr["sims_email_number"].ToString();
                            simsobj.sims_sender_email_id= dr["sims_sender_email_id"].ToString();
                            simsobj.sims_email_father_email_id1= dr["sims_email_father_email_id1"].ToString();
                            simsobj.sims_email_date= dr["sims_email_date"].ToString();
                            simsobj.sims_email_message= dr["sims_email_message"].ToString();
                            simsobj.sims_email_subject= dr["sims_email_subject"].ToString();
                            simsobj.sims_email_attachment= dr["sims_email_attachment"].ToString();
                            simsobj.sims_user_name= dr["sims_user_name"].ToString();
                            // simsobj.pc_code = dr["pc_code"].ToString();
                            //simsobj.pc_desc = dr["pc_desc"].ToString();

                            Inbox_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Inbox_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inbox_list);
        }



        [Route("getfullmessage")]
        public HttpResponseMessage getfullmessage(string email_no)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getfullmessage(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Common", "getfullmessage"));

            List<EmailT> Inbox_list = new List<EmailT>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[pp_sims_email_transaction]",
                        new List<SqlParameter>()
                         {
                           new SqlParameter("@opr", 'R'),
                           new SqlParameter("@sims_email_number", email_no),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmailT simsobj = new EmailT();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.sims_email_number = dr["sims_email_number"].ToString();
                            simsobj.sims_sender_email_id = dr["sims_sender_email_id"].ToString();
                            simsobj.sims_email_father_email_id1 = dr["sims_email_father_email_id1"].ToString();
                            simsobj.sims_email_date = dr["sims_email_date"].ToString();
                            simsobj.sims_email_message = dr["sims_email_message"].ToString();
                            simsobj.sims_email_subject = dr["sims_email_subject"].ToString();
                            simsobj.sims_email_attachment = dr["sims_email_attachment"].ToString();
                            simsobj.sims_user_name = dr["sims_user_name"].ToString();
                            // simsobj.pc_code = dr["pc_code"].ToString();
                            //simsobj.pc_desc = dr["pc_desc"].ToString();

                            Inbox_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Inbox_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inbox_list);
        }



    }
}