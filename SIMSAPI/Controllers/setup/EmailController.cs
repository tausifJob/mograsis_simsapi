﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/emailtemplate")]
    public class EmailController : ApiController
    {
        [Route("getEmailDetail")]
        public HttpResponseMessage getEmailDetail()
        {
            List<email> Email = new List<email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_sms_message_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email obj = new email();
                            obj.sims_msg_sr_no = dr["sims_msg_sr_no"].ToString();
                            obj.sims_msg_mod_code = dr["sims_msg_mod_code"].ToString();

                            // obj.comn_mod_code = dr["comn_mod_code"].ToString();
                            //obj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();

                            obj.sims_msg_appl_code = dr["sims_msg_appl_code"].ToString();
                            obj.sims_msg_type = dr["sims_msg_type"].ToString();
                            obj.sims_msg_subject = dr["sims_msg_subject"].ToString();
                            obj.sims_msg_body = dr["sims_msg_body"].ToString();
                            obj.sims_msg_signature = dr["sims_msg_signature"].ToString();
                            if (dr["sims_msg_status"].ToString() == "A")
                                obj.sims_msg_status = true;
                            else
                                obj.sims_msg_status = false;
                            Email.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Email);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Email);
        }

        [Route("getApplicationCode")]
        public HttpResponseMessage getApplicationCode()
        {
            List<email> Email = new List<email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_sms_message_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "W")

                         }
                         );

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email obj = new email();
                            obj.comn_mod_code = dr["comn_mod_code"].ToString();
                            obj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            if (dr["comn_mod_status"].ToString() == "A")
                                obj.comn_mod_status = true;
                            else
                                obj.comn_mod_status = false;
                            Email.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Email);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Email);
        }

        [Route("getModuleCode")]
        public HttpResponseMessage getModuleCode(string modecode)
        {
            List<email> Email = new List<email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_sms_message_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "V"),
                            new SqlParameter("@comn_mod_code",modecode)

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email obj = new email();
                            obj.comn_appl_code = dr["comn_appl_code"].ToString();
                            obj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            obj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            obj.comn_appl_type = dr["comn_appl_type"].ToString();
                            obj.comn_app_mode = dr["comn_app_mode"].ToString();
                            if (dr["comn_appl_status"].ToString() == "A")
                                obj.comn_appl_status = true;
                            else
                                obj.comn_appl_status = false;
                            Email.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Email);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Email);
        }

        [Route("getAllModuleCode")]
        public HttpResponseMessage getAllModuleCode()
        {
            List<email> Email = new List<email>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_email_sms_message_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "Z")
                            

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            email obj = new email();
                            obj.comn_appl_code = dr["comn_appl_code"].ToString();
                            obj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            obj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            obj.comn_appl_type = dr["comn_appl_type"].ToString();
                            obj.comn_app_mode = dr["comn_app_mode"].ToString();
                            if (dr["comn_appl_status"].ToString() == "A")
                                obj.comn_appl_status = true;
                            else
                                obj.comn_appl_status = false;
                            Email.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Email);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Email);
        }

        [Route("EmailCUD")]
        public HttpResponseMessage EmailCUD(List<email> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (email simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_email_sms_message_proc]",
                            new List<SqlParameter>()

                            {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_msg_sr_no", simsobj.sims_msg_sr_no),
                                 new SqlParameter("@sims_msg_mod_code", simsobj.comn_mod_code),
                                 new SqlParameter("@sims_msg_appl_code", simsobj.comn_appl_code),
                                 new SqlParameter("@sims_msg_type", simsobj.sims_msg_type),
                                 new SqlParameter("@sims_msg_subject", simsobj.sims_msg_subject),
                                 new SqlParameter("@sims_msg_body", simsobj.sims_msg_body),
                                 new SqlParameter("@sims_msg_signature", simsobj.sims_msg_signature),
                                 new SqlParameter("@sims_msg_status", simsobj.sims_msg_status==true?"A":"I")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}