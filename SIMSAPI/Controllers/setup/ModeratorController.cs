﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Moderator")]
    public class ModeratorController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAutoGenerateModeratorCommCode")]
        public HttpResponseMessage getAutoGenerateModeratorCommCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateModeratorCommCode(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "STUDENT", "AutoGenerateModeratorCommCode"));

            string commcode = string.Empty;
            Sims164 simsobj = new Sims164();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_moderator_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sims_comm_code = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj.sims_comm_code);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj.sims_comm_code);
            }
        }

        [Route("getModerator")]
        public HttpResponseMessage getModerator()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModerator(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "Moderator"));

            List<Sims164> moderator_list = new List<Sims164>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_moderator_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims164 simsobj = new Sims164();
                            simsobj.sims_cur_name = dr["sims_cur_code"].ToString();
                            simsobj.sims_attendance_cur_code = dr["curcode"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["acad_yr_desc"].ToString();
                            simsobj.sims_comm_code = dr["sims_comm_code"].ToString();
                            simsobj.sims_comm_code_name = dr["sims_comm_code_name"].ToString();
                            simsobj.sims_email_flag = dr["sims_email_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_sms_flag = dr["sims_sms_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_bcc_flag = dr["sims_bcc_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_bcc_email_flag = dr["sims_bcc_email_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_bcc_sms_flag = dr["sims_bcc_sms_flag"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_status = dr["sims_comm_status"].ToString().Equals("A") ? true : false;
                            moderator_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, moderator_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, moderator_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, moderator_list);

            }
            // return Request.CreateResponse(HttpStatusCode.OK, moderator_list);
        }

        [Route("CUDModerator")]
        public HttpResponseMessage CUDModerator(List<Sims164> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDModerator(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "MODERATOR"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims164 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_moderator_proc]",
                                new List<SqlParameter>() 
                             {                             
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_cur_name", simsobj.sims_attendance_cur_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@sims_comm_code", simsobj.sims_comm_code),
                                new SqlParameter("@sims_comm_code_name", simsobj.sims_comm_code_name),
                                new SqlParameter("@sims_email_flag", simsobj.sims_email_flag == true? "A":"I"),
                                new SqlParameter("@sims_sms_flag", simsobj.sims_sms_flag == true? "A":"I"),
                                new SqlParameter("@sims_bcc_flag", simsobj.sims_bcc_flag == true? "A":"I"),
                                new SqlParameter("@sims_bcc_email_flag", simsobj.sims_bcc_email_flag == true? "A":"I"),
                                new SqlParameter("@sims_bcc_sms_flag", simsobj.sims_bcc_sms_flag == true? "A":"I"),
                                new SqlParameter("@sims_comm_status", simsobj.sims_comm_status == true? "A":"I"),
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }

}