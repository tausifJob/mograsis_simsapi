﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/Honour")]
    public class HonourController : ApiController
    {
        [Route("getMaxValue")]
        public HttpResponseMessage getMaxValue()
        {
            List<Sim613> com_list = new List<Sim613>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "M")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim613 obj = new Sim613();
                            obj.max_num = dr["max_num"].ToString();
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getHonourDetail")]
        public HttpResponseMessage getCostCentreDetail()
        {
            List<Sim613> com_list = new List<Sim613>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim613 obj = new Sim613();
                            obj.sims_honour_cur_code = dr["sims_honour_cur_code"].ToString();
                            obj.sims_honour_academic_year = dr["sims_honour_academic_year"].ToString();
                            obj.sims_honour_cur_name = dr["sims_honour_cur_name"].ToString();
                            obj.year_desc = dr["year_desc"].ToString();
                            
                            obj.sims_honour_code = dr["sims_honour_code"].ToString();
                            obj.sims_honour_desc = dr["sims_honour_desc"].ToString();
                            obj.sims_honour_type = dr["sims_honour_type"].ToString();
                            obj.sims_honour_criteria_type = dr["sims_honour_criteria_type"].ToString();
                            obj.sims_honour_min_criteria = dr["sims_honour_min_criteria"].ToString();
                            if (dr["sims_honour_status"].ToString() == "A")
                                obj.sims_honour_status = true;
                            else
                                obj.sims_honour_status = false;
                            com_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, com_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("HonourCUD")]
        public HttpResponseMessage CostCentreDetailCUD(List<Sim613> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim613 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_honour_proc]",
                            new List<SqlParameter>()

                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_honour_cur_code", simsobj.sims_honour_cur_code),
                                 new SqlParameter("@sims_honour_academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@sims_honour_code", simsobj.sims_honour_code),
                                 new SqlParameter("@sims_honour_desc", simsobj.sims_honour_desc),
                                 new SqlParameter("@sims_honour_type", simsobj.sims_honour_type),
                                 new SqlParameter("@sims_honour_criteria_type", simsobj.sims_honour_criteria_type),
                                 new SqlParameter("@sims_honour_min_criteria", simsobj.sims_honour_min_criteria),
                                 new SqlParameter("@sims_honour_status", simsobj.sims_honour_status==true?"A":"I")
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}