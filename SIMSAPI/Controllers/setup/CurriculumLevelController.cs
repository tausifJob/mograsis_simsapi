﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/CurLevel")]
    [BasicAuthentication]
    public class CurriculumLevelController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCuriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "SETUP", "ERP/Section"));

            List<Sims004> lstCuriculum = new List<Sims004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@tbl_name", "sims.sims_cur"),
                            new SqlParameter("@tbl_col_name1", "*"),
                           new SqlParameter("@tbl_cond", "sims_cur_status='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims004 sequence = new Sims004();
                            sequence.level_cur_code = dr["sims_cur_code"].ToString();
                            sequence.level_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getCurlevel")]
        public HttpResponseMessage getCurlevel()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getCurlevel(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "COMMON", "getCurlevel"));

            List<Sims004> cur_list = new List<Sims004>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur_level",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","S")
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims004 simsobj = new Sims004();
                            simsobj.level_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.level_cur_code_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.level_code = dr["sims_cur_level_code"].ToString();
                            simsobj.level_name_en = dr["sims_cur_level_name_en"].ToString();
                            simsobj.level_name_ar = dr["sims_cur_level_name_ar"].ToString();
                            simsobj.level_name_fr = dr["sims_cur_level_name_fr"].ToString();
                            simsobj.level_name_ot = dr["sims_cur_level_name_ot"].ToString();
                            simsobj.level_status = dr["sims_cur_level_status"].ToString().Equals("A") ? true : false;
                            cur_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, cur_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, cur_list);
        }

        [Route("CUDCurLevel")]
        public HttpResponseMessage CUDCurLevel(Sims004 simsobj)//string data, string opr
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDCurLevel(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "COMMON", "CUDCurLevel", simsobj.opr));

            //Sims004 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims004>(data);
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_cur_level",
                            new List<SqlParameter>() 
                         {                             
                                    new SqlParameter("@opr", simsobj.opr),
                                    new SqlParameter("@sims_cur_code", simsobj.level_cur_code),
                                    new SqlParameter("@sims_cur_level_code", simsobj.level_code),
                                    new SqlParameter("@sims_cur_level_name_en", simsobj.level_name_en),
                                    new SqlParameter("@sims_cur_level_name_ar", string.IsNullOrEmpty(simsobj.level_name_ar) == false?simsobj.level_name_ar:""),
                                    new SqlParameter("@sims_cur_level_name_fr", string.IsNullOrEmpty(simsobj.level_name_fr) == false?simsobj.level_name_fr:""),
                                    new SqlParameter("@sims_cur_level_name_ot", string.IsNullOrEmpty(simsobj.level_name_ot) == false?simsobj.level_name_ot:""),
                                    new SqlParameter("@sims_cur_level_status", simsobj.level_status == true?"A":"I")
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "1";
                        }
                        else
                        {
                            message.strMessage = "0";
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "2";//Error In Parsing Information!!
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "3";//Error In Deleting CuriculumLevelDetails Information!!
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("DCurLevel")]
        public HttpResponseMessage DCurLevel(string objlist)//string data, string opr
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDCurLevel(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "COMMON", "CUDCurLevel", "Delete"));

            //Sims004 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims004>(data);
            List<string> simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(objlist);
            Message message = new Message();
            try
            {
                if (objlist != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr;
                        foreach (string str in simsobj)
                        {
                            dr = db.ExecuteStoreProcedure("sims_cur_level",
                               new List<SqlParameter>() 
                         {                             
                                    new SqlParameter("@opr","D"),
                                    new SqlParameter("@sims_cur_level_code", str),
                         });
                            if (dr.RecordsAffected > 0)
                            {
                                dr.Close();
                                message.strMessage = "1";
                            }
                            else
                            {
                                message.strMessage = "0";
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "2";//Error In Parsing Information!!
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "3";//Error In Deleting CuriculumLevelDetails Information!!
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
    }
}