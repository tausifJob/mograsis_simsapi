﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/ModeratorUser")]
    public class ModeratorUserController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAllAcaYearModUser")]
        public HttpResponseMessage getAllAcaYearModUser(string cur_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcaYearModUser(),PARAMETERS :: CURNAME{2}";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGenerateMedicalSerialNumber", cur_name));

            List<Sims165> mod_list = new List<Sims165>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "[sims].[sims_academic_year]"),
                                new SqlParameter("@tbl_col_name1", "[sims_academic_year],sims_academic_year_description"),
                                //new SqlParameter("@tbl_cond", "[sims_cur_code]=(Select [sims_cur_code] from [sims].[sims_cur] where [sims_cur_short_name_en]=" + "'" + cur_name + "') and sims_academic_year_status=C")
                                 new SqlParameter("@tbl_cond", "[sims_cur_code]='"+cur_name+"' and sims_academic_year_status='C'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims165 simsObj = new Sims165();
                            simsObj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsObj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getAllGrades")]
        public HttpResponseMessage getAllGrades(string cur_code, string ac_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllGrades(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "COMMON", "getAllGrades", cur_code, ac_year));

            List<simsClass> grade_list = new List<simsClass>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_grade",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "Z"),
                                new SqlParameter("@sims_cur_code", cur_code),
                                new SqlParameter("@sims_academic_year", ac_year),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsClass simsobj = new simsClass();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, grade_list);
        }

        [Route("getAllModeratorNames")]
        public HttpResponseMessage getAllModeratorNames()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllModeratorNames(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllModeratorNames"));

            List<Sims165> moderator_list = new List<Sims165>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_moderator_user_proc]",
                        new List<SqlParameter>() 
                         { 
                                 new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims165 simsObj = new Sims165();
                            simsObj.sims_comm_code = dr["sims_comm_code"].ToString();
                            simsObj.sims_comm_code_name = dr["sims_comm_code_name"].ToString();
                            moderator_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, moderator_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, moderator_list);
            }
        }

        [Route("getAllComnUserName")]
        public HttpResponseMessage getAllComnUserName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllComnUserName(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getAllComnUserName"));

            List<Sims165> user_list = new List<Sims165>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_moderator_user_proc]",
                        new List<SqlParameter>() 
                         { 
                                  new SqlParameter("@opr", 'C')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims165 simsObj = new Sims165();
                            simsObj.sims_comm_user_name = dr["comn_user_name"].ToString();
                            user_list.Add(simsObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, user_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, user_list);
            }
        }

        [Route("getModeratorUser")]
        public HttpResponseMessage getModeratorUser()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getModeratorUser(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "getModeratorUser"));

            List<Sims165> user_list = new List<Sims165>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_moderator_user_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims165 simsobj = new Sims165();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            simsobj.sims_comm_code = dr["sims_comm_code"].ToString();
                            simsobj.sims_comm_code_name = dr["sims_comm_code_name"].ToString();
                            simsobj.sims_comm_user_name = dr["sims_comm_user_code"].ToString();
                            simsobj.sims_comm_user_code_status = dr["sims_comm_user_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_email_user_code = dr["sims_comm_email_user_code"].ToString();
                            simsobj.sims_comm_email_user_code_status = dr["sims_comm_email_user_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_sms_user_code = dr["sims_comm_sms_user_code"].ToString();
                            simsobj.sims_comm_sms_user_code_status = dr["sims_comm_sms_user_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_bcc_user_code = dr["sims_comm_bcc_user_code"].ToString();
                            simsobj.sims_comm_bcc_user_code_status = dr["sims_comm_bcc_user_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_bcc_email_user_code = dr["sims_comm_bcc_email_user_code"].ToString();
                            simsobj.sims_comm_bcc_email_user_code_status = dr["sims_comm_bcc_email_user_code_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_comm_bcc_sms_user_code = dr["sims_comm_bcc_sms_user_code"].ToString();
                            simsobj.sims_comm_bcc_sms_user_code_status = dr["sims_comm_bcc_sms_user_code_status"].ToString().Equals("A") ? true : false;
                            user_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, user_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, user_list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, user_list);

            }
            //return Request.CreateResponse(HttpStatusCode.OK, user_list);
        }

        [Route("CUDModeratorUser")]
        public HttpResponseMessage CUDModeratorUser(List<Sims165> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDModeratorUser(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "CUDModeratorUser"));


            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims165 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("sims.sims_moderator_user_proc",
                            new List<SqlParameter>() 
                            {                             
                                    new SqlParameter("@opr", simsobj.opr),
                                    new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                    new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                    new SqlParameter("@sims_comm_name", simsobj.sims_comm_code),
                                    new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                    new SqlParameter("@sims_comm_user_name", simsobj.sims_comm_user_name),                                    
                                    new SqlParameter("@sims_comm_user_code_status", simsobj.sims_comm_user_code_status==true?"A":"I"),                                   
                                    new SqlParameter("@sims_comm_email_user_code", simsobj.sims_comm_email_user_code),                                    
                                    new SqlParameter("@sims_comm_email_user_code_status", simsobj.sims_comm_email_user_code_status==true?"A":"I"),                                   
                                    new SqlParameter("@sims_comm_sms_user_code", simsobj.sims_comm_sms_user_code),                                   
                                    new SqlParameter("@sims_comm_sms_user_code_status", simsobj.sims_comm_sms_user_code_status==true?"A":"I"),                                    
                                    new SqlParameter("@sims_comm_bcc_user_code", simsobj.sims_comm_bcc_user_code),                                    
                                    new SqlParameter("@sims_comm_bcc_user_code_status", simsobj.sims_comm_bcc_user_code_status==true?"A":"I"),                                    
                                    new SqlParameter("@sims_comm_bcc_email_user_code", simsobj.sims_comm_bcc_email_user_code),                                    
                                    new SqlParameter("@sims_comm_bcc_email_user_code_status", simsobj.sims_comm_bcc_email_user_code_status==true?"A":"I"),                                    
                                    new SqlParameter("@sims_comm_bcc_sms_user_code", simsobj.sims_comm_bcc_sms_user_code),
                                    new SqlParameter("@sims_comm_bcc_sms_user_code_status", simsobj.sims_comm_bcc_sms_user_code_status==true?"A":"I"),                                    
                            });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}