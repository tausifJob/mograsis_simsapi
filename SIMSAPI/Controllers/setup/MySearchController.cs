﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.hrms;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/mysearch")]
    public class MySearchController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("CMySearch")]
        public HttpResponseMessage CMySearch(Sims507_SrchQry obj)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : CMySearch(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;
            string srchcode = string.Empty;
            try
            {
                if (obj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),                                
                                new SqlParameter("@sims_search_desc", obj.sims_search_desc),
                                new SqlParameter("@sims_search_short_name", obj.sims_search_short_name),
                                new SqlParameter("@sims_search_status", obj.sims_search_status)
                         });

                        if (dr.Read())
                        {
                            srchcode = dr["SearchCode"].ToString();

                            message.strMessage = srchcode;
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information.";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("CMySearch")]
        private HttpResponseMessage CMySearchUsers(string sims_search_code, string users, string sims_search_query, string sims_search_condition)
        {
            string debug = "MODULE :{0}, APPLICATION :{1}, METHOD : CMySearch(), PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            Message message = new Message();
            bool inserted = false;

            try
            {               
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),                                
                                new SqlParameter("@sims_search_user_code", sims_search_code),
                                new SqlParameter("@sims_search_query", sims_search_query),
                                new SqlParameter("@sims_search_condition", sims_search_condition)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            message.strMessage = "Record Inserted Sucessfully.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        if (dr.RecordsAffected < 0)
                        {
                            message.strMessage = "Record Not Inserted.";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }                
            }
            catch (Exception x)
            {
                Log.Error(x);                
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        }

        [Route("GetgradesAll")]
        public HttpResponseMessage GetgradesAll()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetgradesAll(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims507_SrchQry> lst_searchgrades = new List<Sims507_SrchQry>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "M"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims507_SrchQry simsobj = new Sims507_SrchQry();
                            simsobj.gradecode = dr["sims_grade_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            lst_searchgrades.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_searchgrades);
        }

        [Route("getSearchGradesSections")]
        public HttpResponseMessage getSearchGradesSections(string gradecode) 
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchGradesSections(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims507_SrchQry> lst_searchgradessections = new List<Sims507_SrchQry>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@sims_grade_code", gradecode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims507_SrchQry simsobj = new Sims507_SrchQry();
                            simsobj.sims_section_name_en = dr["sims_grade_name_en"].ToString() + "-" + dr["sims_section_name_en"].ToString();                            
                            simsobj.sectioncode = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            lst_searchgradessections.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_searchgradessections);
        }

        [Route("getSearchReligion")]
        public HttpResponseMessage getSearchReligion()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchReligion(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims507_SrchQry> lst_searchreligion = new List<Sims507_SrchQry>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "N"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims507_SrchQry simsobj = new Sims507_SrchQry();
                            simsobj.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                            lst_searchreligion.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_searchreligion);
        }

        [Route("getSearchNationality")]
        public HttpResponseMessage getSearchNationality()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchNationality(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims507_SrchQry> lst_searchnationality = new List<Sims507_SrchQry>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "O"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims507_SrchQry simsobj = new Sims507_SrchQry();
                            simsobj.sims_nationality_name_en = dr["sims_nationality_name_en"].ToString();
                            lst_searchnationality.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_searchnationality);
        }

        [Route("getSearchSubject")]
        public HttpResponseMessage getSearchSubject()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchSubject(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims507_SrchQry> lst_searchsubject = new List<Sims507_SrchQry>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_SearchQuery_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "Q"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims507_SrchQry simsobj = new Sims507_SrchQry();
                            simsobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            lst_searchsubject.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lst_searchsubject);
        }

    }
}
