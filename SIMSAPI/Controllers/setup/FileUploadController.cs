﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web.Configuration;
using System.Web;
using SIMSAPI.Helper;
using System.Data.SqlClient;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/file")]
    public class FileUpload1Controller : ApiController
    {
        List<string> uploadedFiles = new List<string>();
        string oldftppath = string.Empty;
        string uname = string.Empty;
        string pass = string.Empty;
        string value4 = string.Empty;

        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        string type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + type;
                        hpf.SaveAs((root + filename + "." + type));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (location == "CircularFiles")
                getOldPathToUpload_circular();
            if (location == "NewsFiles")
                getOldPathToUpload_news();

            //Code to save path to Silverlight Path(to access from mobile app-------------------------------)
            string sourcefilepath = root + file;

            string ftpusername = "";
            string ftppassword = "";
            //if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")
            //{
            ftpusername = uname; // e.g. username
            ftppassword = pass; // e.g. password
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")
                {
                    serverpath = oldftppath;
                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            //------------------------------------------------------------------------------------------------

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }



        [HttpPost(), Route("uploadsyllabus")]
        public string uploadsyllabus(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));

                        var etype = hpf.FileName.Split('.')[1];
                        string type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + etype;
                        hpf.SaveAs((root + filename + "." + etype));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (location == "CircularFiles")
                getOldPathToUpload_circular();
            if (location == "NewsFiles")
                getOldPathToUpload_news();

            //Code to save path to Silverlight Path(to access from mobile app-------------------------------)
            string sourcefilepath = root + file;

            string ftpusername = "";
            string ftppassword = "";
            //if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")
            //{
            ftpusername = uname; // e.g. username
            ftppassword = pass; // e.g. password
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")
                {
                    serverpath = oldftppath;
                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            //------------------------------------------------------------------------------------------------

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }


        [HttpPost(), Route("uploadPNG")]
        public string UploadFilesPNG(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        string type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + "png";
                        hpf.SaveAs((root + filename + "." + "png"));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (location == "CircularFiles")
                getOldPathToUpload_circular();
            if (location == "NewsFiles")
                getOldPathToUpload_news();

            //Code to save path to Silverlight Path(to access from mobile app-------------------------------)
            string sourcefilepath = root + file;

            string ftpusername = "";
            string ftppassword = "";
            //if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")
            //{
            ftpusername = uname; // e.g. username
            ftppassword = pass; // e.g. password
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")
                {
                    serverpath = oldftppath;
                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            //------------------------------------------------------------------------------------------------

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }






        //For Student Image View From 
        [HttpPost(), Route("Imageupload")]
        public string Imageupload(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string root = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));

                        var etype = "png";//hpf.FileName.Split('.')[1];
                        string type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + etype;
                        hpf.SaveAs((root + filename + "." + etype));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (location == "CircularFiles")
                getOldPathToUpload_circular();
            if (location == "NewsFiles")
                getOldPathToUpload_news();

            //Code to save path to Silverlight Path(to access from mobile app-------------------------------)
            string sourcefilepath = root + file;

            string ftpusername = "";
            string ftppassword = "";
            //if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")
            //{
            ftpusername = uname; // e.g. username
            ftppassword = pass; // e.g. password
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")
                {
                    serverpath = oldftppath;
                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            //------------------------------------------------------------------------------------------------

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }




        //Circular & News File Upload
        [HttpPost(), Route("upload_file")]
        public string UploadFiles(string filename, string location, string value, string update)
        {
            int iUploadedCnt = 0;
            string sPath = ""; string file = "";
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            //  HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            string root = HttpContext.Current.Server.MapPath(path);

            Directory.CreateDirectory(root);

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            List<string> lstFiles = new List<string>();
            if (!string.IsNullOrEmpty(value))
                lstFiles = value.Substring(1, value.Length - 1).Split(',').ToList();
            string crt = filename + "_";
            int ext_file = 0;
            for (int jk = 0; jk < lstFiles.Count; jk++)
            {
                if (lstFiles[jk].ToString().Contains(crt))
                {
                    ext_file = ext_file + 1;
                }
            }

            try
            {
                for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
                {
                    System.Web.HttpPostedFile hpf = hfc[iCnt];

                    if (lstFiles.Contains(hpf.FileName) && !(uploadedFiles.Contains(hpf.FileName)))
                    {
                        // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                        if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                        {
                            string type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                            file = filename + "." + type;
                            if (update == "Y")
                            {
                                iUploadedCnt = ext_file;
                            }
                            string fileupload_name = root + filename + "_" + (iUploadedCnt + 1) + "." + type;
                            hpf.SaveAs((fileupload_name));
                            iUploadedCnt = iUploadedCnt + 1;
                            uploadedFiles.Add(hpf.FileName);
                        }
                    }
                }
            }
            catch (Exception de)
            { }

            if (location.Contains("CircularFiles"))
                UCircular(lstFiles, filename);
            else if (location.Contains("NewsFiles"))
                UNews(lstFiles, filename);

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        //Circular File Path Update
        private void UCircular(List<string> lstFiles, string circular_number)
        {
            bool updated = false;
            string file1 = "", file2 = "", file3 = "";
            try
            {
                if (lstFiles.Count > 0)
                {
                    file1 = circular_number + "_1" + (lstFiles[0].Substring(lstFiles[0].LastIndexOf('.'))).ToString();
                    try
                    {
                        if (!string.IsNullOrEmpty(lstFiles[1].ToString()))
                            file2 = circular_number + "_2" + (lstFiles[1].Substring(lstFiles[1].LastIndexOf('.'))).ToString();
                    }
                    catch (Exception de)
                    { }
                    try
                    {
                        if (!string.IsNullOrEmpty(lstFiles[2].ToString()))
                            file3 = circular_number + "_3" + (lstFiles[2].Substring(lstFiles[2].LastIndexOf('.'))).ToString();
                    }
                    catch (Exception de)
                    { }
                }
            }
            catch (Exception de)
            { }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "J"),                                
                                new SqlParameter("@sims_circular_number", circular_number),
                                new SqlParameter("@sims_circular_file_path1", file1),
                                new SqlParameter("@sims_circular_file_path2", file2),
                                new SqlParameter("@sims_circular_file_path3", file3),
                         });
                    if (dr.RecordsAffected > 0)
                        updated = true;
                    else
                        updated = false;
                }
            }
            catch (Exception x)
            {
            }
        }

        //News File Path Update
        private void UNews(List<string> lstFiles, string news_number)
        {
            bool updated = false;
            string file1 = "", file2 = "", file3 = "";

            try
            {
                if (lstFiles.Count > 0)
                {
                    file1 = news_number + "_1" + (lstFiles[0].Substring(lstFiles[0].LastIndexOf('.'))).ToString();
                    try
                    {
                        if (!string.IsNullOrEmpty(lstFiles[1].ToString()))
                            file2 = news_number + "_2" + (lstFiles[1].Substring(lstFiles[1].LastIndexOf('.'))).ToString();
                    }
                    catch (Exception de)
                    { }
                    try
                    {
                        if (!string.IsNullOrEmpty(lstFiles[2].ToString()))
                            file3 = news_number + "_3" + (lstFiles[2].Substring(lstFiles[2].LastIndexOf('.'))).ToString();
                    }
                    catch (Exception de)
                    { }
                }
            }
            catch (Exception de)
            { }
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_news", new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "J"),                                
                                new SqlParameter("@sims_news_number", news_number),
                                new SqlParameter("@sims_news_file_path1", file1),
                                new SqlParameter("@sims_news_file_path2", file2),
                                new SqlParameter("@sims_news_file_path3", file3),
                         });
                    if (dr.RecordsAffected > 0)
                        updated = true;
                    else
                        updated = false;
                }
            }
            catch (Exception x)
            {
            }
        }

        [HttpPost(), Route("upload_file_assignment")]
        public string UploadFiles(string filename, string location, string value)
        {
            int iUploadedCnt = 0;

            string sPath = ""; string file = "";
            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);

            Directory.CreateDirectory(root);

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            List<string> lstFiles = new List<string>();
            if (!string.IsNullOrEmpty(value))
                lstFiles = value.Substring(1, value.Length - 1).Split(',').ToList();

            for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (lstFiles.Contains(hpf.FileName) && !(uploadedFiles.Contains(hpf.FileName)))
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        string type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        string fileupload_name = root + filename + "_" + (iUploadedCnt + 1) + "." + type;
                        hpf.SaveAs((fileupload_name));
                        iUploadedCnt = iUploadedCnt + 1;
                        uploadedFiles.Add(hpf.FileName);
                    }
                }
            }

            if (location.Contains("CircularFiles"))
                UCircular(lstFiles, filename);
            else if (location.Contains("NewsFiles"))
                UNews(lstFiles, filename);

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [HttpPost(), Route("uploadCircularDocument")]
        public string uploadCircularDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            string path = "~/Content/" + schoolname + "/Images/" + location + "/";
            //string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        hpf.SaveAs((root + filename + "." + type));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (location == "CircularFiles")
                getOldPathToUpload_circular();
            if (location == "NewsFiles")
                getOldPathToUpload_news();

            //Code to save path to Silverlight Path(to access from mobile app-------------------------------)
            string sourcefilepath = root + file;

            string ftpusername = "";
            string ftppassword = "";
            //if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")
            //{
            ftpusername = uname; // e.g. username
            ftppassword = pass; // e.g. password
            //}
            //else if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "brs")
            //{
            //    ftpusername = "FTP_USR_BRS"; // e.g. username
            //    ftppassword = "Cdyd46!8"; // e.g. password
            //}

            try
            {
                var serverpath = "";
                var fileName = Path.GetFileName(sourcefilepath);
                //  if (HttpContext.Current.Request.Headers["schoolId"].ToString() == "adis")// || HttpContext.Current.Request.Headers["schoolId"].ToString() == "adisw")
                {
                    serverpath = oldftppath;
                }

                var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpusername, ftppassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(sourcefilepath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                Console.WriteLine("Upload done: {0}", response.StatusDescription);
                response.Close();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            //------------------------------------------------------------------------------------------------

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [HttpPost(), Route("uploadmuti")]
        public List<string> uploadmuti(string filename, string location, string value, string delimages)
        {
            List<string> uploded_files = new List<string>();

            int iUploadedCnt = 0;
            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string file = "";
            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";
            //string path = "~/Content/" + "Images/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);

            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            List<string> lstFiles = new List<string>();
            if (!string.IsNullOrEmpty(value))
                lstFiles = value.Substring(1, value.Length - 1).Split(',').ToList();
            lstFiles = lstFiles.Distinct().ToList();
            for (int iCnt = 0; iCnt < lstFiles.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (lstFiles.Contains(hpf.FileName) && !(uploadedFiles.Contains(hpf.FileName)))
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        string type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        string date = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;
                        string fileupload_name = root + filename + "_" + (iUploadedCnt + 1) + date + "." + type;
                        uploded_files.Add(filename + "_" + (iUploadedCnt + 1) + date + "." + type);
                        hpf.SaveAs((fileupload_name));
                        iUploadedCnt = iUploadedCnt + 1;
                        uploadedFiles.Add(hpf.FileName);
                    }
                }
            }

            //foreach (string file_nm in uploded_files)
            //{
            //    string sourcefilepath = root + file_nm; // e.g. “d:/test.docx”
            //    string ftpusername = "support"; // e.g. username
            //    string ftppassword = "Ws0x$6n9"; // e.g. password

            //    //string ftpusername = ""; // comment

            //    try
            //    {
            //        var fileName = Path.GetFileName(sourcefilepath);
            //        var serverpath = "ftp://sjs.mograsys.com/" + "sjs" + ".mograsys.com/" + "Images" + "/PhotoGallery/";
            //        var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

            //        request.Method = WebRequestMethods.Ftp.UploadFile;
            //        request.Credentials = new NetworkCredential(ftpusername, ftppassword);
            //        request.UsePassive = true;
            //        request.UseBinary = true;
            //        request.KeepAlive = false;

            //        using (var fileStream = File.OpenRead(sourcefilepath))
            //        {
            //            using (var requestStream = request.GetRequestStream())
            //            {
            //                fileStream.CopyTo(requestStream);
            //                requestStream.Close();
            //            }
            //        }

            //        var response = (FtpWebResponse)request.GetResponse();
            //        Console.WriteLine("Upload done: {0}", response.StatusDescription);
            //        response.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        //Console.WriteLine(ex.Message);
            //    }
            //}

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return uploded_files;
            }
            else
            {
                return uploded_files;
            }
        }

        [HttpPost(), Route("uploadDocument")]
        public string uploadDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];




            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + type;
                        //file = filename + ".png";
                        hpf.SaveAs((root + filename + "." + type));
                        //  hpf.SaveAs((root + filename + ".png" ));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;



                    }
                }
            }



            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }



        [HttpPost(), Route("uploadDocument_new")]
        public string uploadDocument_new(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];




            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);
                    string type_exe = string.Empty;
                    var file_type = s.Split('.');
                    if (file_type.Length > 0)
                    {
                        type_exe = file_type[1];
                    }

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + type_exe;
                        //file = filename + ".png";
                        hpf.SaveAs((root + filename + "." + type_exe));
                        //  hpf.SaveAs((root + filename + ".png" ));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;



                    }
                }
            }



            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }



        private void getOldPathToUpload_circular()
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A1"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            value4 = dr["sims_appl_form_field_value4"].ToString();
                            oldftppath = dr["sims_appl_form_field_value1"].ToString() + value4;
                            uname = dr["sims_appl_form_field_value2"].ToString();
                            pass = dr["sims_appl_form_field_value3"].ToString();
                        }
                    }

                }
            }
            catch (Exception x)
            {
            }
        }

        private void getOldPathToUpload_news()
        {
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_circular_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A2"),
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            value4 = dr["sims_appl_form_field_value4"].ToString();
                            oldftppath = dr["sims_appl_form_field_value1"].ToString() + value4;
                            uname = dr["sims_appl_form_field_value2"].ToString();
                            pass = dr["sims_appl_form_field_value3"].ToString();
                        }
                    }

                }
            }
            catch (Exception x)
            {
            }
        }

        [HttpPost(), Route("uploadOrderDocument")]
        public string uploadOrderDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            //Original
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";


            //For local
            //string path = "~/Content/" + "/Images/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        hpf.SaveAs((root + filename + "." + type));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }


       
        [HttpPost(),Route("uploadmutiImageNew") ]
        public List<string> uploadmutiImageNew(string filename, string location)
        {
            List<string> imgname = new List<string>();
            string img = "";
         try
            {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
                {
                    var httpPostedFile = hfc[iCnt];
                    if (httpPostedFile != null)
                    {
                        // Get the complete file path
                        img = filename + iCnt + ".png";
                        imgname.Add(img);
                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/"), img);
                        httpPostedFile.SaveAs(fileSavePath);
                    }
                }
            }
            return imgname;            
         } catch (Exception e)
         {
             return imgname;
         }
        }


        [HttpPost(), Route("uploadmultiple")]
        public List<string> uploadmultiple(string filename, string location, string value, string delimages)
        {
            List<string> uploded_files = new List<string>();

            int iUploadedCnt = 0;
            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string file = "";
            //sPath = WebConfigurationManager.AppSettings["webPath"];

            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";
            //string path = "~/Content/" + "Images/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);

            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            List<string> lstFiles = new List<string>();
            if (!string.IsNullOrEmpty(value))
                lstFiles = value.Substring(0, value.Length - 1).Split(',').ToList();
            lstFiles = lstFiles.Distinct().ToList();
            for (int iCnt = 0; iCnt < lstFiles.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (lstFiles.Contains(hpf.FileName) && !(uploadedFiles.Contains(hpf.FileName)))
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        string type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        //    string date = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;
                        string fileupload_name = root + hpf.FileName; //+ "." + type;
                        //uploded_files.Add(hpf.FileName + "." + type);
                        uploded_files.Add(hpf.FileName);
                        hpf.SaveAs((fileupload_name));
                        iUploadedCnt = iUploadedCnt + 1;
                        uploadedFiles.Add(hpf.FileName);
                    }
                }
            }

            //foreach (string file_nm in uploded_files)
            //{
            //    string sourcefilepath = root + file_nm; // e.g. “d:/test.docx”
            //    string ftpusername = "support"; // e.g. username
            //    string ftppassword = "Ws0x$6n9"; // e.g. password

            //    //string ftpusername = ""; // comment

            //    try
            //    {
            //        var fileName = Path.GetFileName(sourcefilepath);
            //        var serverpath = "ftp://sjs.mograsys.com/" + "sjs" + ".mograsys.com/" + "Images" + "/PhotoGallery/";
            //        var request = (FtpWebRequest)WebRequest.Create(serverpath + fileName);

            //        request.Method = WebRequestMethods.Ftp.UploadFile;
            //        request.Credentials = new NetworkCredential(ftpusername, ftppassword);
            //        request.UsePassive = true;
            //        request.UseBinary = true;
            //        request.KeepAlive = false;

            //        using (var fileStream = File.OpenRead(sourcefilepath))
            //        {
            //            using (var requestStream = request.GetRequestStream())
            //            {
            //                fileStream.CopyTo(requestStream);
            //                requestStream.Close();
            //            }
            //        }

            //        var response = (FtpWebResponse)request.GetResponse();
            //        Console.WriteLine("Upload done: {0}", response.StatusDescription);
            //        response.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        //Console.WriteLine(ex.Message);
            //    }
            //}

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return uploded_files;
            }
            else
            {
                return uploded_files;
            }
        }

        [HttpPost(), Route("uploadCampDocument")]
        public string uploadCampDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();

            //Original
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";


            //For local
            //string path = "~/Content/" + "/Images/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        file = filename + "." + type;
                        hpf.SaveAs((root + filename + "." + type));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

    }
}
