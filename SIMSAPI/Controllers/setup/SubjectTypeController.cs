﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/SubjectType")]
    public class Sim047Controller : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSubjectType")]
        public HttpResponseMessage getSubjectType()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "subjectType", "ATTENDANCECRITERIA"));
            List<Sims047> subject_type = new List<Sims047>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_type_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Sims047 simsobj = new Sims047();
                            simsobj.sims_attendance_cur_code = dr["cur_code"].ToString();
                            simsobj.sims_cur_name = dr["sims_short_cur_name"].ToString();
                            simsobj.subject_type_code = dr["sims_subject_type_code"].ToString();
                            simsobj.subject_type_name_eng = dr["sims_subject_type_name_en"].ToString();
                            simsobj.subject_type_name_ar = dr["sims_subject_type_name_ar"].ToString();
                            simsobj.subject_type_name_fr = dr["sims_subject_type_name_fr"].ToString();
                            simsobj.subject_type_name_ot = dr["sims_subject_type_name_ot"].ToString();
                            subject_type.Add(simsobj);
                            // total = subject_type.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_type);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_type);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, subject_type);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("CUDSubjectType")]
        public HttpResponseMessage CUDSubjectType(List<Sims047> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubjectType(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "STUDENT", "SUBJECTTYPE"));
            bool inserted = false;
            //Sims047 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims047>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims047 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_subject_type_proc]",
                                new List<SqlParameter>() 
                         {                             
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@cur_name", simsobj.sims_attendance_cur_code),
                                new SqlParameter("@sims_subject_type_code", simsobj.subject_type_code),
                                new SqlParameter("@sims_subject_type_name_en", simsobj.subject_type_name_eng),
                                new SqlParameter("@sims_subject_type_name_ar", simsobj.subject_type_name_ar),
                                new SqlParameter("@sims_subject_type_name_fr", simsobj.subject_type_name_fr),
                                new SqlParameter("@sims_subject_type_name_ot", simsobj.subject_type_name_ot)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CheckSubjectType")]
        public HttpResponseMessage CheckSubjectType(string SubjectType, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckSubjectType(),PARAMETERS :: SUBJECTTYPE{2},cur_code{3}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckSubjectType", SubjectType, cur_code));

            bool ifexists = false;
            List<Sims047> Subject = new List<Sims047>();
            Sims047 simsobj = new Sims047();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_type_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_subject_type_code", SubjectType),
                                new SqlParameter("@cur_name", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {

                    simsobj.status = true;
                    simsobj.strMessage = "Subject Type Already Exists";
                    Subject.Add(simsobj);
                }
                else
                {
                    simsobj.status = false;
                    simsobj.strMessage = "No Records Found";
                    Subject.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj);
            }
        }
    }
}