﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/CurLevelGrade")]
    [BasicAuthentication]
    public class CurriculumLevelGradeController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCurLevelGrade")]
        public HttpResponseMessage getCurLevelGrade()
        {
            List<Sims005> curlgrade_list = new List<Sims005>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_level_grade_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr","S")
                                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims005 simsobj = new Sims005();
                            simsobj.level_grade_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.level_grade_level_code = dr["sims_cur_level_code"].ToString();
                            simsobj.level_grade_level_code_name = dr["sims_cur_level_name_en"].ToString();
                            simsobj.level_grade_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.level_grade_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.level_grade_grade_code_name = dr["sims_grade_name_en"].ToString();
                            simsobj.level_grade_cur_short_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            curlgrade_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, curlgrade_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, curlgrade_list);
        }

        [Route("getCuriculumLevel")]
        public HttpResponseMessage getCuriculum(string cur_code)
        {
            List<Sims005> lstCuriculum = new List<Sims005>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_level_grade_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@cur_code",cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims005 sequence = new Sims005();
                            sequence.sims_cur_level_code = dr["sims_cur_level_code"].ToString();
                            sequence.sims_cur_level_name_en = dr["sims_cur_level_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("CUDCurGradeLevel")]
        public HttpResponseMessage CUDCurGradeLevel(List<Sims005> data)
        {
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims005 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_cur_level_grade_proc]",
                            new List<SqlParameter>() 
                         { 
		 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@cur_code", simsobj.sims_cur_code),
                                new SqlParameter("@level_code", simsobj.sims_cur_level_code),
                                new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                new SqlParameter("@grade_code", simsobj.sims_grade_code)
                                
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}