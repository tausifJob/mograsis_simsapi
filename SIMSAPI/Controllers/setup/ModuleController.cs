﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;
using SIMSAPI.Models;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;



namespace SIMSRestAPI.Controllers.ERP
{
    [RoutePrefix("api/ModuleMaster")]

    public class ModuleController : ApiController
    {
        #region(Module)

        [Route("getModulesByIndex")]
        public HttpResponseMessage getModulesByIndex()
        {

            List<Comn003> lstSequence = new List<Comn003>();

            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_module",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn003 sequence = new Comn003();
                            sequence.module_code = dr["comn_mod_code"].ToString();
                            sequence.module_name_en = dr["comn_mod_name_en"].ToString();
                            sequence.module_name_ar = dr["comn_mod_name_ar"].ToString();
                            sequence.module_name_fr = dr["comn_mod_name_fr"].ToString();
                            sequence.module_name_ot = dr["comn_mod_name_ot"].ToString();
                            sequence.module_create_date = db.UIDDMMYYYYformat( dr["comn_mod_create_date"].ToString());
                            sequence.module_short_desc = dr["comn_mod_short_description"].ToString();
                            sequence.module_desc = dr["comn_mod_description"].ToString();
                            sequence.module_keywords = dr["comn_mod_keywords"].ToString();
                            sequence.module_status = dr["comn_mod_status"].ToString().Equals("A") ? true : false; ;
                            sequence.module_version = dr["comn_mod_version"].ToString();
                            sequence.module_color = dr["comn_mod_color"].ToString();
                            sequence.module_location = dr["comn_mod_location"].ToString();
                            sequence.module_dislpay_order = dr["comn_mod_display_order"].ToString();
                            lstSequence.Add(sequence);

                        }
                    }

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstSequence);

        }

        [Route("CUDupdateModule")]
        public HttpResponseMessage CUDupdateModule(Comn003 obj)
        {
            bool inserted = false;
            Comn003 module = obj;
            Message message = new Message();

            try
            {
                if (module != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("comn_module",
                            new List<SqlParameter>() 
                         { 
                             //new SqlParameter("@opr",'U'),
                              new SqlParameter("@opr", module.opr),
                             new SqlParameter("@comn_mod_code", module.module_code),
                             new SqlParameter("@comn_mod_name_en",module.module_name_en),
                             new SqlParameter("@comn_mod_name_ar", module.module_name_ar),
                             new SqlParameter("@comn_mod_name_fr", module.module_name_fr),
                             new SqlParameter("@comn_mod_name_ot", module.module_name_ot),
                             new SqlParameter("@comn_mod_ALTER_date", db.DBYYYYMMDDformat( module.module_create_date)),
                             new SqlParameter("@comn_mod_short_description", module.module_short_desc),
                             new SqlParameter("@comn_mod_description", module.module_desc),
                             new SqlParameter("@comn_mod_keywords", module.module_keywords),
                             new SqlParameter("@comn_mod_status", module.module_status==true?"A":"I"),
                             new SqlParameter("@comn_mod_version", module.module_version),
                             new SqlParameter("@comn_mod_color", module.module_color),
                             new SqlParameter("@comn_mod_location", module.module_location),
                             new SqlParameter("@comn_mod_display_order", module.module_dislpay_order)

                         });

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion
    }
}