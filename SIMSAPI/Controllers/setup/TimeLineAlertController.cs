﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/TimeLineAlert")]
    public class TimeLineAlertController:ApiController
    {
        [Route("getUserAlert")]
        public HttpResponseMessage getUserAlert(string uname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserAudit(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "Common", "getUserAudit"));

            List<AuditAlert> goaltarget_list = new List<AuditAlert>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[comn_alert_transaction]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@userName", uname),

                            
                            //new SqlParameter("@comn_mod_code","015"),
                            //new SqlParameter("@comn_appl_code","Sim070"),
                          //  new SqlParameter("@comn_audit_start_time","04/22/2015"),
                           // new SqlParameter("@comn_audit_end_time","04/22/2016"),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            AuditAlert simsobj = new AuditAlert();
                            simsobj.comn_alert_number = dr["comn_alert_number"].ToString();
                            simsobj.comn_alert_user_code = dr["comn_alert_user_code"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_alert_message = dr["comn_alert_message"].ToString();
                            simsobj.comn_alert_status = dr["comn_alert_status"].ToString();
                            simsobj.comn_alert_type_code = dr["comn_alert_type_code"].ToString();
                            simsobj.comn_alert_priority_code = dr["comn_alert_priority_code"].ToString();
                            simsobj.comn_alert_date = dr["comn_alert_date"].ToString();
                            simsobj.st_time = dr["st_time"].ToString();
                            simsobj.st_am = dr["st_am"].ToString();
                            simsobj.st_date = dr["st_date"].ToString();




        goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        }

        [Route("getprojectName")]
        public HttpResponseMessage getprojectName()
        {
            List<Mograversion> mod_list = new List<Mograversion>();
          
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograsisVersion_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'P'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mograversion simsobj = new Mograversion();

                            simsobj.comn_appl_project_code = dr["sims_appl_parameter"].ToString();
                            simsobj.comn_appl_project_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getModuleName")]
        public HttpResponseMessage getModuleName()
        {
            List<Mograversion> mod_list = new List<Mograversion>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograsisVersion_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'M'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mograversion simsobj = new Mograversion();

                            simsobj.comn_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name_e = dr["comn_mod_name_en"].ToString();
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getProjectApplications")]
        public HttpResponseMessage getProjectApplications(string mod_code)
        {
            List<Mograversion> lst = new List<Mograversion>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograsisVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "MA"),
                        new SqlParameter("@ModuleCode", mod_code),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mograversion pn = new Mograversion();
                            pn.comn_appl_code = dr["comn_appl_code"].ToString();
                            pn.comn_appl_name = dr["comn_appl_name_en"].ToString();
                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("getinnerChangesetData")]
        public HttpResponseMessage getinnerChangesetData(string change_setNo,string pro_name,string dev_name, string appl_code)
        {
            List<Mograversion> lst = new List<Mograversion>();
            if (change_setNo == "undefined")
            {
                change_setNo = null;
            }
            if (pro_name == "undefined")
            {
                pro_name = null;
            }
            if (dev_name == "undefined")
            {
                dev_name = null;
            }
            if (appl_code == "undefined")
            {
                appl_code = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograsisVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "S"),
                        new SqlParameter("@ChangesetNo", change_setNo),
                        new SqlParameter("@ProjectName", pro_name),
                        new SqlParameter("@DeveloperName", dev_name),
                        new SqlParameter("@Applicationcode", appl_code),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mograversion pn = new Mograversion();

                            pn.lic_changeset_no= dr["lic_changeset_no"].ToString();
                            pn.lic_appl_code= dr["lic_appl_code"].ToString();
                            pn.lic_appl_name = dr["lic_appl_name"].ToString();
                            pn.lic_sp_name= dr["lic_sp_name"].ToString();
                            pn.lic_sp_remark= dr["lic_sp_remark"].ToString();
                            pn.lic_remark= dr["lic_remark"].ToString();
                            pn.lic_document = dr["lic_document"].ToString();
                            pn.lic_test_case= dr["lic_test_case"].ToString();
                            pn.lic_approve_by = dr["lic_approve_by"].ToString();

                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }


        [Route("CUDVersionUpdate")]
        public HttpResponseMessage CUDVersionUpdate(List<Mograversion> data)
        {
            bool insert = false;
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Mograversion simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[mogra].[mograsisVersion_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", 'R'),
                                new SqlParameter("@approveby",simsobj.approved_by),
                                new SqlParameter("@ChangesetNo", simsobj.lic_changeset_no),
                                new SqlParameter("@Applicationcode", simsobj.lic_appl_code),
                                new SqlParameter("@lic_document", simsobj.doccheck==true?"A":"I"),
                                new SqlParameter("@lic_test_case", simsobj.testcheck==true?"A":"I"),

                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

        [Route("getinnerDeployedChangesetData")]
        public HttpResponseMessage getinnerDeployedChangesetData(string change_setNo, string pro_name, string dev_name, string appl_code)
        {
            List<Mograversion> lst = new List<Mograversion>();
            if (change_setNo == "undefined")
            {
                change_setNo = null;
            }
            if (pro_name == "undefined")
            {
                pro_name = null;
            }
            if (dev_name == "undefined")
            {
                dev_name = null;
            }
            if (appl_code == "undefined")
            {
                appl_code = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[mogra].[mograsisVersion_proc]",
                    new List<SqlParameter>() {
                        new SqlParameter("@opr", "SS"),
                        new SqlParameter("@ChangesetNo", change_setNo),
                        new SqlParameter("@ProjectName", pro_name),
                        new SqlParameter("@DeveloperName", dev_name),
                        new SqlParameter("@Applicationcode", appl_code),
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Mograversion pn = new Mograversion();

                            pn.lic_changeset_no = dr["lic_changeset_no"].ToString();
                            pn.lic_appl_code = dr["lic_appl_code"].ToString();
                            pn.lic_appl_name = dr["lic_appl_name"].ToString();
                            pn.lic_sp_name = dr["lic_sp_name"].ToString();
                            pn.lic_sp_remark = dr["lic_sp_remark"].ToString();
                            pn.lic_remark = dr["lic_remark"].ToString();
                            pn.lic_document = dr["lic_document"].ToString();
                            pn.lic_test_case = dr["lic_test_case"].ToString();
                            pn.lic_approve_by = dr["lic_approve_by"].ToString();

                            lst.Add(pn);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lst);
                    }
                }
            }
            catch (Exception ex)
            { }
            return Request.CreateResponse(HttpStatusCode.OK, lst);
        }

        [Route("CUDDeployedVersionUpdate")]
        public HttpResponseMessage CUDDeployedVersionUpdate(List<Mograversion> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Mograversion simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[mogra].[mograsisVersion_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", "DD"),
                                new SqlParameter("@lic_version_number",simsobj.lic_version_number),
                                new SqlParameter("@lic_release_number",simsobj.lic_release_number),
                                new SqlParameter("@lic_patch_number",simsobj.lic_patch_number),

                                new SqlParameter("@ChangesetNo", simsobj.lic_changeset_no),
                                new SqlParameter("@Applicationcode", simsobj.lic_appl_code),
                                new SqlParameter("@lic_deployed_by", simsobj.doccheck==true?"Y":"N"),
                              
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getApplicatioName")]
        public HttpResponseMessage getApplicatioName(string mod_code, string appl_type)
        {
            List<masterconfig> mod_list = new List<masterconfig>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_master_configuration_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'P'),
                            new SqlParameter("@comn_mod_code",mod_code),
                            new SqlParameter("@comn_appl_type",appl_type),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            masterconfig simsobj = new masterconfig();
                            
                            simsobj.comn_mod_code= dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name_en= dr["comn_mod_name_en"].ToString();
                            simsobj.comn_appl_code= dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_name_en= dr["comn_appl_name_en"].ToString();
                            simsobj.comn_sequence_code= dr["comn_sequence_code"].ToString();
                            simsobj.comn_description= dr["comn_description"].ToString();
                            simsobj.comn_cur_code= dr["comn_cur_code"].ToString();
                            simsobj.comn_academic_year= dr["comn_academic_year"].ToString();
                            simsobj.comn_character_count= dr["comn_character_count"].ToString();
                            simsobj.comn_character_sequence= dr["comn_character_sequence"].ToString();
                            simsobj.comn_number_count= dr["comn_number_count"].ToString();
                            simsobj.comn_number_sequence= dr["comn_number_sequence"].ToString();
                            simsobj.comn_parameter_ref = dr["comn_parameter_ref"].ToString();

                           
                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("getReportURL")]
        public HttpResponseMessage getReportURL()
        {
            List<masterconfig> mod_list = new List<masterconfig>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_master_configuration_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'Q'),
                           

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            masterconfig simsobj = new masterconfig();
                            simsobj.sims_mod_code= dr["sims_mod_code"].ToString();
		                    simsobj.sims_appl_code= dr["sims_appl_code"].ToString();
		                    simsobj.sims_appl_form_field= dr["sims_appl_form_field"].ToString();
		                    simsobj.sims_appl_parameter= dr["sims_appl_parameter"].ToString();
		                    simsobj.sims_appl_form_field_value1= dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_appl_form_field_value2 = dr["sims_appl_form_field_value2"].ToString();

                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("UpdateMatrConfig")]
        public HttpResponseMessage UpdateMatrConfig(List<masterconfig> data)
        {
            bool insert = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (masterconfig simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_master_configuration_proc]",

                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@comn_character_count",simsobj.comn_character_count),
                                new SqlParameter("@comn_character_sequence",simsobj.comn_character_sequence),
                                new SqlParameter("@comn_number_count",simsobj.comn_number_count),

                                new SqlParameter("@comn_number_sequence", simsobj.comn_number_sequence),
                                new SqlParameter("@comn_academic_year", simsobj.comn_academic_year),

                                new SqlParameter("@comn_mod_code", simsobj.comn_mod_code),
                                new SqlParameter("@comn_sequence_code", simsobj.comn_sequence_code),
                                new SqlParameter("@comn_appl_code", simsobj.comn_appl_code),
                                new SqlParameter("@comn_description", simsobj.comn_description),
                     });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


    }
}