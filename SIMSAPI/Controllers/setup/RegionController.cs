﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Setup
{
    [RoutePrefix("api/Region")]
    public class RegionController:ApiController
    {

        [Route("getRegion")]
        public HttpResponseMessage getRegion()
        {
            List<Sims057> result = new List<Sims057>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_region_proc]",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims057 obj = new Sims057();

                            obj.sims_region_code = dr["sims_region_code"].ToString();
                            obj.sims_region_name_en = dr["sims_region_name_en"].ToString();
                            obj.sims_region_name_ar = dr["sims_region_name_ar"].ToString();
                            obj.sims_region_name_fr = dr["sims_region_name_fr"].ToString();
                            obj.sims_region_name_ot = dr["sims_region_name_ot"].ToString();

                            obj.sims_status = dr["sims_status"].ToString() == "A";

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDgetRegion")]
        public HttpResponseMessage CUDgetState(List<Sims057> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDgetRegion", simsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims057 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_region_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_region_code", simsobj.sims_region_code),
                          new SqlParameter("@sims_region_name_en",simsobj.sims_region_name_en),
                          new SqlParameter("@sims_region_name_ar", simsobj.sims_region_name_ar),
                          new SqlParameter("@sims_region_name_fr", simsobj.sims_region_name_fr),
                          new SqlParameter("@sims_region_name_ot", simsobj.sims_region_name_ot),
                          new SqlParameter("@sims_region_status",simsobj.sims_status==true?"A":"I"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}