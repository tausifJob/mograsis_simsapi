﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;


namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/GlobalSearchMapping")]
    public class GlobalSearchMappingController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("insert_student_section_subject")]
        public HttpResponseMessage insert_student_section_subject(List<global_role> ob)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insert_student_section_subject(),PARAMETERS :: obj {2},Opr {2}";
            Log.Debug(string.Format(debug, "PP", "insert_student_section_subject", ob));

            Message message = new Message();
            string opr = "";

            List < SqlParameter > lst=new List<SqlParameter>();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {

                    db.Open();
                    foreach (global_role o in ob)
                {
                    lst.Clear();
                             lst.Add(new SqlParameter("@opr",  "P"));
                             lst.Add(new SqlParameter("@comn_user_name", o.comn_role_code));
                             lst.Add(new SqlParameter("@comn_user_tab", o.comn_user_tab_name));
                             int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_global_search_user_proc]", lst);

                            //if (ins > 0)
                            //{

                            //    message.strMessage = "studen section subject Updated Successfully";
                               

                            //}
                            //else
                            //{
                            //    message.strMessage = "studen section subject Not Updated ";
                            //}
                            message.strMessage = "Student section subject Updated Successfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }
                    }
                }
            
            catch (Exception x)
            {
                message.strMessage = "Error In Parsing Fee Details";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }


        [Route("getRoleName")]
        public HttpResponseMessage getRoleName()
        {
            string cur_code = string.Empty;
            string str = "";
            List<global_role> mod_list = new List<global_role>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_global_search_user_proc]",
                    new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","Q"),
                //new SqlParameter("@sims_cur_code", curcode),
                //new SqlParameter("@sims_academic_year", academicyear),
                //new SqlParameter("@sims_grade_code", gradecode),
                //new SqlParameter("@sims_section_code", section),
              
                         });
                    while (dr.Read())
                    {
                        //Sims089 newobj = new Sims089();
                        str = dr["comn_role_code"].ToString();
                        var v = (from p in mod_list where p.comn_role_code == str select p);

                        if (v.Count() == 0)
                        {
                            global_role ob = new global_role();
                            ob.tablist = new List<tab>();
                            ob.comn_role_code = str;
                            ob.comn_user_tab_name = dr["tab_name"].ToString();
                            ob.comn_role_name = dr["comn_role_name"].ToString();
                            tab sb = new tab();
                            sb.comn_user_tab_no = dr["tab_no"].ToString();
                            sb.comn_user_tab_name = dr["tab_name"].ToString();
                            sb.comn_status = dr["Status"].Equals(1) ? true : false;
                            ob.tablist.Add(sb);
                            mod_list.Add(ob);
                        }
                        else
                        {
                            tab sb = new tab();
                            sb.comn_user_tab_no = dr["tab_no"].ToString();
                            sb.comn_user_tab_name = dr["tab_name"].ToString();
                            sb.comn_status = dr["Status"].Equals(1) ? true : false;
                            v.ElementAt(0).tablist.Add(sb);
                        }
                        //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                        //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                        //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                        //newobj.sims_section_code = dr["sims_section_code"].ToString();
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getApplTab")]
        public HttpResponseMessage getApplTab(string empcode)
        {
            List<tablist> trans_list = new List<tablist>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_global_search_user_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "M"),
                            new SqlParameter("@employee_code", empcode),
                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tablist obj = new tablist();
                            obj.comn_user_tab = dr["comn_user_tab"].ToString();
                            trans_list.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, trans_list);
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, trans_list);

        }

    }
}