﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.setup
{
     [RoutePrefix("api/common/PPFieldSatus")]

    public class PPFieldStatusController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getPPFieldSatus")]
        public HttpResponseMessage getPPFieldSatus()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getPPFieldSatus()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "PPFieldSatus"));

            List<Sims501> lstSequence = new List<Sims501>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_PP_field_status_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims501 comnobj = new Sims501();
                            comnobj.sims_field_code = dr["sims_field_code"].ToString();
                            comnobj.sims_field_name = dr["sims_field_name"].ToString();
                            comnobj.sims_field_iseditable = dr["sims_field_iseditable"].ToString().Equals("Y") ? true : false;
                            comnobj.sims_field_isvisible = dr["sims_field_isvisible"].ToString().Equals("Y") ? true : false;
                            lstSequence.Add(comnobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, lstSequence);
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstSequence);
        }

        [Route("updatePPFieldSatus")]
        public HttpResponseMessage updatePPFieldSatus(string field_code, bool iseditable, bool isvisible)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDModeratorUser(),PARAMETERS :: FIELD_CODE {2},ISEDITABLE{3},ISVISIBLE{4}";
            Log.Debug(string.Format(debug, "ERP", "PPFieldSatus", field_code, iseditable, isvisible));
            bool inserted = false;
            //Sims501 comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims501>(obj);
            Message message = new Message();
            try
            {
                if (field_code != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_PP_field_status_proc",
                            new List<SqlParameter>() 
                         {                             
                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sims_field_code",field_code),
                                    new SqlParameter("@sims_field_iseditable",iseditable==true?"Y":"N"),
                                    new SqlParameter("@sims_field_isvisible",isvisible==true?"Y":"N")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }  
    }
}