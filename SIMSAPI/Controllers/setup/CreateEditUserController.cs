﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.messageClass;

namespace SIMSAPI.Controllers.CreateEditUser
{
    [RoutePrefix("api/common/User")]
    public class CreateEditUserController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getUsers")]
        public HttpResponseMessage getUsers(string data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUsers(),PARAMETERS ::NO";
            Log.Debug(string.Format(debug, "Common", "USERS", data));
            Comn005 comnobj = new Comn005();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                comnobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Comn005>(data);
            }
            List<Comn005> user_list = new List<Comn005>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_user_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@comn_user_name", comnobj.comn_user_name),
                            new SqlParameter("@comn_user_alias", comnobj.comn_user_alias),
                            new SqlParameter("@comn_user_group_name", comnobj.comn_user_group_code),
                            new SqlParameter("@comn_user_email", comnobj.comn_user_email),   

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn005 comc = new Comn005();
                            comc.comn_user_code = dr["comn_user_code"].ToString();
                            comc.comn_user_name = dr["comn_user_name"].ToString();
                            comc.comn_user_alias = dr["comn_user_alias"].ToString();
                            comc.comn_user_group_code = dr["comn_user_group_code"].ToString();
                            comc.comn_user_group_name = dr["comn_user_group_name"].ToString();
                            comc.comn_user_date_created = db.UIDDMMYYYYformat( dr["comn_user_date_created"].ToString());
                            comc.comn_user_email = dr["comn_user_email"].ToString();
                            comc.comn_user_phone_number = dr["comn_user_phone_number"].ToString();
                            comc.comn_user_secret_question_code = dr["comn_user_secret_question_code"].ToString();
                            comc.comn_user_secret_question = dr["sims_appl_form_field_value1"].ToString();
                            comc.comn_user_secret_answer = dr["comn_user_secret_answer"].ToString();
                            string dat = dr["comn_user_expiry_date"].ToString();
                            if (!string.IsNullOrEmpty(dr["comn_user_expiry_date"].ToString()))
                                comc.comn_user_expiry_date = db.UIDDMMYYYYformat(  dr["comn_user_expiry_date"].ToString());
                            comc.comn_user_remark = dr["comn_user_remark"].ToString();
                            comc.comn_user_captcha_status = dr["comn_user_captcha_status"].ToString() == "1" ? true : false;
                            comc.comn_user_status_name = dr["status_name"].ToString();
                            comc.comn_user_status = dr["comn_user_status"].ToString();
                            comc.user_status1 = dr["comn_user_status"].ToString().Equals("A") ? true : false;
                            comc.comn_user_last_login = dr["comn_user_last_login"].ToString();

                            user_list.Add(comc);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, user_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, user_list);
        }

        [Route("CUDUser")]
        public HttpResponseMessage CUDUser(Comn005 data)
        {
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_user_proc",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@comn_user_code",data.comn_user_code),
                                new SqlParameter("@comn_user_name", data.comn_user_name),
                                new SqlParameter("@comn_user_alias", data.comn_user_alias),
                                new SqlParameter("@comn_user_group_name", data.comn_user_group_name),
                                 new SqlParameter("@comn_user_date_created", db.DBYYYYMMDDformat( data.comn_user_date_created)),
                                new SqlParameter("@comn_user_expiry_date",db.DBYYYYMMDDformat(data.comn_user_expiry_date)),
                                new SqlParameter("@comn_user_captcha_status", data.comn_user_captcha_status==true?"1":"0"),
                                new SqlParameter("@comn_user_status", data.comn_user_status_name),
                                new SqlParameter("@comn_user_email", data.comn_user_email),
                                new SqlParameter("@comn_user_phone_number", data.comn_user_phone_number),
                                new SqlParameter("@comn_user_remark", data.comn_user_remark),
                                new SqlParameter("@CreatedBy",data.comn_user_name),
                                new SqlParameter("@comn_user_secret_question", data.comn_user_secret_question),
                                new SqlParameter("@comn_user_secret_answer", data.comn_user_secret_answer)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }


                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
               // inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("UpdateUser")]
        public HttpResponseMessage UpdateUser(Comn005 data)
        {
            bool insertedvalues = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_user_proc",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "U"),
                                new SqlParameter("@comn_user_code",data.comn_user_code),
                                new SqlParameter("@comn_user_name", data.comn_user_name),
                                new SqlParameter("@comn_user_alias", data.comn_user_alias),
                                new SqlParameter("@comn_user_group_name", data.comn_user_group_name),
                                 new SqlParameter("@comn_user_date_created", db.DBYYYYMMDDformat(data.comn_user_date_created)),
                                new SqlParameter("@comn_user_expiry_date", db.DBYYYYMMDDformat(data.comn_user_expiry_date)),
                                new SqlParameter("@comn_user_captcha_status", data.comn_user_captcha_status==true?"1":"0"),
                                new SqlParameter("@comn_user_status", data.comn_user_status_name),
                                new SqlParameter("@comn_user_email", data.comn_user_email),
                                new SqlParameter("@comn_user_phone_number", data.comn_user_phone_number),
                                new SqlParameter("@comn_user_remark", data.comn_user_remark),
                                new SqlParameter("@CreatedBy",data.comn_user_name),
                                new SqlParameter("@comn_user_secret_question", data.comn_user_secret_question),
                                new SqlParameter("@comn_user_secret_answer", data.comn_user_secret_answer)

                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insertedvalues = true;
                        }

                    }
                }
                else
                {
                    insertedvalues = false;
                }
            }
            catch (Exception x)
            {
               // insertedvalues = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insertedvalues);
        }
    }
}