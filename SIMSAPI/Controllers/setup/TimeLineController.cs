﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using SIMSAPI.Helper;
using System.Data.SqlClient;

using log4net;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/TimeLine")]
    public class TimeLineController : ApiController
    {
        [Route("getUserAudit")]
        public HttpResponseMessage getUserAudit(string uname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserAudit(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "Common", "getUserAudit"));

            List<Com007> goaltarget_list = new List<Com007>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@comn_user_name", uname),

                            
                            //new SqlParameter("@comn_mod_code","015"),
                            //new SqlParameter("@comn_appl_code","Sim070"),
                          //  new SqlParameter("@comn_audit_start_time","04/22/2015"),
                           // new SqlParameter("@comn_audit_end_time","04/22/2016"),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                            simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_mod_img = dr["comn_mod_img"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            simsobj.comn_audit_ip = dr["comn_audit_ip"].ToString();
                            simsobj.comn_audit_dns = dr["comn_audit_dns"].ToString();
                            simsobj.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            simsobj.comn_audit_end_time = dr["comn_audit_end_time"].ToString();
                            simsobj.comn_audit_remark = dr["comn_audit_remark"].ToString();
                            simsobj.st_time = dr["st_time"].ToString();
                            simsobj.st_am = dr["st_am"].ToString();
                            simsobj.st_date = dr["st_date"].ToString();
                            try
                            {
                                simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                                simsobj.comn_appl_location = dr["comn_appl_location"].ToString();
                                simsobj.comn_mod_name_ar = dr["comn_mod_name_ar"].ToString();
                                simsobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();


                            }
                            catch (Exception ex) { }


        goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        }


        [Route("getUserAuditNew")]
        public HttpResponseMessage getUserAuditNew(string uname)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getUserAudit(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "Common", "getUserAudit"));

            List<Com007> goaltarget_list = new List<Com007>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[comn_user_audit_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@comn_user_name", uname),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Com007 simsobj = new Com007();
                            simsobj.comn_audit_user_code = dr["comn_audit_user_code"].ToString();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();
                           simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_mod_img = dr["comn_mod_img"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                         
                            simsobj.comn_audit_start_time = dr["comn_audit_start_time"].ToString();
                            simsobj.comn_audit_end_time = dr["comn_audit_end_time"].ToString();

                            simsobj.st_time = dr["st_time"].ToString();
                            simsobj.st_am = dr["st_am"].ToString();
                            simsobj.st_date = dr["st_date"].ToString();
                            try
                            {
                                simsobj.comn_appl_type = dr["comn_appl_type"].ToString();
                                simsobj.comn_appl_location = dr["comn_appl_location"].ToString();

                                simsobj.comn_mod_name_ar = dr["comn_mod_name_ar"].ToString();
                                simsobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();
                            }
                            catch (Exception ex) { }


                            goaltarget_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, goaltarget_list);

        }


        [Route("getModuleCode")]
        public HttpResponseMessage getModuleCode()
        {
            List<Com007> mod_list = new List<Com007>();
          
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'M'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            Com007 simsobj = new Com007();
                            simsobj.comn_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();

                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getApplicationCode")]
        public HttpResponseMessage getApplicationCode()
        {
            List<Com007> mod_list = new List<Com007>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'N'),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Com007 simsobj = new Com007();


                            simsobj.comn_appl_code_parent = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();

                            mod_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }


        [Route("CUDFieldDetails")]
        public HttpResponseMessage CUDFieldDetails(List<Com007> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Com007 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>()
                     {
                          
                         
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sr_no", simsobj.sr_no),
                                new SqlParameter("@comn_appl_mod_code", simsobj.comn_mod_code),
                                new SqlParameter("@comn_appl_code", simsobj.comn_appl_code),
                                new SqlParameter("@comn_appl_field_name_en", simsobj.comn_appl_field_name_en),
                                new SqlParameter("@comn_field_appl_code", simsobj.comn_appl_code_parent),
                                new SqlParameter("@comn_appl_field_desc", simsobj.comn_appl_field_desc),
                                new SqlParameter("@comn_creation_user", simsobj.comn_creation_user),
                                new SqlParameter("@comn_field_display_order", simsobj.comn_field_display_order),
                                new SqlParameter("@comn_show_icon_status", simsobj.comn_show_icon_status==true?"Y":"N"),
                                new SqlParameter("@comn_appl_field_status", simsobj.comn_appl_field_status==true?"Y":"N")

                             
                        });
                        if (ins > 0)
                        {
                            insert = true;
                        }
                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }


        [Route("getFieldDetails")]
        public HttpResponseMessage getFieldDetails(string application_code, string mod_code)
        {
           
            List<Com007> mod_list = new List<Com007>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'S'),
                            new SqlParameter("@comn_appl_code",application_code),
                            new SqlParameter("@comn_appl_mod_code",mod_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Com007 simsobj = new Com007();
                            simsobj.sr_no = dr["sr_no"].ToString();
                            simsobj.comn_appl_code=dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_code_name=dr["comn_appl_code_name"].ToString();
                            simsobj.comn_appl_field_name_en=dr["comn_appl_field_name_en"].ToString();
                            simsobj.comn_field_appl_code=dr["comn_field_appl_code"].ToString();
                            simsobj.comn_appl_field_name=dr["comn_appl_field_name"].ToString();
                            simsobj.comn_show_icon_status = dr["comn_show_icon_status"].Equals("Y") ? true : false;
                            simsobj.comn_appl_field_desc=dr["comn_appl_field_desc"].ToString();
                            simsobj.comn_appl_mod_code=dr["comn_appl_mod_code"].ToString();
                            simsobj.comn_appl_mod_name=dr["comn_appl_mod_name"].ToString();
                            simsobj.comn_creation_user=dr["comn_creation_user"].ToString();
                            simsobj.comn_appl_creation_date=dr["comn_appl_creation_date"].ToString();
                            simsobj.comn_field_display_order = dr["comn_field_display_order"].ToString();
                            simsobj.comn_appl_field_status = dr["comn_appl_field_status"].Equals("Y") ? true : false;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

        [Route("getFirstDetailsCount")]
        public HttpResponseMessage getFirstDetailsCount(string application_code,string mod_code)
        {
            List<Com007> mod_list = new List<Com007>();

            if (application_code == "undefined")
            {
                application_code = null;
            }
            if (mod_code == "undefined")
            {
                mod_code = null;
            }

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_field_details_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'R'),
                            new SqlParameter("@comn_appl_code",application_code),
                            new SqlParameter("@comn_appl_mod_code",mod_code),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            Com007 simsobj = new Com007();
                            simsobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_code_name = dr["comn_appl_code_name"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            simsobj.comn_appl_mod_name = dr["comn_appl_mod_name"].ToString();
                            simsobj.cnt = dr["cnt"].ToString();
                           
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }

        }

    }
}