﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Setup
{
    [RoutePrefix("api/Countrymaster")]
    public class CountryMasterController : ApiController
    {
        [Route("getCountrydata")]
        public HttpResponseMessage getCountrydata()
        {
            List<Sim002> result = new List<Sim002>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_country_proc]",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim002 obj = new Sim002();

                            obj.sims_country_code = dr["sims_country_code"].ToString();
                            obj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            obj.sims_country_name_ar = dr["sims_country_name_ar"].ToString();
                            obj.sims_country_name_fr = dr["sims_country_name_fr"].ToString();
                            obj.sims_country_name_ot = dr["sims_country_name_ot"].ToString();
                            obj.sims_continent = dr["sims_continent"].ToString();
                            obj.sims_region_code = dr["sims_region_code"].ToString();
                            obj.sims_region_code_name = dr["sims_region_name_en"].ToString();
                            obj.sims_currency_code = dr["sims_currency_code"].ToString();
                            obj.sims_currency_code_name = dr["sims_currency_dec"].ToString();
                            obj.sims_currency_dec = dr["excg_curcy_desc"].ToString();
                            obj.sims_status = dr["sims_status"].ToString() == "A";

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDCountrydata")]
        public HttpResponseMessage CUDCountrydata(List<Sim002> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDCountrydata", simsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim002 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_country_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_country_code", simsobj.sims_country_code),
                          new SqlParameter("@sims_country_name_en", simsobj.sims_country_name_en),
                          new SqlParameter("@sims_country_name_ar", simsobj.sims_country_name_ar),
                          new SqlParameter("@sims_country_name_fr", simsobj.sims_country_name_fr),
                          new SqlParameter("@sims_country_name_ot",simsobj.sims_country_name_ot),
                          new SqlParameter("@sims_continent", simsobj.sims_continent),
                          new SqlParameter("@sims_region_code", simsobj.sims_region_code),
                          new SqlParameter("@sims_currency_code", simsobj.sims_currency_code),
                          new SqlParameter("@sims_currency_dec", simsobj.sims_currency_dec),
                          new SqlParameter("@sims_status",simsobj.sims_status==true?"A":"I"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}