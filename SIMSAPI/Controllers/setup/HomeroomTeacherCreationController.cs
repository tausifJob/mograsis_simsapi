﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ERP.gradebookClass;
using System.Text.RegularExpressions;


namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/HomeroomCreation")]
    public class HomeroomTeacherCreationController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("GetHoomroomCreation")]
        public HttpResponseMessage GetHoomroomCreation(string teachercode, string sims_cur_code, string sims_academic_year, string HomeroomSubject,string sims_grade_code)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","S"),
                        new SqlParameter("@user_name",teachercode),
                         new SqlParameter("@sims_cur_code",sims_cur_code),
                           new SqlParameter("@sims_academic_year",sims_academic_year),
                             new SqlParameter("@HomeroomSubject",HomeroomSubject),
                               new SqlParameter("@sims_grade_code",sims_grade_code),
                      
                

                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.HomeroomCode = dr["HomeroomCode"].ToString();
                            simsobj.HomeroomName = dr["HomeroomName"].ToString();
                            simsobj.HomeroomSubject = dr["HomeroomSubject"].ToString();
                            simsobj.HomeroomEmployeeCode = dr["HomeroomEmployeeCode"].ToString();
                            simsobj.HomeroomStatus = dr["HomeroomStatus"].ToString().Equals("A") ? true : false;
                            simsobj.CreatedBy = dr["CreatedBy"].ToString();
                            simsobj.UpdatedBy = dr["UpdatedBy"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetHoomroomStudents")]
        public HttpResponseMessage GetHoomroomStudents(string hoomroom_code)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","M"),
                        new SqlParameter("@HomeroomCode",hoomroom_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.HomeroomName = dr["HomeroomName"].ToString();
                            simsobj.HomeroomSubject = dr["HomeroomSubject"].ToString();
                            simsobj.HomeroomEmployeeCode = dr["HomeroomEmployeeCode"].ToString();
                            simsobj.HomeroomStatus = dr["HomeroomStatus"].ToString().Equals("A") ? true : false;
                            simsobj.CreatedBy = dr["CreatedBy"].ToString();
                            simsobj.UpdatedBy = dr["UpdatedBy"].ToString();
                            simsobj.HomeroomCode = dr["HomeroomCode"].ToString();
                            //simsobj.HomeroomStudentEnrollNumber = dr["HomeroomStudentEnrollNumber"].ToString();
                            //simsobj.Status = dr["Status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.studlist = new List<hoomroomStudent>();
                            hoomroomStudent st = new hoomroomStudent();
                            st.HomeroomStudentEnrollNumber = dr["HomeroomStudentEnrollNumber"].ToString();
                            st.HomeroomCode = dr["HomeroomCode"].ToString();
                            st.Status = dr["Status"].ToString().Equals("A") ? true : false;
                            st.sims_cur_code = dr["sims_cur_code"].ToString();
                            st.sims_academic_year = dr["sims_academic_year"].ToString();

                            simsobj.HomeroomCode = dr["HomeroomCode"].ToString();
                            simsobj.Status = dr["Status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_grade_section_code = dr["sims_grade_section_code"].ToString();

                            //Fro grade Section
                            //simsobj.gradelist = new List<hoomroomClass>();
                            //hoomroomClass gr = new hoomroomClass();
                            //gr.HomeroomCode = dr["HomeroomCode"].ToString();
                            //gr.Status = dr["Status"].ToString().Equals("A") ? true : false;
                            //gr.sims_grade_code = dr["sims_grade_code"].ToString();
                            //gr.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.studlist.Add(st);
                            //simsobj.gradelist.Add(gr);
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getStudentName")]
        public HttpResponseMessage getStudentName(string curcode, string academicyear, string section, string subcode)
        {
            string cur_code = string.Empty;
            string str = "";
            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                         {
                new SqlParameter("@opr","Q"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                //new SqlParameter("@sims_grade_code", gradecode),
                new SqlParameter("@sims_section_code", section),
                new SqlParameter("@sims_subject_code", subcode==""?null:subcode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.sims_student_enroll_number == str select p);

                            Sims089 ob = new Sims089();
                            ob.sublist = new List<subject>();
                            ob.sims_cur_code = dr["sims_cur_code"].ToString();
                            ob.sims_academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_grade_section_code = dr["sims_grade_section_code"].ToString();
                            ob.sims_student_enroll_number = str;
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_student_name = dr["StudentName"].ToString();
                            ob.sims_subject_code = dr["sims_subject_code"].ToString();
                            subject sb = new subject();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_status = dr["Status"].Equals(1) ? true : false;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }

        [Route("getStudentName_DMC")]
        public HttpResponseMessage getStudentName_DMC(string curcode, string academicyear, string section, string subcode)
        {
            string cur_code = string.Empty;
            string str = "";
            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_dmc_proc]",
                    new List<SqlParameter>()
                    {
                           new SqlParameter("@opr","Q"),
                           new SqlParameter("@sims_cur_code", curcode),
                           new SqlParameter("@sims_academic_year", academicyear),
                           //new SqlParameter("@sims_grade_code", gradecode),
                           new SqlParameter("@sims_section_code", section),
                           new SqlParameter("@sims_subject_code", subcode==""?null:subcode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //Sims089 newobj = new Sims089();
                            str = dr["sims_enroll_number"].ToString();
                            var v = (from p in mod_list where p.sims_student_enroll_number == str select p);

                            Sims089 ob = new Sims089();
                            ob.sublist = new List<subject>();
                            ob.sims_cur_code = dr["sims_cur_code"].ToString();
                            ob.sims_academic_year = dr["sims_academic_year"].ToString();
                            ob.sims_grade_code = dr["sims_grade_code"].ToString();
                            ob.sims_section_code = dr["sims_section_code"].ToString();
                            ob.sims_grade_section_code = dr["sims_grade_section_code"].ToString();
                            ob.sims_student_enroll_number = str;
                            ob.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            ob.sims_student_name = dr["StudentName"].ToString();
                            ob.sims_subject_code = dr["sims_subject_code"].ToString();
                            try
                            {
                                ob.sims_roll_number = dr["sims_roll_number"].ToString();
                                ob.sims_roll_number_new = int.Parse(Regex.Match(dr["sims_roll_number"].ToString(), @"\d+").Value);
                            }
                            catch (Exception ex) { }


                            subject sb = new subject();
                            sb.sims_subject_code = dr["sims_subject_code"].ToString();
                            sb.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            sb.sims_status = dr["Status"].Equals(1) ? true : false;

                            if (v.Count() == 0)
                            {
                                ob.sublist.Add(sb);
                                mod_list.Add(ob);
                            }
                            else
                            {
                                v.ElementAt(0).sublist.Add(sb);
                            }
                            //newobj.sims_student_name = dr["sims_enroll_number"].ToString();
                            //newobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //newobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            //newobj.sims_section_code = dr["sims_section_code"].ToString();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                }
            }

            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("getSectionsubject_name")]
        public HttpResponseMessage getSectionsubject_name(string curcode, string academicyear, string login_user)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                         {
                new SqlParameter("@opr","Z"),
                new SqlParameter("@sims_cur_code", curcode),
                new SqlParameter("@sims_academic_year", academicyear),
                //new SqlParameter("@sims_grade_code", gradecode),
                //new SqlParameter("@sims_section_code", section),
                new SqlParameter("@teacher_code", login_user)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.sims_subject_code = dr["sims_subject_code"].ToString();

                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }



        [Route("GetRCGrade")]
        public HttpResponseMessage GetRCGrade(string curcode, string ayear, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                     {

                        new SqlParameter("@opr", "R"),
                        new SqlParameter("@sims_cur_code", curcode),
                        new SqlParameter("@bell_academic_year", ayear),
                        new SqlParameter("@user_name", teachercode)
                     });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_grade_code = dr["sims_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }

        [Route("GetRCSection")]
        public HttpResponseMessage GetRCSection(string curcode, string ayear, string gradecode, string teachercode)
        {

            List<student_report_card_comment> AcademicYear = new List<student_report_card_comment>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "T"),
                            new SqlParameter("@sims_cur_code", curcode),
                            new SqlParameter("@bell_academic_year", ayear),
                            new SqlParameter("@bell_grade_code", gradecode),
                            new SqlParameter("@user_name", teachercode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            student_report_card_comment obj = new student_report_card_comment();
                            obj.sims_section_code = dr["sims_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            AcademicYear.Add(obj);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, AcademicYear);
        }


        // Add Homroom students
        [Route("AddHoomroomStudents")]
        public HttpResponseMessage AddHoomroomStudents(string data1, List<hoomroomCreation> postData)
        {
            bool alert_status = false;
            string homeroom_code = string.Empty;
            string batch_id = string.Empty;

            hoomroomCreation hoomroomObj = Newtonsoft.Json.JsonConvert.DeserializeObject<hoomroomCreation>(data1);
            try
            {
                if (hoomroomObj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@opr",'I'),
                                new SqlParameter("@HomeroomName", hoomroomObj.HomeroomName),
                                new SqlParameter("@HomeroomSubject", hoomroomObj.HomeroomSubject),
                                new SqlParameter("@HomeroomEmployeeCode",hoomroomObj.HomeroomEmployeeCode),
                                new SqlParameter("@HomeroomStatus", hoomroomObj.HomeroomStatus.Equals(true)?"A":"I"),
                                new SqlParameter("@CreatedBy", hoomroomObj.CreatedBy),
                                new SqlParameter("@UpdatedBy", hoomroomObj.UpdatedBy),
                                new SqlParameter("@sims_cur_code", hoomroomObj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", hoomroomObj.sims_academic_year),
                                new SqlParameter("@HomeroomCode", hoomroomObj.homeroomId)                                
                                //new SqlParameter("@CreatedOn", hoomroomObj.CreatedOn),                                
                                //new SqlParameter("@UpdatedOn", hoomroomObj.UpdatedOn),
                                //new SqlParameter("@ApprovedBy", hoomroomObj.ApprovedBy),
                                //new SqlParameter("@ApprovedOn", hoomroomObj.ApprovedOn),
                             });
                        if (dr.Read())
                        {
                            homeroom_code = dr["homeroom_code"].ToString();
                            batch_id = dr["batch_id"].ToString();
                        }
                        //else
                        //{
                        //    alert_status = false;
                        //}
                        dr.Close();

                        foreach (hoomroomCreation obj in postData)
                        {
                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                              new List<SqlParameter>()
                             {
                                new SqlParameter("@opr",'P'),
                                new SqlParameter("@HomeroomCode", homeroom_code),
                                new SqlParameter("@HomeroomStudentEnrollNumber", obj.HomeroomStudentEnrollNumber),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@Status", obj.Status.Equals(true)?"A":"I"),
                                new SqlParameter("@batchId", batch_id)
                             });
                            //if (dr1.RecordsAffected > 0)
                            //{
                            //    //alert_status = true;
                            //}
                            //else
                            //{
                            //    //alert_status = false;
                            //}
                            dr1.Close();
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, homeroom_code);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, homeroom_code);
        }



        [Route("AddHoomroomclass")]
        public HttpResponseMessage AddHoomroomclass(string homeroom_code, List<hoomroomCreation> classData)
        {
            bool alert_status = true;
            //string homeroom_code = string.Empty;

            //hoomroomCreation hoomroomObj = Newtonsoft.Json.JsonConvert.DeserializeObject<hoomroomCreation>(data1);
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (hoomroomCreation ob in classData)
                    {
                        SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                            new List<SqlParameter>()
                            {
                            new SqlParameter("@opr",'C'),
                            new SqlParameter("@HomeroomCode", homeroom_code),
                            new SqlParameter("@sims_cur_code", ob.sims_cur_code),
                            new SqlParameter("@sims_academic_year", ob.sims_academic_year),
                            new SqlParameter("@sims_grade_code", ob.sims_grade_code),
                            new SqlParameter("@sims_section_code", ob.sims_section_code),
                            new SqlParameter("@Status", ob.Status.Equals(true)?"A":"I"),
                            });
                        if (dr2.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr2.Close();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert_status);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }


        [Route("getGradeSection")]
        public HttpResponseMessage getGradeSection(string sims_cur_code, string academic_year, string subject_code, string teachercode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ACADEMICYEAR"));

            List<Sims078> aca_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "B"),
                                new SqlParameter("@sims_cur_code", sims_cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_subject_code", subject_code),
                                new SqlParameter("@user_name", teachercode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            //simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.ClassName = dr["name"].ToString();
                            simsobj.ClassCode = dr["code"].ToString();
                            aca_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, aca_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, aca_list);
            }
        }

        [Route("getGradeSection_DMC")]
        public HttpResponseMessage getGradeSection_DMC(string sims_cur_code, string academic_year, string subject_code, string teachercode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicYear(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "ACADEMICYEAR"));

            List<Sims078> aca_list = new List<Sims078>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_dmc_proc]",
                        new List<SqlParameter>()
                         {
                               new SqlParameter("@opr", "B"),
                                new SqlParameter("@sims_cur_code", sims_cur_code),
                                new SqlParameter("@sims_academic_year", academic_year),
                                new SqlParameter("@sims_subject_code", subject_code),
                                new SqlParameter("@user_name", teachercode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims078 simsobj = new Sims078();
                            //simsobj.academic_year = dr["sims_academic_year"].ToString();
                            simsobj.ClassName = dr["name"].ToString();
                            simsobj.ClassCode = dr["code"].ToString();
                            aca_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, aca_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, aca_list);
            }
        }


        [Route("HomeroomDelete")]
        public HttpResponseMessage HomeroomDelete(List<hoomroomCreation> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (hoomroomCreation obj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                             new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", obj.opr),
                                new SqlParameter("@HomeroomCode", obj.homeroom_code)                                
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        dr.Close();
                    }
                }               
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }

        /* get for subject teacher allocation */

        [Route("GetHoomroomByPara")]
        public HttpResponseMessage GetHoomroomByPara(string cur_code,string academic_year,string emp_code, string grade_code, string section_code)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","L"),
                        new SqlParameter("@sims_cur_code",cur_code),
                        new SqlParameter("@sims_academic_year",academic_year),
                        new SqlParameter("@HomeroomEmployeeCode",emp_code),
                        new SqlParameter("@sims_grade_code",grade_code),
                        new SqlParameter("@sims_section_code",section_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.HomeroomCode = dr["HomeroomCode"].ToString();
                            simsobj.HomeroomName = dr["HomeroomName"].ToString();
                            simsobj.HomeroomSubject = dr["HomeroomSubject"].ToString();
                            simsobj.HomeroomEmployeeCode = dr["HomeroomEmployeeCode"].ToString();
                            simsobj.HomeroomStatus = dr["HomeroomStatus"].ToString().Equals("A") ? true : false;                           
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        /* Update from subject teacher allocation */

        [Route("HomeroomUpdateStatus")]
        public HttpResponseMessage HomeroomUpdateStatus(List<hoomroomCreation> data)
        {
            bool alert_status = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (hoomroomCreation ob in data)
                    {
                        SqlDataReader dr2 = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",'N'),
                                new SqlParameter("@HomeroomSubject", ob.HomeroomSubject),
                                new SqlParameter("@HomeroomEmployeeCode", ob.HomeroomEmployeeCode),
                                new SqlParameter("@sims_cur_code", ob.sims_cur_code),
                                new SqlParameter("@sims_academic_year", ob.sims_academic_year),
                                new SqlParameter("@sims_grade_code", ob.sims_grade_code),
                                new SqlParameter("@sims_section_code", ob.sims_section_code),
                                new SqlParameter("@Status", ob.Status.Equals(true)?"A":"I"),
                            });
                        if (dr2.RecordsAffected > 0)
                        {
                            alert_status = true;
                        }
                        else
                        {
                            alert_status = false;
                        }
                        dr2.Close();
                    }
                }
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert_status);
        }


        [Route("GetTeacher")]
        public HttpResponseMessage GetTeacher(string academicyear,string subject_code,string grade_section)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                if (academicyear == "undefined" || academicyear == "")
                    academicyear = null;
                if (subject_code == "undefined" || subject_code == "")
                    subject_code = null;
                if (grade_section == "undefined" || grade_section == "")
                    grade_section = null;
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","V"),
                        new SqlParameter("@bell_academic_year",academicyear),
                        new SqlParameter("@sims_sub_code",subject_code),
                        new SqlParameter("@bell_grade_code",grade_section)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetTeacherByAcademic")]
        public HttpResponseMessage GetTeacherByAcademic(string academicyear)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                if (academicyear == "undefined" || academicyear == "")
                    academicyear = null;
                
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_dmc_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","V"),
                        new SqlParameter("@bell_academic_year",academicyear)                    
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("GetUserRole")]
        public HttpResponseMessage GetUserRole(string username)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","W"),
                        new SqlParameter("@user_name",username)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.user_status = dr["user_status"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("GetHoomroomCreation_DMC")]
        public HttpResponseMessage GetHoomroomCreation_DMC(string teachercode, string sims_cur_code, string sims_academic_year)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                if (teachercode == "" || teachercode == "undefined")
                    teachercode = null;
                if (sims_cur_code == "" || sims_cur_code == "undefined")
                    sims_cur_code = null;
                if (sims_academic_year == "" || sims_academic_year == "undefined")
                    sims_academic_year = null;
               
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_dmc_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","X"),
                        new SqlParameter("@user_name",teachercode),
                        new SqlParameter("@sims_cur_code",sims_cur_code),
                        new SqlParameter("@sims_academic_year",sims_academic_year)                                   
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.HomeroomCode = dr["HomeroomCode"].ToString();
                            simsobj.HomeroomName = dr["HomeroomName"].ToString();
                            simsobj.HomeroomSubject = dr["HomeroomSubject"].ToString();
                            simsobj.HomeroomEmployeeCode = dr["HomeroomEmployeeCode"].ToString();
                            simsobj.HomeroomStatus = dr["HomeroomStatus"].ToString().Equals("A") ? true : false;
                            simsobj.CreatedBy = dr["CreatedBy"].ToString();
                            simsobj.UpdatedBy = dr["UpdatedBy"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        public object sims_cur_code { get; set; }

        public object sims_academic_year { get; set; }

        public object sims_subject_code { get; set; }

        public object sims_grade_section { get; set; }

        public object sims_section_code { get; set; }

        public object sims_sub_code { get; set; }


        [Route("getSectionsubject_name_abqis")]
        public HttpResponseMessage getSectionsubject_name_abqis(string sims_cur_code, string sims_academic_year)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                         {
                new SqlParameter("@opr","A"),
                new SqlParameter("@sims_cur_code",sims_cur_code),
                new SqlParameter("@sims_academic_year",sims_academic_year),
               
               
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_subject_name_en = dr["sims_subject_name_en"].ToString();
                            newobj.HomeroomSubject = dr["HomeroomSubject"].ToString();
                            	
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }


        [Route("getgradesection_abqis")]
        public HttpResponseMessage getgradesection_abqis(string sims_cur_code, string sims_academic_year, string HomeroomSubject)
        {

            List<Sims089> mod_list = new List<Sims089>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_proc]",
                    new List<SqlParameter>()
                         {
                new SqlParameter("@opr","O"),
                new SqlParameter("@sims_cur_code", sims_cur_code),
                new SqlParameter("@sims_academic_year", sims_academic_year),
               
                new SqlParameter("@HomeroomSubject", HomeroomSubject)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims089 newobj = new Sims089();
                            newobj.sims_Section_code = dr["sims_Section_code"].ToString();
                            newobj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            	
                            mod_list.Add(newobj);
                        }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("GetTeacherBySubject")]
        public HttpResponseMessage GetTeacherBySubject(string subjectcode)
        {
            List<hoomroomCreation> mod_list = new List<hoomroomCreation>();
            try
            {
                if (subjectcode == "undefined" || subjectcode == "")
                    subjectcode = null;

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[homeroom_teacher_creation_dmc_proc]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","O"),
                        new SqlParameter("@bell_academic_year",subjectcode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            hoomroomCreation simsobj = new hoomroomCreation();
                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_employee_code = dr["sims_employee_code"].ToString();

                            mod_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}