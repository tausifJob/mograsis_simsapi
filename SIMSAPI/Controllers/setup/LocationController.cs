﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/loc")]
    public class ILocationController : ApiController
    {

        [Route("getLoc")]
        public HttpResponseMessage getLoc()
        {
            List<Sim163> loc = new List<Sim163>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_location_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim163 obj = new Sim163();

                            obj.sims_location_code = dr["sims_location_code"].ToString();
                            obj.sims_location_desc = dr["sims_location_desc"].ToString();
                            obj.sims_location_status = dr["sims_location_status"].Equals("A") ? true : false;

                            loc.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loc);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loc);
        }


        [Route("LocationCUD")]
        public HttpResponseMessage LocationCUD(List<Sim163> data)
        {

            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim163 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_location_proc",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@sims_location_code",simsobj.sims_location_code),
                                new SqlParameter("@sims_location_desc", simsobj.sims_location_desc),
                                new SqlParameter("@sims_location_status",simsobj.sims_location_status==true?"A":"I")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}