﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.Common
{
    [RoutePrefix("api/common/MasterStoredProcedureList")]

    public class CommonMasterSPController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getStoredProcedureList")]
        public HttpResponseMessage getStoredProcedureList()
        {
            List<Sims012> criteria = new List<Sims012>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[comn_master_sp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims012 crit = new Sims012();
                            crit.sp_code = dr["sp_code"].ToString();
                            crit.sp_name = dr["sp_name"].ToString();
                            crit.sp_type = dr["sp_type"].ToString();
                            crit.sp_appl_code = dr["sp_appl_code"].ToString();
                            
                            if (dr["sp_read"].ToString().Equals("Y"))
                                crit.sp_read = true;
                            else
                                crit.sp_read = false;

                            if (dr["sp_insert"].ToString().Equals("Y"))
                                crit.sp_insert = true;
                            else
                                crit.sp_insert = false;

                            if (dr["sp_update"].ToString().Equals("Y"))
                                crit.sp_update = true;
                            else
                                crit.sp_update = false;

                            if (dr["sp_delete"].ToString().Equals("Y"))
                                crit.sp_delete = true;
                            else
                                crit.sp_delete = false;

                            crit.sp_remark = dr["sp_remark"].ToString();
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

      

        [Route("getSPType")]
        public HttpResponseMessage getSPType()
        {
            List<Sims012> criteria = new List<Sims012>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[comn_master_sp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims012 crit = new Sims012();
                            crit.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            crit.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                           
                            criteria.Add(crit);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, criteria);
        }

       

        [Route("CUDInsertStoredProcedure")]
        public HttpResponseMessage CUDInsertStoredProcedure(List<Sims012> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDInsertStoredProcedure()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP/STUDENT/", "CUDInsertStoredProcedure"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims012 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[dbo].[comn_master_sp_proc]",
                                 new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sp_code", simsobj.sp_code),
                            new SqlParameter("@sp_name", simsobj.sp_name),
                            new SqlParameter("@sp_type", simsobj.sp_type),
                            new SqlParameter("@sp_appl_code", simsobj.sp_appl_code),
                            new SqlParameter("@sp_read", simsobj.sp_read==true?"Y":"N"),
                            new SqlParameter("@sp_insert", simsobj.sp_insert==true?"Y":"N"),
                            new SqlParameter("@sp_update", simsobj.sp_update==true?"Y":"N"),
                            new SqlParameter("@sp_delete", simsobj.sp_delete==true?"Y":"N"),
                            new SqlParameter("@sp_remark", simsobj.sp_remark),
                        });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
        }


    }
}