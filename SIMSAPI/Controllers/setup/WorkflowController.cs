﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.hrmsClass;
using System.Collections;

using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.hrms
{
    [RoutePrefix("api/Workflow")]
    public class WorkflowController : ApiController
    {

        [Route("GetMod")]
        public HttpResponseMessage GetMod()
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "M")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.sims_workflow_mod_code = dr["comn_mod_code"].ToString();
                            hrmsObj.sims_workflow_mod_name = dr["comn_mod_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetAppl")]
        public HttpResponseMessage GetAppl(string mod_code)
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N"),
                new SqlParameter("@sims_workflow_mod_code", mod_code)
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.sims_workflow_appl_code = dr["comn_appl_code"].ToString();
                            hrmsObj.sims_workflow_appl_name = dr["comn_appl_name_en"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDept")]
        public HttpResponseMessage GetDept()
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                    new List<SqlParameter>() 
                    { 
                        new SqlParameter("@opr", "K")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.dept_code = dr["codp_dept_no"].ToString();
                            hrmsObj.dept_name = dr["codp_dept_name"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetDesg")]
        public HttpResponseMessage GetDesg()
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[pays_salary_details_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "N")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.dg_code = dr["dg_code"].ToString();
                            hrmsObj.dg_name = dr["dg_desc"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetWorkFlowType")]
        public HttpResponseMessage GetWorkFlowType()
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "W")
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.sims_workflow_type = dr["sims_appl_parameter"].ToString();
                            hrmsObj.sims_workflow_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);

        }

        [Route("GetEmployee")]
        public HttpResponseMessage Getemployee(string empid,string desg)
        {
            if (empid == "undefined")
                empid = null;
            if (desg == "undefined")
                desg = null;
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                            new SqlParameter("@sims_workflow_employee_code", empid),
                            new SqlParameter("@desg_Code", desg),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 simsobj = new Sims048();
                            simsobj.sims_workflow_employee_code= dr["em_login_code"].ToString();
                            simsobj.sims_workflow_employee_name = dr["emp_name"].ToString();
                            simsobj.dept_name = dr["codp_dept_name"].ToString();
                            simsobj.select_status = false;
                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("updateEmployeeThoseRemove")]
        public HttpResponseMessage updateEmployeeThoseRemove(string empid)
        {
            bool Inserted = false;
            if (empid == "undefined")
                empid = null;           
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'X'),
                            new SqlParameter("@sims_workflow_employee_code", empid)                          
                         });
                    if (dr.RecordsAffected > 0)
                    {
                        Inserted = true;
                    }
                    dr.Close();
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, Inserted);
        }

        [Route("GetEmpWorkflowDetails")]
        public HttpResponseMessage GetEmpWorkflowDetails(string mod_code, string appl_code,string dept_code, string sims_workflow_type)
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                    new List<SqlParameter>() 
                    { 
                         new SqlParameter("@opr", 'V'),
                         new SqlParameter("@sims_workflow_appl_code", appl_code),
                         new SqlParameter("@sims_workflow_mod_code", mod_code),
                         new SqlParameter("@dept", dept_code),
                         new SqlParameter("@sims_workflow_type", sims_workflow_type)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 simsobj = new Sims048();
                            simsobj.sims_workflow_det_empno = dr["sims_workflow_det_empno"].ToString();
                            simsobj.sims_workflow_employee_name = dr["Emp_name"].ToString();
                            simsobj.sims_workflow_type = dr["sims_workflow_type"].ToString();
                            simsobj.sims_worfflow_description = dr["sims_workflow_description"].ToString();
                            simsobj.sims_workflow = dr["sims_workflow"].ToString();
                            simsobj.sims_workflow_srno = dr["sims_workflow_srno"].ToString();
                            simsobj.sims_workflow_employee_code = dr["sims_workflow_employee_code"].ToString();
                            simsobj.sims_workflow_order = dr["sims_workflow_order"].ToString();
                            simsobj.sims_workflow_effectivity = dr["sims_workflow_effectivity"].ToString();                            
                            simsobj.sims_workflow_editable = dr["sims_workflow_editable"].ToString().Equals("Y") ? true : false;

                            mod_list.Add(simsobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);

                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }
                
        //[Route("LeaveDetails")]
        //public HttpResponseMessage LeaveDetails(string dept,string desg,string l_type,string empcode)
        //{

        //     if (dept == "undefined")
        //            dept = null;
        //     if (desg == "undefined")
        //            desg = null;
        //     if (l_type == "undefined")
        //            l_type = null;
        //     if (empcode == "undefined")
        //            empcode = null;
        //    List<Per314> mod_list = new List<Per314>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[pays].[pays_update_employee_leave_balance]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", 'S'),
        //                    new SqlParameter("@depcode", dept),
        //                    new SqlParameter("@desgcode", desg),
        //                    new SqlParameter("@leave_code", l_type),
        //                    new SqlParameter("@emp_code", empcode),
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Per314 simsobj = new Per314();
        //                    simsobj.emp_code = dr["el_number"].ToString();
        //                    simsobj.emp_name = dr["emp_name"].ToString();
        //                    simsobj.leave_code = dr["el_leave_code"].ToString();
        //                    simsobj.leave_name = dr["cl_desc"].ToString();
        //                    simsobj.leave_taken = dr["el_days_taken"].ToString();
        //                    simsobj.leave_bal = dr["el_leave_bal"].ToString();
        //                    simsobj.leave_max = dr["el_maximum_days"].ToString();
        //                    simsobj.leave_max_old = dr["el_maximum_days"].ToString();
        //                    mod_list.Add(simsobj);

        //                }
        //            }
                  
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        //}

         [Route("InsertSimsWorkflow")]
        public HttpResponseMessage InsertSimsWorkflow(Sims048 obj)
        {
            bool Inserted =false;
            string srno="";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", 'I'),
                    new SqlParameter("@sims_worfflow_description", obj.sims_worfflow_description),
                    new SqlParameter("@sims_workflow_mod_code", obj.sims_workflow_mod_code),
                    new SqlParameter("@sims_workflow_appl_code", obj.sims_workflow_appl_code),
                    new SqlParameter("@sims_workflow_type", obj.sims_workflow_type),
                    new SqlParameter("@dept", obj.dept_code),
                    new SqlParameter("@sims_workflow_status", 'A')

                      });

                        //if (dr.RecordsAffected > 0)
                        //{
                        //    Inserted = true;
                        //}

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            srno = dr["srno"].ToString();
                        }
                    }
                        dr.Close();
                    }
            }
            catch (Exception e)
            {
            
            }

            return Request.CreateResponse(HttpStatusCode.OK, srno);
        }

         [Route("InsertSimsWorkflowemp")]
         public HttpResponseMessage InsertSimsWorkflowemp(List<Sims048> simsobj)
         {
             bool Inserted = false;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (var item in simsobj)
                     {
                         SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                            new SqlParameter("@sims_workflow_employee_code", item.sims_workflow_employee_code),
                            new SqlParameter("@sims_workflow_order", item.sims_workflow_order),
                            new SqlParameter("@sims_workflow_effectivity", item.sims_workflow_effectivity),
                            new SqlParameter("@sims_workflow_surpassable", item.sims_workflow_surpassable),
                            new SqlParameter("@sims_workflow_status", 'A'),
                            new SqlParameter("@sims_workflow_srno", item.sims_workflow_srno),
                            new SqlParameter("@sims_workflow_editable", item.sims_workflow_editable.Equals(true)?"Y":"N")                           
                        });

                         if (dr.RecordsAffected > 0)
                         {
                             Inserted = true;
                         }
                         dr.Close();
                     }
                 }
             }
             catch (Exception e)
             {

             }

             return Request.CreateResponse(HttpStatusCode.OK, Inserted);
         }

         [Route("UpdateSimsWorkflow")]
         public HttpResponseMessage UpdateSimsWorkflow(Sims048 obj)
         {
             bool Inserted = false;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();

                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                             new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", 'U'),
                    new SqlParameter("@sims_workflow", obj.sims_workflow),
                    new SqlParameter("@sims_worfflow_description", obj.sims_worfflow_description),
                    new SqlParameter("@sims_workflow_type", obj.sims_workflow_type),
                    new SqlParameter("@sims_workflow_srno", obj.sims_workflow_srno),

                      });

                     if (dr.RecordsAffected > 0)
                     {
                         Inserted = true;
                     }
                     dr.Close();
                 }
             }
             catch (Exception e)
             {

             }

             return Request.CreateResponse(HttpStatusCode.OK, Inserted);
         }

         [Route("UpdateSimsWorkflowemp")]
         public HttpResponseMessage UpdateSimsWorkflowemp(List<Sims048> simsobj)
         {
             bool Inserted = false;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (var item in simsobj)
                     {
                         SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                         new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'P'),
                            new SqlParameter("@sims_workflow_det_empno", item.sims_workflow_det_empno),
                            new SqlParameter("@sims_workflow_srno", item.sims_workflow_srno),
                            new SqlParameter("@sims_workflow_employee_code", item.sims_workflow_employee_code),
                            new SqlParameter("@sims_workflow_order", item.sims_workflow_order),
                            new SqlParameter("@sims_workflow_effectivity", item.sims_workflow_effectivity),
                            new SqlParameter("@sims_workflow_surpassable", item.sims_workflow_surpassable),
                            new SqlParameter("@sims_workflow_status", 'A'),
                            new SqlParameter("@sims_workflow_editable", item.sims_workflow_editable.Equals(true)?"Y":"N")
                        });

                         if (dr.RecordsAffected > 0)
                         {
                             Inserted = true;
                         }
                         dr.Close();
                     }
                 }
             }
             catch (Exception e)
             {

             }

             return Request.CreateResponse(HttpStatusCode.OK, Inserted);
         }


         [Route("UDeleteSimsWorkflowemp")]
         public HttpResponseMessage UDeleteSimsWorkflowemp(List<Sims048> simsobj)
         {
             bool Inserted = false;
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     foreach (var item in simsobj)
                     {
                         SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                                 new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", 'D'),
                    new SqlParameter("@sims_workflow_det_empno", item.sims_workflow_det_empno),
                    new SqlParameter("@sims_workflow_employee_code", item.sims_workflow_employee_code),

                      });

                         if (dr.RecordsAffected > 0)
                         {
                             Inserted = true;
                         }
                         dr.Close();
                     }
                 }
             }
             catch (Exception e)
             {

             }

             return Request.CreateResponse(HttpStatusCode.OK, Inserted);
         }

         [Route("GetInventoryDept")]
         public HttpResponseMessage GetInventoryDept()
         {
             List<Sims048> mod_list = new List<Sims048>();
             try
             {
                 using (DBConnection db = new DBConnection())
                 {
                     db.Open();
                     SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                         new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", "B")
                });
                     if (dr.HasRows)
                     {
                         while (dr.Read())
                         {
                             Sims048 hrmsObj = new Sims048();
                             hrmsObj.dept_code = dr["codp_dept_no"].ToString();
                             hrmsObj.dept_name = dr["codp_dept_name"].ToString();
                             mod_list.Add(hrmsObj);
                         }
                     }
                 }
             }
             catch (Exception e)
             {

             }
             return Request.CreateResponse(HttpStatusCode.OK, mod_list);

         }


        [Route("GetWorkFlowDepartmentWise")]
        public HttpResponseMessage GetWorkFlowDepartmentWise(string dept_code)
        {
            List<Sims048> mod_list = new List<Sims048>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_workflow_new]",
                    new List<SqlParameter>()
                    {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@dept",dept_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims048 hrmsObj = new Sims048();
                            hrmsObj.sims_workflow_type = dr["sims_appl_parameter"].ToString();
                            hrmsObj.sims_workflow_type_name = dr["sims_appl_form_field_value1"].ToString();
                            mod_list.Add(hrmsObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}