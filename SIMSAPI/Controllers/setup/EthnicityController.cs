﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Setup
{
    [RoutePrefix("api/Ethnicity")]
    public class EthnicityController:ApiController
    {
        [Route("getEthnicitydata")]
        public HttpResponseMessage getEthnicitydata()
        {
            List<Sim006> result = new List<Sim006>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_ethnicity",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim006 obj = new Sim006();

                            obj.sims_ethnicity_code = dr["sims_ethnicity_code"].ToString();
                            obj.sims_ethnicity_name_en = dr["sims_ethnicity_name_en"].ToString();
                            obj.sims_ethnicity_name_ar = dr["sims_ethnicity_name_ar"].ToString();
                            obj.sims_ethnicity_name_fr = dr["sims_ethnicity_name_fr"].ToString();
                            obj.sims_ethnicity_name_ot = dr["sims_ethnicity_name_ot"].ToString();
                            obj.sims_ethnicity_status = dr["sims_ethnicity_status"].ToString() == "A";

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("CUDEthnicitydata")]
        public HttpResponseMessage CUDEthnicitydata(List<Sim006> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDEthnicitydata", simsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim006 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims_ethnicity",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_ethnicity_code", simsobj.sims_ethnicity_code),
                          new SqlParameter("@sims_ethnicity_name_en", simsobj.sims_ethnicity_name_en),
                          new SqlParameter("@sims_ethnicity_name_ar", simsobj.sims_ethnicity_name_ar),
                          new SqlParameter("@sims_ethnicity_name_fr", simsobj.sims_ethnicity_name_fr),
                          new SqlParameter("@sims_ethnicity_name_ot",simsobj.sims_ethnicity_name_ot),
                          new SqlParameter("@sims_ethnicity_status",simsobj.sims_ethnicity_status==true?"A":"I"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}