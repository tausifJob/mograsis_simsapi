﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Controllers.setup;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/ReportingParameterController")]
    public class ReportingParameterController : ApiController
    {
        [Route("getParameter")]
        public HttpResponseMessage getParameter()
        {
            List<SAR002> lstParameter = new List<SAR002>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_reporting_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'P')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SAR002 simsobj = new SAR002();
                            
                            simsobj.comn_mod_name = dr["mod_name"].ToString();
                            simsobj.comn_appl_name = dr["comn_appl_name"].ToString();
                            
                            simsobj.comn_appl_form_field = dr["comn_appl_form_field"].ToString();
                            simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            simsobj.comn_appl_form_field_value2 = dr["comn_appl_form_field_value2"].ToString();
                            simsobj.comn_appl_form_field_value3 = dr["comn_appl_form_field_value3"].ToString();
                            simsobj.comn_appl_form_field_value4 = dr["comn_appl_form_field_value4"].ToString();
                            simsobj.comn_appl_form_field_value5 = dr["comn_appl_form_field_value5"].ToString();
                            simsobj.old_form_field = dr["old_form_field"].ToString();
                            simsobj.old_value1 = dr["old_value1"].ToString();
                            simsobj.comn_serial_number = dr["comn_serial_number"].ToString();
                            simsobj.comnapplicationcode = dr["comnapplicationcode"].ToString();
                            
                            lstParameter.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }


        [Route("getParameterbyIndex")]
        public HttpResponseMessage getParameterbyIndex()
        {
            List<Sims031> lstParameter = new List<Sims031>();
            // int total = 0, skip = 0, 
            int cnt = 1;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims031 simsobj = new Sims031();
                            simsobj.myid = cnt;
                            simsobj.comn_appl_mod_code = dr["sims_mod_code"].ToString();
                            simsobj.sims_mod_name = dr["mod_name"].ToString();
                            simsobj.comn_application_code = dr["sims_appl_code"].ToString();
                            simsobj.sims_appl_name = dr["appl_name"].ToString();
                            simsobj.sims_appl_form_field = dr["sims_appl_form_field"].ToString();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_appl_form_field_value2 = dr["sims_appl_form_field_value2"].ToString();
                            simsobj.sims_appl_form_field_value3 = dr["sims_appl_form_field_value3"].ToString();
                            simsobj.sims_appl_form_field_value4 = dr["sims_appl_form_field_value4"].ToString();
                            simsobj.appl_form_field = dr["sims_appl_form_field"].ToString();
                            simsobj.appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstParameter.Add(simsobj);
                            // total = lstParameter.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "");
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }

        [Route("getParameterbyIndex")]
        public HttpResponseMessage getParameterbyIndex(string comn_appl_code)
        {
            List<Sims031> lstParameter = new List<Sims031>();
            // int total = 0, skip = 0, 
            int cnt = 1;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@appl_code",comn_appl_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims031 simsobj = new Sims031();
                            simsobj.myid = cnt;
                            simsobj.comn_appl_mod_code = dr["sims_mod_code"].ToString();
                            simsobj.sims_mod_name = dr["mod_name"].ToString();
                            simsobj.comn_application_code = dr["sims_appl_code"].ToString();
                            simsobj.sims_appl_name = dr["appl_name"].ToString();
                            simsobj.sims_appl_form_field = dr["sims_appl_form_field"].ToString();
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_appl_form_field_value2 = dr["sims_appl_form_field_value2"].ToString();
                            simsobj.sims_appl_form_field_value3 = dr["sims_appl_form_field_value3"].ToString();
                            simsobj.sims_appl_form_field_value4 = dr["sims_appl_form_field_value4"].ToString();
                            simsobj.appl_form_field = dr["sims_appl_form_field"].ToString();
                            simsobj.appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstParameter.Add(simsobj);
                            // total = lstParameter.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                    }
                }
            }
            catch (Exception x)
            {

            } return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
            //return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }

        [Route("updateInsertDeleteParameter")]
        public HttpResponseMessage updateInsertDeleteParameter(List<Sims031> data)
        {
            Message message = new Message();
            bool inserted = false;
            //Sims031 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims031>(obj);
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims031 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_parameter_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_mod_name", simsobj.comn_appl_mod_code),
                            new SqlParameter("@sims_appl_name", simsobj.comn_application_code),
                            new SqlParameter("@sims_appl_form_field", simsobj.sims_appl_form_field),
                            new SqlParameter("@sims_appl_parameter", simsobj.sims_appl_parameter),
                            new SqlParameter("@sims_appl_form_field_value1", simsobj.sims_appl_form_field_value1),
                            new SqlParameter("@sims_appl_form_field_value2", simsobj.sims_appl_form_field_value2),
                            new SqlParameter("@sims_appl_form_field_value3", simsobj.sims_appl_form_field_value3),
                            new SqlParameter("@sims_appl_form_field_value4", simsobj.sims_appl_form_field_value4)
                         });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("updateParameterMaster")]
        public HttpResponseMessage updateParameterMaster(List<Sims031> data)
        {
            Message message = new Message();
            bool inserted = false;
            //Sims031 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims031>(obj);
            //  Sims031 []obj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims031[]>(oldvalue);
            try
            {
                if (data != null)
                {
                    //int cnt=obj1.Count();

                    using (DBConnection db = new DBConnection())
                    {
                        //if (cnt > 0)
                        // {

                        db.Open();
                        foreach (Sims031 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_parameter_proc]",
                                new List<SqlParameter>() 
                             { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_mod_name", simsobj.comn_appl_mod_code),
                                new SqlParameter("@sims_appl_name", simsobj.comn_application_code),
                          
                                new SqlParameter("@old_form_field",simsobj.appl_form_field),
                                new SqlParameter("@old_parameter",simsobj.appl_parameter),
                                new SqlParameter("@old_value1",simsobj.appl_form_field_value1),

                                new SqlParameter("@sims_appl_form_field", simsobj.sims_appl_form_field),
                                new SqlParameter("@sims_appl_parameter", simsobj.sims_appl_parameter),
                                new SqlParameter("@sims_appl_form_field_value1", simsobj.sims_appl_form_field_value1),

                                new SqlParameter("@sims_appl_form_field_value2", simsobj.sims_appl_form_field_value2),
                                new SqlParameter("@sims_appl_form_field_value3", simsobj.sims_appl_form_field_value3),
                                new SqlParameter("@sims_appl_form_field_value4", simsobj.sims_appl_form_field_value4),
                             });

                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getApplication")]
        public HttpResponseMessage getApplication(string modCode)
        {
            List<Comn004> lstCuriculum = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr",'C'),
                            new SqlParameter("@com_mode_name",modCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 sequence = new Comn004();
                            sequence.comn_application_code = dr["comn_appl_code"].ToString();
                            sequence.comn_application_name = dr["comn_appl_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getApplication")]
        public HttpResponseMessage getApplication()
        {
            List<Comn004> lstCuriculum = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_application_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'Z'),                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 sequence = new Comn004();
                            sequence.comn_application_code = dr["comn_appl_code"].ToString();
                            sequence.comn_application_name = dr["comn_appl_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("CUDReoprtParameter")]
        public HttpResponseMessage CUDReoprtParameter(List<reporting> data)
        {
            Message message = new Message();
            bool inserted = false;
            //Sims031 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims031>(obj);
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (reporting simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_reporting_parameter_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            
                            new SqlParameter("@comn_serial_number", simsobj.comn_serial_number),
                            new SqlParameter("@comn_mod_name", simsobj.comn_appl_mod_code),
                            new SqlParameter("@comn_appl_name", simsobj.comn_application_code),
                            new SqlParameter("@comn_appl_form_field", simsobj.comn_appl_form_field),
                            new SqlParameter("@comn_appl_parameter", simsobj.comn_appl_parameter),
                            new SqlParameter("@comn_appl_form_field_value1", simsobj.comn_appl_form_field_value1),
                            new SqlParameter("@comn_appl_form_field_value2", simsobj.comn_appl_form_field_value2),
                            new SqlParameter("@comn_appl_form_field_value3", simsobj.comn_appl_form_field_value3),
                            new SqlParameter("@comn_appl_form_field_value4", simsobj.comn_appl_form_field_value4),
                            new SqlParameter("@comn_appl_form_field_value5", simsobj.comn_appl_form_field_value5)

                         });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                            dr.Close();
                        }
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("getReportingParameter")]
        public HttpResponseMessage getReportingParameter()
        {
            List<reporting> lstParameter = new List<reporting>();
            // int total = 0, skip = 0, 
            int cnt = 1;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_reporting_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reporting simsobj = new reporting();
                            simsobj.myid = cnt;

                            simsobj.comn_serial_number = dr["comn_serial_number"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name = dr["mod_name"].ToString();
                            simsobj.comn_application_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_name = dr["appl_name"].ToString();
                            simsobj.comn_appl_form_field = dr["comn_appl_form_field"].ToString();
                            simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            simsobj.comn_appl_form_field_value2 = dr["comn_appl_form_field_value2"].ToString();
                            simsobj.comn_appl_form_field_value3 = dr["comn_appl_form_field_value3"].ToString();
                            simsobj.comn_appl_form_field_value4 = dr["comn_appl_form_field_value4"].ToString();
                            simsobj.comn_appl_form_field_value5 = dr["comn_appl_form_field_value5"].ToString();
                            //simsobj.appl_form_field = dr["comn_appl_form_field"].ToString();
                            //simsobj.appl_parameter = dr["comn_appl_parameter"].ToString();
                            //simsobj.appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            lstParameter.Add(simsobj);
                            // total = lstParameter.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "");
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }


        [Route("getReportingParameter")]
        public HttpResponseMessage getReportingParameter(string comn_appl_code)
        {
            List<reporting> lstParameter = new List<reporting>();
            // int total = 0, skip = 0, 
            int cnt = 1;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_reporting_parameter_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@comnapplicationcode",comn_appl_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            reporting simsobj = new reporting();
                            simsobj.myid = cnt;

                            simsobj.comn_serial_number = dr["comn_serial_number"].ToString();
                            simsobj.comn_appl_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name = dr["mod_name"].ToString();
                            simsobj.comn_application_code = dr["comn_appl_code"].ToString();
                            simsobj.comn_appl_name = dr["appl_name"].ToString();
                            simsobj.comn_appl_form_field = dr["comn_appl_form_field"].ToString();
                            simsobj.comn_appl_parameter = dr["comn_appl_parameter"].ToString();
                            simsobj.comn_appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            simsobj.comn_appl_form_field_value2 = dr["comn_appl_form_field_value2"].ToString();
                            simsobj.comn_appl_form_field_value3 = dr["comn_appl_form_field_value3"].ToString();
                            simsobj.comn_appl_form_field_value4 = dr["comn_appl_form_field_value4"].ToString();
                            simsobj.comn_appl_form_field_value5 = dr["comn_appl_form_field_value5"].ToString();
                            //simsobj.appl_form_field = dr["comn_appl_form_field"].ToString();
                            //simsobj.appl_parameter = dr["comn_appl_parameter"].ToString();
                            //simsobj.appl_form_field_value1 = dr["comn_appl_form_field_value1"].ToString();
                            lstParameter.Add(simsobj);
                            // total = lstParameter.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                    }
                }
            }
            catch (Exception x)
            {


            } return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
            //return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }




    }
}