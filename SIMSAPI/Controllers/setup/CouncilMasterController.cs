﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Setup
{
   [RoutePrefix("api/CouncilMaster")]
    public class CouncilMasterController : ApiController
    {
        [Route("getCouncil")]
        public HttpResponseMessage getCouncil()
        {
            List<Sims182> result = new List<Sims182>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_council_proc]",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims182 obj = new Sims182();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            //obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            // obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_council_code = dr["sims_council_code"].ToString();
                            obj.sims_council_name = dr["sims_council_name"].ToString();
                            obj.sims_council_status = dr["sims_council_status"].ToString() == "A";
                            obj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDCouncil")]
        public HttpResponseMessage CUDCouncil(List<Sims182> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "CUDCouncil", simsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims182 simsobj in data)
                    {

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_council_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                          new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                          new SqlParameter("@sims_council_code", simsobj.sims_council_code),
                          new SqlParameter("@sims_council_name",simsobj.sims_council_name),
                          new SqlParameter("@sims_council_status",simsobj.sims_council_status==true?"A":"I")

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        //[Route("getCuriculum")]
        //public HttpResponseMessage getCuriculum(Sims182 simsobj)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
        //    // Log.Debug(string.Format(debug, "PP", "CUDCouncil", simsobj));

        //    bool inserted = false;
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_council]",
        //                new List<SqlParameter>()
        //                {
        //                  new SqlParameter("@opr",simsobj.opr),
        //                  new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
        //                  new SqlParameter("@sims_cur_short_name_en",simsobj.sims_cur_short_name_en)


        //                });

        //            if (ins > 0)
        //            {
        //                inserted = true;
        //            }
        //            else
        //            {
        //                inserted = false;
        //            }

        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        // Log.Error(x);
        //        return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
        //}



        //[Route("getCurriculum")]
        //public HttpResponseMessage getCurriculum()
        //{
        //    List<Sims182> list = new List<Sims182>();

        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_council]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", "B"),
                            
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims182 simsobj = new Sims182();
        //                    simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //                    simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();

        //                    list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, list);
        //}


        //[Route("getYear")]
        //public HttpResponseMessage getYear()
        //{
        //    List<Sims182> list = new List<Sims182>();

        //    try
        //    {

        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_council]",
        //                new List<SqlParameter>() 
        //                 { 
        //                    new SqlParameter("@opr", "A"),
                            
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sims182 simsobj = new Sims182();
        //                    simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
        //                    simsobj.sims_academic_year = dr["sims_academic_year"].ToString();

        //                    list.Add(simsobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception x) { }
        //    return Request.CreateResponse(HttpStatusCode.OK, list);
        //}
    }
}

