﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/common/common_seq")]

    public class CommonSequenceController : ApiController
    {
        [Route("getcomn004_show")]
        public HttpResponseMessage getcomn004_show()
        {
            List<Comn004> lstseq = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_sequence_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 comseq = new Comn004();
                            comseq.comn_application_code = dr["comn_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application Name"].ToString();
                            comseq.comn_mod_code = dr["comn_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode Name"].ToString();
                            comseq.comn_squence_code = dr["comn_sequence_code"].ToString();
                            comseq.comn_description = dr["comn_description"].ToString();
                            comseq.comm_cur_code = dr["comn_cur_code"].ToString();
                            comseq.comm_cur_name = dr["sims_cur_short_name_en"].ToString();
                            comseq.comn_academic_year = dr["comn_academic_year"].ToString();
                            comseq.comn_academic_year_desc = dr["acad_yr_desc"].ToString();
                            comseq.comn_character_count = dr["comn_character_count"].ToString();
                            comseq.comn_character_squence = dr["comn_character_sequence"].ToString();
                            comseq.comm_number_count = dr["comn_number_count"].ToString();
                            comseq.comn_number_squence = dr["comn_number_sequence"].ToString();
                            bool p_ref = false;
                            if (dr["comn_parameter_ref"].ToString().Equals("A"))
                                p_ref = true;
                            else
                                p_ref = false;
                            comseq.comn_parameter_ref = p_ref;
                            lstseq.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("getcomn004_show")]
        public HttpResponseMessage getcomn004_show(string comn_appl_code)
        {
            List<Comn004> lstseq = new List<Comn004>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_sequence_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@comn_appl_code", comn_appl_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn004 comseq = new Comn004();
                            comseq.comn_application_code = dr["comn_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application Name"].ToString();
                            comseq.comn_mod_code = dr["comn_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode Name"].ToString();
                            comseq.comn_squence_code = dr["comn_sequence_code"].ToString();
                            comseq.comn_description = dr["comn_description"].ToString();
                            comseq.comm_cur_code = dr["comn_cur_code"].ToString();
                            comseq.comm_cur_name = dr["sims_cur_short_name_en"].ToString();
                            comseq.comn_academic_year = dr["comn_academic_year"].ToString();
                            comseq.comn_academic_year_desc = dr["acad_yr_desc"].ToString();
                            comseq.comn_character_count = dr["comn_character_count"].ToString();
                            comseq.comn_character_squence = dr["comn_character_sequence"].ToString();
                            comseq.comm_number_count = dr["comn_number_count"].ToString();
                            comseq.comn_number_squence = dr["comn_number_sequence"].ToString();
                            bool p_ref = false;
                            if (dr["comn_parameter_ref"].ToString().Equals("A"))
                                p_ref = true;
                            else
                                p_ref = false;
                            comseq.comn_parameter_ref = p_ref;
                            lstseq.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("comn_comn004insert")]
        public HttpResponseMessage comn_comn004insert(List<Comn004> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn004 comc in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[comn].[comn_sequence_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@comn_sequence_code", comc.comn_squence_code),
                            new SqlParameter("@comn_description", comc.comn_description),
                            new SqlParameter("@comn_cur_code", comc.comm_cur_code),
                            new SqlParameter("@comn_academic_year", comc.comn_academic_year),
                            new SqlParameter("@comn_character_count", comc.comn_character_count),
                            new SqlParameter("@comn_character_sequence", comc.comn_character_squence),
                            new SqlParameter("@comn_number_count", comc.comm_number_count),
                            new SqlParameter("@comn_number_sequence", comc.comn_number_squence),
                            new SqlParameter("comn_parameter_ref", comc.comn_parameter_ref==true?'A':'I')
                         });

                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("comn_comn004Update")]
        public HttpResponseMessage comn_comn004Update(List<Comn004> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn004 comc in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[comn].[comn_sequence_proc]",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@comn_sequence_code", comc.comn_squence_code),
                            new SqlParameter("@comn_description", comc.comn_description),
                            new SqlParameter("@comn_cur_code", comc.comm_cur_code),
                            new SqlParameter("@comn_academic_year", comc.comn_academic_year),
                            new SqlParameter("@comn_character_count", comc.comn_character_count),
                            new SqlParameter("@comn_character_sequence", comc.comn_character_squence),
                            new SqlParameter("@comn_number_count", comc.comm_number_count),
                            new SqlParameter("@comn_number_sequence", comc.comn_number_squence),
                            new SqlParameter("comn_parameter_ref", comc.comn_parameter_ref==true?'A':'I')
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getAllAcademicYear")]
        public HttpResponseMessage getAllAcademicYear(string cur)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
           // Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "W"),
                             new SqlParameter("@sims_cur_code",cur),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
               // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }


        [Route("getAllAcademicYear1")]
        public HttpResponseMessage getAllAcademicYear1(string cur)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllAcademicYear(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "AcademicYear", "AcademicYear"));

            List<Sims541> mod_list = new List<Sims541>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_academic_year_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "V"),
                             new SqlParameter("@sims_cur_code",cur),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims541 simsobj = new Sims541();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                // Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
        }




        [Route("getuserLang_show")]
        public HttpResponseMessage getuserLang_show()
        {
            List<Comn007> lstseq = new List<Comn007>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn007 comseq = new Comn007();
                            comseq.comn_application_code = dr["sims_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application_Name"].ToString();
                            
                            comseq.comn_mod_code = dr["sims_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode_Name"].ToString();

                            comseq.sims_english = dr["sims_english"].ToString();
                            comseq.sims_arabic_en = dr["sims_arabic_en"].ToString();
                            comseq.sims_english_en = dr["sims_english_en"].ToString();

                            lstseq.Add(comseq);
                             
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("getuserLangcode_show")]
        public HttpResponseMessage getuserLangcode_show(string comn_appl_code)
        {
            List<Comn007> lstseq = new List<Comn007>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", 'P'),
                             new SqlParameter("@comn_appl_code", comn_appl_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn007 comseq = new Comn007();
                            comseq.comn_application_code = dr["sims_appl_code"].ToString();
                            comseq.comn_application_name = dr["Application_Name"].ToString();

                            comseq.comn_mod_code = dr["sims_mod_code"].ToString();
                            comseq.comn_mod_name_en = dr["Mode_Name"].ToString();

                            comseq.sims_english = dr["sims_english"].ToString();
                            comseq.sims_arabic_en = dr["sims_arabic_en"].ToString();
                            comseq.sims_english_en = dr["sims_english_en"].ToString();
                           
                            lstseq.Add(comseq);
                        }
                    }
                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, lstseq);
        }

        [Route("userLang_insert")]
        public HttpResponseMessage userLang_insert(List<Comn007> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn007 comc in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_arabic_english_proc",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),
                            
                        
                         });

                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("userLang_Update")]
        public HttpResponseMessage userLang_Update(List<Comn007> data)
        {
            Message message = new Message();
            bool inserted = false;
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Comn007 comc in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("sims.sims_arabic_english_proc",
                                new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", comc.opr),
                            new SqlParameter("@comn_appl_code", comc.comn_application_code),
                            new SqlParameter("@comn_mod_code", comc.comn_mod_code),
                            new SqlParameter("@sims_english", comc.sims_english),
                            new SqlParameter("@sims_arabic_en", comc.sims_arabic_en),
                            new SqlParameter("@sims_english_en", comc.sims_english_en),
                             
                            
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}