﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/AcademicYear")]
    public class Sim001Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getAcademicYear()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));

            List<Sims001> academicyear = new List<Sims001>();
            // int total = 0, skip = 0, cnt = 1;
            int cnt = 1;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims001 simsobj = new Sims001();
                            simsobj.myid = cnt;
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_academic_year_desc = dr["sims_academic_year_description"].ToString();
                            simsobj.sims_academic_year_start_date = db.UIDDMMYYYYformat( dr["sims_academic_year_start_date"].ToString());
                            simsobj.sims_academic_year_end_date = db.UIDDMMYYYYformat( dr["sims_academic_year_end_date"].ToString());
                            //simsobj.sims_academic_year_start_date = DateTime.Parse(dr["sims_academic_year_start_date"].ToString()).ToShortDateString();
                            //simsobj.sims_academic_year_end_date = DateTime.Parse(dr["sims_academic_year_end_date"].ToString()).ToShortDateString();
                            simsobj.sims_academic_year_status = dr["acad_status"].ToString();
                            simsobj.sims_academic_year_status_name = dr["sims_academic_year_status"].ToString();
                            academicyear.Add(simsobj);
                            // total = academicyear.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, academicyear);
                    }

                }
            }
            catch (Exception x)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, academicyear);
        }

        //[Route("CUDAcademicYear")]
        //public HttpResponseMessage CUDAcademicYear(Sims001 simsobj)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDAcademicYear()PARAMETERS ::No";
        //    Log.Debug(string.Format(debug, "ERP", "SETUP"));

        //    //Sims001 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims001>(data);
        //    Message message = new Message();
        //    try
        //    {
        //        if (simsobj != null)
        //        {
        //            using (DBConnection db = new DBConnection())
        //            {
        //                //int vf = DateTime.Parse(simsobj.sims_academic_year_end_date).Year;
        //                //string end_date=(DateTime.Parse(simsobj.sims_academic_year_end_date).Year+'-'+DateTime.Parse(simsobj.sims_academic_year_end_date).Month+'-'+DateTime.Parse(simsobj.sims_academic_year_end_date).Day).ToString();
        //                //string start_date=(DateTime.Parse(simsobj.sims_academic_year_start_date).Year+'-'+DateTime.Parse(simsobj.sims_academic_year_start_date).Month+'-'+DateTime.Parse(simsobj.sims_academic_year_start_date).Day).ToString();
        //                db.Open();
        //                if (string.IsNullOrEmpty(simsobj.sims_academic_year_start_date))
        //                    simsobj.sims_academic_year_start_date = DateTime.Now.ToString();
        //                if (string.IsNullOrEmpty(simsobj.sims_academic_year_end_date))
        //                    simsobj.sims_academic_year_end_date = DateTime.Now.ToString();
        //                SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
        //                    new List<SqlParameter>() 
        //                 { 
        //                   new SqlParameter("@opr", simsobj.opr),
        //                    new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
        //                    new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
        //                    // new SqlParameter("@sims_academic_year_start_date", !string.IsNullOrEmpty(simsobj.sims_academic_year_start_date)?DateTime.Parse(simsobj.sims_academic_year_start_date):DateTime.Now),
        //                      //new SqlParameter("@sims_academic_year_start_date", !string.IsNullOrEmpty(simsobj.sims_academic_year_end_date)?DateTime.Parse(simsobj.sims_academic_year_end_date):DateTime.Now),
        //                    new SqlParameter("@sims_academic_year_start_date",DateTime.Parse(simsobj.sims_academic_year_start_date)),
        //                    new SqlParameter("@sims_academic_year_end_date", DateTime.Parse(simsobj.sims_academic_year_end_date)),
        //                    new SqlParameter("@sims_academic_year_name", simsobj.sims_academic_year_status),
        //                    new SqlParameter("@sims_academic_year_description", simsobj.sims_academic_year_desc),
        //                 });
        //                if (dr.RecordsAffected > 0)
        //                {
        //                    if (simsobj.opr.Equals("U"))
        //                        message.strMessage = "Academic Year Information Updated Sucessfully";
        //                    else if (simsobj.opr.Equals("I"))
        //                        message.strMessage = "Academic Year Information Added Sucessfully";
        //                    else if (simsobj.opr.Equals("D"))
        //                        message.strMessage = "Academic Year Information Deleted Sucessfully";

        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }
        //                if (dr.RecordsAffected < 0)
        //                {
        //                    message.strMessage = "Record Already Mapped.Can't be Deleted";
        //                    message.systemMessage = string.Empty;
        //                    message.messageType = MessageType.Success;
        //                }
        //                return Request.CreateResponse(HttpStatusCode.OK, message);
        //            }
        //        }
        //        else
        //        {
        //            message.strMessage = "Error In Parsing Information!!";
        //            message.messageType = MessageType.Error;
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        Log.Error(x);
        //        if (simsobj.opr.Equals("U"))
        //            message.strMessage = "Error In Updating Academic Year Information";
        //        else if (simsobj.opr.Equals("I"))
        //            message.strMessage = "Error In Adding Academic Year Information";
        //        else if (simsobj.opr.Equals("D"))
        //            message.strMessage = "Error In Deleting Academic Year Information";
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, message);
        //}

        [Route("CUDInsertAcademicYear")]
        public HttpResponseMessage CUDInsertAcademicYear(List<Sims001> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDAcademicYear()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims001 simsobj in data)
                        {
                            if (string.IsNullOrEmpty(simsobj.sims_academic_year_start_date))
                                simsobj.sims_academic_year_start_date = DateTime.Now.ToString();
                            if (string.IsNullOrEmpty(simsobj.sims_academic_year_end_date))
                                simsobj.sims_academic_year_end_date = DateTime.Now.ToString();

                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_academic_year_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_academic_year_start_date",db.DBYYYYMMDDformat(simsobj.sims_academic_year_start_date)),
                            new SqlParameter("@sims_academic_year_end_date", db.DBYYYYMMDDformat(simsobj.sims_academic_year_end_date)),
                            new SqlParameter("@sims_academic_year_name", simsobj.sims_academic_year_status),
                            new SqlParameter("@sims_academic_year_description", simsobj.sims_academic_year_desc),
                         });
                            if (ins > 0)
                            
                                //string cnt = dr["param_cnt"].ToString();
                                //if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDAcademicYear")]
        public HttpResponseMessage CUDAcademicYear(List<Sims001> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDAcademicYear()PARAMETERS ::No";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims001 simsobj in data)
                        {
                            if (string.IsNullOrEmpty(simsobj.sims_academic_year_start_date))
                                simsobj.sims_academic_year_start_date = DateTime.Now.ToString();
                            if (string.IsNullOrEmpty(simsobj.sims_academic_year_end_date))
                                simsobj.sims_academic_year_end_date = DateTime.Now.ToString();

                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_academic_year_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_academic_year_start_date",db.DBYYYYMMDDformat(simsobj.sims_academic_year_start_date)),
                            new SqlParameter("@sims_academic_year_end_date",db.DBYYYYMMDDformat(simsobj.sims_academic_year_end_date)),
                            new SqlParameter("@sims_academic_year_name", simsobj.sims_academic_year_status),
                            new SqlParameter("@sims_academic_year_description", simsobj.sims_academic_year_desc),
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims001> lstCuriculum = new List<Sims001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@tbl_name", "sims.sims_cur"),
                            new SqlParameter("@tbl_col_name1", "*"),
                           new SqlParameter("@tbl_cond", "sims_cur_status='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims001 cur = new Sims001();
                            cur.sims_cur_code = dr["sims_cur_code"].ToString();
                            cur.sims_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAcademicStatus")]
        public HttpResponseMessage getAcademicStatus()
        {
            List<Sims001> mod_list = new List<Sims001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>() 
                         {      
                            new SqlParameter("@opr",'A'),
                           
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims001 comnobj = new Sims001();
                            comnobj.sims_academic_year_status_name = dr["sims_appl_form_field_value1"].ToString();
                            comnobj.sims_academic_year_status = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(comnobj);
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }
    }
}