﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/building")]
    public class BuildingController : ApiController
    {
        [Route("getBuilding")]
        public HttpResponseMessage getBuilding()
        {
            List<Sim162> building_list = new List<Sim162>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_building_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim162 obj = new Sim162();

                            obj.sims_building_code = dr["sims_building_code"].ToString();
                            obj.sims_building_desc = dr["sims_building_desc"].ToString();
                            obj.sims_buiding_student_capacity = dr["sims_buiding_student_capacity"].ToString();

                            building_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, building_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, building_list);
        }

        [Route("BuildingCUD")]
        public HttpResponseMessage BuildingCUD(List<Sim162> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "LocationCUD", simsobj));

            int i = 0;


            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim162 simsobj in data)
                    {
                        if (simsobj.opr == "I")
                        {
                            i = Convert.ToInt32(simsobj.sims_building_code);
                            if (i < 10)
                            {
                                simsobj.sims_building_code = "0" + i;
                            }
                        }
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_building_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_building_code",simsobj.sims_building_code),
                          new SqlParameter("@sims_building_desc", simsobj.sims_building_desc),
                          new SqlParameter("@sims_building_student_capacity", simsobj.sims_buiding_student_capacity),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }

                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}


























    
