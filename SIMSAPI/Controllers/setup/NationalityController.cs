﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;

namespace SIMSAPI.Controllers.modules.setup
{
    [RoutePrefix("api/common/Nationality")]
    public class NationalityController : ApiController
    {
        public int sims_country_name_en { get; private set; }

        [Route("getAllNationalityDetails")]
        public HttpResponseMessage getAllNationalityDetails() //string data, string opr
        {
            List<Sims008> nationalitydata = new List<Sims008>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_nationality_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims008 obj = new Sims008();
                            obj.nationality_code = dr["sims_nationality_code"].ToString();
                            obj.sims_country_code = dr["sims_country_code"].ToString();
                            obj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            obj.nationality_name_eng = dr["sims_nationality_name_en"].ToString();
                            obj.nationality_name_ar = dr["sims_nationality_name_ar"].ToString();
                            obj.nationality_name_fr = dr["sims_nationality_name_fr"].ToString();
                            obj.nationality_name_ot = dr["sims_nationality_name_ot"].ToString();
                            obj.nationality_status = dr["sims_status"].ToString().Equals("A") ? true : false;

                            nationalitydata.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, nationalitydata);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, nationalitydata);
        }

        [Route("getAllCountries")]
        public HttpResponseMessage getAllCountries() //string data, string opr
        {
            List<Sims008> countryname = new List<Sims008>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_nationality_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims008 obj = new Sims008();
                            obj.sims_country_code = dr["sims_country_code"].ToString();
                            obj.sims_country_name_en = dr["sims_country_name_en"].ToString();

                            countryname.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, countryname);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, countryname);
        }

        [Route("NationalityCUD")]
        public HttpResponseMessage NationalityCUD(List<Sims008> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims008 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_nationality_proc]",
                            new List<SqlParameter>()

                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_nationality_code", simsobj.nationality_code),
                                 new SqlParameter("@sims_country_code", simsobj.sims_country_code),
                                 new SqlParameter("@sims_nationality_name_en", simsobj.nationality_name_eng),
                                 new SqlParameter("@sims_nationality_name_ar", simsobj.nationality_name_ar),
                                 new SqlParameter("@sims_nationality_name_fr", simsobj.nationality_name_fr),
                                 new SqlParameter("@sims_nationality_name_ot", simsobj.nationality_name_ot),
                                 new SqlParameter("@sims_status", simsobj.nationality_status==true?"A":"I")


                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}