﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.UserApplicationsFinsController
{


    [RoutePrefix("api/common/UserApplicationsFinsController")]
    [BasicAuthentication]
    public class UserApplicationsFinsController : ApiController
    {

        [Route("GetCompany")]
        public HttpResponseMessage GetCompany()
        {
            List<Comn019> mod_list = new List<Comn019>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "fins.fins_company_master"),
                            new SqlParameter("@tbl_col_name1", "comp_code,comp_name, comp_short_name"),
                        
                            new SqlParameter("@tbl_OrdBy", "comp_code")
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn019 comnObj = new Comn019();
                            comnObj.fins_comp_code = dr["comp_code"].ToString();
                            comnObj.fins_comp_name = dr["comp_name"].ToString();
                            comnObj.fins_comp_short_name = dr["comp_short_name"].ToString();
                            mod_list.Add(comnObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetCompanyStatusWithAppl")]
        public HttpResponseMessage GetCompanyStatusWithAppl (string userName)
        {
            
            List<Comn019> user_app = new List<Comn019>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application_fins",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@comn_user_name", userName)
             
                           
                          
                         });
                    if (dr.HasRows)
                    {
                        int cnt = 0;
                        while (dr.Read())
                        {
                            Comn019 app = new Comn019();
                            app.fins_comn_user_appl_code = dr["comn_appl_code"].ToString();
                            app.fins_comn_user_appl_name = dr["comn_appl_name_en"].ToString();

                            #region Company
                            for (int i = 2; i < dr.FieldCount; i++)
                            {
                                //  string colname = dr.GetFieldValue<string>(i).ToString();

                                int j = i - 2;
                                j++;
                                bool status = false;
                                if (string.IsNullOrEmpty(dr[i].ToString()) || dr[i].ToString() == "I")
                                    status = false;
                                else
                                    status = true;

                                switch (j)
                                {

                                    case 1:
                                        app.comp_status_1 = status;
                                        app.comp_code_1 = string.Empty;
                                        break;
                                    case 2:
                                        app.comp_status_2 = status;
                                        app.comp_code_2 = string.Empty;
                                        break;
                                    case 3:
                                        app.comp_status_3 = status;
                                        app.comp_code_3 = string.Empty;
                                        break;
                                    case 4:
                                        app.comp_status_4 = status;
                                        app.comp_code_4 = string.Empty;
                                        break;
                                    case 5:
                                        app.comp_status_5 = status;
                                        app.comp_code_5 = string.Empty;
                                        break;
                                    case 6:
                                        app.comp_status_6 = status;
                                        app.comp_code_6 = string.Empty;
                                        break;
                                    case 7:
                                        app.comp_status_7 = status;
                                        app.comp_code_7 = string.Empty;
                                        break;
                                    case 8:
                                        app.comp_status_8 = status;
                                        app.comp_code_8 = string.Empty;
                                        break;
                                    case 9:
                                        app.comp_status_9 = status;
                                        app.comp_code_9 = string.Empty;
                                        break;
                                    case 10:
                                        app.comp_status_10 = status;
                                        app.comp_code_10 = string.Empty;
                                        break;
                                }
                            }
                            #endregion

                            user_app.Add(app);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, user_app);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, user_app);
            }
        }


      

        [Route("InsertApplications")]
        public HttpResponseMessage InsertApplications(List<Comn019> collection1)
        {
            //  Comn006 collection=
            //  Comn006 collection = Newtonsoft.Json.JsonConvert.DeserializeObject<Comn006>(collection1);

            bool flag = false;
            foreach (Comn019 obj in collection1)
            {


                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", "I"));
                        para.Add(new SqlParameter("@comn_user_name", obj.username));
                       para.Add(new SqlParameter("@comn_user_appl_code", obj.fins_comn_user_appl_code));
                        para.Add(new SqlParameter("@comn_user_appl_fins_comp_code", obj.comp_code));
                        if (obj.comn_user_appl_fins_comp_status)
                        para.Add(new SqlParameter("@comn_user_appl_fins_comp_status",'A' ));
                        else
                            para.Add(new SqlParameter("@comn_user_appl_fins_comp_status", 'I'));

                         SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application_fins", para);
                        flag = dr.RecordsAffected > 0 ? true : false;
                    }
                    //  return Request.CreateResponse(HttpStatusCode.OK, flag);
                }
                catch (Exception e)
                {
                     return Request.CreateResponse(HttpStatusCode.OK, e.Message);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);


        }


    }
}   