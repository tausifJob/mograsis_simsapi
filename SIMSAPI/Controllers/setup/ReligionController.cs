﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
//using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
//using System.Web.Services.Description;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using System.Web;

namespace SIMSAPI.Controllers.modules.ReligionController
{
    [RoutePrefix("api/ReligionDetails")]
    public class ReligionController : ApiController
    {


        [Route("getReligion")]
        public HttpResponseMessage getReligion()
        {
            List<Sims009> religion_list = new List<Sims009>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_religion_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@OPR", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims009 obj = new Sims009();


                            obj.sims_religion_code = dr["sims_religion_code"].ToString();
                            obj.sims_religion_name_en = dr["sims_religion_name_en"].ToString();
                            obj.sims_religion_name_ar = dr["sims_religion_name_ar"].ToString();
                            obj.sims_religion_name_fr = dr["sims_religion_name_fr"].ToString();
                            obj.sims_religion_name_ot = dr["sims_religion_name_ot"].ToString();
                            obj.sims_religion_img = dr["sims_religion_img"].ToString();
                            obj.sims_religion_status = dr["sims_religion_status"].ToString().Equals("A") ? true : false;


                            religion_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, religion_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, religion_list);
        }


        [Route("ReligionCUD")]
        public HttpResponseMessage ReligionCUD(List<Sims009> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims009 simsobj in data)
                    {
                        if (simsobj.sims_religion_code == "")
                        {
                            simsobj.sims_religion_code = null;
                        }
                        if (simsobj.sims_religion_name_en == "")
                        {
                            simsobj.sims_religion_name_en = null;
                        }
                        if (simsobj.sims_religion_name_ar == "")
                        {
                            simsobj.sims_religion_name_ar = null;
                        }
                        if (simsobj.sims_religion_name_fr == "")
                        {
                            simsobj.sims_religion_name_fr = null;
                        }
                        if (simsobj.sims_religion_name_ot == "")
                        {
                            simsobj.sims_religion_name_ot = null;
                        }
                        if (simsobj.sims_religion_img == "")
                        {
                            simsobj.sims_religion_img = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_religion_proc]",
                            new List<SqlParameter>()
                        {
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_religion_code", simsobj.sims_religion_code),
                                 new SqlParameter("@sims_religion_name_en", simsobj.sims_religion_name_en),
                                 new SqlParameter("@sims_religion_name_ar", simsobj.sims_religion_name_ar),
                                 new SqlParameter("@sims_religion_name_fr", simsobj.sims_religion_name_fr),
                                 new SqlParameter("@sims_religion_name_ot", simsobj.sims_religion_name_ot),
                                 new SqlParameter("@sims_religion_img", simsobj.sims_religion_img),
                                 new SqlParameter("@sims_religion_status", simsobj.sims_religion_status==true?"A":"I")
                                 
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }

}