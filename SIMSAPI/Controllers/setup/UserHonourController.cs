﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Web;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/UserHonour")]
    public class UserHonourController:ApiController
    {

        [Route("getStudentsDetails")]
        public HttpResponseMessage getStudentsDetails(string curcode, string academicyear, string gradecode, string section)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<Sim614> com_list = new List<Sim614>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try

            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_user_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sims_honour_cur_code", curcode),
                             new SqlParameter("@sims_honour_academic_year", academicyear),
                             new SqlParameter("@sims_honor_grade_code", gradecode),
                             new SqlParameter("@sims_honor_section_code", section),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim614 sequence = new Sim614();

                            sequence.sims_honor_user_number = dr["sims_honor_user_number"].ToString();
                            sequence.sims_honour_cur_code = dr["sims_honour_cur_code"].ToString();
                            sequence.sims_honour_cur_name = dr["sims_honour_cur_name"].ToString();
                            sequence.fullname = dr["fullname"].ToString();
                            sequence.sims_honour_academic_year = dr["sims_honour_academic_year"].ToString();
                            sequence.year_desc = dr["year_desc"].ToString();
                            sequence.sims_honour_code = dr["sims_honour_code"].ToString();
                            sequence.sims_honor_grade_code = dr["sims_honor_grade_code"].ToString();
                            sequence.grade_name = dr["grade_name"].ToString();
                            sequence.sims_honor_section_code = dr["sims_honor_section_code"].ToString();
                            sequence.section_name = dr["section_name"].ToString();
                            sequence.gradesection = dr["gradesection"].ToString();
                            sequence.sims_honor_enroll_number = dr["sims_honor_enroll_number"].ToString();
                            sequence.image = dr["image"].ToString();
                            sequence.sims_honour_value = dr["sims_honour_value"].ToString();
                            sequence.sims_honour_user_status = dr["sims_honour_user_status"].ToString();


                            com_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("getEmployeeDetails")]
        public HttpResponseMessage getEmployeeDetails(string dgcode, string deptcode, string comp_code, string year)
        {
            string cur_code = string.Empty, str = string.Empty;
            List<Sim614> com_list = new List<Sim614>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try

            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_user_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@em_dept_code", deptcode),
                             new SqlParameter("@em_desg_code", dgcode),
                             new SqlParameter("@finance_year", year),
                             new SqlParameter("@comp_code", comp_code),


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim614 sequence = new Sim614();

                            sequence.emp_Name = dr["emp_Name"].ToString();
                            sequence.comp_name = dr["comp_name"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            sequence.em_img = dr["em_img"].ToString();
                            sequence.sims_honour_value = dr["sims_honour_value"].ToString();
                            sequence.sims_honor_emp_code = dr["sims_honor_emp_code"].ToString();
                          
                            com_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

        [Route("getDepartmentDetails")]
        public HttpResponseMessage getDepartmentDetails()
        {
            string cur_code = string.Empty, str = string.Empty;
            List<Sim614> com_list = new List<Sim614>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try

            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_user_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "V"),
                        


                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim614 sequence = new Sim614();

                            sequence.codp_dept_no = dr["codp_dept_no"].ToString();
                            sequence.codp_dept_name = dr["codp_dept_name"].ToString();
                           
                            com_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }


        [Route("getDesginationDetails")]
        public HttpResponseMessage getDesginationDetails()
        {
            string cur_code = string.Empty, str = string.Empty;
            List<Sim614> com_list = new List<Sim614>();
            string location = "", root = "";

            var schoolname = HttpContext.Current.Request.Headers["schoolId"].ToString();
            string path = "~/Content/" + schoolname + "/Images/" + location + "/";

            root = HttpContext.Current.Server.MapPath(path);
            try

            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_honour_user_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "W"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim614 sequence = new Sim614();

                            sequence.dg_code = dr["dg_code"].ToString();
                            sequence.dg_desc = dr["dg_desc"].ToString();
                            com_list.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, com_list);
        }

    }
}