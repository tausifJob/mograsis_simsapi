﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/createeditsibling")]
    public class CreateEditSiblingController : ApiController
    {

        [Route("GetSims040")]
        public HttpResponseMessage GetSims040(string sib_enroll ,string opr)
        {

            List<Uccw035> lst = new List<Uccw035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         { 
                
                new SqlParameter("@OPR", opr),
                new SqlParameter("@sims_sibling_student_enroll_number", sib_enroll),
                new SqlParameter("@sims_sibling_parent_number", ""),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw035 sd = new Uccw035();
                            sd.parent_number = dr["sims_parent_number"].ToString();
                            sd.parent_name = dr["ParentName"].ToString() + " (" + sd.parent_number + " )";
                            sd.sib_admiss_date = dr["sims_student_date"].ToString();
                            sd.sib_class = dr["Class"].ToString();
                            sd.sib_enroll = dr["sims_sibling_student_enroll_number"].ToString();
                            sd.sib_gender = dr["gender"].ToString();
                            sd.sib_name = dr["Student_Name"].ToString() + " (" + sd.sib_enroll + " )";
                            sd.sib_photo = dr["sims_student_img"].ToString();
                            sd.academic_year = dr["sims_academic_year"].ToString();
                            sd.grade_code = dr["sims_grade_code"].ToString();
                            sd.section_code = dr["sims_section_code"].ToString();
                            sd.curr_code = dr["sims_cur_code"].ToString();
                            lst.Add(sd);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("Sims040Delete")]
        public HttpResponseMessage Sims040Delete(Sims040 simsobj)
        {

            bool deleted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr", 'D'),
                new SqlParameter("@sims_sibling_code_list", simsobj.sibling_student_enrollNo),
                new SqlParameter("@sims_sibling_parent_number", simsobj.sims_sibling_parent_number),
                         });

                    if (dr.RecordsAffected > 0)
                    {
                        deleted = true;
                    }
                    else
                    {
                        deleted = false;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, deleted);
            }

            catch (Exception e)
            {
                //deleted = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, deleted);
        }

        [Route("Sims040Insert")]
        public HttpResponseMessage Sims040Insert(Sims040 simsobj)
        {
            bool res = false;
            try
            {
                
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                            new List<SqlParameter>() 
                         { 
                    new SqlParameter("@opr", "I"),
                    new SqlParameter("@sims_sibling_code_list", simsobj.sibling_student_enrollNo),
                    new SqlParameter("@sims_sibling_parent_number", simsobj.sibling_parent_code),
                });
                        if (dr.RecordsAffected > 0)
                            res = true;
                        else
                            res = false;

                    }
               
                return Request.CreateResponse(HttpStatusCode.OK, res);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, res);



        }

        [Route("Sims040Update")]
        public HttpResponseMessage Sims040Update(Sims040 simsobj)
        {

            bool updated = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", "U"),
                new SqlParameter("@sims_sibling_code_list", simsobj.sibling_student_enrollNo),
                new SqlParameter("@sims_sibling_parent_number", simsobj.sims_sibling_parent_number),
                new SqlParameter("@sims_sibling_old_parent_number", simsobj.sims_sibling_old_parent_number)
                });

                    if (dr.RecordsAffected > 0)
                        updated = true;
                    else
                        updated = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, updated);

            }
            catch (Exception e)
            {
               // updated = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, updated);

        }


        [Route("Getunmappingparent")]
        public HttpResponseMessage Getunmappingparent(string parentid)
        {

            List<Uccw035> lst = new List<Uccw035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         { 
                
                new SqlParameter("@opr", "B"),
                //new SqlParameter("@sims_sibling_student_enroll_number", sib_enroll),
                new SqlParameter("@sims_sibling_parent_number", parentid),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw035 sd = new Uccw035();
                            sd.parent_number = dr["sims_parent_number"].ToString();
                            //if (string.IsNullOrEmpty(dr["im_inv_no"].ToString()) == false)
                            if (string.IsNullOrEmpty(dr["ParentName"].ToString()) == false)
                            {
                                sd.parent_name = dr["ParentName"].ToString() + " (" + sd.parent_number + " )";
                                sd.sims_parent_father_nationality1_code = dr["sims_parent_father_nationality1_code1"].ToString();
                                sd.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                sd.sims_parent_father_country_code = dr["sims_parent_father_country_code1"].ToString();
                                sd.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                sd.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                sd.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                                sd.sims_parent_father_img = dr["sims_parent_father_img"].ToString();


                            }
                            else if (string.IsNullOrEmpty(dr["MotherName"].ToString()) == false)
                            {
                                sd.parent_name = dr["MotherName"].ToString() + " (" + sd.parent_number + " )";

                                sd.sims_parent_father_nationality1_code = dr["sims_parent_mother_nationality1_code1"].ToString();
                                sd.sims_parent_father_summary_address = dr["sims_parent_mother_summary_address"].ToString();
                                sd.sims_parent_father_country_code = dr["sims_parent_mother_country_code1"].ToString();
                                sd.sims_parent_father_mobile = dr["sims_parent_mother_mobile"].ToString();
                                sd.sims_parent_father_email = dr["sims_parent_mother_email"].ToString();
                                sd.sims_parent_father_passport_number = dr["sims_parent_mother_passport_number"].ToString();
                                sd.sims_parent_father_img = dr["sims_parent_mother_img"].ToString();

                            }
                            else if (string.IsNullOrEmpty(dr["GuardianName"].ToString()) == false)
                            {
                                sd.parent_name = dr["GuardianName"].ToString() + " (" + sd.parent_number + " )";

                                sd.sims_parent_father_nationality1_code = dr["sims_parent_guardian_nationality1_code1"].ToString();
                                sd.sims_parent_father_summary_address = dr["sims_parent_guardian_summary_address"].ToString();
                                sd.sims_parent_father_country_code = dr["sims_parent_guardian_country_code1"].ToString();
                                sd.sims_parent_father_mobile = dr["sims_parent_guardian_mobile"].ToString();
                                sd.sims_parent_father_email = dr["sims_parent_guardian_email"].ToString();
                                sd.sims_parent_father_passport_number = dr["sims_parent_guardian_passport_number"].ToString();
                                sd.sims_parent_father_img = dr["sims_parent_guardian_img"].ToString();

                            }

                            else
                            {
                                sd.parent_name = dr["ParentName"].ToString() + " (" + sd.parent_number + " )";
                                sd.sims_parent_father_nationality1_code = dr["sims_parent_father_nationality1_code1"].ToString();
                                sd.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                sd.sims_parent_father_country_code = dr["sims_parent_father_country_code1"].ToString();
                                sd.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                sd.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                sd.sims_parent_father_passport_number = dr["sims_parent_father_passport_number"].ToString();
                                sd.sims_parent_father_img = dr["sims_parent_father_img"].ToString();


                            }
                            lst.Add(sd);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("GetunmappingStudent")]
        public HttpResponseMessage GetunmappingStudent(string studentid)
        {

            List<Uccw035> lst = new List<Uccw035>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         { 
                
                new SqlParameter("@opr", "E"),
                new SqlParameter("@sims_sibling_code_list", studentid),
                
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Uccw035 sd = new Uccw035();
                            sd.sib_admiss_date = dr["sims_student_date"].ToString();
                            sd.sib_class = dr["Class"].ToString();
                            sd.sib_enroll = dr["sims_student_enroll_number"].ToString();
                            sd.sib_gender = dr["gender"].ToString();
                            sd.sib_name = dr["Student_Name"].ToString() + " (" + sd.sib_enroll + " )";
                            sd.sib_photo = dr["sims_student_img"].ToString();
                            sd.academic_year = dr["sims_academic_year"].ToString();
                            sd.grade_code = dr["sims_grade_code"].ToString();
                            sd.section_code = dr["sims_section_code"].ToString();
                            sd.curr_code = dr["sims_cur_code"].ToString();
                            lst.Add(sd);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, lst);

        }

        [Route("mappedStudenttounmappedparent")]
        public HttpResponseMessage mappedStudenttounmappedparent(Sims040 simsobj)
        {

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", "F"),
                new SqlParameter("@sims_sibling_code_list", simsobj.sibling_student_enrollNo),
                new SqlParameter("@sims_sibling_parent_number", simsobj.sims_sibling_parent_number),
               
                });

                    if (dr.RecordsAffected > 0)
                        insert = true;
                    else
                        insert = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, insert);

            }
            catch (Exception e)
            {
                //insert = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

        [Route("mappedStudenttomappedparent")]
        public HttpResponseMessage mappedStudenttomappedparent(Sims040 simsobj)
        {

            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_sibling_proc]",
                        new List<SqlParameter>() 
                         {
                new SqlParameter("@opr", "G"),
                new SqlParameter("@sims_sibling_code_list", simsobj.sibling_student_enrollNo),
                new SqlParameter("@sims_sibling_parent_number", simsobj.sims_sibling_parent_number),
               
                });

                    if (dr.RecordsAffected > 0)
                        insert = true;
                    else
                        insert = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, insert);

            }
            catch (Exception e)
            {
                //insert = false;
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);

        }

    }
}