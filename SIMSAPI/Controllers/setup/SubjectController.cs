﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Subject")]

    public class Sim045Controller : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getAutoGeneratesubjectCode")]
        public HttpResponseMessage getAutoGeneratesubjectCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGeneratesubjectCode(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGeneratesubjectCode"));

            string commcode = string.Empty;
            Sims045 simsobj = new Sims045();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'F'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.subject_code = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj.subject_code);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj.subject_code);
            }
        }

        [Route("getSubject")]
        public HttpResponseMessage getSubject()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceCriteria(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Subject", "ATTENDANCECRITERIA"));
            List<Sims045> Subject = new List<Sims045>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        
                        while (dr.Read())
                        {
                            Sims045 simsobj = new Sims045();
                            simsobj.sims_attendance_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.subject_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.subject_code = dr["sims_subject_code"].ToString();
                            simsobj.subject_group_code = dr["sims_subject_group_code"].ToString();
                            simsobj.subject_group_code_name = dr["sims_subject_group_name_en"].ToString();
                            simsobj.subject_name_en = dr["sims_subject_name_en"].ToString();
                            simsobj.subject_name_ar = dr["sims_subject_name_ar"].ToString();
                            simsobj.subject_name_fr = dr["sims_subject_name_fr"].ToString();
                            simsobj.subject_name_ot = dr["sims_subject_name_ot"].ToString();
                            simsobj.subject_name_abbr = dr["sims_subject_name_abbr"].ToString();
                            simsobj.subject_moe_code = dr["sims_subject_moe_code"].ToString();
                            simsobj.subject_status = dr["sims_subject_status"].ToString().Equals("A") ? true : false;
                            simsobj.subject_type = dr["sims_subject_type"].ToString();
                            simsobj.subject_type_name = dr["sims_subject_type_name_en"].ToString();
                            simsobj.subject_country_code = dr["sims_country_code"].ToString();
                            simsobj.subject_country_code_name = dr["sims_country_name_en"].ToString();
                            simsobj.sims_subject_color_code = dr["sims_subject_color"].ToString();
                            simsobj.sims_religion_language = dr["sims_religion_language"].ToString();
                            Subject.Add(simsobj);
                            //    total = Subject.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, Subject);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, Subject);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, Subject);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, Subject);
        }

        [Route("getCuriculum")]
        public HttpResponseMessage getCuriculum()
        {
            List<Sims045> lstCuriculum = new List<Sims045>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@tbl_name", "sims.sims_cur"),
                            new SqlParameter("@tbl_col_name1", "*"),
                           new SqlParameter("@tbl_cond", "sims_cur_status='A'")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims045 cur = new Sims045();
                            cur.subject_cur_code = dr["sims_cur_code"].ToString();
                            cur.subject_cur_code_name = dr["sims_cur_short_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getSubjectGroup")]
        public HttpResponseMessage getSubjectGroup(string curCode)
        {
            List<Sims045> lstCuriculum = new List<Sims045>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                           new SqlParameter("@sims_cur_code",curCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims045 cur = new Sims045();
                            cur.subject_group_code = dr["sims_subject_group_code"].ToString();
                            cur.subject_group_code_name = dr["sims_subject_group_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getSubjectType")]
        public HttpResponseMessage getSubjectType(string curCode)
        {
            List<Sims045> lstCuriculum = new List<Sims045>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C'),
                            new SqlParameter("@sims_cur_code",curCode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims045 cur = new Sims045();
                            cur.subject_type = dr["sims_subject_type_code"].ToString();
                            cur.subject_type_name = dr["sims_subject_type_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getCountry")]
        public HttpResponseMessage getCountry()
        {
            List<Sims045> lstCuriculum = new List<Sims045>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'E'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims045 cur = new Sims045();
                            cur.subject_country_code = dr["sims_country_code"].ToString();
                            cur.subject_country_code_name = dr["sims_country_name_en"].ToString();
                            lstCuriculum.Add(cur);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("updateInsertDeleteSubject")]
        public HttpResponseMessage updateInsertDeleteSubject(List<Sims045> data)
        {
            bool inserted = false;
            // Sims045 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims045>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims045 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_subject_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_attendance_cur_code),
                            new SqlParameter("@sims_subject_code", simsobj.subject_code),
                            new SqlParameter("@sims_subject_group_code", simsobj.subject_group_code),
                            new SqlParameter("@sims_subject_name_en", simsobj.subject_name_en),
                            new SqlParameter("@sims_subject_name_ar", simsobj.subject_name_ar),
                            new SqlParameter("@sims_subject_name_fr", simsobj.subject_name_fr),
                            new SqlParameter("@sims_subject_name_ot", simsobj.subject_name_ot),
                            new SqlParameter("@sims_subject_name_abbr", simsobj.subject_name_abbr),
                            new SqlParameter("@sims_subject_moe_code", simsobj.subject_moe_code),
                            new SqlParameter("@sims_subject_status", simsobj.subject_status==true?"A":"I"),
                            new SqlParameter("@sims_subject_type", simsobj.subject_type),
                            new SqlParameter("@sims_country_code", simsobj.subject_country_code),
                            new SqlParameter("@sims_subject_color", simsobj.sims_subject_color_code),
                            new SqlParameter("@sims_religion_language", simsobj.sims_religion_language),

                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CheckSubject")]
        public HttpResponseMessage CheckSubject(string SubjectCode, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckTypeCode(),PARAMETERS :: SUBJECTCODE{2},cur_code{3}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckSubject", SubjectCode, cur_code));

            bool ifexists = false;
            List<Sims045> Subject = new List<Sims045>();
            Sims045 simsobj = new Sims045();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_subject_code", SubjectCode),
                                new SqlParameter("@sims_cur_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {

                    simsobj.status = true;
                    simsobj.strMessage = "Subject Already Exists";
                    Subject.Add(simsobj);
                }
                else
                {
                    simsobj.status = false;
                    simsobj.strMessage = "No Records Found";
                    Subject.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj);
            }
        }

    }
}