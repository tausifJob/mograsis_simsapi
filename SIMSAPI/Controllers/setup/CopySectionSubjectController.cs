﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;



namespace SIMSAPI.Controllers.setup
{

    [RoutePrefix("api/common/SectionSubject")]
    public class CopySectionSubjectController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSectionNames")]
        public HttpResponseMessage getSectionNames(string curcode, string gradecode, string academicyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionNames(),PARAMETERS :: CURCODE{2},GRADECODE{3},ACAYEAR{4}";
            Log.Debug(string.Format(debug, "STUDENT", "getSectionNames", curcode, gradecode, academicyear));

            List<Sims039> section_list = new List<Sims039>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'G'),
                                new SqlParameter("@sims_cur_code", curcode),
                                new SqlParameter("@sims_grade_code", gradecode),
                                new SqlParameter("@sims_academic_year", academicyear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims039 simsobj = new Sims039();
                            simsobj.sims_section_name = dr["sims_section_name_en"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            section_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, section_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, section_list);
            }
        }

        [Route("getSubjectByIndex")]
        public HttpResponseMessage getSubjectByIndex(string curcode, string gradecode, string academicyear, string sectioncode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjectByIndex(),PARAMETERS  :: CURCODE{2},GRADECODE{3},ACAYEAR{4},SECTIONCODE{5}";
            Log.Debug(string.Format(debug, "STUDENT", "getSubjectByIndex", curcode, gradecode, academicyear, sectioncode));

            List<Sims039> subject_list = new List<Sims039>();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_cur_code", curcode),
                                new SqlParameter("@sims_grade_code", gradecode),
                                new SqlParameter("@sims_academic_year", academicyear),
                                new SqlParameter("@sims_section_code", sectioncode),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims039 simsobj = new Sims039();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_abbr = dr["sims_subject_name_abbr"].ToString();
                            simsobj.sims_moe_code = dr["sims_subject_moe_code"].ToString();
                            simsobj.sims_subject_type_name = dr["sims_subject_type_name_en"].ToString();
                            simsobj.sims_subject_group_code = dr["sims_subject_group_code"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            simsobj.sims_display_order = dr["sims_display_order"].ToString();
                            simsobj.sims_subject_mdl_course_id= dr["sims_subject_mdl_course_id"].ToString();
                            simsobj.sims_section_subject_credit_hours = dr["sims_section_subject_credit_hours"].ToString();
                            if (dr["sub_status"].ToString() == "A")
                                simsobj.sims_subject_status = true;
                            subject_list.Add(simsobj);


                            
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, subject_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subject_list);
            }
        }

        /* for copy subject one section to another */

        [Route("CUDSectionSubjectDetails")]
        public HttpResponseMessage CUDSectionSubjectDetails(Sims039 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSectionSubjectDetails(),PARAMETERS :: OPR {2}";
            Log.Debug(string.Format(debug, "STUDENT", "CUDSectionSubjectDetails", data));

            Sims039 simsobj = data;
            Message message = new Message();
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_proc",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                            new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                            new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                            new SqlParameter("@sims_section_code_list", simsobj.sims_section_code),
                            new SqlParameter("@sims_subject_code_list", simsobj.sims_subject_code),
                            new SqlParameter("@sims_section_code",simsobj.sims_section_name)
                           
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            if (simsobj.opr.Equals("U"))
                                message.strMessage = "Section Subject  Updated Sucessfully  ";
                            else if (simsobj.opr.Equals("E"))
                                message.strMessage = "Section Subject Inserted Sucessfully ";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        else
                        {
                            if (simsobj.opr.Equals("U"))
                                message.strMessage = "SectionSubject Details Not Updated ";
                            else if (simsobj.opr.Equals("E"))
                                message.strMessage = "Subject Allready Exist Choose Another Subject ";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "Subject Allready Exist Choose Another Subject";
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("getGradeNameYearly")]
        public HttpResponseMessage GradeNameYearly(string curcode, string academicyear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GradeNameYearly(),PARAMETERS :: CURCODE{2},ACAYEAR{3}";
            Log.Debug(string.Format(debug, "STUDENT", "GradeNameYearly", curcode, academicyear));

            List<Sims039> grade_list = new List<Sims039>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_section_subject_proc",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'H'),
                                new SqlParameter("@sims_cur_code", curcode),
                                new SqlParameter("@sims_academic_year", academicyear),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims039 simsobj = new Sims039();
                            simsobj.sims_grade_name = dr["sims_grade_name_en"].ToString();
                            grade_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, grade_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, grade_list);
            }
        }

        [Route("getSectionSubject")]
        public HttpResponseMessage getSectionSubject(string curcode, string gradecode, string academicyear, string sectioncode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionSubject(),PARAMETERS :: CURCODE{2},GRADECODE{3},ACAYEAR{4},SECTIONCODE{5}";
            Log.Debug(string.Format(debug, "STUDENT", "getSectionSubject", curcode, gradecode, academicyear, sectioncode));

            List<Sims039> secsubject_list = new List<Sims039>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.GetSubject_proc",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@cur_code", curcode),
                            new SqlParameter("@academic_year", academicyear),
                            new SqlParameter("@grade_code", gradecode),
                            new SqlParameter("@section_code", sectioncode),                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims039 simsobj = new Sims039();
                            simsobj.sims_subject_name = dr["sims_subject_name_en"].ToString();
                            simsobj.sims_subject_code = dr["sims_subject_code"].ToString();
                            secsubject_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, secsubject_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, secsubject_list);
        }

        [Route("getSectionNamesWithSubjects")]
        public HttpResponseMessage getSectionNamesWithSubjects(string curcode, string gradecode, string academicyear, string sectioncode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSectionNamesWithSubjects(),PARAMETERS :: CURCODE{2},GRADECODE{3},ACAYEAR{4}";
            Log.Debug(string.Format(debug, "STUDENT", "getSectionNamesWithSubjects", curcode, gradecode, academicyear));

            List<Sims039> subject_list1 = new List<Sims039>();

            try
            {

                using (DBConnection db11 = new DBConnection())
                {
                    db11.Open();

                    SqlDataReader dr11 = db11.ExecuteStoreProcedure("sims.sims_section_subject_proc", new List<SqlParameter>() 
                                { 
                                    new SqlParameter("@opr", 'S'),
                                    new SqlParameter("@sims_cur_code", curcode),
                                    new SqlParameter("@sims_grade_code", gradecode),
                                    new SqlParameter("@sims_academic_year", academicyear),
                                    new SqlParameter("@sims_section_code", sectioncode),
                                });
                    if (dr11.HasRows)
                    {
                        while (dr11.Read())
                        {
                            Sims039 simsobj1 = new Sims039();
                            simsobj1.sims_cur_code = dr11["sims_cur_code"].ToString();
                            simsobj1.sims_academic_year = dr11["sims_academic_year"].ToString();
                            simsobj1.sims_grade_code = dr11["sims_grade_code"].ToString();
                            simsobj1.sims_section_code = dr11["sims_section_code"].ToString();
                            simsobj1.sims_subject_name = dr11["sims_subject_name_en"].ToString();
                            simsobj1.sims_subject_abbr = dr11["sims_subject_name_abbr"].ToString();
                            simsobj1.sims_moe_code = dr11["sims_subject_moe_code"].ToString();
                            simsobj1.sims_subject_type_name = dr11["sims_subject_type_name_en"].ToString();
                            simsobj1.sims_subject_group_code = dr11["sims_subject_group_code"].ToString();
                            simsobj1.sims_subject_code = dr11["sims_subject_code"].ToString();
                            if (dr11["sub_status"].ToString() == "A")
                                simsobj1.sims_subject_status = true;
                            subject_list1.Add(simsobj1);
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, subject_list1);

                    }

                }


            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subject_list1);
            }

            return Request.CreateResponse(HttpStatusCode.OK, subject_list1);

        }


        /* defined subject to given section */

        [Route("InsertSectionSubject")]
        public HttpResponseMessage InsertSectionSubject(List<Sims039> data)
        {
            bool subject_list1 = false;
            try
            {
                foreach (var simsobj in data)
                {
                    using (DBConnection db11 = new DBConnection())
                    {
                        db11.Open();
                        SqlDataReader dr11 = db11.ExecuteStoreProcedure("sims.sims_section_subject_proc", new List<SqlParameter>() 
                                { 
                  new SqlParameter("@opr", "I"),
                  new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                  new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                  new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                  new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                  new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                  new SqlParameter("@sims_display_order",simsobj.sims_display_order),
                  new SqlParameter("@sims_subject_mdl_course_id",simsobj.sims_subject_mdl_course_id),
                  new SqlParameter("@sims_section_subject_credit_hours",simsobj.sims_section_subject_credit_hours),
                    
                                });
                        if (dr11.RecordsAffected > 0)
                        {
                            subject_list1 = true;
                        }

                        db11.Dispose();
                        dr11.Close();
                    }
                }

            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, subject_list1);
        }

        [Route("IDeleteSectionSubject")]
        public HttpResponseMessage IDeleteSectionSubject(List<Sims039> data)
        {
            string subject_list1 = "";
            try
            {
                foreach (var simsobj in data)
                {
                    using (DBConnection db11 = new DBConnection())
                    {
                        db11.Open();
                        SqlDataReader dr11 = db11.ExecuteStoreProcedure("sims.sims_section_subject_proc", new List<SqlParameter>() 
                                { 
                  new SqlParameter("@opr", "D"),
                  new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                  new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                  new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                  new SqlParameter("@sims_section_code", simsobj.sims_section_code),
                  new SqlParameter("@sims_subject_code", simsobj.sims_subject_code),
                                });
                        if (dr11.HasRows)
                        {
                            while (dr11.Read())
                            {
                                if (dr11["subject_name"].ToString().Length > 0)
                                    subject_list1 = subject_list1 + dr11["subject_name"].ToString() + ",";

                            }
                        }
                        db11.Dispose();
                        dr11.Close();
                    }
                }
                if (subject_list1 != "")
                {
                    subject_list1 = "Subject :" + subject_list1 + "Are Mapped With Agenda";
                }
                else
                {
                    subject_list1 = "Record Updated Successfully";
                }


            }
            catch (Exception e)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, subject_list1);
        }
    }
}