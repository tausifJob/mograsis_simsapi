﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;
using SIMSAPI.Attributes;
using SIMSAPI.Models.SIMS.simsClass;
using System.Data;

namespace SIMSAPI.Controllers.modules.UserApplicationsController
{

    
    [RoutePrefix("api/common/UserApplicationsController")]
    [BasicAuthentication]
    public class UserApplicationsController : ApiController
    {

        //User Applications
        public class temp
        {
            public string comn_user_appl_code { get; set; }
            public string comn_role_code { get; set; }
            public string users { get; set; }

        }


        public class role_nm
        {
            public string Label { get; set; }
            public string Value { get; set; }


        }

        [Route("GetRoles")]
        public HttpResponseMessage GetRoles()
        {
            List<Comn006> mod_list = new List<Comn006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@tbl_name", "[comn].[comn_user_role]"),
                            new SqlParameter("@tbl_col_name1", "comn_role_code,comn_role_name"),
                            new SqlParameter("@tbl_cond", "[comn_role_status]='Y'"),
                            new SqlParameter("@tbl_OrdBy", "comn_role_name")
                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn006 comnObj = new Comn006();
                            comnObj.comn_role_code = dr["comn_role_code"].ToString();
                            comnObj.comn_role_name = dr["comn_role_name"].ToString();
                            mod_list.Add(comnObj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        [Route("GetAssignRoles")]
        public HttpResponseMessage GetAssignRoles(string userName)
        {
            List<Comn006> mod_list = new List<Comn006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@comn_user_code", userName)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn006 comnobj = new Comn006();
                            comnobj.comn_user_code = dr["comn_user_code"].ToString();
                            comnobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            comnobj.comn_user_appl_name = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_role_code = dr["comn_user_role_id"].ToString();

                            if (dr["comn_user_read"].ToString().ToLower() == "1")
                                comnobj.comn_user_read = true;
                            else
                                comnobj.comn_user_read = false;

                            if (dr["comn_user_insert"].ToString().ToLower() == "1")
                                comnobj.comn_user_insert = true;
                            else
                                comnobj.comn_user_insert = false;

                            if (dr["comn_user_update"].ToString().ToLower() == "1")
                                comnobj.comn_user_update = true;
                            else
                                comnobj.comn_user_update = false;

                            if (dr["comn_user_delete"].ToString().ToLower() == "1")
                                comnobj.comn_user_delete = true;
                            else
                                comnobj.comn_user_delete = false;

                            if (dr["comn_user_massupdate"].ToString().ToLower() == "1")
                                comnobj.comn_user_massupdate = true;
                            else
                                comnobj.comn_user_massupdate = false;

                            if (dr["comn_user_appl_status"].ToString().ToLower() == "a")
                                comnobj.comn_user_appl_status = true;
                            else
                                comnobj.comn_user_appl_status = false;
                            mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("GetUserAssignRoles")]
        public HttpResponseMessage GetUserAssignRoles(string userName)
        {
            // List<Comn006> mod_list = new List<Comn006>();
            List<role_nm> mod_list = new List<role_nm>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("GetData",
                       new List<SqlParameter>() 
                         { 
                                new SqlParameter("@tbl_name", "comn.comn_user_role r INNER JOIN comn.comn_user_application a ON r.comn_role_code = a.comn_user_role_id"),
                                new SqlParameter("@tbl_col_name1", "DISTINCT r.comn_role_name,r.comn_role_code"),
                                new SqlParameter("@tbl_cond", "a.comn_user_code='" + userName + "'"),
                                new SqlParameter("@tbl_ordby", "r.comn_role_code"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            role_nm comnobj = new role_nm();
                            comnobj.Value = dr["comn_role_code"].ToString();
                            comnobj.Label = dr["comn_role_name"].ToString();
                            mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }




        [Route("CopyApplications")]
        public HttpResponseMessage CopyApplications(List<Comn006> collection1)
        {
            bool result = false;

            foreach (Comn006 collection in collection1)
            {
                try
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", "B"));
                        para.Add(new SqlParameter("@comn_user_code", collection1[0].users));
                        para.Add(new SqlParameter("@comn_user_appl_code", collection.comn_user_appl_code));
                        para.Add(new SqlParameter("@comn_user_role_id", collection.comn_role_code));
                        para.Add(new SqlParameter("@comn_user_appl_fav", "0"));
                        para.Add(new SqlParameter("@comn_user_appl_count", "0"));
                        para.Add(new SqlParameter("@comn_user_admin", "0"));

                        if (collection.comn_user_read == true)
                            para.Add(new SqlParameter("@comn_user_read", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_read", "0"));

                        if (collection.comn_user_insert == true)
                            para.Add(new SqlParameter("@comn_user_insert", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_insert", "0"));

                        if (collection.comn_user_update == true)
                            para.Add(new SqlParameter("@comn_user_update", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_update", "0"));

                        if (collection.comn_user_delete == true)
                            para.Add(new SqlParameter("@comn_user_delete", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_delete", "0"));

                        if (collection.comn_user_massupdate == true)
                            para.Add(new SqlParameter("@comn_user_massupdate", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_massupdate", "0"));

                        if (collection.comn_user_appl_status == true)
                            para.Add(new SqlParameter("@comn_user_appl_status", "A"));
                        else
                            para.Add(new SqlParameter("@comn_user_appl_status", "I"));


                        SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application", para);
                        int r = dr.RecordsAffected;
                        result = r > 0 ? true : false;
                    }
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);


        }


        [Route("EditRoleApplications")]
        public HttpResponseMessage EditRoleApplications(string userName, string roleCode)
        {
            List<Comn006> mod_list = new List<Comn006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application",
                       new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@comn_user_code", userName),
                                new SqlParameter("@comn_user_role_id", roleCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn006 comnobj = new Comn006();
                            comnobj.comn_user_code = dr["comn_user_code"].ToString();
                            comnobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            comnobj.comn_user_appl_name = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_role_code = dr["comn_user_role_id"].ToString();
                            if (dr["comn_user_read"].ToString().ToLower() == "1")
                                comnobj.comn_user_read = true;
                            else
                                comnobj.comn_user_read = false;

                            if (dr["comn_user_insert"].ToString().ToLower() == "1")
                                comnobj.comn_user_insert = true;
                            else
                                comnobj.comn_user_insert = false;

                            if (dr["comn_user_update"].ToString().ToLower() == "1")
                                comnobj.comn_user_update = true;
                            else
                                comnobj.comn_user_update = false;

                            if (dr["comn_user_delete"].ToString().ToLower() == "1")
                                comnobj.comn_user_delete = true;
                            else
                                comnobj.comn_user_delete = false;

                            if (dr["comn_user_massupdate"].ToString().ToLower() == "1")
                                comnobj.comn_user_massupdate = true;
                            else
                                comnobj.comn_user_massupdate = false;

                            if (dr["comn_user_appl_status"].ToString().ToLower() == "a")
                                comnobj.comn_user_appl_status = true;
                            else
                                comnobj.comn_user_appl_status = false;

                            if (string.IsNullOrEmpty(dr["comn_user_appl_code"].ToString()) == true && string.IsNullOrEmpty(dr["comn_appl_name_en"].ToString()) &&

    string.IsNullOrEmpty(dr["comn_user_code"].ToString()) != true && string.IsNullOrEmpty(dr["comn_user_role_id"].ToString()) != true)

                                mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }


        [Route("EditRoleApplication")]
        public HttpResponseMessage GetAssignRoles(string userName, string roleCode)
        {
            List<Comn006> mod_list = new List<Comn006>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application",
                       new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "C"),
                                new SqlParameter("@comn_user_code", userName),
                                new SqlParameter("@comn_user_role_id", roleCode)
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn006 comnobj = new Comn006();
                            comnobj.comn_user_code = dr["comn_user_code"].ToString();
                            comnobj.comn_user_appl_code = dr["comn_user_appl_code"].ToString();
                            comnobj.comn_user_appl_name = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_role_code = dr["comn_user_role_id"].ToString();

                            if (dr["comn_user_read"].ToString().ToLower() == "1")
                                comnobj.comn_user_read = true;
                            else
                                comnobj.comn_user_read = false;

                            if (dr["comn_user_insert"].ToString().ToLower() == "1")
                                comnobj.comn_user_insert = true;
                            else
                                comnobj.comn_user_insert = false;

                            if (dr["comn_user_update"].ToString().ToLower() == "1")
                                comnobj.comn_user_update = true;
                            else
                                comnobj.comn_user_update = false;

                            if (dr["comn_user_delete"].ToString().ToLower() == "1")
                                comnobj.comn_user_delete = true;
                            else
                                comnobj.comn_user_delete = false;

                            if (dr["comn_user_massupdate"].ToString().ToLower() == "1")
                                comnobj.comn_user_massupdate = true;
                            else
                                comnobj.comn_user_massupdate = false;

                            if (dr["comn_user_appl_status"].ToString().ToLower() == "a")
                                comnobj.comn_user_appl_status = true;
                            else
                                comnobj.comn_user_appl_status = false;
                            mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }




        [Route("InsertApplications")]
        public HttpResponseMessage InsertApplications(List<Comn006> collection1)
        {
            //  Comn006 collection=
            //  Comn006 collection = Newtonsoft.Json.JsonConvert.DeserializeObject<Comn006>(collection1);

            bool flag = false;
            foreach (Comn006 collection in collection1)
            {


                try
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();

                        List<SqlParameter> para = new List<SqlParameter>();
                        para.Add(new SqlParameter("@opr", "I"));
                        para.Add(new SqlParameter("@comn_user_code", collection.users));
                        para.Add(new SqlParameter("@comn_user_appl_code", collection.comn_user_appl_code));
                        para.Add(new SqlParameter("@comn_user_role_id", collection.comn_role_code));
                        para.Add(new SqlParameter("@comn_user_appl_assigned_by_user_code", collection.comn_user_appl_assigned_by_user_code));
                        para.Add(new SqlParameter("@comn_user_appl_fav", "0"));
                        para.Add(new SqlParameter("@comn_user_appl_count", "0"));
                        para.Add(new SqlParameter("@comn_user_admin", "0"));

                        if (collection.comn_user_read == true)
                            para.Add(new SqlParameter("@comn_user_read", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_read", "0"));

                        if (collection.comn_user_insert == true)
                            para.Add(new SqlParameter("@comn_user_insert", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_insert", "0"));

                        if (collection.comn_user_update == true)
                            para.Add(new SqlParameter("@comn_user_update", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_update", "0"));

                        if (collection.comn_user_delete == true)
                            para.Add(new SqlParameter("@comn_user_delete", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_delete", "0"));

                        if (collection.comn_user_massupdate == true)
                            para.Add(new SqlParameter("@comn_user_massupdate", "1"));
                        else
                            para.Add(new SqlParameter("@comn_user_massupdate", "0"));

                        if (collection.comn_user_appl_status == true)
                            para.Add(new SqlParameter("@comn_user_appl_status", "A"));
                        else
                            para.Add(new SqlParameter("@comn_user_appl_status", "I"));


                        SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application", para);
                        flag = dr.RecordsAffected > 0 ? true : false;
                    }
                    //  return Request.CreateResponse(HttpStatusCode.OK, flag);
                }
                catch (Exception e)
                {
                    // return Request.CreateResponse(HttpStatusCode.OK, flag);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, flag);


        }



        #region API Student Behaviour Targe...

        [Route("FeeTransactionDetails")]
        public HttpResponseMessage FeeTransactionDetails(SimFTD obj)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("pp_sims_fee_transaction_details",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "E"),
                                new SqlParameter("@sims_cur_code", obj.sims_cur_code),
                                new SqlParameter("@sims_academic_year", obj.sims_academic_year),
                                new SqlParameter("@sims_grade_code", obj.sims_grade_code),
                                new SqlParameter("@sims_section_code", obj.sims_section_code),
                                new SqlParameter("@sims_enroll_number", obj.temp_dd_enroll_number),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion


        [Route("CUD_fee_doc")]
        public HttpResponseMessage CUD_fee_doc(List<sims_temp_fee_document_ppn> data)
        {
            bool inserted = false;
            string id = string.Empty;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (sims_temp_fee_document_ppn insert_invs in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("pp_sims_temp_fee_document_ppn",
                            new List<SqlParameter>()
                         {

                           new SqlParameter("@opr","DO"),
                           new SqlParameter("@temp_dd_doc_no", insert_invs.temp_dd_doc_no),
                           new SqlParameter("@temp_dd_realize_date", db.DBYYYYMMDDformat( insert_invs.temp_dd_realize_date)),

                          

                         
                         
                         });
                        if (dr.Read())
                        {
                            id = dr["temp_dd_doc_no"].ToString();

                        }
                        dr.Close();
                    }

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, id);
        }



        [Route("getStudentfee_table")]
        public HttpResponseMessage getStudentfee_table()
        {
            string cur_code = string.Empty, str = string.Empty;
            List<sims_temp_fee_document_ppn> mod_list = new List<sims_temp_fee_document_ppn>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[pp_sims_fee_transaction_details]",
                    new List<SqlParameter>()
                         {
                            new SqlParameter("@opr","B"),
                            //new SqlParameter("@sims_cur_code", curcode),
                            //new SqlParameter("@sims_academic_year", academicyear),
                            //new SqlParameter("@sims_grade_code", gradecode),
                            //new SqlParameter("@sims_section_code", section),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string str1 = "";
                            sims_temp_fee_document_ppn simsobj = new sims_temp_fee_document_ppn();
                            str1 = dr["temp_dd_doc_no"].ToString();
                            var v = from p in mod_list where p.temp_dd_doc_no == str1 select p;

                            simsobj.Student_Info = dr["Student_Info"].ToString();
                            simsobj.temp_dd_doc_no = dr["temp_dd_doc_no"].ToString();
                            simsobj.total_amt = dr["total_amt"].ToString();

                            simsobj.sims_fee_name = new List<sims_fee_name>();

                            sims_fee_name h = new sims_fee_name();

                            h.temp_dd_doc_no = dr["temp_dd_doc_no"].ToString();
                            h.temp_dd_fee_amount = dr["temp_dd_fee_amount"].ToString();
                            h.temp_dd_fee_name = dr["sims_fee_code_description"].ToString();

                            if (v.Count() == 0)
                            {
                                simsobj.sims_fee_name.Add(h);
                                mod_list.Add(simsobj);
                            }
                            else
                            {
                                v.ElementAt(0).sims_fee_name.Add(h);

                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, mod_list);
                    }
                }
            }
            catch (Exception e)
            {
            }

            return Request.CreateResponse(HttpStatusCode.OK, mod_list);


        }



        [Route("GetUserAssignRoles_new")]
        public HttpResponseMessage GetUserAssignRoles_new(string userName)
        {
            // List<Comn006> mod_list = new List<Comn006>();
            List<role_nm> mod_list = new List<role_nm>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("comn_user_application",
                       new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "F"),
                                new SqlParameter("@comn_user_code", userName),
                             
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            role_nm comnobj = new role_nm();
                            comnobj.Value = dr["comn_role_code"].ToString();
                            comnobj.Label = dr["comn_role_name"].ToString();
                            mod_list.Add(comnobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
        }

        


    }
}   