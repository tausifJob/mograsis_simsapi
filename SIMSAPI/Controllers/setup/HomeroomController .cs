﻿using log4net;
using SIMSAPI.Helper;
using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SIMSAPI.Controllers.HomeroomController
{
    [RoutePrefix("api/homeroom")]
    public class HomeroomController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region(HomeroomBatch)

      
        [Route("getAutoGenerate_HomeroombatchCode")]
        public HttpResponseMessage getAutoGenerate_HomeroombatchCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerate_HomeroomCode()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Scheduling/", "getAutoGenerate_HomeroomCode"));

            int sims_homeroombatch_code=0;

            string sims_homeroombatch_code1 = string.Empty;

            List<Sims159> trans_list = new List<Sims159>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B')
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_homeroombatch_code = int.Parse(dr["HomeRoomBatchNumber"].ToString());
                            if (sims_homeroombatch_code < 10)
                            {
                                sims_homeroombatch_code1 = '0' + sims_homeroombatch_code.ToString();
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, sims_homeroombatch_code1);

                }
            }
            catch (Exception x)
            {
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_homeroombatch_code1);
        }

        [Route("getSims_HomeroomBatch")]
        public HttpResponseMessage getSims_HomeroomBatch()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSims_HomeroomBatchByIndex()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Scheduling/", "getSims_HomeroomBatchByIndex"));

            List<Sims159> trans_list = new List<Sims159>();

            Message message = new Message();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims159 simsobj = new Sims159();
                            simsobj.sims_cur_name = dr["sims_cur_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_batch_code = dr["sims_batch_code"].ToString();
                            simsobj.sims_batch_name = dr["sims_batch_name"].ToString();
                            simsobj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            simsobj.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            simsobj.sims_batch_status = dr["sims_batch_status"].ToString().Equals("A") ? true : false;
                            trans_list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, trans_list);
                    }
                    
                }
            }
            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, trans_list);
            }
           return Request.CreateResponse(HttpStatusCode.OK, trans_list);
        }

        [Route("CUDRHomeroomBatch")]
        public HttpResponseMessage CUDRsoSchedules(Sims159 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDRHomeroomBatch()PARAMETERS ::DATA {2}";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "CUDRHomeroomBatch", data));

            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_proc]",
                            new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", data.opr),
                new SqlParameter("@sims_cur_code", data.sims_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_batch_code", data.sims_batch_code),
                new SqlParameter("@sims_batch_name", data.sims_batch_name), 
                new SqlParameter("@sims_batch_status", data.sims_batch_status.Equals(true)?"A":"I"),
                new SqlParameter("@sims_homeroom_code",data.sims_homeroom_code)
                           

                          });
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, inserted);
                    }
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                inserted = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }


        [Route("CUDRHomeroomBatchDelete")]
        public HttpResponseMessage CUDRHomeroomBatchDelete(List<Sims159> data1)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDRHomeroomBatch()PARAMETERS ::DATA {2}";
            Log.Debug(string.Format(debug, "ERP/Inventory/", "CUDRHomeroomBatch", data1));

            bool inserted = false;

            Message message = new Message();
            try
            {
                if (data1 != null)
                {

                    foreach (Sims159 data in data1)
                    {
                        using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", data.opr),
                new SqlParameter("@sims_cur_code", data.sims_cur_code),
                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                new SqlParameter("@sims_batch_code", data.sims_batch_code),
                new SqlParameter("@sims_batch_name", data.sims_batch_name),
                new SqlParameter("@sims_batch_status", data.sims_batch_status==true?"A":"I"),
                new SqlParameter("@sims_homeroom_code",data.sims_homeroom_code)
                            

                          });
                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                            
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
                else
                {
                    inserted = false;
                }
            }
            catch (Exception x)
            {
                //inserted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);

        }



        #endregion

        #region(HomeroomTeacher)

        [Route("GetAllHomeroomName")]
        public HttpResponseMessage GetAllHomeroomName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : GetAllHomeroomName(),PARAMETERS ::NO";
            Log.Debug(string.Format(debug, "STUDENT", "HOMEROOMNAMES"));

            List<Sims161> batch_list = new List<Sims161>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr","H")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims161 simsObj = new Sims161();
                            simsObj.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            simsObj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            batch_list.Add(simsObj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, batch_list);
        }

        [Route("getTeacher")]
        public HttpResponseMessage getTeacher()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getHomeroomTeacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "HOMEROOMTEACHER"));

            List<Sims161> teacher_list = new List<Sims161>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_teacher_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'T'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims161 simsobj = new Sims161();

                            simsobj.sims_teacher_code = dr["sims_teacher_code"].ToString();
                            simsobj.sims_teacher_name = dr["sims_teacher_name"].ToString();
                            teacher_list.Add(simsobj);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, teacher_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, teacher_list);
        }

        [Route("getHomeroomTeacher")]
        public HttpResponseMessage getHomeroomTeacher()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getHomeroomTeacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "HOMEROOMTEACHER"));

            List<Sims161> teacher_list = new List<Sims161>();
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_teacher_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims161 simsobj = new Sims161();

                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            simsobj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            simsobj.sims_homeroom_teacher_code = dr["sims_homeroom_teacher_code"].ToString();
                            simsobj.sims_homeroom_teacher_name = dr["sims_teacher_name"].ToString();
                            simsobj.sims_status = dr["sims_status"].ToString().Equals("A") ? true : false;
                            teacher_list.Add(simsobj);
                           
                        }
                    }
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);

            }
            return Request.CreateResponse(HttpStatusCode.OK, teacher_list);
        }

        [Route("CHomeroomTeacher")]
        public HttpResponseMessage CHomeroomTeacher(Sims161 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CHomeroomTeacher(),PARAMETERS :: DATA {2}";
            Log.Debug(string.Format(debug, "STUDENT", "CREATEHOMEROOMTEACHER",data));

            bool insrted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                { using (DBConnection db = new DBConnection())
                        {
                            db.Open();
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_teacher_proc]",
                                new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "I"),
                                new SqlParameter("@sims_cur_code", data.sims_cur_code),
                                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                                new SqlParameter("@sims_homeroom_code", data.sims_homeroom_code),
                                new SqlParameter("@sims_homeroom_teacher_code", data.sims_homeroom_teacher_code1),
                                new SqlParameter("@sims_status", data.sims_status==true?"A":"I")
                            });
                            if (dr.RecordsAffected > 0)
                            {
                                insrted = true;
                            }
                            else
                            {
                                insrted = false;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, insrted);
                        }
                    
                }
                else
                {
                    insrted = false;
                }
            }
            catch (Exception x)
            {
                //insrted = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, insrted);
        }

        [Route("UHomeroomTeacher")]
        public HttpResponseMessage UHomeroomTeacher(Sims161 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UHomeroomTeacher(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "UHomeroomTeacher"));

            bool updated = false;
           
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_teacher_proc]",
                            new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", data.opr),
                                 new SqlParameter("@sims_cur_code", data.sims_cur_code),
                                new SqlParameter("@sims_academic_year", data.sims_academic_year),
                                new SqlParameter("@sims_homeroom_code", data.sims_homeroom_code),
                                new SqlParameter("@sims_homeroom_teacher_code", data.sims_homeroom_teacher_code),
                                new SqlParameter("@old_sims_homeroom_teacher_code",data.old_sims_homeroom_teacher_code),
                                new SqlParameter("@sims_status", data.sims_status==true?"A":"I")
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, updated);
                    }
                }
                else
                {
                    updated = false;
                }
            }
            catch (Exception x)
            {
                //updated = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        #endregion

        #region(HomeroomStudent)

        [Route("getAllHomeroomBatchName")]
        public HttpResponseMessage getAllHomeroomBatchName()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllHomeroomBatchName(),PARAMETERS ::NO";
            Log.Debug(string.Format(debug, "STUDENT", "HOMEROOMBATCHNAMES"));

            List<Sims160> batch_list = new List<Sims160>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "B"),
                            
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims160 simsObj = new Sims160();
                            simsObj.sims_batch_name = dr["sims_batch_name"].ToString();
                            simsObj.sims_batch_code = dr["sims_batch_code"].ToString();
                            batch_list.Add(simsObj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, batch_list);
        }

        [Route("getHomeroomBatchStudent")]
        public HttpResponseMessage getHomeroomBatchStudent(string cur_code, string a_year, string homeroom_code, string homeroom_batch)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getHomeroomBatchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "HOMEROOMBATCHSTUDENT"));

            List<Sims160> student_list = new List<Sims160>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", a_year),
                            new SqlParameter("@sims_homeroom_code", homeroom_code),
                            new SqlParameter("@sims_batch_code", homeroom_batch),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims160 simsobj = new Sims160();
                            simsobj.sims_cur_name = dr["sims_cur_code"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_student_passport_first_name_en = dr["sims_student_passport_first_name_en"].ToString();
                            simsobj.sims_batch_enroll_number = dr["sims_batch_enroll_number"].ToString();
                            simsobj.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            simsobj.sims_batch_name = dr["sims_batch_name"].ToString();
                            simsobj.sims_batch_code = dr["sims_batch_code"].ToString();
                            simsobj.sims_batch_status = dr["sims_batch_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            simsobj.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            student_list.Add(simsobj);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, student_list);

            }
            return Request.CreateResponse(HttpStatusCode.OK, student_list);
        }

        [Route("CHomeroomBatchStudent")]
        public HttpResponseMessage CHomeroomBatchStudent(Sims160 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDHomeroomBatchStudent(),PARAMETERS :: DATA {2}";
            Log.Debug(string.Format(debug, "STUDENT", "CREATEHOMEROOMBATCHSTUDENT", data));

            bool insreted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@sims_cur_code",data.sims_cur_name),
                            new SqlParameter("@sims_academic_year",data.sims_academic_year),
                            new SqlParameter("@sims_batch_code",data.sims_batch_name),
                            new SqlParameter("@sims_batch_enroll_number", data.sims_batch_enroll_number),
                            new SqlParameter("@sims_batch_status", data.sims_batch_status==true?"A":"I"),
                            new SqlParameter("@sims_homeroom_code",data.sims_homeroom_code)
                         });
                        if (dr.RecordsAffected > 0)
                        {
                            insreted = true;
                        }
                        else
                        {
                            insreted = false;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, insreted);
                    }

                }
                else
                {
                    insreted = false;

                }
                return Request.CreateResponse(HttpStatusCode.OK, insreted);
            }
            catch (Exception x)
            {
                message.strMessage = "Error In Adding HomeroomBatchStudentDetails Information!!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        [Route("UHomeroomBatchStudent")]
        public HttpResponseMessage UHomeroomBatchStudent(Sims160 data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UHomeroomBatchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "UHomeroomBatchStudent"));

            bool updated = false;

            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                            new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", data.opr),
                            new SqlParameter("@sims_cur_code",data.sims_cur_name),
                            new SqlParameter("@sims_academic_year",data.sims_academic_year),
                            new SqlParameter("@sims_batch_code",data.sims_batch_name),
                            new SqlParameter("@sims_batch_enroll_number", data.sims_batch_enroll_number),
                            new SqlParameter("@sims_batch_status", data.sims_batch_status==true?"A":"I"),
                            new SqlParameter("@sims_homeroom_code",data.sims_homeroom_code)
                            });
                        if (dr.RecordsAffected > 0)
                        {
                            updated = true;
                        }
                        else
                        {
                            updated = false;
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, updated);
                    }
                }
                else
                {
                    updated = false;
                }
            }
            catch (Exception x)
            {
                //updated = false;
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, updated);
        }

        [Route("gethomeroomall")]
        public HttpResponseMessage gethomeroomall(string cur_code, string a_year, string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UHomeroomBatchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "UHomeroomBatchStudent"));

            List<Sims160> batch_list = new List<Sims160>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", a_year),
                            new SqlParameter("@sims_user_code", user_code),
                         }
                    );

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims160 simsObj = new Sims160();
                            simsObj.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            simsObj.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            batch_list.Add(simsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, batch_list);
        }

        [Route("getAllhomeroombatch")]
        public HttpResponseMessage getAllhomeroombatch(string cur_code, string a_year, string homeroom_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : UHomeroomBatchStudent(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "UHomeroomBatchStudent"));

            List<Sims160> batch_list = new List<Sims160>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_batch_student_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_academic_year", a_year),
                            new SqlParameter("@sims_homeroom_code", homeroom_code),
                         }
                    );

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims160 simsObj = new Sims160();
                            simsObj.sims_batch_name = dr["sims_batch_name"].ToString();
                            simsObj.sims_batch_code = dr["sims_batch_code"].ToString();
                            batch_list.Add(simsObj);
                        }
                    }
                }
            }
            catch (Exception x)
            {

            }

            return Request.CreateResponse(HttpStatusCode.OK, batch_list);
        }

        #endregion

        #region(Homeroom)


        [Route("getAutoGenerate_HomeroomCode")]
        public HttpResponseMessage getAutoGenerate_HomeroomCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerate_HomeroomCode()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP/Scheduling/", "getAutoGenerate_HomeroomCode"));

            int sims_homeroom_code = 0;
            string sims_homeroom_code1 =string.Empty;
            List<Sims159> trans_list = new List<Sims159>();
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'C')
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {

                            sims_homeroom_code = int.Parse(dr["HomeRoomNumber"].ToString());
                            if (sims_homeroom_code < 10)
                            {
                                sims_homeroom_code1 = "0" + sims_homeroom_code.ToString();
                            }
                            else
                            {
                                sims_homeroom_code1 = sims_homeroom_code.ToString();
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, sims_homeroom_code1);

                }
            }
            catch (Exception x)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, sims_homeroom_code1);
        }


        [Route("getAllHomeroom")]
        public HttpResponseMessage getAllHomeroom()
        {
            List<Sims158> result = new List<Sims158>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@opr", "S"));
           
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_proc]", lst);
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims158 c = new Sims158();
                            c.sims_homeroom_status = dr["sims_homeroom_status"].Equals("A") ? true : false;
                            c.sims_homeroom_location_details = dr["sims_homeroom_location_details"].ToString();
                            c.sims_homeroom_name = dr["sims_homeroom_name"].ToString();
                            c.sims_homeroom_code = dr["sims_homeroom_code"].ToString();
                            c.sims_academic_year = dr["sims_academic_year"].ToString();
                            c.sims_cur_code = dr["sims_cur_code"].ToString();
                            c.sims_cur_short_name_en = dr["sims_cur_short_name_en"].ToString();
                            result.Add(c);

                           
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception ex)
            {
               
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [Route("CUDHomeroom")]
        public HttpResponseMessage CUDHomeroom(Sims158 simsobj)
        {
            bool message = false;
            try
            {
                if (simsobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_homeroom_proc]",

                            new List<SqlParameter>() 
                         { 
                      
                        new SqlParameter("@opr", simsobj.opr),
                             new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                             new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                             new SqlParameter("@sims_homeroom_code", simsobj.sims_homeroom_code),
                             new SqlParameter("@sims_homeroom_name", simsobj.sims_homeroom_name),
                             new SqlParameter("@sims_homeroom_location_details", simsobj.sims_homeroom_location_details),
                             new SqlParameter("@sims_homeroom_status", simsobj.sims_homeroom_status.Equals(true)?"A":"I")
                            
                        });

                        if (dr.RecordsAffected > 0)
                        {
                            message = true;

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
            }
            catch (Exception ex)
            {
               // message = false;
                 return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
        

            }
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        #endregion


    }
}