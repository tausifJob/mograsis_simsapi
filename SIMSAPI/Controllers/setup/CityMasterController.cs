﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.Setup
{
    [RoutePrefix("api/CityMaster")]
    public class CityMasterController:ApiController
    {
        [Route("getCity")]
        public HttpResponseMessage getCity()
        {
            List<Sim056> result = new List<Sim056>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_city_proc]",
                        new List<SqlParameter>()
                         {

                    new SqlParameter("@opr", "S")
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim056 obj = new Sim056();


                            obj.sims_city_code = dr["sims_city_code"].ToString();
                            obj.sims_state_code = dr["sims_state_code"].ToString();
                            obj.sims_city_name_en = dr["sims_city_name_en"].ToString();
                            obj.sims_city_name_ar = dr["sims_city_name_ar"].ToString();
                            obj.sims_city_name_fr = dr["sims_city_name_fr"].ToString();
                            obj.sims_city_name_ot = dr["sims_city_name_ot"].ToString();
                            obj.sims_state_name_en = dr["sims_state_name_en"].ToString();
                            obj.sims_country_code = dr["sims_country_code"].ToString();
                            obj.sims_country_name_en = dr["sims_country_name_en"].ToString();
                            obj.sims_status = dr["sims_status"].ToString() == "A";

                            result.Add(obj);

                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }

            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("CUDgetCity")]
        public HttpResponseMessage CUDgetCity(List<Sim056> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDgetCity", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim056 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_city_proc]",
                            new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),

                          new SqlParameter("@sims_city_code",simsobj.sims_city_code),
                         new SqlParameter("@sims_state_code", simsobj.sims_state_code),
                          new SqlParameter("@sims_city_name_en",simsobj.sims_city_name_en),
                          new SqlParameter("@sims_city_name_ar", simsobj.sims_city_name_ar),                   
                          new SqlParameter("@sims_city_name_fr", simsobj.sims_city_name_fr),
                          new SqlParameter("@sims_city_name_ot", simsobj.sims_city_name_ot),                        
                          new SqlParameter("@sims_status",simsobj.sims_status==true?"A":"I"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}