﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMSAPI.Controllers.setup
{
    class SAR002
    {
        public string comn_mod_name { get; set; }

        public string comn_appl_name { get; set; }

        public string comn_appl_form_field { get; set; }

        public string comn_appl_parameter { get; set; }

        public string comn_appl_form_field_value1 { get; set; }

        public string comn_appl_form_field_value2 { get; set; }

        public string comn_appl_form_field_value3 { get; set; }

        public string comn_appl_form_field_value4 { get; set; }

        public string comn_appl_form_field_value5 { get; set; }

        public string old_form_field { get; set; }

        public string old_value1 { get; set; }

        public string comn_serial_number { get; set; }

        public string comnapplicationcode { get; set; }

        public int myid { get; set; }
    }
}
