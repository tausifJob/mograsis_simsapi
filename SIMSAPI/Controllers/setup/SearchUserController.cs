﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;


namespace SIMSAPI.Controllers.setup
{
     [RoutePrefix("api/SearchUserDetails")]
    public class SearchUserController:ApiController
    {

        [Route("getSearchUser")]
        public HttpResponseMessage getSearchUser()
        {
            List<Sim510> search_user_list = new List<Sim510>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_SearchQuery]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")

                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim510 obj = new Sim510();

                            obj.sims_search_code = dr["sims_search_code"].ToString();
                            obj.Description = dr["Description"].ToString();
                            obj.Users = dr["Users"].ToString();

                            search_user_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, search_user_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, search_user_list);
        }

        //[Route("getSearchUserOutout")]
        //public HttpResponseMessage getSearchUserOutout(string search_code)
        //{
        //    string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSearchUserOutout(),PARAMETERS :: NO";

        //    List<Sim510> SearchUserList = new List<Sim510>();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[sims_SearchQuery]",
        //                new List<SqlParameter>()
        //                 {
        //                         new SqlParameter("@opr", "T"),
        //                         new SqlParameter("@sims_search_code",search_code)
        //                 });
        //            if (dr.HasRows)
        //            {
        //                while (dr.Read())
        //                {
        //                    Sim510 simsobj = new Sim510();

        //                    simsobj.sims_cur_short_name_en = dr["Curiculum Short Name"].ToString();
        //                    simsobj.sims_cur_level_name_en = dr["Curiculum Level Name"].ToString();
        //                    simsobj.sims_grade_name_en = dr["Grade Name"].ToString();
        //                    simsobj.sims_section_name_en = dr["Section Name"].ToString();
        //                    simsobj.sims_student_enroll_number = dr["Student Enroll Number"].ToString();
        //                    simsobj.sims_student_passport_name_en = dr["Student_Passport_Name"].ToString();
        //                    simsobj.sims_student_nickname = dr["Student Nickname"].ToString();
        //                    //simsobj.sims_student_gender = dr["Student Gender"].ToString();
        //                    simsobj.sims_religion_name_en = dr["Student Religion Name"].ToString();
        //                    simsobj.sims_nationality_name_en = dr["Student Nationality Name"].ToString();
        //                    simsobj.sims_student_remark = dr["Student Remark"].ToString();
        //                    simsobj.sims_house_name = dr["Student House Name"].ToString();
        //                    simsobj.sims_parent_father_name = dr["Student Father Name"].ToString();
        //                    simsobj.sims_parent_mother_name = dr["Student Mother Name"].ToString();
        //                    simsobj.sims_parent_guardian_name = dr["Student Guardian Name"].ToString();
        //                    simsobj.sims_parent_father_family_name = dr["Student Father Family Name"].ToString();
        //                    simsobj.sims_parent_mother_family_name = dr["Student Mother Family Name"].ToString();
        //                    simsobj.sims_parent_guardian_family_name = dr["Student Guardian Family Name"].ToString();
        //                    simsobj.sims_parent_father_mobile = dr["Student Father Mobile Number"].ToString();
        //                    simsobj.sims_parent_mother_mobile = dr["Student Mother Mobile Number"].ToString();
        //                    simsobj.sims_parent_guardian_mobile = dr["Student Guardian Mobile Number"].ToString();
        //                    simsobj.sims_parent_father_email = dr["Student Father Email"].ToString();
        //                    simsobj.sims_parent_mother_email = dr["Student Mother Email"].ToString();
        //                    simsobj.sims_parent_guardian_email = dr["Student Guardian Email"].ToString();
        //                    simsobj.sims_subject_name_en = dr["Student Subject Name"].ToString();


        //                    SearchUserList.Add(simsobj);
        //                }
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, SearchUserList);
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, SearchUserList);
        //    }
        //}


        [Route("getSearchUserOut")]
        public HttpResponseMessage getSearchUserOut(string search_code)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            DataSet ds = new DataSet();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    ds = db.ExecuteStoreProcedureDS("[dbo].[sims_SearchQuery]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "T"),
                                 new SqlParameter("@sims_search_code",search_code)
                         });

                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
        }

        //[Route("getSearchUserOut")]
        //public HttpResponseMessage getSearchUserOut(string search_code)
        //{
        //    object o = null;
        //    HttpStatusCode s = HttpStatusCode.NoContent;
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        using (DBConnection db = new DBConnection())
        //        {
        //            db.Open();
        //            ds = db.ExecuteStoreProcedureDS("[dbo].[sims_SearchQuery]",
        //                new List<SqlParameter>()
        //                 {
        //                    new SqlParameter("@opr", "T"),
        //                    new SqlParameter("@sims_search_code",search_code)
        //                 });
        //            ds.DataSetName = "AdjData";
        //            o = ds;
        //            s = HttpStatusCode.OK;

        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        o = x;
        //        s = HttpStatusCode.InternalServerError;
        //    }
        //    return Request.CreateResponse(s, o);
        //}

        [Route("CUDSearch_User")]
        public HttpResponseMessage CUDSearch_User(List<Sim510> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            // Log.Debug(string.Format(debug, "PP", "CUDSearch_User", simsobj));


            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim510 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[dbo].[sims_SearchQuery]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_search_code",simsobj.sims_search_code),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


    }
}