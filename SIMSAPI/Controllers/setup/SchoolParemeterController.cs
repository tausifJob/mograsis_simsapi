﻿using SIMSAPI.Helper;
using SIMSAPI.Models.COMMON;
using SIMSAPI.Models.SIMS.simsClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/SchoolParameter")]
    public class SchoolParemeterController : ApiController
    {
        [Route("getParameterBySchool")]
        public HttpResponseMessage getParameterBySchool()
        {
            List<SchoolParameter> lstCuriculum = new List<SchoolParameter>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_school_parameter_new_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr",'X'),                            
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SchoolParameter sequence = new SchoolParameter();
                            sequence.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            //sequence.sims_appl_form_field_id = dr["field_id"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }


        [Route("getSchoolParameter")]
        public HttpResponseMessage getSchoolParameter()
        {
            List<SchoolParameter> lstParameter = new List<SchoolParameter>();
            // int total = 0, skip = 0, 
            int cnt = 1;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_school_parameter_new_proc]",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SchoolParameter simsobj = new SchoolParameter();
                            simsobj.myid = cnt;
                            simsobj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            lstParameter.Add(simsobj);
                            // total = lstParameter.Count;
                            // skip = PageSize * (pageIndex - 1);
                            cnt++;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "");
            }
            //return Request.CreateResponse(HttpStatusCode.OK, lstParameter);
        }

        [Route("CUDSchoolByParameter")]
        public HttpResponseMessage CUDSchoolByParameter(List<SchoolParameter> data)
        {
            bool insert = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (SchoolParameter school in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_school_parameter_new_proc]",
                             new List<SqlParameter>()
                         {
                             new SqlParameter("@opr",school.opr),
                             new SqlParameter("@sims_appl_parameter",school.sims_appl_parameter),
                             new SqlParameter("@sims_appl_form_field_value1",school.sims_appl_form_field_value1)

                         });
                        if (ins > 0)
                        {
                            insert = true;

                        }
                        else
                        {
                            insert = false;

                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }
            }
            catch (Exception x)
            {


            }
            return Request.CreateResponse(HttpStatusCode.OK, insert);
        }
    }
}




