﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.setup
{
     [RoutePrefix("api/SMTProtocol")]
    public class SMTPController:ApiController
    {
       
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getSMTP")]
        public HttpResponseMessage getSMTP()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSMTP()PARAMETERS ::NA";
          
            List<Sim041> GetAllRecordsSmtp = new List<Sim041>();
            // int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_smtp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim041 simsobj = new Sim041();
                            simsobj.sims_school_code = dr["sims_school_code"].ToString();
                            simsobj.sims_smtp_code = dr["sims_smtp_code"].ToString();
                            simsobj.sims_mod_code = dr["sims_mod_code"].ToString();
                            simsobj.sims_smtp_serverip = dr["sims_smtp_serverip"].ToString();
                            simsobj.sims_smtp_port = dr["sims_smtp_port"].ToString();
                            simsobj.sims_smtp_username = dr["sims_smtp_username"].ToString();
                            simsobj.sims_smtp_password = dr["sims_smtp_password"].ToString();
                            simsobj.sims_smtp_from_email = dr["sims_smtp_from_email"].ToString();
                            //simsobj.sims_smtp_status = dr["sims_smtp_status"].ToString();
                            simsobj.sims_smtp_status = dr["sims_smtp_status"].ToString().Equals("A") ? true : false;
                            simsobj.sims_smtp_auth_required = dr["sims_smtp_auth_required"].ToString();
                            simsobj.sims_smtp_ssl_required = dr["sims_smtp_ssl_required"].ToString();
                            simsobj.sims_smtp_max_mail_hour = dr["sims_smtp_max_mail_hour"].ToString();
                            simsobj.sims_smtp_max_mail_day = dr["sims_smtp_max_mail_day"].ToString();
                            simsobj.sims_smtp_encrypt_mode = dr["sims_smtp_encrypt_mode"].ToString();
                            simsobj.sims_smtp_from_email_name = dr["sims_smtp_from_email_name"].ToString();
                            simsobj.sims_smtp_config_id = dr["sims_smtp_config_id"].ToString();
                            simsobj.sims_smtp_signature = dr["sims_smtp_signature"].ToString();
                            //simsobj.sims_smtp_remark = dr["sims_smtp_remark"].ToString();


                            GetAllRecordsSmtp.Add(simsobj);
                            // total = curriculum.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
            }
            //  return Request.CreateResponse(HttpStatusCode.OK, curriculum);
        }


        [Route("getSchools")]
        public HttpResponseMessage getSchools()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getSchools()PARAMETERS ::NA";

            List<Sim041> GetAllRecordsSmtp = new List<Sim041>();
            // int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_smtp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'A'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim041 simsobj = new Sim041();
                            simsobj.lic_school_code = dr["lic_school_code"].ToString();
                            simsobj.lic_school_name = dr["lic_school_name"].ToString();
                           
                            GetAllRecordsSmtp.Add(simsobj);
                            // total = curriculum.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
            }
            //  return Request.CreateResponse(HttpStatusCode.OK, curriculum);
        }

        [Route("getModule")]
        public HttpResponseMessage getModule()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getModule()PARAMETERS ::NA";

            List<Sim041> GetAllRecordsSmtp = new List<Sim041>();
            // int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_smtp_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'B'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim041 simsobj = new Sim041();
                            simsobj.comn_mod_code = dr["comn_mod_code"].ToString();
                            simsobj.comn_mod_name_en = dr["comn_mod_name_en"].ToString();

                            GetAllRecordsSmtp.Add(simsobj);
                            // total = curriculum.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, GetAllRecordsSmtp);
            }
            //  return Request.CreateResponse(HttpStatusCode.OK, curriculum);
        }

        [Route("CUDSMTPDemo")]
        public HttpResponseMessage CUDSMTPDemo(List<Sim041> data)
        {
            //  string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCUDSMTP()PARAMETERS ::NA";
            bool insert = false;
            Message message = new Message();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim041 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_smtp_proc]",
                        new List<SqlParameter>()
                     {

                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_school_code", simsobj.sims_school_code),
                                new SqlParameter("@sims_smtp_code",simsobj.sims_smtp_code),
                                new SqlParameter("@sims_mod_code", simsobj.sims_mod_code),
                                new SqlParameter("@sims_smtp_serverip", simsobj.sims_smtp_serverip),
                                
                                new SqlParameter("@sims_smtp_port", simsobj.sims_smtp_port),
                                new SqlParameter("@sims_smtp_username", simsobj.sims_smtp_username),
                                new SqlParameter("@sims_smtp_password", simsobj.sims_smtp_password),
                                new SqlParameter("@sims_smtp_from_email", simsobj.sims_smtp_from_email),
                               
                                new SqlParameter("@sims_smtp_status", simsobj.sims_smtp_status==true?"A":"I"),

                                new SqlParameter("@sims_smtp_auth_required", simsobj.sims_smtp_auth_required),
                                // new SqlParameter("@sims_smtp_address", simsobj.sims_smtp_address),
                               
                                new SqlParameter("@sims_smtp_ssl_required",simsobj.sims_smtp_ssl_required),

                                //new SqlParameter("@sims_smtp_address", simsobj.sims_smtp_address),
                                new SqlParameter("@sims_smtp_max_mail_hour", simsobj.sims_smtp_max_mail_hour),

                                new SqlParameter("@sims_smtp_max_mail_day", simsobj.sims_smtp_max_mail_day),
                                new SqlParameter("@sims_smtp_encrypt_mode", simsobj.sims_smtp_encrypt_mode),
                                new SqlParameter("@sims_smtp_from_email_name", simsobj.sims_smtp_from_email_name),
                                new SqlParameter("@sims_smtp_config_id", simsobj.sims_smtp_config_id),
                                new SqlParameter("@sims_smtp_signature", simsobj.sims_smtp_signature),
                                //new SqlParameter("@sims_smtp_remark", simsobj.sims_smtp_remark)


                     });
                        if (ins > 0)
                        {
                            insert = true;
                        }


                        else
                        {
                            insert = false;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, insert);
                }

            }


            catch (Exception x)
            {

               /* message.strMessage = x.Message;
                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;*/
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, insert);
        }

      

     
     }
}