﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/SubjectGroup")]
    [BasicAuthentication]
    public class Sim046Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getAutoGeneratesubjectgrpCode")]
        public HttpResponseMessage getAutoGeneratesubjectgrpCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGeneratesubjectgrpCode(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "STUDENT", "getAutoGeneratesubjectgrpCode"));

            string commcode = string.Empty;
            Sims046 simsobj = new Sims046();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_group_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'B'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.subject_group_code = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj.subject_group_code);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj.subject_group_code);
            }
        }

        [Route("getSubjectGroup")]
        public HttpResponseMessage getSubjectGroup()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSubjectGroup(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SubjectGroup"));

            List<Sims046> subject_group = new List<Sims046>();
            //int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_group_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims046 simsobj = new Sims046();

                            simsobj.sims_cur_short_name_en = dr["sims_short_cur_name"].ToString();
                            simsobj.sims_attendance_cur_code = dr["sims_cur"].ToString();
                            simsobj.subject_group_code = dr["sims_subject_group_code"].ToString();
                            simsobj.subject_group_name_eng = dr["sims_subject_group_name_en"].ToString();
                            simsobj.subject_group_name_ar = dr["sims_subject_group_name_ar"].ToString();
                            simsobj.subject_group_name_fr = dr["sims_subject_group_name_fr"].ToString();
                            simsobj.subject_group_name_ot = dr["sims_subject_group_name_ot"].ToString();
                            subject_group.Add(simsobj);
                            //total = subject_group.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, subject_group);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, subject_group);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, subject_group);
            }
            // return Request.CreateResponse(HttpStatusCode.OK, subject_group);
        }

        [Route("CUDSubjectGroup")]
        public HttpResponseMessage CUDSubjectGroup(List<Sims046> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDSubjectGroup(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "STUDENT", "SubjectGroup"));
            bool inserted = false;
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims046 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_subject_group_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@cur_name", simsobj.sims_attendance_cur_code),
                            new SqlParameter("@sims_subject_group_code", simsobj.subject_group_code),
                            new SqlParameter("@sims_subject_group_name_en", simsobj.subject_group_name_eng),
                            new SqlParameter("@sims_subject_group_name_ar", simsobj.subject_group_name_ar),
                            new SqlParameter("@sims_subject_group_name_fr", simsobj.subject_group_name_fr),
                            new SqlParameter("@sims_subject_group_name_ot", simsobj.subject_group_name_ot)
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CheckSubjectGroup")]
        public HttpResponseMessage CheckSubjectGroup(string SubjectGroup, string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CheckTypeCode(),PARAMETERS :: SUBJECTGROUP{2},cur_code{3}";
            Log.Debug(string.Format(debug, "STUDENT", "CheckSubjectGroup", SubjectGroup, cur_code));

            bool ifexists = false;
            List<Sims046> SubjectGrp = new List<Sims046>();
            Sims046 simsobj = new Sims046();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_subject_group_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_subject_group_code", SubjectGroup),
                                new SqlParameter("@cur_name", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        ifexists = true;

                    }
                }

                if (ifexists)
                {

                    simsobj.status = true;
                    simsobj.strMessage = "Subject Group Already Exists";
                    SubjectGrp.Add(simsobj);
                }
                else
                {
                    simsobj.status = false;
                    simsobj.strMessage = "No Records Found";
                    SubjectGrp.Add(simsobj);
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

    }
}