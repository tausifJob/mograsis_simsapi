﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;



namespace SIMSAPI.Controllers.modules.Common
{
    [RoutePrefix("api/common/Curriculum")]
    public class Sim003Controller : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getCurriculum")]
        public HttpResponseMessage getCurriculum()
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : getCurriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            List<Sims003> curriculum = new List<Sims003>();
            // int total = 0, skip = 0;
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims003 simsobj = new Sims003();
                            simsobj.curriculum_code = dr["sims_cur_code"].ToString();
                            simsobj.curriculum_short_name = dr["sims_cur_short_name_en"].ToString();
                            simsobj.curriculum_short_name_ar = dr["sims_cur_short_name_ar"].ToString();
                            simsobj.curriculum_short_name_fr = dr["sims_cur_short_name_fr"].ToString();
                            simsobj.curriculum_short_name_ot = dr["sims_cur_short_name_ot"].ToString();
                            simsobj.curriculum_full_name = dr["sims_cur_full_name_en"].ToString();
                            simsobj.curriculum_full_name_ar = dr["sims_cur_full_name_ar"].ToString();
                            simsobj.curriculum_full_name_fr = dr["sims_cur_full_name_fr"].ToString();
                            simsobj.curriculum_full_name_ot = dr["sims_cur_full_name_ot"].ToString();
                            simsobj.curriculum_status = dr["sims_cur_status"].ToString().Equals("A") ? true : false;
                            curriculum.Add(simsobj);
                            // total = curriculum.Count;
                            // skip = PageSize * (pageIndex - 1);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, curriculum);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, curriculum);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, curriculum);
            }
            //  return Request.CreateResponse(HttpStatusCode.OK, curriculum);
        }

        [Route("CUDInsertCurriculum")]
        public HttpResponseMessage CUDInsertCurriculum(List<Sims003> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDCurriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            bool inserted = false;
            // Sims003 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims003>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims003 simsobj in data)
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_cur_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@cur_code", simsobj.curriculum_code),
                            new SqlParameter("@cur_short_name_en", simsobj.curriculum_short_name),
                            new SqlParameter("@cur_short_name_ar", simsobj.curriculum_short_name_ar),
                            new SqlParameter("@cur_short_name_fr", simsobj.curriculum_short_name_fr),
                            new SqlParameter("@cur_short_name_ot", simsobj.curriculum_short_name_ot),
                            new SqlParameter("@cur_full_name_en", simsobj.curriculum_full_name),
                            new SqlParameter("@cur_full_name_ar", simsobj.curriculum_full_name_ar),
                            new SqlParameter("@cur_full_name_fr", simsobj.curriculum_full_name_fr),
                            new SqlParameter("@cur_full_name_ot", simsobj.curriculum_full_name_ot),
                            new SqlParameter("@cur_status",simsobj.curriculum_status == true?"A":"I")
                         });
                            while (dr.Read())
                            {
                                string cnt = dr["param_cnt"].ToString();
                                if (cnt == "1")
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
               // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CUDCurriculum")]
        public HttpResponseMessage CUDCurriculum(List<Sims003> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},,METHOD : CUDCurriculum()PARAMETERS ::NA";
            Log.Debug(string.Format(debug, "ERP", "SETUP"));
            bool inserted = false;
            // Sims003 simsobj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims003>(data);
            Message message = new Message();
            try
            {
                if (data != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims003 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_cur_proc]",
                                new List<SqlParameter>() 
                         { 
                           new SqlParameter("@opr", simsobj.opr),
                            new SqlParameter("@cur_code", simsobj.curriculum_code),
                            new SqlParameter("@cur_short_name_en", simsobj.curriculum_short_name),
                            new SqlParameter("@cur_short_name_ar", simsobj.curriculum_short_name_ar),
                            new SqlParameter("@cur_short_name_fr", simsobj.curriculum_short_name_fr),
                            new SqlParameter("@cur_short_name_ot", simsobj.curriculum_short_name_ot),
                            new SqlParameter("@cur_full_name_en", simsobj.curriculum_full_name),
                            new SqlParameter("@cur_full_name_ar", simsobj.curriculum_full_name_ar),
                            new SqlParameter("@cur_full_name_fr", simsobj.curriculum_full_name_fr),
                            new SqlParameter("@cur_full_name_ot", simsobj.curriculum_full_name_ot),
                            new SqlParameter("@cur_status",simsobj.curriculum_status == true?"A":"I")
                         });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, inserted);
                }
            }
            catch (Exception x)
            {
                //Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}