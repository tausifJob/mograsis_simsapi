﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using log4net;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Models.SIMS;
using SIMSAPI.Models.SIMS.simsClass;

namespace SIMSAPI.Controllers.student
{
    [RoutePrefix("api/common/AttendanceRule")]

    public class AttendanceRuleController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [Route("getAutoGenerateRuleCode")]
        public HttpResponseMessage getAutoGenerateRuleCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAutoGenerateRuleCode(),PARAMETERS :: NA";
            Log.Debug(string.Format(debug, "Attendance", "getAutoGenerateRuleCode"));

            string commcode = string.Empty;
            Sims518 simsobj = new Sims518();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_attedance_rule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", 'M'),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            simsobj.sims_attendance_rule_code = dr["max"].ToString().Equals("") ? Convert.ToString(1) : dr["max"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, simsobj.sims_attendance_rule_code);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, simsobj.sims_attendance_rule_code);
            }
        }

        [Route("getAttendanceRule")]
        public HttpResponseMessage getAttendanceRule()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendanceRule(),PARAMETERS :: NO";
            Log.Debug(string.Format(debug, "Attendance", "AttendanceRule"));

            List<Sims518> list = new List<Sims518>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_attedance_rule_proc]",
                        new List<SqlParameter>() 
                         { 
                            new SqlParameter("@opr", 'S'),
                          
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims518 simsobj = new Sims518();
                            simsobj.sims_academic_year_desc = dr["acad_desc"].ToString();
                            simsobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            simsobj.sims_cur_name = dr["sims_cur_name"].ToString();
                            simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            simsobj.sims_grade_name = dr["sims_grade_name"].ToString();
                            simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            simsobj.sims_attendance_attendance_code = dr["sims_attendance_attendance_code"].ToString();
                            simsobj.sims_attendance_attendance_name = dr["sims_attendance_attendance_name"].ToString();
                            simsobj.sims_attendance_category_code = dr["sims_attendance_category_code"].ToString();
                            simsobj.sims_attendance_consequence_code = dr["sims_attendance_consequence_code"].ToString();
                            simsobj.sims_attendance_consequence_name = dr["sims_attendance_consequence_name"].ToString();
                            simsobj.sims_attendance_rule_code = dr["sims_attendance_rule_code"].ToString();
                            simsobj.sims_attendance_rule_name = dr["sims_attendance_rule_name"].ToString();
                            simsobj.sims_attendance_threshold_value1 = dr["sims_attendance_threshold_value1"].ToString();
                            simsobj.sims_attendance_threshold_value2 = dr["sims_attendance_threshold_value2"].ToString();
                            simsobj.sims_attendance_threshold_value3 = dr["sims_attendance_threshold_value3"].ToString();
                            simsobj.sims_attendance_threshold_value4 = dr["sims_attendance_threshold_value4"].ToString();
                            simsobj.sims_attendance_threshold_value5 = dr["sims_attendance_threshold_value5"].ToString();
                            if (dr["sims_attendance_academic_year_flag"].ToString() == "A")
                                simsobj.sims_attendance_academic_year_flag = true;
                            else
                                simsobj.sims_attendance_academic_year_flag = false;

                            if (dr["sims_attendance_report_card_flag"].ToString() == "A")
                                simsobj.sims_attendance_report_card_flag = true;
                            else
                                simsobj.sims_attendance_report_card_flag = false;

                            if (dr["sims_attendance_term_flag"].ToString() == "A")
                                simsobj.sims_attendance_term_flag = true;
                            else
                                simsobj.sims_attendance_term_flag = false;
                            list.Add(simsobj);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            // return Request.CreateResponse(HttpStatusCode.OK, moderator_list);
        }

        [Route("CUDAttendanceRule")]
        public HttpResponseMessage CUDAttendanceRule(List<Sims518> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : CUDAttendanceRule(),PARAMETERS :: No";
            Log.Debug(string.Format(debug, "Attendance", "CUDAttendanceRule"));

            Message message = new Message();
            bool inserted = false;
            try
            {

                if (data != null)
                {

                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        foreach (Sims518 simsobj in data)
                        {
                            int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_attedance_rule_proc]",
                                new List<SqlParameter>() 
                             {                             
                                 new SqlParameter("@opr", simsobj.opr),
                                 new SqlParameter("@sims_academic_year", simsobj.sims_academic_year),
                                 new SqlParameter("@sims_cur_code", simsobj.sims_cur_code),
                                 new SqlParameter("@sims_grade_code", simsobj.sims_grade_code),
                                 new SqlParameter("@sims_attendance_rule_code", simsobj.sims_attendance_rule_code),
                                 new SqlParameter("@sims_attendance_rule_name", simsobj.sims_attendance_rule_name),
                                 new SqlParameter("@sims_attendance_category_code", simsobj.sims_attendance_category_code),
                                 new SqlParameter("@sims_attendance_attendance_code", simsobj.sims_attendance_attendance_code),
                                 new SqlParameter("@sims_attendance_threshold_value1", simsobj.sims_attendance_threshold_value1),
                                 new SqlParameter("@sims_attendance_threshold_value2", simsobj.sims_attendance_threshold_value2),
                                 new SqlParameter("@sims_attendance_threshold_value3", simsobj.sims_attendance_threshold_value3),
                                 new SqlParameter("@sims_attendance_threshold_value4", simsobj.sims_attendance_threshold_value4),
                                 new SqlParameter("@sims_attendance_threshold_value5", simsobj.sims_attendance_threshold_value5),
                                 new SqlParameter("@sims_attendance_consequence_code", simsobj.sims_attendance_consequence_code),
                                 new SqlParameter("@sims_attendance_report_card_flag", simsobj.sims_attendance_report_card_flag == true?"A":"I"),
                                 new SqlParameter("@sims_attendance_term_flag", simsobj.sims_attendance_term_flag == true?"A":"I"),
                                 new SqlParameter("@sims_attendance_academic_year_flag", simsobj.sims_attendance_academic_year_flag == true?"A":"I")
                             });
                            if (ins > 0)
                            {
                                inserted = true;
                            }
                            else
                            {
                                inserted = false;
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getConsequence")]
        public HttpResponseMessage getConsequence(string cur_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getConsequence(),PARAMETERS :: CURNAME{2}";
            Log.Debug(string.Format(debug, "CONSEQUENCE", "getConsequence", cur_name));

            List<Sims518> mod_list = new List<Sims518>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_attedance_rule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_cur_code", cur_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims518 simsobj = new Sims518();
                            simsobj.sims_attendance_consequence_code = dr["sims_incidence_consequence_code"].ToString();
                            simsobj.sims_attendance_consequence_name = dr["sims_incidence_consequence_desc"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("getAttendance")]
        public HttpResponseMessage getAttendance(string cur_name)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAttendance(),PARAMETERS :: CURNAME{2}";
            Log.Debug(string.Format(debug, "ATTENDANCE", "getAttendance", cur_name));

            List<Sims518> mod_list = new List<Sims518>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_attedance_rule_proc]",
                        new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr", "A"),
                                new SqlParameter("@sims_cur_code", cur_name)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims518 simsobj = new Sims518();
                            simsobj.sims_attendance_attendance_code = dr["sims_attendance_code"].ToString();
                            simsobj.sims_attendance_attendance_name = dr["sims_attendance_description"].ToString();
                            mod_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }
    }
}