﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using SIMSAPI.Models.Common;

using SIMSAPI.Models.ERP.messageClass;
using SIMSAPI.Models.COMMON;

namespace SIMSAPI.Controllers.modules.Common
{

    [RoutePrefix("api/common/ApplicationMaster")]
    public class CommonApplicationController : ApiController
    {
        [Route("getApplications")]
        public HttpResponseMessage getApplications()
        {
            List<Comn001> lstSequence = new List<Comn001>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 comnobj = new Comn001();
                            comnobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            comnobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();
                            comnobj.comn_appl_name_fr = dr["comn_appl_name_fr"].ToString();
                            comnobj.comn_appl_name_ot = dr["comn_appl_name_ot"].ToString();
                            comnobj.comn_appl_mod_code = dr["comn_appl_mod_code"].ToString();
                            comnobj.comn_appl_mod_name = dr["comn_appl_mod_name"].ToString();
                            comnobj.comn_app_mode = dr["comn_app_mode"].ToString();
                            comnobj.comn_appl_type_code = dr["appl_type"].ToString();
                            comnobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            comnobj.comn_appl_keywords = dr["comn_appl_keywords"].ToString();
                            if (dr["Comn_appl_status"].ToString().Equals("A"))
                                comnobj.comn_appl_status = true;
                            else
                                comnobj.comn_appl_status = false;

                            comnobj.comn_appl_version = dr["comn_appl_version"].ToString();
                            comnobj.comn_appl_location = dr["comn_appl_location"].ToString();
                            comnobj.comn_appl_logic = dr["comn_appl_logic"].ToString();
                            if (!string.IsNullOrEmpty(dr["comn_appl_display_order"].ToString()))
                                comnobj.comn_appl_display_order = int.Parse(dr["comn_appl_display_order"].ToString());
                            comnobj.comn_appl_tag_code = dr["appl_tag"].ToString();
                            comnobj.comn_appl_tag = dr["comn_appl_tag"].ToString();
                            comnobj.comn_appl_help_url = dr["comn_appl_help_url"].ToString();
                            comnobj.comn_appl_video_url = dr["comn_appl_video_url"].ToString();

                            lstSequence.Add(comnobj);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstSequence);
        }

        [Route("getApplicationsbyIndex")]
        public HttpResponseMessage getApplicationsbyIndex()
        {
            List<Comn001> lstSequence = new List<Comn001>();
            int total = 0, skip = 0;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", 'S')
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 comnobj = new Comn001();
                            comnobj.comn_appl_code = dr["comn_appl_code"].ToString();
                            comnobj.comn_appl_name_en = dr["comn_appl_name_en"].ToString();
                            comnobj.comn_appl_name_ar = dr["comn_appl_name_ar"].ToString();
                            comnobj.comn_appl_name_fr = dr["comn_appl_name_fr"].ToString();
                            comnobj.comn_appl_name_ot = dr["comn_appl_name_ot"].ToString();
                            comnobj.comn_appl_mod_code = dr["module_code"].ToString();
                            comnobj.comn_appl_mod_name = dr["comn_appl_mod_name"].ToString();
                            comnobj.comn_app_mode_code = dr["mode_code"].ToString();
                            comnobj.comn_app_mode = dr["comn_app_mode"].ToString();
                            comnobj.comn_appl_type_code = dr["appl_type"].ToString();
                            comnobj.comn_appl_type = dr["comn_appl_type"].ToString();
                            comnobj.comn_appl_keywords = dr["comn_appl_keywords"].ToString();
                            if (dr["Comn_appl_status"].ToString().Equals("A"))
                                comnobj.comn_appl_status = true;
                            else
                                comnobj.comn_appl_status = false;

                            comnobj.comn_appl_version = dr["comn_appl_version"].ToString();
                            comnobj.comn_appl_location = dr["comn_appl_location"].ToString();
                            comnobj.comn_appl_logic = dr["comn_appl_logic"].ToString();
                            if (!string.IsNullOrEmpty(dr["comn_appl_display_order"].ToString()))
                                comnobj.comn_appl_display_order = int.Parse(dr["comn_appl_display_order"].ToString());
                            comnobj.comn_appl_tag_code = dr["appl_tag"].ToString();
                            comnobj.comn_appl_tag = dr["comn_appl_tag"].ToString();
                            comnobj.comn_appl_help_url = dr["comn_appl_help_url"].ToString();
                            comnobj.comn_appl_video_url = dr["comn_appl_video_url"].ToString();
                            comnobj.comn_appl_location_html = dr["comn_appl_location_html"].ToString();

                            lstSequence.Add(comnobj);

                        }
                        return Request.CreateResponse(HttpStatusCode.OK, lstSequence);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, dr);
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "");
            }
        }

        [Route("CUDupdateInsertApplications")]
        public HttpResponseMessage CUDupdateInsertApplications(Comn001 obj)
        {
            Comn001 comnobj = obj;
            Message message = new Message();
            try
            {
                if (comnobj != null)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                            new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", comnobj.opr),
                             new SqlParameter("@comn_appl_code", comnobj.comn_appl_code),
                             new SqlParameter("@comn_appl_name_en", comnobj.comn_appl_name_en),
                             new SqlParameter("@comn_appl_name_ar", comnobj.comn_appl_name_ar),
                             new SqlParameter("@comn_appl_name_fr", comnobj.comn_appl_name_fr),
                             new SqlParameter("@comn_appl_name_ot", comnobj.comn_appl_name_ot),
                             new SqlParameter("@com_mode_name", comnobj.comn_appl_mod_code),
                             new SqlParameter("@sims_app_mode_name", comnobj.comn_app_mode),
                             new SqlParameter("@sims_appl_type_name", comnobj.comn_appl_type),
                             new SqlParameter("@comn_appl_keywords", comnobj.comn_appl_keywords),
                             new SqlParameter("@comn_appl_status",comnobj.comn_appl_status==true?"A":"I"),
                             new SqlParameter("@comn_appl_version", comnobj.comn_appl_version),
                             new SqlParameter("@comn_appl_location", comnobj.comn_appl_location),
                             new SqlParameter("@comn_appl_logic", comnobj.comn_appl_logic),
                             new SqlParameter("@comn_appl_display_order", comnobj.comn_appl_display_order),
                             new SqlParameter("@comn_appl_tag", comnobj.comn_appl_tag),
                             new SqlParameter("@comn_appl_help_url", comnobj.comn_appl_help_url),
                             new SqlParameter("@comn_appl_video_url", comnobj.comn_appl_video_url),
                             new SqlParameter("@comn_appl_location_html", comnobj.comn_appl_location_html),
                             
                             new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code),
                             new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code),
                             new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field),
                             new SqlParameter("@sims_appl_form_field1", CommonStaticClass.sims_appl_form_field1),
                             new SqlParameter("@sims_appl_form_field2", CommonStaticClass.sims_appl_form_field2)
                             
                             
                         });

                        if (dr.RecordsAffected > 0)
                        {
                            if (comnobj.opr.Equals("U"))
                                message.strMessage = "Application Information Updated Sucessfully";
                            if (comnobj.opr.Equals("I"))
                                message.strMessage = "Application Information Added Sucessfully";
                            if (comnobj.opr.Equals("D"))
                                message.strMessage = "Application Information Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        else
                        {

                            if (comnobj.opr.Equals("U"))
                                message.strMessage = "Application Information Updated Sucessfully";
                            if (comnobj.opr.Equals("I"))
                                message.strMessage = "Application Information Added Sucessfully";
                            if (comnobj.opr.Equals("D"))
                                message.strMessage = "Application Information Deleted Sucessfully";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }


                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
                else
                {
                    message.strMessage = "Error In Parsing Information";
                    message.messageType = MessageType.Error;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                }
            }
            catch (Exception x)
            {
                if (comnobj.opr.Equals("U"))
                    message.strMessage = "Application Information  Updated Sucessfully";
                if (comnobj.opr.Equals("I"))
                    message.strMessage = "Application Information Added Sucessfully";
                if (comnobj.opr.Equals("D"))
                    message.strMessage = "Application Information Deleted Sucessfully";

                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }


            return Request.CreateResponse(HttpStatusCode.OK, message);

        }

        [Route("CUDupdateInsertApplicationsNew")]
        public HttpResponseMessage CUDupdateInsertApplicationsNew(List<Comn001> obj)
        {
           // Comn001 comnobj = obj;
            Message message = new Message();
            try
            {
                //if (comnobj != null)
                foreach(Comn001 comnobj in obj)
                {
                    using (DBConnection db = new DBConnection())
                    {
                        db.Open();
                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                            new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", comnobj.opr),
                             new SqlParameter("@comn_appl_code", comnobj.comn_appl_code),
                             new SqlParameter("@comn_appl_name_en", comnobj.comn_appl_name_en),
                             new SqlParameter("@comn_appl_name_ar", comnobj.comn_appl_name_ar),
                             new SqlParameter("@comn_appl_name_fr", comnobj.comn_appl_name_fr),
                             new SqlParameter("@comn_appl_name_ot", comnobj.comn_appl_name_ot),
                             new SqlParameter("@com_mode_name", comnobj.comn_appl_mod_code),
                             new SqlParameter("@sims_app_mode_name", comnobj.comn_app_mode),
                             new SqlParameter("@sims_appl_type_name", comnobj.comn_appl_type),
                             new SqlParameter("@comn_appl_keywords", comnobj.comn_appl_keywords),
                             new SqlParameter("@comn_appl_status",comnobj.comn_appl_status==true?"A":"I"),
                             new SqlParameter("@comn_appl_version", comnobj.comn_appl_version),
                             new SqlParameter("@comn_appl_location", comnobj.comn_appl_location),
                             new SqlParameter("@comn_appl_logic", comnobj.comn_appl_logic),
                             new SqlParameter("@comn_appl_display_order", comnobj.comn_appl_display_order),
                             new SqlParameter("@comn_appl_tag", comnobj.comn_appl_tag),
                             new SqlParameter("@comn_appl_help_url", comnobj.comn_appl_help_url),
                             new SqlParameter("@comn_appl_video_url", comnobj.comn_appl_video_url),
                             new SqlParameter("@comn_appl_location_html", comnobj.comn_appl_location_html),

                             new SqlParameter("@sims_mod_code", CommonStaticClass.sims_mod_code),
                             new SqlParameter("@sims_appl_code", CommonStaticClass.sims_appl_code),
                             new SqlParameter("@sims_appl_form_field", CommonStaticClass.sims_appl_form_field),
                             new SqlParameter("@sims_appl_form_field1", CommonStaticClass.sims_appl_form_field1),
                             new SqlParameter("@sims_appl_form_field2", CommonStaticClass.sims_appl_form_field2)


                         });

                        if (dr.RecordsAffected > 0)
                        {
                            if (comnobj.opr.Equals("U"))
                                message.strMessage = "Application Information Updated Sucessfully";
                            if (comnobj.opr.Equals("I"))
                                message.strMessage = "Application Information Added Sucessfully";
                            if (comnobj.opr.Equals("D"))
                                message.strMessage = "Application Information Deleted Sucessfully";
                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }

                        else
                        {

                            if (comnobj.opr.Equals("U"))
                                message.strMessage = "Application Information Updated Sucessfully";
                            if (comnobj.opr.Equals("I"))
                                message.strMessage = "Application Information Added Sucessfully";
                            if (comnobj.opr.Equals("D"))
                                message.strMessage = "Application Information Deleted Sucessfully";

                            message.systemMessage = string.Empty;
                            message.messageType = MessageType.Success;
                        }


                        return Request.CreateResponse(HttpStatusCode.OK, message);
                    }
                }
              
            }
            catch (Exception x)
            {
                //if (comnobj.opr.Equals("U"))
                //    message.strMessage = "Application Information  Updated Sucessfully";
                //if (comnobj.opr.Equals("I"))
                //    message.strMessage = "Application Information Added Sucessfully";
                //if (comnobj.opr.Equals("D"))
                //    message.strMessage = "Application Information Deleted Sucessfully";

                message.systemMessage = string.Empty;
                message.messageType = MessageType.Success;
            }


            return Request.CreateResponse(HttpStatusCode.OK, message);

        }



        [Route("getModules")]
        public HttpResponseMessage getModules()
        {
            List<Comn001> lstCuriculum = new List<Comn001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         { 
                             new SqlParameter("@opr", "M"),
                           
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 sequence = new Comn001();
                            sequence.comn_appl_mod_code = dr["comn_mod_code"].ToString();
                            sequence.comn_appl_mod_name = dr["comn_mod_name_en"].ToString();
                            lstCuriculum.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstCuriculum);
        }

        [Route("getAppModes")]
        public HttpResponseMessage getAppModes()
        {
            List<Comn001> mod_list = new List<Comn001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         {      
                            new SqlParameter("@opr", "A"),
                           
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 comnobj = new Comn001();
                            comnobj.comn_app_mode = dr["sims_appl_form_field_value1"].ToString();
                            comnobj.comn_app_mode_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(comnobj);
                        }
                        dr.Close();
                    }


                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAppTypes")]
        public HttpResponseMessage getAppTypes()
        {
            List<Comn001> mod_list = new List<Comn001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         {      
                            new SqlParameter("@opr", "T"),
                            
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 comnobj = new Comn001();
                            comnobj.comn_appl_type = dr["sims_appl_form_field_value1"].ToString();
                            comnobj.comn_appl_type_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(comnobj);
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

        [Route("getAppTags")]
        public HttpResponseMessage getAppTags()
        {
            List<Comn001> mod_list = new List<Comn001>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.comn_application_proc",
                        new List<SqlParameter>() 
                         {      
                            new SqlParameter("@opr", "B"),
                            
                        });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Comn001 comnobj = new Comn001();
                            comnobj.comn_appl_tag = dr["sims_appl_form_field_value1"].ToString();
                            comnobj.comn_appl_tag_code = dr["sims_appl_parameter"].ToString();
                            mod_list.Add(comnobj);
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, mod_list);
        }

    }
}   