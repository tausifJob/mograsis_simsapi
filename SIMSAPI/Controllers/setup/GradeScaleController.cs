﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/grd")]
    public class GradeScaleController : ApiController
    {

        [Route("getGrd")]
        public HttpResponseMessage getLoc()
        {
            List<Sim685> loc = new List<Sim685>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_assesment_grade_scale_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim685 obj = new Sim685();

                            obj.sims_mark_assesment_grade_code = dr["sims_mark_assesment_grade_code"].ToString();
                            obj.sims_mark_assesment_grade_name = dr["sims_mark_assesment_grade_name"].ToString();
                            //obj.sims_location_status = dr["sims_location_status"].Equals("A") ? true : false;
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_cur_code = dr["sims_cur_code"].ToString();
                            loc.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, loc);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, loc);
        }


        [Route("GradeScaleeCUD")]
        public HttpResponseMessage GradeScaleeCUD(List<Sim685> data)
        {

            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim685 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("sims.sims_assesment_grade_scale_proc",
                           new List<SqlParameter>() 
                         { 
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@sims_cur_code",simsobj.sims_cur_code),
                                 new SqlParameter("@sims_academic_year",simsobj.sims_academic_year),
                                new SqlParameter("@sims_mark_assesment_grade_name", simsobj.sims_mark_assesment_grade_name),
                                 new SqlParameter("@grade_assesment_code", simsobj.sims_mark_assesment_grade_code),
                                //new SqlParameter("@sims_location_status",simsobj.sims_location_status==true?"A":"I")
                         });
                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }

                }

            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }



    }
}