﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/UpdateCancleStudentTransDetails")]
    public class TransportCancleUpdateController : ApiController
    {

        #region API FOR STUDENT UPDATE/CANCEL BY USING GRADE & SECTION OR ENROLL NUMBER...

        [Route("getStudentTransportDetails")]
        public HttpResponseMessage getStudentTransportDetails(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code)
        {

            double sims_1f = 0, sims_2f = 0, sims_3f = 0, sims_4f = 0, sims_5f = 0, sims_6f = 0, sims_7f = 0, sims_8f = 0, sims_9f = 0, sims_10f = 0, sims_11f = 0, sims_12f = 0;
            double Paid = 0;
            double Unpaid = 0;
            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_transport_cancel",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            if (!string.IsNullOrEmpty(dr["sims_png"].ToString()))
                            {
                                obj.sims_png = dr["sims_png"].ToString();
                                StudentTrans_list.Add(obj);

                                //{
                                //    st = dr["dp_value"].ToString();
                                //    msg.strMessage = st;
                                //}
                            }
                            else
                            {
                                
                                obj.sims_png = dr["sims_png"].ToString();
                                obj.sims_student_name = dr["sims_student_name"].ToString();
                                obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                try
                                {
                                    obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                }
                                catch (Exception ex)
                                {
                                    obj.sims_fee_cur_code = "";
                                }
                                try
                                {
                                    obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                                }
                                catch (Exception)
                                {

                                    obj.sims_fee_grade_code = "";
                                }
                                obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                                }
                                catch (Exception)
                                {

                                    obj.sims_fee_section_code = "";
                                }
                                obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_number = dr["sims_fee_number"].ToString();
                                }
                                catch (Exception)
                                {
                                    obj.sims_fee_number = "";
                                }
                                try
                                {
                                    obj.sims_fee_code = dr["sims_fee_code"].ToString();
                                }
                                catch (Exception)
                                {
                                    obj.sims_fee_code = "";
                                }
                                obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                                obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                                obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                                obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                                obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                                obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                                obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                                obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                                obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                                obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                                obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                                obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                                obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                                obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                                obj.V_Name = dr["V_Name"].ToString();
                                obj.V_Code = dr["V_Code"].ToString();
                                obj.V_Name_Code = dr["V_Name_Code"].ToString();

                                obj.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                                obj.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                                obj.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                                obj.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                                obj.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                                obj.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                                obj.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                                obj.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                                obj.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                                obj.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                                obj.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                                obj.sims_fee_period12 = dr["sims_fee_period12"].ToString();


                                obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                                obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                                obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                                obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                                obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                                obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                                obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                                obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                                obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                                obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                                obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                                obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                                obj.sims_fee_period1_paid = dr["sims_fee_period1_paid"].ToString();
                                obj.sims_fee_period2_paid = dr["sims_fee_period2_paid"].ToString();
                                obj.sims_fee_period3_paid = dr["sims_fee_period3_paid"].ToString();
                                obj.sims_fee_period4_paid = dr["sims_fee_period4_paid"].ToString();
                                obj.sims_fee_period5_paid = dr["sims_fee_period5_paid"].ToString();
                                obj.sims_fee_period6_paid = dr["sims_fee_period6_paid"].ToString();
                                obj.sims_fee_period7_paid = dr["sims_fee_period7_paid"].ToString();
                                obj.sims_fee_period8_paid = dr["sims_fee_period8_paid"].ToString();
                                obj.sims_fee_period9_paid = dr["sims_fee_period9_paid"].ToString();
                                obj.sims_fee_period10_paid = dr["sims_fee_period10_paid"].ToString();
                                obj.sims_fee_period11_paid = dr["sims_fee_period11_paid"].ToString();
                                obj.sims_fee_period12_paid = dr["sims_fee_period12_paid"].ToString();

                                try
                                {
                                    sims_1 = double.Parse(dr["sims_fee_period1_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_1 = 0.0;
                                }
                                try
                                {
                                    sims_2 = double.Parse(dr["sims_fee_period2_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_2 = 0.0;
                                }
                                try
                                {
                                    sims_3 = double.Parse(dr["sims_fee_period3_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_3 = 0.0;
                                }
                                try
                                {
                                    sims_4 = double.Parse(dr["sims_fee_period4_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_4 = 0.0;
                                }
                                try
                                {
                                    sims_5 = double.Parse(dr["sims_fee_period5_paid"].ToString());
                                }
                                catch (Exception )
                                {
                                    sims_5 = 0.0;
                                }
                                try
                                {
                                    sims_6 = double.Parse(dr["sims_fee_period6_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_6 = 0.0;
                                }
                                try
                                {
                                    sims_7 = double.Parse(dr["sims_fee_period7_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_7 = 0.0;
                                }
                                try
                                {
                                    sims_8 = double.Parse(dr["sims_fee_period8_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_8 = 0.0;
                                }
                                try
                                {
                                    sims_9 = double.Parse(dr["sims_fee_period9_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_9 = 0.0;
                                }
                                try
                                {
                                    sims_10 = double.Parse(dr["sims_fee_period10_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_10 = 0.0;
                                }
                                try
                                {
                                    sims_11 = double.Parse(dr["sims_fee_period11_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_11 = 0.0;
                                }
                                try
                                {
                                    sims_12 = double.Parse(dr["sims_fee_period12_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_12 = 0.0;
                                }

                                try
                                {
                                    sims_1f = double.Parse(dr["sims_fee_period1"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_1f = 0.0;
                                }
                                try
                                {
                                    sims_2f = double.Parse(dr["sims_fee_period2"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_2f = 0.0;
                                }
                                try
                                {
                                    sims_3f = double.Parse(dr["sims_fee_period3"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_3f = 0.0;
                                }
                                try
                                {
                                    sims_4f = double.Parse(dr["sims_fee_period4"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_4f = 0.0;
                                }
                                try
                                {
                                    sims_5f = double.Parse(dr["sims_fee_period5"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_5f = 0.0;
                                }
                                try
                                {
                                    sims_6f = double.Parse(dr["sims_fee_period6"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_6f = 0.0;
                                }
                                try
                                {
                                    sims_7f = double.Parse(dr["sims_fee_period7"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_7f = 0.0;
                                }
                                try
                                {
                                    sims_8f = double.Parse(dr["sims_fee_period8"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_8f = 0.0;
                                }
                                try
                                {
                                    sims_9f = double.Parse(dr["sims_fee_period9"].ToString());
                                }
                                catch (Exception)
                                {

                                    sims_9f = 0.0;
                                }
                                try
                                {
                                    sims_10f = double.Parse(dr["sims_fee_period10"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_10f = 0.0;
                                }
                                try
                                {
                                    sims_11f = double.Parse(dr["sims_fee_period11"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_11f = 0.0;
                                }
                                try
                                {
                                    sims_12f = double.Parse(dr["sims_fee_period12"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_12f = 0.0;
                                }

                                Paid = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                                obj.sims_paid_total = Paid;
                                Unpaid = sims_1f + sims_2f + sims_3f + sims_4f + sims_5f + sims_6f + sims_7f + sims_8f + sims_9f + sims_10f + sims_11f + sims_12f;
                                obj.sims_pending_amount = Unpaid - Paid;
                                obj.sims_total_amount = Unpaid;
                                
                                StudentTrans_list.Add(obj);
                            }
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        [Route("getStudentTransportDetails_AHGS")]
        public HttpResponseMessage getStudentTransportDetails_AHGS(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code)
        {

            double sims_1f = 0, sims_2f = 0, sims_3f = 0, sims_4f = 0, sims_5f = 0, sims_6f = 0, sims_7f = 0, sims_8f = 0, sims_9f = 0, sims_10f = 0, sims_11f = 0, sims_12f = 0;
            double Paid = 0;
            double Unpaid = 0;
            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_transport_cancel_AHGS",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            if (!string.IsNullOrEmpty(dr["sims_png"].ToString()))
                            {
                                obj.sims_png = dr["sims_png"].ToString();
                                StudentTrans_list.Add(obj);

                                //{
                                //    st = dr["dp_value"].ToString();
                                //    msg.strMessage = st;
                                //}
                            }
                            else
                            {

                                obj.sims_png = dr["sims_png"].ToString();
                                obj.sims_student_name = dr["sims_student_name"].ToString();
                                obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                try
                                {
                                    obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                }
                                catch (Exception ex)
                                {
                                    obj.sims_fee_cur_code = "";
                                }
                                try
                                {
                                    obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                                }
                                catch (Exception)
                                {

                                    obj.sims_fee_grade_code = "";
                                }
                                obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                                }
                                catch (Exception)
                                {

                                    obj.sims_fee_section_code = "";
                                }
                                obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_number = dr["sims_fee_number"].ToString();
                                }
                                catch (Exception)
                                {
                                    obj.sims_fee_number = "";
                                }
                                try
                                {
                                    obj.sims_fee_code = dr["sims_fee_code"].ToString();
                                }
                                catch (Exception)
                                {
                                    obj.sims_fee_code = "";
                                }
                                obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                                obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                                obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                                obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                                obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                                obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                                obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                                obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                                obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                                obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                                obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                                obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                                obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                                obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                                obj.V_Name = dr["V_Name"].ToString();
                                obj.V_Code = dr["V_Code"].ToString();
                                obj.V_Name_Code = dr["V_Name_Code"].ToString();

                                obj.sims_transport_vehicle_safe_travel_code = dr["sims_transport_vehicle_safe_travel_code"].ToString();
                                obj.sims_transport_grade_code = dr["sims_transport_grade_code"].ToString();
                                obj.sims_transport_section_code = dr["sims_transport_section_code"].ToString();
                                obj.rfid_card = dr["rfid_card"].ToString();
                                obj.sims_student_gender = dr["sims_student_gender"].ToString();
                                obj.sims_student_dob = dr["sims_student_dob"].ToString();

                                obj.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                                obj.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                                obj.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                                obj.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                                obj.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                                obj.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                                obj.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                                obj.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                                obj.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                                obj.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                                obj.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                                obj.sims_fee_period12 = dr["sims_fee_period12"].ToString();


                                obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                                obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                                obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                                obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                                obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                                obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                                obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                                obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                                obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                                obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                                obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                                obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                                obj.sims_fee_period1_paid = dr["sims_fee_period1_paid"].ToString();
                                obj.sims_fee_period2_paid = dr["sims_fee_period2_paid"].ToString();
                                obj.sims_fee_period3_paid = dr["sims_fee_period3_paid"].ToString();
                                obj.sims_fee_period4_paid = dr["sims_fee_period4_paid"].ToString();
                                obj.sims_fee_period5_paid = dr["sims_fee_period5_paid"].ToString();
                                obj.sims_fee_period6_paid = dr["sims_fee_period6_paid"].ToString();
                                obj.sims_fee_period7_paid = dr["sims_fee_period7_paid"].ToString();
                                obj.sims_fee_period8_paid = dr["sims_fee_period8_paid"].ToString();
                                obj.sims_fee_period9_paid = dr["sims_fee_period9_paid"].ToString();
                                obj.sims_fee_period10_paid = dr["sims_fee_period10_paid"].ToString();
                                obj.sims_fee_period11_paid = dr["sims_fee_period11_paid"].ToString();
                                obj.sims_fee_period12_paid = dr["sims_fee_period12_paid"].ToString();

                                try
                                {
                                    sims_1 = double.Parse(dr["sims_fee_period1_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_1 = 0.0;
                                }
                                try
                                {
                                    sims_2 = double.Parse(dr["sims_fee_period2_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_2 = 0.0;
                                }
                                try
                                {
                                    sims_3 = double.Parse(dr["sims_fee_period3_paid"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    sims_3 = 0.0;
                                }
                                try
                                {
                                    sims_4 = double.Parse(dr["sims_fee_period4_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_4 = 0.0;
                                }
                                try
                                {
                                    sims_5 = double.Parse(dr["sims_fee_period5_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_5 = 0.0;
                                }
                                try
                                {
                                    sims_6 = double.Parse(dr["sims_fee_period6_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_6 = 0.0;
                                }
                                try
                                {
                                    sims_7 = double.Parse(dr["sims_fee_period7_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_7 = 0.0;
                                }
                                try
                                {
                                    sims_8 = double.Parse(dr["sims_fee_period8_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_8 = 0.0;
                                }
                                try
                                {
                                    sims_9 = double.Parse(dr["sims_fee_period9_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_9 = 0.0;
                                }
                                try
                                {
                                    sims_10 = double.Parse(dr["sims_fee_period10_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_10 = 0.0;
                                }
                                try
                                {
                                    sims_11 = double.Parse(dr["sims_fee_period11_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_11 = 0.0;
                                }
                                try
                                {
                                    sims_12 = double.Parse(dr["sims_fee_period12_paid"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_12 = 0.0;
                                }

                                try
                                {
                                    sims_1f = double.Parse(dr["sims_fee_period1"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_1f = 0.0;
                                }
                                try
                                {
                                    sims_2f = double.Parse(dr["sims_fee_period2"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_2f = 0.0;
                                }
                                try
                                {
                                    sims_3f = double.Parse(dr["sims_fee_period3"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_3f = 0.0;
                                }
                                try
                                {
                                    sims_4f = double.Parse(dr["sims_fee_period4"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_4f = 0.0;
                                }
                                try
                                {
                                    sims_5f = double.Parse(dr["sims_fee_period5"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_5f = 0.0;
                                }
                                try
                                {
                                    sims_6f = double.Parse(dr["sims_fee_period6"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_6f = 0.0;
                                }
                                try
                                {
                                    sims_7f = double.Parse(dr["sims_fee_period7"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_7f = 0.0;
                                }
                                try
                                {
                                    sims_8f = double.Parse(dr["sims_fee_period8"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_8f = 0.0;
                                }
                                try
                                {
                                    sims_9f = double.Parse(dr["sims_fee_period9"].ToString());
                                }
                                catch (Exception)
                                {

                                    sims_9f = 0.0;
                                }
                                try
                                {
                                    sims_10f = double.Parse(dr["sims_fee_period10"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_10f = 0.0;
                                }
                                try
                                {
                                    sims_11f = double.Parse(dr["sims_fee_period11"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_11f = 0.0;
                                }
                                try
                                {
                                    sims_12f = double.Parse(dr["sims_fee_period12"].ToString());
                                }
                                catch (Exception)
                                {
                                    sims_12f = 0.0;
                                }

                                Paid = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                                obj.sims_paid_total = Paid;
                                Unpaid = sims_1f + sims_2f + sims_3f + sims_4f + sims_5f + sims_6f + sims_7f + sims_8f + sims_9f + sims_10f + sims_11f + sims_12f;
                                obj.sims_pending_amount = Unpaid - Paid;
                                obj.sims_total_amount = Unpaid;

                                StudentTrans_list.Add(obj);
                            }
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        [Route("getEmployeeTransportDetails")]        
        public HttpResponseMessage getEmployeeTransportDetails(string acayear, string common_user_code, string vehicle_code)
        {


            if (common_user_code == "undefined" || common_user_code == "null" || common_user_code == "")
                common_user_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null" || vehicle_code == "")
                vehicle_code = null;

            List<Sims525> EmployeeTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_employee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_transport_em_number", common_user_code),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();

                            obj.pays_employee_name = dr["pays_employee_name"].ToString();
                            obj.sims_transport_em_number = dr["sims_transport_em_number"].ToString();
                            obj.employee_name = (obj.pays_employee_name + '-' + obj.sims_transport_em_number);
                            obj.sims_transport_route_employee_code = dr["sims_transport_route_employee_code"].ToString();

                            obj.V_Name = dr["V_Name"].ToString();
                            obj.V_Code = dr["V_Code"].ToString();
                            obj.V_Name_Code = dr["V_Name_Code"].ToString();
                            obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                            obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                            obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();
                            obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());
                            obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();


                            obj.StartMonth = dr["StartMonth"].ToString();
                            obj.EndtMonth = dr["EndtMonth"].ToString();

                            EmployeeTrans_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, EmployeeTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, EmployeeTrans_list);
        }

        [Route("getBusCode")]
        public HttpResponseMessage getBusCode()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBusCode(),PARAMETERS :: NO)";

            List<Sims525> bus_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "E")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.V_Code = dr["V_Code"].ToString();
                            simsobj.V_Name = dr["V_Name"].ToString();
                            simsobj.V_Name_Code = dr["V_Name_Code"].ToString();
                            simsobj.sims_transport_vehicle_seating_capacity = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            bus_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_code);
            }
        }

        [Route("getBusCode_AHGS")]
        public HttpResponseMessage getBusCode_AHGS()
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBusCode(),PARAMETERS :: NO)";

            List<Sims525> bus_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_cancel_AHGS",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "E")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.V_Code = dr["V_Code"].ToString();
                            simsobj.V_Name = dr["V_Name"].ToString();
                            simsobj.V_Name_Code = dr["V_Name_Code"].ToString();
                            simsobj.sims_transport_vehicle_seating_capacity = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            simsobj.sims_transport_vehicle_safe_travel_code = dr["sims_transport_vehicle_safe_travel_code"].ToString();
                            bus_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_code);
            }
        }

        [Route("getAcademicDate")]
        public HttpResponseMessage getAcademicDate(string cur_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAcademicDate(),PARAMETERS :: NO)";

            List<Sims525> bus_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "F"),
                             new SqlParameter("@sims_transport_vehicle_code", cur_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_effective_from1 = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            simsobj.sims_transport_effective_upto1 = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            bus_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_code);
            }
        }

        [Route("getEffectiveDate")]
        public HttpResponseMessage getEffectiveDate(string date)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getEffectiveDate(),PARAMETERS :: NO)";

            List<Sims525> effectiveDate = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "G"),
                             new SqlParameter("@sims_transport_academic_year", date)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_effective_from1 = db.UIDDMMYYYYformat(dr["sims_academic_year_start_date"].ToString());
                            simsobj.sims_transport_effective_upto1 = db.UIDDMMYYYYformat(dr["sims_academic_year_end_date"].ToString());
                            simsobj.sims_academic_year_status = (dr["sims_academic_year_status"].ToString());
                            effectiveDate.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, effectiveDate);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, effectiveDate);
            }
        }

        [Route("getfeeeditaccess")]
        public HttpResponseMessage getfeeeditaccess(string user_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getBusCode(),PARAMETERS :: NO)";

            string usercode = null;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_update_employee_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "N"),
                             new SqlParameter("@user_code", user_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            usercode = dr["sims_appl_parameter"].ToString();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, usercode);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, usercode);
            }
            usercode = null;
        }

        [Route("getAcademicYear")]
        public HttpResponseMessage getAcademicYear()
        {
            List<Sims179> lstModules = new List<Sims179>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_academic_year_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W"),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims179 sequence = new Sims179();
                            sequence.sims_academic_year = dr["sims_academic_year"].ToString();
                            sequence.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            lstModules.Add(sequence);
                        }
                    }
                }
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, lstModules);
        }

        [Route("getDirectionByRouteCodeName")]
        public HttpResponseMessage getDirectionByRouteCodeName(string route_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getDirectionByRouteName(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "FLEET", "ALLDIRECTION"));

            List<Sims525> Direct_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                                new SqlParameter("@opr", "M"),
                                new SqlParameter("@sims_transport_route_code", route_code),
                                new SqlParameter("@sims_academic_year", aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            Direct_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Direct_list);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Direct_list);
            }
        }

        [Route("getRouteNameByBusCode")]
        public HttpResponseMessage getRouteNameByBusCode(string AcaYear, string Vehicle_Code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRouteNameByBusCode(),PARAMETERS :: NO)";


            List<Sims525> route_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@sims_academic_year", AcaYear),
                             new SqlParameter("@sims_transport_route_code", Vehicle_Code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_name"].ToString();
                            route_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, route_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, route_code);
            }
        }

        [Route("getStopByRoute")]
        public HttpResponseMessage getStopByRoute(string route_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStopByRoute(),PARAMETERS :: NO";


            List<Sims525> Stop_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "G"),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_academic_year", aca_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            simsobj.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            Stop_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Stop_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Stop_list);
            }
        }

        [Route("getFees")]
        public HttpResponseMessage getFees(string aca_year, string route_code, string route_direct, string sims_transport_effective_from, string sims_transport_effective_upto)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStopByRoute(),PARAMETERS :: NO";
            List<Sims525> fee_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "H"),
                             new SqlParameter("@sims_transport_academic_year", aca_year),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_route_direction", route_direct),
                             new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(sims_transport_effective_from)),
                             new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(sims_transport_effective_upto))
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                            simsobj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                            simsobj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                            simsobj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                            simsobj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                            simsobj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                            simsobj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                            simsobj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                            simsobj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                            simsobj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                            simsobj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                            simsobj.sims_transport_period12 = dr["sims_transport_period12"].ToString();
                            simsobj.sims_transport_toatal = dr["sims_transport_toatal"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("UpdateTransportCUD")]
        public HttpResponseMessage UpdateTransportCUD(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

                    foreach (Sims525 simobj in data)
                    {
                        double fp1 = 0, fp2 = 0, fp3 = 0, fp4 = 0, fp5 = 0, fp6 = 0, fp7 = 0, fp8 = 0, fp9 = 0, fp10 = 0, fp11 = 0, fp12 = 0;
                        double tp1 = 0, tp2 = 0, tp3 = 0, tp4 = 0, tp5 = 0, tp6 = 0, tp7 = 0, tp8 = 0, tp9 = 0, tp10 = 0, tp11 = 0, tp12 = 0;

                        if (simobj.sims_appl_parameter == "undefined" || simobj.sims_appl_parameter == "null" || simobj.sims_appl_parameter == "")
                            simobj.sims_appl_parameter = null;
                        if (simobj.sims_transport_pickup_stop_code_old == "undefined" || simobj.sims_transport_pickup_stop_code_old == "null" || simobj.sims_transport_pickup_stop_code_old == "")
                            simobj.sims_transport_pickup_stop_code_old = null;

                        string fDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                        DateTime fpDateo = DateTime.Parse(fDateo);
                        string uDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old.ToString());
                        DateTime upDateo = DateTime.Parse(uDateo);


                        if (simobj.sub_opr == "A")
                        {
                            sims_1 = double.Parse(simobj.sims_fee_period1);
                            sims_2 = double.Parse(simobj.sims_fee_period2);
                            sims_3 = double.Parse(simobj.sims_fee_period3);
                            sims_4 = double.Parse(simobj.sims_fee_period4);
                            sims_5 = double.Parse(simobj.sims_fee_period5);
                            sims_6 = double.Parse(simobj.sims_fee_period6);
                            sims_7 = double.Parse(simobj.sims_fee_period7);
                            sims_8 = double.Parse(simobj.sims_fee_period8);
                            sims_9 = double.Parse(simobj.sims_fee_period9);
                            sims_10 = double.Parse(simobj.sims_fee_period10);
                            sims_11 = double.Parse(simobj.sims_fee_period11);
                            sims_12 = double.Parse(simobj.sims_fee_period12);
                        }

                        else if (simobj.sub_opr == "B" || simobj.sub_opr == "C")
                        {
                            string fDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from.ToString());
                            DateTime fpDate = DateTime.Parse(fDate);
                            string uDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto.ToString());
                            DateTime upDate = DateTime.Parse(uDate);

                            #region

                            ////if (string.IsNullOrEmpty(simobj.sims_appl_parameter))
                            //{

                            //    fp1 = double.Parse(simobj.sims_fee_period1_paid);
                            //    fp2 = double.Parse(simobj.sims_fee_period2_paid);
                            //    fp3 = double.Parse(simobj.sims_fee_period3_paid);
                            //    fp4 = double.Parse(simobj.sims_fee_period4_paid);
                            //    fp5 = double.Parse(simobj.sims_fee_period5_paid);
                            //    fp6 = double.Parse(simobj.sims_fee_period6_paid);
                            //    fp7 = double.Parse(simobj.sims_fee_period7_paid);
                            //    fp8 = double.Parse(simobj.sims_fee_period8_paid);
                            //    fp9 = double.Parse(simobj.sims_fee_period9_paid);
                            //    fp10 = double.Parse(simobj.sims_fee_period10_paid);
                            //    fp11 = double.Parse(simobj.sims_fee_period11_paid);
                            //    fp12 = double.Parse(simobj.sims_fee_period12_paid);

                            //    tp1 = double.Parse(simobj.sims_transport_period1);
                            //    tp2 = double.Parse(simobj.sims_transport_period2);
                            //    tp3 = double.Parse(simobj.sims_transport_period3);
                            //    tp4 = double.Parse(simobj.sims_transport_period4);
                            //    tp5 = double.Parse(simobj.sims_transport_period5);
                            //    tp6 = double.Parse(simobj.sims_transport_period6);
                            //    tp7 = double.Parse(simobj.sims_transport_period7);
                            //    tp8 = double.Parse(simobj.sims_transport_period8);
                            //    tp9 = double.Parse(simobj.sims_transport_period9);
                            //    tp10 = double.Parse(simobj.sims_transport_period10);
                            //    tp11 = double.Parse(simobj.sims_transport_period11);
                            //    tp12 = double.Parse(simobj.sims_transport_period12);

                            //    #region condition check for B Block
                            //    if (upDate <= upDateo)
                            //    {
                            //        upDate = upDate.AddMonths(1);
                            //        for (DateTime k = upDate; k <= upDateo; )
                            //        {
                            //            #region
                            //            if (k.Month == 1)
                            //            {
                            //                sims_1 = fp1 - tp1;
                            //            }
                            //            else if (k.Month == 2)
                            //            {
                            //                sims_2 = fp2 - tp2;
                            //            }
                            //            else if (k.Month == 3)
                            //            {
                            //                sims_3 = fp3 - tp3;
                            //            }
                            //            else if (k.Month == 4)
                            //            {
                            //                sims_4 = fp4 - tp4;
                            //            }
                            //            else if (k.Month == 5)
                            //            {
                            //                sims_5 = fp5 - tp5;
                            //            }
                            //            else if (k.Month == 6)
                            //            {
                            //                sims_6 = fp6 - tp6;
                            //            }
                            //            else if (k.Month == 7)
                            //            {
                            //                sims_7 = fp7 - tp7;
                            //            }
                            //            else if (k.Month == 8)
                            //            {
                            //                sims_8 = fp8 - tp8;
                            //            }
                            //            else if (k.Month == 9)
                            //            {
                            //                sims_9 = fp9 - tp9;
                            //            }
                            //            else if (k.Month == 10)
                            //            {
                            //                sims_10 = fp10 - tp10;
                            //            }
                            //            else if (k.Month == 11)
                            //            {
                            //                sims_11 = fp11 - tp11;
                            //            }
                            //            else if (k.Month == 12)
                            //            {
                            //                sims_12 = fp12 - tp12;
                            //            }
                            //            k = k.AddMonths(1);
                            //            #endregion
                            //        }
                            //        upDate = upDate.AddMonths(-1);
                            //        for (DateTime k = fpDate; k <= upDate; )
                            //        {
                            //            #region
                            //            if (k.Month == 1)
                            //            {
                            //                sims_1 = fp1;
                            //            }
                            //            else if (k.Month == 2)
                            //            {
                            //                sims_2 = fp2;
                            //            }
                            //            else if (k.Month == 3)
                            //            {
                            //                sims_3 = fp3;
                            //            }
                            //            else if (k.Month == 4)
                            //            {
                            //                sims_4 = fp4;
                            //            }
                            //            else if (k.Month == 5)
                            //            {
                            //                sims_5 = fp5;
                            //            }
                            //            else if (k.Month == 6)
                            //            {
                            //                sims_6 = fp6;
                            //            }
                            //            else if (k.Month == 7)
                            //            {
                            //                sims_7 = fp7;
                            //            }
                            //            else if (k.Month == 8)
                            //            {
                            //                sims_8 = fp8;
                            //            }
                            //            else if (k.Month == 9)
                            //            {
                            //                sims_9 = fp9;
                            //            }
                            //            else if (k.Month == 10)
                            //            {
                            //                sims_10 = fp10;
                            //            }
                            //            else if (k.Month == 11)
                            //            {
                            //                sims_11 = fp11;
                            //            }
                            //            else if (k.Month == 12)
                            //            {
                            //                sims_12 = fp12;
                            //            }
                            //            k = k.AddMonths(1);
                            //            #endregion
                            //        }


                            //        if (fpDateo < fpDate)
                            //        {
                            //            for (DateTime k = fpDateo; k < fpDate; )
                            //            {
                            //                #region
                            //                if (k.Month == 1)
                            //                {
                            //                    sims_1 = fp1 - tp1;
                            //                }
                            //                else if (k.Month == 2)
                            //                {
                            //                    sims_2 = fp2 - tp2;
                            //                }
                            //                else if (k.Month == 3)
                            //                {
                            //                    sims_3 = fp3 - tp3;
                            //                }
                            //                else if (k.Month == 4)
                            //                {
                            //                    sims_4 = fp4 - tp4;
                            //                }
                            //                else if (k.Month == 5)
                            //                {
                            //                    sims_5 = fp5 - tp5;
                            //                }
                            //                else if (k.Month == 6)
                            //                {
                            //                    sims_6 = fp6 - tp6;
                            //                }
                            //                else if (k.Month == 7)
                            //                {
                            //                    sims_7 = fp7 - tp7;
                            //                }
                            //                else if (k.Month == 8)
                            //                {
                            //                    sims_8 = fp8 - tp8;
                            //                }
                            //                else if (k.Month == 9)
                            //                {
                            //                    sims_9 = fp9 - tp9;
                            //                }
                            //                else if (k.Month == 10)
                            //                {
                            //                    sims_10 = fp10 - tp10;
                            //                }
                            //                else if (k.Month == 11)
                            //                {
                            //                    sims_11 = fp11 - tp11;
                            //                }
                            //                else if (k.Month == 12)
                            //                {
                            //                    sims_12 = fp12 - tp12;
                            //                }
                            //                k = k.AddMonths(1);
                            //                #endregion
                            //            }
                            //        }

                            //    }
                            //    else if (upDateo <= upDate)
                            //    {
                            //        upDateo = upDateo.AddMonths(1);
                            //        for (DateTime k = upDateo; k <= upDate; )
                            //        {
                            //            #region
                            //            if (k.Month == 1)
                            //            {
                            //                sims_1 = fp1 + tp1;
                            //            }
                            //            else if (k.Month == 2)
                            //            {
                            //                sims_2 = fp2 + tp2;
                            //            }
                            //            else if (k.Month == 3)
                            //            {
                            //                sims_3 = fp3 + tp3;
                            //            }
                            //            else if (k.Month == 4)
                            //            {
                            //                sims_4 = fp4 + tp4;
                            //            }
                            //            else if (k.Month == 5)
                            //            {
                            //                sims_5 = fp5 + tp5;
                            //            }
                            //            else if (k.Month == 6)
                            //            {
                            //                sims_6 = fp6 + tp6;
                            //            }
                            //            else if (k.Month == 7)
                            //            {
                            //                sims_7 = fp7 + tp7;
                            //            }
                            //            else if (k.Month == 8)
                            //            {
                            //                sims_8 = fp8 + tp8;
                            //            }
                            //            else if (k.Month == 9)
                            //            {
                            //                sims_9 = fp9 + tp9;
                            //            }
                            //            else if (k.Month == 10)
                            //            {
                            //                sims_10 = fp10 + tp10;
                            //            }
                            //            else if (k.Month == 11)
                            //            {
                            //                sims_11 = fp11 + tp11;
                            //            }
                            //            else if (k.Month == 12)
                            //            {
                            //                sims_12 = fp12 + tp12;
                            //            }
                            //            k = k.AddMonths(1);
                            //            #endregion
                            //        }
                            //        upDateo = upDateo.AddMonths(-1);
                            //        for (DateTime k = fpDate; k <= upDateo; )
                            //        {
                            //            #region
                            //            if (k.Month == 1)
                            //            {
                            //                sims_1 = fp1;
                            //            }
                            //            else if (k.Month == 2)
                            //            {
                            //                sims_2 = fp2;
                            //            }
                            //            else if (k.Month == 3)
                            //            {
                            //                sims_3 = fp3;
                            //            }
                            //            else if (k.Month == 4)
                            //            {
                            //                sims_4 = fp4;
                            //            }
                            //            else if (k.Month == 5)
                            //            {
                            //                sims_5 = fp5;
                            //            }
                            //            else if (k.Month == 6)
                            //            {
                            //                sims_6 = fp6;
                            //            }
                            //            else if (k.Month == 7)
                            //            {
                            //                sims_7 = fp7;
                            //            }
                            //            else if (k.Month == 8)
                            //            {
                            //                sims_8 = fp8;
                            //            }
                            //            else if (k.Month == 9)
                            //            {
                            //                sims_9 = fp9;
                            //            }
                            //            else if (k.Month == 10)
                            //            {
                            //                sims_10 = fp10;
                            //            }
                            //            else if (k.Month == 11)
                            //            {
                            //                sims_11 = fp11;
                            //            }
                            //            else if (k.Month == 12)
                            //            {
                            //                sims_12 = fp12;
                            //            }
                            //            k = k.AddMonths(1);
                            //            #endregion
                            //        }
                            //    }
                            //    #endregion

                            //}

                            #endregion

                        }
                        else if (simobj.sub_opr == "E")
                        {
                            #region old data comment by Kishor (not updating fee)
                            //tp1 = double.Parse(simobj.sims_transport_period1);
                            //tp2 = double.Parse(simobj.sims_transport_period2);
                            //tp3 = double.Parse(simobj.sims_transport_period3);
                            //tp4 = double.Parse(simobj.sims_transport_period4);
                            //tp5 = double.Parse(simobj.sims_transport_period5);
                            //tp6 = double.Parse(simobj.sims_transport_period6);
                            //tp7 = double.Parse(simobj.sims_transport_period7);
                            //tp8 = double.Parse(simobj.sims_transport_period8);
                            //tp9 = double.Parse(simobj.sims_transport_period9);
                            //tp10 = double.Parse(simobj.sims_transport_period10);
                            //tp11 = double.Parse(simobj.sims_transport_period11);
                            //tp12 = double.Parse(simobj.sims_transport_period12);
                            #endregion
                            tp1 = double.Parse(simobj.sims_fee_period1);
                            tp2 = double.Parse(simobj.sims_fee_period2);
                            tp3 = double.Parse(simobj.sims_fee_period3);
                            tp4 = double.Parse(simobj.sims_fee_period4);
                            tp5 = double.Parse(simobj.sims_fee_period5);
                            tp6 = double.Parse(simobj.sims_fee_period6);
                            tp7 = double.Parse(simobj.sims_fee_period7);
                            tp8 = double.Parse(simobj.sims_fee_period8);
                            tp9 = double.Parse(simobj.sims_fee_period9);
                            tp10 = double.Parse(simobj.sims_fee_period10);
                            tp11 = double.Parse(simobj.sims_fee_period11);
                            tp12 = double.Parse(simobj.sims_fee_period12);

                            //string fDate = simobj.sims_transport_effective_from.ToString();
                            //DateTime fpDate = DateTime.Parse(fDate);
                            //string uDate = simobj.sims_transport_effective_upto.ToString();
                            //DateTime upDate = DateTime.Parse(uDate);

                            string fDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from.ToString());
                            DateTime fpDate = DateTime.Parse(fDate);
                            string uDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto.ToString());
                            DateTime upDate = DateTime.Parse(uDate);


                            #region condition check for B Block

                            //if (fpDate <= upDate)
                            {

                                for (DateTime k = fpDate; k <= upDate; )
                                {
                                    #region
                                    if (k.Month == 1)
                                    {
                                        sims_1 = tp1;
                                    }
                                    else if (k.Month == 2)
                                    {
                                        sims_2 = tp2;
                                    }
                                    else if (k.Month == 3)
                                    {
                                        sims_3 = tp3;
                                    }
                                    else if (k.Month == 4)
                                    {
                                        sims_4 = tp4;
                                    }
                                    else if (k.Month == 5)
                                    {
                                        sims_5 = tp5;
                                    }
                                    else if (k.Month == 6)
                                    {
                                        sims_6 = tp6;
                                    }
                                    else if (k.Month == 7)
                                    {
                                        sims_7 = tp7;
                                    }
                                    else if (k.Month == 8)
                                    {
                                        sims_8 = tp8;
                                    }
                                    else if (k.Month == 9)
                                    {
                                        sims_9 = tp9;
                                    }
                                    else if (k.Month == 10)
                                    {
                                        sims_10 = tp10;
                                    }
                                    else if (k.Month == 11)
                                    {
                                        sims_11 = tp11;
                                    }
                                    else if (k.Month == 12)
                                    {
                                        sims_12 = tp12;
                                    }
                                    k = k.AddMonths(1);
                                    #endregion
                                }
                            }

                            #endregion

                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_update",
                            new List<SqlParameter>()
                      {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sub_opr",simobj.sub_opr),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),

                          new SqlParameter("@cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),

                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),

                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          
                          new SqlParameter("@sims_student_passport_first_name_en",simobj.sims_png),

                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12),
                          new SqlParameter("@sims_pu7",simobj.sims_transport_toatal),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)
                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CancelTransport")]
        public HttpResponseMessage CancelTransport(Sims525 simobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

                    double fp1 = 0, fp2 = 0, fp3 = 0, fp4 = 0, fp5 = 0, fp6 = 0, fp7 = 0, fp8 = 0, fp9 = 0, fp10 = 0, fp11 = 0, fp12 = 0;
                    double tp1 = 0, tp2 = 0, tp3 = 0, tp4 = 0, tp5 = 0, tp6 = 0, tp7 = 0, tp8 = 0, tp9 = 0, tp10 = 0, tp11 = 0, tp12 = 0;

                    fp1 = double.Parse(simobj.sims_fee_period1);
                    fp2 = double.Parse(simobj.sims_fee_period2);
                    fp3 = double.Parse(simobj.sims_fee_period3);
                    fp4 = double.Parse(simobj.sims_fee_period4);
                    fp5 = double.Parse(simobj.sims_fee_period5);
                    fp6 = double.Parse(simobj.sims_fee_period6);
                    fp7 = double.Parse(simobj.sims_fee_period7);
                    fp8 = double.Parse(simobj.sims_fee_period8);
                    fp9 = double.Parse(simobj.sims_fee_period9);
                    fp10 = double.Parse(simobj.sims_fee_period10);
                    fp11 = double.Parse(simobj.sims_fee_period11);
                    fp12 = double.Parse(simobj.sims_fee_period12);

                    tp1 = double.Parse(simobj.sims_transport_period1);
                    tp2 = double.Parse(simobj.sims_transport_period2);
                    tp3 = double.Parse(simobj.sims_transport_period3);
                    tp4 = double.Parse(simobj.sims_transport_period4);
                    tp5 = double.Parse(simobj.sims_transport_period5);
                    tp6 = double.Parse(simobj.sims_transport_period6);
                    tp7 = double.Parse(simobj.sims_transport_period7);
                    tp8 = double.Parse(simobj.sims_transport_period8);
                    tp9 = double.Parse(simobj.sims_transport_period9);
                    tp10 = double.Parse(simobj.sims_transport_period10);
                    tp11 = double.Parse(simobj.sims_transport_period11);
                    tp12 = double.Parse(simobj.sims_transport_period12);

                    //string fDateo = simobj.sims_transport_effective_from_old + "";
                    string fDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old);
                    DateTime fpDateo = DateTime.Parse(fDateo);
                    //string uDateo = simobj.sims_transport_effective_upto_old + "";
                    string uDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old);
                    DateTime upDateo = DateTime.Parse(uDateo);
                    DateTime s_date = fpDateo;
                    DateTime e_date = upDateo;

                    if (simobj.sims_check_month_wise == false)
                    {
                        #region condition check for G Block
                        string vDate = null;
                        DateTime vpDate;
                        if (!string.IsNullOrEmpty(simobj.sims_transport_route_rejecte_date))
                        {
                            vDate = db.DBYYYYMMDDformat(simobj.sims_transport_route_rejecte_date.ToString());
                            vpDate = DateTime.Parse(vDate);
                        }
                        else
                        {
                            vDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                            vpDate = DateTime.Parse(vDate);
                            simobj.sims_transport_route_rejecte_date = null;
                        }
                        if (vpDate >= fpDateo || vpDate <= upDateo)
                        {
                            if (vpDate != fpDateo)
                                vpDate = vpDate.AddMonths(1);
                            for (DateTime k = vpDate; k <= upDateo; )
                            {
                                #region

                                if (k.Month == 1)
                                {
                                    sims_1 = fp1 - tp1;
                                }
                                else if (k.Month == 2)
                                {
                                    sims_2 = fp2 - tp2;
                                }
                                else if (k.Month == 3)
                                {
                                    sims_3 = fp3 - tp3;
                                }
                                else if (k.Month == 4)
                                {
                                    sims_4 = fp4 - tp4;
                                }
                                else if (k.Month == 5)
                                {
                                    sims_5 = fp5 - tp5;
                                }
                                else if (k.Month == 6)
                                {
                                    sims_6 = fp6 - tp6;
                                }
                                else if (k.Month == 7)
                                {
                                    sims_7 = fp7 - tp7;
                                }
                                else if (k.Month == 8)
                                {
                                    sims_8 = fp8 - tp8;
                                }
                                else if (k.Month == 9)
                                {
                                    sims_9 = fp9 - tp9;
                                }
                                else if (k.Month == 10)
                                {
                                    sims_10 = fp10 - tp10;
                                }
                                else if (k.Month == 11)
                                {
                                    sims_11 = fp11 - tp11;
                                }
                                else if (k.Month == 12)
                                {
                                    sims_12 = fp12 - tp12;
                                }
                                k = k.AddMonths(1);

                                #endregion
                            }
                            int day = vpDate.Day;
                            if (day == 01)
                                vpDate = vpDate.AddMonths(-2);
                            else
                                vpDate = vpDate.AddMonths(-1);

                            for (DateTime k = fpDateo; k <= vpDate; )
                            {
                                #region

                                if (k.Month == 1)
                                {
                                    sims_1 = fp1;
                                }
                                else if (k.Month == 2)
                                {
                                    sims_2 = fp2;
                                }
                                else if (k.Month == 3)
                                {
                                    sims_3 = fp3;
                                }
                                else if (k.Month == 4)
                                {
                                    sims_4 = fp4;
                                }
                                else if (k.Month == 5)
                                {
                                    sims_5 = fp5;
                                }
                                else if (k.Month == 6)
                                {
                                    sims_6 = fp6;
                                }
                                else if (k.Month == 7)
                                {
                                    sims_7 = fp7;
                                }
                                else if (k.Month == 8)
                                {
                                    sims_8 = fp8;
                                }
                                else if (k.Month == 9)
                                {
                                    sims_9 = fp9;
                                }
                                else if (k.Month == 10)
                                {
                                    sims_10 = fp10;
                                }
                                else if (k.Month == 11)
                                {
                                    sims_11 = fp11;
                                }
                                else if (k.Month == 12)
                                {
                                    sims_12 = fp12;
                                }
                                k = k.AddMonths(1);

                                #endregion
                            }
                        }
                        #endregion

                        #region Code For Send Data To Database...

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims_transport_update",
                          new List<SqlParameter>()
                        {

                          new SqlParameter("@opr", 'U'),
                          new SqlParameter("@sub_opr", 'G'),
                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),
                          new SqlParameter("@cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_validate_upto",db.DBYYYYMMDDformat(simobj.sims_transport_route_rejecte_date)),
                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12),

                          new SqlParameter("@sims_pu1",fp1),
                          new SqlParameter("@sims_pu2",fp2),
                          new SqlParameter("@sims_pu3",fp3),
                          new SqlParameter("@sims_pu4",fp4),
                          new SqlParameter("@sims_pu5",fp5),
                          new SqlParameter("@sims_pu6",fp6),
                          new SqlParameter("@sims_pu7",fp7),
                          new SqlParameter("@sims_pu8",fp8),
                          new SqlParameter("@sims_pu9",fp9),
                          new SqlParameter("@sims_pu10",fp10),
                          new SqlParameter("@sims_pu11",fp11),
                          new SqlParameter("@sims_pu12",fp12),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)

                       });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();

                        #endregion
                    }

                    else
                    {
                        temp t = new temp();
                        t.AYEndDate = e_date;
                        t.AYStartDate = s_date;

                        #region periods

                        if (simobj.sims_fee_period1_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(1)) == false)
                                t.periods.Add(t.periodDate(1));
                            sims_1 = fp1 - tp1;
                        }
                        else if (simobj.sims_fee_period1_paid == "0" || simobj.sims_fee_period1_paid == null)
                        {
                            sims_1 = fp1;
                        }

                        if (simobj.sims_fee_period2_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(2)) == false)
                                t.periods.Add(t.periodDate(2));
                            sims_2 = fp2 - tp2;
                        }
                        else if (simobj.sims_fee_period2_paid == "0" || simobj.sims_fee_period2_paid == null)
                        {
                            sims_2 = fp2;
                        }

                        if (simobj.sims_fee_period3_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(3)) == false)
                                t.periods.Add(t.periodDate(3));
                            sims_3 = fp3 - tp3;
                        }
                        else if (simobj.sims_fee_period3_paid == "0" || simobj.sims_fee_period3_paid == null)
                        {
                            sims_3 = fp3;
                        }

                        if (simobj.sims_fee_period4_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(4)) == false)
                                t.periods.Add(t.periodDate(4));
                            sims_4 = fp4 - tp4;
                        }
                        else if (simobj.sims_fee_period4_paid == "0" || simobj.sims_fee_period4_paid == null)
                        {
                            sims_4 = fp4;
                        }

                        if (simobj.sims_fee_period5_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(5)) == false)
                                t.periods.Add(t.periodDate(5));
                            sims_5 = fp5 - tp5;
                        }
                        else if (simobj.sims_fee_period5_paid == "0" || simobj.sims_fee_period5_paid == null)
                        {
                            sims_5 = fp5;
                        }

                        if (simobj.sims_fee_period6_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(6)) == false)
                                t.periods.Add(t.periodDate(6));
                            sims_6 = fp6 - tp6;
                        }
                        else if (simobj.sims_fee_period6_paid == "0" || simobj.sims_fee_period6_paid == null)
                        {
                            sims_6 = fp6;
                        }

                        if (simobj.sims_fee_period7_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(7)) == false)
                                t.periods.Add(t.periodDate(7));
                            sims_7 = fp7 - tp7;
                        }
                        else if (simobj.sims_fee_period7_paid == "0" || simobj.sims_fee_period7_paid == null)
                        {
                            sims_7 = fp7;
                        }

                        if (simobj.sims_fee_period8_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(8)) == false)
                                t.periods.Add(t.periodDate(8));
                            sims_8 = fp8 - tp8;
                        }
                        else if (simobj.sims_fee_period8_paid == "0" || simobj.sims_fee_period8_paid == null)
                        {
                            sims_8 = fp8;
                        }

                        if (simobj.sims_fee_period9_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(9)) == false)
                                t.periods.Add(t.periodDate(9));
                            sims_9 = fp9 - tp9;
                        }
                        else if (simobj.sims_fee_period9_paid == "0" || simobj.sims_fee_period9_paid == null)
                        {
                            sims_9 = fp9;
                        }

                        if (simobj.sims_fee_period10_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(10)) == false)
                                t.periods.Add(t.periodDate(10));
                            sims_10 = fp10 - tp10;
                        }
                        else if (simobj.sims_fee_period10_paid == "0" || simobj.sims_fee_period10_paid == null)
                        {
                            sims_10 = fp10;
                        }

                        if (simobj.sims_fee_period11_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(11)) == false)
                                t.periods.Add(t.periodDate(11));
                            sims_11 = fp11 - tp11;
                        }
                        else if (simobj.sims_fee_period11_paid == "0" || simobj.sims_fee_period11_paid == null)
                        {
                            sims_11 = fp11;
                        }

                        if (simobj.sims_fee_period12_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(12)) == false)
                                t.periods.Add(t.periodDate(12));
                            sims_12 = fp12 - tp12;
                        }
                        else if (simobj.sims_fee_period12_paid == "0" || simobj.sims_fee_period12_paid == null)
                        {
                            sims_12 = fp12;
                        }

                        #endregion

                        t.periods.Sort();
                        DateTime tempSDate = s_date;
                        foreach (var item in t.periods)
                        {
                            int b = tempSDate.CompareTo(item);
                            Sims525 tobj = new Sims525();
                            if (b < 0)
                            {

                                tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                                tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                                tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                                tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                                tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                                tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                                tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                                if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                    tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                                else
                                    tobj.sims_transport_pickup_stop_code = null;

                                if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                    tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                                else
                                    tobj.sims_transport_drop_stop_code = null;

                                tobj.V_Code = simobj.V_Name_Code;
                                tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                                tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                                tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;
                                t.AYStartDate = tempSDate;
                                t.AYEndDate = item.AddDays(-1);
                                tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                                tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;

                                tempSDate = item.AddMonths(1);
                            }
                            else
                            {
                                tempSDate = item.AddMonths(1);
                            }

                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims_transport_update",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from)),
                                    new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto)),

                                    new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from_new)),
                                    new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto_new)),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number)

                                });

                                if (dr1.Read())
                                {
                                    st = dr1[0].ToString();
                                    msg.strMessage = st;

                                }
                                if (dr1.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr1.Close();

                            }

                            #endregion
                        }

                        int l = tempSDate.CompareTo(e_date);
                        if (l < 0)
                        {
                            Sims525 tobj = new Sims525();
                            tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                            tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                            tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                            tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                            tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                            tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                            tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                            else
                                tobj.sims_transport_pickup_stop_code = null;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                            else
                                tobj.sims_transport_drop_stop_code = null;

                            tobj.V_Code = simobj.V_Name_Code;
                            tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                            tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                            tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;

                            t.AYStartDate = tempSDate;
                            t.AYEndDate = e_date;
                            tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                            tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;

                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_transport_update",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from)),
                                    new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto)),

                                    new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from_new)),
                                    new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto_new)),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number),
                                    new SqlParameter("@sims_transport_route_student_update_by",tobj.sims_transport_route_student_update_by),

                                });

                                if (dr2.Read())
                                {
                                    st = dr2[0].ToString();
                                    msg.strMessage = st;

                                }
                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr2.Close();

                            }

                            #endregion
                        }
                        /*
                        else
                        {
                            Sims525 tobj = new Sims525();
                            tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                            tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                            tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                            tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                            tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                            tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                            tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                            else
                                tobj.sims_transport_pickup_stop_code = null;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                            else
                                tobj.sims_transport_drop_stop_code = null;

                            tobj.V_Code = simobj.V_Name_Code;

                            tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                            tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                            tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;

                            t.AYStartDate = DateTime.Parse(tobj.sims_transport_effective_from);
                            t.AYEndDate = e_date;
                            tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                            tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;
                            
                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims_transport_update",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",tobj.sims_transport_effective_from),
                                    new SqlParameter("@sims_transport_effective_upto",tobj.sims_transport_effective_upto),

                                    new SqlParameter("@sims_transport_effective_from_new", tobj.sims_transport_effective_from_new),
                                    new SqlParameter("@sims_transport_effective_upto_new",tobj.sims_transport_effective_upto_new),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number)

                                });

                                if (dr2.Read())
                                {
                                    st = dr2[0].ToString();
                                    msg.strMessage = st;

                                }
                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr2.Close();

                            }

                            #endregion
                        }
                        */
                    }
                }
            }

            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        class temp
        {
            public DateTime AYStartDate { get; set; }
            public DateTime AYEndDate { get; set; }

            public List<DateTime> periods = new List<DateTime>();
            public DateTime periodDate(int period)
            {
                DateTime dt2 = AYEndDate;
                DateTime dt1 = AYStartDate;
                int d = dt1.Day;
                int y = period >= dt1.Month ? dt1.Year : dt2.Year;
                DateTime mydt = new DateTime(y, period, d);
                return mydt;
            }

        }

        #region New Cancel Update Block....

        [Route("UpdateTransportCUDN")]
        public HttpResponseMessage UpdateTransportCUDN(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Sims525 simobj in data)
                    {

                        if (simobj.sims_appl_parameter == "undefined" || simobj.sims_appl_parameter == "null" || simobj.sims_appl_parameter == "")
                            simobj.sims_appl_parameter = null;
                        if (simobj.sims_transport_pickup_stop_code_old == "undefined" || simobj.sims_transport_pickup_stop_code_old == "null" || simobj.sims_transport_pickup_stop_code_old == "")
                            simobj.sims_transport_pickup_stop_code_old = null;

                        string fDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                        DateTime fpDateo = DateTime.Parse(fDateo);
                        string uDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old.ToString());
                        DateTime upDateo = DateTime.Parse(uDateo);
                        //A,B,C,E

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_update_proc",
                        new List<SqlParameter>()
                        {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sub_opr",simobj.sub_opr),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),

                          new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),

                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),

                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)
                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("CancelTransportN")]
        public HttpResponseMessage CancelTransportN(Sims525 simobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    if (simobj.sims_check_month_wise == false)
                    {

                        #region Code For Send Data To Database...

                        SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_update_proc",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr", 'U'),
                          new SqlParameter("@sub_opr", 'G'),
                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),
                          new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_validate_upto",db.DBYYYYMMDDformat(simobj.sims_transport_route_rejecte_date)),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)
                       });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();

                        #endregion
                    }

                    else
                    {
                        double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

                        double fp1 = 0, fp2 = 0, fp3 = 0, fp4 = 0, fp5 = 0, fp6 = 0, fp7 = 0, fp8 = 0, fp9 = 0, fp10 = 0, fp11 = 0, fp12 = 0;
                        double tp1 = 0, tp2 = 0, tp3 = 0, tp4 = 0, tp5 = 0, tp6 = 0, tp7 = 0, tp8 = 0, tp9 = 0, tp10 = 0, tp11 = 0, tp12 = 0;

                        fp1 = double.Parse(simobj.sims_fee_period1);
                        fp2 = double.Parse(simobj.sims_fee_period2);
                        fp3 = double.Parse(simobj.sims_fee_period3);
                        fp4 = double.Parse(simobj.sims_fee_period4);
                        fp5 = double.Parse(simobj.sims_fee_period5);
                        fp6 = double.Parse(simobj.sims_fee_period6);
                        fp7 = double.Parse(simobj.sims_fee_period7);
                        fp8 = double.Parse(simobj.sims_fee_period8);
                        fp9 = double.Parse(simobj.sims_fee_period9);
                        fp10 = double.Parse(simobj.sims_fee_period10);
                        fp11 = double.Parse(simobj.sims_fee_period11);
                        fp12 = double.Parse(simobj.sims_fee_period12);

                        tp1 = double.Parse(simobj.sims_transport_period1);
                        tp2 = double.Parse(simobj.sims_transport_period2);
                        tp3 = double.Parse(simobj.sims_transport_period3);
                        tp4 = double.Parse(simobj.sims_transport_period4);
                        tp5 = double.Parse(simobj.sims_transport_period5);
                        tp6 = double.Parse(simobj.sims_transport_period6);
                        tp7 = double.Parse(simobj.sims_transport_period7);
                        tp8 = double.Parse(simobj.sims_transport_period8);
                        tp9 = double.Parse(simobj.sims_transport_period9);
                        tp10 = double.Parse(simobj.sims_transport_period10);
                        tp11 = double.Parse(simobj.sims_transport_period11);
                        tp12 = double.Parse(simobj.sims_transport_period12);

                        string fDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old) + "";
                        DateTime fpDateo = DateTime.Parse(fDateo);
                        string uDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old) + "";
                        DateTime upDateo = DateTime.Parse(uDateo);
                        DateTime s_date = fpDateo;
                        DateTime e_date = upDateo;

                        temp1 t = new temp1();
                        t.AYEndDate = e_date;
                        t.AYStartDate = s_date;

                        #region periods

                        if (simobj.sims_fee_period1_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(1)) == false)
                                t.periods.Add(t.periodDate(1));
                            sims_1 = fp1 - tp1;
                        }
                        else if (simobj.sims_fee_period1_paid == "0" || simobj.sims_fee_period1_paid == null)
                        {
                            sims_1 = fp1;
                        }

                        if (simobj.sims_fee_period2_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(2)) == false)
                                t.periods.Add(t.periodDate(2));
                            sims_2 = fp2 - tp2;
                        }
                        else if (simobj.sims_fee_period2_paid == "0" || simobj.sims_fee_period2_paid == null)
                        {
                            sims_2 = fp2;
                        }

                        if (simobj.sims_fee_period3_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(3)) == false)
                                t.periods.Add(t.periodDate(3));
                            sims_3 = fp3 - tp3;
                        }
                        else if (simobj.sims_fee_period3_paid == "0" || simobj.sims_fee_period3_paid == null)
                        {
                            sims_3 = fp3;
                        }

                        if (simobj.sims_fee_period4_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(4)) == false)
                                t.periods.Add(t.periodDate(4));
                            sims_4 = fp4 - tp4;
                        }
                        else if (simobj.sims_fee_period4_paid == "0" || simobj.sims_fee_period4_paid == null)
                        {
                            sims_4 = fp4;
                        }

                        if (simobj.sims_fee_period5_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(5)) == false)
                                t.periods.Add(t.periodDate(5));
                            sims_5 = fp5 - tp5;
                        }
                        else if (simobj.sims_fee_period5_paid == "0" || simobj.sims_fee_period5_paid == null)
                        {
                            sims_5 = fp5;
                        }

                        if (simobj.sims_fee_period6_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(6)) == false)
                                t.periods.Add(t.periodDate(6));
                            sims_6 = fp6 - tp6;
                        }
                        else if (simobj.sims_fee_period6_paid == "0" || simobj.sims_fee_period6_paid == null)
                        {
                            sims_6 = fp6;
                        }

                        if (simobj.sims_fee_period7_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(7)) == false)
                                t.periods.Add(t.periodDate(7));
                            sims_7 = fp7 - tp7;
                        }
                        else if (simobj.sims_fee_period7_paid == "0" || simobj.sims_fee_period7_paid == null)
                        {
                            sims_7 = fp7;
                        }

                        if (simobj.sims_fee_period8_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(8)) == false)
                                t.periods.Add(t.periodDate(8));
                            sims_8 = fp8 - tp8;
                        }
                        else if (simobj.sims_fee_period8_paid == "0" || simobj.sims_fee_period8_paid == null)
                        {
                            sims_8 = fp8;
                        }

                        if (simobj.sims_fee_period9_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(9)) == false)
                                t.periods.Add(t.periodDate(9));
                            sims_9 = fp9 - tp9;
                        }
                        else if (simobj.sims_fee_period9_paid == "0" || simobj.sims_fee_period9_paid == null)
                        {
                            sims_9 = fp9;
                        }

                        if (simobj.sims_fee_period10_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(10)) == false)
                                t.periods.Add(t.periodDate(10));
                            sims_10 = fp10 - tp10;
                        }
                        else if (simobj.sims_fee_period10_paid == "0" || simobj.sims_fee_period10_paid == null)
                        {
                            sims_10 = fp10;
                        }

                        if (simobj.sims_fee_period11_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(11)) == false)
                                t.periods.Add(t.periodDate(11));
                            sims_11 = fp11 - tp11;
                        }
                        else if (simobj.sims_fee_period11_paid == "0" || simobj.sims_fee_period11_paid == null)
                        {
                            sims_11 = fp11;
                        }

                        if (simobj.sims_fee_period12_paid == "1")
                        {
                            if (t.periods.Contains(t.periodDate(12)) == false)
                                t.periods.Add(t.periodDate(12));
                            sims_12 = fp12 - tp12;
                        }
                        else if (simobj.sims_fee_period12_paid == "0" || simobj.sims_fee_period12_paid == null)
                        {
                            sims_12 = fp12;
                        }

                        #endregion

                        t.periods.Sort();
                        DateTime tempSDate = s_date;
                        foreach (var item in t.periods)
                        {
                            int b = tempSDate.CompareTo(item);
                            Sims525 tobj = new Sims525();
                            if (b < 0)
                            {
                                tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                                tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                                tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                                tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                                tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                                tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                                tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                                if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                    tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                                else
                                    tobj.sims_transport_pickup_stop_code = null;

                                if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                    tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                                else
                                    tobj.sims_transport_drop_stop_code = null;

                                tobj.V_Code = simobj.V_Name_Code;
                                tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                                tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                                tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;
                                t.AYStartDate = tempSDate;
                                t.AYEndDate = item.AddDays(-1);
                                tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                                tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;

                                tempSDate = item.AddMonths(1);
                            }
                            else
                            {
                                tempSDate = item.AddMonths(1);
                            }

                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db1 = new DBConnection())
                            {
                                db1.Open();
                                SqlDataReader dr1 = db1.ExecuteStoreProcedure("sims.sims_transport_update_proc",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from)),
                                    new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto)),

                                    new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from_new)),
                                    new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto_new)),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number)

                                });

                                if (dr1.Read())
                                {
                                    st = dr1[0].ToString();
                                    msg.strMessage = st;
                                }
                                if (dr1.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr1.Close();

                            }

                            #endregion
                        }

                        int l = tempSDate.CompareTo(e_date);
                        if (l < 0)
                        {
                            Sims525 tobj = new Sims525();
                            tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                            tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                            tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                            tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                            tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                            tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                            tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                            else
                                tobj.sims_transport_pickup_stop_code = null;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                            else
                                tobj.sims_transport_drop_stop_code = null;

                            tobj.V_Code = simobj.V_Name_Code;
                            tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                            tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                            tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;

                            t.AYStartDate = tempSDate;
                            t.AYEndDate = e_date;
                            tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                            tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;

                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_transport_update_proc",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from)),
                                    new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto)),

                                    new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from_new)),
                                    new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto_new)),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number)

                                });

                                if (dr2.Read())
                                {
                                    st = dr2[0].ToString();
                                    msg.strMessage = st;

                                }
                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr2.Close();

                            }

                            #endregion
                        }

                        else
                        {
                            Sims525 tobj = new Sims525();
                            tobj.sims_transport_enroll_number = simobj.sims_transport_enroll_number;
                            tobj.sims_fee_cur_code = simobj.sims_fee_cur_code;
                            tobj.sims_transport_academic_year = simobj.sims_transport_academic_year;
                            tobj.sims_fee_grade_code = simobj.sims_fee_grade_code;
                            tobj.sims_fee_section_code = simobj.sims_fee_section_code;
                            tobj.sims_transport_route_code = simobj.sims_transport_route_code;
                            tobj.sims_transport_route_direction = simobj.sims_transport_route_direction;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_pickup_stop_code))
                                tobj.sims_transport_pickup_stop_code = simobj.sims_transport_pickup_stop_code;
                            else
                                tobj.sims_transport_pickup_stop_code = null;

                            if (!string.IsNullOrEmpty(simobj.sims_transport_drop_stop_code))
                                tobj.sims_transport_drop_stop_code = simobj.sims_transport_drop_stop_code;
                            else
                                tobj.sims_transport_drop_stop_code = null;

                            tobj.V_Code = simobj.V_Name_Code;

                            tobj.sims_transport_effective_from = s_date.Year + "-" + s_date.Month + "-" + s_date.Day;
                            tobj.sims_transport_effective_upto = e_date.Year + "-" + e_date.Month + "-" + e_date.Day;
                            tobj.sims_transport_route_student_code = simobj.sims_transport_route_student_code;

                            t.AYStartDate = DateTime.Parse(tobj.sims_transport_effective_from);
                            t.AYEndDate = e_date;
                            tobj.sims_transport_effective_from_new = t.AYStartDate.Year + "-" + t.AYStartDate.Month + "-" + t.AYStartDate.Day;
                            tobj.sims_transport_effective_upto_new = t.AYEndDate.Year + "-" + t.AYEndDate.Month + "-" + t.AYEndDate.Day;

                            //send to db
                            #region Code For Send Data To Database...

                            using (DBConnection db2 = new DBConnection())
                            {
                                db2.Open();
                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("sims.sims_transport_update_proc",
                                new List<SqlParameter>()
                                {

                                    new SqlParameter("@opr", 'U'),
                                    new SqlParameter("@sub_opr", 'P'),
                                    new SqlParameter("@sims_transport_academic_year",tobj.sims_transport_academic_year),
                                    new SqlParameter("@sims_transport_enroll_number",tobj.sims_transport_enroll_number),
                                    new SqlParameter("@sims_cur_code",tobj.sims_fee_cur_code),
                                    new SqlParameter("@sims_grade_code",tobj.sims_fee_grade_code),
                                    new SqlParameter("@sims_section_code",tobj.sims_fee_section_code),
                                    new SqlParameter("@sims_transport_route_code",tobj.sims_transport_route_code),
                                    new SqlParameter("@sims_transport_route_direction",tobj.sims_transport_route_direction),
                                    new SqlParameter("@sims_transport_pickup_stop_code",tobj.sims_transport_pickup_stop_code),
                                    new SqlParameter("@sims_transport_drop_stop_code",tobj.sims_transport_drop_stop_code),
                                    new SqlParameter("@sims_transport_route_student_code",tobj.sims_transport_route_student_code),
                                    new SqlParameter("@sims_transport_vehicle_code",tobj.V_Name_Code),
                                    new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from)),
                                    new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto)),

                                    new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_from_new)),
                                    new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(tobj.sims_transport_effective_upto_new)),
                                    new SqlParameter("@sims_1",sims_1),
                                    new SqlParameter("@sims_2",sims_2),
                                    new SqlParameter("@sims_3",sims_3),
                                    new SqlParameter("@sims_4",sims_4),
                                    new SqlParameter("@sims_5",sims_5),
                                    new SqlParameter("@sims_6",sims_6),
                                    new SqlParameter("@sims_7",sims_7),
                                    new SqlParameter("@sims_8",sims_8),
                                    new SqlParameter("@sims_9",sims_9),
                                    new SqlParameter("@sims_10",sims_10),
                                    new SqlParameter("@sims_11",sims_11),
                                    new SqlParameter("@sims_12",sims_12),
                                    new SqlParameter("@sims_transport_fee_number_old",tobj.sims_fee_number)

                                });

                                if (dr2.Read())
                                {
                                    st = dr2[0].ToString();
                                    msg.strMessage = st;

                                }
                                if (dr2.RecordsAffected > 0)
                                {
                                    inserted = true;
                                }

                                dr2.Close();

                            }

                            #endregion
                        }

                    }
                }
            }

            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        class temp1
        {
            public DateTime AYStartDate { get; set; }
            public DateTime AYEndDate { get; set; }

            public List<DateTime> periods = new List<DateTime>();
            public DateTime periodDate(int period)
            {
                DateTime dt2 = AYEndDate;
                DateTime dt1 = AYStartDate;
                int d = dt1.Day;
                int y = period >= dt1.Month ? dt1.Year : dt2.Year;
                DateTime mydt = new DateTime(y, period, d);
                return mydt;
            }

        }

        #endregion

        [Route("EmployeeCancelTransportCUD")]
        public HttpResponseMessage EmployeeCancelTransportCUD(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims525 simobj in data)
                    {

                        if (simobj.sims_appl_parameter == "undefined" || simobj.sims_appl_parameter == "null" || simobj.sims_appl_parameter == "")
                            simobj.sims_appl_parameter = null;
                        if (simobj.sims_transport_pickup_stop_code_old == "undefined" || simobj.sims_transport_pickup_stop_code_old == "null" || simobj.sims_transport_pickup_stop_code_old == "")
                            simobj.sims_transport_pickup_stop_code_old = null;

                        string fDateo = simobj.sims_transport_effective_from_old.ToString();
                        DateTime fpDateo = DateTime.Parse(fDateo);
                        string uDateo = simobj.sims_transport_effective_upto_old.ToString();
                        DateTime upDateo = DateTime.Parse(uDateo);

                        //if(simobj.sub_opr=="D")
                        //{

                        //    simobj.sims_transport_effective_from=fpDateo.ToString();
                        //    string nDateu = simobj.sims_transport_effective_upto.ToString();
                        //    DateTime nDateue = DateTime.Parse(nDateu);
                        //    //simobj.sims_transport_effective_upto
                        //}

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_update_employee_proc]",

                            new List<SqlParameter>()
                      {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sub_opr",simobj.sub_opr),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_em_number",simobj.sims_transport_em_number),

                          new SqlParameter("@sims_transport_route_employee_code",simobj.sims_transport_route_employee_code),
                          //new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),
                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),

                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),

                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),

                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),

                          new SqlParameter("@Allocated_Seat_old",simobj.sims_transport_seat_number),
                          
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),

                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("EmployeeUpdateTransportCUD")]
        public HttpResponseMessage EmployeeUpdateTransportCUD(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims525 simobj in data)
                    {

                        if (simobj.sims_appl_parameter == "undefined" || simobj.sims_appl_parameter == "null" || simobj.sims_appl_parameter == "")
                            simobj.sims_appl_parameter = null;
                        if (simobj.sims_transport_pickup_stop_code_old == "undefined" || simobj.sims_transport_pickup_stop_code_old == "null" || simobj.sims_transport_pickup_stop_code_old == "")
                            simobj.sims_transport_pickup_stop_code_old = null;

                        if (simobj.sims_transport_pickup_stop_code == "undefined" || simobj.sims_transport_pickup_stop_code == "null" || simobj.sims_transport_pickup_stop_code == "")
                            simobj.sims_transport_pickup_stop_code = null;

                        if (simobj.sims_transport_drop_stop_code_old == "undefined" || simobj.sims_transport_drop_stop_code_old == "null" || simobj.sims_transport_drop_stop_code_old == "")
                            simobj.sims_transport_drop_stop_code_old = null;

                        if (simobj.sims_transport_drop_stop_code == "undefined" || simobj.sims_transport_drop_stop_code == "null" || simobj.sims_transport_drop_stop_code == "")
                            simobj.sims_transport_drop_stop_code = null;


                        if (simobj.sims_transport_route_employee_stop_lat == "undefined" || simobj.sims_transport_route_employee_stop_lat == "null" || simobj.sims_transport_route_employee_stop_lat == "")
                            simobj.sims_transport_route_employee_stop_lat = null;

                        if (simobj.sims_transport_route_employee_stop_long == "undefined" || simobj.sims_transport_route_employee_stop_long == "null" || simobj.sims_transport_route_employee_stop_long == "")
                            simobj.sims_transport_route_employee_stop_long = null;

                        string fDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                        DateTime fpDateo = DateTime.Parse(fDateo);
                        string uDateo = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old.ToString());
                        DateTime upDateo = DateTime.Parse(uDateo);

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_update_employee_proc]",

                            new List<SqlParameter>()
                      {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sub_opr",simobj.sub_opr),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_em_number",simobj.sims_transport_em_number),

                          new SqlParameter("@sims_transport_route_employee_code",simobj.sims_transport_route_employee_code),
                          //new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),
                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),

                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),

                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),

                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),

                          new SqlParameter("@Allocated_Seat_old",simobj.sims_transport_seat_number),

                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),

                          new SqlParameter("@sims_transport_route_employee_stop_lat",simobj.sims_transport_route_employee_stop_lat),
                          new SqlParameter("@sims_transport_route_employee_stop_long",simobj.sims_transport_route_employee_stop_long),
                          
                          

                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion


        #region API FOR STUDENT UPDATE/CANCEL BY USING GRADE & SECTION OR ENROLL NUMBER FOR DPSMIS SCHOOL...

        [Route("getStudentTransportDetailsDpsmis")]
        public HttpResponseMessage getStudentTransportDetailsDpsmis(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code)
        {
            double sims_1f = 0, sims_2f = 0, sims_3f = 0, sims_4f = 0, sims_5f = 0, sims_6f = 0, sims_7f = 0, sims_8f = 0, sims_9f = 0, sims_10f = 0, sims_11f = 0, sims_12f = 0;
            double Paid = 0;
            double Unpaid = 0;
            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_cancel_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();

                            obj.sims_student_name = dr["sims_student_name"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.sims_fee_number = dr["sims_fee_number"].ToString();
                            obj.sims_fee_code = dr["sims_fee_code"].ToString();
                            obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                            obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                            obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                            obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                            obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                            obj.V_Name = dr["V_Name"].ToString();
                            obj.V_Code = dr["V_Code"].ToString();
                            obj.V_Name_Code = dr["V_Name_Code"].ToString();

                            obj.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                            obj.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                            obj.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                            obj.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                            obj.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                            obj.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                            obj.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                            obj.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                            obj.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                            obj.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                            obj.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                            obj.sims_fee_period12 = dr["sims_fee_period12"].ToString();


                            obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                            obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                            obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                            obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                            obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                            obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                            obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                            obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                            obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                            obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                            obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                            obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                            obj.sims_fee_period1_paid = dr["sims_fee_period1_paid"].ToString();
                            obj.sims_fee_period2_paid = dr["sims_fee_period2_paid"].ToString();
                            obj.sims_fee_period3_paid = dr["sims_fee_period3_paid"].ToString();
                            obj.sims_fee_period4_paid = dr["sims_fee_period4_paid"].ToString();
                            obj.sims_fee_period5_paid = dr["sims_fee_period5_paid"].ToString();
                            obj.sims_fee_period6_paid = dr["sims_fee_period6_paid"].ToString();
                            obj.sims_fee_period7_paid = dr["sims_fee_period7_paid"].ToString();
                            obj.sims_fee_period8_paid = dr["sims_fee_period8_paid"].ToString();
                            obj.sims_fee_period9_paid = dr["sims_fee_period9_paid"].ToString();
                            obj.sims_fee_period10_paid = dr["sims_fee_period10_paid"].ToString();
                            obj.sims_fee_period11_paid = dr["sims_fee_period11_paid"].ToString();
                            obj.sims_fee_period12_paid = dr["sims_fee_period12_paid"].ToString();

                            sims_1 = double.Parse(dr["sims_fee_period1_paid"].ToString());
                            sims_2 = double.Parse(dr["sims_fee_period2_paid"].ToString());
                            sims_3 = double.Parse(dr["sims_fee_period3_paid"].ToString());
                            sims_4 = double.Parse(dr["sims_fee_period4_paid"].ToString());
                            sims_5 = double.Parse(dr["sims_fee_period5_paid"].ToString());
                            sims_6 = double.Parse(dr["sims_fee_period6_paid"].ToString());
                            sims_7 = double.Parse(dr["sims_fee_period7_paid"].ToString());
                            sims_8 = double.Parse(dr["sims_fee_period8_paid"].ToString());
                            sims_9 = double.Parse(dr["sims_fee_period9_paid"].ToString());
                            sims_10 = double.Parse(dr["sims_fee_period10_paid"].ToString());
                            sims_11 = double.Parse(dr["sims_fee_period11_paid"].ToString());
                            sims_12 = double.Parse(dr["sims_fee_period12_paid"].ToString());

                            sims_1f = double.Parse(dr["sims_fee_period1"].ToString());
                            sims_2f = double.Parse(dr["sims_fee_period2"].ToString());
                            sims_3f = double.Parse(dr["sims_fee_period3"].ToString());
                            sims_4f = double.Parse(dr["sims_fee_period4"].ToString());
                            sims_5f = double.Parse(dr["sims_fee_period5"].ToString());
                            sims_6f = double.Parse(dr["sims_fee_period6"].ToString());
                            sims_7f = double.Parse(dr["sims_fee_period7"].ToString());
                            sims_8f = double.Parse(dr["sims_fee_period8"].ToString());
                            sims_9f = double.Parse(dr["sims_fee_period9"].ToString());
                            sims_10f = double.Parse(dr["sims_fee_period10"].ToString());
                            sims_11f = double.Parse(dr["sims_fee_period11"].ToString());
                            sims_12f = double.Parse(dr["sims_fee_period12"].ToString());

                            Paid = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                            obj.sims_paid_total = Paid;
                            Unpaid = sims_1f + sims_2f + sims_3f + sims_4f + sims_5f + sims_6f + sims_7f + sims_8f + sims_9f + sims_10f + sims_11f + sims_12f;
                            obj.sims_pending_amount = Unpaid - Paid;
                            obj.sims_total_amount = Unpaid;


                            StudentTrans_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        #endregion


        #region API FOR STUDENT UPDATE/CANCEL OR APPLY NEW TRANSPORT BY USING GRADE & SECTION OR ENROLL NUMBER ...

        [Route("getSecondBusCode")]
        public HttpResponseMessage getSecondBusCode(string V_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSecondBusCode(),PARAMETERS :: NO)";

            List<Sims525> bus_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "A"),
                             new SqlParameter("@sims_transport_vehicle_code", V_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_vehicle_code_2 = dr["sims_transport_vehicle_code"].ToString();
                            simsobj.sims_transport_vehicle_name_2 = dr["sims_transport_vehicle_name_plate"].ToString();
                            simsobj.sims_transport_vehicle_seating_capacity_2 = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            bus_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_code);
            }
        }

        [Route("getSecondBusCode_AHGS")]
        public HttpResponseMessage getSecondBusCode_AHGS(string V_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSecondBusCode(),PARAMETERS :: NO)";

            List<Sims525> bus_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel_AHGS",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "A"),
                             new SqlParameter("@sims_transport_vehicle_code", V_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_vehicle_code_2 = dr["sims_transport_vehicle_code"].ToString();
                            simsobj.sims_transport_vehicle_name_2 = dr["sims_transport_vehicle_name_plate"].ToString();
                            simsobj.sims_transport_vehicle_seating_capacity_2 = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            simsobj.sims_transport_vehicle_safe_travel_code = dr["sims_transport_vehicle_safe_travel_code"].ToString();
                            bus_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_code);
            }
        }

        [Route("getSecondRouteName")]
        public HttpResponseMessage getSecondRouteName(string AcaYear, string Vehicle_Code, string r_code, string d_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRouteNameByBusCode(),PARAMETERS :: NO)";

            List<Sims525> route_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "B"),
                             new SqlParameter("@sims_transport_academic_year", AcaYear),
                             new SqlParameter("@sims_transport_vehicle_code", Vehicle_Code),
                             new SqlParameter("@sims_transport_route_code", r_code),
                             new SqlParameter("@sims_transport_route_direction", d_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_route_name_2 = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_code_2 = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_name"].ToString();

                            route_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, route_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, route_code);
            }
        }

        [Route("getStopByRoutedirectioncode")]
        public HttpResponseMessage getStopByRoutedirectioncode(string AcaYear, string Vehicle_Code, string r_code, string d_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getRouteNameByBusCode(),PARAMETERS :: NO)";


            List<Sims525> route_code = new List<Sims525>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "C"),
                             new SqlParameter("@sims_transport_academic_year", AcaYear),
                             new SqlParameter("@sims_transport_vehicle_code_new", Vehicle_Code),
                             new SqlParameter("@sims_transport_route_code_new", r_code),
                             new SqlParameter("@sims_transport_route_direction_new", d_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_stop_code_2 = dr["sims_transport_route_stop_code"].ToString();
                            simsobj.sims_transport_stop_name_2 = dr["sims_transport_stop_name"].ToString();
                            route_code.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, route_code);
            }
            catch (Exception e)
            {
                //  Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, route_code);
            }
        }

        [Route("getFeesnewapply")]
        public HttpResponseMessage getFeesnewapply(string aca_year, string route_code, string route_code_new, string route_direct, string route_direct_new, string sims_transport_effective_from, string sims_transport_effective_upto)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStopByRoute(),PARAMETERS :: NO";


            List<Sims525> fee_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_new_apply_update_cancel",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "E"),
                             new SqlParameter("@sims_transport_academic_year", aca_year),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_route_direction", route_direct),
                             new SqlParameter("@sims_transport_route_code_new", route_code_new),
                             new SqlParameter("@sims_transport_route_direction_new", route_direct_new),
                             new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(sims_transport_effective_from)),
                             new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(sims_transport_effective_upto))

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                            simsobj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                            simsobj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                            simsobj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                            simsobj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                            simsobj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                            simsobj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                            simsobj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                            simsobj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                            simsobj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                            simsobj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                            simsobj.sims_transport_period12 = dr["sims_transport_period12"].ToString();
                            simsobj.sims_transport_toatal = dr["sims_transport_toatal"].ToString();
                            fee_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, fee_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, fee_list);
            }
        }

        [Route("ApplyTransportCUD")]
        public HttpResponseMessage ApplyTransportCUD(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims525 simobj in data)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_new_apply_update_cancel]",
                            new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simobj.opr),
                            new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                            new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),
                            new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                            new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                            new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),
                            new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                            new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),
                            new SqlParameter("@sims_transport_fee_number",simobj.sims_fee_number),

                            new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code_old),
                            new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction_old),
                            new SqlParameter("@sims_transport_route_code_new",simobj.sims_transport_route_code),
                            new SqlParameter("@sims_transport_route_direction_new",simobj.sims_transport_route_direction),

                            new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                            new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                            new SqlParameter("@sims_transport_effective_from_new",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                            new SqlParameter("@sims_transport_effective_upto_new",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),

                            new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code_old),
                            new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code_old),
                            new SqlParameter("@sims_transport_pickup_stop_code_new", simobj.sims_transport_pickup_stop_code),
                            new SqlParameter("@sims_transport_drop_stop_code_new",simobj.sims_transport_drop_stop_code),
                
                            new SqlParameter("@sims_transport_vehicle_code",simobj.V_Code),
                            new SqlParameter("@sims_transport_vehicle_code_new",simobj.V_Name_Code),

                            new SqlParameter("@sims_1",simobj.sims_fee_period1),
                            new SqlParameter("@sims_2",simobj.sims_fee_period2),
                            new SqlParameter("@sims_3",simobj.sims_fee_period3),
                            new SqlParameter("@sims_4",simobj.sims_fee_period4),
                            new SqlParameter("@sims_5",simobj.sims_fee_period5),
                            new SqlParameter("@sims_6",simobj.sims_fee_period6),
                            new SqlParameter("@sims_7",simobj.sims_fee_period7),
                            new SqlParameter("@sims_8",simobj.sims_fee_period8),
                            new SqlParameter("@sims_9",simobj.sims_fee_period9),
                            new SqlParameter("@sims_10",simobj.sims_fee_period10),
                            new SqlParameter("@sims_11",simobj.sims_fee_period11),
                            new SqlParameter("@sims_12",simobj.sims_fee_period12)

                        });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        #endregion


        #region API FOR STUDENT UPDATE/CANCEL BY USING BUS CODE OR ENROLL NUMBER...
        //-----------------------------------FOR_BUSWise_Transfer_Transport----------------------------------//

        [Route("getAllrotenameforBuswiseTransfer")]
        public HttpResponseMessage getAllrotenameforBuswiseTransfer(string academic_yr, string route_code, string direction_aca_route)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllrotenameforBuswiseTransfer(),PARAMETERS :: NO";

            List<Sims085> bus_wise_route_list = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_bus_trasfer_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@sims_transport_academic_year", academic_yr),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_route_direction", direction_aca_route)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();

                            bus_wise_route_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, bus_wise_route_list);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, bus_wise_route_list);
            }
        }


        [Route("getBUSWiseStudentTransDetails")]
        public HttpResponseMessage getBUSWiseStudentTransDetails(string academic_yr, string route_code, string direction_aca_route)
        {
            double sims_1f = 0, sims_2f = 0, sims_3f = 0, sims_4f = 0, sims_5f = 0, sims_6f = 0, sims_7f = 0, sims_8f = 0, sims_9f = 0, sims_10f = 0, sims_11f = 0, sims_12f = 0;
            double Paid = 0;
            double Unpaid = 0;
            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;


            List<Sims525> EmployeeTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_student_bus_trasfer_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "P"),
                            new SqlParameter("@sims_transport_academic_year",academic_yr),
                            new SqlParameter("@sims_transport_route_code", route_code),
                            new SqlParameter("@sims_transport_route_direction", direction_aca_route)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();

                            obj.sims_student_name = dr["sims_student_name"].ToString();
                            obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                            obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                            obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                            obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                            obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                            obj.sims_fee_number = dr["sims_fee_number"].ToString();
                            obj.sims_fee_code = dr["sims_fee_code"].ToString();
                            obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                            obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                            obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                            obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                            obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                            obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                            obj.V_Name = dr["V_Name"].ToString();
                            obj.V_Code = dr["V_Code"].ToString();
                            obj.V_Name_Code = dr["V_Name_Code"].ToString();

                            obj.sims_fee_period1 = dr["sims_fee_period1"].ToString();
                            obj.sims_fee_period2 = dr["sims_fee_period2"].ToString();
                            obj.sims_fee_period3 = dr["sims_fee_period3"].ToString();
                            obj.sims_fee_period4 = dr["sims_fee_period4"].ToString();
                            obj.sims_fee_period5 = dr["sims_fee_period5"].ToString();
                            obj.sims_fee_period6 = dr["sims_fee_period6"].ToString();
                            obj.sims_fee_period7 = dr["sims_fee_period7"].ToString();
                            obj.sims_fee_period8 = dr["sims_fee_period8"].ToString();
                            obj.sims_fee_period9 = dr["sims_fee_period9"].ToString();
                            obj.sims_fee_period10 = dr["sims_fee_period10"].ToString();
                            obj.sims_fee_period11 = dr["sims_fee_period11"].ToString();
                            obj.sims_fee_period12 = dr["sims_fee_period12"].ToString();

                            obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                            obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                            obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                            obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                            obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                            obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                            obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                            obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                            obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                            obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                            obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                            obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                            obj.sims_fee_period1_paid = dr["sims_fee_period1_paid"].ToString();
                            obj.sims_fee_period2_paid = dr["sims_fee_period2_paid"].ToString();
                            obj.sims_fee_period3_paid = dr["sims_fee_period3_paid"].ToString();
                            obj.sims_fee_period4_paid = dr["sims_fee_period4_paid"].ToString();
                            obj.sims_fee_period5_paid = dr["sims_fee_period5_paid"].ToString();
                            obj.sims_fee_period6_paid = dr["sims_fee_period6_paid"].ToString();
                            obj.sims_fee_period7_paid = dr["sims_fee_period7_paid"].ToString();
                            obj.sims_fee_period8_paid = dr["sims_fee_period8_paid"].ToString();
                            obj.sims_fee_period9_paid = dr["sims_fee_period9_paid"].ToString();
                            obj.sims_fee_period10_paid = dr["sims_fee_period10_paid"].ToString();
                            obj.sims_fee_period11_paid = dr["sims_fee_period11_paid"].ToString();
                            obj.sims_fee_period12_paid = dr["sims_fee_period12_paid"].ToString();

                            sims_1 = double.Parse(dr["sims_fee_period1_paid"].ToString());
                            sims_2 = double.Parse(dr["sims_fee_period2_paid"].ToString());
                            sims_3 = double.Parse(dr["sims_fee_period3_paid"].ToString());
                            sims_4 = double.Parse(dr["sims_fee_period4_paid"].ToString());
                            sims_5 = double.Parse(dr["sims_fee_period5_paid"].ToString());
                            sims_6 = double.Parse(dr["sims_fee_period6_paid"].ToString());
                            sims_7 = double.Parse(dr["sims_fee_period7_paid"].ToString());
                            sims_8 = double.Parse(dr["sims_fee_period8_paid"].ToString());
                            sims_9 = double.Parse(dr["sims_fee_period9_paid"].ToString());
                            sims_10 = double.Parse(dr["sims_fee_period10_paid"].ToString());
                            sims_11 = double.Parse(dr["sims_fee_period11_paid"].ToString());
                            sims_12 = double.Parse(dr["sims_fee_period12_paid"].ToString());

                            sims_1f = double.Parse(dr["sims_fee_period1"].ToString());
                            sims_2f = double.Parse(dr["sims_fee_period2"].ToString());
                            sims_3f = double.Parse(dr["sims_fee_period3"].ToString());
                            sims_4f = double.Parse(dr["sims_fee_period4"].ToString());
                            sims_5f = double.Parse(dr["sims_fee_period5"].ToString());
                            sims_6f = double.Parse(dr["sims_fee_period6"].ToString());
                            sims_7f = double.Parse(dr["sims_fee_period7"].ToString());
                            sims_8f = double.Parse(dr["sims_fee_period8"].ToString());
                            sims_9f = double.Parse(dr["sims_fee_period9"].ToString());
                            sims_10f = double.Parse(dr["sims_fee_period10"].ToString());
                            sims_11f = double.Parse(dr["sims_fee_period11"].ToString());
                            sims_12f = double.Parse(dr["sims_fee_period12"].ToString());

                            Paid = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                            obj.sims_paid_total = Paid;
                            Unpaid = sims_1f + sims_2f + sims_3f + sims_4f + sims_5f + sims_6f + sims_7f + sims_8f + sims_9f + sims_10f + sims_11f + sims_12f;
                            obj.sims_pending_amount = Unpaid - Paid;
                            obj.sims_total_amount = Unpaid;

                            EmployeeTrans_list.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, EmployeeTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, EmployeeTrans_list);
        }

        [Route("getStopByRouteBusWise")]
        public HttpResponseMessage getStopByRouteBusWise(string academic_yr, string route_code, string direction)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getStopByRouteBusWise(),PARAMETERS :: NO";


            List<Sims525> Stop_list_busWise = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_bus_trasfer_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "G"),
                             new SqlParameter("@sims_transport_academic_year", academic_yr),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_route_direction", direction)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 simsobj = new Sims525();
                            simsobj.sims_transport_route_stop_code = dr["sims_transport_route_stop_code"].ToString();
                            simsobj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            Stop_list_busWise.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Stop_list_busWise);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, Stop_list_busWise);
            }
        }

        [Route("CUDTransportStudentBusWise")]
        public HttpResponseMessage CUDTransportStudentBusWise(List<Sims081> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            //string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims081 routeobj in data)
                    {
                        string sims_1 = null, sims_2 = null, sims_3 = null, sims_4 = null, sims_5 = null, sims_6 = null, sims_7 = null, sims_8 = null, sims_9 = null, sims_10 = null, sims_11 = null, sims_12 = null;


                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_bus_trasfer_proc]",
                            new List<SqlParameter>()
                        {

                          new SqlParameter("@opr",routeobj.opr),
                          new SqlParameter("@sub_opr",routeobj.sub_opr),
                          
                          new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),

                          new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                          new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_code_old", routeobj.sims_transport_route_code_old),

                          new SqlParameter("@sims_transport_route_direction",routeobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_route_direction_old",routeobj.sims_transport_route_direction_old),
                          
                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_effective_upto_old", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto_old)),

                          new SqlParameter("@sims_transport_pickup_stop_code",routeobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old",routeobj.sims_transport_pickup_stop_code),

                          new SqlParameter("@sims_transport_stop_lat_pickup", routeobj.sims_transport_route_student_stop_lat),
                          new SqlParameter("@sims_transport_stop_long_pickup",routeobj.sims_transport_route_student_stop_long),

                          new SqlParameter("@sims_transport_drop_stop_code",routeobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_stop_lat_drop", routeobj.sims_transport_stop_lat_drop),
                          new SqlParameter("@sims_transport_stop_long_drop",routeobj.sims_transport_stop_long_drop),

                    });

                        int ins = dr.RecordsAffected;
                        Sims081_Msg st = new Sims081_Msg();
                        if (dr.RecordsAffected > 0)
                        {
                            if (dr.Read())
                            {
                                st.sims_enroll = dr["Enroll"].ToString();
                                ins = int.Parse(dr["status"].ToString());
                                st.sims_msg = dr["sims_msg"].ToString();
                                st.sims_status = ins > 0 ? "Success" : "Fail";
                                msg.strMessage = st.sims_msg;
                            }
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
        //-----------------------------------FOR_BUSWise_Transfer_Transport----------------------------------//

        #endregion
    }
}