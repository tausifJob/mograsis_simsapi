﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/TransportCaretaker")]
    public class TransportCaretakerController:ApiController
    {
        [Route("getTransportCaretaker")]
        public HttpResponseMessage getTransportCaretaker()
        {
            List<Sims087> TransportCaretaker = new List<Sims087>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_caretaker_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 obj = new Sims087();

                            obj.sims_caretaker_code = dr["sims_caretaker_code"].ToString();
                            obj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();
                            obj.sims_employee_code = dr["sims_employee_code"].ToString();
                            obj.sims_employee_code_name = dr["sims_employee_code_name"].ToString();
                            obj.sims_user_code = dr["sims_user_code"].ToString();
                            obj.sims_caretaker_img = dr["sims_caretaker_img"].ToString();
                            obj.sims_experience_years = dr["sims_experience_years"].ToString();
                            obj.sims_caretaker_status = dr["sims_caretaker_status"].ToString().Equals("A") ? true : false;
                            obj.sims_caretaker_email = dr["sims_caretaker_email"].ToString();
                            obj.comn_user_phone_number = dr["comn_user_phone_number"].ToString();
                            TransportCaretaker.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
        }

        [Route("getTransportUserCode")]
        public HttpResponseMessage getTransportUserCode()
        {
            List<Sims087> TransportCaretaker = new List<Sims087>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_caretaker_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "W")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 obj = new Sims087();
                            obj.sims_user_code = dr["sims_user_code"].ToString();                          
                            TransportCaretaker.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
        }

        [Route("CUDTransportCareTaker")]
        public HttpResponseMessage CUDTransportCareTaker(List<Sims087> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportCareTaker", simsobj));
            string strMessage = "";
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims087 simsobj in data)
                    {

                        if (simsobj.sims_caretaker_name == "")
                        {
                            simsobj.sims_caretaker_name = null;
                        }
                        if (simsobj.sims_employee_code == "")
                        {
                            simsobj.sims_employee_code = null;
                        }
                        if (simsobj.sims_user_code == "")
                        {
                            simsobj.sims_user_code = null;
                        }
                        if (simsobj.sims_caretaker_img == "")
                        {
                            simsobj.sims_caretaker_img = null;
                        }
                        if (simsobj.sims_experience_years == "")
                        {
                            simsobj.sims_experience_years = null;
                        }
                        if (simsobj.sims_caretaker_email == "")
                        {
                            simsobj.sims_caretaker_email = null;
                        }
                        if (simsobj.comn_user_phone_number == "")
                        {
                            simsobj.comn_user_phone_number = null;
                        }
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_caretaker_proc]",
                       new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_caretaker_code",simsobj.sims_caretaker_code),
                          new SqlParameter("@sims_caretaker_name", simsobj.sims_caretaker_name),
                          new SqlParameter("@sims_employee_code",simsobj.sims_employee_code),
                          new SqlParameter("@sims_user_code", simsobj.sims_user_code),
                          new SqlParameter("@sims_caretaker_img",simsobj.sims_caretaker_img),
                          new SqlParameter("@sims_experience_years", simsobj.sims_experience_years),
                          new SqlParameter("@sims_caretaker_status",simsobj.sims_caretaker_status==true?"A":"I"),
                          new SqlParameter("@comn_user_email", simsobj.sims_caretaker_email),
                          new SqlParameter("@comn_user_phone_number", simsobj.comn_user_phone_number),

                        });

                        if (dr.Read())
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        else
                        {
                            strMessage = "Record Not Insert....";
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                
                return Request.CreateResponse(HttpStatusCode.OK, strMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strMessage);
        }

        [Route("getAllEmpName")]
        public HttpResponseMessage getAllEmpName(string desg_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllEmpName(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "FLEET", "getAllEmpDriver"));

            List<Sims087> emp_caretaker = new List<Sims087>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_caretaker_proc",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "C"),
                             new SqlParameter("@dg_code", desg_code)

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            simsobj.sims_employee_code_name = dr["sims_employee_name"].ToString();
                            simsobj.emp_full_name = dr["emp_full_name"].ToString();
                            simsobj.sims_employee_code = dr["em_login_code"].ToString();
                            emp_caretaker.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_caretaker);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_caretaker);
            }
        }

        [Route("TransportCaretakerDelete")]
        public HttpResponseMessage TransportCaretakerDelete(List<Sims087> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
           

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims087 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_caretaker_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@sims_caretaker_code",simsobj.sims_caretaker_code),
                                new SqlParameter("@sims_caretaker_name", simsobj.sims_caretaker_name),
                                new SqlParameter("@sims_employee_code",simsobj.sims_employee_code),
                                new SqlParameter("@sims_user_code", simsobj.sims_user_code),
                                new SqlParameter("@sims_caretaker_img",simsobj.sims_caretaker_img),
                                new SqlParameter("@sims_experience_years", simsobj.sims_experience_years),
                                new SqlParameter("@sims_caretaker_status",simsobj.sims_caretaker_status==true?"A":"I"),
                                new SqlParameter("@comn_user_email", simsobj.sims_caretaker_email),
                                new SqlParameter("@comn_user_phone_number", simsobj.comn_user_phone_number),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateTransportCareTaker")]
        public HttpResponseMessage UpdateTransportCareTaker(List<Sims087> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims087 simsobj in data)
                    {
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_caretaker_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@sims_caretaker_code",simsobj.sims_caretaker_code),
                                new SqlParameter("@sims_caretaker_name", simsobj.sims_caretaker_name),
                                new SqlParameter("@sims_employee_code",simsobj.sims_employee_code),
                                new SqlParameter("@sims_user_code", simsobj.sims_user_code),
                                new SqlParameter("@sims_caretaker_img",simsobj.sims_caretaker_img),
                                new SqlParameter("@sims_experience_years", simsobj.sims_experience_years),
                                new SqlParameter("@sims_caretaker_status",simsobj.sims_caretaker_status==true?"A":"I"),
                                new SqlParameter("@comn_user_email", simsobj.sims_caretaker_email),
                                new SqlParameter("@comn_user_phone_number", simsobj.comn_user_phone_number),
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    }
}