﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/TransportVehicle")]
    public class TransportVehicleController : ApiController
    {

        #region Api For Transport Vehicle...

        [Route("getTransportVehicle")]
        public HttpResponseMessage getTransportVehicle()
        {
            List<Sims086> TransportVehicle = new List<Sims086>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims086 obj = new Sims086();

                            obj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            obj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            obj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.sims_transport_vehicle_ownership = dr["sims_transport_vehicle_ownership"].ToString();
                            obj.sims_transport_vehicle_ownership_name = dr["sims_transport_vehicle_ownership_name"].ToString();
                            obj.sims_transport_vehicle_transmission = dr["sims_transport_vehicle_transmission"].ToString();
                            obj.sims_transport_vehicle_transmission_name = dr["sims_transport_vehicle_transmission_name"].ToString();
                            obj.sims_transport_manufacturer_name = dr["sims_transport_manufacturer_name"].ToString();
                            obj.sims_transport_vehicle_model_name = dr["sims_transport_vehicle_model_name"].ToString();
                            obj.sims_transport_vehicle_model_year = dr["sims_transport_vehicle_model_year"].ToString();
                            obj.sims_transport_vehicle_power = dr["sims_transport_vehicle_power"].ToString();
                            obj.sims_transport_vehicle_color = dr["sims_transport_vehicle_color"].ToString();
                            obj.sims_transport_vehicle_seating_capacity = int.Parse(dr["sims_transport_vehicle_seating_capacity"].ToString());
                            obj.sims_transport_vehicle_security_enabled = dr["sims_transport_vehicle_security_enabled"].ToString().Equals("Y") ? true : false;
                            obj.sims_transport_vehicle_camera_enabled = dr["sims_transport_vehicle_camera_enabled"].ToString().Equals("Y") ? true : false;
                            obj.sims_transport_vehicle_status = dr["sims_transport_vehicle_status"].ToString().Equals("A") ? true : false;
                            obj.sims_transport_vehicle_virtual_seating_capacity = int.Parse(dr["sims_transport_vehicle_virtual_seating_capacity"].ToString());
                            obj.sims_transport_vehicle_insurance_no = dr["sims_transport_vehicle_insurance_no"].ToString();
                            obj.sims_transport_vehicle_insurance_cmp = dr["sims_transport_vehicle_insurance_company"].ToString();
                            obj.vehicle_code_check = false;
                            obj.sims_transport_vehicle_insurance_issue_date =       db.UIDDMMYYYYformat(dr["sims_transport_vehicle_insurance_issue_date"].ToString());
                            obj.sims_transport_vehicle_insurance_expiry_date =      db.UIDDMMYYYYformat(dr["sims_transport_vehicle_insurance_expiry_date"].ToString());
                            obj.sims_transport_vehicle_date_of_purchase =           db.UIDDMMYYYYformat(dr["sims_transport_vehicle_date_of_purchase"].ToString());
                            obj.sims_transport_vehicle_registration_date =          db.UIDDMMYYYYformat(dr["sims_transport_vehicle_registration_date"].ToString());
                            obj.sims_transport_vehicle_registration_expiry_date =   db.UIDDMMYYYYformat(dr["sims_transport_vehicle_registration_expiry_date"].ToString());

                            TransportVehicle.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
        }

        [Route("getTransportApplyVehicleData")]
        public HttpResponseMessage getTransportApplyVehicleData()
        {
            List<Sims086> TransportVehicle = new List<Sims086>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "P")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims086 obj = new Sims086();
                            obj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();                      
                            TransportVehicle.Add(obj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
        }

        [Route("CUDTransportVehicle")]
        public HttpResponseMessage CUDTransportVehicle(List<Sims086> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportVehicle", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims086 simsobj in data)
                    {
                        if (simsobj.sims_transport_vehicle_code == "")
                        {
                            simsobj.sims_transport_vehicle_code = null;
                        }
                        if (simsobj.sims_transport_vehicle_registration_number == "")
                        {
                            simsobj.sims_transport_vehicle_registration_number = null;
                        }
                        if (simsobj.sims_transport_vehicle_name_plate == "")
                        {
                            simsobj.sims_transport_vehicle_name_plate = null;
                        }
                        if (simsobj.sims_transport_vehicle_ownership == "")
                        {
                            simsobj.sims_transport_vehicle_ownership = null;
                        }
                        if (simsobj.sims_transport_manufacturer_name == "")
                        {
                            simsobj.sims_transport_manufacturer_name = null;
                        }
                        if (simsobj.sims_transport_vehicle_model_name == "")
                        {
                            simsobj.sims_transport_vehicle_model_name = null;
                        }
                        if (simsobj.sims_transport_vehicle_model_year == "")
                        {
                            simsobj.sims_transport_vehicle_model_year = null;
                        }
                        if (simsobj.sims_transport_vehicle_registration_date == "")
                        {
                            simsobj.sims_transport_vehicle_registration_date = null;
                        }
                        if (simsobj.sims_transport_vehicle_registration_expiry_date == "")
                        {
                            simsobj.sims_transport_vehicle_registration_expiry_date = null;
                        }

                        if (simsobj.sims_transport_vehicle_power == "")
                        {
                            simsobj.sims_transport_vehicle_power = null;
                        }
                        if (simsobj.sims_transport_vehicle_transmission == "")
                        {
                            simsobj.sims_transport_vehicle_transmission = null;
                        }
                        if (simsobj.sims_transport_vehicle_color == "")
                        {
                            simsobj.sims_transport_vehicle_color = null;
                        }
                        if (simsobj.sims_transport_vehicle_date_of_purchase == "")
                        {
                            simsobj.sims_transport_vehicle_date_of_purchase = null;
                        }


                        if (simsobj.sims_transport_vehicle_insurance_no == "")
                        {
                            simsobj.sims_transport_vehicle_insurance_no = null;
                        }
                        if (simsobj.sims_transport_vehicle_insurance_issue_date == "")
                        {
                            simsobj.sims_transport_vehicle_insurance_issue_date = null;
                        }
                        if (simsobj.sims_transport_vehicle_insurance_expiry_date == "")
                        {
                            simsobj.sims_transport_vehicle_insurance_expiry_date = null;
                        }
                        if (simsobj.sims_transport_vehicle_insurance_cmp == "")
                        {
                            simsobj.sims_transport_vehicle_insurance_cmp = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_vehicle_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simsobj.opr),
                                new SqlParameter("@sims_transport_vehicle_code",simsobj.sims_transport_vehicle_code),
                                new SqlParameter("@sims_transport_vehicle_registration_number", simsobj.sims_transport_vehicle_registration_number),
                                new SqlParameter("@sims_transport_vehicle_name_plate",simsobj.sims_transport_vehicle_name_plate),
                                new SqlParameter("@sims_transport_vehicle_ownershiptype", simsobj.sims_transport_vehicle_ownership),
                                new SqlParameter("@sims_transport_manufacturer_name",simsobj.sims_transport_manufacturer_name),
                                new SqlParameter("@sims_transport_vehicle_model_name", simsobj.sims_transport_vehicle_model_name),
                                new SqlParameter("@sims_transport_vehicle_model_year",simsobj.sims_transport_vehicle_model_year),
                                new SqlParameter("@sims_transport_vehicle_seating_capacity", simsobj.sims_transport_vehicle_seating_capacity),
                                new SqlParameter("@sims_transport_vehicle_power",simsobj.sims_transport_vehicle_power),
                                new SqlParameter("@sims_transport_vehicle_transmissiontype", simsobj.sims_transport_vehicle_transmission),
                                new SqlParameter("@sims_transport_vehicle_color", simsobj.sims_transport_vehicle_color),
                                new SqlParameter("@sims_transport_vehicle_insurance_no", simsobj.sims_transport_vehicle_insurance_no),
                                new SqlParameter("@sims_transport_vehicle_insurance_cmp", simsobj.sims_transport_vehicle_insurance_cmp),
                                new SqlParameter("@sims_transport_vehicle_camera_enabled",simsobj.sims_transport_vehicle_camera_enabled==true?"Y":"N"),
                                new SqlParameter("@sims_transport_vehicle_security_enabled",simsobj.sims_transport_vehicle_security_enabled==true?"Y":"N"),
                                new SqlParameter("@sims_transport_vehicle_status",simsobj.sims_transport_vehicle_status==true?"A":"I"),
                                new SqlParameter("@sims_transport_vehicle_virtual_seating_capacity",simsobj.sims_transport_vehicle_virtual_seating_capacity),

                                new SqlParameter("@sims_transport_vehicle_insurance_issue_date",    db.DBYYYYMMDDformat(simsobj.sims_transport_vehicle_insurance_issue_date)),
                                new SqlParameter("@sims_transport_vehicle_insurance_expiry_date",   db.DBYYYYMMDDformat(simsobj.sims_transport_vehicle_insurance_expiry_date)),
                                new SqlParameter("@sims_transport_vehicle_registration_date",       db.DBYYYYMMDDformat(simsobj.sims_transport_vehicle_registration_date)),
                                new SqlParameter("@sims_transport_vehicle_date_of_purchase",        db.DBYYYYMMDDformat(simsobj.sims_transport_vehicle_date_of_purchase)),
                                new SqlParameter("@sims_transport_vehicle_registration_expiry_date",db.DBYYYYMMDDformat(simsobj.sims_transport_vehicle_registration_expiry_date))

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

    }
}