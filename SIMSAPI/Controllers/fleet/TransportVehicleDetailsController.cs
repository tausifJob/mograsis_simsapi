﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportVehicleDetails")]
    public class TransportVehicleDetailsController:ApiController
    {

        [Route("getTransportCaretaker")]
        public HttpResponseMessage getTransportCaretaker()
        {
            List<Sims087> TransportCaretaker = new List<Sims087>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_caretaker_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 obj = new Sims087();
                            obj.sims_caretaker_code = dr["sims_caretaker_code"].ToString();
                            obj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();
                            obj.sims_employee_code = dr["sims_employee_code"].ToString();
                           // obj.sims_caretaker_info = dr["sims_caretaker_name"].ToString() + "/" + dr["sims_employee_code"].ToString();
                            obj.sims_user_code = dr["sims_user_code"].ToString();
                            obj.sims_caretaker_img = dr["sims_caretaker_img"].ToString();
                            //obj.sims_caretaker_img_url = dr["sims_caretaker_img"].ToString());
                            obj.sims_experience_years = dr["sims_experience_years"].ToString();
                            obj.sims_caretaker_status = dr["sims_caretaker_status"].ToString().Equals("A") ? true : false;
                            TransportCaretaker.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
        }

        [Route("CUDTransportCareTaker")]
        public HttpResponseMessage CUDTransportCareTaker(Sims087 simsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportCareTaker", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_caretaker_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simsobj.opr),
                            new SqlParameter("@sims_caretaker_code",simsobj.sims_caretaker_code),
                            new SqlParameter("@sims_caretaker_name", simsobj.sims_caretaker_name),
                            new SqlParameter("@sims_employee_code",simsobj.sims_employee_code),
                            new SqlParameter("@sims_user_code", simsobj.sims_user_code),
                            new SqlParameter("@sims_caretaker_img",simsobj.sims_caretaker_img),
                            new SqlParameter("@sims_experience_years", simsobj.sims_experience_years),
                            new SqlParameter("@sims_caretaker_status",simsobj.sims_caretaker_status==true?"A":"I")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getTransportVehicle")]
        public HttpResponseMessage getTransportVehicle()
        {
            List<Sims086> TransportVehicle = new List<Sims086>();
            try
            {
                using (DBConnection db = new DBConnection())
                { 
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims086 vobj  = new Sims086();

                            vobj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            vobj.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            vobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            vobj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            //vobj.sims_transport_vehicle_transmission_name = dr["sims_transport_vehicle_transmission_name"].ToString();
                            vobj.sims_transport_vehicle_ownership = dr["sims_transport_vehicle_ownership"].ToString();
                            vobj.sims_transport_vehicle_transmission= dr["sims_transport_vehicle_transmission"].ToString();
                            vobj.sims_transport_manufacturer_name = dr["sims_transport_manufacturer_name"].ToString();
                            vobj.sims_transport_vehicle_model_name = dr["sims_transport_vehicle_model_name"].ToString();
                            vobj.sims_transport_vehicle_model_year = dr["sims_transport_vehicle_model_year"].ToString();
                            vobj.sims_transport_vehicle_power = dr["sims_transport_vehicle_power"].ToString();
                            vobj.sims_transport_vehicle_color = dr["sims_transport_vehicle_color"].ToString();
                            vobj.sims_transport_vehicle_seating_capacity = int.Parse(dr["sims_transport_vehicle_seating_capacity"].ToString());
                            vobj.sims_transport_vehicle_security_enabled = dr["sims_transport_vehicle_security_enabled"].ToString().Equals("Y") ? true : false; 
                            vobj.sims_transport_vehicle_camera_enabled = dr["sims_transport_vehicle_camera_enabled"].ToString().Equals("Y") ? true : false; 
                            vobj.sims_transport_vehicle_status = dr["sims_transport_vehicle_status"].ToString().Equals("A") ? true : false;

                            vobj.sims_transport_vehicle_date_of_purchase =          db.UIDDMMYYYYformat(dr["sims_transport_vehicle_date_of_purchase"].ToString());
                            vobj.sims_transport_vehicle_registration_date =         db.UIDDMMYYYYformat(dr["sims_transport_vehicle_registration_date"].ToString());
                            vobj.sims_transport_vehicle_registration_expiry_date =  db.UIDDMMYYYYformat(dr["sims_transport_vehicle_registration_expiry_date"].ToString());
                          
                            TransportVehicle.Add(vobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportVehicle);
        }

        [Route("CUDTransportVehicle")]
        public HttpResponseMessage CUDTransportVehicle(Sims086 simsobjv)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportVehicle", simsobjv));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims.sims_transport_vehicle_proc",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobjv.opr),
                          new SqlParameter("@sims_transport_vehicle_code",simsobjv.sims_transport_vehicle_code),
                          new SqlParameter("@sims_transport_vehicle_registration_number", simsobjv.sims_transport_vehicle_registration_number),
                          new SqlParameter("@sims_transport_vehicle_name_plate",simsobjv.sims_transport_vehicle_name_plate),
                          new SqlParameter("@sims_transport_vehicle_ownershiptype", simsobjv.sims_transport_vehicle_ownership),
                          new SqlParameter("@sims_transport_manufacturer_name",simsobjv.sims_transport_manufacturer_name),
                          new SqlParameter("@sims_transport_vehicle_model_name", simsobjv.sims_transport_vehicle_model_name),
                          new SqlParameter("@sims_transport_vehicle_model_year",simsobjv.sims_transport_vehicle_model_year),
                          new SqlParameter("@sims_transport_vehicle_seating_capacity", simsobjv.sims_transport_vehicle_seating_capacity),
                          new SqlParameter("@sims_transport_vehicle_power",simsobjv.sims_transport_vehicle_power),
                          new SqlParameter("@sims_transport_vehicle_transmissiontype", simsobjv.sims_transport_vehicle_transmission),
                          new SqlParameter("@sims_transport_vehicle_color", simsobjv.sims_transport_vehicle_color),
                          new SqlParameter("@sims_transport_vehicle_camera_enabled",simsobjv.sims_transport_vehicle_camera_enabled==true?"Y":"N"),
                          new SqlParameter("@sims_transport_vehicle_security_enabled",simsobjv.sims_transport_vehicle_security_enabled==true?"Y":"N"),
                          new SqlParameter("@sims_transport_vehicle_status",simsobjv.sims_transport_vehicle_status==true?"A":"I"),

                          new SqlParameter("@sims_transport_vehicle_date_of_purchase",          db.DBYYYYMMDDformat(simsobjv.sims_transport_vehicle_date_of_purchase)),
                          new SqlParameter("@sims_transport_vehicle_registration_date",         db.DBYYYYMMDDformat(simsobjv.sims_transport_vehicle_registration_date)),
                          new SqlParameter("@sims_transport_vehicle_registration_expiry_date",  db.DBYYYYMMDDformat(simsobjv.sims_transport_vehicle_registration_expiry_date))
                          
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getTransportDriver")]
        public HttpResponseMessage getTransportDriver()
        {
            List<Sims088> TransportDriver = new List<Sims088>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 objd = new Sims088();

                            objd.sims_driver_code = dr["sims_driver_code"].ToString();
                            objd.sims_driver_name = dr["sims_driver_name"].ToString();
                            objd.sims_employee_code = dr["sims_employee_code"].ToString();
                            objd.sims_user_code = dr["sims_user_code"].ToString();
                            objd.sims_driver_type = dr["sims_driver_type"].ToString();
                            objd.sims_driver_gender = dr["sims_driver_gender"].ToString();
                            objd.sims_driver_experience_years = dr["sims_driver_experience_years"].ToString();
                            objd.sims_driver_mobile_number1 = dr["sims_driver_mobile_number1"].ToString();
                            objd.sims_driver_mobile_number2 = dr["sims_driver_mobile_number2"].ToString();
                            objd.sims_driver_mobile_number3 = dr["sims_driver_mobile_number3"].ToString();
                            objd.sims_driver_address = dr["sims_driver_address"].ToString();
                            objd.sims_driver_driving_license_number = dr["sims_driver_driving_license_number"].ToString();
                            objd.sims_driver_license_place_of_issue = dr["sims_driver_license_place_of_issue"].ToString();
                            objd.sims_driver_license_vehicle_mode = dr["sims_driver_license_vehicle_mode"].ToString();
                            objd.sims_driver_license_vehicle_category = dr["sims_driver_license_vehicle_category"].ToString();
                            objd.sims_driver_visa_number = dr["sims_driver_visa_number"].ToString();
                            objd.sims_driver_visa_issuing_place = dr["sims_driver_visa_issuing_place"].ToString();
                            objd.sims_driver_visa_issuing_authority = dr["sims_driver_visa_issuing_authority"].ToString();
                            objd.sims_driver_visa_type = dr["sims_driver_visa_type"].ToString();
                            objd.sims_driver_national_id = dr["sims_driver_national_id"].ToString();

                            objd.sims_driver_date_of_birth =            db.UIDDMMYYYYformat(dr["sims_driver_date_of_birth"].ToString());
                            objd.sims_driver_national_id_issue_date =   db.UIDDMMYYYYformat(dr["sims_driver_national_id_issue_date"].ToString());
                            objd.sims_driver_national_id_expiry_date =  db.UIDDMMYYYYformat(dr["sims_driver_national_id_expiry_date"].ToString());
                            objd.sims_driver_license_issue_date =       db.UIDDMMYYYYformat(dr["sims_driver_license_issue_date"].ToString());
                            objd.sims_driver_license_expiry_date =      db.UIDDMMYYYYformat(dr["sims_driver_license_expiry_date"].ToString());
                            objd.sims_driver_visa_issue_date =          db.UIDDMMYYYYformat(dr["sims_driver_visa_issue_date"].ToString());
                            objd.sims_driver_visa_expiry_date =         db.UIDDMMYYYYformat(dr["sims_driver_visa_expiry_date"].ToString());
                            
                            objd.sims_driver_status = dr["sims_driver_status"].ToString().Equals("A") ? true : false;
                            TransportDriver.Add(objd);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportDriver);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportDriver);
        }

        [Route("CUDTransportDriver")]
        public HttpResponseMessage CUDTransportDriver(Sims088 simsobjd)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportDriver", simsobjd));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_driver_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobjd.opr),
                          new SqlParameter("@sims_driver_code",simsobjd.sims_driver_code),
                          new SqlParameter("@sims_driver_name", simsobjd.sims_driver_name),
                          //new SqlParameter("@employee_name",simsobjd.employee_name),
                          new SqlParameter("@sims_driver_gender", simsobjd.sims_driver_gender),
                          new SqlParameter("@sims_driver_experience_years",simsobjd.sims_driver_experience_years),
                          new SqlParameter("@sims_user_code", simsobjd.sims_user_code),
                          new SqlParameter("@sims_employee_code",simsobjd.sims_employee_code),

                          new SqlParameter("@sims_driver_mobile_number1", simsobjd.sims_driver_mobile_number1),
                          new SqlParameter("@sims_driver_mobile_number2",simsobjd.sims_driver_mobile_number2),
                          new SqlParameter("@sims_driver_mobile_number3", simsobjd.sims_driver_mobile_number3),
                          new SqlParameter("@sims_driver_address",simsobjd.sims_driver_address),
                          new SqlParameter("@sims_driver_license_vehicle_category",simsobjd.sims_driver_license_vehicle_category),
                          new SqlParameter("@sims_driver_type", simsobjd.sims_driver_type),
                          new SqlParameter("@sims_driver_driving_license_number",simsobjd.sims_driver_driving_license_number),
                          new SqlParameter("@sims_driver_license_place_of_issue",simsobjd.sims_driver_license_place_of_issue),
                          new SqlParameter("@sims_driver_visa_number",simsobjd.sims_driver_visa_number),
                          new SqlParameter("@sims_driver_visa_issuing_place",simsobjd.sims_driver_visa_issuing_place),
                          new SqlParameter("@sims_driver_visa_issuing_authority",simsobjd.sims_driver_visa_issuing_authority),
                          new SqlParameter("@sims_driver_visa_type", simsobjd.sims_driver_visa_type),
                          new SqlParameter("@sims_driver_national_id", simsobjd.sims_driver_national_id),
                          new SqlParameter("@sims_driver_status",simsobjd.sims_driver_status==true?"A":"I"),

                          new SqlParameter("@sims_driver_national_id_issue_date", db.DBYYYYMMDDformat(simsobjd.sims_driver_national_id_issue_date)),
                          new SqlParameter("@sims_driver_national_id_expiry_date",db.DBYYYYMMDDformat(simsobjd.sims_driver_national_id_expiry_date)),
                          new SqlParameter("@sims_driver_date_of_birth",          db.DBYYYYMMDDformat(simsobjd.sims_driver_date_of_birth)),
                          new SqlParameter("@sims_driver_license_issue_date",     db.DBYYYYMMDDformat(simsobjd.sims_driver_license_issue_date)),
                          new SqlParameter("@sims_driver_license_expiry_date",    db.DBYYYYMMDDformat(simsobjd.sims_driver_license_expiry_date)),
                          new SqlParameter("@sims_driver_visa_issue_date",        db.DBYYYYMMDDformat(simsobjd.sims_driver_visa_issue_date)),
                          new SqlParameter("@sims_driver_visa_expiry_date",       db.DBYYYYMMDDformat(simsobjd.sims_driver_visa_expiry_date))
 

                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getTransportCaretakercode")]
        public HttpResponseMessage getTransportCaretakercode()
        {
            List<Sims087> TransportCaretaker = new List<Sims087>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_caretaker_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "V")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 obj = new Sims087();
                            obj.sims_caretaker_code = dr["caretaker_code"].ToString();
                            TransportCaretaker.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportCaretaker);
        }

    }
}