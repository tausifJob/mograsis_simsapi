﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/TransRouteStudDetails")]
    public class TransportRouteStudController:ApiController
    {

        #region Api For Student Transport Apply...

        [Route("GetAllStudentName")]
        public HttpResponseMessage GetAllStudentName(string academic_year)
        {
            List<Sims081> term_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                        new List<SqlParameter>() 
                         { 
                new SqlParameter("@opr","T"),
                new SqlParameter("@sims_academic_year",academic_year)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();

                            try
                            {
                                simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                                simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                                simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            }
                            catch (Exception) { }

                            try
                            {
                                simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                                simsobj.parent_name = dr["parent_name"].ToString();
                                simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                                simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                                simsobj.sims_student_date = dr["sims_student_date"].ToString();
                                simsobj.sims_student_transport_status = dr["sims_student_transport_status"].ToString();
                                simsobj.nationality = dr["nationality"].ToString();
                                simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                simsobj.rfid_card = dr["rfid_card"].ToString();
                                simsobj.mother_name = dr["mother_name"].ToString();
                            }
                            catch (Exception) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }


        [Route("GetAllStudentName_AHGS")]
        public HttpResponseMessage GetAllStudentName_AHGS(string academic_year)
        {
            List<Sims081> term_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_AHGS_proc]",
                     new List<SqlParameter>()
                    {
                         new SqlParameter("@opr","T"),
                         new SqlParameter("@sims_academic_year",academic_year)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                            simsobj.student_name = dr["student_name"].ToString();

                            try
                            {
                                simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                                simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                                simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            }
                            catch (Exception) { }

                            try
                            {
                                simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                                simsobj.parent_name = dr["parent_name"].ToString();
                                simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                                simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                                simsobj.sims_student_date = dr["sims_student_date"].ToString();
                                simsobj.sims_student_transport_status = dr["sims_student_transport_status"].ToString();
                                simsobj.nationality = dr["nationality"].ToString();
                                simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                simsobj.rfid_card = dr["rfid_card"].ToString();
                                simsobj.mother_name = dr["mother_name"].ToString();
                                simsobj.sims_transport_grade_code = dr["sims_transport_grade_code"].ToString();
                                simsobj.sims_transport_section_code = dr["sims_transport_section_code"].ToString();
                            }
                            catch (Exception) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("getSearchEmployee")]
        public HttpResponseMessage getSearchEmployee(string data)
        {

            Sims081 obj = new Sims081();
            List<Sims081> emp_list = new List<Sims081>();

            if (data == "undefined" || data == "\"\"")
            {
                data = null;
            }
            else
            {
                obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims081>(data);
            }

            Message message = new Message();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[dbo].[Search]",  //this SP used to search employees n user data
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", 'S'),
                            new SqlParameter("@opr_mem_code", 'R'),
                            new SqlParameter("@user_name",obj.comn_user_name),
                            new SqlParameter("@emp_name",obj.EmpName),

                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.comn_user_name = dr["comn_user_name"].ToString();
                            simsobj.EmpName = dr["EmpName"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.em_email = dr["em_email"].ToString();
                            emp_list.Add(simsobj);
                        }
                        
                        return Request.CreateResponse(HttpStatusCode.OK, emp_list);

                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, emp_list);
                }
            }
            catch (Exception x)
            {
                message.strMessage = "No Records Found";
                message.messageType = MessageType.Error;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
            }
        }

        [Route("getAllRouteNameForOutward")]
        public HttpResponseMessage getAllRouteNameForOutward(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRouteNameForOutward(),PARAMETERS :: NO";

            List<Sims085> routename_outward = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "O"),
                             new SqlParameter("@sims_academic_year",Acayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            routename_outward.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename_outward);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename_outward);
            }
        }

        [Route("getAllRouteNameForInward")]
        public HttpResponseMessage getAllRouteNameForInward(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllRouteNameForInward(),PARAMETERS :: NO";

            List<Sims085> routename_inward = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "R"),
                             new SqlParameter("@sims_academic_year",Acayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            routename_inward.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename_inward);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename_inward);
            }
        }

        [Route("getLanLogByRouteStop")]
        public HttpResponseMessage getLanLogByRouteStop(string route_code, string aca_year, string route_stop)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getLanLogByRouteStop(),PARAMETERS :: NO";
            List<Sims081> lang_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "L"),
                            new SqlParameter("@sims_transport_route_code", route_code),
                            new SqlParameter("@sims_transport_academic_year", aca_year),
                            new SqlParameter("@sims_transport_pickup_stop_code", route_stop),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_route_student_stop_lat = dr["sims_transport_stop_lat"].ToString();
                            simsobj.sims_transport_route_student_stop_long = dr["sims_transport_stop_long"].ToString();
                            lang_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lang_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, lang_list);
            }
        }

        [Route("getAllOutwardRouteName")]
        public HttpResponseMessage getAllOutwardRouteName(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllOutwardRouteName(),PARAMETERS :: NO";

            List<Sims085> routename = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "O"),
                              new SqlParameter("@sims_academic_year",Acayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            routename.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename);
            }
        }

        [Route("CUDTransportRouteStudent")]
        public HttpResponseMessage CUDTransportRouteStudent(List<Sims081> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            //string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims081 routeobj in data)
                    {
                        #region Insertion Operation
                        try
                        {
                            string fDate = db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from.ToString());
                            DateTime fpDate = DateTime.Parse(fDate);
                            string uDate = db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto.ToString());
                            DateTime upDate = DateTime.Parse(uDate);
                            string s_date = null, e_date = null, r_code = null, d_code = null;
                            string sims_1 = null, sims_2 = null, sims_3 = null, sims_4 = null, sims_5 = null, sims_6 = null, sims_7 = null, sims_8 = null, sims_9 = null, sims_10 = null, sims_11 = null, sims_12 = null;
                            double p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, p8 = 0, p9 = 0, p10 = 0, p11 = 0, p12 = 0;
                            double n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0, n8 = 0, n9 = 0, n10 = 0, n11 = 0, n12 = 0;
                            double r1 = 0, r2 = 0, r3 = 0, r4 = 0, r5 = 0, r6 = 0, r7 = 0, r8 = 0, r9 = 0, r10 = 0, r11 = 0, r12 = 0;

                            for (DateTime k = fpDate; k <= upDate;)
                            {
                                #region
                                if (k.Month == 1)
                                {
                                    sims_1 = "1";
                                }
                                else if (k.Month == 2)
                                {
                                    sims_2 = "2";
                                }
                                else if (k.Month == 3)
                                {
                                    sims_3 = "3";
                                }
                                else if (k.Month == 4)
                                {
                                    sims_4 = "4";
                                }
                                else if (k.Month == 5)
                                {
                                    sims_5 = "5";
                                }
                                else if (k.Month == 6)
                                {
                                    sims_6 = "6";
                                }
                                else if (k.Month == 7)
                                {
                                    sims_7 = "7";
                                }
                                else if (k.Month == 8)
                                {
                                    sims_8 = "8";
                                }
                                else if (k.Month == 9)
                                {
                                    sims_9 = "9";
                                }
                                else if (k.Month == 10)
                                {
                                    sims_10 = "10";
                                }
                                else if (k.Month == 11)
                                {
                                    sims_11 = "11";
                                }
                                else if (k.Month == 12)
                                {
                                    sims_12 = "12";
                                }
                                k = k.AddMonths(1);
                                #endregion
                            }

                            SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                                new List<SqlParameter>()
                            {

                          new SqlParameter("@opr",routeobj.opr),
                          new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),
                          new SqlParameter("@sims_cur_code",routeobj.sims_cur_code),
                          new SqlParameter("@sims_grade_code",routeobj.sims_grade_code),
                          new SqlParameter("@sims_section_code",routeobj.sims_section_code),
                          new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                          new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",routeobj.sims_transport_route_direction),
                          new SqlParameter("@sims_seat_capacity", routeobj.sims_transport_vehicle_seating_capacity),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto)),

                          new SqlParameter("@sims_transport_pickup_stop_code",routeobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_stop_lat_pickup", routeobj.sims_transport_route_student_stop_lat),
                          new SqlParameter("@sims_transport_stop_long_pickup",routeobj.sims_transport_route_student_stop_long),
                          new SqlParameter("@sims_transport_drop_stop_code",routeobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_stop_lat_drop", routeobj.sims_transport_stop_lat_drop),
                          new SqlParameter("@sims_transport_stop_long_drop",routeobj.sims_transport_stop_long_drop),

                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12)

                        });

                            int ins = dr.RecordsAffected;
                            Sims081_Msg st = new Sims081_Msg();
                            if (dr.Read())
                            {
                                st.sims_enroll = dr["Enroll"].ToString();
                                ins = int.Parse(dr["status"].ToString());
                                st.sims_msg = dr["sims_msg"].ToString();
                                st.sims_status = ins > 0 ? "Success" : "Fail";
                                msg.strMessage = st.sims_msg;
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                                st.sims_status = ins > 0 ? "Success" : "Fail";
                                if (st.sims_msg == "Update Fee")
                                {

                                    if (!string.IsNullOrEmpty(routeobj.sims_route_fee))
                                    {

                                    }
                                    else
                                    {

                                        try
                                        {

                                            #region Code For Update Old Transport Fee...
                                            dr.Close();
                                            SqlDataReader dr1 = db.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                                                 new List<SqlParameter>()
                                                 {
                                                   new SqlParameter("@opr", 'A'),
                                                   new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),
                                                   new SqlParameter("@sims_cur_code",routeobj.sims_cur_code),
                                                   new SqlParameter("@sims_grade_code",routeobj.sims_grade_code),
                                                   new SqlParameter("@sims_section_code",routeobj.sims_section_code),
                                                   new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                                                 });

                                            dr1.Close();

                                            #endregion

                                            #region Code For Updation...

                                            using (DBConnection db2 = new DBConnection())
                                            {
                                                db2.Open();
                                                SqlDataReader dr2 = db2.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                                                new List<SqlParameter>()
                                                {
                                                 new SqlParameter("@opr", 'O'),
                                                 new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),
                                                 new SqlParameter("@sims_cur_code",routeobj.sims_cur_code),
                                                 new SqlParameter("@sims_grade_code",routeobj.sims_grade_code),
                                                 new SqlParameter("@sims_section_code",routeobj.sims_section_code),
                                                 new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                                                });
                                                if (dr2.HasRows)
                                                {
                                                    while (dr2.Read())
                                                    {
                                                        try
                                                        {
                                                            s_date = db.UIDDMMYYYYformat(dr2["sims_transport_effective_from"].ToString());
                                                            e_date = db.UIDDMMYYYYformat(dr2["sims_transport_effective_upto"].ToString());
                                                            r_code = (dr2["sims_transport_route_code"].ToString());
                                                            d_code = (dr2["sims_transport_route_direction"].ToString());
                                                        }
                                                        catch (Exception ex1)
                                                        {
                                                        }
                                                        //DateTime fpDate_new = DateTime.Parse(s_date);                                                    
                                                        //DateTime upDate_new = DateTime.Parse(e_date);

                                                        #region Code For Get Old Transport Fee...
                                                        //dr2.Close();
                                                        using (DBConnection db3 = new DBConnection())
                                                        {
                                                            db3.Open();
                                                            SqlDataReader dr3 = db3.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                                                            new List<SqlParameter>()
                                                            {
                                                            new SqlParameter("@opr", 'N'),
                                                            new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                                                            new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number)
                                                            });

                                                            if (dr3.HasRows)
                                                            {
                                                                while (dr3.Read())
                                                                {
                                                                    p1 = double.Parse(dr3["sims_fee_period1"].ToString());
                                                                    p2 = double.Parse(dr3["sims_fee_period2"].ToString());
                                                                    p3 = double.Parse(dr3["sims_fee_period3"].ToString());
                                                                    p4 = double.Parse(dr3["sims_fee_period4"].ToString());
                                                                    p5 = double.Parse(dr3["sims_fee_period5"].ToString());
                                                                    p6 = double.Parse(dr3["sims_fee_period6"].ToString());
                                                                    p7 = double.Parse(dr3["sims_fee_period7"].ToString());
                                                                    p8 = double.Parse(dr3["sims_fee_period8"].ToString());
                                                                    p9 = double.Parse(dr3["sims_fee_period9"].ToString());
                                                                    p10 = double.Parse(dr3["sims_fee_period10"].ToString());
                                                                    p11 = double.Parse(dr3["sims_fee_period11"].ToString());
                                                                    p12 = double.Parse(dr3["sims_fee_period12"].ToString());
                                                                }
                                                            }
                                                            dr3.Close();
                                                        }

                                                        #endregion

                                                        #region Code For Get New Transport Fee...
                                                        using (DBConnection db3 = new DBConnection())
                                                        {
                                                            db3.Open();
                                                            SqlDataReader dr4 = db3.ExecuteStoreProcedure("sims_transport_cancel",
                                                            new List<SqlParameter>()
                                                        {
                                                        new SqlParameter("@opr", 'S'),
                                                        new SqlParameter("@sub_opr",'H'),
                                                        new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                                                        new SqlParameter("@sims_transport_route_code", r_code),
                                                        new SqlParameter("@sims_transport_route_direction", d_code)
                                                        });

                                                            if (dr4.HasRows)
                                                            {
                                                                while (dr4.Read())
                                                                {
                                                                    n1 = double.Parse(dr4["sims_transport_period1"].ToString());
                                                                    n2 = double.Parse(dr4["sims_transport_period2"].ToString());
                                                                    n3 = double.Parse(dr4["sims_transport_period3"].ToString());
                                                                    n4 = double.Parse(dr4["sims_transport_period4"].ToString());
                                                                    n5 = double.Parse(dr4["sims_transport_period5"].ToString());
                                                                    n6 = double.Parse(dr4["sims_transport_period6"].ToString());
                                                                    n7 = double.Parse(dr4["sims_transport_period7"].ToString());
                                                                    n8 = double.Parse(dr4["sims_transport_period8"].ToString());
                                                                    n9 = double.Parse(dr4["sims_transport_period9"].ToString());
                                                                    n10 = double.Parse(dr4["sims_transport_period10"].ToString());
                                                                    n11 = double.Parse(dr4["sims_transport_period11"].ToString());
                                                                    n12 = double.Parse(dr4["sims_transport_period12"].ToString());
                                                                }
                                                            }

                                                            dr4.Close();
                                                        }
                                                        #endregion

                                                        #region Code For Period...
                                                        try
                                                        {
                                                            DateTime fpDate_new = DateTime.ParseExact(s_date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                            DateTime upDate_new = DateTime.ParseExact(e_date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                            for (DateTime k = fpDate_new; k <= upDate_new;)
                                                            {
                                                                #region

                                                                if (k.Month == 1)
                                                                {
                                                                    r1 = p1 + n1;
                                                                }
                                                                else if (k.Month == 2)
                                                                {
                                                                    r2 = p2 + n2;
                                                                }
                                                                else if (k.Month == 3)
                                                                {
                                                                    r3 = p3 + n3;
                                                                }
                                                                else if (k.Month == 4)
                                                                {
                                                                    r4 = p4 + n4;
                                                                }
                                                                else if (k.Month == 5)
                                                                {
                                                                    r5 = p5 + n5;
                                                                }
                                                                else if (k.Month == 6)
                                                                {
                                                                    r6 = p6 + n6;
                                                                }
                                                                else if (k.Month == 7)
                                                                {
                                                                    r7 = p7 + n7;
                                                                }
                                                                else if (k.Month == 8)
                                                                {
                                                                    r8 = p8 + n8;
                                                                }
                                                                else if (k.Month == 9)
                                                                {
                                                                    r9 = p9 + n9;
                                                                }
                                                                else if (k.Month == 10)
                                                                {
                                                                    r10 = p10 + n10;
                                                                }
                                                                else if (k.Month == 11)
                                                                {
                                                                    r11 = p11 + n11;
                                                                }
                                                                else if (k.Month == 12)
                                                                {
                                                                    r12 = p12 + n12;
                                                                }
                                                                k = k.AddMonths(1);

                                                                #endregion
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }

                                                        n1 = n2 = n3 = n4 = n5 = n6 = n7 = n8 = n9 = n10 = n11 = n12 = 0;

                                                        #endregion

                                                        #region Code For Update Transport Fee...

                                                        using (DBConnection db3 = new DBConnection())
                                                        {
                                                            try
                                                            {
                                                                db3.Open();

                                                                SqlDataReader dr5 = db3.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                                                                new List<SqlParameter>()
                                                                {
                                                                new SqlParameter("@opr", "J"),
                                                                new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                                                                new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),
                                                                new SqlParameter("@sims_cur_code",routeobj.sims_cur_code),
                                                                new SqlParameter("@sims_grade_code",routeobj.sims_grade_code),
                                                                new SqlParameter("@sims_section_code",routeobj.sims_section_code),
                                                                new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from)),
                                                                new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto)),
                                                                new SqlParameter("@sims_1p", r1),
                                                                new SqlParameter("@sims_2p", r2),
                                                                new SqlParameter("@sims_3p", r3),
                                                                new SqlParameter("@sims_4p", r4),
                                                                new SqlParameter("@sims_5p", r5),
                                                                new SqlParameter("@sims_6p", r6),
                                                                new SqlParameter("@sims_7p", r7),
                                                                new SqlParameter("@sims_8p", r8),
                                                                new SqlParameter("@sims_9p", r9),
                                                                new SqlParameter("@sims_10p", r10),
                                                                new SqlParameter("@sims_11p", r11),
                                                                new SqlParameter("@sims_12p", r12),
                                                                });

                                                                int ins1 = dr5.RecordsAffected;

                                                                if (dr5.Read())
                                                                {
                                                                    st.sims_msg = null;
                                                                    st.sims_enroll = dr5["Enroll"].ToString();
                                                                    ins1 = int.Parse(dr5["status"].ToString());
                                                                    st.sims_msg = dr5["sims_msg"].ToString();
                                                                    st.sims_status = ins1 > 0 ? "Success" : "Fail";
                                                                    msg.strMessage = st.sims_msg;
                                                                }

                                                                if (dr5.RecordsAffected > 0)
                                                                {
                                                                    inserted = true;
                                                                    st.sims_status = ins1 > 0 ? "Success" : "Fail";
                                                                }

                                                                dr5.Close();
                                                            }
                                                            catch (Exception p)
                                                            {

                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }

                                            #endregion

                                        }

                                        catch (Exception e)
                                        {

                                        }

                                    }
                                }
                            }
                            dr.Close();
                        }
                        catch (Exception ex)
                        {

                        } 
                        #endregion
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("CUDTransportRouteEmployee")]
        public HttpResponseMessage CUDTransportRouteEmployee(List<Sims081> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            //string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims081 routeobj in data)
                    {
                        string fDate = db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from.ToString());
                        DateTime fpDate = DateTime.Parse(fDate);
                        string uDate = db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto.ToString());
                        DateTime upDate = DateTime.Parse(uDate);

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_employee_proc]",
                            new List<SqlParameter>()
                        {

                            new SqlParameter("@opr",routeobj.opr),
                            new SqlParameter("@sims_transport_em_number",routeobj.sims_transport_em_number),
                            new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                            new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                            new SqlParameter("@sims_transport_route_direction",routeobj.sims_transport_route_direction),
                            new SqlParameter("@sims_seat_capacity", routeobj.sims_transport_vehicle_seating_capacity),
                            new SqlParameter("@sims_transport_effective_from", fpDate),
                            new SqlParameter("@sims_transport_effective_upto", upDate),

                            new SqlParameter("@sims_transport_pickup_stop_code",routeobj.sims_transport_pickup_stop_code),
                            new SqlParameter("@sims_transport_stop_lat_pickup", routeobj.sims_transport_route_student_stop_lat),
                            new SqlParameter("@sims_transport_stop_long_pickup",routeobj.sims_transport_route_student_stop_long),
                            new SqlParameter("@sims_transport_drop_stop_code",routeobj.sims_transport_drop_stop_code),
                            new SqlParameter("@sims_transport_stop_lat_drop", routeobj.sims_transport_stop_lat_drop),
                            new SqlParameter("@sims_transport_stop_long_drop",routeobj.sims_transport_stop_long_drop),

                        });

                        int ins = dr.RecordsAffected;
                        Sims081_Msg st = new Sims081_Msg();
                        if (dr.Read())
                        {
                            st.sims_enroll = dr["Enroll"].ToString();
                            ins = int.Parse(dr["status"].ToString());
                            st.sims_msg = dr["sims_msg"].ToString();
                            st.sims_status = ins > 0 ? "Success" : "Fail";
                            msg.strMessage = st.sims_msg;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("getenrollrelatedrecords")]
        public HttpResponseMessage getenrollrelatedrecords(string trans_Acayear, string trans_enroll)
        {
            
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getenrollrelatedrecords(),PARAMETERS :: NO";

            List<Sims085> routename = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_apply_update_proc]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "T"),
                             new SqlParameter("@sims_transport_academic_year",trans_Acayear),
                             new SqlParameter("@sims_transport_enroll_number",trans_enroll)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_enroll_number = dr["sims_transport_enroll_number"].ToString();
                            simsobj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            simsobj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());
                            routename.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename);
            }
        }
        
        [Route("getenrollrelatedEmployeerecords")]
        public HttpResponseMessage getenrollrelatedEmployeerecords(string trans_Acayear, string trans_enroll)
        {
            
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getenrollrelatedEmployeerecords(),PARAMETERS :: NO";
            
            List<Sims085> routename = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_update_employee_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "R"),
                              new SqlParameter("@sims_transport_academic_year",trans_Acayear),
                              new SqlParameter("@sims_transport_em_number",trans_enroll)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_em_number = dr["sims_transport_em_number"].ToString();
                            simsobj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_appl_form_field_value1 = dr["sims_appl_form_field_value1"].ToString();
                            simsobj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            simsobj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());
                            routename.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename);
            }
        }

        [Route("getTransportSeatCount")]
        public HttpResponseMessage getTransportSeatCount(string academic_year, string route_code, string route_direction)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSitingCapacity(),PARAMETERS :: NO";

            List<Sims081> Seat_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_new_apply_update_cancel]",
                        new List<SqlParameter>()
                         {
                             new SqlParameter("@opr", "S"),
                             new SqlParameter("@sub_opr", "R"),
                             new SqlParameter("@sims_transport_academic_year", academic_year),
                             new SqlParameter("@sims_transport_route_code", route_code),
                             new SqlParameter("@sims_transport_route_direction", route_direction),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_vehicle_seating_capacity = dr["sims_transport_vehicle_seating_capacity"].ToString();
                            simsobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            simsobj.sims_vehicle_seating_capacity = dr["vehicle_seating_capacity"].ToString();
                            simsobj.sims_allocated_seat = dr["allocated_seat"].ToString();
                            simsobj.sims_max_to_exceed_seating_capacity = dr["max_to_exceed_seating_capacity"].ToString();
                            Seat_list.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Seat_list);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Seat_list);
            }
        }

        [Route("getTransportHistory")]
        public HttpResponseMessage getTransportHistory(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Sims525 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims525>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_transport_student_history_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'S'),
                                new SqlParameter("@sims_transport_academic_year", sf.sims_transport_academic_year),
                                new SqlParameter("@sims_transport_vehicle_code", sf.V_Code),
                                new SqlParameter("@sims_transport_route_code", sf.sims_transport_route_code),
                                new SqlParameter("@sims_transport_route_direction", sf.sims_transport_route_direction),
                                new SqlParameter("@sims_transport_pickup_stop_code", sf.sims_transport_pickup_stop_code),
                                new SqlParameter("@sims_student_name", sf.sims_student_name),
                                new SqlParameter("@sims_transport_enroll_number", sf.sims_transport_enroll_number),
                                new SqlParameter("@sims_parent_name",sf.sims_parent_name),
                                new SqlParameter("@sims_parent_id",sf.sims_parent_id),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        #endregion

        #region API FOR TRANSPORT ROUTE STUDENT MISSING IN FEE OR TRANSPORT...

        [Route("getTransportstudentfeetransportHistory")]
        public HttpResponseMessage getTransportstudentfeetransportHistory(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Sims525 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims525>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_transport_student_history_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_transport_academic_year", sf.sims_transport_academic_year),
                                new SqlParameter("@sims_transport_vehicle_code", sf.V_Code),
                                new SqlParameter("@sims_transport_route_code", sf.sims_transport_route_code),
                                new SqlParameter("@sims_transport_route_direction", sf.sims_transport_route_direction),
                                new SqlParameter("@sims_transport_pickup_stop_code", sf.sims_transport_pickup_stop_code),
                                new SqlParameter("@sims_student_name", sf.sims_student_name),
                                new SqlParameter("@sims_transport_enroll_number", sf.sims_transport_enroll_number),
                                new SqlParameter("@sims_transport_condition_code",sf.sims_transport_condition_code),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CUDUpdateTransportFeeInStudentFee")]
        public HttpResponseMessage CUDUpdateTransportFeeInStudentFee(List<Sims081> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims081 routeobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_history_proc]",
                            new List<SqlParameter>()
                        {

                          new SqlParameter("@opr","U"),
                          new SqlParameter("@sims_transport_enroll_number",routeobj.sims_transport_enroll_number),
                          new SqlParameter("@sims_cur_code",routeobj.sims_cur_code),
                          new SqlParameter("@sims_grade_code",routeobj.sims_grade_code),
                          new SqlParameter("@sims_section_code",routeobj.sims_section_code),
                          new SqlParameter("@sims_transport_academic_year",routeobj.sims_academic_year),
                          new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",routeobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(routeobj.sims_transport_effective_upto)),

                    });

                        int ins = dr.RecordsAffected;
                        Sims081_Msg st = new Sims081_Msg();
                        if (dr.Read())
                        {
                            st.sims_enroll = dr["Enroll"].ToString();
                            ins = int.Parse(dr["status"].ToString());
                            st.sims_msg = dr["sims_msg"].ToString();
                            st.sims_status = ins > 0 ? "Success" : "Fail";
                            msg.strMessage = st.sims_msg;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("GetAllParentName")]
        public HttpResponseMessage GetAllParentName()
        {
            List<Sims081> term_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                     new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","B")                        
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();                    
                            try
                            {
                                simsobj.sims_parent_number = dr["sims_parent_number"].ToString();
                                simsobj.sims_parent_login_code = dr["sims_parent_login_code"].ToString();
                                simsobj.parent_name = dr["parent_name"].ToString();
                            }
                            catch (Exception) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK,e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetStudentFromParent")]
        public HttpResponseMessage GetStudentFromParent(string parent_login_code)
        {
            List<Sims081> term_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_proc]",
                     new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","C"),
                        new SqlParameter("@parent_login_code",parent_login_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            try
                            {
                                simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                simsobj.student_name = dr["student_name"].ToString();
                                simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                                simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                                simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            }
                            catch (Exception) { }

                            try
                            {
                                simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                                simsobj.parent_name = dr["parent_name"].ToString();
                                simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                                simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                                simsobj.sims_student_date = dr["sims_student_date"].ToString();
                                simsobj.sims_student_transport_status = dr["sims_student_transport_status"].ToString();
                                simsobj.nationality = dr["nationality"].ToString();
                                simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                simsobj.rfid_card = dr["rfid_card"].ToString();
                                simsobj.mother_name = dr["mother_name"].ToString();
                            }
                            catch (Exception) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        [Route("GetStudentFromParent_AHGS")]
        public HttpResponseMessage GetStudentFromParent_AHGS(string parent_login_code)
        {
            List<Sims081> term_list = new List<Sims081>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_approved_AHGS_proc]",
                     new List<SqlParameter>()
                    {
                        new SqlParameter("@opr","C"),
                        new SqlParameter("@parent_login_code",parent_login_code)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            try
                            {
                                simsobj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                simsobj.student_name = dr["student_name"].ToString();
                                simsobj.sims_cur_code = dr["sims_cur_code"].ToString();
                                simsobj.sims_grade_code = dr["sims_grade_code"].ToString();
                                simsobj.sims_section_code = dr["sims_section_code"].ToString();
                            }
                            catch (Exception) { }

                            try
                            {
                                simsobj.sims_student_dob = dr["sims_student_dob"].ToString();
                                simsobj.parent_name = dr["parent_name"].ToString();
                                simsobj.sims_parent_father_summary_address = dr["sims_parent_father_summary_address"].ToString();
                                simsobj.sims_parent_father_mobile = dr["sims_parent_father_mobile"].ToString();
                                simsobj.sims_parent_father_email = dr["sims_parent_father_email"].ToString();
                                simsobj.sims_parent_mother_mobile = dr["sims_parent_mother_mobile"].ToString();
                                simsobj.sims_parent_mother_email = dr["sims_parent_mother_email"].ToString();
                                simsobj.sims_parent_father_area_number = dr["sims_parent_father_area_number"].ToString();
                                simsobj.sims_student_date = dr["sims_student_date"].ToString();
                                simsobj.sims_student_transport_status = dr["sims_student_transport_status"].ToString();
                                simsobj.nationality = dr["nationality"].ToString();
                                simsobj.sims_student_gender = dr["sims_student_gender"].ToString();
                                simsobj.rfid_card = dr["rfid_card"].ToString();
                                simsobj.mother_name = dr["mother_name"].ToString();
                                simsobj.sims_transport_grade_code = dr["sims_transport_grade_code"].ToString();
                                simsobj.sims_transport_section_code = dr["sims_transport_section_code"].ToString();
                            }
                            catch (Exception) { }

                            term_list.Add(simsobj);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, term_list);

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, term_list);

        }

        #endregion


        [Route("SafeTravelAPI")]
        public HttpResponseMessage SafeTravelAPI(List<Sims081> data)
        {
            List<Sims081> items = new List<Sims081>();
            try
            {
                //WebClient client = new WebClient();
                //Stream stream = client.OpenRead("http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd:3','username':'hgc_admin','paswrd':'hgcadmin'}");
                //StreamReader reader = new StreamReader(stream);
                //String content = reader.ReadToEnd();
                //items = JsonConvert.DeserializeObject<List<Sims081>>(content);
                //return Request.CreateResponse(HttpStatusCode.OK, items);

                //string url = "http://www.mysafetravel.net/sims/api/scldatav1.php?data=";
                //HttpClient httpClient = new HttpClient();
                //var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
                //var result = httpClient.PostAsync(url, content).Result;
                //return Request.CreateResponse(HttpStatusCode.OK, result);


                //string send_sms_str = "http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd':'3','username':'hgc_admin','paswrd':'hgcadmin'}";
                //  string send_sms_str = string.Format("{0}username={1}&pin={2}&message={3}&mnumber={4}&signature={5}", smsip_obj.sims_sms_http_string, smsip_obj.sims_sms_user, smsip_obj.sims_sms_password, ma.sims_sms_message, ma.sims_recepient_id, smsip_obj.sims_sms_sender_id);
                //if (ma.sims_sms_recurrance_id == "1")
                //{
                //    send_sms_str = string.Format("{0}username={1}&pin={2}&message={3}&mnumber={4}&signature={5}&msgType={6}", smsip_obj.sims_sms_http_string, smsip_obj.sims_sms_user, smsip_obj.sims_sms_password, ma.sims_sms_message, ma.sims_recepient_id, smsip_obj.sims_sms_sender_id, "UC");
                //}
                //else
                //{
                //    send_sms_str = string.Format("{0}username={1}&pin={2}&message={3}&mnumber={4}&signature={5}", smsip_obj.sims_sms_http_string, smsip_obj.sims_sms_user, smsip_obj.sims_sms_password, ma.sims_sms_message, ma.sims_recepient_id, smsip_obj.sims_sms_sender_id);
                //}
                string responseString = string.Empty;
                foreach (Sims081 routeobj in data)
                {
                    //string send_sms_str = "http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd':'1','username':'hgc_admin','paswrd':'hgcadmin','data':[{'enroll_no':'" + routeobj.enroll_no + "','first_name':'" + routeobj.first_name + "','card_no':'" + routeobj.card_no + "','gender':'" + routeobj.gender + "' ,'class_id':'" + routeobj.class_id + "','section_id':'" + routeobj.section_id + "','roll_no':'" + routeobj.roll_no + "','dob':'" + routeobj.dob + "','trip_type':'" + routeobj.trip_type + "','pick_bs_no_id':'" + routeobj.pick_bs_no_id + "','pick_shift_id':'" + routeobj.pick_shift_id + "','pick_point_id':'" + routeobj.pick_point_id + "' ,'drop_bs_no_id':'" + routeobj.drop_bs_no_id + "' ,'drop_shift_id':'" + routeobj.drop_shift_id + "' ,'drop_point_id':'" + routeobj.drop_point_id + "' ,'status':'" + routeobj.status + "' ,'is_transport':'1' ,'sts_transfee':'1','adm_date':'07-08-2019','last_name':'Albert','blood_group':'A+','nationality':'Indian','address1':'Mumbai','address2':'Nerul','father_name':'Varghese','father_mob':'123456','mother_name':'Merlin','mother_mob':'54544','area':'Nerul','alternate_mob':'5555','email':'test@test.com','photo':'N' }]}";
                    string send_sms_str = "http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd':'1','username':'hgc_admin','paswrd':'hgcadmin','data':[{'enroll_no':'"+routeobj.enroll_no +"','first_name':'"+routeobj.first_name + "','card_no':'"+ routeobj.card_no + "','gender':'" + routeobj.gender + "' ,'class_id':'" + routeobj.class_id + "','section_id':'" + routeobj.section_id + "','roll_no':'" + routeobj.roll_no + "','dob':'" + routeobj.dob + "','trip_type':'" + routeobj.trip_type + "','pick_bs_no_id':'" + routeobj.pick_bs_no_id + "','pick_shift_id':'" + routeobj.pick_shift_id + "','pick_point_id':'" + routeobj.pick_point_id + "' ,'drop_bs_no_id':'" + routeobj.drop_bs_no_id + "' ,'drop_shift_id':'" + routeobj.drop_shift_id + "' ,'drop_point_id':'" + routeobj.drop_point_id + "' ,'status':'" + routeobj.status + "','address1':'" + routeobj.address1 + "','father_name':'" + routeobj.father_name + "','father_mob':'" + routeobj.father_mob + "','email':'" + routeobj.email + "','adm_date':'" + routeobj.adm_date + "','mother_name':'" + routeobj.mother_name + "','mother_mob':'" + routeobj.mother_mob + "','area':'" + routeobj.area + "' }]}";
                
                    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(send_sms_str);
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    responseString = respStreamReader.ReadToEnd();
                    //items = JsonConvert.DeserializeObject<List<Sims081>>(responseString);
                    respStreamReader.Close();                
                }

                return Request.CreateResponse(HttpStatusCode.OK, responseString);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("SafeTravelAPI_Update")]
        public HttpResponseMessage SafeTravelAPI_Update(List<Sims081> data)
        {
            List<Sims081> items = new List<Sims081>();
            try
            {               
                string responseString = string.Empty;
                foreach (Sims081 routeobj in data)
                {
                    //string send_sms_str = "http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd':'1','username':'hgc_admin','paswrd':'hgcadmin','data':[{'enroll_no':'" + routeobj.enroll_no + "','first_name':'" + routeobj.first_name + "','card_no':'" + routeobj.card_no + "','gender':'" + routeobj.gender + "' ,'class_id':'" + routeobj.class_id + "','section_id':'" + routeobj.section_id + "','roll_no':'" + routeobj.roll_no + "','dob':'" + routeobj.dob + "','trip_type':'" + routeobj.trip_type + "','pick_bs_no_id':'" + routeobj.pick_bs_no_id + "','pick_shift_id':'" + routeobj.pick_shift_id + "','pick_point_id':'" + routeobj.pick_point_id + "' ,'drop_bs_no_id':'" + routeobj.drop_bs_no_id + "' ,'drop_shift_id':'" + routeobj.drop_shift_id + "' ,'drop_point_id':'" + routeobj.drop_point_id + "' ,'status':'" + routeobj.status + "' ,'is_transport':'1' ,'sts_transfee':'1','adm_date':'07-08-2019','last_name':'Albert','blood_group':'A+','nationality':'Indian','address1':'Mumbai','address2':'Nerul','father_name':'Varghese','father_mob':'123456','mother_name':'Merlin','mother_mob':'54544','area':'Nerul','alternate_mob':'5555','email':'test@test.com','photo':'N' }]}";
                    string send_sms_str = "http://www.mysafetravel.net/sims/api/scldatav1.php?data={'cmd':'1','username':'hgc_admin','paswrd':'hgcadmin','data':[{'enroll_no':'" + routeobj.enroll_no + "','first_name':'" + routeobj.first_name + "','card_no':'" + routeobj.card_no + "','gender':'" + routeobj.gender + "' ,'class_id':'" + routeobj.class_id + "','section_id':'" + routeobj.section_id + "','roll_no':'" + routeobj.roll_no + "','dob':'" + routeobj.dob + "','trip_type':'" + routeobj.trip_type + "','pick_bs_no_id':'" + routeobj.pick_bs_no_id + "','pick_shift_id':'" + routeobj.pick_shift_id + "','pick_point_id':'" + routeobj.pick_point_id + "' ,'drop_bs_no_id':'" + routeobj.drop_bs_no_id + "' ,'drop_shift_id':'" + routeobj.drop_shift_id + "' ,'drop_point_id':'" + routeobj.drop_point_id + "','status':'" + routeobj.status + "' ,'is_transport':'" + routeobj.is_transport + "'}]}";

                    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(send_sms_str);
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    responseString = respStreamReader.ReadToEnd();
                    //items = JsonConvert.DeserializeObject<List<Sims081>>(responseString);
                    respStreamReader.Close();
                }

                return Request.CreateResponse(HttpStatusCode.OK, responseString);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


        [Route("getBusShiftDetailsSafeTravelAPI")]
        public HttpResponseMessage getBusShiftDetailsSafeTravelAPI()
        {
            List<Sims081> items = new List<Sims081>();
            try
            {
                string responseString = string.Empty;

                string send_sms_str = " http://mysafetravel.net/sims/api/scldatav1.php?data={'username':'hgc_admin','paswrd':'hgcadmin','cmd':'7','bref_id':'all','brno':'ALL'}";
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(send_sms_str);
                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                return Request.CreateResponse(HttpStatusCode.OK, responseString);

                //string url = "http://mysafetravel.net/sims/api/scldatav1.php?data={'username':'hgc_admin','paswrd':'hgcadmin','cmd':'7','bref_id':'all','brno':'ALL'}";
                //HttpClient httpClient = new HttpClient();
                //var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
                //var result = httpClient.PostAsync(url, content).Result;
                //return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }

        [Route("getPickPointDetailsSafeTravelAPI")]
        public HttpResponseMessage getPickPointDetailsSafeTravelAPI()
        {
            List<Sims081> items = new List<Sims081>();
            try
            {
                string responseString = string.Empty;
                // Get Pick and Drop Points
                string send_sms_str = " http://mysafetravel.net/sims/api/scldatav1.php?data={'username':'hgc_admin','paswrd':'hgcadmin','cmd':'5'}";
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(send_sms_str);
                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                return Request.CreateResponse(HttpStatusCode.OK, responseString);                
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
        }


    }
}