﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
//using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;
using SIMSAPI.Models.FEE;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportRoute")]
    public class TransportRouteController : ApiController
    {

        #region API For Transport Route MyRegion...

        [Route("getTransportRoute")]
        public HttpResponseMessage getTransportRoute()
        {
            List<Sims083> TransportRoute = new List<Sims083>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 vobj = new Sims083();
                            vobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            vobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            vobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            vobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            vobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            vobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            vobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            vobj.sims_transport_route_vehicle_code = dr["sims_transport_route_vehicle_code"].ToString();
                            vobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            vobj.sims_transport_route_driver_code = dr["sims_transport_route_driver_code"].ToString();
                            vobj.sims_driver_name = dr["sims_driver_name"].ToString();
                            vobj.sims_transport_route_caretaker_code = dr["sims_transport_route_caretaker_code"].ToString();
                            vobj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();
                            vobj.sims_transport_route_status = dr["sims_transport_route_status"].ToString().Equals("A") ? true : false;
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }

        [Route("CUDTransportRoute")]
        public HttpResponseMessage CUDTransportRoute(List<Sims083> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (Sims083 routeobj in data)
                    {
                        if (routeobj.sims_transport_route_short_name == "")
                        {
                            routeobj.sims_transport_route_short_name = null;
                        }
                        if (routeobj.sims_transport_route_name == "")
                        {
                            routeobj.sims_transport_route_name = null;
                        }
                        if (routeobj.sims_transport_route_vehicle_code == "")
                        {
                            routeobj.sims_transport_route_vehicle_code = null;
                        }
                        if (routeobj.sims_transport_route_driver_code == "")
                        {
                            routeobj.sims_transport_route_driver_code = null;
                        }
                        if (routeobj.sims_transport_route_caretaker_code == "")
                        {
                            routeobj.sims_transport_route_caretaker_code = null;
                        }
                        //int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_route_proc]",
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",routeobj.opr),
                                new SqlParameter("@sims_academic_year",routeobj.sims_academic_year),
                                new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                                new SqlParameter("@sims_transport_route_direction",routeobj.sims_transport_route_direction),
                                new SqlParameter("@sims_transport_route_vehicle_code", routeobj.sims_transport_route_vehicle_code),
                                new SqlParameter("@sims_transport_route_short_name",routeobj.sims_transport_route_short_name),
                                new SqlParameter("@sims_transport_route_name", routeobj.sims_transport_route_name),
                                new SqlParameter("@sims_transport_route_driver_code",routeobj.sims_transport_route_driver_code),
                                new SqlParameter("@sims_transport_route_caretaker_code", routeobj.sims_transport_route_caretaker_code),
                                new SqlParameter("@sims_transport_route_status",routeobj.sims_transport_route_status==true?"A":"I")
                            });

                        //                if (ins > 0)
                        //                {
                        //                    inserted = true;
                        //                }
                        //                else
                        //                {
                        //                    inserted = false;
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception x)
                        //    {
                        //        // Log.Error(x);
                        //        return Request.CreateResponse(HttpStatusCode.OK, x.Message);
                        //    }
                        //    return Request.CreateResponse(HttpStatusCode.OK, inserted);
                        //}

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("GetStudentFee1")]
        public HttpResponseMessage GetStudentFee1(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_student_fee_TRA]", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        //sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        [Route("GetStudentFeeNew")]
        public HttpResponseMessage GetStudentFeeNew(string data)
        {
            FeeDetails sf = Newtonsoft.Json.JsonConvert.DeserializeObject<FeeDetails>(data);
            List<StudentFee> stdFee = new List<StudentFee>();
            List<SqlParameter> lst = new List<SqlParameter>();
            lst.Add(new SqlParameter("@OPR", "G"));
            lst.Add(new SqlParameter("@ENROLL", sf.StudEnroll));
            lst.Add(new SqlParameter("@CURR", sf.StudCurr));
            lst.Add(new SqlParameter("@ACADEMIC", sf.StudAcademic));
            lst.Add(new SqlParameter("@FEE_NUMBER", sf.StudFeeNumber));
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("dbo.sims_student_fee_TRA1New", lst);
                    while (dr.Read())
                    {
                        StudentFee sfe = new StudentFee();
                        sfe.FeeDetails = new List<FeeDetails>();
                        sfe.OtherFeeDetails = new List<OtherFee>();
                        sfe.StudCurr = dr["sims_cur_code"].ToString();
                        sfe.StudAcademic = dr["sims_academic_year"].ToString();
                        sfe.StudGrade = dr["sims_grade_code"].ToString();
                        sfe.StudSection = dr["sims_section_code"].ToString();
                        sfe.StudGradeName = dr["sims_grade_name_en"].ToString();
                        sfe.StudSectionName = dr["sims_section_name_en"].ToString();
                        sfe.StudDName = dr["STUDENTNAME"].ToString();
                        sfe.StudDEnroll = dr["sims_enroll_number"].ToString();
                        sfe.ParentID = dr["sims_sibling_parent_number"].ToString();
                        sfe.ParentName = dr["ParentName"].ToString();
                        sfe.PayingAgentName = dr["PANAME"].ToString();
                        try
                        {
                            sfe.ParentMobileNo = dr["ParentMobileNo"].ToString();
                            sfe.ParentEmpID = dr["ParentEmpID"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                        stdFee.Add(sfe);
                    }
                    dr.NextResult();
                    while (dr.Read())
                    {
                        OtherFee of = new OtherFee();
                        string str = dr["sims_enroll_number"].ToString();
                        of.OF_AMT = dr["sims_fee_amount"].ToString();
                        of.OF_Fee_code = dr["sims_fee_code"].ToString();
                        of.OF_Fee_cat = dr["sims_fee_category"].ToString();
                        of.OF_Desc = dr["sims_fee_code_description"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == str);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).OtherFeeDetails.Add(of);
                        }
                    }
                    dr.NextResult();
                    int i = 0;
                    while (dr.Read())
                    {
                        FeeDetails fd = new FeeDetails();
                        fd.SrNo = (++i) + "";
                        fd.StudEnroll = dr["sims_enroll_number"].ToString();
                        fd.StudCurr = dr["sims_fee_cur_code"].ToString();
                        fd.StudAcademic = dr["sims_fee_academic_year"].ToString();
                        fd.StudGrade = dr["sims_fee_grade_code"].ToString();
                        fd.StudSection = dr["sims_fee_section_code"].ToString();
                        fd.StudFeeNumber = dr["sims_fee_number"].ToString();
                        fd.StudFeeCode = dr["sims_fee_code"].ToString();
                        fd.StudFeeDesc = dr["sims_fee_code_description"].ToString();
                        fd.StudFeeCategoryCode = dr["sims_fee_category"].ToString();
                        //fd.StudFeeCategoryDesc = dr[""].ToString();
                        fd.StudFeeFrequencyCode = dr["sims_fee_frequency"].ToString();
                        //fd.StudFeeFrequencyDesc = dr[""].ToString();
                        fd.StudFeePeriodNo = dr["periodNo"].ToString();
                        fd.StudFeePeriodDesc = dr["sims_fee_period"].ToString();
                        fd.StudFeeInstallmentMode = dr["installment_mode"].ToString();
                        fd.StudFeeInstallmentMinAMT = dr["installment_min_amount"].ToString();
                        //fd.StudTotalFeeAMT = dr[""].ToString();
                        fd.StudExpectedFeeAMT = dr["expected_Fee"].ToString();

                        fd.StudConcessionAMT = dr["conc_Fee"].ToString();
                        fd.StudConcessionPaidAMT = dr["conc_Fee_Paid"].ToString();

                        fd.StudTotalPaidAMT = dr["paid_Fee"].ToString();
                        fd.StudBalanceAMT = dr["bal_Fee"].ToString();
                        fd.StudBalancePayingAMT = fd.StudBalanceAMT;
                        fd.payingAgent = dr["isPayingAgent"].ToString();
                        fd.StudInvoiceNO = dr["InvoiceNo"].ToString();
                        fd.StudInvoiceTransNO = dr["pa_trans_number"].ToString();
                        fd.StudInvoiceTransLineNO = dr["pa_trans_line_no"].ToString();
                        var v = stdFee.Where(q => q.StudDEnroll == fd.StudEnroll);
                        if (v.Count() > 0)
                        {
                            v.ElementAt(0).FeeDetails.Add(fd);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, stdFee);
        }

        #endregion

        #region API For Transport Route MyRegion...

        [Route("getCurrentYear")]
        public HttpResponseMessage getCurrentYear()
        {
            List<SimTCA> TransportRoute = new List<SimTCA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTCA vobj = new SimTCA();
                            vobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            vobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }


        [Route("getAdvanceYear")]
        public HttpResponseMessage getAdvanceYear()
        {
            List<SimTCA> TransportRoute = new List<SimTCA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "L")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTCA vobj = new SimTCA();
                            vobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            vobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }

        [Route("getModuleName")]
        public HttpResponseMessage getModuleName()
        {
            List<SimTCA> TransportRoute = new List<SimTCA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTCA vobj = new SimTCA();
                            vobj.sims_module_code = dr["comn_mod_code"].ToString();
                            vobj.sims_module_name = dr["comn_mod_name_en"].ToString();
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }

        [Route("getApplicationName")]
        public HttpResponseMessage getApplicationName(string module_code)
        {
            List<SimTCA> TransportRoute = new List<SimTCA>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B"),
                            new SqlParameter("@sims_module_code", module_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTCA vobj = new SimTCA();
                            vobj.sims_application_code = dr["comn_appl_code"].ToString();
                            vobj.sims_application_name = dr["comn_appl_name_en"].ToString();
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }

        [Route("CUDTransportConfigure")]
        public HttpResponseMessage CUDTransportConfigure(List<SimTCA> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    foreach (SimTCA routeobj in data)
                    {
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",routeobj.opr),
                                new SqlParameter("@sims_academic_year",routeobj.sims_academic_year),
                                new SqlParameter("@sims_module_code", routeobj.sims_module_code),
                                new SqlParameter("@sims_application_code",routeobj.sims_application_code),
                            });


                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }

            catch (Exception p)
            {
                return Request.CreateResponse(HttpStatusCode.OK, p.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [Route("getAdvanceTransportRoute")]
        public HttpResponseMessage getAdvanceTransportRoute(string ac_year, string module_code, string appl_code)
        {
            List<Sims083> TransportRoute = new List<Sims083>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_configure_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "C"),
                            new SqlParameter("@sims_academic_year", ac_year),
                            new SqlParameter("@sims_module_code", module_code),
                            new SqlParameter("@sims_application_code", appl_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 vobj = new Sims083();
                            vobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            vobj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            //vobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            //vobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            vobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            //vobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            vobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            //vobj.sims_transport_route_vehicle_code = dr["sims_transport_route_vehicle_code"].ToString();
                            //vobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            //vobj.sims_transport_route_driver_code = dr["sims_transport_route_driver_code"].ToString();
                            //vobj.sims_driver_name = dr["sims_driver_name"].ToString();
                            //vobj.sims_transport_route_caretaker_code = dr["sims_transport_route_caretaker_code"].ToString();
                            //vobj.sims_caretaker_name = dr["sims_caretaker_name"].ToString();
                            vobj.sims_transport_route_status = dr["sims_transport_route_status"].ToString().Equals("A") ? true : false;
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }


        #endregion

    }
}