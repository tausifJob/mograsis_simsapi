﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportStop")]
    public class TransportStopController:ApiController
    {
        #region Api For Transport Stop...
        [Route("getTransportStop")]
        public HttpResponseMessage getTransportStop()
        {
            List<Sims084> TransportStop = new List<Sims084>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_stop_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims084 obj = new Sims084();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            obj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            obj.sims_transport_stop_landmark = dr["sims_transport_stop_landmark"].ToString();
                            obj.sims_transport_stop_lat = dr["sims_transport_stop_lat"].ToString();
                            obj.sims_transport_stop_long = dr["sims_transport_stop_long"].ToString();
                            obj.sims_transport_stop_status = dr["sims_transport_stop_status"].ToString().Equals("A") ? true : false;
                            TransportStop.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportStop);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportStop);
        }

        [Route("CUDTransportStop")]
        public HttpResponseMessage CUDTransportStop(List<Sims084> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims084 simobj in data)
                    {
                        if (simobj.sims_transport_stop_name == "")
                        {
                            simobj.sims_transport_stop_name = null;
                        }
                        if (simobj.sims_transport_stop_landmark == "")
                        {
                            simobj.sims_transport_stop_landmark = null;
                        }
                        if (simobj.sims_transport_stop_lat == "")
                        {
                            simobj.sims_transport_stop_lat = null;
                        }
                        if (simobj.sims_transport_stop_long == "")
                        {
                            simobj.sims_transport_stop_long = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_stop_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simobj.opr),
                                new SqlParameter("@sims_academic_year",simobj.sims_academic_year),
                                new SqlParameter("@sims_transport_stop_code", simobj.sims_transport_stop_code),
                                new SqlParameter("@sims_transport_stop_name",simobj.sims_transport_stop_name),
                                new SqlParameter("@sims_transport_stop_landmark", simobj.sims_transport_stop_landmark),
                                new SqlParameter("@sims_transport_stop_lat",simobj.sims_transport_stop_lat),
                                new SqlParameter("@sims_transport_stop_long", simobj.sims_transport_stop_long),
                                new SqlParameter("@sims_transport_stop_status",simobj.sims_transport_stop_status==true?"A":"I")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

    }
}