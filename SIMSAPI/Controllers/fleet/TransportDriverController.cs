﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/TransportDriver")]
    public class TransportDriverController : ApiController
    {

        [Route("getTransportDriver")]
        public HttpResponseMessage getTransportDriver()
        {
            List<Sims088> TransportDriver = new List<Sims088>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_driver_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 obj = new Sims088();

                            obj.sims_driver_code = dr["sims_driver_code"].ToString();
                            obj.sims_driver_name = dr["sims_driver_name"].ToString();
                            obj.sims_employee_code = dr["sims_employee_code"].ToString();
                            obj.sims_employee_code_name = dr["sims_employee_code_name"].ToString();
                            obj.sims_user_code = dr["sims_user_code"].ToString();
                            obj.sims_driver_type = dr["sims_driver_type"].ToString();
                            obj.sims_driver_type_name = dr["sims_driver_type_name"].ToString();
                            obj.sims_driver_gender = dr["sims_driver_gender"].ToString();
                            obj.sims_driver_experience_years = dr["sims_driver_experience_years"].ToString();
                            obj.sims_driver_mobile_number1 = dr["sims_driver_mobile_number1"].ToString();
                            obj.sims_driver_mobile_number2 = dr["sims_driver_mobile_number2"].ToString();
                            obj.sims_driver_mobile_number3 = dr["sims_driver_mobile_number3"].ToString();
                            obj.sims_driver_address = dr["sims_driver_address"].ToString();
                            obj.sims_driver_img = dr["sims_driver_img"].ToString();
                            obj.sims_driver_driving_license_number = dr["sims_driver_driving_license_number"].ToString();
                            obj.sims_driver_license_place_of_issue = dr["sims_driver_license_place_of_issue"].ToString();
                            obj.sims_driver_license_vehicle_mode = dr["sims_driver_license_vehicle_mode"].ToString();
                            obj.sims_driver_license_vehicle_mode_name = dr["sims_driver_license_vehicle_mode_name"].ToString();
                            obj.sims_driver_license_vehicle_category = dr["sims_driver_license_vehicle_category"].ToString();
                            obj.sims_driver_license_vehicle_categoryname = dr["sims_driver_license_vehicle_categoryname"].ToString();
                            obj.sims_driver_visa_number = dr["sims_driver_visa_number"].ToString();
                            obj.sims_driver_visa_issuing_place = dr["sims_driver_visa_issuing_place"].ToString();
                            obj.sims_driver_visa_issuing_authority = dr["sims_driver_visa_issuing_authority"].ToString();
                            obj.sims_driver_visa_type = dr["sims_driver_visa_type"].ToString();
                            obj.sims_driver_national_id = dr["sims_driver_national_id"].ToString();
                            obj.sims_driver_status = dr["sims_driver_status"].ToString().Equals("A") ? true : false;
                            obj.sims_driver_email = dr["sims_driver_email"].ToString();
                            obj.comn_user_phone_number = dr["comn_user_phone_number"].ToString();
                            obj.sims_driver_dot_license_no = dr["sims_driver_dot_license_no"].ToString();

                            //try
                            //{
                            //    obj.sims_driver_date_of_birth = dr["sims_driver_date_of_birth"].ToString();
                            //    obj.sims_driver_license_issue_date = db.UIDDMMYYYYformat(dr["sims_driver_license_issue_date"].ToString());
                            //    obj.sims_driver_license_expiry_date = db.UIDDMMYYYYformat(dr["sims_driver_license_expiry_date"].ToString());
                            //    obj.sims_driver_visa_issue_date = db.UIDDMMYYYYformat(dr["sims_driver_visa_issue_date"].ToString());
                            //    obj.sims_driver_visa_expiry_date = db.UIDDMMYYYYformat(dr["sims_driver_visa_expiry_date"].ToString());
                            //    obj.sims_driver_national_id_issue_date = db.UIDDMMYYYYformat(dr["sims_driver_national_id_issue_date"].ToString());
                            //    obj.sims_driver_national_id_expiry_date = db.UIDDMMYYYYformat(dr["sims_driver_national_id_expiry_date"].ToString());
                            //    obj.sims_driver_dot_license_issue_date = db.UIDDMMYYYYformat(dr["sims_driver_dot_license_issue_date"].ToString());
                            //    obj.sims_driver_dot_license_expiry_date = db.UIDDMMYYYYformat(dr["sims_driver_dot_license_expiry_date"].ToString());
                            //}
                            //catch (Exception ex)
                            //{
                            //}

                            TransportDriver.Add(obj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportDriver);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportDriver);
        }

        [Route("CUDTransportDriver")]
        public HttpResponseMessage CUDTransportDriver(List<Sims088> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportDriver", simobj));

            bool inserted = false;
            string strMessage = "";
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims088 simobj in data)
                    {
                        if (simobj.sims_employee_code == "")
                        {
                            simobj.sims_employee_code = null;
                        }
                        if (simobj.sims_driver_email == "")
                        {
                            simobj.sims_driver_email = null;
                        }
                        if (simobj.sims_driver_date_of_birth == "")
                        {
                            simobj.sims_driver_date_of_birth = null;
                        }
                        if (simobj.sims_driver_license_issue_date == "")
                        {
                            simobj.sims_driver_license_issue_date = null;
                        }
                        if (simobj.sims_driver_license_expiry_date == "")
                        {
                            simobj.sims_driver_license_expiry_date = null;
                        }
                        if (simobj.sims_driver_visa_issue_date == "")
                        {
                            simobj.sims_driver_visa_issue_date = null;
                        }
                        if (simobj.sims_driver_visa_expiry_date == "")
                        {
                            simobj.sims_driver_visa_expiry_date = null;
                        }
                        if (simobj.sims_driver_national_id_issue_date == "")
                        {
                            simobj.sims_driver_national_id_issue_date = null;
                        }
                        if (simobj.sims_driver_national_id_expiry_date == "")
                        {
                            simobj.sims_driver_national_id_expiry_date = null;
                        }


                        if (simobj.sims_driver_gender == "")
                        {
                            simobj.sims_driver_gender = null;
                        }
                        if (simobj.sims_driver_img == "")
                        {
                            simobj.sims_driver_img = null;
                        }
                        if (simobj.sims_driver_experience_years == "")
                        {
                            simobj.sims_driver_experience_years = null;
                        }
                        if (simobj.sims_driver_mobile_number1 == "")
                        {
                            simobj.sims_driver_mobile_number1 = null;
                        }
                        if (simobj.sims_driver_mobile_number2 == "")
                        {
                            simobj.sims_driver_mobile_number2 = null;
                        }

                        if (simobj.sims_driver_mobile_number3 == "")
                        {
                            simobj.sims_driver_mobile_number3 = null;
                        }
                        if (simobj.sims_driver_driving_license_number == "")
                        {
                            simobj.sims_driver_driving_license_number = null;
                        }
                        if (simobj.sims_driver_license_place_of_issue == "")
                        {
                            simobj.sims_driver_license_place_of_issue = null;
                        }
                        if (simobj.sims_driver_license_vehicle_category == "")
                        {
                            simobj.sims_driver_license_vehicle_category = null;
                        }
                        if (simobj.sims_driver_license_vehicle_mode == "")
                        {
                            simobj.sims_driver_license_vehicle_mode = null;
                        }

                        if (simobj.sims_driver_visa_number == "")
                        {
                            simobj.sims_driver_visa_number = null;
                        }
                        if (simobj.sims_driver_visa_issuing_place == "")
                        {
                            simobj.sims_driver_visa_issuing_place = null;
                        }
                        if (simobj.sims_driver_visa_issuing_authority == "")
                        {
                            simobj.sims_driver_visa_issuing_authority = null;
                        }
                        if (simobj.sims_driver_visa_type == "")
                        {
                            simobj.sims_driver_visa_type = null;
                        }
                        if (simobj.sims_driver_national_id == "")
                        {
                            simobj.sims_driver_national_id = null;
                        }

                        if (simobj.sims_driver_address == "")
                        {
                            simobj.sims_driver_address = null;
                        }
                        if (simobj.comn_user_phone_number == "")
                        {
                            simobj.comn_user_phone_number = null;
                        }

                        if (simobj.sims_driver_dot_license_no == "")
                        {
                            simobj.sims_driver_dot_license_no = null;
                        }

                        if (simobj.sims_driver_dot_license_issue_date == "")
                        {
                            simobj.sims_driver_dot_license_issue_date = null;
                        }

                        if (simobj.sims_driver_dot_license_expiry_date == "")
                        {
                            simobj.sims_driver_dot_license_expiry_date = null;
                        }
                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_driver_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simobj.opr),
                            new SqlParameter("@sims_driver_code",simobj.sims_driver_code),
                            new SqlParameter("@sims_driver_name", simobj.sims_driver_name),
                            new SqlParameter("@sims_driver_gender", simobj.sims_driver_gender),
                            new SqlParameter("@sims_driver_experience_years",simobj.sims_driver_experience_years),
                            new SqlParameter("@sims_user_code", simobj.sims_user_code),
                             new SqlParameter("@sims_driver_img",simobj.sims_driver_img),
                            new SqlParameter("@sims_employee_code",simobj.sims_employee_code),

                            new SqlParameter("@sims_driver_mobile_number1", simobj.sims_driver_mobile_number1),
                            new SqlParameter("@sims_driver_mobile_number2",simobj.sims_driver_mobile_number2),
                            new SqlParameter("@sims_driver_mobile_number3", simobj.sims_driver_mobile_number3),
                            new SqlParameter("@sims_driver_address",simobj.sims_driver_address),
                            new SqlParameter("@sims_driver_license_vehicle_category",simobj.sims_driver_license_vehicle_category),
                            new SqlParameter("@sims_driver_type", simobj.sims_driver_type),
                            new SqlParameter("@sims_driver_license_vehicle_mode",simobj.sims_driver_license_vehicle_mode),
                            new SqlParameter("@sims_driver_driving_license_number",simobj.sims_driver_driving_license_number),
                            new SqlParameter("@sims_driver_license_place_of_issue",simobj.sims_driver_license_place_of_issue),
                            new SqlParameter("@sims_driver_visa_number",simobj.sims_driver_visa_number),
                            new SqlParameter("@sims_driver_visa_issuing_place",simobj.sims_driver_visa_issuing_place),
                            new SqlParameter("@sims_driver_visa_issuing_authority",simobj.sims_driver_visa_issuing_authority),
                            new SqlParameter("@sims_driver_visa_type", simobj.sims_driver_visa_type),
                            new SqlParameter("@sims_driver_national_id", simobj.sims_driver_national_id),
                            new SqlParameter("@sims_driver_status",simobj.sims_driver_status==true?"A":"I"),
                            new SqlParameter("@comn_user_email", simobj.sims_driver_email),
                            new SqlParameter("@comn_user_phone_number", simobj.comn_user_phone_number),
                            new SqlParameter("@sims_driver_dot_license_no", simobj.sims_driver_dot_license_no),
                            
                            new SqlParameter("@sims_driver_date_of_birth", db.DBYYYYMMDDformat(simobj.sims_driver_date_of_birth)),
                            new SqlParameter("@sims_driver_license_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_license_issue_date)),
                            new SqlParameter("@sims_driver_license_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_license_expiry_date)),
                            new SqlParameter("@sims_driver_visa_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_visa_issue_date)),
                            new SqlParameter("@sims_driver_visa_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_visa_expiry_date)),
                            new SqlParameter("@sims_driver_national_id_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_national_id_issue_date)),
                            new SqlParameter("@sims_driver_national_id_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_national_id_expiry_date)),
                            new SqlParameter("@sims_driver_dot_license_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_issue_date)),
                            new SqlParameter("@sims_driver_dot_license_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_expiry_date))

                        });

                        if (dr.Read())
                        {
                            strMessage = dr["sims_msg"].ToString();
                        }
                        else
                        {
                            strMessage = "Record Not Insert....";
                        }
                        dr.Close();
                        
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, strMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, strMessage);
        }

        [Route("getPaysDesignation")]
        public HttpResponseMessage getPaysDesignation()
        {
            List<Sims088> Designation_list = new List<Sims088>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_driver_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "P")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims088 obj = new Sims088();
                            obj.dg_code = dr["dg_code"].ToString();
                            obj.dg_desc = dr["dg_desc"].ToString();
                            Designation_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, Designation_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, Designation_list);
        }

        [Route("getAllEmpDriverName")]
        public HttpResponseMessage getAllEmpDriverName(string desg_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getAllEmpDriverName(),PARAMETERS :: NO";
            //Log.Debug(string.Format(debug, "FLEET", "getAllEmpDriver"));

            List<Sims087> emp_driver = new List<Sims087>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "E"),
                              new SqlParameter("@dg_code", desg_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            simsobj.sims_employee_code_name = dr["sims_employee_name"].ToString();
                            simsobj.emp_full_name = dr["emp_full_name"].ToString();
                            simsobj.sims_employee_code = dr["em_login_code"].ToString();
                            emp_driver.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_driver);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_driver);
            }
        }

        [Route("showwDataAsperEmployee")]
        public HttpResponseMessage showwDataAsperEmployee(string employeeCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : showwDataAsperEmployee(),PARAMETERS :: NO";
            List<Sims087> emp_driver = new List<Sims087>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_driver_proc",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "F"),
                              new SqlParameter("@sims_employee_code", employeeCode)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims087 simsobj = new Sims087();
                            simsobj.em_sex = dr["em_sex"].ToString();
                            simsobj.em_mobile = dr["em_mobile"].ToString();
                            simsobj.addressEmployee = dr["addressEmployee"].ToString();
                            simsobj.em_date_of_birth = dr["em_date_of_birth"].ToString();
                            simsobj.em_visa_number = dr["em_visa_number"].ToString();
                            simsobj.em_visa_issue_date = dr["em_visa_issue_date"].ToString();
                            simsobj.em_visa_expiry_date = dr["em_visa_expiry_date"].ToString();
                            simsobj.em_visa_issuing_place = dr["em_visa_issuing_place"].ToString();
                            simsobj.em_visa_issuing_authority = dr["em_visa_issuing_authority"].ToString();
                            simsobj.em_visa_type = dr["em_visa_type"].ToString();
                            simsobj.em_national_id = dr["em_national_id"].ToString();
                            simsobj.em_national_id_issue_date = dr["em_national_id_issue_date"].ToString();
                            simsobj.em_national_id_expiry_date = dr["em_national_id_expiry_date"].ToString();                            
                            emp_driver.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, emp_driver);
            }
            catch (Exception e)
            {                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, emp_driver);
            }
        }

        [Route("updateTransportDriver")]
        public HttpResponseMessage updateTransportDriver(List<Sims088> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportCareTaker", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims088 simobj in data)
                    {
                        if (simobj.sims_driver_date_of_birth == "")
                        {
                            simobj.sims_driver_date_of_birth = null;
                        }
                        if (simobj.sims_driver_license_issue_date == "")
                        {
                            simobj.sims_driver_license_issue_date = null;
                        }
                        if (simobj.sims_driver_license_expiry_date == "")
                        {
                            simobj.sims_driver_license_expiry_date = null;
                        }
                        if (simobj.sims_driver_visa_issue_date == "")
                        {
                            simobj.sims_driver_visa_issue_date = null;
                        }
                        if (simobj.sims_driver_visa_expiry_date == "")
                        {
                            simobj.sims_driver_visa_expiry_date = null;
                        }
                        if (simobj.sims_driver_national_id_issue_date == "")
                        {
                            simobj.sims_driver_national_id_issue_date = null;
                        }
                        if (simobj.sims_driver_national_id_expiry_date == "")
                        {
                            simobj.sims_driver_national_id_expiry_date = null;
                        }
                        if (simobj.comn_user_phone_number == "")
                        {
                            simobj.comn_user_phone_number = null;
                        }
                        if (simobj.sims_driver_dot_license_no == "")
                        {
                            simobj.sims_driver_dot_license_no = null;
                        }

                        if (simobj.sims_driver_dot_license_issue_date == "")
                        {
                            simobj.sims_driver_dot_license_issue_date = null;
                        }

                        if (simobj.sims_driver_dot_license_expiry_date == "")
                        {
                            simobj.sims_driver_dot_license_expiry_date = null;
                        }
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_driver_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simobj.opr),
                                new SqlParameter("@sims_driver_code",simobj.sims_driver_code),
                                new SqlParameter("@sims_driver_name", simobj.sims_driver_name),
                                new SqlParameter("@sims_driver_gender", simobj.sims_driver_gender),
                                new SqlParameter("@sims_driver_experience_years",simobj.sims_driver_experience_years),
                                new SqlParameter("@sims_user_code", simobj.sims_user_code),
                                new SqlParameter("@sims_driver_img",simobj.sims_driver_img),
                                new SqlParameter("@sims_employee_code",simobj.sims_employee_code),
                                new SqlParameter("@sims_driver_mobile_number1", simobj.sims_driver_mobile_number1),
                                new SqlParameter("@sims_driver_mobile_number2",simobj.sims_driver_mobile_number2),
                                new SqlParameter("@sims_driver_mobile_number3", simobj.sims_driver_mobile_number3),
                                new SqlParameter("@sims_driver_address",simobj.sims_driver_address),
                                new SqlParameter("@sims_driver_license_vehicle_category",simobj.sims_driver_license_vehicle_category),
                                new SqlParameter("@sims_driver_type", simobj.sims_driver_type),
                                new SqlParameter("@sims_driver_license_vehicle_mode",simobj.sims_driver_license_vehicle_mode),
                                new SqlParameter("@sims_driver_driving_license_number",simobj.sims_driver_driving_license_number),
                                new SqlParameter("@sims_driver_license_place_of_issue",simobj.sims_driver_license_place_of_issue),
                                new SqlParameter("@sims_driver_visa_number",simobj.sims_driver_visa_number),
                                new SqlParameter("@sims_driver_visa_issuing_place",simobj.sims_driver_visa_issuing_place),
                                new SqlParameter("@sims_driver_visa_issuing_authority",simobj.sims_driver_visa_issuing_authority),
                                new SqlParameter("@sims_driver_visa_type", simobj.sims_driver_visa_type),
                                new SqlParameter("@sims_driver_national_id", simobj.sims_driver_national_id),
                                new SqlParameter("@sims_driver_dot_license_no", simobj.sims_driver_dot_license_no),
                                new SqlParameter("@sims_driver_status",simobj.sims_driver_status==true?"A":"I"),
                                new SqlParameter("@comn_user_email", simobj.sims_driver_email),
                                new SqlParameter("@comn_user_phone_number", simobj.comn_user_phone_number),
                                
                                new SqlParameter("@sims_driver_dot_license_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_issue_date)),
                                new SqlParameter("@sims_driver_dot_license_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_expiry_date)),
                                new SqlParameter("@sims_driver_date_of_birth",          db.DBYYYYMMDDformat(simobj.sims_driver_date_of_birth)),
                                new SqlParameter("@sims_driver_license_issue_date",     db.DBYYYYMMDDformat(simobj.sims_driver_license_issue_date)),
                                new SqlParameter("@sims_driver_license_expiry_date",    db.DBYYYYMMDDformat(simobj.sims_driver_license_expiry_date)),
                                new SqlParameter("@sims_driver_visa_issue_date",        db.DBYYYYMMDDformat(simobj.sims_driver_visa_issue_date)),
                                new SqlParameter("@sims_driver_visa_expiry_date",       db.DBYYYYMMDDformat(simobj.sims_driver_visa_expiry_date)),
                                new SqlParameter("@sims_driver_national_id_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_national_id_issue_date)),
                                new SqlParameter("@sims_driver_national_id_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_national_id_expiry_date))
                                
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("TransportDriverdelete")]
        public HttpResponseMessage TransportDriverdelete(List<Sims088> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportCareTaker", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims088 simobj in data)
                    {
                        if (simobj.sims_driver_date_of_birth == "")
                        {
                            simobj.sims_driver_date_of_birth = null;
                        }
                        if (simobj.sims_driver_license_issue_date == "")
                        {
                            simobj.sims_driver_license_issue_date = null;
                        }
                        if (simobj.sims_driver_license_expiry_date == "")
                        {
                            simobj.sims_driver_license_expiry_date = null;
                        }
                        if (simobj.sims_driver_visa_issue_date == "")
                        {
                            simobj.sims_driver_visa_issue_date = null;
                        }
                        if (simobj.sims_driver_visa_expiry_date == "")
                        {
                            simobj.sims_driver_visa_expiry_date = null;
                        }
                        if (simobj.sims_driver_national_id_issue_date == "")
                        {
                            simobj.sims_driver_national_id_issue_date = null;
                        }
                        if (simobj.sims_driver_national_id_expiry_date == "")
                        {
                            simobj.sims_driver_national_id_expiry_date = null;
                        }
                        if (simobj.comn_user_phone_number == "")
                        {
                            simobj.comn_user_phone_number = null;
                        }
                        if (simobj.sims_driver_dot_license_no == "")
                        {
                            simobj.sims_driver_dot_license_no = null;
                        }

                        if (simobj.sims_driver_dot_license_issue_date == "")
                        {
                            simobj.sims_driver_dot_license_issue_date = null;
                        }

                        if (simobj.sims_driver_dot_license_expiry_date == "")
                        {
                            simobj.sims_driver_dot_license_expiry_date = null;
                        }
                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_driver_proc]",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@opr",simobj.opr),
                                new SqlParameter("@sims_driver_code",simobj.sims_driver_code),
                                new SqlParameter("@sims_driver_name", simobj.sims_driver_name),
                                new SqlParameter("@sims_driver_gender", simobj.sims_driver_gender),
                                new SqlParameter("@sims_driver_experience_years",simobj.sims_driver_experience_years),
                                new SqlParameter("@sims_user_code", simobj.sims_user_code),
                                new SqlParameter("@sims_driver_img",simobj.sims_driver_img),
                                new SqlParameter("@sims_employee_code",simobj.sims_employee_code),
                                new SqlParameter("@sims_driver_mobile_number1", simobj.sims_driver_mobile_number1),
                                new SqlParameter("@sims_driver_mobile_number2",simobj.sims_driver_mobile_number2),
                                new SqlParameter("@sims_driver_mobile_number3", simobj.sims_driver_mobile_number3),
                                new SqlParameter("@sims_driver_address",simobj.sims_driver_address),
                                new SqlParameter("@sims_driver_license_vehicle_category",simobj.sims_driver_license_vehicle_category),
                                new SqlParameter("@sims_driver_type", simobj.sims_driver_type),
                                new SqlParameter("@sims_driver_license_vehicle_mode",simobj.sims_driver_license_vehicle_mode),
                                new SqlParameter("@sims_driver_driving_license_number",simobj.sims_driver_driving_license_number),
                                new SqlParameter("@sims_driver_license_place_of_issue",simobj.sims_driver_license_place_of_issue),
                                new SqlParameter("@sims_driver_visa_number",simobj.sims_driver_visa_number),
                                new SqlParameter("@sims_driver_visa_issuing_place",simobj.sims_driver_visa_issuing_place),
                                new SqlParameter("@sims_driver_visa_issuing_authority",simobj.sims_driver_visa_issuing_authority),
                                new SqlParameter("@sims_driver_visa_type", simobj.sims_driver_visa_type),
                                new SqlParameter("@sims_driver_national_id", simobj.sims_driver_national_id),
                                new SqlParameter("@sims_driver_dot_license_no", simobj.sims_driver_dot_license_no),
                                new SqlParameter("@sims_driver_status",simobj.sims_driver_status==true?"A":"I"),
                                new SqlParameter("@comn_user_email", simobj.sims_driver_email),
                                new SqlParameter("@comn_user_phone_number", simobj.comn_user_phone_number),

                                new SqlParameter("@sims_driver_date_of_birth",          db.DBYYYYMMDDformat(simobj.sims_driver_date_of_birth)),
                                new SqlParameter("@sims_driver_license_issue_date",     db.DBYYYYMMDDformat(simobj.sims_driver_license_issue_date)),
                                new SqlParameter("@sims_driver_license_expiry_date",    db.DBYYYYMMDDformat(simobj.sims_driver_license_expiry_date)),
                                new SqlParameter("@sims_driver_visa_issue_date",        db.DBYYYYMMDDformat(simobj.sims_driver_visa_issue_date)),
                                new SqlParameter("@sims_driver_visa_expiry_date",       db.DBYYYYMMDDformat(simobj.sims_driver_visa_expiry_date)),
                                new SqlParameter("@sims_driver_national_id_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_national_id_issue_date)),
                                new SqlParameter("@sims_driver_national_id_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_national_id_expiry_date)),
                                new SqlParameter("@sims_driver_dot_license_issue_date", db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_issue_date)),
                                new SqlParameter("@sims_driver_dot_license_expiry_date",db.DBYYYYMMDDformat(simobj.sims_driver_dot_license_expiry_date))

                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }
    
    }
}