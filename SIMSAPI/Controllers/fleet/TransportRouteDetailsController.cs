﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportRouteDetails")]
    public class TransportRouteDetailsController : ApiController
    {

        [Route("getTransportRoute")]
        public HttpResponseMessage getTransportRoute()
        {
            List<Sims083> TransportRoute = new List<Sims083>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 vobj = new Sims083();
                            vobj.sims_academic_year = dr["sims_academic_year"].ToString();
                            vobj.sims_academic_year_description = dr["sims_academic_year"].ToString();
                            vobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            vobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            vobj.sims_transport_route_vehicle_code = dr["sims_transport_route_vehicle_code"].ToString();
                            vobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            vobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            vobj.sims_transport_route_driver_code = dr["sims_transport_route_driver_code"].ToString();
                            vobj.sims_transport_route_caretaker_code = dr["sims_transport_route_caretaker_code"].ToString();
                            vobj.sims_transport_route_status = dr["sims_transport_route_status"].ToString().Equals("A") ? true : false;
                            TransportRoute.Add(vobj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportRoute);
        }

        [Route("CUDTransportRoute")]
        public HttpResponseMessage CUDTransportRoute(Sims083 routeobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_route_proc]",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",routeobj.opr),
                            new SqlParameter("@sims_academic_year",routeobj.sims_academic_year),
                            new SqlParameter("@sims_transport_route_code", routeobj.sims_transport_route_code),
                            new SqlParameter("@sims_transport_route_direction_name",routeobj.sims_transport_route_direction),
                            new SqlParameter("@sims_transport_route_vehicle_codename", routeobj.sims_transport_route_vehicle_code),
                            new SqlParameter("@sims_transport_route_short_name",routeobj.sims_transport_route_short_name),
                            new SqlParameter("@sims_transport_route_name", routeobj.sims_transport_route_name),
                            new SqlParameter("@sims_transport_route_driver_codename",routeobj.sims_transport_route_driver_code),
                            new SqlParameter("@sims_transport_route_caretaker_codename", routeobj.sims_transport_route_caretaker_code),
                            new SqlParameter("@sims_transport_route_status",routeobj.sims_transport_route_status==true?"A":"I")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getTransportStop")]
        public HttpResponseMessage getTransportStop()
        {
            List<Sims084> TransportStop = new List<Sims084>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_stop_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims084 obj = new Sims084();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year"].ToString();
                            obj.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            obj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            obj.sims_transport_stop_landmark = dr["sims_transport_stop_landmark"].ToString();
                            obj.sims_transport_stop_lat = dr["sims_transport_stop_lat"].ToString();
                            obj.sims_transport_stop_long = dr["sims_transport_stop_long"].ToString();
                            obj.sims_transport_stop_status = dr["sims_transport_stop_status"].ToString().Equals("A") ? true : false;
                            TransportStop.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportStop);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportStop);
        }

        [Route("CUDTransportStop")]
        public HttpResponseMessage CUDTransportStop(Sims084 simobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims.sims_transport_stop_proc",
                        new List<SqlParameter>()
                        {
                            new SqlParameter("@opr",simobj.opr),
                            new SqlParameter("@sims_academic_year",simobj.sims_academic_year),
                            new SqlParameter("@sims_transport_stop_code", simobj.sims_transport_stop_code),
                            new SqlParameter("@sims_transport_stop_name",simobj.sims_transport_stop_name),
                            new SqlParameter("@sims_transport_stop_landmark", simobj.sims_transport_stop_landmark),
                            new SqlParameter("@sims_transport_stop_lat",simobj.sims_transport_stop_lat),
                            new SqlParameter("@sims_transport_stop_long", simobj.sims_transport_stop_long),
                            new SqlParameter("@sims_transport_stop_status",simobj.sims_transport_stop_status==true?"A":"I")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }

                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getRouteStop")]
        public HttpResponseMessage getRouteStop()
        {
            List<Sims085> RouteStop = new List<Sims085>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 objd = new Sims085();
                            objd.sims_academic_year = dr["sims_academic_year"].ToString();
                            objd.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            objd.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            objd.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            objd.sims_transport_route_stop_code = dr["sims_transport_route_stop_code"].ToString();
                            objd.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            objd.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            objd.sims_transport_route_stop_expected_time = dr["sims_transport_route_stop_expected_time"].ToString();
                            objd.sims_transport_route_stop_waiting_time = dr["sims_transport_route_stop_waiting_time"].ToString();
                            objd.sims_transport_route_stop_status = dr["sims_transport_route_stop_status"].ToString().Equals("A") ? true : false;
                            RouteStop.Add(objd);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, RouteStop);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, RouteStop);
        }

        [Route("CUDRoutetStop")]
        public HttpResponseMessage CUDRoutetStop(Sims085 rsobj)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDRoutetStop", rsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    int ins = db.ExecuteStoreProcedureforInsert("sims.sims_transport_route_stop_proc",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",rsobj.opr),
                          new SqlParameter("@sims_academic_year",rsobj.sims_academic_year),
                          new SqlParameter("@sims_transport_route_code", rsobj.sims_transport_route_code),//10,
                          new SqlParameter("@sims_transport_route_stop_code",rsobj.sims_transport_route_stop_code),
                          new SqlParameter("@sims_transport_route_direction", rsobj.sims_transport_route_direction),//01,
                          new SqlParameter("@sims_transport_route_stop_expected_time",rsobj.sims_transport_route_stop_expected_time),
                          new SqlParameter("@sims_transport_route_stop_waiting_time", rsobj.sims_transport_route_stop_waiting_time),
                          new SqlParameter("@sims_transport_route_stop_status",rsobj.sims_transport_route_stop_status==true?"A":"I")
                        });

                    if (ins > 0)
                    {
                        inserted = true;
                    }
                    else
                    {
                        inserted = false;
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, inserted);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

    }
}