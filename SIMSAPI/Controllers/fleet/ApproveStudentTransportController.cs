﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
//using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.fleet
{
    [RoutePrefix("api/ApproveStudentTransportDetails")]
    public class ApproveStudentTransportController:ApiController
    {

        #region API FOR APPROVE STUDENT TRANSPORT...

        [Route("getApproveStudentTransport")]
        public HttpResponseMessage getApproveStudentTransport(string cur_code, string acayear, string grade, string sec_code, string bus_code)
        {
            if (grade == "undefined")
                grade = null;
            if (sec_code == "undefined")
                sec_code = null;
            if (bus_code == "undefined")
                bus_code = null;
            List<Sims519> ApproveStudent = new List<Sims519>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_application_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_vehicle_code", bus_code)
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims519 vobj = new Sims519();

                            vobj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            vobj.sims_transport_enroll_number = dr["sims_transport_enroll_number"].ToString();
                            vobj.sims_student_name = dr["sims_student_name"].ToString();
                            vobj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            vobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            vobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            vobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            vobj.C_direction = dr["C_direction"].ToString();
                            vobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            vobj.sims_transport_route_student_request_by = dr["sims_transport_route_student_request_by"].ToString();
                            //vobj.sims_transport_route_student_request_date = db.UIDDMMYYYYformat(dr["sims_transport_route_student_request_date"].ToString());
                            vobj.sims_transport_route_student_request_date = dr["sims_transport_route_student_request_date"].ToString();
                            vobj.sims_transport_route_approve_by = dr["sims_transport_route_approve_by"].ToString();
                            //vobj.sims_transport_route_approve_date = db.UIDDMMYYYYformat(dr["sims_transport_route_approve_date"].ToString());
                            vobj.sims_transport_route_approve_date = dr["sims_transport_route_approve_date"].ToString();
                            vobj.sims_transport_route_reject_by = dr["sims_transport_route_reject_by"].ToString();
                            vobj.sims_transport_route_reject_reason = dr["sims_transport_route_reject_reason"].ToString();
                            //vobj.sims_transport_route_rejecte_date = db.UIDDMMYYYYformat(dr["sims_transport_route_rejecte_date"].ToString());
                            vobj.sims_transport_route_rejecte_date = dr["sims_transport_route_rejecte_date"].ToString();
                            vobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            vobj.sims_section_code = dr["sims_section_code"].ToString();
                            vobj.Available_Seat = dr["Available_Seat"].ToString();
                            vobj.Allocated_Seat = dr["Allocated_Seat"].ToString();
                            //vobj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            //vobj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());
                            vobj.sims_transport_effective_from = dr["sims_transport_effective_from"].ToString();
                            vobj.sims_transport_effective_upto = dr["sims_transport_effective_upto"].ToString();
                            vobj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            vobj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            vobj.sims_transport_route_student_status = dr["sims_transport_route_student_status"].Equals("R") ? true : false;
                            ApproveStudent.Add(vobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApproveStudent);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ApproveStudent);
        }

        [Route("ApproveStudentTransportCUD")]
        public HttpResponseMessage ApproveStudentTransportCUD(List<Sims519> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            //string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims519 simobj in data)
                    {
                        //string fDate = db.UIDDMMYYYYformat(simobj.sims_transport_effective_from.ToString());
                        string fDate = simobj.sims_transport_effective_from.ToString();
                        DateTime fpDate = DateTime.Parse(fDate);
                        //string uDate = db.UIDDMMYYYYformat(simobj.sims_transport_effective_upto.ToString());
                        string uDate = simobj.sims_transport_effective_upto.ToString();
                        DateTime upDate = DateTime.Parse(uDate);
                        string sims_1 = null, sims_2 = null, sims_3 = null, sims_4 = null, sims_5 = null, sims_6 = null, sims_7 = null, sims_8 = null, sims_9 = null, sims_10 = null, sims_11 = null, sims_12 = null;
                        for (DateTime k = fpDate; k <= upDate; )
                        {

                            #region
                            if (k.Month == 1)
                            {
                                sims_1 = "1";
                            }
                            else if (k.Month == 2)
                            {
                                sims_2 = "2";
                            }
                            else if (k.Month == 3)
                            {
                                sims_3 = "3";
                            }
                            else if (k.Month == 4)
                            {
                                sims_4 = "4";
                            }
                            else if (k.Month == 5)
                            {
                                sims_5 = "5";
                            }
                            else if (k.Month == 6)
                            {
                                sims_6 = "6";
                            }
                            else if (k.Month == 7)
                            {
                                sims_7 = "7";
                            }
                            else if (k.Month == 8)
                            {
                                sims_8 = "8";
                            }
                            else if (k.Month == 9)
                            {
                                sims_9 = "9";
                            }
                            else if (k.Month == 10)
                            {
                                sims_10 = "10";
                            }
                            else if (k.Month == 11)
                            {
                                sims_11 = "11";
                            }
                            else if (k.Month == 12)
                            {
                                sims_12 = "12";
                            }
                            k = k.AddMonths(1);
                            #endregion
                        }


                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_application_proc]",
                            // int ins = db.ExecuteStoreProcedureforInsert("dbo.sims_transport_route_student_approved",
                            new List<SqlParameter>()
                      {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),
                          new SqlParameter("@sims_cur_code",simobj.sims_cur_code),
                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_grade_code",simobj.sims_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_section_code),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.C_direction),
                          //new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          //new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_effective_from", simobj.sims_transport_effective_from),
                          new SqlParameter("@sims_transport_effective_upto", simobj.sims_transport_effective_upto),
                          ////new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          //new SqlParameter("@sims_transport_effective_from",simobj.sims_transport_effective_from),
                          //new SqlParameter("@sims_transport_effective_upto", simobj.sims_transport_effective_upto),
                          new SqlParameter("@sims_transport_route_reject_reason",simobj.sims_transport_route_reject_reason),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code", simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_route_approve_by", simobj.sims_transport_route_approve_by),
                          new SqlParameter("@sims_transport_route_student_status",simobj.sims_transport_route_student_status==true?"R":"P"),
                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12)
                    });
                       
                        int ins = dr.RecordsAffected;
                        Sims081_Msg st = new Sims081_Msg();
                        if (dr.Read())
                        {
                            st.sims_msg = dr[0].ToString();
                            msg.strMessage = st.sims_msg;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        #endregion

        #region API FOR APPROVE STUDENT TRANSPORT NEW...

        [Route("getApproveStudentTransportNew")]
        public HttpResponseMessage getApproveStudentTransportNew(string cur_code, string acayear, string direction_code, string stop_code)
        {
            if (direction_code == "undefined")
                direction_code = null;
            if (stop_code == "undefined")
                stop_code = null;
            List<Sims519> ApproveStudent = new List<Sims519>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_application_new_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_transport_route_direction", direction_code),
                            new SqlParameter("@sims_transport_pickup_stop_code", stop_code),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims519 vobj = new Sims519();
                            vobj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                            vobj.sims_transport_enroll_number = dr["sims_transport_enroll_number"].ToString();
                            vobj.sims_student_name = dr["sims_student_name"].ToString();
                            vobj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                            vobj.sims_cur_code = dr["sims_cur_code"].ToString();
                            vobj.C_direction = dr["C_direction"].ToString();
                            vobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            vobj.sims_transport_route_student_request_by = dr["sims_transport_route_student_request_by"].ToString();
                            //vobj.sims_transport_route_student_request_date = db.UIDDMMYYYYformat(dr["sims_transport_route_student_request_date"].ToString());
                            vobj.sims_transport_route_student_request_date = dr["sims_transport_route_student_request_date"].ToString();
                            vobj.sims_transport_route_approve_by = dr["sims_transport_route_approve_by"].ToString();
                            //vobj.sims_transport_route_approve_date = db.UIDDMMYYYYformat(dr["sims_transport_route_approve_date"].ToString());
                            vobj.sims_transport_route_approve_date = dr["sims_transport_route_approve_date"].ToString();
                            vobj.sims_transport_route_reject_by = dr["sims_transport_route_reject_by"].ToString();
                            vobj.sims_transport_route_reject_reason = dr["sims_transport_route_reject_reason"].ToString();
                            //vobj.sims_transport_route_rejecte_date = db.UIDDMMYYYYformat(dr["sims_transport_route_rejecte_date"].ToString());
                            vobj.sims_transport_route_rejecte_date = dr["sims_transport_route_rejecte_date"].ToString();
                            vobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            vobj.sims_section_code = dr["sims_section_code"].ToString();
                            vobj.Available_Seat = dr["Available_Seat"].ToString();
                            vobj.Allocated_Seat = dr["Allocated_Seat"].ToString();
                            //vobj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                            //vobj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());
                            vobj.sims_transport_effective_from = dr["sims_transport_effective_from"].ToString();
                            vobj.sims_transport_effective_upto = dr["sims_transport_effective_upto"].ToString();
                            vobj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                            vobj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                            vobj.sims_transport_route_student_status = dr["sims_transport_route_student_status"].Equals("R") ? true : false;
                            ApproveStudent.Add(vobj);

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApproveStudent);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, ApproveStudent);
        }

        [Route("getStopByDirection")]
        public HttpResponseMessage getStopByDirection(string direction_code, string aca_year)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehicleCode(),PARAMETERS :: NO";
            List<Sims081> VehicleCode = new List<Sims081>();
            if (direction_code == "undefined")
                direction_code = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_student_application_new_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "A"),
                                 new SqlParameter("@sims_transport_academic_year", aca_year),
                                 new SqlParameter("@sims_transport_route_direction", direction_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_stop_code = dr["sims_transport_stop_code"].ToString();
                            simsobj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            VehicleCode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, VehicleCode);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, VehicleCode);
            }
        }

        [Route("getRouteByDirection")]
        public HttpResponseMessage getRouteByDirection(string direction_code, string aca_year, string stop_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehicleCode(),PARAMETERS :: NO";
            List<Sims081> VehicleCode = new List<Sims081>();
            if (direction_code == "undefined")
                direction_code = null;
            if (stop_code == "undefined")
                stop_code = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_student_application_new_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "B"),
                                 new SqlParameter("@sims_transport_academic_year", aca_year),
                                 new SqlParameter("@sims_transport_pickup_stop_code", stop_code),
                                 new SqlParameter("@sims_transport_route_direction", direction_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims081 simsobj = new Sims081();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_short_name = dr["sims_transport_route_short_name"].ToString();
                            VehicleCode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, VehicleCode);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, VehicleCode);
            }
        }

        [Route("getVehicleByDirection")]
        public HttpResponseMessage getVehicleByDirection(string direction_code, string aca_year, string stop_code, string route_code)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getVehicleCode(),PARAMETERS :: NO";
            List<Sims083> VehicleCode = new List<Sims083>();
            if (direction_code == "undefined")
                direction_code = null;
            if (stop_code == "undefined")
                stop_code = null;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_route_student_application_new_proc",
                        new List<SqlParameter>()
                         {
                                 new SqlParameter("@opr", "E"),
                                 new SqlParameter("@sims_transport_route_code", route_code),
                                 new SqlParameter("@sims_transport_academic_year", aca_year),
                                 new SqlParameter("@sims_transport_pickup_stop_code", stop_code),
                                 new SqlParameter("@sims_transport_route_direction", direction_code),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims083 simsobj = new Sims083();
                            simsobj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            simsobj.sims_transport_vehicle_name_plate = dr["sims_transport_vehicle_name_plate"].ToString();
                            VehicleCode.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, VehicleCode);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, VehicleCode);
            }
        }

        [Route("CUDApproveStudentTransportNew")]
        public HttpResponseMessage CUDApproveStudentTransportNew(List<Sims519> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            Message msg = new Message();
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims519 simobj in data)
                    {

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_student_application_new_proc]",
                            new List<SqlParameter>()
                      {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),
                          new SqlParameter("@sims_cur_code",simobj.sims_cur_code),
                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_grade_code",simobj.sims_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_section_code),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.C_direction),
                          //new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          //new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_effective_from", simobj.sims_transport_effective_from),
                          new SqlParameter("@sims_transport_effective_upto", simobj.sims_transport_effective_upto),
                          new SqlParameter("@sims_transport_route_reject_reason",simobj.sims_transport_route_reject_reason),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code", simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_route_approve_by", simobj.sims_transport_route_approve_by),
                          new SqlParameter("@sims_transport_route_student_status",simobj.sims_transport_route_student_status==true?"R":"P"),
                    });
                        int ins = dr.RecordsAffected;
                        Sims081_Msg st = new Sims081_Msg();
                        if (dr.Read())
                        {
                            st.sims_msg = dr[0].ToString();
                            msg.strMessage = st.sims_msg;
                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        #endregion

    }
}