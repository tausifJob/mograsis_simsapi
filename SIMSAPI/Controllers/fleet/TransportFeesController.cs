﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportFees")]
    public class TransportFeesController : ApiController
    {

        #region API For Transport Fee...

        [Route("getTransportFees")]
        public HttpResponseMessage getTransportFees()
        {
            List<Sim079> TransportFees = new List<Sim079>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sim079 obj = new Sim079();
                            obj.sims_transport_fee_number = dr["sims_transport_fee_number"].ToString();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_category = dr["sims_transport_category"].ToString();
                            obj.sims_fee_category_description = dr["sims_fee_category_description"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            obj.sims_transport_code = dr["sims_transport_code"].ToString();
                            obj.sims_fee_code_description = dr["sims_fee_code_description"].ToString();
                            obj.sims_transport_amount = dr["sims_transport_amount"].ToString();
                            obj.sims_transport_installment_min_amount = dr["sims_transport_installment_min_amount"].ToString();
                            obj.sims_transport_frequency = dr["sims_transport_frequency"].ToString();
                            obj.FeeFrequency = dr["FeeFrequency"].ToString();
                            obj.sims_transport_fee_code_type = dr["sims_transport_code_type"].ToString();
                            obj.FeeCodeType = dr["FeeCodeType"].ToString();

                            obj.transport_fee_period1 = dr["sims_transport_period1"].ToString();
                            obj.transport_fee_period2 = dr["sims_transport_period2"].ToString();
                            obj.transport_fee_period3 = dr["sims_transport_period3"].ToString();
                            obj.transport_fee_period4 = dr["sims_transport_period4"].ToString();
                            obj.transport_fee_period5 = dr["sims_transport_period5"].ToString();
                            obj.transport_fee_period6 = dr["sims_transport_period6"].ToString();
                            obj.transport_fee_period7 = dr["sims_transport_period7"].ToString();
                            obj.transport_fee_period8 = dr["sims_transport_period8"].ToString();
                            obj.transport_fee_period9 = dr["sims_transport_period9"].ToString();
                            obj.transport_fee_period10 = dr["sims_transport_period10"].ToString();
                            obj.transport_fee_period11 = dr["sims_transport_period11"].ToString();
                            obj.transport_fee_period12 = dr["sims_transport_period12"].ToString();

                            obj.sims_transport_status = dr["sims_transport_status"].ToString().Equals("A") ? true : false;
                            obj.sims_transport_installment_mode = dr["sims_transport_installment_mode"].ToString().Equals("Y") ? true : false;
                            obj.sims_transport_mandatory = dr["sims_transport_mandatory"].ToString().Equals("Y") ? true : false;
                            obj.sims_transport_refundable = dr["sims_transport_refundable"].ToString().Equals("Y") ? true : false;
                            obj.sims_transport_manual_receipt = dr["sims_transport_manual_receipt"].ToString().Equals("Y") ? true : false;

                            TransportFees.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
        }

        [Route("CUDTransportFees")]
        public HttpResponseMessage CUDTransportFees(List<Sim079> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sim079 simsobj in data)
                    {
                        //if(simsobj.transport_fee_period1=="" || simsobj.transport_fee_period1=="undefined")
                        //    simsobj.transport_fee_period1="0";
                        //if(simsobj.transport_fee_period2=="" || simsobj.transport_fee_period2=="undefined")
                        //    simsobj.transport_fee_period2="0";
                        //if(simsobj.transport_fee_period3=="" || simsobj.transport_fee_period3=="undefined")
                        //    simsobj.transport_fee_period3="0";
                        //if(simsobj.transport_fee_period4=="" || simsobj.transport_fee_period4=="undefined")
                        //    simsobj.transport_fee_period4="0";
                        //if(simsobj.transport_fee_period5=="" || simsobj.transport_fee_period5=="undefined")
                        //    simsobj.transport_fee_period5="0";
                        //if(simsobj.transport_fee_period6=="" || simsobj.transport_fee_period6=="undefined")
                        //    simsobj.transport_fee_period6="0";
                        //if(simsobj.transport_fee_period7=="" || simsobj.transport_fee_period7=="undefined")
                        //    simsobj.transport_fee_period7="0";
                        //if(simsobj.transport_fee_period8=="" || simsobj.transport_fee_period8=="undefined")
                        //    simsobj.transport_fee_period8="0";
                        //if(simsobj.transport_fee_period9=="" || simsobj.transport_fee_period9=="undefined")
                        //    simsobj.transport_fee_period9="0";
                        //if(simsobj.transport_fee_period10==""|| simsobj.transport_fee_period1=="undefined")
                        //    simsobj.transport_fee_period10="0";
                        //if(simsobj.transport_fee_period11==""|| simsobj.transport_fee_period1=="undefined")
                        //    simsobj.transport_fee_period11="0";
                        //if(simsobj.transport_fee_period12==""|| simsobj.transport_fee_period1=="undefined")
                        //    simsobj.transport_fee_period12="0";


                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_fee_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr",simsobj.opr),
                          new SqlParameter("@sims_transport_fee_number",simsobj.sims_transport_fee_number),
                          new SqlParameter("@sims_transport_fee_academic_year",simsobj.sims_academic_year),
                          new SqlParameter("@sims_transport_fee_route_code",simsobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_fee_route_direction",simsobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_fee_category",simsobj.sims_transport_category),
                          new SqlParameter("@sims_transport_fee_amount",simsobj.sims_transport_amount),
                          new SqlParameter("@sims_transport_fee_installment_min_amount",simsobj.sims_transport_installment_min_amount),
                          new SqlParameter("@sims_transport_fee_frequency",simsobj.sims_transport_frequency),
                          new SqlParameter("@sims_transport_fee_code",simsobj.sims_transport_code),
                          new SqlParameter("@sims_transport_fee_code_type",simsobj.sims_transport_code_type),

                          new SqlParameter("@sims_transport_fee_period1",simsobj.transport_fee_period1),
                          new SqlParameter("@sims_transport_fee_period2",simsobj.transport_fee_period2),
                          new SqlParameter("@sims_transport_fee_period3",simsobj.transport_fee_period3),
                          new SqlParameter("@sims_transport_fee_period4",simsobj.transport_fee_period4),
                          new SqlParameter("@sims_transport_fee_period5",simsobj.transport_fee_period5),
                          new SqlParameter("@sims_transport_fee_period6",simsobj.transport_fee_period6),
                          new SqlParameter("@sims_transport_fee_period7",simsobj.transport_fee_period7),
                          new SqlParameter("@sims_transport_fee_period8",simsobj.transport_fee_period8),
                          new SqlParameter("@sims_transport_fee_period9",simsobj.transport_fee_period9),
                          new SqlParameter("@sims_transport_fee_period10",simsobj.transport_fee_period10),
                          new SqlParameter("@sims_transport_fee_period11",simsobj.transport_fee_period11),
                          new SqlParameter("@sims_transport_fee_period12",simsobj.transport_fee_period12),

                          new SqlParameter("@sims_transport_fee_status",simsobj.sims_transport_status==true?"A":"I"),
                          new SqlParameter("@sims_transport_fee_installment_mode",simsobj.sims_transport_installment_mode==true?"Y":"N"),
                          new SqlParameter("@sims_transport_fee_mandatory",simsobj.sims_transport_mandatory==true?"Y":"N"),
                          new SqlParameter("@sims_transport_fee_refundable",simsobj.sims_transport_refundable==true?"Y":"N"),
                          new SqlParameter("@sims_transport_fee_manual_receipt",simsobj.sims_transport_manual_receipt==true?"Y":"N"),
                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("getallRouteNameForTransFees")]
        public HttpResponseMessage getallRouteNameForTransFees(string Acayear)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getallRouteNameForTransFees(),PARAMETERS :: NO";
            // Log.Debug(string.Format(debug, "FLEET", "getAllRouteName"));

            List<Sims085> routename = new List<Sims085>();

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_proc]",
                        new List<SqlParameter>()
                         {
                              new SqlParameter("@opr", "R"),
                              new SqlParameter("@sims_transport_fee_academic_year",Acayear)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 simsobj = new Sims085();
                            simsobj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            simsobj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            simsobj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            simsobj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            routename.Add(simsobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, routename);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, routename);
            }
        }

        [Route("getStudentTransportData")]
        public HttpResponseMessage getStudentTransportData(string data)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                Sims085 sf = Newtonsoft.Json.JsonConvert.DeserializeObject<Sims085>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("[sims].[sims_transport_fee_proc]",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", 'A'),
                                new SqlParameter("@sims_transport_fee_academic_year", sf.sims_academic_year),
                                new SqlParameter("@sims_transport_fee_route_code", sf.sims_transport_route_code),
                                new SqlParameter("@sims_transport_fee_route_direction", sf.sims_transport_route_direction),
                             });
                    ds.DataSetName = "TransData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CUDStudentTransportFees")]
        public HttpResponseMessage CUDStudentTransportFees(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "FLEET", "CUDTransportFees", simsobj));

            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims525 simsobj in data)
                    {

                        if (simsobj.sims_transport_period1 == "" || simsobj.sims_transport_period1 == "undefined")
                            simsobj.sims_transport_period1 = "0";
                        if (simsobj.sims_transport_period2 == "" || simsobj.sims_transport_period2 == "undefined")
                            simsobj.sims_transport_period2 = "0";
                        if (simsobj.sims_transport_period3 == "" || simsobj.sims_transport_period3 == "undefined")
                            simsobj.sims_transport_period3 = "0";
                        if (simsobj.sims_transport_period4 == "" || simsobj.sims_transport_period4 == "undefined")
                            simsobj.sims_transport_period4 = "0";
                        if (simsobj.sims_transport_period5 == "" || simsobj.sims_transport_period5 == "undefined")
                            simsobj.sims_transport_period5 = "0";
                        if (simsobj.sims_transport_period6 == "" || simsobj.sims_transport_period6 == "undefined")
                            simsobj.sims_transport_period6 = "0";
                        if (simsobj.sims_transport_period7 == "" || simsobj.sims_transport_period7 == "undefined")
                            simsobj.sims_transport_period7 = "0";
                        if (simsobj.sims_transport_period8 == "" || simsobj.sims_transport_period8 == "undefined")
                            simsobj.sims_transport_period8 = "0";
                        if (simsobj.sims_transport_period9 == "" || simsobj.sims_transport_period9 == "undefined")
                            simsobj.sims_transport_period9 = "0";
                        if (simsobj.sims_transport_period10 == "" || simsobj.sims_transport_period1 == "undefined")
                            simsobj.sims_transport_period10 = "0";
                        if (simsobj.sims_transport_period11 == "" || simsobj.sims_transport_period1 == "undefined")
                            simsobj.sims_transport_period11 = "0";
                        if (simsobj.sims_transport_period12 == "" || simsobj.sims_transport_period1 == "undefined")
                            simsobj.sims_transport_period12 = "0";

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_fee_proc]",
                        new List<SqlParameter>()
                        {
                          new SqlParameter("@opr","B"),
                          new SqlParameter("@sims_transport_fee_number",simsobj.sims_fee_number),
                          new SqlParameter("@sims_transport_fee_academic_year",simsobj.sims_transport_academic_year),
                          new SqlParameter("@sims_fee_cur_code",simsobj.sims_fee_cur_code),
                          new SqlParameter("@sims_fee_grade_code",simsobj.sims_fee_grade_code),
                          new SqlParameter("@sims_fee_section_code",simsobj.sims_fee_section_code),
                          new SqlParameter("@sims_enroll_number",simsobj.sims_enroll_number),

                          new SqlParameter("@sims_transport_fee_route_code",simsobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_fee_route_direction",simsobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_fee_period1",simsobj.sims_transport_period1),
                          new SqlParameter("@sims_transport_fee_period2",simsobj.sims_transport_period2),
                          new SqlParameter("@sims_transport_fee_period3",simsobj.sims_transport_period3),
                          new SqlParameter("@sims_transport_fee_period4",simsobj.sims_transport_period4),
                          new SqlParameter("@sims_transport_fee_period5",simsobj.sims_transport_period5),
                          new SqlParameter("@sims_transport_fee_period6",simsobj.sims_transport_period6),
                          new SqlParameter("@sims_transport_fee_period7",simsobj.sims_transport_period7),
                          new SqlParameter("@sims_transport_fee_period8",simsobj.sims_transport_period8),
                          new SqlParameter("@sims_transport_fee_period9",simsobj.sims_transport_period9),
                          new SqlParameter("@sims_transport_fee_period10",simsobj.sims_transport_period10),
                          new SqlParameter("@sims_transport_fee_period11",simsobj.sims_transport_period11),
                          new SqlParameter("@sims_transport_fee_period12",simsobj.sims_transport_period12),
                          new SqlParameter("@sims_transport_effective_from",db.DBYYYYMMDDformat(simsobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto",db.DBYYYYMMDDformat(simsobj.sims_transport_effective_upto)),

                        });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region API For Transport Maintenance...

        [Route("getvehiclecode")]
        public HttpResponseMessage getvehiclecode()
        {
            List<SimTVS> TransportFees = new List<SimTVS>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_maintenance_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTVS obj = new SimTVS();

                            obj.sims_transport_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString() + " / " + dr["sims_transport_vehicle_name"].ToString();
                            TransportFees.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
        }

        [Route("getvehicledrivercode")]
        public HttpResponseMessage getvehicledrivercode(string vehicle_code)
        {
            List<SimTVS> TransportFees = new List<SimTVS>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_maintenance_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code)
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTVS obj = new SimTVS();
                            obj.sims_driver_code = dr["sims_driver_code"].ToString();
                            obj.sims_driver_name = dr["sims_driver_code"].ToString() + " / " + dr["sims_driver_name"].ToString();
                            TransportFees.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
        }

        [Route("getvehicledoccode")]
        public HttpResponseMessage getvehicledoccode()
        {
            List<SimTVS> TransportFees = new List<SimTVS>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_maintenance_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            SimTVS obj = new SimTVS();
                            obj.sims_transport_maintenance_doc_no = dr["sims_transport_maintenance_doc_no"].ToString();
                            obj.sims_transport_vehicle_registration_number = dr["sims_transport_vehicle_registration_number"].ToString();
                            TransportFees.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, TransportFees);
        }

        [Route("Targetcodesdetailstudent")]
        public HttpResponseMessage Targetcodesdetailstudent(SimTVS obj)
        {
            object o = null;
            HttpStatusCode s = HttpStatusCode.NoContent;
            try
            {
                //SimSTD sf = Newtonsoft.Json.JsonConvert.DeserializeObject<SimSTD>(data);
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    DataSet ds = db.ExecuteStoreProcedureDS("sims.sims_transport_maintenance_proc",
                        new List<SqlParameter>()
                             {
                                new SqlParameter("@opr", "C"),
                                new SqlParameter("@sims_transport_maintenance_doc_no", obj.sims_transport_maintenance_doc_no),
                                //new SqlParameter("@sims_student_behaviour_academic_year", obj.sims_academic_year),
                             });
                    ds.DataSetName = "AdjData";
                    o = ds;
                    s = HttpStatusCode.OK;
                }
            }
            catch (Exception x)
            {
                o = x;
                s = HttpStatusCode.InternalServerError;
            }
            return Request.CreateResponse(s, o);
        }

        [Route("CUDMaintenanceDetails")]
        public HttpResponseMessage CUDMaintenanceDetails(string data1, List<SimTVS> data)
        {
            Message msg = new Message();
            string st = string.Empty;
            string sn = string.Empty;

            bool inserted = false;
            SimTVS obj = Newtonsoft.Json.JsonConvert.DeserializeObject<SimTVS>(data1);
            // Invs058_item obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Invs058_item>(data1);
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr1 = db.ExecuteStoreProcedure("sims.sims_transport_maintenance_proc",
                       new List<SqlParameter>() 
                                 { 
                                     new SqlParameter("@opr",obj.opr),
                                     new SqlParameter("@sims_transport_vehicle_code",obj.sims_transport_vehicle_code),			
                                     new SqlParameter("@sims_transport_maintenance_date",db.DBYYYYMMDDformat(obj.sims_transport_maintenance_date)),
                                     new SqlParameter("@sims_transport_maintenance_service_station",obj.sims_transport_maintenance_service_station),
                                     new SqlParameter("@sims_transport_maintenance_purpose",obj.sims_transport_maintenance_purpose),
                                     new SqlParameter("@sims_transport_maintenance_total_amount",obj.sims_transport_maintenance_total_amount),
                                     new SqlParameter("@sims_transport_maintenance_approve_by",obj.sims_transport_maintenance_approve_by),
                                     new SqlParameter("@sims_transport_maintenance_driver_code",obj.sims_transport_maintenance_driver_code),	
                                     new SqlParameter("@sims_transport_maintenance_next_date",db.DBYYYYMMDDformat(obj.sims_transport_maintenance_next_date)),
                                     new SqlParameter("@sims_transport_maintenance_doc_no",obj.sims_transport_maintenance_doc_no),
                                     new SqlParameter("@sims_transport_maintenance_status", obj.sims_transport_maintenance_status==true?"A":"I"),
                                     new SqlParameter("sims_transport_maintenance_Quantity", obj.sims_transport_maintenance_Quantity),

                                 });

                    if (dr1.Read())
                    {
                        st = dr1["sims_transport_maintenance_doc_no"].ToString();
                    }

                    if (dr1.RecordsAffected > 0)
                    {
                        inserted = true;
                    }
                    dr1.Close();


                    foreach (SimTVS simsobj in data)
                    {
                        if (!string.IsNullOrEmpty(st))
                        {
                            SqlDataReader dr = db.ExecuteStoreProcedure("sims.sims_transport_maintenance_proc",

                               new List<SqlParameter>() 
                                { 
                                new SqlParameter("@opr", simsobj.opr),
                                new SqlParameter("@sims_transport_maintenance_doc_no",st),
                                new SqlParameter("@sims_transport_maintenance_desc",simsobj.sims_transport_maintenance_desc),
                                new SqlParameter("@sims_transport_maintenance_amount", simsobj.sims_transport_maintenance_amount),
                                new SqlParameter("@sims_transport_maintenance_detail_status", simsobj.sims_transport_maintenance_detail_status==true?"A":"I"),
                                new SqlParameter("@sims_transport_maintenance_line_no", simsobj.sims_transport_maintenance_line_no),
                                new SqlParameter("@sims_transport_maintenance_Quantity", simsobj.sims_transport_maintenance_Quantity),

                         });

                            if (dr.Read())
                            {
                                st = dr["sims_transport_maintenance_doc_no"].ToString();
                            }

                            if (dr.RecordsAffected > 0)
                            {
                                inserted = true;
                            }
                            dr.Close();
                            if (inserted == false)
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("sims.sims_transport_maintenance_proc",
                                new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@sims_transport_vehicle_code", obj.sims_transport_vehicle_code),
                                    new SqlParameter("@sims_transport_maintenance_date", obj.sims_transport_maintenance_date),
                                    new SqlParameter("@sims_transport_maintenance_doc_no",st),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                            else
                            {
                                sn = st;
                            }
                        }

                        else
                        {
                            {
                                int ins1 = db.ExecuteStoreProcedureforInsert("sims.sims_transport_maintenance_proc",
                                 new List<SqlParameter>() 
                                 { 
                                    new SqlParameter("@opr",'D'),
                                    new SqlParameter("@sims_transport_vehicle_code", obj.sims_transport_vehicle_code),
                                    new SqlParameter("@sims_transport_maintenance_date", obj.sims_transport_maintenance_date),
                                    new SqlParameter("@sims_transport_maintenance_doc_no",st),
                                });
                                if (ins1 > 0)
                                {
                                    inserted = false;
                                }
                            }
                        }
                    }

                    if (sn != null)
                    {
                        msg.strMessage = sn;
                    }
                    else
                    {
                        msg.strMessage = "Transport Maintenance Not Added...";
                    }
                }
            }

            catch (Exception x)
            {

                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        #endregion

        #region API For Approve Transport Fee...

        [Route("getStudentTransportDetails")]
        public HttpResponseMessage getStudentTransportDetails(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code,string status)
        {
            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            if (status == "undefined" || status == "null")
                status = null;
            
            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code),
                            new SqlParameter("@sims_transport_student_status", status),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            if (!string.IsNullOrEmpty(dr["sims_png"].ToString()))
                            {
                                obj.sims_png = dr["sims_png"].ToString();
                                StudentTrans_list.Add(obj);
                            }
                            else
                            {
                                string fDate = null;
                                string uDate = null;
                                obj.sims_png = dr["sims_png"].ToString();
                                obj.sims_student_name = dr["sims_student_name"].ToString();
                                obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                                obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                                obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_number = dr["sims_fee_number"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    obj.sims_fee_code = dr["sims_fee_code"].ToString();
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                                    obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                                    obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                                    obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                                    obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                                    obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                                    obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                    obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                                    obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                                    obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                                    obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                                    obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                                    obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                                    obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                                    obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                                    obj.V_Name = dr["V_Name"].ToString();
                                    obj.V_Code = dr["V_Code"].ToString();
                                    obj.V_Name_Code = dr["V_Name_Code"].ToString();

                                    obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                                    obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                                    obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                                    obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                                    obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                                    obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                                    obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                                    obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                                    obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                                    obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                                    obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                                    obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                                    try
                                    {
                                        obj.sims_fee_period1 = dr["sims_transport_route_student_status"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        obj.sims_fee_period2 = dr["sims_transport_route_student_update_status"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                        
                                    }
                                    try
                                    {
                                        obj.sims_fee_period3 = dr["sims_transport_route_student_updated_date"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                        
                                    }

                                    fDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_from.ToString());
                                    DateTime fpDate = DateTime.Parse(fDate);
                                    uDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_upto.ToString());
                                    DateTime upDate = DateTime.Parse(uDate);

                                    #region

                                    for (DateTime k = fpDate; k <= upDate;)
                                    {

                                        #region

                                        if (k.Month == 1)
                                        {
                                            sims_1 = Convert.ToDouble(obj.sims_transport_period1);
                                        }
                                        else if (k.Month == 2)
                                        {
                                            sims_2 = Convert.ToDouble(obj.sims_transport_period2);
                                        }
                                        else if (k.Month == 3)
                                        {
                                            sims_3 = Convert.ToDouble(obj.sims_transport_period3);
                                        }
                                        else if (k.Month == 4)
                                        {
                                            sims_4 = Convert.ToDouble(obj.sims_transport_period4);
                                        }
                                        else if (k.Month == 5)
                                        {
                                            sims_5 = Convert.ToDouble(obj.sims_transport_period5);
                                        }
                                        else if (k.Month == 6)
                                        {
                                            sims_6 = Convert.ToDouble(obj.sims_transport_period6);
                                        }
                                        else if (k.Month == 7)
                                        {
                                            sims_7 = Convert.ToDouble(obj.sims_transport_period7);
                                        }
                                        else if (k.Month == 8)
                                        {
                                            sims_8 = Convert.ToDouble(obj.sims_transport_period8);
                                        }
                                        else if (k.Month == 9)
                                        {
                                            sims_9 = Convert.ToDouble(obj.sims_transport_period9);
                                        }
                                        else if (k.Month == 10)
                                        {
                                            sims_10 = Convert.ToDouble(obj.sims_transport_period10);
                                        }
                                        else if (k.Month == 11)
                                        {
                                            sims_11 = Convert.ToDouble(obj.sims_transport_period11);
                                        }
                                        else if (k.Month == 12)
                                        {
                                            sims_12 = Convert.ToDouble(obj.sims_transport_period12);
                                        }
                                        k = k.AddMonths(1);

                                        #endregion
                                    }

                                    obj.sims_paid_total = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                                    sims_1 = 0; sims_2 = 0; sims_3 = 0; sims_4 = 0; sims_5 = 0; sims_6 = 0; sims_7 = 0; sims_8 = 0; sims_9 = 0; sims_10 = 0; sims_11 = 0; sims_12 = 0;
                                    #endregion

                                    StudentTrans_list.Add(obj);
                                }
                                catch (Exception)
                                {
                                    
                                }
                            }
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        [Route("getStudentTransportDetailsNewFeeStructure")]
        public HttpResponseMessage getStudentTransportDetailsNewFeeStructure(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code, string status)
        {
            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            if (status == "undefined" || status == "null")
                status = null;

            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code),
                            new SqlParameter("@sims_transport_student_status", status),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            if (!string.IsNullOrEmpty(dr["sims_png"].ToString()))
                            {
                                obj.sims_png = dr["sims_png"].ToString();
                                StudentTrans_list.Add(obj);
                            }
                            else
                            {
                                string fDate = null;
                                string uDate = null;
                                obj.sims_png = dr["sims_png"].ToString();
                                obj.sims_student_name = dr["sims_student_name"].ToString();
                                obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                                obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                                obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_number = dr["sims_fee_number"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    obj.sims_fee_code = dr["sims_fee_code"].ToString();
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                                    obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                                    obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                                    obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                                    obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                                    obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                                    obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                    obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                                    obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                                    obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                                    obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                                    obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                                    obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                                    obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                                    obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                                    obj.V_Name = dr["V_Name"].ToString();
                                    obj.V_Code = dr["V_Code"].ToString();
                                    obj.V_Name_Code = dr["V_Name_Code"].ToString();

                                    obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                                    obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                                    obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                                    obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                                    obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                                    obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                                    obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                                    obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                                    obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                                    obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                                    obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                                    obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                                    try
                                    {
                                        obj.sims_fee_period1 = dr["sims_transport_route_student_status"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        obj.sims_fee_period2 = dr["sims_transport_route_student_update_status"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        obj.sims_fee_period3 = dr["sims_transport_route_student_updated_date"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        obj.both_amt = dr["both_amt"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    fDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_from.ToString());
                                    DateTime fpDate = DateTime.Parse(fDate);
                                    uDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_upto.ToString());
                                    DateTime upDate = DateTime.Parse(uDate);

                                    #region

                                    for (DateTime k = fpDate; k <= upDate;)
                                    {

                                        #region

                                        if (k.Month == 1)
                                        {
                                            if (sims_1 == 0.0)
                                            {
                                                sims_1 = Convert.ToDouble(obj.sims_transport_period1);
                                            }
                                            else
                                            {
                                                sims_1 = sims_1 + Convert.ToDouble(obj.sims_transport_period1);
                                            }
                                            
                                        }
                                        else if (k.Month == 2)
                                        {
                                            if (sims_2 == 0.0)
                                            {
                                                sims_2 = Convert.ToDouble(obj.sims_transport_period2);
                                            }
                                            else
                                            {
                                                sims_2 = sims_2 + Convert.ToDouble(obj.sims_transport_period2);
                                            }
                                            
                                        }
                                        else if (k.Month == 3)
                                        {
                                            if (sims_3 == 0.0)
                                            {
                                                sims_3 = Convert.ToDouble(obj.sims_transport_period3);
                                            }
                                            else
                                            {
                                                sims_3 = sims_3 + Convert.ToDouble(obj.sims_transport_period3);
                                            }
                                            
                                        }
                                        else if (k.Month == 4)
                                        {
                                            if (sims_4 == 0.0)
                                            {
                                                sims_4 = Convert.ToDouble(obj.sims_transport_period4);
                                            }
                                            else
                                            {
                                                sims_4 = sims_4 + Convert.ToDouble(obj.sims_transport_period4);
                                            }
                                            
                                        }
                                        else if (k.Month == 5)
                                        {
                                            if (sims_5 == 0.0)
                                            {
                                                sims_5 = Convert.ToDouble(obj.sims_transport_period5);
                                            }
                                            else
                                            {
                                                sims_5 = sims_5 + Convert.ToDouble(obj.sims_transport_period5);
                                            }
                                            
                                        }
                                        else if (k.Month == 6)
                                        {
                                            if (sims_6 == 0.0)
                                            {
                                                sims_6 = Convert.ToDouble(obj.sims_transport_period6);
                                            }
                                            else
                                            {
                                                sims_6 = sims_6 + Convert.ToDouble(obj.sims_transport_period6);
                                            }

                                        }
                                        else if (k.Month == 7)
                                        {
                                            if (sims_7 == 0.0)
                                            {
                                                sims_7 = Convert.ToDouble(obj.sims_transport_period7);
                                            }
                                            else
                                            {
                                                sims_7 = sims_7 + Convert.ToDouble(obj.sims_transport_period7);
                                            }
                                        }
                                        else if (k.Month == 8)
                                        {
                                            if (sims_8 == 0.0)
                                            {
                                                sims_8 = Convert.ToDouble(obj.sims_transport_period8);
                                            }
                                            else
                                            {
                                                sims_8 = sims_8 + Convert.ToDouble(obj.sims_transport_period8);
                                            }
                                            
                                        }
                                        else if (k.Month == 9)
                                        {
                                            if (sims_9 == 0.0)
                                            {
                                                sims_9 = Convert.ToDouble(obj.sims_transport_period9);
                                            }
                                            else
                                            {
                                                sims_9 = sims_9 + Convert.ToDouble(obj.sims_transport_period9);
                                            }
                                            
                                        }
                                        else if (k.Month == 10)
                                        {
                                            if (sims_10 == 0.0)
                                            {
                                                sims_10 = Convert.ToDouble(obj.sims_transport_period10);
                                            }
                                            else
                                            {
                                                sims_10 = sims_10 + Convert.ToDouble(obj.sims_transport_period10);
                                            }
                                            
                                        }
                                        else if (k.Month == 11)
                                        {
                                            if (sims_11 == 0.0)
                                            {
                                                sims_11 = Convert.ToDouble(obj.sims_transport_period11);
                                            }
                                            else
                                            {
                                                sims_11 = sims_11 + Convert.ToDouble(obj.sims_transport_period11);
                                            }
                                            
                                        }
                                        else if (k.Month == 12)
                                        {
                                            if (sims_12 == 0.0)
                                            {
                                                sims_12 = Convert.ToDouble(obj.sims_transport_period12);
                                            }
                                            else
                                            {
                                                sims_12 = sims_12 + Convert.ToDouble(obj.sims_transport_period12);
                                            }
                                            
                                        }
                                        k = k.AddMonths(1);

                                        #endregion
                                    }

                                    obj.sims_paid_total = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                                    sims_1 = 0; sims_2 = 0; sims_3 = 0; sims_4 = 0; sims_5 = 0; sims_6 = 0; sims_7 = 0; sims_8 = 0; sims_9 = 0; sims_10 = 0; sims_11 = 0; sims_12 = 0;
                                    #endregion

                                    StudentTrans_list.Add(obj);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }


        [Route("getStudentTransportDetailsNewFeeStructure_ASIS")]
        public HttpResponseMessage getStudentTransportDetailsNewFeeStructure_ASIS(string cur_code, string acayear, string grade, string sec_code, string enroll, string vehicle_code, string status)
        {
            if (enroll == "undefined" || enroll == "null")
                enroll = null;
            if (cur_code == "undefined" || cur_code == "null")
                cur_code = null;
            if (acayear == "undefined" || acayear == "null")
                acayear = null;
            if (grade == "undefined" || grade == "null")
                grade = null;
            if (sec_code == "undefined" || sec_code == "null")
                sec_code = null;
            if (vehicle_code == "undefined" || vehicle_code == "null")
                vehicle_code = null;
            if (status == "undefined" || status == "null")
                status = null;

            double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_asis_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S"),
                            new SqlParameter("@sub_opr", "C"),
                            new SqlParameter("@sims_cur_code", cur_code),
                            new SqlParameter("@sims_transport_academic_year",acayear),
                            new SqlParameter("@sims_grade_code", grade),
                            new SqlParameter("@sims_section_code", sec_code),
                            new SqlParameter("@sims_transport_enroll_number", enroll),
                            new SqlParameter("@sims_transport_vehicle_code", vehicle_code),
                            new SqlParameter("@sims_transport_student_status", status),
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            if (!string.IsNullOrEmpty(dr["sims_png"].ToString()))
                            {
                                obj.sims_png = dr["sims_png"].ToString();
                                StudentTrans_list.Add(obj);
                            }
                            else
                            {
                                string fDate = null;
                                string uDate = null;
                                obj.sims_png = dr["sims_png"].ToString();
                                obj.sims_student_name = dr["sims_student_name"].ToString();
                                obj.sims_enroll_number = dr["sims_enroll_number"].ToString();
                                obj.sims_fee_cur_code = dr["sims_fee_cur_code"].ToString();
                                obj.sims_fee_grade_code = dr["sims_fee_grade_code"].ToString();
                                obj.sims_grade_name_en = dr["sims_grade_name_en"].ToString();
                                obj.sims_fee_section_code = dr["sims_fee_section_code"].ToString();
                                obj.sims_section_name_en = dr["sims_section_name_en"].ToString();
                                try
                                {
                                    obj.sims_fee_number = dr["sims_fee_number"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    obj.sims_fee_code = dr["sims_fee_code"].ToString();
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    obj.sims_transport_academic_year = dr["sims_transport_academic_year"].ToString();
                                    obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();

                                    obj.sims_transport_route_student_code = dr["sims_transport_route_student_code"].ToString();
                                    obj.sims_appl_parameter = dr["sims_appl_parameter"].ToString();

                                    obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                                    obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                                    obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                                    obj.sims_transport_route_direction_1 = dr["sims_transport_route_direction_1"].ToString();
                                    obj.sims_transport_pickup_stop_code = dr["sims_transport_pickup_stop_code"].ToString();
                                    obj.sims_transport_pickup_stop_code_1 = dr["sims_transport_pickup_stop_code_1"].ToString();
                                    obj.sims_transport_drop_stop_code = dr["sims_transport_drop_stop_code"].ToString();
                                    obj.sims_transport_drop_stop_code_1 = dr["sims_transport_drop_stop_code_1"].ToString();

                                    obj.sims_transport_effective_from = db.UIDDMMYYYYformat(dr["sims_transport_effective_from"].ToString());
                                    obj.sims_transport_effective_upto = db.UIDDMMYYYYformat(dr["sims_transport_effective_upto"].ToString());

                                    obj.sims_transport_seat_number = dr["sims_transport_seat_number"].ToString();
                                    obj.V_Name = dr["V_Name"].ToString();
                                    obj.V_Code = dr["V_Code"].ToString();
                                    obj.V_Name_Code = dr["V_Name_Code"].ToString();

                                    obj.sims_transport_period1 = dr["sims_transport_period1"].ToString();
                                    obj.sims_transport_period2 = dr["sims_transport_period2"].ToString();
                                    obj.sims_transport_period3 = dr["sims_transport_period3"].ToString();
                                    obj.sims_transport_period4 = dr["sims_transport_period4"].ToString();
                                    obj.sims_transport_period5 = dr["sims_transport_period5"].ToString();
                                    obj.sims_transport_period6 = dr["sims_transport_period6"].ToString();
                                    obj.sims_transport_period7 = dr["sims_transport_period7"].ToString();
                                    obj.sims_transport_period8 = dr["sims_transport_period8"].ToString();
                                    obj.sims_transport_period9 = dr["sims_transport_period9"].ToString();
                                    obj.sims_transport_period10 = dr["sims_transport_period10"].ToString();
                                    obj.sims_transport_period11 = dr["sims_transport_period11"].ToString();
                                    obj.sims_transport_period12 = dr["sims_transport_period12"].ToString();

                                    try
                                    {
                                        obj.sims_fee_period1 = dr["sims_transport_route_student_status"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        obj.sims_fee_period2 = dr["sims_transport_route_student_update_status"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        obj.sims_fee_period3 = dr["sims_transport_route_student_updated_date"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        obj.both_amt = dr["both_amt"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    fDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_from.ToString());
                                    DateTime fpDate = DateTime.Parse(fDate);
                                    uDate = db.DBYYYYMMDDformat(obj.sims_transport_effective_upto.ToString());
                                    DateTime upDate = DateTime.Parse(uDate);

                                    #region

                                    for (DateTime k = fpDate; k <= upDate;)
                                    {

                                        #region

                                        if (k.Month == 1)
                                        {
                                            if (sims_1 == 0.0)
                                            {
                                                sims_1 = Convert.ToDouble(obj.sims_transport_period1);
                                            }
                                            else
                                            {
                                                sims_1 = sims_1 + Convert.ToDouble(obj.sims_transport_period1);
                                            }

                                        }
                                        else if (k.Month == 2)
                                        {
                                            if (sims_2 == 0.0)
                                            {
                                                sims_2 = Convert.ToDouble(obj.sims_transport_period2);
                                            }
                                            else
                                            {
                                                sims_2 = sims_2 + Convert.ToDouble(obj.sims_transport_period2);
                                            }

                                        }
                                        else if (k.Month == 3)
                                        {
                                            if (sims_3 == 0.0)
                                            {
                                                sims_3 = Convert.ToDouble(obj.sims_transport_period3);
                                            }
                                            else
                                            {
                                                sims_3 = sims_3 + Convert.ToDouble(obj.sims_transport_period3);
                                            }

                                        }
                                        else if (k.Month == 4)
                                        {
                                            if (sims_4 == 0.0)
                                            {
                                                sims_4 = Convert.ToDouble(obj.sims_transport_period4);
                                            }
                                            else
                                            {
                                                sims_4 = sims_4 + Convert.ToDouble(obj.sims_transport_period4);
                                            }

                                        }
                                        else if (k.Month == 5)
                                        {
                                            if (sims_5 == 0.0)
                                            {
                                                sims_5 = Convert.ToDouble(obj.sims_transport_period5);
                                            }
                                            else
                                            {
                                                sims_5 = sims_5 + Convert.ToDouble(obj.sims_transport_period5);
                                            }

                                        }
                                        else if (k.Month == 6)
                                        {
                                            if (sims_6 == 0.0)
                                            {
                                                sims_6 = Convert.ToDouble(obj.sims_transport_period6);
                                            }
                                            else
                                            {
                                                sims_6 = sims_6 + Convert.ToDouble(obj.sims_transport_period6);
                                            }

                                        }
                                        else if (k.Month == 7)
                                        {
                                            if (sims_7 == 0.0)
                                            {
                                                sims_7 = Convert.ToDouble(obj.sims_transport_period7);
                                            }
                                            else
                                            {
                                                sims_7 = sims_7 + Convert.ToDouble(obj.sims_transport_period7);
                                            }
                                        }
                                        else if (k.Month == 8)
                                        {
                                            if (sims_8 == 0.0)
                                            {
                                                sims_8 = Convert.ToDouble(obj.sims_transport_period8);
                                            }
                                            else
                                            {
                                                sims_8 = sims_8 + Convert.ToDouble(obj.sims_transport_period8);
                                            }

                                        }
                                        else if (k.Month == 9)
                                        {
                                            if (sims_9 == 0.0)
                                            {
                                                sims_9 = Convert.ToDouble(obj.sims_transport_period9);
                                            }
                                            else
                                            {
                                                sims_9 = sims_9 + Convert.ToDouble(obj.sims_transport_period9);
                                            }

                                        }
                                        else if (k.Month == 10)
                                        {
                                            if (sims_10 == 0.0)
                                            {
                                                sims_10 = Convert.ToDouble(obj.sims_transport_period10);
                                            }
                                            else
                                            {
                                                sims_10 = sims_10 + Convert.ToDouble(obj.sims_transport_period10);
                                            }

                                        }
                                        else if (k.Month == 11)
                                        {
                                            if (sims_11 == 0.0)
                                            {
                                                sims_11 = Convert.ToDouble(obj.sims_transport_period11);
                                            }
                                            else
                                            {
                                                sims_11 = sims_11 + Convert.ToDouble(obj.sims_transport_period11);
                                            }

                                        }
                                        else if (k.Month == 12)
                                        {
                                            if (sims_12 == 0.0)
                                            {
                                                sims_12 = Convert.ToDouble(obj.sims_transport_period12);
                                            }
                                            else
                                            {
                                                sims_12 = sims_12 + Convert.ToDouble(obj.sims_transport_period12);
                                            }

                                        }
                                        k = k.AddMonths(1);

                                        #endregion
                                    }

                                    obj.sims_paid_total = sims_1 + sims_2 + sims_3 + sims_4 + sims_5 + sims_6 + sims_7 + sims_8 + sims_9 + sims_10 + sims_11 + sims_12;
                                    sims_1 = 0; sims_2 = 0; sims_3 = 0; sims_4 = 0; sims_5 = 0; sims_6 = 0; sims_7 = 0; sims_8 = 0; sims_9 = 0; sims_10 = 0; sims_11 = 0; sims_12 = 0;
                                    #endregion

                                    StudentTrans_list.Add(obj);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        [Route("getStatusnew")]
        public HttpResponseMessage getStatusnew()
        {
            
            List<Sims525> StudentTrans_list = new List<Sims525>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();

                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A"),
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims525 obj = new Sims525();
                            obj.sims_get_status = dr["sims_appl_parameter"].ToString();
                            obj.sims_get_status_en = dr["sims_appl_form_field_value1"].ToString();
                            StudentTrans_list.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, StudentTrans_list);
        }

        [Route("UpdateTransportCUD")]
        public HttpResponseMessage UpdateTransportCUD(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;

                    foreach (Sims525 simobj in data)
                    {

                        sims_1 = double.Parse(simobj.sims_transport_period1);
                        sims_2 = double.Parse(simobj.sims_transport_period2);
                        sims_3 = double.Parse(simobj.sims_transport_period3);
                        sims_4 = double.Parse(simobj.sims_transport_period4);
                        sims_5 = double.Parse(simobj.sims_transport_period5);
                        sims_6 = double.Parse(simobj.sims_transport_period6);
                        sims_7 = double.Parse(simobj.sims_transport_period7);
                        sims_8 = double.Parse(simobj.sims_transport_period8);
                        sims_9 = double.Parse(simobj.sims_transport_period9);
                        sims_10 = double.Parse(simobj.sims_transport_period10);
                        sims_11 = double.Parse(simobj.sims_transport_period11);
                        sims_12 = double.Parse(simobj.sims_transport_period12);

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_proc]",
                            new List<SqlParameter>()
                        {

                          new SqlParameter("@opr",simobj.opr),
                          new SqlParameter("@sub_opr",simobj.sub_opr),
                          new SqlParameter("@sims_transport_route_student_status",simobj.sims_fee_period1),
                          new SqlParameter("@sims_transport_route_student_update_status",simobj.sims_fee_period2),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),

                          new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),

                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),

                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),

                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12),
                          new SqlParameter("@sims_pu7",simobj.sims_transport_toatal),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)
                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        [Route("UpdateTransportCUDNewFeeStructure")]
        public HttpResponseMessage UpdateTransportCUDNewFeeStructure(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;
                    //simobj.opr
                   
                    foreach (Sims525 simobj in data)
                    {

                       string fDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                        DateTime fpDate = DateTime.Parse(fDate);
                        string uDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old.ToString());
                        DateTime upDate = DateTime.Parse(uDate);
                        for (DateTime k = fpDate; k <= upDate;)
                        {
                            #region

                            if (k.Month == 1)
                            {
                                if (sims_1==0.0)
                                {
                                    sims_1 = Convert.ToDouble(simobj.sims_transport_period1); 
                                }
                                else
                                {
                                    sims_1 = sims_1+Convert.ToDouble(simobj.sims_transport_period1);
                                }
                            }
                            else if (k.Month == 2)
                            {
                                if (sims_2 == 0.0)
                                {
                                    sims_2 = Convert.ToDouble(simobj.sims_transport_period2);
                                }
                                else
                                {
                                    sims_2 = sims_2 + Convert.ToDouble(simobj.sims_transport_period2);
                                }
                                
                            }
                            else if (k.Month == 3)
                            {
                                if (sims_3 == 0.0)
                                {
                                    sims_3 = Convert.ToDouble(simobj.sims_transport_period3);
                                }
                                else
                                {
                                    sims_3 = sims_3 + Convert.ToDouble(simobj.sims_transport_period3);
                                }
                                
                            }
                            else if (k.Month == 4)
                            {
                                if (sims_4 == 0.0)
                                {
                                    sims_4 = Convert.ToDouble(simobj.sims_transport_period4);
                                }
                                else
                                {
                                    sims_4 = sims_4 + Convert.ToDouble(simobj.sims_transport_period4);
                                }
                                
                            }
                            else if (k.Month == 5)
                            {
                                if (sims_5 == 0.0)
                                {
                                    sims_5 = Convert.ToDouble(simobj.sims_transport_period5);
                                }
                                else
                                {
                                    sims_5 = sims_5 + Convert.ToDouble(simobj.sims_transport_period5);
                                }
                                
                            }
                            else if (k.Month == 6)
                            {
                                if (sims_6 == 0.0)
                                {
                                    sims_6 = Convert.ToDouble(simobj.sims_transport_period6);
                                }
                                else
                                {
                                    sims_6 = sims_6 + Convert.ToDouble(simobj.sims_transport_period6);
                                }
                                
                            }
                            else if (k.Month == 7)
                            {
                                if (sims_7 == 0.0)
                                {
                                    sims_7 = Convert.ToDouble(simobj.sims_transport_period7);
                                }
                                else
                                {
                                    sims_7 = sims_7 + Convert.ToDouble(simobj.sims_transport_period7);
                                }
                                
                            }
                            else if (k.Month == 8)
                            {
                                if (sims_8 == 0.0)
                                {
                                    sims_8 = Convert.ToDouble(simobj.sims_transport_period8);
                                }
                                else
                                {
                                    sims_8 = sims_8 + Convert.ToDouble(simobj.sims_transport_period8);
                                }
                                
                            }
                            else if (k.Month == 9)
                            {
                                if (sims_9 == 0.0)
                                {
                                    sims_9 = Convert.ToDouble(simobj.sims_transport_period9);
                                }
                                else
                                {
                                    sims_9 = sims_9 + Convert.ToDouble(simobj.sims_transport_period9);
                                }
                                
                            }
                            else if (k.Month == 10)
                            {
                                if (sims_10 == 0.0)
                                {
                                    sims_10 = Convert.ToDouble(simobj.sims_transport_period10);
                                }
                                else
                                {
                                    sims_10 = sims_10 + Convert.ToDouble(simobj.sims_transport_period10);
                                }
                                
                            }
                            else if (k.Month == 11)
                            {
                                if (sims_11 == 0.0)
                                {
                                    sims_11 = Convert.ToDouble(simobj.sims_transport_period11);
                                }
                                else
                                {
                                    sims_11 = sims_11 + Convert.ToDouble(simobj.sims_transport_period11);
                                }
                                
                            }
                            else if (k.Month == 12)
                            {
                                if (sims_12 == 0.0)
                                {
                                    sims_12 = Convert.ToDouble(simobj.sims_transport_period12);
                                }
                                else
                                {
                                    sims_12 = sims_12 + Convert.ToDouble(simobj.sims_transport_period12);
                                }                                
                            }
                            k = k.AddMonths(1);

                            #endregion
                        }
                        //sims_1 = double.Parse(simobj.sims_transport_period1);
                        //sims_2 = double.Parse(simobj.sims_transport_period2);
                        //sims_3 = double.Parse(simobj.sims_transport_period3);
                        //sims_4 = double.Parse(simobj.sims_transport_period4);
                        //sims_5 = double.Parse(simobj.sims_transport_period5);
                        //sims_6 = double.Parse(simobj.sims_transport_period6);
                        //sims_7 = double.Parse(simobj.sims_transport_period7);
                        //sims_8 = double.Parse(simobj.sims_transport_period8);
                        //sims_9 = double.Parse(simobj.sims_transport_period9);
                        //sims_10 = double.Parse(simobj.sims_transport_period10);
                        //sims_11 = double.Parse(simobj.sims_transport_period11);
                        //sims_12 = double.Parse(simobj.sims_transport_period12);

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_proc]",
                            new List<SqlParameter>()
                        {

                          new SqlParameter("@opr","F"),
                          new SqlParameter("@sub_opr",simobj.sub_opr),
                          new SqlParameter("@sims_transport_route_student_status",simobj.sims_fee_period1),
                          new SqlParameter("@sims_transport_route_student_update_status",simobj.sims_fee_period2),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),

                          new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),

                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),

                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          new SqlParameter("@sims_transport_route_student_update_by",simobj.sims_transport_route_student_update_by),
                          
                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12),
                          new SqlParameter("@sims_pu7",simobj.sims_transport_toatal),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number)
                          
                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }


        [Route("UpdateTransportCUDNewFeeStructure_ASIS")]
        public HttpResponseMessage UpdateTransportCUDNewFeeStructure_ASIS(List<Sims525> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportRoute", routeobj));
            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    double sims_1 = 0, sims_2 = 0, sims_3 = 0, sims_4 = 0, sims_5 = 0, sims_6 = 0, sims_7 = 0, sims_8 = 0, sims_9 = 0, sims_10 = 0, sims_11 = 0, sims_12 = 0;
                    //simobj.opr

                    foreach (Sims525 simobj in data)
                    {

                        string fDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old.ToString());
                        DateTime fpDate = DateTime.Parse(fDate);
                        string uDate = db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old.ToString());
                        DateTime upDate = DateTime.Parse(uDate);
                        for (DateTime k = fpDate; k <= upDate;)
                        {
                            #region

                            if (k.Month == 1)
                            {
                                if (sims_1 == 0.0)
                                {
                                    sims_1 = Convert.ToDouble(simobj.sims_transport_period1);
                                }
                                else
                                {
                                    sims_1 = sims_1 + Convert.ToDouble(simobj.sims_transport_period1);
                                }
                            }
                            else if (k.Month == 2)
                            {
                                if (sims_2 == 0.0)
                                {
                                    sims_2 = Convert.ToDouble(simobj.sims_transport_period2);
                                }
                                else
                                {
                                    sims_2 = sims_2 + Convert.ToDouble(simobj.sims_transport_period2);
                                }

                            }
                            else if (k.Month == 3)
                            {
                                if (sims_3 == 0.0)
                                {
                                    sims_3 = Convert.ToDouble(simobj.sims_transport_period3);
                                }
                                else
                                {
                                    sims_3 = sims_3 + Convert.ToDouble(simobj.sims_transport_period3);
                                }

                            }
                            else if (k.Month == 4)
                            {
                                if (sims_4 == 0.0)
                                {
                                    sims_4 = Convert.ToDouble(simobj.sims_transport_period4);
                                }
                                else
                                {
                                    sims_4 = sims_4 + Convert.ToDouble(simobj.sims_transport_period4);
                                }

                            }
                            else if (k.Month == 5)
                            {
                                if (sims_5 == 0.0)
                                {
                                    sims_5 = Convert.ToDouble(simobj.sims_transport_period5);
                                }
                                else
                                {
                                    sims_5 = sims_5 + Convert.ToDouble(simobj.sims_transport_period5);
                                }

                            }
                            else if (k.Month == 6)
                            {
                                if (sims_6 == 0.0)
                                {
                                    sims_6 = Convert.ToDouble(simobj.sims_transport_period6);
                                }
                                else
                                {
                                    sims_6 = sims_6 + Convert.ToDouble(simobj.sims_transport_period6);
                                }

                            }
                            else if (k.Month == 7)
                            {
                                if (sims_7 == 0.0)
                                {
                                    sims_7 = Convert.ToDouble(simobj.sims_transport_period7);
                                }
                                else
                                {
                                    sims_7 = sims_7 + Convert.ToDouble(simobj.sims_transport_period7);
                                }

                            }
                            else if (k.Month == 8)
                            {
                                if (sims_8 == 0.0)
                                {
                                    sims_8 = Convert.ToDouble(simobj.sims_transport_period8);
                                }
                                else
                                {
                                    sims_8 = sims_8 + Convert.ToDouble(simobj.sims_transport_period8);
                                }

                            }
                            else if (k.Month == 9)
                            {
                                if (sims_9 == 0.0)
                                {
                                    sims_9 = Convert.ToDouble(simobj.sims_transport_period9);
                                }
                                else
                                {
                                    sims_9 = sims_9 + Convert.ToDouble(simobj.sims_transport_period9);
                                }

                            }
                            else if (k.Month == 10)
                            {
                                if (sims_10 == 0.0)
                                {
                                    sims_10 = Convert.ToDouble(simobj.sims_transport_period10);
                                }
                                else
                                {
                                    sims_10 = sims_10 + Convert.ToDouble(simobj.sims_transport_period10);
                                }

                            }
                            else if (k.Month == 11)
                            {
                                if (sims_11 == 0.0)
                                {
                                    sims_11 = Convert.ToDouble(simobj.sims_transport_period11);
                                }
                                else
                                {
                                    sims_11 = sims_11 + Convert.ToDouble(simobj.sims_transport_period11);
                                }

                            }
                            else if (k.Month == 12)
                            {
                                if (sims_12 == 0.0)
                                {
                                    sims_12 = Convert.ToDouble(simobj.sims_transport_period12);
                                }
                                else
                                {
                                    sims_12 = sims_12 + Convert.ToDouble(simobj.sims_transport_period12);
                                }
                            }
                            k = k.AddMonths(1);

                            #endregion
                        }
                        //sims_1 = double.Parse(simobj.sims_transport_period1);
                        //sims_2 = double.Parse(simobj.sims_transport_period2);
                        //sims_3 = double.Parse(simobj.sims_transport_period3);
                        //sims_4 = double.Parse(simobj.sims_transport_period4);
                        //sims_5 = double.Parse(simobj.sims_transport_period5);
                        //sims_6 = double.Parse(simobj.sims_transport_period6);
                        //sims_7 = double.Parse(simobj.sims_transport_period7);
                        //sims_8 = double.Parse(simobj.sims_transport_period8);
                        //sims_9 = double.Parse(simobj.sims_transport_period9);
                        //sims_10 = double.Parse(simobj.sims_transport_period10);
                        //sims_11 = double.Parse(simobj.sims_transport_period11);
                        //sims_12 = double.Parse(simobj.sims_transport_period12);

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_fee_approval_by_school_asis_proc]",
                            new List<SqlParameter>()
                        {

                          new SqlParameter("@opr","F"),
                          new SqlParameter("@sub_opr",simobj.sub_opr),
                          new SqlParameter("@sims_transport_route_student_status",simobj.sims_fee_period1),
                          new SqlParameter("@sims_transport_route_student_update_status",simobj.sims_fee_period2),

                          new SqlParameter("@sims_transport_academic_year",simobj.sims_transport_academic_year),
                          new SqlParameter("@sims_transport_enroll_number",simobj.sims_transport_enroll_number),

                          new SqlParameter("@sims_cur_code",simobj.sims_fee_cur_code),
                          new SqlParameter("@sims_grade_code",simobj.sims_fee_grade_code),
                          new SqlParameter("@sims_section_code",simobj.sims_fee_section_code),

                          new SqlParameter("@sims_transport_route_student_code",simobj.sims_transport_route_student_code),
                          new SqlParameter("@sims_appl_parameter_712",simobj.sims_appl_parameter),

                          new SqlParameter("@sims_transport_route_code_old",simobj.sims_transport_route_code_old),
                          new SqlParameter("@sims_transport_route_direction_old",simobj.sims_transport_route_direction_old),
                          new SqlParameter("@sims_transport_effective_from_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_from_old)),
                          new SqlParameter("@sims_transport_effective_upto_old",db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto_old)),
                          new SqlParameter("@sims_transport_route_code",simobj.sims_transport_route_code),
                          new SqlParameter("@sims_transport_route_direction",simobj.sims_transport_route_direction),
                          new SqlParameter("@sims_transport_effective_from", db.DBYYYYMMDDformat(simobj.sims_transport_effective_from)),
                          new SqlParameter("@sims_transport_effective_upto", db.DBYYYYMMDDformat(simobj.sims_transport_effective_upto)),
                          new SqlParameter("@sims_transport_pickup_stop_code",simobj.sims_transport_pickup_stop_code),
                          new SqlParameter("@sims_transport_drop_stop_code",simobj.sims_transport_drop_stop_code),
                          new SqlParameter("@sims_transport_pickup_stop_code_old", simobj.sims_transport_pickup_stop_code_old),
                          new SqlParameter("@sims_transport_drop_stop_code_old",simobj.sims_transport_drop_stop_code_old),
                          new SqlParameter("@sims_transport_vehicle_code_odl",simobj.V_Code),
                          new SqlParameter("@sims_transport_vehicle_code",simobj.V_Name_Code),
                          new SqlParameter("@sims_transport_route_student_update_by",simobj.sims_transport_route_student_update_by),

                          new SqlParameter("@sims_1",sims_1),
                          new SqlParameter("@sims_2",sims_2),
                          new SqlParameter("@sims_3",sims_3),
                          new SqlParameter("@sims_4",sims_4),
                          new SqlParameter("@sims_5",sims_5),
                          new SqlParameter("@sims_6",sims_6),
                          new SqlParameter("@sims_7",sims_7),
                          new SqlParameter("@sims_8",sims_8),
                          new SqlParameter("@sims_9",sims_9),
                          new SqlParameter("@sims_10",sims_10),
                          new SqlParameter("@sims_11",sims_11),
                          new SqlParameter("@sims_12",sims_12),
                          new SqlParameter("@sims_pu7",simobj.sims_transport_toatal),
                          new SqlParameter("@sims_transport_fee_number_old",simobj.sims_fee_number),
                          new SqlParameter("@sims_paid_total",simobj.sims_paid_total)
                    });
                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;

                        }
                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion


    }
}