﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.Common;
using SIMSAPI.Models.SIMS.simsClass;
using SIMSAPI.Models.ParentClass;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data.SqlClient;
using System.Data;
using log4net;
using SIMSAPI.Models.ERP.messageClass;
using System.Configuration;

namespace SIMSAPI.Controllers.Fleet
{
    [RoutePrefix("api/TransportRouteStop")]
    public class TransportRouteStopController:ApiController
    {

        #region Api For Transport Stop...

        [Route("getRouteStop")]
        public HttpResponseMessage getRouteStop()
        {
            List<Sims085> RouteStop = new List<Sims085>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_route_stop_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sims085 obj = new Sims085();
                            obj.sims_academic_year = dr["sims_academic_year"].ToString();
                            obj.sims_academic_year_description = dr["sims_academic_year_description"].ToString();
                            obj.sims_transport_route_name = dr["sims_transport_route_name"].ToString();
                            obj.sims_transport_route_code = dr["sims_transport_route_code"].ToString();
                            obj.sims_transport_stop_name = dr["sims_transport_stop_name"].ToString();
                            obj.sims_transport_route_stop_code = dr["sims_transport_route_stop_code"].ToString();
                            obj.sims_transport_route_direction_name = dr["sims_transport_route_direction_name"].ToString();
                            obj.sims_transport_route_direction = dr["sims_transport_route_direction"].ToString();
                            obj.sims_transport_route_stop_expected_time = dr["sims_transport_route_stop_expected_time"].ToString();
                            obj.sims_transport_route_stop_waiting_time = dr["sims_transport_route_stop_waiting_time"].ToString();
                            obj.sims_transport_route_stop_status = dr["sims_transport_route_stop_status"].ToString().Equals("A") ? true : false;
                            RouteStop.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, RouteStop);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, RouteStop);
        }

        [Route("CUDRoutetStop")]
        public HttpResponseMessage CUDRoutetStop(List<Sims085> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDRoutetStop", rsobj));
            bool inserted = false;
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Sims085 rsobj in data)
                    {
                        if (rsobj.sims_transport_route_stop_expected_time == "")
                        {
                            rsobj.sims_transport_route_stop_expected_time = null;
                        }
                        if (rsobj.sims_transport_route_stop_waiting_time == "")
                        {
                            rsobj.sims_transport_route_stop_waiting_time = null;
                        }

                        int ins = db.ExecuteStoreProcedureforInsert("[sims].[sims_transport_route_stop_proc]",
                            new List<SqlParameter>()
                            {
                               new SqlParameter("@opr",rsobj.opr),
                               new SqlParameter("@sims_academic_year",rsobj.sims_academic_year),
                               new SqlParameter("@sims_transport_route_code", rsobj.sims_transport_route_code),
                               new SqlParameter("@sims_transport_route_stop_code",rsobj.sims_transport_route_stop_code),
                               new SqlParameter("@sims_transport_route_direction", rsobj.sims_transport_route_direction),
                               new SqlParameter("@sims_transport_route_stop_expected_time",rsobj.sims_transport_route_stop_expected_time),
                               new SqlParameter("@sims_transport_route_stop_waiting_time", rsobj.sims_transport_route_stop_waiting_time),
                               new SqlParameter("@sims_transport_route_stop_status",rsobj.sims_transport_route_stop_status==true?"A":"I")
                            });

                        if (ins > 0)
                        {
                            inserted = true;
                        }
                        else
                        {
                            inserted = false;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, x.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, inserted);
        }

        #endregion

        #region Api For Transport Vehicle Expense...

        /*
        [Route("getVehicleName")]
        public HttpResponseMessage getVehicleName()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "S")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_vehicle_code = dr["sims_transport_vehicle_code"].ToString();
                            obj.fvex_vehicle_name = dr["sims_transport_vehicle_name_plate"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("getVehicleExpanseDetails")]
        public HttpResponseMessage getVehicleExpanseDetails()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "A")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_transaction_number = dr["fvex_transaction_number"].ToString();
                            obj.fvex_vehicle_code = dr["fvex_vehicle_code"].ToString();
                            obj.fvex_expense_type = dr["fvex_expense_type"].ToString();
                            obj.fvex_expense_amount = dr["fvex_expense_amount"].ToString();
                            obj.fvex_vechile_expense_date = dr["fvex_vechile_expense_date"].ToString();
                            obj.fvex_dd_cheque_bank_name = dr["fvex_dd_cheque_bank_name"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("getBankName")]
        public HttpResponseMessage getBankName()
        {
            List<Fins234> VehicleName = new List<Fins234>();
            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                        new List<SqlParameter>()
                         {
                            new SqlParameter("@opr", "B")
                         }
                         );
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Fins234 obj = new Fins234();
                            obj.fvex_dd_cheque_bank_code = dr["pb_bank_code"].ToString();
                            obj.fvex_dd_cheque_bank_name = dr["pb_bank_name"].ToString();
                            VehicleName.Add(obj);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
            }
            catch (Exception x) { }
            return Request.CreateResponse(HttpStatusCode.OK, VehicleName);
        }

        [Route("CUDVehicleExpense")]
        public HttpResponseMessage CUDVehicleExpense(List<Fins234> data)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : insertSuggestion(),PARAMETERS :: OBJ {2}";
            //Log.Debug(string.Format(debug, "PP", "CUDTransportStop", simobj));

            Message msg = new Message();
            string st = string.Empty;
            bool inserted = false;

            try
            {
                using (DBConnection db = new DBConnection())
                {
                    db.Open();
                    foreach (Fins234 simobj in data)
                    {
                        if (simobj.fvex_dd_cheque_number == "undefined")
                        {
                            simobj.fvex_dd_cheque_number = null;
                        }
                        if (simobj.fvex_dd_cheque_due_date == "undefined")
                        {
                            simobj.fvex_dd_cheque_due_date = null;
                        }
                        if (simobj.fvex_dd_cheque_bank_code == "undefined")
                        {
                            simobj.fvex_dd_cheque_bank_code = null;
                        }
                        if (simobj.fvex_cc_number == "undefined")
                        {
                            simobj.fvex_cc_number = null;
                        }

                        SqlDataReader dr = db.ExecuteStoreProcedure("[sims].[sims_transport_vehicle_expense_proc]",
                             new List<SqlParameter>()
                            {
                                 new SqlParameter("@opr",simobj.opr),
                                 new SqlParameter("@fvex_vechile_expense_date",simobj.fvex_vechile_expense_date),
                                 new SqlParameter("@fvex_vehicle_code", simobj.fvex_vehicle_code),
                                 new SqlParameter("@fvex_expense_type",simobj.fvex_expense_type),
                                 new SqlParameter("@fvex_expense_amount", simobj.fvex_expense_amount),
                                 new SqlParameter("@fvex_expense_remark",simobj.fvex_expense_remark),
                                 new SqlParameter("@fvex_expense_creation_user", simobj.fvex_expense_creation_user),
                                 new SqlParameter("@fvex_dd_cheque_number",simobj.fvex_dd_cheque_number),
                                 new SqlParameter("@fvex_dd_cheque_due_date",simobj.fvex_dd_cheque_due_date),
                                 new SqlParameter("@fvex_dd_cheque_bank_code",simobj.fvex_dd_cheque_bank_code),
                                 new SqlParameter("@fvex_cc_number",simobj.fvex_cc_number),

                            });

                        if (dr.Read())
                        {
                            st = dr[0].ToString();
                            msg.strMessage = st;
                        }

                        if (dr.RecordsAffected > 0)
                        {
                            inserted = true;
                        }
                    }
                }
            }
            catch (Exception x)
            {
                // Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            }
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
        */

        #endregion

    }
}