﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web.Configuration;
using System.Web;

namespace SIMSAPI.Controllers.setup
{
    [RoutePrefix("api/file1")]
    public class FileUploadController : ApiController
    {
        [HttpPost(), Route("upload")]
        public string UploadFiles(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];




            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        string type = Path.GetFileName(hpf.ContentType);
                        //  file = filename + "." + type;
                        file = filename + ".png";
                        //  hpf.SaveAs((root + filename + "." + type));
                        hpf.SaveAs((root + filename + ".png"));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;



                    }
                }
            }
            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }


        [HttpPost(), Route("uploadDocument")]
        public string uploadDocument(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];



            string path = "~/Content/" + location + "/";
            //string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);


            //Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName);
                    var v = Path.GetFullPath(hpf.FileName);

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));
                        type = Path.GetFileName(hpf.ContentType);
                        file = filename + "." + type;
                        //file = filename + ".png";
                        hpf.SaveAs((root + filename + "." + type));
                        //  hpf.SaveAs((root + filename + ".png" ));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }

        [HttpPost(), Route("uploadDocument_Att")]
        public string uploadDocument_Att(string filename, string location)
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES. //http://sis.mograsys.com/Images/
            string sPath = ""; string fname = ""; string file = ""; string type = "";

            //sPath = WebConfigurationManager.AppSettings["webPath"];

            //string path = "~/Content/" + location + "/";
            string path = "~/Content/" + HttpContext.Current.Request.Headers["schoolId"].ToString() + "/" + location + "/";

            string root = HttpContext.Current.Server.MapPath(path);

            //Directory.CreateDirectory(root);

            // sPath = System.Web.Hosting.HostingEnvironment.MapPath(WebConfigurationManager.AppSettings["webPath"]);   //"~/App_Data/img/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.

            for (int iCnt = 0; iCnt < hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    var s = Path.GetFileName(hpf.FileName.Trim());
                    var v = Path.GetFullPath(hpf.FileName.Trim());

                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists((sPath + Path.GetFileName(hpf.FileName))))
                    {
                        //FileInfo f = new FileInfo(sPath + Path.GetFileName(hpf.FileName));
                        //  f.CopyTo(sPath + Path.GetFileName(hpf.FileName));
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs((root + Path.GetFileName(hpf.FileName)));


                        //type = Path.GetFileName(hpf.ContentType);
                        type = Path.GetFileName(hpf.FileName.Substring(hpf.FileName.LastIndexOf('.') + 1));
                        //filename = hpf.FileName.Substring(0, hpf.FileName.LastIndexOf('.'));
                       // file = filename.Trim() + "." + type;
                        file = filename.Trim();
                       // hpf.SaveAs((root + filename.Trim() + "." + type));
                        hpf.SaveAs((root + filename.Trim()));
                        fname = Path.GetFileName(hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;


                        //file = s;
                        //hpf.SaveAs((root + s));
                        ////fname = Path.GetFileName(hpf.FileName);
                        //iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return file;
            }
            else
            {
                return "false";
            }
        }
    }
}
