﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SIMSAPI.Models.ParentClass;
using System.Data.SqlClient;
using SIMSAPI.Attributes;
using SIMSAPI.Helper;
using System.Data;
using System.IO;
using System.Web;
using log4net;
using SIMSAPI.Models.SIMS.simsClass;


namespace SIMSRestAPI.Controllers.ParentPortal
{
    [RoutePrefix("api/uploadimages")]
    [BasicAuthentication]
    public class UploadImageController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getActiveParentImage")]
        public HttpResponseMessage getActiveParentImage(string parentCode)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getActiveParentImage(),PARAMETERS :: PARENTID {2}";
            Log.Debug(string.Format(debug, "PP", "UPLOADIMAGE", parentCode));

            admissionClass pdata = new admissionClass();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                  
                    var ds = new DataSet();
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("pp_sims_parent", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "J"),
                        new SqlParameter("@sims_parent_number", parentCode)
                    });
                    if (dr.HasRows)
                    {
                        dr.Read();
                        pdata.sims_admisison_father_image = dr["Image"].ToString();
                        pdata.active_member = dr["sims_student_primary_contact_pref"].ToString();
                        pdata.sims_admission_father_full_name = dr["Name"].ToString();

                        if (!string.IsNullOrEmpty(pdata.sims_admission_father_full_name))
                        {
                        //    string [] s=pdata.sims_admission_father_full_name.Split(' ');
                        //string last_nm =  s[s.Length - 1];
                            pdata.sims_admission_father_full_name = pdata.sims_admission_father_full_name.Split(' ')[0].ToString();

                                                 }

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, pdata);

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, pdata);
            }
        }


        [Route("getSiblingImage")]
        public HttpResponseMessage getSiblingImage(string parentCode)
        {

            string debug = "MODULE :{0},APPLICATION :{1},METHOD : getSiblingImage(),PARAMETERS :: PARENTID {2}";
            Log.Debug(string.Format(debug, "PP", "UPLOADIMAGE", parentCode));
            List<parentClass> mod_list = new List<parentClass>();
            try
            {

                using (DBConnection db = new DBConnection())
                {
                    var ds = new DataSet();
                    db.Open();
                    SqlDataReader dr = db.ExecuteStoreProcedure("sims_parent", new List<SqlParameter>()
                    {
                        new SqlParameter("@opr", "P"),
                        new SqlParameter("@sims_parent_number", parentCode)
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            parentClass ppobj = new parentClass();
                            ppobj.sims_cur_code = dr["sims_student_cur_code"].ToString();
                            ppobj.sims_grade_code = dr["sims_grade_code"].ToString();
                            ppobj.sims_section_code = dr["sims_section_code"].ToString();
                            ppobj.sims_acad_yr = dr["sims_academic_year"].ToString();
                            ppobj.sims_sibling_student_enroll_number = dr["sims_sibling_student_enroll_number"].ToString();
                            ppobj.sims_student_img = dr["sims_student_img"].ToString();
                            ppobj.sims_student_img_path = ppobj.sims_student_img.ToString();
                            ppobj.sims_student_full_name = dr["Name_in_English"].ToString();
                            ppobj.status = dr["status"].ToString();
                            mod_list.Add(ppobj);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, mod_list);

            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, mod_list);
            }
        }

        [Route("updateImage")]
        public HttpResponseMessage updateImage(string filename)
        {
            string debug = "MODULE :{0},APPLICATION :{1},METHOD : updateImage(),PARAMETERS :: filename {2}";
            Log.Debug(string.Format(debug, "PP", "UPLOADIMAGE", filename));

            string path = HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath)+"//";
          
            DirectoryInfo di = new DirectoryInfo(filename);
            string filepath=string.Format("{0}{1}", path, di.Name);
            try
            {
                
                if (!File.Exists(filepath))
                {
                    File.Copy(filename, string.Format("{0}{1}", path, di.Name));
                }
                else
                {
                    File.Copy(filename, string.Format("{0}{1}", path, di.Name), true);
                }
            }
            catch (Exception x)
            {
                Log.Error(x);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "");
                
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "OK");

        }

        //public ActionResult UploadFile()
        //{
        //    var file = Request.Files[0];
        //    var path = Path.Combine(Server.MapPath("~/Photos/") + file.FileName);
        //    file.SaveAs(path);

        //    // prepare a relative path to be stored in the database and used to display later on.
        //    path = Url.Content(Path.Combine("~/Photos/" + file.FileName));
        //    // save to db
        //    return Json(path.ToString(), JsonRequestBehavior.AllowGet);

        //}
    }
}