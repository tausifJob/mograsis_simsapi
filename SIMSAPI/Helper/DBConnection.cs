﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace SIMSAPI.Helper
{
    public class DBConnection : IDisposable
    {
        private SqlConnection sqlConn = null;
        public bool IsConnected { get; set; }

        public DBConnection()
        {
            //  HttpContext.Current.Request.Headers

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Headers["schoolId"].ToString()))
            {
                string str = HttpContext.Current.Request.Headers["schoolId"].ToString();
                sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings[str].ConnectionString);
            }
            else
            {
                var connectionString = ConfigurationManager.ConnectionStrings["databaseConnectionString"].ConnectionString;
                sqlConn = new SqlConnection(connectionString);
            }
            IsConnected = false;
        }
       
        public DBConnection(string connectionString)
        {
            sqlConn = new SqlConnection(connectionString);
            IsConnected = false;
        }

        public void Dispose()
        {
            if (sqlConn.State != ConnectionState.Closed)
                sqlConn.Close();
            sqlConn.Dispose();
            IsConnected = false;
        }

        public bool Open()
        {
            try
            {
                sqlConn.Open();
                IsConnected = (sqlConn.State == ConnectionState.Open);
                return true;
            }
            catch (Exception)
            {
                IsConnected = false;
                return false;
            }
        }

        public bool SelectStatement(string sqlQuery, out DataSet returnData, List<SqlParameter> paraCollection = null)
        {
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                var objCom = new SqlCommand(sqlQuery, sqlConn);
                var objData = new SqlDataAdapter(objCom);
                if (paraCollection != null)
                {
                    objCom.Parameters.Clear();
                    objCom.Parameters.AddRange(paraCollection.ToArray());
                }
                returnData = new DataSet();
                objData.Fill(returnData);

                return true;
            }
            catch (Exception ex)
            {
                returnData = null;
                throw ex;
            }
        }

        public SqlDataReader ExecuteStoreProcedure(string procedureName, List<SqlParameter> paraCollection)
        {
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                var objCom = new SqlCommand(procedureName, sqlConn);
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandTimeout = 300;
                objCom.Parameters.AddRange(paraCollection.ToArray());
                return objCom.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlDataReader ExecuteStoreProcedure(string procedureName, List<SqlParameter> paraCollection, out SqlCommand command)
        {
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                command = new SqlCommand(procedureName, sqlConn);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;
                command.Parameters.AddRange(paraCollection.ToArray());
                return command.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ExecuteStoreProcedureforInsert(string procedureName, List<SqlParameter> paraCollection)
        {
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                var objCom = new SqlCommand(procedureName, sqlConn);
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandTimeout = 300;
                objCom.Parameters.AddRange(paraCollection.ToArray());
                return objCom.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }



        public DataSet ExecuteStoreProcedureDS(string procedureName, List<SqlParameter> paraCollection)
        {
            try
            {
                DataSet ds = new DataSet();

                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                var objCom = new SqlCommand(procedureName, sqlConn);
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandTimeout = 300;
                objCom.Parameters.AddRange(paraCollection.ToArray());

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = objCom;

                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExecuteStatement(string sqlQuery, List<SqlParameter> paraCollection, out int Count)
        {
            Count = 0;
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                var objCom = new SqlCommand(sqlQuery, sqlConn);
                objCom.Parameters.AddRange(paraCollection.ToArray());
                objCom.CommandTimeout = 300;
                Count = objCom.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        #region
        public string UIDDMMYYYYformat(string dt)
        {
            string dbgetdate = dt;

            string uigetformatDate = null;

            if (!string.IsNullOrEmpty(dbgetdate))
            {
                DateTime uigetDate = Convert.ToDateTime(dbgetdate);
                ////string uigetday = uigetDate.Day.ToString();
                ////string uigetmonth = uigetDate.Month.ToString();
                ////string uigetyear = uigetDate.Year.ToString();
                ////uigetformatDate = Convert.ToDateTime(uigetday + "-" + uigetmonth + "-" + uigetyear).ToString("dd-MM-yyyy");
                uigetformatDate = uigetDate.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
                // var year = dbgetdate.Split('-')[0].Substring(0, 4);
                //var month = dbgetdate.Split('-')[1].Substring(0, 2);
                //var day = dbgetdate.Split('-')[2].Substring(0,2);

                //uigetformatDate = day + "-" + month + "-" + year;
                // uigetformatDate = dt;
            }
            else
            {
                return uigetformatDate;
            }

            return uigetformatDate;
        }
        #endregion
        //Date Format dd-mm-yyyy to yyyy-mm-dd
        #region Date_Convert_Function_Client
        public string DBYYYYMMDDformat(string dt)
        {
            var uigetdate = dt;
            string dbgetformatDate = null;
            if (uigetdate == null || uigetdate == "" || uigetdate == "N/A")
            {
                return dbgetformatDate;
            }
            else
            {
                var day = uigetdate.Split('-')[0];
                var month = uigetdate.Split('-')[1];
                var year = uigetdate.Split('-')[2];

                dbgetformatDate = year + "-" + month + "-" + day;


            }
            return dbgetformatDate;
        }
        #endregion
    }
}
