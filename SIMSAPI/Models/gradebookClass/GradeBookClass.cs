﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.gradebookClass
{

    #region Student-Report-Card-Comment
    public class RCstudent_report_card_sub_comment
    {
        public string sims_sub_comment { get; set; }
        public string sims_sub_comment_code { get; set; }
        public string sims_type { get; set; }
        public string sims_student_comment_header_id { get; set; }
        public string sims_comment_code { get; set; }
        public string sims_root_comment_id { get; set; }
    }

    public class RCstudent_report_card_level1_comment
    {
        public RCstudent_report_card_level1_comment()
        {
            sub_comments = new List<RCstudent_report_card_sub_comment>();
        }
        public string sims_comment { get; set; }
        public bool sims_comment_status { get; set; }

        public string sims_comment_code { get; set; }
        public string sims_type { get; set; }


        public List<RCstudent_report_card_sub_comment> sub_comments { get; set; }

        public string sims_student_comment_header_id { get; set; }
    }


    public class gradebook_copy_new
    {
        public string sims_grade_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }

        public List<sectionlist> sec_list = new List<sectionlist>();

    }

    public class sectionlist
    {

        public string grade_section { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_section_code { get; set; }

        public List<gradebooklist> gb_lst = new List<gradebooklist>();

    }

    public class gradebooklist
    {

        public string sims_gb_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_subject_code { get; set; }

        public List<GB_Category_list> gb_cat_lst = new List<GB_Category_list>();
    }

    public class GB_Category_list
    {

        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_subject_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }

        public List<gb_cat_assign_list> assign_lst = new List<gb_cat_assign_list>();

    }

    public class gb_cat_assign_list
    {

        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
    }

    public class assessment_student
    {
        public string grade_section_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string month_name { get; set; }
        public string Week_name { get; set; }
        public string sims_subject_name { get; set; }
        public string assessment_syllabus { get; set; }
        public string grade_name { get; set; }
        public string sims_assignment_name { get; set; }
        public string sims_gb_cat_name { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string Stud_Full_Name { get; set; }
        public string student_full_Name { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string new_marks { get; set; }
        public string old_marks { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public int StudEnroll1 { get; set; }
        public List<student_grade_scale> scalelist = new List<student_grade_scale>();
        public string sims_subject_code { get; set; }
    }

    public class student_grade_scale
    {

        public string sims_mark_grade_code { get; set; }
        public string sims_mark_grade { get; set; }
        public string sims_mark_grade_description { get; set; }
        public string sims_gb_cat_assign_final_grade { get; set; }
        public bool ischecked { get; set; }
    }


    public class RCstudent_report_card_comment
    {

        public RCstudent_report_card_comment()
        {
            coments_lst = new List<RCstudent_report_card_level1_comment>();
        }
        public string sims_type { get; set; }
        public List<RCstudent_report_card_level1_comment> coments_lst { get; set; }
        public string sims_report_card_level { get; set; }
        public string sims_report_card_level_name { get; set; }
        public string sims_report_card { get; set; }
        public string sims_report_card_name { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_gradebook_code { get; set; }
        public string sims_gradebook_name { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_Name { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_comment_type { get; set; }
        public string sims_comment_type_name { get; set; }
        public string sims_student_comment_header_id { get; set; }
        public string sims_comment_header_desc { get; set; }
        public string ReportCardComment { get; set; }
        public string sims_student_comment_id { get; set; }
        public bool sims_student_check_status { get; set; }
        public bool sims_comment_status { get; set; }

        public string sims_root_comment_id { get; set; }

        public bool sims_student_check_status2 { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name { get; set; }

        public string sims_cur_short_name { get; set; }

        public string sims_subject_name_en { get; set; }
    }

    #endregion


    #region student status
    public class studstatus
    {

        public string country { get; set; }

        public string female { get; set; }

        public string male { get; set; }

        public string total { get; set; }

        public string percentage { get; set; }

        public string grades { get; set; }

        public string present_class { get; set; }

        public string class_capacity { get; set; }

        public string curr_capacity { get; set; }

        public string active_students { get; set; }

        public string withdrawn_students { get; set; }

        public string present_attendees { get; set; }

        public string vacancy { get; set; }

        public string enquiry_cnt { get; set; }

        public string reg_stud { get; set; }

        public string paid_stud { get; set; }

        public string per_stud_reg_to_enq { get; set; }

        public string per_stud_paid_to_enq { get; set; }

        public string per_stud_paid_to_reg { get; set; }

        public string cur_name_en { get; set; }

        public string cur_code { get; set; }

        public string academic_year { get; set; }

        public string academic_year_desc { get; set; }

        public string concession_description { get; set; }

        public string discount_value { get; set; }

        public string stud_cnt { get; set; }

        public string sims_academic_year { get; set; }

        public string avg_expected { get; set; }

        public string avg_concession { get; set; }

        public string avg_net_tution { get; set; }

        public string per_stud_avail_discount { get; set; }

        public string staff_name { get; set; }

        public string nationality_name { get; set; }

        public string dg_desc { get; set; }

        public string staff_type { get; set; }

        public string em_date_of_join { get; set; }

        public string em_left_date { get; set; }

        public string staff_status { get; set; }

        public string Concession { get; set; }

        public string Discount { get; set; }

        public string Student { get; set; }
    }
    #endregion

    public class ExRefM
    {
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string gb_ref_name { get; set; }
        public string gb_ref_desc { get; set; }
        public bool gb_status { get; set; }
        public string gb_ref_file_name { get; set; }
        public string gb_ref_display_name { get; set; }
        public string gb_ref_publish_date { get; set; }
        public string gb_ref_expiry_date { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_gb_ref_gb_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_gb_ref_gb_cat_code { get; set; }
        public string sims_gb_ref_name { get; set; }
        public string sims_gb_ref_desc { get; set; }
        public bool sims_gb_ref_status { get; set; }
        public string sims_gb_ref_publish_date { get; set; }
        public string sims_gb_ref_expiry_date { get; set; }
        public string sims_gb_ref_filename { get; set; }
        public string sims_gb_ref_display_name { get; set; }
        public string sims_gb_ref_line_number { get; set; }
        public string sims_gb_ref_number { get; set; }
    }

    public class template
    {

        public string sims_comment_template_name { get; set; }

        public string sims_comment_template_id { get; set; }

        public string sims_comment_code { get; set; }

        public string sims_comment_type { get; set; }

        public string sims_value_type { get; set; }

        public string sims_header_comment_code { get; set; }

        public string sims_header_desc { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_lesson_subject_code { get; set; }

        public string sims_lesson_id { get; set; }

        public string sims_lesson_date { get; set; }

        public string sims_lesson_period_no { get; set; }

        public string user_name { get; set; }

        public string sims_lesson_remark { get; set; }

        public object sims_comment_description { get; set; }

        public bool sims_comment_code_value { get; set; }

        public string desc { get; set; }

        public string sims_drop_down_display_text { get; set; }

        public string sims_lesson_created_for { get; set; }

        public string sims_drop_down_display_code { get; set; }

        public string display_text { get; set; }

        public string display_text_short { get; set; }

        public string sims_lesson_date_time { get; set; }
    }
    public class SubTrt
    {
        public string sims_subject_trait_code { get; set; }
        public string sims_subject_trait_name { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_term_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_trait_name { get; set; }
        public bool trait_status { get; set; }
        public string sims_section_name { get; set; }
        public string sims_cur_desc { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_term_desc { get; set; }
        public string sims_trait_code { get; set; }
        public string sims_grade_name { get; set; }
    }


    public class BaseMk
    {
        public string sims_subject_name_en { get; set; }
        public string sims_subject_code { get; set; }

        public string sims_student_enroll_number { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string opr { get; set; }
        public string sims_cur_code { get; set; }
        public string bell_academic_year { get; set; }
        public string bell_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_subject_baseline_obtained_score { get; set; }
        public string sims_subject_baseline_max_score { get; set; }

        public string sims_subject_target_obtained_score { get; set; }
        public string sims_subject_target_max_score { get; set; }
        public string sims_user_target_created_by { get; set; }

        public string sims_user_baseline_created_by { get; set; }

    }



    public class studentstrait
    {
        public string sims_enroll_number { get; set; }
        public string stud_name { get; set; }

        public List<StuTrt> studtraitlst = new List<StuTrt>();
    }

    public class StuTrt
    {
        public string sims_trait_code { get; set; }
        public string sims_trait_name { get; set; }
        public string sims_trait_comment1 { get; set; }
        public string sims_trait_comment2 { get; set; }
        public string sims_trait_comment3 { get; set; }
        public string sims_trait_comment4 { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_term_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_term_desc_ar { get; set; }

    }

    public class gradebookClass
    {
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_co_scholastic_group_code { get; set; }
        public string sims_co_scholastic_group_name { get; set; }
        public string param_desc { get; set; }
        public string sims_cur_level { get; set; }
        public string sims_level_name { get; set; }
        public string sims_report_card_name { get; set; }
        public string sims_report_card_parameter_master_code { get; set; }
        public bool sims_grade_status { get; set; }
        public string opr { get; set; }
        public string sims_level_code { get; set; }
        public bool sims_level_status { get; set; }
    }

    #region Sims070 (Gradebook)
    public class Sims070 : gradebookClass
    {
        //public string sims_academic_year { get; set; }
        //public string sims_cur_code { get; set; }

        //public string sims_grade_code { get; set; }
        public string sims_grade_code_name { get; set; }
        //public string sims_section_code { get; set; }
        public string sims_section_code_name { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_type { get; set; }
        public string sims_gb_type_name { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_gb_term_code { get; set; }
        public string sims_gb_term_code_name { get; set; }
        public string sims_gb_grade_scale { get; set; }
        public string sims_gb_grade_scale_name { get; set; }
        public string sims_gb_start_date { get; set; }
        public string sims_gb_end_date { get; set; }
        public string sims_gb_teacher_code { get; set; }
        public string sims_gb_teacher_code_name { get; set; }
        public string sims_gb_remarks { get; set; }
        public bool sims_gb_status { get; set; }

    }

    #endregion

    #region Sims071 (Gradebook Mark Status)
    public class Sims071 : gradebookClass
    {

        public string sims_marks_status_code { get; set; }
        public string sims_marks_status_name { get; set; }
        public decimal sims_marks { get; set; }
    }

    #endregion

    #region Sims072 (Gradebook Narrative Mark grade Scale)
    public class Sims072 : gradebookClass
    {

        //public string sims_academic_year { get; set; }
        //public string sims_cur_code { get; set; }
        public string sims_mark_narr_grade_code { get; set; }
        public string sims_mark_narr_grade_name { get; set; }
        public string sims_mark_narr_grade { get; set; }
        public decimal sims_mark_narr_grade_per_max_mark { get; set; }
        public decimal sims_mark_narr_grade_low { get; set; }
        public decimal sims_mark_narr_grade_high { get; set; }
        public string sims_mark_narr_grade_description { get; set; }
    }
    #endregion

    #region Sims073 (Gradebook Mark Grade Scale)
    public class Sims073 : gradebookClass
    {
        //public string sims_academic_year { get; set; }
        //public string sims_cur_code { get; set; }
        public string sims_mark_grade_code { get; set; }
        public string sims_mark_grade_name { get; set; }
        public string sims_mark_grade { get; set; }
        public bool sims_mark_grade_point_applicable { get; set; }
        public decimal sims_mark_grade_low { get; set; }
        public decimal sims_mark_grade_high { get; set; }
        public decimal sims_mark_grade_point { get; set; }
        public string sims_mark_grade_description { get; set; }
    }
    #endregion

    #region Report card
    //for sims138 Report Card
    public class Sims138
    {
        public string sims_report_card_code { get; set; }
        public string sims_report_card_desc { get; set; }
        public string sims_report_card_declaration_date { get; set; }
        public string sims_report_card_publish_date { get; set; }
        public string sims_report_card_created_user { get; set; }
        public bool sims_report_card_freeze_status { get; set; }
        public string sims_report_card_parameter_master_code_desc { get; set; }
        public string sims_report_card_parameter_master_code { get; set; }
        public string user_name { get; set; }

        public string sims_academic_year { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_level_name { get; set; }
        public string sims_report_card_name { get; set; }
        public string param_desc { get; set; }
        public string sims_term_code { get; set; }
        public string term_name { get; set; }


        public string opr { get; set; }

        public string par_desc { get; set; }

        public string sims_level_code { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_report_card_term { get; set; }
    }
    #endregion



    #region report card attribute
    //for sims140 report_card_attribute
    public class Sims140 : gradebookClass
    {
        public string sims_attribute_group_code { get; set; }
        public string sims_attribute_group_name { get; set; }
        public string sims_attribute_code { get; set; }
        public string sims_attribute_name { get; set; }
        public string sims_attribute_display_order { get; set; }
        public bool sims_attribute_status { get; set; }
        public bool sims_group_status { get; set; }
    }
    #endregion

    #region report card co scholastic group
    //for sims143 report_card_co_scholastic_group
    public class Sims143 : gradebookClass
    {
        public string sims_co_scholastic_group_desc { get; set; }
        public string sims_co_scholastic_group_heading { get; set; }
        public string sims_co_scholastic_group_note { get; set; }
        public string sims_co_scholastic_group_assessment_count { get; set; }
        public string sims_co_scholastic_group_display_order { get; set; }
        public bool sims_co_scholastic_group_status { get; set; }
        public string sims_grade_section { get; set; }
    }
    #endregion

    #region report card co scholastic attribute
    //for sims144 report_card_co_scholastic_attribute
    public class Sims144 : gradebookClass
    {
        public string sims_co_scholastic_attribute_code { get; set; }
        public string sims_co_scholastic_attribute_name { get; set; }
        public string sims_co_scholastic_attribute_display_order { get; set; }
        public bool sims_co_scholastic_attribute_status { get; set; }
        public bool sims_group_status { get; set; }
    }
    #endregion

    #region report card subject attribute
    //for sims148 report_card_subject_attribute
    public class Sims148 : gradebookClass
    {
        public string sims_subject_name { get; set; }
        public string sims_subject_attribute_code { get; set; }
        public string sims_subject_attribute_name { get; set; }
        public string sims_subject_attribute_display_order { get; set; }
        public bool sims_subject_attribute_status { get; set; }
        public string sims_subject_code { get; set; }
        public bool sims_subject_status { get; set; }
    }

    #endregion

    #region report card co scholastic descripitve indicator
    //for sims142 report_card_co_scholastic_descriptive_indicator
    public class Sims142 : gradebookClass
    {
        public string sims_co_scholastic_attribute_name { get; set; }
        public string sims_co_scholastic_discriptive_indicator_code { get; set; }
        public string sims_co_scholastic_discriptive_indicator_name { get; set; }
        public bool sims_co_scholastic_discriptive_indicator_status { get; set; }
    }
    #endregion

    #region report card global parameter
    //for uccw025 report_card_global_parameter
    public class Uccw025 : gradebookClass
    {

        public string sims_report_card_param_master_code { get; set; }
        public string sims_report_card_param_code { get; set; }
        public string sims_report_card_param_desc { get; set; }
        public string sims_report_card_value1 { get; set; }
        public string sims_report_card_value2 { get; set; }
        public string sims_report_card_value3 { get; set; }
        public string sims_report_card_value4 { get; set; }
        public string sims_report_card_value5 { get; set; }
        public string sims_report_card_value6 { get; set; }
        public string sims_report_card_value7 { get; set; }
        public string sims_report_card_value8 { get; set; }
        public string sims_report_card_value9 { get; set; }
        public string sims_report_card_value10 { get; set; }
        public string sims_report_card_value11 { get; set; }
        public string sims_report_card_value12 { get; set; }
        public string sims_report_card_value13 { get; set; }
        public string sims_report_card_value14 { get; set; }
        public string sims_report_card_value15 { get; set; }
        public string sims_report_card_value16 { get; set; }
        public string sims_report_card_value17 { get; set; }
        public string sims_report_card_value18 { get; set; }
        public string sims_report_card_value19 { get; set; }
        public string sims_report_card_value20 { get; set; }
        public string sims_report_card_value21 { get; set; }
        public string sims_report_card_value22 { get; set; }
        public string sims_report_card_value23 { get; set; }
        public string sims_report_card_value24 { get; set; }
        public string sims_report_card_value25 { get; set; }
        public string sims_report_card_value26 { get; set; }
        public string sims_report_card_value27 { get; set; }
        public string sims_report_card_value28 { get; set; }
        public string sims_report_card_value29 { get; set; }
        public string sims_report_card_value30 { get; set; }
        public string sims_report_card_value31 { get; set; }
        public string sims_report_card_value32 { get; set; }
        public string sims_report_card_value33 { get; set; }
        public string sims_report_card_value34 { get; set; }
        public string sims_report_card_value35 { get; set; }
        public string sims_report_card_value36 { get; set; }
        public string sims_report_card_value37 { get; set; }
        public string sims_report_card_value38 { get; set; }
        public string sims_report_card_value39 { get; set; }
        public string sims_report_card_value40 { get; set; }
    }
    #endregion

    #region report card level
    //sims146 sims_report_card_level
    public class Sims146 : gradebookClass
    {
        // public string sims_level_code { get; set; }
        //public bool sims_level_status { get; set; }

        // public object opr { get; set; }
    }
    #endregion

    #region report card allocation
    //sims139 sims_report_card_allocation
    public class Sims139 : gradebookClass
    {

        public string sims_report_card_code_or_assign_number { get; set; }
        public string sims_report_card_serial_number { get; set; }
        public string sims_report_card_allocation { get; set; }
        public bool sims_report_card_status { get; set; }
        public string sims_report_card_code { get; set; }
        public string sims_report_card_level { get; set; }
        public string sims_report_card_level_name { get; set; }
        public string sims_report_card_master_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public bool report_select { get; set; }
    }
    #endregion

    public class sectionsubject
    {
        public string sims_academic_year { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_subject_code { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_homeroom_code { get; set; }

        public string sims_homeroom_name { get; set; }
    }



    public class GradeSection
    {
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
    }

    public class gradesectionsubjectteacher
    {

        public bool sims_gb_teacher_status { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_employee_code { get; set; }
        public List<GradeSection> gradesection = new List<GradeSection>();

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_name { get; set; }

        public string teacher_code { get; set; }

        public string Subject_Code { get; set; }

        public bool gb_status { get; set; }

        public bool adminUser { get; set; }

        public List<gradebook_category> Categories { get; set; }

        public string sims_homeroom_teacher_code { get; set; }

        public string sims_homeroom_name { get; set; }

        public bool teacher_freeze_status { get; set; }
    }

    public class GradeSchemeforco_scholastic
    {
        public string GradeGroupCode { get; set; }
        public string GradeGroupDesc { get; set; }
        public string Curr_Code { get; set; }
        public string Academic_Year { get; set; }
        public List<ScholGrade> gradelst = new List<ScholGrade>();
        public object grade_status { get; set; }
        public object opr { get; set; }
    }

    public class massUpdation
    {
        public string sims_academic_year { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_table_name { get; set; }
        public string sims_table_name_code { get; set; }
        public string column_name { get; set; }
        public string column_name_code { get; set; }
        public string column_old_value { get; set; }
        public string column_new_value { get; set; }
        public string sims_term_code { get; set; }
        public string opr { get; set; }
        public string sims_appl_parameter { get; set; }

        public string marks_student { get; set; }
    }

    public class gradebook
    {
        public string sims_subject_name { get; set; }
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string gb_number { get; set; }
        public string gb_name { get; set; }
        public string gb_type { get; set; }
        public string gb_type_desc { get; set; }
        public string gb_term_code { get; set; }
        public string gb_term_name { get; set; }
        public string gb_grade_scale { get; set; }
        public string gb_start_date { get; set; }
        public string gb_end_date { get; set; }
        public string teacher_code { get; set; }
        public string teacher_name { get; set; }
        public string gb_remark { get; set; }
        public bool gb_status { get; set; }
        public string Subject_Code { get; set; }
        public string TeacherList { get; set; }
        public string termStartDate { get; set; }
        public string termEndDate { get; set; }
        public bool adminUser { get; set; }
        public List<gradebook_category> Categories { get; set; }
        public List<Subject> Subjects { get; set; }
        public string QueryResult { get; set; }
        public string OPR { get; set; }

        public object sims_cur_code { get; set; }

        public object sims_academic_year { get; set; }

        public object sims_teacher_code { get; set; }

        public object sims_gb_term_name { get; set; }

        public string Subject_name { get; set; }

        public object sims_batch_code { get; set; }

        public object sims_homeroom_code { get; set; }

        public string sims_homeroom_batch_code { get; set; }

        public object sims_grade_code { get; set; }

        public object sims_section_code { get; set; }

        public string sims_gb_cat_name { get; set; }

        public object cat_code { get; set; }

        public object assign_code { get; set; }

        public bool ischecked { get; set; }

        public int cnt { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public object SubjectCurr { get; set; }

        public object SubjectAcademic { get; set; }

        public object dTerm_code { get; set; }

        public object SubjectGrade { get; set; }

        public object SubjectSection { get; set; }

        public object SubjectCode { get; set; }

        public object isCateogry { get; set; }

        public object isAssignment { get; set; }

        public string enroll_number { get; set; }

        public object sims_gb_assign_name { get; set; }

        public bool gb_freeze_status { get; set; }
        public string assign_type { get; set; }
        public string GB_CAT_NUMBER { get; set; }
    }

    public class gradebook_category
    {
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string gb_number { get; set; }
        public string gb_name { get; set; }
        public string cat_code { get; set; }
        public string cat_name { get; set; }
        public string cat_short_name { get; set; }
        public string cat_color { get; set; }
        public string cat_weightage_type { get; set; }
        public string cat_weightage_type_desc { get; set; }
        public string cat_weightage_per { get; set; }
        public string cat_points_possible { get; set; }
        public string cat_extra_points { get; set; }
        public string cat_score_types { get; set; }
        public string cat_score_types_desc { get; set; }
        public bool isIncludedInFinalGrade { get; set; }
        public string cat_drop_low { get; set; }
        public string cat_drop_per { get; set; }
        public string publishDate { get; set; }
        public bool status { get; set; }
        public string teacher_code { get; set; }
        public string QueryResult { get; set; }
        public List<Assignment> AssignMents { get; set; }
        public string OPR { get; set; }

        public bool insert { get; set; }

        public bool ischecked { get; set; }

        public int cnt { get; set; }

        public object withAssign { get; set; }

        public object cat_gb_number { get; set; }

        public object dTerm_code { get; set; }

        public object to_gb_number { get; set; }

        public object to_cat_code { get; set; }

        public object Assignment_Code { get; set; }

        public bool gb_cat_freeze_status { get; set; }

        public string sims_category_grade_scale { get; set; }
    }

    public class Co_Scholastic_copy
    {


        public string CO_SCHO_CUR { get; set; }
        public string CO_SCHO_ACADEMIC_YEAR { get; set; }
        public string CO_SCHO_GRADE_CODE { get; set; }
        public string CO_SCHO_SECTION_CODE { get; set; }
        public string CO_SCHO_GROUP_CODE { get; set; }
        public string CO_SCHO_GROUP_ATT_CODE { get; set; }
        public string CO_SCHO_GROUP_ATT_INDI_CODE { get; set; }

        public string CO_SCHO_TO_CUR { get; set; }
        public string CO_SCHO_TO_ACADEMIC_YEAR { get; set; }
        public string CO_SCHO_TO_GRADE_CODE { get; set; }
        public string CO_SCHO_TO_SECTION_CODE { get; set; }
        public string CO_SCHO_TO_IsAttribute { get; set; }
        public string CO_SCHO_TO_IsIndicator { get; set; }
        public string CO_SCHO_TO_Attribute_LST { get; set; }
        public string CO_SCHO_TO_Indicator_LST { get; set; }
        public string CO_SCHO_TO_GROUP_CODE { get; set; }
        public string CO_SCHO_TO_GROUP_ATT_CODE { get; set; }
        public string CO_SCHO_TO_GROUP_ATT_INDI_CODE { get; set; }
    }



    public class Assignment
    {
        public bool Assignment_Visible_To_Portal { get; set; }
        public bool AssignmentIncludedInFinalGrade { get; set; }
        public string Assignment_cur_code { get; set; }
        public string Assignment_cur_name { get; set; }
        public string Assignment_academic_year { get; set; }
        public string Assignment_grade_code { get; set; }
        public string Assignment_grade_name { get; set; }
        public string Assignment_section_code { get; set; }
        public string Assignment_section_name { get; set; }
        public string Assignment_gb_number { get; set; }
        public string Assignment_gb_name { get; set; }
        public string Assignment_cat_code { get; set; }
        public string Assignment_cat_name { get; set; }
        public string teacher_code { get; set; }
        public string Assignment_Name { get; set; }
        public string Assignment_Code { get; set; }
        public string Assignment_Type { get; set; }
        public string Assignment_Type_Desc { get; set; }
        public string Assignment_Score_type { get; set; }
        public string Assignment_Score_type_Desc { get; set; }
        public string Assignment_Date { get; set; }
        public string Assignment_Due_Date { get; set; }
        public string Assignment_Due_Time { get; set; }
        public string AssignmentMaxScore { get; set; }
        public string AssignmentMaxScore_correct { get; set; }
        public bool Assignment_NarrativeGrade { get; set; }
        public string Assignment_NarrativeGrade_Desc { get; set; }
        public bool Assignment_IP_BY_STD { get; set; }
        public string Assignment_Weightage { get; set; }
        public bool Assignment_Email { get; set; }
        public string Assignment_Extra_Creadit { get; set; }
        public string Assignment_ExamID { get; set; }
        public bool Assignment_GradeCompleted_Status { get; set; }
        public bool Assignment_Score_Visible_To_Portal { get; set; }
        //public string Assignment_Score_Visible_To_Portal { get; set; }
        public string Assignment_Remark { get; set; }
        public string Assignment_Mark { get; set; }
        public string Assignment_final_grade { get; set; }
        public string Assignment_final_grade_desc { get; set; }
        public string Assignment_max_point { get; set; }
        public string Assignment_max_correct { get; set; }
        public string Assignment_Comment { get; set; }
        public string Assignment_Status { get; set; }
        public string Assignment_Completed_Date { get; set; }
        public string Color_Code { get; set; }
        public string StudEnroll { get; set; }
        public string AttrGroupCode { get; set; }
        public string AttrCode { get; set; }
        public string AttrName { get; set; }
        public List<Students> StudentList { get; set; }
        public string QueryResult { get; set; }
        public string Assignment_student_name { get; set; }
        public string Assignment_Mark_Total { get; set; }
        public string OPR { get; set; }

        public bool insert { get; set; }

        public object sims_mark_final_grade_code { get; set; }

        public string Assignment_Status1 { get; set; }

        public object Stud_Enroll { get; set; }

        public string sims_gb_grade_scale { get; set; }

        public string Assignment_Code1 { get; set; }

        public string Assignment_cat_code1 { get; set; }

        public object term_code { get; set; }

        public object Assignment_subject_code { get; set; }

        public bool assign_select_status { get; set; }

        public string Assignment_Other_Name { get; set; }

        public bool sims_gb_cat_assign_freeze { get; set; }

        public string sims_category_assignment_grade_scale { get; set; }

        public object Assignment_term_code { get; set; }
        public string sims_category_assignment_student_type { get; set; }
    }

    public class TeacherDetails
    {
        public string TeacherId { get; set; }
        public string TeacherEmpId { get; set; }
        public string TeacherName { get; set; }
        public List<Subject> Subjects { get; set; }
    }

    public class Subject
    {
        public string SubjectCode { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCurr { get; set; }
        public string SubjectAcademic { get; set; }
        public string SubjectGrade { get; set; }
        public string SubjectTerm { get; set; }
        public string SubjectGrade_Name { get; set; }
        public string SubjectSection { get; set; }
        public string SubjectSection_Name { get; set; }
        public string SubjectGroupCode { get; set; }
        public string GradeBookName { get; set; }
        public string GradeBookNumber { get; set; }
        public string categoryNumber { get; set; }
        public string AssignmentNumber { get; set; }
        public string SubTeacherId { get; set; }
        public string SubTeacherEmpId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherFullName { get; set; }
        public bool TeacherStatus { get; set; }
        public bool enabled { get; set; }
        public string classs { get; set; }
        public bool subject_select_status { get; set; }
        public bool status { get; set; }

        public string stud_enroll { get; set; }

        public string subcount { get; set; }

        public object Level_code { get; set; }
    }


    public class Grade
    {
        public string Grade_Code { get; set; }
        public string Grade_Name { get; set; }
        public string Grd_Teacher_Code { get; set; }
        public List<Section> Sectionslis = new List<Section>();
    }

    public class Section
    {
        public string Section_Name { get; set; }
        public string Section_Code { get; set; }
        public string Sec_Grade_Code { get; set; }
        public List<Subject> Subjectslist = new List<Subject>();
    }

    public class Students
    {
        public string Stud_FirstName { get; set; }
        public string Stud_MidName { get; set; }
        public string Stud_LastName { get; set; }
        public string Stud_CurrName { get; set; }
        public string Stud_CurrCode { get; set; }
        public string Stud_AcademicYear { get; set; }
        public string Stud_GradeCode { get; set; }
        public string Stud_GradeName { get; set; }
        public string Stud_SectionName { get; set; }
        public string Stud_SectionCode { get; set; }
        public string Stud_Full_Name { get; set; }
        public string Stud_Enroll { get; set; }
        public string Included { get; set; }
        public List<Assignment> AssignMents { get; set; }
        public string Assignment_Mark { get; set; }
        public bool Assignment_final_grade { get; set; }
        public string Assignment_final_grade_desc { get; set; }
        public string Assignment_max_point { get; set; }
        public string AssignmentMaxScore { get; set; }
        public string Assignment_max_correct { get; set; }
        public string Assignment_Comment { get; set; }
        public string Assignment_Status { get; set; }
        public string Color_Code { get; set; }
        public string Assignment_Completed_Date { get; set; }
        public bool ischecked = false;
        public string sims_mark_final_grade_code { get; set; }
        public bool Stud_status { get; set; }
        public string Stud_gb_number { get; set; }
        public string stud_gb_cat_code { get; set; }
        public string assignment_number { get; set; }
        public string sims_gb_grade_scale { get; set; }
        public string Stud_subject_code { get; set; }

        public List<Subject> subject_list = new List<Subject>();

        public string Stud_roll { get; set; }

        public int Stud_roll_sort { get; set; }

        public string Stud_roll_sort_cint { get; set; }

        public string sims_roll_number_display { get; set; }
        public string sims_student_gender { get; set; }

        public List<indicator> indicators { get; set; }

    }

    public class GradeScheme
    {
        public List<gradesection> gradesectionlist = new List<gradesection>();
        public string opr { get; set; }
        public string AcademicYear { get; set; }
        public string CurrCode { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroupCode { get; set; }
        public string GradeGroupDesc { get; set; }
        public string PointsApplicable { get; set; }
        public string PerMaxMark { get; set; }
        public string GradeLow { get; set; }
        public string GradeHigh { get; set; }
        public string Description { get; set; }
        public string Points { get; set; }
        public string TeacherCode { get; set; }
        public string ColorCode { get; set; }
        public bool status { get; set; }
        public List<GradeScheme> MarkGradeList { get; set; }

        public string section_code { get; set; }

        public string section_name { get; set; }
    }

    public class GradeScale
    {
        public string opr { get; set; }

        public string gradegroupnumber { get; set; }

        public string gradeName { get; set; }

        public string description { get; set; }

        public bool status { get; set; }


    }
    public class NarrativeGrade
    {
        public string AcademicYear { get; set; }
        public string CurrCode { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroupCode { get; set; }
        public string GradeGroupDesc { get; set; }
        public string PerMaxMark { get; set; }
        public string Description { get; set; }
        public string narr_grade_status { get; set; }
        public List<NarrativeGrade> NarrativeGradeList { get; set; }

        public string narr_grade_group_name { get; set; }

        public string sims_mark_grade_code { get; set; }

        public string sims_mark_grade_name { get; set; }

        public string sims_mark_grade_description { get; set; }

        public string sims_mark_grade_low { get; set; }

        public string sims_mark_grade_high { get; set; }

        public string sims_mark_grade_point { get; set; }

        public string sims_mark_grade_color_code { get; set; }

        public object opr { get; set; }

        //public string sims_mark_grade_color_code { get; set; }
    }

    public class StudentStatus
    {
        public string status_id { get; set; }
        public string status_desc { get; set; }
    }

    public class ClasssDetails
    {
        public string curr_code { get; set; }
        public string curr_name { get; set; }
        public string academic_year { get; set; }
        public string academic_year_desc { get; set; }
        public string Aycur { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string GrCur { get; set; }
        public string GrAca { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string SecCur { get; set; }
        public string SecAcy { get; set; }
        public string SecGr { get; set; }
        public string employee_code { get; set; }
    }


    #region Report Card
    public class ReportCardLevel
    {
        public string Curr_Code { get; set; }
        public string Curr_Name { get; set; }
        public string Academic_Year { get; set; }
        public string Grade_Code { get; set; }
        public string Section_code { get; set; }
        public string Grade_Name { get; set; }
        public string Section_Name { get; set; }
        public string Level_Name { get; set; }
        public string Level_code { get; set; }
        public string Report_Card_Code { get; set; }
        public string Report_Card_Name { get; set; }
        public string Report_Card_Desc { get; set; }
        public string Report_Card_Decla_Date { get; set; }
        public string Report_Card_Publishing_Date { get; set; }
        public string Report_Card_User_Code { get; set; }
        public string Report_Card_Freez_Status { get; set; }
        public string Report_Card_Parameter_Master { get; set; }
        public string Report_Card_Term { get; set; }
        public string Report_Card_Status { get; set; }
    }

    public class ReportCardStudent
    {
        public string enroll { get; set; }
        public string StudName { get; set; }
        public string Curr_Code { get; set; }
        public string Curr_Name { get; set; }
        public string Academic_Year { get; set; }
        public string Grade_Code { get; set; }
        public string Section_code { get; set; }
        public string Grade_Name { get; set; }
        public string Section_Name { get; set; }
        public string GB_Name { get; set; }
        public string GB_Number { get; set; }
        public string Level_Name { get; set; }
        public string Level_code { get; set; }
        public string Subject_Code { get; set; }
        public string Report_Card_Code { get; set; }
        public string Report_Card_Name { get; set; }
        public string Comment1_code { get; set; }
        public string Comment1_desc { get; set; }
        public string user1 { get; set; }
        public string Comment2_desc { get; set; }
        public string Comment2_code { get; set; }
        public string user2 { get; set; }

        public string Comment3_code { get; set; }
        public string Comment3_desc { get; set; }
        public string user3 { get; set; }

        public string Status { get; set; }
        public string included { get; set; }
        public string EnrollList_I { get; set; }
        public string EnrollList_U { get; set; }
        public string commnetNo { get; set; }

        public object opr { get; set; }

        public string ColumnVisibility { get; set; }
    }

    public class ReportCardComment
    {
        public string CurrCode { get; set; }
        public string Academic { get; set; }
        public string LevelCode { get; set; }
        public string ReportCardCode { get; set; }
        public string CommentCode { get; set; }
        public string CommentDesc { get; set; }
        public string Status { get; set; }
    }

    #endregion

    #region Co-Schol
    public class DescIndicator
    {
        public string sims_config_assign_number { get; set; }
        public string sims_co_scolastic_teacher_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_co_scholastic_group_code { get; set; }
        public string sims_co_scholastic_attribute_code { get; set; }
        public string sims_co_scholastic_discriptive_indicator_code { get; set; }
        public string sims_co_scholastic_discriptive_indicator_name { get; set; }
        public bool sims_co_scholastic_discriptive_indicator_status { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_co_scholastic_group_name { get; set; }
        public string sims_co_scholastic_attribute_name { get; set; }
        public string Stud_List { get; set; }
        public string sims_co_scholastic_discriptive_indicator_name_ot { get; set; }
        public string sims_co_scholastic_discriptive_dispaly_name { get; set; }
        public string sims_co_scholastic_attribute_dispaly_name { get; set; }
        public string sims_co_scolastic_employee_code { get; set; }
        public string sims_co_scolastic_User_type { get; set; }
        public string sims_co_scolastic_Score_type { get; set; }
        public string opr { get; set; }
        public string sims_term_code { get; set; }
        public string StudentList { get; set; }
        public string grade_scaleGroupCode { get; set; }
        public string sims_co_scholastic_attribute_display_order { get; set; }
        public string sims_co_scholastic_descriptive_indicator_display_order { get; set; }
        public bool sims_co_scholastic_descriptive_indicator_freeze_status { get; set; }
        public string sims_co_scolastic_grading_scale { get; set; }
        public bool sims_co_scholastic_multiple_teacher { get; set; }
        public string sims_co_scholastic_overall_result { get; set; }
        public string sims_co_scholastic_teacher_code { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_term_code { get; set; }
        public string sims_report_card_category_code { get; set; }
        public string sims_report_card_assign_number { get; set; }
        public string sims_report_card_assign_max_score { get; set; }
        public string sims_co_scholastic_grading_scale_code { get; set; }

        public string sims_co_scholastic_descriptive_indicator_start_date { get; set; }

        public string sims_co_scholastic_descriptive_indicator_end_date { get; set; }

        public string sims_co_scholastic_descriptive_indicator_freeze_date { get; set; }

        public string sims_co_scholastic_descriptive_indicator_type { get; set; }

        public string sims_co_scholastic_descriptive_indicator_mark { get; set; }

        public string sims_co_scholastic_descriptive_indicator_grade_scale { get; set; }

        public object entry_status { get; set; }

        public string config_code { get; set; }
    }

    public class schol_Attribute
    {
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_co_scholastic_group_code { get; set; }
        public string sims_co_scholastic_attribute_code { get; set; }
        public string sims_co_scholastic_attribute_name { get; set; }
        public string sims_co_scholastic_attribute_display_order { get; set; }
        public bool sims_co_scholastic_attribute_status { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_co_scholastic_group_name { get; set; }
        public List<DescIndicator> indlist { get; set; }
        public string sims_co_scholastic_attribute_name_ot { get; set; }
        public string sims_co_scholastic_attribute_dispaly_name { get; set; }
        public string sims_co_scolastic_employee_code { get; set; }
        public string sims_co_scolastic_User_type { get; set; }
        public string sims_co_scolastic_Score_type { get; set; }

        public object opr { get; set; }

        public string sims_co_scholastic_group_grade_scale_code { get; set; }

        public string term_code { get; set; }

        public string flag { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string entry_status { get; set; }

        public string sims_report_card_scheme_grade_name { get; set; }
    }

    public class schol_group
    {
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_co_scholastic_group_code { get; set; }

        public string sims_co_scholastic_group_name { get; set; }
        public string sims_co_scholastic_group_description { get; set; }
        public string sims_co_scholastic_group_heading { get; set; }
        public string sims_co_scholastic_group_note { get; set; }
        public string sims_co_scholastic_group_assessment_count { get; set; }
        public string sims_co_scholastic_group_display_order { get; set; }
        public bool sims_co_scholastic_group_status { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public List<schol_Attribute> Attrlist { get; set; }
        public string sims_co_scholastic_group_name_ot { get; set; }
        public string sims_co_scholastic_group_dispaly_name { get; set; }
        public string sims_co_scolastic_employee_code { get; set; }
        public string sims_co_scolastic_employee_name { get; set; }
        public string sims_co_scolastic_grading_scale { get; set; }
        public string sims_co_scholastic_grade_scale_description { get; set; }
        public string sims_co_scholastic_grade_scale_status { get; set; }

        public string sims_co_scolastic_User_type { get; set; }
        public string sims_co_scolastic_Score_type { get; set; }
        public string sims_co_scolastic_Score_type_Descc { get; set; }

        public object opr { get; set; }

        public object sims_grd_sec_list { get; set; }

        public object sims_co_scholastic_teacherList { get; set; }

        public string TeacherList { get; set; }

        public bool teacher_freeze_status { get; set; }
        public string sims_co_scholastic_group_type { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_config_type { get; set; }
        public bool sims_co_scholastic_group_freeze_status { get; set; }

        public string sims_co_scholastic_group_name_old { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_report_card_config_flag { get; set; }

        public string entry_status { get; set; }

        public string sims_report_card_config_desc { get; set; }

        public string sims_report_card_type_desc { get; set; }

        public string sims_report_card_scheme_grade_name { get; set; }

        public string sims_co_scholastic_grade_scale_code { get; set; }

        public bool sims_report_card_config_status { get; set; }
    }

    public class ScholStudent
    {
        public string Stud_FirstName { get; set; }
        public string Stud_MidName { get; set; }
        public string Stud_LastName { get; set; }
        public string Stud_CurrName { get; set; }
        public string Stud_CurrCode { get; set; }
        public string Stud_AcademicYear { get; set; }
        public string Stud_GradeCode { get; set; }
        public string Stud_GradeName { get; set; }
        public string Stud_SectionName { get; set; }
        public string Stud_SectionCode { get; set; }
        public string Stud_Full_Name { get; set; }
        //public string Stud_Full_Name_ot { get; set; }
        //public string Stud_Full_Name_ot { get; set; }
        public string Stud_Enroll { get; set; }
        public bool StudFlag { get; set; }
        public string Grade_C { get; set; }
        public string Grade_Scale_C { get; set; }
        public string Grade_Point { get; set; }
        public string Grade_D { get; set; }
        public string Grade_Desc { get; set; }
        public string Color_Code { get; set; }
        public string Group_Name { get; set; }
        public string Group_No { get; set; }
        public string AttribNo { get; set; }
        public string AttribName { get; set; }
        public string IndCode { get; set; }
        public string Ind_Name { get; set; }
        public string sims_co_scolastic_Score_type { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc { get; set; }
        public string grade_desc_group { get; set; }
        public bool isEnabled { get; set; }
        public List<termdesccode> termlist = new List<termdesccode>();
        public object opr { get; set; }
        public object SubTeacherEmpId { get; set; }


        public object Stud_indic_teacher_code { get; set; }
    }

    public class menuclass
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public List<submenuclass> secList { get; set; }

    }
    public class submenuclass
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public List<schol_group> groupList { get; set; }

    }
    public class gradesection
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
    }

    public class termdesccode
    {
        public string sims_term_code { get; set; }
        public string sims_term_desc { get; set; }
        public bool StudFlag { get; set; }

        public string Stud_indic_teacher_code { get; set; }
    }

    public class ScholGrade
    {
        public string Curr_Code { get; set; }
        public string Academic_Year { get; set; }
        public string Scale_Code { get; set; }
        public string Scale_Name { get; set; }
        public string Grade_Code { get; set; }
        public string Grade_Name { get; set; }
        public string Grade_Point_low { get; set; }
        public string Grade_Point_high { get; set; }
        public string Grade_Description { get; set; }
        public string Color_Code { get; set; }
        public bool Status { get; set; }

        public string Grade_Point { get; set; }

        public string GradegruopCode { get; set; }

        public object opr { get; set; }
    }

    #endregion

    #region Subject Attribute
    public class SubjectAttr
    {
        public string CurrCode { get; set; }
        public string AcademicYear { get; set; }
        public string GradeCode { get; set; }
        public string SectionCode { get; set; }
        public string SubjectCode { get; set; }
        public string Group_Code { get; set; }
        public string Group_Name { get; set; }
        public string Attr_Code { get; set; }
        public string Attr_Name { get; set; }
        public string Display_Order { get; set; }
        public bool attr_Status { get; set; }
        public bool IsSelected { get; set; }
        public string gb_number { get; set; }
        public string gb_cat_number { get; set; }
        public string gb_cat_assign_number { get; set; }
        public string Attr_Marks { get; set; }
        public string Attr_MaxScore_Grading { get; set; }

        public object Attr_Marks_correct { get; set; }
    }

    public class AttributeStudent
    {
        public string CompletedDate { get; set; }
        public string Stud_FirstName { get; set; }
        public string Stud_MidName { get; set; }
        public string Stud_LastName { get; set; }
        public string Stud_GradeCode { get; set; }
        public string Stud_SectionCode { get; set; }
        public string Stud_Full_Name { get; set; }
        public string Stud_Enroll { get; set; }
        public string Group_Name { get; set; }
        public string Group_No { get; set; }
        public string AttribNo { get; set; }
        public string AttribName { get; set; }
        public string Color_Code { get; set; }
        public string Attr_Mark { get; set; }
        public string Attr_final_grade { get; set; }
        public string Attr_final_grade_desc { get; set; }
        public string Attr_max_point { get; set; }
        public string Attr_MaxScore { get; set; }
        public string Attr_max_correct { get; set; }
        public string Attr_Comment { get; set; }
        public string Attr_gb_number { get; set; }
        public string Attr_cat_code { get; set; }
        public string Attr_cat_Assign_code { get; set; }
        public string Attr_Name { get; set; }
        public string Attr_Code { get; set; }
        public string Attr_Status { get; set; }
    }

    #endregion

    public class Sim072
    {
        public string sims_cur_code { get; set; }
        public string cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string section_name { get; set; }
        public string sims_subject_code { get; set; }
        public string subject_name { get; set; }
        public string sims_subject_attribute_group_code { get; set; }
        public string group_name { get; set; }
        public string sims_subject_attribute_code { get; set; }
        public string sims_subject_attribute_name { get; set; }
        public string sims_subject_attribute_display_order { get; set; }
        public bool sims_subject_attribute_status { get; set; }
        public bool subject_select { get; set; }
        public bool group_select { get; set; }
        public string old_subj { get; set; }

        public string sims_subject_attribute_name_ot { get; set; }


    }
    public class marks_history
    {

        public object sims_academic_year { get; set; }

        public object sims_cur_code { get; set; }

        public object sims_grade_code { get; set; }

        public object sims_section_code { get; set; }

        public object sims_gb_number { get; set; }

        public object sims_gb_cat_code { get; set; }

        public object sims_gb_cat_assign_number { get; set; }

        public object sims_gb_cat_assign_enroll_number { get; set; }

        public object sims_gb_cat_assign_mark { get; set; }

        public object sims_gb_cat_assign_final_grade { get; set; }

        public object sims_gb_cat_assign_max_point { get; set; }

        public object sims_gb_cat_assign_max_correct { get; set; }

        public object sims_gb_cat_assign_comment { get; set; }

        public object sims_gb_cat_assign_status { get; set; }

        public object sims_gb_updated_by { get; set; }
    }

    #region Sims527 (Agenda)
    public class Sims527
    {
        public string oldpath { get; set; }
        public string newpath { get; set; }
        public string ftpusername { get; set; }
        public string ftppassword { get; set; }
        public string file_format { get; set; }
        public string user_code { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_acaedmic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_agenda_date { get; set; }
        public string day { get; set; }
        public string vis { get; set; }
        public string sims_agenda_name { get; set; }
        public string sims_agenda_desc { get; set; }
        public bool loading_ind { get; set; }
        public string file { get; set; }
        public string sims_agenda_teacher_code { get; set; }
        public bool sims_agenda_visible_to_parent_portal { get; set; }
        public bool sims_agenda_status { get; set; }
        public string sims_calendar_exception_description { get; set; }
        public string sims_academic_year_description { get; set; }
        public List<Sims527_doc> sims_doc { get; set; }

        public string sims_agenda_doc_line_no { get; set; }

        public string sims_agenda_doc_name { get; set; }

        public string sims_agenda_doc_sr_no { get; set; }

        public string sims_agenda_doc_name_en { get; set; }

        public List<agenda_dates> ag_dates = new List<agenda_dates>();



        public string sims_agenda_to_time { get; set; }

        public string sims_agenda_page_no { get; set; }

        public string sims_agenda_stage { get; set; }

        public string sims_agenda_from_time { get; set; }

        public string sims_bell_academic_year { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string sims_agenda_dateto { get; set; }



    }
    public class agenda_dates
    {
        public string sims_agenda_date { get; set; }
        public List<subject_details> sub_list = new List<subject_details>();

        public string aid { get; set; }
    }

    public class subject_details
    {

        public string sims_cur_code { get; set; }
        public string sims_acaedmic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        //public string sims_agenda_number { get; set; }
        public string sims_agenda_date { get; set; }
        public string sims_subject_name { get; set; }

        public List<Agenda_details> agendalist = new List<Agenda_details>();


        public string sid { get; set; }
    }

    public class Agenda_details
    {
        public string sims_cur_code { get; set; }
        public string sims_acaedmic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_agenda_date { get; set; }
        public string sims_agenda_name { get; set; }
        public string sims_agenda_desc { get; set; }
        public string sims_agenda_teacher_code { get; set; }

        public List<Sims527_doc> doc_list = new List<Sims527_doc>();


        public string agid { get; set; }
    }

    public class Sims527_doc
    {
        public string sims_agenda_number { get; set; }
        public string sims_agenda_doc_line_no { get; set; }
        public string sims_agenda_doc_name { get; set; }
        public string sims_agenda_doc_name_en { get; set; }
        public string sims_agenda_doc_sr_no { get; set; }


        public string sims_agenda_date { get; set; }

        public string sims_subject_code { get; set; }

        public string did { get; set; }
    }
    #endregion

    #region Property
    public class GBRoot
    {
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
        public string term_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public List<GB_Category> Categories { get; set; }
        public bool isSelected { get; set; }
        public string Status { get; set; }
        /// <summary>
        /// Get or Set the Property Being Changed of Selected Relation
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// Get or Set Current Selected Relation
        /// </summary>
        public string RelationName { get; set; }
        /// <summary>
        /// Get Or Set Value For Property
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Get or Set the Criteria for Changing Property of Selected Relation
        /// </summary>
        public string Criteria { get; set; }
        /// <summary>
        /// Get or Set Criteria Value for changing Selected Property
        /// </summary>
        public string CriteriaValue { get; set; }

        public object OPR { get; set; }
    }

    public class GBRootProp
    {
        public string propName { get; set; }
        /// <summary>
        /// Get or Set the Property Being Changed of Selected Relation
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// Get or Set Current Selected Relation
        /// </summary>
        public string RelationName { get; set; }
        public string propName_D { get; set; }
        public string db_prop { get; set; }
    }

    public class CatAssignment
    {
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_number { get; set; }
        public string Assignment_number { get; set; }
        public string Assignment_name { get; set; }
        public bool isSelected { get; set; }
    }

    public class GB_Category
    {
        public string sims_gb_number { get; set; }
        public string Category_name { get; set; }
        public string Category_code { get; set; }
        public List<CatAssignment> AssignMents { get; set; }
        public string Status { get; set; }
        public bool isSelected { get; set; }
    }

    public class SaveChart
    {
        /// <summary>
        /// Get or Set the Property Being Changed of Selected Relation
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// Get or Set Current Selected Relation
        /// </summary>
        public string RelationName { get; set; }
        /// <summary>
        /// Get Or Set Value For Property
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Get or Set the Criteria for Changing Property of Selected Relation
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// Get Criteria Value for changing Selected Property
        /// </summary>
        public string CriteriaValue { get; set; }
        public string GB_number { get; set; }
        public string GB_Cat_number { get; set; }
        public string GB_Cat_AssignNumber { get; set; }
    }

    #endregion




    #region Report card attribute group
    //for sims141 report card attribute group
    public class Sims141//:gradebookClass
    {
        public string sims_attribute_group_code { get; set; }
        public string sims_attribute_group_name { get; set; }
        public string sims_attribute_group_display_order { get; set; }
        public bool sims_attribute_group_status { get; set; }
        public string sims_grade_section { get; set; }
        public string sims_attribute_group_type { get; set; }
        public string sims_attribute_group_type_en { get; set; }
        public bool section_select { get; set; }
        public bool grade_select { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }


        public string sims_attribute_group_name_ot { get; set; }
    }
    #endregion

    #region report card allocation
    //sims139 sims_report_card_allocation
    public class Sims506
    {
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_term_name { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_report_card_master_code { get; set; }
        public string sims_report_card_code_or_assign_number { get; set; }
        public string sims_report_card_serial_number { get; set; }
        public string sims_report_card_allocation { get; set; }
        public bool sims_report_card_status { get; set; }
        public string sims_report_card_name { get; set; }
        public bool report_select { get; set; }
        public string sims_report_card_level { get; set; }
        public string sims_report_card_level_name { get; set; }

        public object sims_report_card_code { get; set; }
    }
    #endregion

    #region Upload Assignment Teacher(Sim508)
    public class Sims508
    {
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_assignment_title { get; set; }
        public string sims_assignment_desc { get; set; }
        public string sims_assignment_start_date { get; set; }
        public string sims_assignment_submission_date { get; set; }
        public string sims_assignment_freeze_date { get; set; }
        public bool sims_assignment_status { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_assignment_number { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_assignment_teacher_code { get; set; }
        public string sims_assignment_teacher_name { get; set; }

        public string sims_assignment_subject_code { get; set; }
        public string stud_name { get; set; }
        public string stud_code { get; set; }
        public string stud_status { get; set; }
        public string sims_assignment_doc_path { get; set; }
        public bool status { get; set; }
        public string sims_gb_cat_assign_date { get; set; }
        public string sims_gb_cat_assign_due_date { get; set; }
        public string sims_term_code { get; set; }

        public string sims_employee_code { get; set; }
        public string sims_gb_term_code { get; set; }
    }
    #endregion

    #region Upload Assignment Student(Sim511)
    public class Sims511
    {
        public string sims_assignment_number { get; set; }
        public string sims_assignment_subject_code { get; set; }
        public string sims_assignment_title { get; set; }
        public string sims_assignment_desc { get; set; }
        public string sims_assignment_student_doc_student_remark { get; set; }
        public string sims_assignment_start_date { get; set; }
        public string sims_assignment_submission_date { get; set; }
        public string sims_assignment_freeze_date { get; set; }
        public string sims_assignment_status { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_assignment_enroll_number { get; set; }
        public string sims_assignment_student_line_no { get; set; }
        public string sims_assignment_student_doc_path { get; set; }
        public string sims_assignment_student_doc_date { get; set; }
        public string sims_assignment_student_doc_teacher_remark { get; set; }
        public bool sims_assignment_student_doc_status { get; set; }
        public bool sims_assignment_student_doc_verify { get; set; }
        public string Stud_code { get; set; }
        public string sims_assignment_teacher_code { get; set; }
        public string Stud_name { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_subject_name { get; set; }
    }
    #endregion



    #region Student-Report-Card-Comment
    public class student_report_card_sub_comment
    {
        public string sims_sub_comment { get; set; }
        public string sims_sub_comment_code { get; set; }
        public string sims_type { get; set; }


        public string sims_student_comment_header_id { get; set; }

        public string sims_comment_code { get; set; }

        public string sims_root_comment_id { get; set; }
    }

    public class student_report_card_level1_comment
    {
        public student_report_card_level1_comment()
        {
            sub_comments = new List<student_report_card_sub_comment>();
        }
        public string sims_comment { get; set; }
        public bool sims_comment_status { get; set; }

        public string sims_comment_code { get; set; }
        public string sims_type { get; set; }


        public List<student_report_card_sub_comment> sub_comments { get; set; }

        public string sims_student_comment_header_id { get; set; }

        public string sims_subject_code { get; set; }
    }

    public class student_report_card_comment
    {
        public string sims_comment_desc { get; set; }
        public student_report_card_comment()
        {
            coments_lst = new List<student_report_card_level1_comment>();
        }

        public bool sims_sims_freeze_status { get; set; }
        public string sims_student_comment_id1 { get; set; }
        public string boolean_value { get; set; }
        public string sims_type { get; set; }
        public List<student_report_card_level1_comment> coments_lst { get; set; }
        public string sims_report_card_level { get; set; }
        public string sims_report_card_level_name { get; set; }
        public string sims_report_card { get; set; }
        public string sims_report_card_name { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_gradebook_code { get; set; }
        public string sims_gradebook_name { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_Name { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_comment_type { get; set; }
        public string sims_comment_type_name { get; set; }
        public string sims_student_comment_header_id { get; set; }
        public string sims_comment_header_desc { get; set; }
        public string ReportCardComment { get; set; }
        public string sims_student_comment_id { get; set; }
        public bool sims_student_check_status { get; set; }
        public bool sims_comment_status { get; set; }

        public string sims_root_comment_id { get; set; }

        public bool sims_student_check_status2 { get; set; }

        public object sims_user_code { get; set; }

        public object copyEnrolls { get; set; }

        public object sims_comment_type1 { get; set; }

        public object sims_comment_code { get; set; }

        public object sims_comment { get; set; }

        public string sims_comment_type_code { get; set; }

        public string sims_header_comment_code { get; set; }

        public string sims_comment_sr { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_level_code { get; set; }
        public string sims_report_card_code { get; set; }

        public string sims_student_root_comment_display_order { get; set; }
        public string sims_student_header_code { get; set; }

        public string student_name { get; set; }
        public string sims_comment_status_details { get; set; }
        public string student_type { get; set; }

        public string sims_cur_level_code { get; set; }

        public string sims_grade_scale_code { get; set; }
    }


    public class student_report_card_comment_new
    {
        public List<student_report_card_comment_new_sub> sublist1 { get; set; }

        public string sims_enroll_number { get; set; }
        public string StudName { get; set; }

        public string sims_comment_code { get; set; }
        public string sims_comment_desc { get; set; }

        public string s_sims_comment_code { get; set; }
        public string s_sims_comment_desc { get; set; }



    }

    public class student_report_card_comment_new_sub
    {
        public string sims_enroll_number { get; set; }
        public string StudName { get; set; }
        public string s_sims_comment_code { get; set; }
        public string s_sims_comment_desc { get; set; }

        public string sims_comment_code { get; set; }

        public string sims_comment_desc { get; set; }

        public string sims_comment_desc_len { get; set; }

        public List<grade_scale_new> grade_scale_lst { get; set; }

        public string sims_grade_scale_code { get; set; }

        public string sims_cur_level_code { get; set; }
    }

    public class grade_scale_new
    {

        public string sims_mark_grade_code { get; set; }

        public string sims_mark_grade_name { get; set; }

        public string sims_mark_grade_description { get; set; }

        public string sims_mark_grade { get; set; }
    }

    #endregion

    public class GDA001
    {
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string average { get; set; }
        public string sims_mark_grade_name { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_gb_term_code { get; set; }
        public string total_students { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_nationality_code { get; set; }
        public string sims_nationality_name_en { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_subject_group_code { get; set; }
        public string sims_subject_group_name_en { get; set; }





    }

    public class Group
    {

        public string opr { get; set; }
        public string cur_code { get; set; }
        public string aca_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
        public string sims_co_scholastic_group_code { get; set; }

        public string sims_co_scholastic_group_name { get; set; }


        public string sims_co_scholastic_attribute_code { get; set; }

        public string sims_co_scholastic_descriptive_indicator_code { get; set; }

        public List<attributess> attributes { get; set; }


        public string sims_subject_code { get; set; }

        public string sims_subject_name { get; set; }

        public object sims_report_card_config_code { get; set; }

        public object term_code { get; set; }

        public object flag { get; set; }
    }

    public class attributess
    {

        public string sims_co_scholastic_group_code { get; set; }
        public string sims_co_scholastic_attribute_code { get; set; }

        public List<indicator> indicators { get; set; }


        public string sims_co_scholastic_attribute_master_code { get; set; }

        public string sims_co_scholastic_attribute_name { get; set; }
    }

    public class indicator
    {
        public string sims_co_scholastic_group_code { get; set; }

        public string sims_co_scholastic_descriptive_indicator_master_code { get; set; }

        public string sims_co_scholastic_descriptive_indicator_name { get; set; }

        public string sims_co_scholastic_attribute_master_code { get; set; }

        public bool insert { get; set; }

        public string sims_co_scholastic_descriptive_indicator_type { get; set; }

        public string sims_co_scholastic_descriptive_indicator_mark { get; set; }

        public string sims_co_scholastic_descriptive_indicator_grade_scale { get; set; }

        public List<grade_scale> grade_scale_lst { get; set; }


        public string sims_co_scholastic_cdi_srl_no { get; set; }

        public string sims_co_scholastic_discriptive_indicator_grade_code { get; set; }

        public string sims_co_scholastic_discriptive_indicator_grade { get; set; }

        public string sims_report_card_co_scholastic_grade_point { get; set; }

        public string sims_config_co_scholastic_descriptive_indicator_code { get; set; }

        public string sims_student_assign_exam_status { get; set; }

        public string Stud_Full_Name { get; set; }

        public string Stud_Enroll { get; set; }
    }

    public class studentTargetTerm {

        public string aca_year { get; set; }

        public string cur_code { get; set; }

        public string grade_code { get; set; }

        public string section_code { get; set; }

        public string subject_code { get; set; }

        public string term_code { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_mark_grade_code { get; set; }

        public string sims_grade_scale_code { get; set; }

        public string sims_report_card_att1 { get; set; }

        public string sims_report_card_att2 { get; set; }

        public string sims_report_card_att3 { get; set; }

        public string sims_report_card_student_target_status { get; set; }
    }

    public class grade_scale
    {

        public string sims_co_scholastic_grade_scale_code { get; set; }

        public string sims_co_scholastic_grade_code { get; set; }

        public string sims_co_scholastic_grade_name { get; set; }

        public string sims_co_scholastic_grade_point_low { get; set; }

        public string sims_co_scholastic_grade_point_high { get; set; }

        public string sims_co_scholastic_grade_description { get; set; }

        public string sims_co_scholastic_grade_color_code { get; set; }
    }

    public class co_scholastic_mark
    {

        public object aca_year { get; set; }

        public object cur_code { get; set; }

        public object sims_subject_srl_no { get; set; }

        public object sims_co_scholastic_enroll_number { get; set; }

        public object sims_co_scholastic_discriptive_indicator_grade_code { get; set; }

        public object sims_co_scholastic_discriptive_indicator_grade { get; set; }

        public object sims_report_card_co_scholastic_grade_point { get; set; }

        public object sims_student_assign_exam_status { get; set; }

        public object sims_student_assign_entered_by { get; set; }

    }
    public class insertrubrics_grade
    {

        public object aca_year { get; set; }

        public object cur_code { get; set; }

        public object sims_subject_srl_no { get; set; }

        public object sims_co_scholastic_enroll_number { get; set; }

        public object sims_co_scholastic_discriptive_indicator_grade_code { get; set; }

        public object sims_co_scholastic_discriptive_indicator_grade { get; set; }

        public object sims_report_card_co_scholastic_grade_point { get; set; }

        public object sims_student_assign_exam_status { get; set; }

        public object sims_student_assign_entered_by { get; set; }

        public object grade_code { get; set; }

        public object section_code { get; set; }

        public object sims_report_card_config_code { get; set; }

        public object term_code { get; set; }

        public object flag { get; set; }

        public object sims_co_scholastic_group_code { get; set; }

        public object sims_co_scholastic_attribute_code { get; set; }

        public object sims_co_scholastic_descriptive_indicator_code { get; set; }

        public object sims_student_assign_comment { get; set; }

    }
    public class gradebook_commententry
    {
        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_report_card_term_desc { get; set; }

        public string sims_enroll_number { get; set; }

        public string Student_Name { get; set; }

        public string Student_TotalMark { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_term_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_comment_type { get; set; }

        public string sims_user_code { get; set; }

        public string sims_comment_desc { get; set; }

        public string s_sims_comment_desc { get; set; }

        public string sims_comment_desc_ot { get; set; }
    }

}