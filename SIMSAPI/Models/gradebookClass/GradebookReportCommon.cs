﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.GradebookReportCommon
{
    public class GBRQ011 {
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public List<GBRQ01> acad_data = new List<GBRQ01>();

    }
    public class GBRQ01
    {
         //   public string sims_academic_year	{ get; set; }
            public string sims_cur_code	{ get; set; }
            public string sims_grade_code	{ get; set; }
            public string sims_section_code	{ get; set; }
            public string no_of_students	{ get; set; }
            public string sims_sports_flag { get; set; }
            public string sims_grade_name_en	{ get; set; }
            public string sims_section_name_en	{ get; set; }
            public string sims_gb_term_code	{ get; set; }
            public string sims_gb_subject_code	{ get; set; }
            public string sims_subject_name_en	{ get; set; }
            public string grade_avarage	{ get; set; }
            public string below50	{ get; set; }
            public string between50to59	{ get; set; }
            public string between60to69	{ get; set; }
            public string between70to79	{ get; set; }
            public string between80to89	{ get; set; }
            public string between90to100 { get; set; }
            public string mapped_categories { get; set; }


            public string sims_gb_number { get; set; }

            public string sims_term_code { get; set; }

            public string sims_term_desc_en { get; set; }

            public string sims_gb_cat_code { get; set; }

            public string sims_gb_cat_name { get; set; }

            public string sims_academic_year { get; set; }

            public string sims_gb_cat_assign_number { get; set; }

            public string sims_gb_cat_assign_enroll_number { get; set; }

            public string final_mark { get; set; }

            public string sims_gb_cat_assign_final_grade { get; set; }

            public string final_grade { get; set; }

            public string sims_mark_grade_color_code { get; set; }

            public string sims_gb_cat_assign_status { get; set; }

            public string sims_gb_cat_assign_comment { get; set; }

            public string sims_gb_cat_assign_name { get; set; }

            public string student_name { get; set; }

            public string sims_gb_cat_assign_max_score { get; set; }

            public string sims_gb_cat_assign_max_score_correct { get; set; }

            public string sims_gb_remarks { get; set; }

            public string sims_gb_cat_points_possible { get; set; }

            public string sims_subject_type_name_en { get; set; }

            public string category_name { get; set; }

            public string category_weight { get; set; }

            public string sims_gb_name { get; set; }

            public string narr_grade_group_name { get; set; }

            public string sims_gb_cat_assign_include_in_final_grade { get; set; }

            public string sims_gb_cat_assign_weightage { get; set; }

            public string sims_gb_cat_assign_grade_completed_status { get; set; }

            public string sims_gb_grade_scale { get; set; }

            public string sims_gb_cat_assign_date { get; set; }

            public string sims_allocation_status { get; set; }

            public string perr { get; set; }

            public string final_marks { get; set; }

            public string sims_gb_cat_assign_status_cal { get; set; }

            public string per { get; set; }

            public string sims_activity_number { get; set; }

            public string sims_activity_name { get; set; }

            public string sims_activity_symbol { get; set; }

            public string sims_activity_max_point { get; set; }

            public string sims_activity_max_point_subject { get; set; }

            public string sims_activity_priority { get; set; }

            public string sims_activity_points { get; set; }

            public string sims_gb_cat_assign_mark { get; set; }

            public string marks_needed_to_pass { get; set; }

            public string passing_marks { get; set; }

            public string balance_activity { get; set; }

            public object sims_enroll_number { get; set; }

            public object sims_subject_code { get; set; }

            public object sims_marks_obtain { get; set; }

            public object sims_marks_applied { get; set; }

            public object sims_balance_activity_points { get; set; }
            public string bal_act { get; set; }
            public string sims_roll_number { get; set; }
            public string activity_student_has { get; set; }
            public string sims_activity_points_gen { get; set; }
            public string points_from_other_than_general_gracing { get; set; }
            public int sims_roll_number_sort { get; set; }
            public string user { get; set; }
            public string distinct_sims_gb_cat_assign_enroll_number { get; set; }
            public string distinct_sims_gb_subject_code { get; set; }
            public string total_student_pass_subject_count { get; set; }

            public string sims_passing_marks_of_student { get; set; }
            public string sims_roll_number_sort_1 { get; set; }
            public string opr { get; set; }
            public string name { get; set; }
            public string code { get; set; }
           public string tot_cat_weight { get; set; }

            public string sims_gracing_type { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string cnt_assign { get; set; }
        public string sims_category_name { get; set; }
        public string sims_category_sequence { get; set; }
        public List<activity_list> activities { get; set; }
        public List<category_list> category { get; set; }
        public List<stud_assign> stud_assign { get; set; }
        public List<Student> Student { get; set; }
    }


    public class Student {
            //   public string sims_academic_year	{ get; set; }
            public string sims_cur_code { get; set; }
            public string sims_grade_code { get; set; }
            public string sims_section_code { get; set; }
            public string no_of_students { get; set; }
            public string sims_grade_name_en { get; set; }
            public string sims_section_name_en { get; set; }
            public string sims_gb_term_code { get; set; }
            public string sims_gb_subject_code { get; set; }
            public string sims_subject_name_en { get; set; }
            public string grade_avarage { get; set; }
            public string below50 { get; set; }
            public string between50to59 { get; set; }
            public string between60to69 { get; set; }
            public string between70to79 { get; set; }
            public string between80to89 { get; set; }
            public string between90to100 { get; set; }


            public string sims_gb_number { get; set; }

            public string sims_term_code { get; set; }

            public string sims_term_desc_en { get; set; }

            public string sims_gb_cat_code { get; set; }

            public string sims_gb_cat_name { get; set; }

            public string sims_academic_year { get; set; }

            public string sims_gb_cat_assign_number { get; set; }

            public string sims_gb_cat_assign_enroll_number { get; set; }

            public string final_mark { get; set; }

            public string sims_gb_cat_assign_final_grade { get; set; }

            public string final_grade { get; set; }

            public string sims_mark_grade_color_code { get; set; }

            public string sims_gb_cat_assign_status { get; set; }

            public string sims_gb_cat_assign_comment { get; set; }

            public string sims_gb_cat_assign_name { get; set; }

            public string student_name { get; set; }

            public string sims_gb_cat_assign_max_score { get; set; }

            public string sims_gb_cat_assign_max_score_correct { get; set; }

            public string sims_gb_remarks { get; set; }

            public string sims_gb_cat_points_possible { get; set; }

            public string sims_subject_type_name_en { get; set; }

            public string category_name { get; set; }

            public string category_weight { get; set; }

            public string sims_gb_name { get; set; }

            public string narr_grade_group_name { get; set; }

            public string sims_gb_cat_assign_include_in_final_grade { get; set; }

            public string sims_gb_cat_assign_weightage { get; set; }

            public string sims_gb_cat_assign_grade_completed_status { get; set; }

            public string sims_gb_grade_scale { get; set; }

            public string sims_gb_cat_assign_date { get; set; }

            public string sims_allocation_status { get; set; }

            public string perr { get; set; }

            public string final_marks { get; set; }

            public string sims_gb_cat_assign_status_cal { get; set; }

            public string per { get; set; }

            public string sims_activity_number { get; set; }

            public string sims_activity_name { get; set; }

            public string sims_activity_symbol { get; set; }

            public string sims_activity_max_point { get; set; }

            public string sims_activity_max_point_subject { get; set; }

            public string sims_activity_priority { get; set; }

            public string sims_activity_points { get; set; }

            public string sims_gb_cat_assign_mark { get; set; }

            public string marks_needed_to_pass { get; set; }

            public string passing_marks { get; set; }

            public string balance_activity { get; set; }

            public object sims_enroll_number { get; set; }

            public object sims_subject_code { get; set; }

            public object sims_marks_obtain { get; set; }

            public object sims_marks_applied { get; set; }

            public object sims_balance_activity_points { get; set; }
            public string bal_act { get; set; }
            public string sims_roll_number { get; set; }
            public string activity_student_has { get; set; }
            public string sims_activity_points_gen { get; set; }
            public string points_from_other_than_general_gracing { get; set; }
            public int sims_roll_number_sort { get; set; }
            public string user { get; set; }
            public string distinct_sims_gb_cat_assign_enroll_number { get; set; }
            public string distinct_sims_gb_subject_code { get; set; }
            public string total_student_pass_subject_count { get; set; }

            public string sims_passing_marks_of_student { get; set; }
            public string sims_roll_number_sort_1 { get; set; }
            public string opr { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string tot_cat_weight { get; set; }


            public string sims_appl_parameter { get; set; }
            public string sims_appl_form_field_value1 { get; set; }
            public List<activity_list> activities { get; set; }
            public List<stud_assign> stud_assign { get; set; }
        
    }

    public class stud_assign
    {
        //   public string sims_academic_year	{ get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string no_of_students { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_gb_term_code { get; set; }
        public string sims_gb_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string grade_avarage { get; set; }
        public string below50 { get; set; }
        public string between50to59 { get; set; }
        public string between60to69 { get; set; }
        public string between70to79 { get; set; }
        public string between80to89 { get; set; }
        public string between90to100 { get; set; }


        public string sims_gb_number { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_gb_cat_code { get; set; }

        public string sims_gb_cat_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_gb_cat_assign_number { get; set; }

        public string sims_gb_cat_assign_enroll_number { get; set; }

        public string final_mark { get; set; }

        public string sims_gb_cat_assign_final_grade { get; set; }

        public string final_grade { get; set; }

        public string sims_mark_grade_color_code { get; set; }

        public string sims_gb_cat_assign_status { get; set; }

        public string sims_gb_cat_assign_comment { get; set; }

        public string sims_gb_cat_assign_name { get; set; }

        public string student_name { get; set; }

        public string sims_gb_cat_assign_max_score { get; set; }

        public string sims_gb_cat_assign_max_score_correct { get; set; }

        public string sims_gb_remarks { get; set; }

        public string sims_gb_cat_points_possible { get; set; }

        public string sims_subject_type_name_en { get; set; }

        public string category_name { get; set; }

        public string category_weight { get; set; }

        public string sims_gb_name { get; set; }

        public string narr_grade_group_name { get; set; }

        public string sims_gb_cat_assign_include_in_final_grade { get; set; }

        public string sims_gb_cat_assign_weightage { get; set; }

        public string sims_gb_cat_assign_grade_completed_status { get; set; }

        public string sims_gb_grade_scale { get; set; }

        public string sims_gb_cat_assign_date { get; set; }

        public string sims_allocation_status { get; set; }

        public string perr { get; set; }

        public string final_marks { get; set; }

        public string sims_gb_cat_assign_status_cal { get; set; }

        public string per { get; set; }

        public string sims_activity_number { get; set; }

        public string sims_activity_name { get; set; }

        public string sims_activity_symbol { get; set; }

        public string sims_activity_max_point { get; set; }

        public string sims_activity_max_point_subject { get; set; }

        public string sims_activity_priority { get; set; }

        public string sims_activity_points { get; set; }

        public string sims_gb_cat_assign_mark { get; set; }

        public string marks_needed_to_pass { get; set; }

        public string passing_marks { get; set; }

        public string balance_activity { get; set; }

        public object sims_enroll_number { get; set; }

        public object sims_subject_code { get; set; }

        public object sims_marks_obtain { get; set; }

        public object sims_marks_applied { get; set; }

        public object sims_balance_activity_points { get; set; }
        public string bal_act { get; set; }
        public string sims_roll_number { get; set; }
        public string activity_student_has { get; set; }
        public string sims_activity_points_gen { get; set; }
        public string points_from_other_than_general_gracing { get; set; }
        public int sims_roll_number_sort { get; set; }
        public string user { get; set; }
        public string distinct_sims_gb_cat_assign_enroll_number { get; set; }
        public string distinct_sims_gb_subject_code { get; set; }
        public string total_student_pass_subject_count { get; set; }

        public string sims_passing_marks_of_student { get; set; }
        public string sims_roll_number_sort_1 { get; set; }
        public string opr { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string tot_cat_weight { get; set; }


        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }

        public static implicit operator stud_assign(List<stud_assign> v)
        {
            throw new NotImplementedException();
        }
    }

    public class student_assignment_list
    {
        //   public string sims_academic_year	{ get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string no_of_students { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_gb_term_code { get; set; }
        public string sims_gb_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string grade_avarage { get; set; }
        public string below50 { get; set; }
        public string between50to59 { get; set; }
        public string between60to69 { get; set; }
        public string between70to79 { get; set; }
        public string between80to89 { get; set; }
        public string between90to100 { get; set; }


        public string sims_gb_number { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_gb_cat_code { get; set; }

        public string sims_gb_cat_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_gb_cat_assign_number { get; set; }

        public string sims_gb_cat_assign_enroll_number { get; set; }

        public string final_mark { get; set; }

        public string sims_gb_cat_assign_final_grade { get; set; }

        public string final_grade { get; set; }

        public string sims_mark_grade_color_code { get; set; }

        public string sims_gb_cat_assign_status { get; set; }

        public string sims_gb_cat_assign_comment { get; set; }

        public string sims_gb_cat_assign_name { get; set; }

        public string student_name { get; set; }

        public string sims_gb_cat_assign_max_score { get; set; }

        public string sims_gb_cat_assign_max_score_correct { get; set; }

        public string sims_gb_remarks { get; set; }

        public string sims_gb_cat_points_possible { get; set; }

        public string sims_subject_type_name_en { get; set; }

        public string category_name { get; set; }

        public string category_weight { get; set; }

        public string sims_gb_name { get; set; }

        public string narr_grade_group_name { get; set; }

        public string sims_gb_cat_assign_include_in_final_grade { get; set; }

        public string sims_gb_cat_assign_weightage { get; set; }

        public string sims_gb_cat_assign_grade_completed_status { get; set; }

        public string sims_gb_grade_scale { get; set; }

        public string sims_gb_cat_assign_date { get; set; }

        public string sims_allocation_status { get; set; }

        public string perr { get; set; }

        public string final_marks { get; set; }

        public string sims_gb_cat_assign_status_cal { get; set; }

        public string per { get; set; }

        public string sims_activity_number { get; set; }

        public string sims_activity_name { get; set; }

        public string sims_activity_symbol { get; set; }

        public string sims_activity_max_point { get; set; }

        public string sims_activity_max_point_subject { get; set; }

        public string sims_activity_priority { get; set; }

        public string sims_activity_points { get; set; }

        public string sims_gb_cat_assign_mark { get; set; }

        public string marks_needed_to_pass { get; set; }

        public string passing_marks { get; set; }

        public string balance_activity { get; set; }

        public object sims_enroll_number { get; set; }

        public object sims_subject_code { get; set; }

        public object sims_marks_obtain { get; set; }

        public object sims_marks_applied { get; set; }

        public object sims_balance_activity_points { get; set; }
        public string bal_act { get; set; }
        public string sims_roll_number { get; set; }
        public string activity_student_has { get; set; }
        public string sims_activity_points_gen { get; set; }
        public string points_from_other_than_general_gracing { get; set; }
        public int sims_roll_number_sort { get; set; }
        public string user { get; set; }
        public string distinct_sims_gb_cat_assign_enroll_number { get; set; }
        public string distinct_sims_gb_subject_code { get; set; }
        public string total_student_pass_subject_count { get; set; }

        public string sims_passing_marks_of_student { get; set; }
        public string sims_roll_number_sort_1 { get; set; }
        public string opr { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string tot_cat_weight { get; set; }


        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public List<stud_assign> stud_assign { get; set; }
    }

    public class category_list
    {
        //   public string sims_academic_year	{ get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string no_of_students { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_gb_term_code { get; set; }
        public string sims_gb_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string grade_avarage { get; set; }
        public string below50 { get; set; }
        public string between50to59 { get; set; }
        public string between60to69 { get; set; }
        public string between70to79 { get; set; }
        public string between80to89 { get; set; }
        public string between90to100 { get; set; }


        public string sims_gb_number { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_gb_cat_code { get; set; }

        public string sims_gb_cat_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_gb_cat_assign_number { get; set; }

        public string sims_gb_cat_assign_enroll_number { get; set; }

        public string final_mark { get; set; }

        public string sims_gb_cat_assign_final_grade { get; set; }

        public string final_grade { get; set; }

        public string sims_mark_grade_color_code { get; set; }

        public string sims_gb_cat_assign_status { get; set; }

        public string sims_gb_cat_assign_comment { get; set; }

        public string sims_gb_cat_assign_name { get; set; }

        public string student_name { get; set; }

        public string sims_gb_cat_assign_max_score { get; set; }

        public string sims_gb_cat_assign_max_score_correct { get; set; }

        public string sims_gb_remarks { get; set; }

        public string sims_gb_cat_points_possible { get; set; }

        public string sims_subject_type_name_en { get; set; }

        public string category_name { get; set; }

        public string category_weight { get; set; }

        public string sims_gb_name { get; set; }

        public string narr_grade_group_name { get; set; }

        public string sims_gb_cat_assign_include_in_final_grade { get; set; }

        public string sims_gb_cat_assign_weightage { get; set; }

        public string sims_gb_cat_assign_grade_completed_status { get; set; }

        public string sims_gb_grade_scale { get; set; }

        public string sims_gb_cat_assign_date { get; set; }

        public string sims_allocation_status { get; set; }

        public string perr { get; set; }

        public string final_marks { get; set; }

        public string sims_gb_cat_assign_status_cal { get; set; }

        public string per { get; set; }

        public string sims_activity_number { get; set; }

        public string sims_activity_name { get; set; }

        public string sims_activity_symbol { get; set; }

        public string sims_activity_max_point { get; set; }

        public string sims_activity_max_point_subject { get; set; }

        public string sims_activity_priority { get; set; }

        public string sims_activity_points { get; set; }

        public string sims_gb_cat_assign_mark { get; set; }

        public string marks_needed_to_pass { get; set; }

        public string passing_marks { get; set; }

        public string balance_activity { get; set; }

        public object sims_enroll_number { get; set; }

        public object sims_subject_code { get; set; }

        public object sims_marks_obtain { get; set; }

        public object sims_marks_applied { get; set; }

        public object sims_balance_activity_points { get; set; }
        public string bal_act { get; set; }
        public string sims_roll_number { get; set; }
        public string activity_student_has { get; set; }
        public string sims_activity_points_gen { get; set; }
        public string points_from_other_than_general_gracing { get; set; }
        public int sims_roll_number_sort { get; set; }
        public string user { get; set; }
        public string distinct_sims_gb_cat_assign_enroll_number { get; set; }
        public string distinct_sims_gb_subject_code { get; set; }
        public string total_student_pass_subject_count { get; set; }

        public string sims_passing_marks_of_student { get; set; }
        public string sims_roll_number_sort_1 { get; set; }
        public string opr { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string tot_cat_weight { get; set; }


        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_category_sequence { get; set; }
    }

    public class activity_list {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_enrollment_number { get; set; }
        public string sims_activity_points { get; set; }
        public string sims_activity_name { get; set; }
        public string sims_term_number { get; set; }
        public string sims_activity_number { get; set; }
    }

    public class teachertracker { 
        public string sims_academic_year	 { get; set; }
        public string sims_lesson_created_for	 { get; set; }
        public string teacher_name	 { get; set; }
        public string month_name	 { get; set; }
        public string monthDesc	 { get; set; }
        public string sims_lesson_subject_code	 { get; set; }
        public string sims_subject_name_en	 { get; set; }
        public string totalObservation { get; set; }
    }
}