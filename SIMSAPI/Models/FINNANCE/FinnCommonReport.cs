﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FinnCommonReport
{
    public class FIN01
    {
        //Family Analysis Report
        public float sltr_tran_amt_dr	{ get; set; }
        public float sltr_tran_amt_cr	{ get; set; }
        public float pdc_amount	{ get; set; }
        public float net_amt { get; set; }
        public float perc_amt { get; set; }
        public string with_pdc { get; set; }
        public float return_check_amt { get; set; }
        public string registration_status { get; set; }
        public string father_name	{ get; set; }
        public string sims_parent_number { get; set; }
       
        //Student Transport Fee Details
        public string sims_enroll_number{ get; set; }
        public string student_name      { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_transport_vehicle_name_plate { get; set; }
        public string sims_section_name_en   { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public double sims_fee_expected      { get; set; }
        public double sims_fee_concession { get; set; }
        public double sims_fee_paid_amount { get; set; }
        public double sims_fee_concession_paid { get; set; }

        public string opr { get; set; }
	    public string view { get; set; }
	    public string report_type { get; set; }
	    public string period_no { get; set; }
	    public string cur_code { get; set; }
	    public string acad_year { get; set; }
	    public string grade_code { get; set; }
	    public string transport_route_code { get; set; }
	    public string section_code { get; set; }
	    public string vehicle_code { get; set; }
	    public string transport_status { get; set; }
        public string search { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string comn_appl_parameter { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string class1 { get; set; }

        //student not register for next year
        public string sims_parent_name{ get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string parent_contact_number{ get; set; }
         public string sims_parent_mobile{ get; set; }
         public string number_of_sibling { get; set; }
         public string sims_parent_father_phone { get; set; }
         public string sims_parent_mother_phone { get; set; }
         public string sims_nationality_name_en { get; set; }
        
         public List<studlist> stud_list = new List<studlist>();

        // Concession Detail Report
         public string sims_concession_number { get; set; }
         public string sims_concession_description { get; set; }
         public string sims_concession_from_date	{ get; set; }
         public string sims_concession_to_date	{ get; set; }
         public string grade_name	{ get; set; }
         public string section_name	{ get; set; }
         public string concession_Description	{ get; set; }
         public string sims_concession_discount_value	{ get; set; }
         public string sims_student_date { get; set; }


         public string fee_status_desc { get; set; }

         public string sims_remark { get; set;}
        public string sltr_tran_amt_dr_final { get; set; }
        public string sltr_tran_amt_cr_final { get; set; }
        public string net_bal_trial_bal { get; set; }
        public string net_perc_exclude_OB { get; set; }
        public bool sims_status { get; set; }
    }
    public class studlist
    {
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
    }

    public class RAQR01
    {
        public string sims_academic_year { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string no_of_families { get; set; }
        public string parent_name { get; set; }
        public float balance0to1Year { get; set; }
        public float balance1to2Year { get; set; }
        public float balance2to3Year { get; set; }
        public float graterThan3Year { get; set; }
        public float total_amount { get; set; }
        public float ob_paid_amount { get; set; }        
        public float pdcInHand { get; set; }
        public float pdcDishonored { get; set; }
        public float expected_amt { get; set; }
        public string net_outstanding { get; set; }
        public string perc_net_outstanding { get; set; }
        public float seventyFIVE_nintyAMT { get; set; }
        public float greaterthannintyAMT { get; set; }
        public float eventyFIVE_nintyAMT { get; set; }
        public float fifty_seventyfourAMT { get; set; }
        public float lessthanfiftyAMT { get; set; }
        public string lessthanfiftyFAMITY { get; set; }
        public string greaterthannintyFAMITY { get; set; }
        public string seventyFIVE_nintyFAMITY { get; set; }
        public string fifty_seventyfourFamily { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }


        public float curYearInv { get; set; }
        public float prevYearInv { get; set; }
        public float prev_prevYearInv { get; set; }
        public float moreThan3Year { get; set; }
        public float nFive { get; set; }
        public float pc_amount { get; set; }
        public float current_year_collection { get; set; }
        public float actual_pc_amount { get; set; }
        public float return_check_amt { get; set; }


    }
}