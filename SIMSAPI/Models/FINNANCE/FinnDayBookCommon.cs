﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FinnDay
{
    public class FinnDayBookCommon
    {
        //day book 
        public string  fins_comp_code { get; set; }
        public string fins_appl_code { get; set; }
        public string fins_appl_form_field { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_name { get; set; }
        public string gltr_pstng_date { get; set; }
        public string gltr_doc_code { get; set; }
        public string gltr_our_doc_no { get; set; }
        public string gltr_acct_code { get; set; }
        public string gltr_acct_name { get; set; }
        public string gltr_dept_no { get; set; }
        public string gltr_doc_narr { get; set; }
        public string coce_cost_centre_code { get; set; }
        public string coce_cost_centre_name { get; set; }
        public string gltr_tran_amt_dr { get; set; }
        public string gltr_tran_amt_cr { get; set; }

        //public string comp_code { get; set; }
        //public string fin_year { get; set; }
        //public string start_date { get; set; }
        //public string end_date { get; set; }
        //public string doc_code { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_year { get; set; }
        public string year_description { get; set; }
        public string centre { get; set; }
        
       //Asset Litral Report
        public string comp_short_name { get; set; }
        public string rec_type_name { get; set; }
        public string gal_type_desc { get; set; }
        public string gal_asset_acno { get; set; }
        public string asset_acno_name { get; set; }
        public string gal_deprn_acno { get; set; }
        public string depr_acno_name { get; set; }
        public string gal_deprn_percent { get; set; }
        public string gal_asst_type { get; set; }
        
        //Balance Sheet Report
        public string group_name { get; set; }
        public string sched_name { get; set; }
        public string glac_name { get; set; }
        public string dept_name { get; set; }
        public string glma_op_bal_amt { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string cl_balance { get; set; }
      

        







       







        

        







  




 







    }
}