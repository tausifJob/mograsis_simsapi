﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SIMSAPI.Models.FINNANCE
{
    public class ComnFinance
    {

        public string pb_bank_name { get; set; }
        public string pc_cheque_no { get; set; }
        public string cheque_date { get; set; }
        public string enroll_no { get; set; }
        public string doc_date { get; set; }
        public string doc_ref_code { get; set; }
        public string amount { get; set; }
        public string comp_code { get; set; }
        public string comp_name{ get; set; }
        
        
        // assest master
         public string gam_item_no { get; set; }
        public string gam_desc { get; set; }
        public string gam_des { get; set; }
        public string gal_type_desc { get; set; }
        public string sims_location_desc { get; set; }
        public string emp_name { get; set; }
        public string sup_name { get; set; }
        public string gam_invoice_no { get; set; }
        public string gam_receipt_date{ get; set; }
        public string gam_entry_date { get; set; }
        public string gam_quantity { get; set; }
        public string gam_order_no { get; set; }
        public string gam_invoice_amount { get; set; }
        public string gam_book_value { get; set; }
         public string gam_cum_deprn { get; set; }
         public string gam_deprn_percent { get; set; }
        public string gam_status { get; set; }
        public string gal_asst_type { get; set; }
        public string gam_mth_deprn { get; set; } 

        // gl summary
      
        public string doc_no { get; set; }
         public string gltr_pstng_date { get; set; }
         public string gltr_doc_narr { get; set; }
        public string debit_amt { get; set; }
        public string credit_amt { get; set; }
        
          public string glma_acct_code { get; set; }
         public string glma_acct_name { get; set; }
        public string dept_name { get; set; }
        public string codp_dept_no { get; set; }
    }
}