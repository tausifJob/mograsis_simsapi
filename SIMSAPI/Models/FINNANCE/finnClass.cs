﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FINNANCE
{
    public class finnClass
    {

        public class FCBD01
        {
            public string opr { get; set; }
            public string comp_bank_code { get; set; }
            public string comp_bank_name { get; set; }
            public string comp_bank_branch_name { get; set; }
            public string comp_bank_address { get; set; }
            public string comp_bank_account_no { get; set; }
            public string comp_bank_iban_no { get; set; }
            public string comp_route_code { get; set; }
            public string comp_ifsc_code { get; set; }
            public string comp_swift_code { get; set; }
            public bool comp_bank_status { get; set; }

        }

        


        #region Finn021
        public class Finn021
        {
            public string gam_comp_code { get; set; }
            public string gam_comp_code_name { get; set; }
            public string gam_dept_code { get; set; }
            public string gam_dept_code_name { get; set; }
            public string gam_asst_type { get; set; }
            public string gam_asst_type_name { get; set; }
            public int gam_item_no { get; set; }
            public string gam_item_no1 { get; set; }
            public string gam_location { get; set; }
            public string gam_location_name { get; set; }
            public string gam_desc_1 { get; set; }
            public string gam_desc_2 { get; set; }
            public string gam_supl_name { get; set; }
            public string gam_acct_code { get; set; }
            public string gam_acct_code_name { get; set; }
            public string gam_order_no { get; set; }
            public string gam_invoice_no { get; set; }
            public string gam_new_sec { get; set; }
            public decimal gam_page_no { get; set; }
            public decimal gam_line_no { get; set; }
            public decimal gam_quantity { get; set; }
            public decimal gam_invoice_amount { get; set; }
            public decimal gam_val_add_cum { get; set; }
            public decimal gam_val_add_mtd { get; set; }
            public decimal gam_val_add_ytd { get; set; }
            public decimal gam_book_value { get; set; }
            public decimal gam_cum_deprn { get; set; }
            public decimal gam_ytd_deprn { get; set; }
            public decimal gam_mth_deprn { get; set; }
            public decimal gam_sale_value { get; set; }
            public decimal gam_repl_cost { get; set; }
            public string gam_receipt_date { get; set; }
            public string gam_sale_date { get; set; }
            public string gam_entry_date { get; set; }
            public string gam_amend_date { get; set; }
            public decimal gam_deprn_percent { get; set; }
            public decimal gam_used_on_item { get; set; }
            public string gam_status_name { get; set; }

            //Summary Variables
            public int gam_items { get; set; }
            public decimal gam_total_quantity { get; set; }
            public decimal gam_amount { get; set; }
            public string gam_sold { get; set; }
            public string gam_delete { get; set; }
            public string gam_dep { get; set; }
            public string gam_status { get; set; }

            public string gam_comp_name { get; set; }
        }
        #endregion
    }

    #region Finn003
    public class Finn003
    {
        public string glac_acct_code { get; set; }

        public string comp_inter_comp_acct { get; set; }

        public string comp_against_inter_comp_acct { get; set; }

        public string glac_acct_name { get; set; }
    }
    #endregion

    

    #region Finn226

    public class Finn226
    {
        public string gltd_doc_code { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_post_date { get; set; }
        public string pty_name { get; set; }
        public string voucher_amt { get; set; }
    }

    #endregion



    #region Finn155
    public class Finn155
    {
        public List<Finn155> AccCollection { get; set; }

        public string fins_fee_cur_code { get; set; }
        public string fins_fee_academic_year { get; set; }
        public string fins_fee_grade_code { get; set; }
        public string fins_fee_section_code { get; set; }
        public string fins_fee_number { get; set; }
        public string sims_fee_code_description { get; set; }
        public string fins_fee_desc { get; set; }
        public string fins_account_name { get; set; }
        public string fins_revenue_acno { get; set; }
        public string fins_advance_received_acno { get; set; }
        public string fins_advance_received_acno_jan { get; set; }
        public string fins_advance_received_acno_feb { get; set; }
        public string fins_advance_received_acno_mar { get; set; }
        public string fins_advance_received_acno_apr { get; set; }
        public string fins_advance_received_acno_may { get; set; }
        public string fins_advance_received_acno_jun { get; set; }
        public string fins_advance_received_acno_jul { get; set; }
        public string fins_advance_received_acno_aug { get; set; }
        public string fins_advance_received_acno_sep { get; set; }
        public string fins_advance_received_acno_oct { get; set; }
        public string fins_advance_received_acno_nov { get; set; }
        public string fins_advance_received_acno_dec { get; set; }
        public string fins_receivable_acno { get; set; }
        public bool fins_fees_posting_status { get; set; }
        public string receipt_based { get; set; }
        public string fins_discount_acno { get; set; }

        public string fins_advance_academic_year_acno { get; set; }

        public string opr { get; set; }

        public string fins_revenue_acname { get; set; }

        public string fins_receivable_acname { get; set; }

        public string fins_discount_acname { get; set; }

        public string fins_advance_academic_year_acname { get; set; }

        public string fins_advance_received_acname_jan { get; set; }

        public string fins_advance_received_acname_feb { get; set; }

        public string fins_advance_received_acname_mar { get; set; }

        public string fins_advance_received_acname_apr { get; set; }

        public string fins_advance_received_acname_may { get; set; }

        public string fins_advance_received_acname_jun { get; set; }

        public string fins_advance_received_acname_jul { get; set; }

        public string fins_advance_received_acname_aug { get; set; }

        public string fins_advance_received_acname_sep { get; set; }

        public string fins_advance_received_acname_oct { get; set; }

        public string fins_advance_received_acname_nov { get; set; }

        public string fins_advance_received_acname_dec { get; set; }
    }
    #endregion


    #region Finn064(GL Query)
    public class Finn064
    {
        public string opr { get; set; }
        public string gldc_doc_name { get; set; }
        public string gltr_comp_code { get; set; }
        public string gltr_dept_no { get; set; }
        public string gltr_acct_code { get; set; }
        public string gltr_acct_chk_dgt { get; set; }
        public string gltr_pstng_date { get; set; }
        public string gltr_pstng_type { get; set; }
        public int gltr_pstng_no { get; set; }
        public int gltr_pstng_seq_no { get; set; }
        public string gltr_user_id { get; set; }
        public string gltr_doc_code { get; set; }
        public string gltr_our_doc_no { get; set; }
        public string gltr_doc_date { get; set; }
        public string gltr_doc_narr { get; set; }
        public decimal gltr_tran_amt { get; set; }
        public decimal gltr_running_total_amt { get; set; }
        public decimal gltr_dr_amount { get; set; }
        public decimal gltr_cr_amount { get; set; }
        public decimal gltr_total_dr_amount { get; set; }
        public decimal gltr_total_cr_amount { get; set; }
        public decimal gltr_fc_amt { get; set; }
        public decimal gltr_fc_dr_amount { get; set; }
        public decimal gltr_fc_cr_amount { get; set; }
        public decimal gltr_tran_qty { get; set; }
        public string gltr_cur_date { get; set; }
        public string gltr_anal_code { get; set; }
        public string gltr_fc_code { get; set; }
        public string gltr_tran_uom { get; set; }
        public string gltr_eff_date { get; set; }
        public int gltr_year { get; set; }
        public int gltr_prd_no { get; set; }
        public string gltr_attr_class1 { get; set; }
        public string gltr_attr1 { get; set; }
        public string gltr_attr_class2 { get; set; }
        public string gltr_attr2 { get; set; }
        public string gltr_attr_class3 { get; set; }
        public string gltr_attr3 { get; set; }
        public string gltr_attr_class4 { get; set; }
        public string gltr_attr4 { get; set; }
        public string gltr_attr_class5 { get; set; }
        public string gltr_attr5 { get; set; }
        public decimal gltr_tran_amt_orig { get; set; }
        public decimal gltr_conv_rate { get; set; }
        public decimal gltr_conv_rate_orig { get; set; }
        public string gltr_source { get; set; }
        public string coce_cost_centre_name{ get; set; }
        public string glco_cost_centre_code { get; set; }
        public string gdn_comp_code { get; set; }
        public string gdn_doc_code { get; set; }
        public string gdn_doc_narration_code { get; set; }
        public string gdn_doc_narration { get; set; }
        public bool gdn_doc_narration_status { get; set; }
        public int comp_code { get; set; }
        public string finance_year { get; set; }
        public string gltd_doc_code { get; set; }
    }
    #endregion

    #region Fin123(SL Query)
    public class Fin123
    {
        public string slma_acno { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_ldgrctl_code1 { get; set; }

        public string coad_pty_short_name { get; set; }
        public string coad_xref_addr_id { get; set; }
        public string coad_dept_no { get; set; }
        public string codp_dept_name { get; set; }
        public string slma_pty_bank_acno { get; set; }
        public string slma_yob_amt { get; set; }
        public string slma_cr_limit { get; set; }
        public string slma_credit_prd { get; set; }
        public string slma_stop_cr_ind { get; set; }



        public string sltr_comp_code { get; set; }
        public string sltr_ldgr_code { get; set; }
        public string sltr_ldgr_name { get; set; }
        public string sltr_slmast_acno { get; set; }
        public string sltr_pstng_date { get; set; }
        public string sltr_pstng_type { get; set; }
        public string sltr_pstng_no { get; set; }
        public string sltr_pstng_seq_no { get; set; }
        public string sltr_doc_code { get; set; }
        public string sltr_our_doc_no { get; set; }
        public string sltr_doc_date { get; set; }
        public string sltr_pty_ref_no { get; set; }
        public string sltr_doc_narr { get; set; }
        public string sltr_tran_amt { get; set; }
        public string sltr_fc_amt { get; set; }
        public string sltr_tran_qty { get; set; }
        public string sltr_user_id { get; set; }
        public string sltr_dept_no { get; set; }
        public string sltr_cur_date { get; set; }
        public string sltr_match_ind { get; set; }
        public string sltr_match_ref_no { get; set; }
        public string sltr_fc_code { get; set; }
        public string sltr_tran_uom { get; set; }
        public string sltr_eff_date { get; set; }
        public string sltr_anal_code { get; set; }
        public string sltr_cal_type { get; set; }
        public string sltr_pty_bank_id { get; set; }
        public string sltr_due_date { get; set; }
        public string sltr_match_date { get; set; }
        public string sltr_year { get; set; }
        public string sltr_prd_no { get; set; }
        public string sltr_attr_class1 { get; set; }
        public string sltr_attr1 { get; set; }
        public string sltr_attr_class2 { get; set; }
        public string sltr_attr2 { get; set; }
        public string sltr_attr_class3 { get; set; }
        public string sltr_attr3 { get; set; }
        public string sltr_attr_class4 { get; set; }
        public string sltr_attr4 { get; set; }
        public string sltr_attr_class5 { get; set; }
        public string sltr_attr5 { get; set; }
        public string sltr_term_name { get; set; }
        public string sltr_tran_amt_orig { get; set; }
        public string sltr_conv_rate { get; set; }
        public string sltr_conv_rate_orig { get; set; }
        public string pb_amount { get; set; }
        public string sltr_source { get; set; }
        public string sltr_print_status { get; set; }
        public string sltr_bank_id { get; set; }
        public string sltr_dr_amount { get; set; }
        public decimal sltr_cr_amount { get; set; }
        public string sltr_fc_dr_amount { get; set; }
        public decimal sltr_fc_cr_amount { get; set; }
        public string sltr_yob_amt { get; set; }

        public string pc_dept_no { get; set; }
        public string pc_doc_type { get; set; }
        public string pc_our_doc_no { get; set; }
        public string pc_due_date { get; set; }

        public string pc_bank_from { get; set; }
        public string pc_bank_slno { get; set; }
        public string pc_cheque_no { get; set; }
        public string pc_discd { get; set; }
        public decimal pc_amount { get; set; }
        public string pc_ref_no { get; set; }

        public string pb_dept_no { get; set; }
        public string pb_doc_type { get; set; }
        public string pb_our_doc_no { get; set; }
        public string pb_due_date { get; set; }
        public string pb_bank_from { get; set; }
        public string pb_bank_slno { get; set; }
        public string pb_cheque_no { get; set; }
        public string pb_discd { get; set; }

        public decimal pb_amount1 { get; set; }
        public string pb_ref_no { get; set; }


        public string sllc_ldgr_name { get; set; }
    }
    #endregion


    public class Finn139
    {
        public string gldd_ledger_code { get; set; }

        public string slma_ldgrctl_code { get; set; }

        public string slma_pty_bank_id { get; set; }

        public string slma_pty_bank_name { get; set; }

        public string pb_gl_acno { get; set; }

        public string company_name { get; set; }

        public string gldd_acct_code { get; set; }

        public string gldd_acct_name { get; set; }

        public string gldd_party_ref_no { get; set; }

        public string gldd_dept_code { get; set; }

        public string gldd_dept_name { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public decimal gldd_doc_amount { get; set; }

        public decimal gldd_doc_amount_debit { get; set; }

        public string gltd_cur_status { get; set; }

        public string glma_acct_code { get; set; }

        public string fins_appl_parameter { get; set; }

        public string fins_appl_form_field_value1 { get; set; }

        public string acc_code { get; set; }

        public string dept_code { get; set; }

    }

    public class Finn139A : finnClass
    {

        #region Finn139...

        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public decimal gltd_post_no { get; set; }
        public decimal gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }



        ////// fins_temp_doc_details
        public string gldd_acct_code_gl_master { get; set; }
        public string glma_acct_name_gl_master { get; set; }
        public string gldd_dept_code_gl_master { get; set; }
        public string gldd_dept_name_gl_master { get; set; }


        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        //public double gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_fc_name { get; set; }
        ////fins_currency_master
        public string excg_curcy_desc { get; set; }

        ////////slgr_master
        public string slma_ldgrctl_code { get; set; }


        ////////////////fins_financial_documents
        public decimal gldc_next_prv_no { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }
        #endregion

        public string gltd_reference_doc_code { get; set; }

        public string gltd_reference_final_doc_no { get; set; }

        public List<Finn139A> detailsdata { get; set; }
    }

    public class Fin102
    {

        public string gltd_doc_code { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string dept_no { get; set; }
        public string from_date { get; set; }
        public string ldgr_code { get; set; }
        public string ledger_code { get; set; }
        public string ledger_name { get; set; }
        public string fins_appl_form_field_value1 { get; set; }
        public string fins_appl_parameter { get; set; }
        public string opr { get; set; }
        public string pc_amount { get; set; }
        public string pc_bank_from_name { get; set; }
        public string pc_cheque_no { get; set; }
        public string pc_cur_date { get; set; }
        public DateTime pc_cur_date1 { get; set; }

        public string pc_dept_no { get; set; }
        public string pc_our_doc_no { get; set; }
        public string pc_ref_no { get; set; }
        public string pc_sl_acno { get; set; }
        public string pc_sl_acno_name { get; set; }
        public string pc_submission_date { get; set; }
        public DateTime pc_submission_date1 { get; set; }
        public decimal ps_amount { get; set; }
        public string ps_bank_code { get; set; }
        public string ps_bank_code_name { get; set; }
        public string ps_cheque_no { get; set; }
        public string ps_comp_code { get; set; }

        public string ps_approv_date { get; set; }

        public string ps_dept_no { get; set; }
        public string ps_discd { get; set; }
        public string ps_discd_desc { get; set; }

        public string ps_due_date { get; set; }
        public DateTime ps_due_date1 { get; set; }
        public string ps_ldgr_code { get; set; }
        public string ps_ldgr_code_name { get; set; }
        public string ps_rec_type { get; set; }
        public bool ps_select { get; set; }
        public string ps_sl_acno { get; set; }
        public string ps_sl_acno_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string to_date { get; set; }

        public string pdc_from_date { get; set; }
        public string pdc_to_date { get; set; }
        public bool temp_select { get; set; }
        public int cnt { get; set; }

        public string ps_dept_name { get; set; }

        public string ps_numeric { get; set; }

        public string ps_bank_slno { get; set; }

        public string createdBY { get; set; }

        public string createdby_usr { get; set; }

        public string recFromDate { get; set; }

        public string recToDate { get; set; }

        public string pc_payment_mode { get; set; }

        public string filterRange { get; set; }

        public string ps_due_dateStr { get; set; }

        public string subMissionDate { get; set; }

        public string filterText { get; set; }

        public string GradeSec { get; set; }

        public string pc_calendar { get; set; }

        public string pc_remark { get; set; }

        public string pc_narrative { get; set; }
        public string pc_father_full_name { get; set; }
        public string pc_mother_full_name { get; set; }
        public string pc_parent_number { get; set; }

        public string pdc_clearance_date { get; set; }

        public string sims_default_bank { get; set; }




        public string pc_bank_to { get; set; }
    }


    public class Fin138
    {


        public string acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_acct_name { get; set; }
        public string opr { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_bank_name { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_sl_acno { get; set; }
        public string pb_sl_code { get; set; }
        public string pb_srl_no { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_ldgrctl_code1 { get; set; }
    }

    public class Finn102
    {

        public string comp_name { get; set; }

        public string comp_code { get; set; }

        public decimal financial_year { get; set; }

        public string sims_academic_year_description { get; set; }
        public string comp_short_name { get; set; }
        public string comp_input_vat_posting_account_code { get; set; }
        public string include_vat_enable_status { get; set; }
        public string comp_vat_per { get; set; }
    }

    #region Finn006(Asset Literals)
    public class Finn006
    {
        public string gal_comp_code { get; set; }
        public string gal_asst_type { get; set; }
        public string gal_rec_type { get; set; }
        public string gal_rec_type_name { get; set; }
        public string gal_type_desc { get; set; }
        public string gal_asset_acno { get; set; }
        public string gal_asset_acno_name { get; set; }
        public string gal_deprn_acno { get; set; }
        public string gal_deprn_acno_name { get; set; }
        public string gal_resrv_acno { get; set; }
        public string gal_resrv_acno_name { get; set; }

        //public string gal_asst_type_name { get; set; }

        public string gal_comp_code_name { get; set; }

        public string gal_deprn_percent { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Finn010(Department)
    public class Finn010
    {

        public string dept_comp_name { get; set; }

        public string dept_comp_code { get; set; }

        public string accd_comp_name { get; set; }

        public string fp_comp_name { get; set; }

        public string excg_comp_code_name { get; set; }

        public string dept_no { get; set; }

        public string dept_division { get; set; }

        public string dept_short_name { get; set; }

        public string dept_type { get; set; }

        public string dept_type_code { get; set; }

        public string dept_name { get; set; }

        public decimal dept_year { get; set; }

        public bool dept_status { get; set; }

        public string dept_paswrd { get; set; }

        public string comp_code { get; set; }

        public decimal financial_year { get; set; }
    }
    #endregion

    #region Finn013...
    public class Finn013
    {
        public bool ischecked { get; set; }
        public decimal glpr_bud_amt { get; set; }
        public decimal glpr_bud_qty { get; set; }
        public decimal glpr_rev_bud_amt { get; set; }
        public decimal glpr_rev_bud_qty { get; set; }
        public decimal glpr_prd_no { get; set; }
        public string glpr_comp_code { get; set; }
        public decimal glpr_year { get; set; }
        public string glpr_dept_no { get; set; }
        public string glpr_acct_code { get; set; }
        public string comp_name { get; set; }
        public string deptname { get; set; }
        public string account_name { get; set; }
        public string account_code { get; set; }
        public string glpr_dept_desc { get; set; }
        public string opr { get; set; }
        public decimal glpr_bud_debit { get; set; }
        public decimal glpr_bud_credit { get; set; }
    }
    #endregion

    #region Finn016
    public class Finn016
    {
        public string slma_comp_code { get; set; }
        public string slma_comp_name { get; set; }
        public string slma_ldgrctl_year { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_ldgrctl_name { get; set; }
        public string slma_acno { get; set; }
        public string slma_acno_comp_code { get; set; }
        public string slma_acno_dept_no { get; set; }
        public string slma_acno_accnt_code { get; set; }
        public string slma_addr_id { get; set; }
        public string slma_addr_type { get; set; }
        public string slma_addr_type_name { get; set; }
        public bool slma_status { get; set; }
        public string slma_cntrl_class { get; set; }
        public string slma_cntrl_class_name { get; set; }
        public string slma_pty_type { get; set; }
        public string slma_pty_type_name { get; set; }
        public string slma_ctry_code { get; set; }
        public string slma_ctry_name { get; set; }
        public string slma_xref_ldgr_code { get; set; }
        public string slma_xref_ldgr_name { get; set; }
        public string slma_xref_acno { get; set; }
        public bool slma_fc_flag { get; set; }
        public bool slma_qty_flag { get; set; }
        public bool slma_remove_tran { get; set; }
        public decimal slma_yob_amt { get; set; }
        public decimal slma_outstg_dr_amt { get; set; }
        public decimal slma_outstg_cr_amt { get; set; }
        public decimal slma_for_curcy_bal_amt { get; set; }
        public decimal slma_tdy_dr_amt { get; set; }
        public decimal slma_tdy_cr_amt { get; set; }
        public decimal slma_cr_limit { get; set; }
        public decimal slma_loan_inst_amt { get; set; }
        public decimal slma_orig_loan_amt { get; set; }
        public decimal slma_ovrid_amt { get; set; }
        public decimal slma_bank_clos_bal_amt { get; set; }
        public string slma_curcy_code { get; set; }
        public string slma_curcy_name { get; set; }
        public bool slma_cash_crd_ind { get; set; }
        public bool slma_stop_cr_ind { get; set; }
        public string slma_pty_bank_acno { get; set; }
        public string slma_bank_stmt_date { get; set; }
        public string slma_pmt_start_date { get; set; }
        public string slma_doc_date { get; set; }
        public string slma_pty_bank_id { get; set; }
        public string slma_pty_bank_name { get; set; }
        public string slma_loan_inst_type { get; set; }
        public string slma_loan_inst_type_name { get; set; }
        public int slma_credit_prd { get; set; }
        public int slma_ovrid_prd { get; set; }
        public int slma_stmt_prd { get; set; }
        public string slma_sched_code { get; set; }
        public string slma_sched_name { get; set; }
        public string slma_pmt_term { get; set; }
        public string slma_qty_code { get; set; }
        public string slma_last_pmt_date { get; set; }
        public decimal slma_last_pmt_amt { get; set; }
        public decimal slma_int_rate { get; set; }
        public int slma_int_prd { get; set; }
        public string slma_term_name { get; set; }
        public decimal slma_outstg_dr_amt_orig { get; set; }
        public decimal slma_outstg_cr_amt_orig { get; set; }
        public decimal slma_yob_amt_orig { get; set; }
        public string slma_reval_date { get; set; }
        public string slma_cntrl_class_org { get; set; }
        public string slma_credit_remark { get; set; }
        // fins_pty_address
        public string coad_dept_no { get; set; }
        public string coad_xref_addr_id { get; set; }
        public string coad_xref_addr_name { get; set; }
        public string coad_pty_full_name { get; set; }
        public string coad_pty_short_name { get; set; }
        public string coad_alt_key { get; set; }
        public string coad_pty_arab_name { get; set; }
        public string coad_line_1 { get; set; }
        public string coad_line_2 { get; set; }
        public string coad_po_box { get; set; }
        public string coad_tel_no { get; set; }
        public string coad_fax { get; set; }
        public string coad_telex { get; set; }
        public string coad_region_code { get; set; }
        public string coad_region_name { get; set; }
        public string coad_class_level1 { get; set; }
        public string coad_class_level2 { get; set; }
        public string coad_class_level3 { get; set; }
        public string coad_class_level4 { get; set; }
        public string coad_class_level5 { get; set; }
        public string coad_sms_no { get; set; }

        public string coad_email { get; set; }

        public string opr { get; set; }

        public string financialyear { get; set; }

        public string opr_ins { get; set; }
        public string sllc_ldgr_name { get; set; }
    }
    #endregion

    #region Finn012
    public class Finn012
    {
        public string comp_code { get; set; }
        public string fp_comp_name { get; set; }
        public int prd_year { get; set; }
        public int prd_no { get; set; }
        public string prd_name { get; set; }
        public string prd_st_dt { get; set; }
        public string prd_end_dt { get; set; }
        public int prd_days { get; set; }
        public string prd_jgen_dt { get; set; }
        public bool status { get; set; }

        public string dept_comp_name { get; set; }

        public string opr { get; set; }

        public string dept_comp_code { get; set; }

        public string glfp_year_desc { get; set; }

        public object finance_year { get; set; }

        public object user_name { get; set; }
    }
    #endregion

    public class Finn014 : finnClass
    {
        #region Finn014...
        public string comp_code { get; set; }
        public string finance_year { get; set; }
        public string opr { get; set; }
        public string glpr_bud_amt { get; set; }
        public string account_budget_quntity { get; set; }
        public string glma_comp_code { get; set; }
        public decimal glma_year { get; set; }
        public string glma_acct_code { get; set; }
        public string glac_comp_code { get; set; }
        public bool glma_check_digit { get; set; }
        public string glma_acct_name { get; set; }
        public decimal glma_tdy_dr { get; set; }
        public decimal glma_tdy_cr { get; set; }
        public decimal glma_net_dr { get; set; }
        public decimal glma_net_cr { get; set; }
        public decimal glma_op_bal_amt { get; set; }
        public decimal glma_op_bal_amt_old_val { get; set; }
        public decimal glma_yr_end_tran_amt { get; set; }
        public string login_user{ get; set; }
        public string current_date { get; set; }
        public bool glma_status { get; set; }
        public bool glma_stmt_reqd { get; set; }
        public string glma_ldgr_code { get; set; }
        public string glma_pty_class { get; set; }
        public bool glma_fc_flag { get; set; }
        public bool glma_qty_flag { get; set; }
        public decimal glma_net_dr_orig { get; set; }
        public decimal glma_net_cr_orig { get; set; }
        public decimal glma_op_bal_amt_orig { get; set; }
        public DateTime? glma_reval_date { get; set; }

        public string sims_student_enroll_number{ get; set; }
        public string student_name{ get; set; }
        public string grade_name{ get; set; }
        public string section_name{ get; set; }
        public string father_name{ get; set; }
        public string mother_name{ get; set; }
        public string sims_sibling_parent_number { get; set; }


        #endregion

        public string department_name { get; set; }

        public string glma_dept_no { get; set; }
        public string sims_financial_year_description { get; set; }



        public string slac_cntrl_class_desc { get; set; }

        public string glma_ldgr_name { get; set; }

        public string codp_dept_name { get; set; }

        public string glma_comp_name { get; set; }

        public string glpr_bud_qty { get; set; }



        public object user_name { get; set; }
    }

    public class Finn001
    {
        public string comp_code { get; set; }
        public string glac_name { get; set; }
        public string glac_acct_code { get; set; }
        public string glac_type { get; set; }
        public string glac_type_code { get; set; }
        public bool post_allow { get; set; }
        public bool glac_status { get; set; }
        public bool glac_post_allow { get; set; }
        public string glac_sched_code { get; set; }
        public string glac_sched_name { get; set; }
        public string glac_parent_acct { get; set; }
        public string glac_parent_acct_code { get; set; }
        public string Post { get; set; }

        public string opr { get; set; }

        public string comp_name { get; set; }

        public string glac_type1 { get; set; }

        public string glac_parent_acct1 { get; set; }

        public string glac_sched_code1 { get; set; }

        public string glac_sched_desc { get; set; }

        public string glac_year { get; set; }

        public object user_name { get; set; }

        public string glac_display_acct_code { get; set; }

        public string glma_dept_no { get; set; }
    }

    public class Fin206
    {
        public string gdua_comp_code { get; set; }
        public string gdua_year { get; set; }
        public string gdua_dept_no { get; set; }
        public string gdua_doc_code { get; set; }
        public string gdua_acct_code { get; set; }
        public string gdua_doc_user_code { get; set; }
        public string comn_user_name { get; set; }
        public bool gdua_status { get; set; }
        public string comn_user_group_code { get; set; }
        public string comp_name { get; set; }
        public string codp_dept_name { get; set; }
        public string gldc_doc_name { get; set; }
        public string glma_acct_name { get; set; }
        public string user_name { get; set; }
        public string comp_code { get; set; }
        public string gdua_dept_name { get; set; }
        public string glma_acct_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string opr { get; set; }
        public string gdua_year_code { get; set; }
    }
    public class Fin043
    {
        public string excg_comp_code { get; set; }
        public string excg_comp_code_name { get; set; }
        public string excg_curcy_code { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal excg_min_rate { get; set; }
        public decimal excg_max_rate { get; set; }
        public string excg_deci_desc { get; set; }
        public int excg_curcy_decimal { get; set; }
        public bool excg_enabled_flag { get; set; }
        public decimal excg_reval_rate { get; set; }
        public string excg_effective_date { get; set; }
        public string comp_name { get; set; }
        public bool status { get; set; }
        public string opr { get; set; }
        public string gdua_comp_code { get; set; }
    }

    public class Fin003
    {
        public string comp_cntry_name { get; set; }
        public string comp_cntry_code { get; set; }
        public string comp_curcy_name { get; set; }
        public int comp_curcy_dec { get; set; }
        public string comp_curcy_decimal_desc { get; set; }
        public string comp_curcy_code { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string comp_short_name { get; set; }
        public string comp_max_dept { get; set; }
        public string comp_min_dept { get; set; }
        public bool comp_enable_chk_dgt { get; set; }
        public bool comp_allow_multi_curcy { get; set; }
        public bool comp_allow_job_ledger { get; set; }
        public string comp_final_pl_acct { get; set; }
        public string comp_exchg_gain_loss { get; set; }
        public string comp_inter_comp_acct { get; set; }
        public string comp_suspect_acct { get; set; }
        public string comp_final_pl_acct_name { get; set; }
        public string comp_inter_comp_acct_name { get; set; }
        public string comp_exchg_gain_loss_name { get; set; }
        public string comp_suspect_acct_name { get; set; }
        public bool comp_allow_inter_comp_jv { get; set; }
        public bool comp_setup_status { get; set; }
        public string comp_addr_line1 { get; set; }
        public string comp_addr_line2 { get; set; }
        public string comp_addr_line3 { get; set; }
        public string comp_tel_no { get; set; }
        public string comp_fax_no { get; set; }
        public string comp_email { get; set; }
        public string comp_web_site { get; set; }
        public string opr { get; set; }

        public string comp_incorporation_number { get; set; }
	    public string comp_incorporation_reg_date { get; set; }
	    public string comp_incorporation_renewal_date { get; set; }
	    public string comp_incorporation_renewal_remind_days { get; set; }
	    public string comp_pf_number { get; set; }
	    public string comp_pf_reg_date { get; set; }
	    public string comp_pf_posting_account_code { get; set; }
	    public string comp_sales_tax_number { get; set; }
	    public string comp_sales_tax_reg_date { get; set; }
	    public string comp_sales_tax_posting_account_code { get; set; }
	    public string comp_vat_number { get; set; }
	    public string comp_vat_reg_date { get; set; }
	    public string comp_vat_posting_account_code { get; set; }
	    public bool comp_vat_roundup_calculation{ get; set; }

        public string comp_input_vat_posting_account_code { get; set; }
        public bool include_vat_enable_status { get; set; }
        public string comp_vat_roundup_posting_account_code { get; set; }




        public string comp_vat_per { get; set; }
    }

    public class Fin153
    {
        public string opr { get; set; }
        public string opr_upd { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string Full_name { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_bank_name { get; set; }
        public string Full_Bank_Name { get; set; }
        public string em_number { get; set; }
        public string Employee_Full_Name { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_doc_amount { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_amount { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string em_dept_code { get; set; }
        public string Empname { get; set; }
        public string data1 { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string codp_dept_name { get; set; }
        public string glsg_parent_group { get; set; }
        public string gldd_fc_rate { get; set; }
        public string gldd_line_no { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_comp_code { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_dept_code { get; set; }
        public string coad_dept_no { get; set; }
        public string fins_party_ref_date { get; set; }
        public string fins_past_date { get; set; }
        public string coad_pty_short_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string glma_acno { get; set; }
        public string gdua_dept_no { get; set; }
        public string glac_name { get; set; }
        public string glacnameacno { get; set; }
        public string ptyref_date { get; set; }
        public string gldc_comp_code { get; set; }
        public string gldc_year { get; set; }
        public string gldc_next_srl_no { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gldd_doc_narr1 { get; set; }
        public string gldd_doc_remark { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_date { get; set; }
        public string authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_post_no { get; set; }
        public string gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string paidfr { get; set; }
        public string cheqNo { get; set; }
        public string gltd_reference_final_doc_no { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_paid_from { get; set; }
        public string gltd_cheque_No { get; set; }
        public string gltd_doc_narration { get; set; }
        public string gltd_doc_remark { get; set; }
        public string gltd_final_doc_no { get; set; }
    }
    #region Ganesh (PDC Payment)
    #region (PDC Payment)
    public class Finn212
    {
        public string pb_comp_code { get; set; }
        public string pb_sl_ldgr_code { get; set; }
        public string pb_sl_acno { get; set; }
        public string pb_dept_no { get; set; }
        public string pb_doc_type { get; set; }
        public string pb_our_doc_no { get; set; }
        public decimal pb_rec_serial { get; set; }
        public string pb_bank_from { get; set; }
        public string pb_bank_slno { get; set; }
        public string pb_cheque_no { get; set; }
        public string pb_calendar { get; set; }
        public string pb_due_date { get; set; }
        public decimal pb_amount { get; set; }
        public decimal pb_pstng_no { get; set; }
        public string pb_narrative { get; set; }
        public string pb_bank_to { get; set; }
        public string pb_discd { get; set; }
        public decimal pb_ret_times { get; set; }
        public string pb_ref_no { get; set; }
        public string pb_resub_ref { get; set; }
        public string pb_cur_date { get; set; }
        public string pb_amd_flag { get; set; }
        public string fins_comp_code { get; set; }
        public string fins_appl_parameter { get; set; }
        public string fins_appl_form_field_value1 { get; set; }
        public string fins_appl_form_field { get; set; }
        public string fins_appl_form_field_value2 { get; set; }
        public string acc_code { get; set; }
        public string dept_code { get; set; }
        public bool pb_select { get; set; }

        public string ps_approv_date { get; set; }
        public string pb_ldgr_code_name { get; set; }
        public string pb_sl_acno_name { get; set; }
        public string pb_bank_code_name { get; set; }
        public string slac_ctrl_acno { get; set; }


        public string gdn_doc_narration { get; set; }

        public string codp_dept_name { get; set; }

        public string codp_dept_no { get; set; }


        public string gdn_doc_narration_code { get; set; }

        public string gdn_comp_code { get; set; }

        public string gdn_doc_code { get; set; }

        public bool gdn_doc_narration_status { get; set; }

        public string sims_municipality_name_en { get; set; }
    }


    #endregion
    #endregion

    public class Fin141
    {
        public string opr { get; set; }
        public string opr_upd { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string Full_name { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_bank_name { get; set; }
        public string Full_Bank_Name { get; set; }
        public string em_number { get; set; }
        public string Employee_Full_Name { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_doc_amount { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_amount { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string em_dept_code { get; set; }
        public string Empname { get; set; }
        public string data1 { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string codp_dept_name { get; set; }
        public string glsg_parent_group { get; set; }
        public string gldd_fc_rate { get; set; }
        public string gldd_line_no { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_comp_code { get; set; }
        public string doc_code { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_dept_code { get; set; }
        public string coad_dept_no { get; set; }
        public string fins_party_ref_date { get; set; }
        public string fins_past_date { get; set; }
        public string coad_pty_short_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string glma_acno { get; set; }
        public string gdua_dept_no { get; set; }
        public string glac_name { get; set; }
        public string glacnameacno { get; set; }
        public string ptyref_date { get; set; }
        public string gldc_comp_code { get; set; }
        public string gldc_year { get; set; }
        public string gldc_next_srl_no { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gldd_doc_narr1 { get; set; }
        public string gldd_doc_remark { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_date { get; set; }
        public string authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_post_no { get; set; }
        public string gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string paidfr { get; set; }
        public string cheqNo { get; set; }
        public string gltd_reference_final_doc_no { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_paid_from { get; set; }
        public string gltd_cheque_No { get; set; }
        public string gltd_doc_narration { get; set; }
        public string gltd_doc_remark { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gldc_doc_type { get; set; }
        public string master_acno { get; set; }
        public string master_acno_name { get; set; }
        public string master_ac_cdname { get; set; }
        public string gldu_verify_user { get; set; }
        public string gldu_authorize_user { get; set; }
        public string gltd_paid_to { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal sltr_yob_amt { get; set; }
        public string gldd_dept_name { get; set; }
        public string gdua_acct_code { get; set; }
        public string next_prov_no { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string finance_year { get; set; }

        public string invoice { get; set; }

        public string gldd_cost_center_code { get; set; }

        public string coce_cost_centre_name1 { get; set; }

        public string payment { get; set; }

        public string pending_voucher { get; set; }

        public string pending_voucher_amount { get; set; }




        public string slac_cntrl_class_desc { get; set; }

        public string slac_comp_code { get; set; }

        public string slac_ldgr_code { get; set; }

        public string slac_year { get; set; }

        public string supplier_trn_number { get; set; }

        public string gltd_cheque_no { get; set; }
    }
    public class Fin145
    {
        public string opr { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string Full_name { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_bank_name { get; set; }
        public string Full_Bank_Name { get; set; }
        public string em_number { get; set; }
        public string Employee_Full_Name { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_doc_amount { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_amount { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string em_dept_code { get; set; }
        public string Empname { get; set; }
        public string data1 { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string codp_dept_name { get; set; }
        public string glsg_parent_group { get; set; }
        public string gldd_fc_rate { get; set; }
        public string gldd_line_no { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_comp_code { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_dept_code { get; set; }
        public string coad_dept_no { get; set; }
        public string fins_party_ref_date { get; set; }
        public string fins_past_date { get; set; }
        public string coad_pty_short_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string glma_acno { get; set; }
        public string gdua_dept_no { get; set; }
        public string glac_name { get; set; }
        public string glacnameacno { get; set; }
        public string ptyref_date { get; set; }
        public string gldc_comp_code { get; set; }
        public string gldc_year { get; set; }
        public string gldc_next_srl_no { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_cheque_No { get; set; }
        public string gltd_paid_from { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_doc_remark { get; set; }
        public string gltd_doc_narration { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_doc_date { get; set; }
    }



    public class Fin150
    {
        public string coad_dept_no { get; set; }
        public string coad_pty_full_name { get; set; }
        public string coad_pty_short_name { get; set; }
        public string codp_dept_name { get; set; }
        public string comn_user_appl_fins_comp_status { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_name { get; set; }
        public string gldc_doc_type { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_comp_code { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldu_authorize { get; set; }
        public string coce_cost_centre_name { get; set; }
        public string gldd_cost_center_code { get; set; }
        public string gldu_prepare { get; set; }
        public string gldu_verify { get; set; }
        public string gltd_acct_code { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_ledger_code { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_remarks { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string revert_doc_no { get; set; }

        public string gltd_verify_date { get; set; }

        public string gltd_authorize_date { get; set; }

        public string gldd_acct_name { get; set; }

        public string gldc_doc_short_name { get; set; }

        public string gltd_paid_to { get; set; }

        public string gltd_cheque_no { get; set; }

        public string gltd_doc_type { get; set; }

        public string gltd_doc_no { get; set; }

        public string gltd_doc_line_no { get; set; }

        public string gltd_doc_type_name { get; set; }

        public string gldd_line_no { get; set; }

        public string gltd_final_doc_no { get; set; }

        public string gldd_doc_narr_old { get; set; }

        public string sims_financial_year_start_date { get; set; }

        public string sims_financial_year_end_date { get; set; }

        public string gldc_doc_code_type { get; set; }

        public string gldc_seq_prefix { get; set; }

        public string fins_appl_code { get; set; }

        public string fins_appl_form_field_value1 { get; set; }

        public string fins_appl_parameter { get; set; }

        public string inv_comp_code { get; set; }

        public string vat_desc { get; set; }

        public string vat_from_date { get; set; }

        public string vat_per_value { get; set; }

        public string vat_included_in_price { get; set; }

        public string vat_to_date { get; set; }

        public string vat_value_type { get; set; }

        public string vat_code { get; set; }

        public bool vat_status { get; set; }

        public string vat_invoice_acno { get; set; }

        public string vat_refund_acno { get; set; }
    }

    public class Fin211
    {
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_post_no { get; set; }
        public string gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }



        ////// fins_temp_doc_details
        public string gldd_acct_code_gl_master { get; set; }
        public string glma_acct_name_gl_master { get; set; }
        public string gldd_dept_code_gl_master { get; set; }
        public string gldd_dept_name_gl_master { get; set; }


        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        //public double gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_fc_name { get; set; }
        ////fins_currency_master
        public string excg_curcy_desc { get; set; }

        ////////slgr_master
        public string slma_ldgrctl_code { get; set; }


        ////////////////fins_financial_documents
        public decimal gldc_next_prv_no { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }


        public string gltd_reference_doc_code { get; set; }

        public string gltd_reference_final_doc_no { get; set; }
        public string gltd_docdate { get; set; }
        public string gltd_postdate { get; set; }
        public string gltd_verifydate { get; set; }
    }


    //    public class Finn139
    //    {
    //        public string acc_code { get; set; }
    //        public string company_name { get; set; }
    //        public string dept_code { get; set; }
    //        public string fins_appl_form_field_value1 { get; set; }
    //        public string fins_appl_parameter { get; set; }
    //        public string gldd_acct_code { get; set; }
    //        public string gldd_acct_name { get; set; }
    //        public string gldd_dept_code { get; set; }
    //        public string gldd_dept_name { get; set; }
    //        public decimal gldd_doc_amount { get; set; }
    //        public decimal gldd_doc_amount_debit { get; set; }
    //        public string gldd_ledger_code { get; set; }
    //        public string gldd_party_ref_no { get; set; }
    //        public string glma_acct_code { get; set; }
    //        public string gltd_cur_status { get; set; }
    //        public string pb_gl_acno { get; set; }
    //        public string slma_ldgrctl_code { get; set; }
    //        public string slma_pty_bank_id { get; set; }
    //        public string slma_pty_bank_name { get; set; }
    //        public string sltr_pty_bank_id { get; set; }
    //        public decimal sltr_yob_amt { get; set; }
    //    }

    #region Finn141
    public class Finn141
    {
        public string sllc_ldgr_code;
        public string sllc_ldgr_name;
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public string gldd_cost_center_code { get; set; }
        public int gltd_post_no { get; set; }
        public int gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }
        public string master_acno { get; set; }
        public string master_ac_cdname { get; set; }
        public string master_acno_name { get; set; }
        public string gltd_vat_enable { get; set; }
        public string gldd_include_vat { get; set; }
        public string gldd_vat_amt { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
      

        ////// fins_temp_doc_details
        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_ledger_name { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_name { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_cheque_no { get; set; }
        public string gldd_cheque_due_date { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal glbt_pstng_no { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public string coad_tel_no { get; set; }

        public string coad_po_box { get; set; }

        public string coad_line_1 { get; set; }

        public string coad_line_2 { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public string glma_acct_code { get; set; }

        public string pc_bank_code { get; set; }
        public string old_gldd_acct_code { get; set; }

        public string slma_pty_bank_id { get; set; }
        public string slma_pty_bank_name { get; set; }
        public string pb_gl_acno { get; set; }
        public string fins_appl_parameter { get; set; }
        public string fins_appl_form_field_value2 { get; set; }

        public bool gltd_authorize { get; set; }

        public bool gltd_verify { get; set; }

        public bool gltd_revert { get; set; }

        public string gldc_doc_short_name { get; set; }

        public string glpc_expense_acno { get; set; }

        public string glpc_petty_cash_acno { get; set; }

        public string glpc_petty_cash_acno_name { get; set; }

        public string glpc_expense_acno_name { get; set; }

        public string coce_cost_centre_name { get; set; }

        public List<Finn158> inner_data = new List<Finn158>();


        public string glpc_amount { get; set; }

        public string gltd_reference_doc_code { get; set; }

        public string gltd_reference_final_doc_no { get; set; }

        public string user_name { get; set; }

        public string finance_year { get; set; }

        public string old_gldd_acccount_code { get; set; }

        public string gldd_fc_amount { get; set; }

        public string gldd_bank_code { get; set; }

        public string pc_sup_item_code { get; set; }

        public string prod_code { get; set; }
        public string pc_gldd_acct_code { get; set; }

        public string remarks { get; set; }
    }


    public class Finn158
    {
        public string glpcd_receipt_no { get; set; }
        public string glpcd_receipt_date { get; set; }
        public string glpcd_receipt_narr { get; set; }
        public string glpcd_amount { get; set; }


    }
    #endregion

    #region Finn153
    public class Finn153
    {
        public string sllc_ldgr_code;
        public string sllc_ldgr_name;
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public int gltd_post_no { get; set; }
        public int gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }
        public string master_acno { get; set; }
        public string master_ac_cdname { get; set; }
        public string master_acno_name { get; set; }


        ////// fins_temp_doc_details
        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_ledger_name { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_name { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_cheque_no { get; set; }
        public string gldd_cheque_due_date { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal glbt_pstng_no { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public string coad_tel_no { get; set; }

        public string coad_po_box { get; set; }

        public string coad_line_1 { get; set; }

        public string coad_line_2 { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public string glma_acct_code { get; set; }

        public string pc_bank_code { get; set; }
    }
    #endregion

    #region Finn145
    public class Finn145
    {
        public string sllc_ldgr_code;
        public string sllc_ldgr_name;
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public int gltd_post_no { get; set; }
        public int gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }
        public string master_acno { get; set; }
        public string master_ac_cdname { get; set; }
        public string master_acno_name { get; set; }


        ////// fins_temp_doc_details
        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_ledger_name { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_name { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_cheque_no { get; set; }
        public string gldd_cheque_due_date { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal glbt_pstng_no { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public string coad_tel_no { get; set; }

        public string coad_po_box { get; set; }

        public string coad_line_1 { get; set; }

        public string coad_line_2 { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public string glma_acct_code { get; set; }

        public string pc_bank_code { get; set; }
    }
    #endregion


    #region Finn152
    public class Finn152
    {
        public string sllc_ldgr_code;
        public string sllc_ldgr_name;
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public int gltd_post_no { get; set; }
        public int gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }
        public string master_acno { get; set; }
        public string master_ac_cdname { get; set; }
        public string master_acno_name { get; set; }


        ////// fins_temp_doc_details
        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_ledger_name { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_name { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_cheque_no { get; set; }
        public string gldd_cheque_due_date { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal glbt_pstng_no { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public string coad_tel_no { get; set; }

        public string coad_po_box { get; set; }

        public string coad_line_1 { get; set; }

        public string coad_line_2 { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public string glma_acct_code { get; set; }

        public string pc_bank_code { get; set; }
    }
    #endregion

    #region Finn225
    public class Finn225
    {
        public string sims_cur_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_student_name { get; set; }
        public string sims_enroll_number { get; set; }
        public DateTime sims_from_date { get; set; }
        public DateTime sims_to_date { get; set; }
        public string sims_fee_code_description { get; set; }
        public string expected_fee { get; set; }
        public string fee_paid { get; set; }
        public string dd_fee_payment_mode { get; set; }
        public string discount { get; set; }
        public string total { get; set; }
        public string fee_status_desc { get; set; }
        public string doc_date { get; set; }
        public string sims_enroll_numberR { get; set; }

        public string sims_fee_amount { get; set; }

        public string fins_cr_no { get; set; }

        public string fins_dr_acno { get; set; }

        public string sims_fee_code { get; set; }

        public string total_fee_amount { get; set; }

    }
    #endregion

    public class Fin227
    {
        public string codp_dept_no { get; set; }
        public string codp_dept_name { get; set; }
        public string glpc_number { get; set; }
        public string glpc_comp_code { get; set; }
        public string glpc_year { get; set; }
        public string glpc_dept_no { get; set; }
        public string glpc_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string glpc_max_limit { get; set; }
        public string glpc_min_limit { get; set; }
        public string glpc_max_limit_per_transaction { get; set; }
        public string glpc_curr_balance { get; set; }
        public string glpc_creation_date { get; set; }
        public string glpc_creation_user { get; set; }
        public string glpc_authorize_date { get; set; }
        public string glpc_authorize_user { get; set; }
        public string glpc_cur_date { get; set; }
        public bool glpc_cost_center_flag { get; set; }
        public bool glpc_status { get; set; }
        public string opr { get; set; }

        public string glma_acct_full_name { get; set; }
        public string glma_acct_code { get; set; }
        public string glpca_prepare_user { get; set; }

    }

    public class Fin152
    {
        public string glco_cost_centre_code { get; set; }
        public string coce_cost_centre_name { get; set; }

        public string opr { get; set; }
        public string opr_upd { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string Full_name { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_bank_name { get; set; }
        public string Full_Bank_Name { get; set; }
        public string em_number { get; set; }
        public string Employee_Full_Name { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_doc_amount { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_amount { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string em_dept_code { get; set; }
        public string Empname { get; set; }
        public string data1 { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string codp_dept_name { get; set; }
        public string glsg_parent_group { get; set; }
        public string gldd_fc_rate { get; set; }
        public string gldd_line_no { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_comp_code { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_dept_code { get; set; }
        public string coad_dept_no { get; set; }
        public string fins_party_ref_date { get; set; }
        public string fins_past_date { get; set; }
        public string coad_pty_short_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string glma_acno { get; set; }
        public string gdua_dept_no { get; set; }
        public string glac_name { get; set; }
        public string glacnameacno { get; set; }
        public string ptyref_date { get; set; }
        public string gldc_comp_code { get; set; }
        public string gldc_year { get; set; }
        public string gldc_next_srl_no { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gldd_doc_narr1 { get; set; }
        public string gldd_doc_remark { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_date { get; set; }
        public string authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_post_no { get; set; }
        public string gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string paidfr { get; set; }
        public string cheqNo { get; set; }
        public string gltd_reference_final_doc_no { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_paid_from { get; set; }
        public string gltd_cheque_No { get; set; }
        public string gltd_doc_narration { get; set; }
        public string gltd_doc_remark { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gldc_doc_type { get; set; }
        public string master_acno { get; set; }
        public string master_acno_name { get; set; }
        public string master_ac_cdname { get; set; }
        public string gldu_verify_user { get; set; }
        public string gldu_authorize_user { get; set; }
        public string gltd_paid_to { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal sltr_yob_amt { get; set; }
        public string gldd_dept_name { get; set; }
        public string gdua_acct_code { get; set; }
        public string next_prov_no { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
    }

    public class FinVoucher
    {

        public string doc_code { get; set; }

        public string final_doc_no { get; set; }

        public string doc_date { get; set; }

        public string ledger_doc_narr { get; set; }

        public string acct_code { get; set; }

        public string acct_name { get; set; }

        public string doc_amount { get; set; }

        public string username { get; set; }

        public string company_code { get; set; }

        public string financial_year { get; set; }

        public string glpr_dept_no { get; set; }
        public string glpr_dept_name { get; set; }
        public string glpr_acct_code { get; set; }
        public string glpr_bud_amt { get; set; }
        public string glpr_acct_name { get; set; }

    }

    public class Fin201
    {
        
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_comp_code { get; set; }
        public string gldd_dept_code { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_final_doc_no { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_line_no { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_post_date { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_remarks { get; set; }
        public string opr { get; set; }
        public string opr_upd { get; set; }


        public string std_fee_enroll_number { get; set; }

        public string std_fee_academic_year { get; set; }

        public string std_fee_section_name { get; set; }

        public string std_fee_parent_name { get; set; }

        public string std_TotalPaid { get; set; }

        public string std_fee_cur_code { get; set; }

        public string std_fee_grade_code { get; set; }

        public string std_fee_Class { get; set; }

        public string std_fee_student_name { get; set; }

        public string std_fee_section_code { get; set; }

        public string std_fee_grade_name { get; set; }

        public string std_fee_parent_id { get; set; }

        public string std_fee_mother_name { get; set; }

        public string std_fee_amount { get; set; }

        public string std_BalanceFee { get; set; }

        public string std_fee_last_payment_date { get; set; }

        public string student_attendance_last_date { get; set; }

        public bool std_fee_status { get; set; }

        public string std_fee_category_name { get; set; }

        public string parent_email_id { get; set; }

        public string fees_paid_sms_btn { get; set; }

        public string sims_student_academic_status { get; set; }

        public string parent_mobile_no { get; set; }

        public string fees_paid_email_btn { get; set; }

        public string student_attendance_per { get; set; }

        public string sims_student_academic_status_code { get; set; }

        public bool is_parent_employee { get; set; }

        public bool is_adv_fee_defined { get; set; }

        public bool is_adv_section_defined { get; set; }

        public string adv_fee_year { get; set; }

        public string is_adv_grade { get; set; }

        public string is_adv_section { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_fee_code_description { get; set; }

        public object username { get; set; }

        public object sims_fee_grade_code { get; set; }

        public object sims_fee_cur_code { get; set; }

        public object sims_fee_section_code { get; set; }

        public object sims_fee_academic_year { get; set; }

        public object remark { get; set; }

        public object receipt_date { get; set; }
    }

    public class Finnyearshifting
    {
        public string total_amount { get; set; }
        public string comn_rollover_code { get; set; }
        public string comn_rollover_desc_en { get; set; }
        public string comn_rollover_icon_color { get; set; }
        public string comn_rollover_proc_name_opr { get; set; }
        public string comn_rollover_process_remark { get; set; }
        public string comn_rollover_process_remark_status { get; set; }
            public string sims_enroll_number  {get;set;}
            public string sims_cur_code       {get;set;}
            public string sims_academic_year  {get;set;}
        public string student_name { get; set; }
        public List<fee_details> fee_det = new List<fee_details>();
          public string user_name { get; set; }
        public string sims_fee_code { get; set; }
       
    }

    public class fee_details
    {
        public string posting_fee_code { get; set; }
            public string fee_amount { get; set; }
            public string sims_fee_code { get;set;}
            public string sims_fee_code_description { get;set;}
           public string text_colour { get; set; }
        public string fee_total_ammount { get; set; }
    }

    public class Finn218
    {
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string deptno_fullname { get; set; }
        public string emp_code { get; set; }
        public string em_email { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_login_code { get; set; }
        public string em_mobile { get; set; }
        public string em_number { get; set; }
        public string em_status { get; set; }
        public string fal_from_date { get; set; }
        public bool fal_status { get; set; }
        public string fal_upto_date { get; set; }
        public string fal_user_code { get; set; }
        public string fullname { get; set; }
        public string gal_asst_type { get; set; }
        public string gal_comp_code { get; set; }
        public string gal_type_desc { get; set; }
        public string gam_desc_1 { get; set; }
        public string gam_item_no { get; set; }
        public string gam_location { get; set; }
        public bool gam_status { get; set; }

        public string loc_code { get; set; }

        public string loc_name { get; set; }
    }


    public class FCBD01
    {
        public string opr { get; set; }
        public string comp_bank_code { get; set; }
        public string comp_bank_name { get; set; }
        public string comp_bank_branch_name { get; set; }
        public string comp_bank_address { get; set; }
        public string comp_bank_account_no { get; set; }
        public string comp_bank_iban_no { get; set; }
        public string comp_route_code { get; set; }
        public string comp_ifsc_code { get; set; }
        public string comp_swift_code { get; set; }
        public bool comp_bank_status { get; set; }
        public string comp_code { get; set; }
    }


    public class Fin221
    {
        public string opr { get; set; }
        public string coce_comp_code { get; set; }
        public string coce_cost_center_name { get; set; }
        public string coce_cost_centre_code { get; set; }
        public string coce_cost_centre_type { get; set; }
        public bool coce_status { get; set; }
        public string coce_year { get; set; }
        public string coce_cost_centre_person_responsible { get; set; }
        public string coce_cost_centre_currency { get; set; }
        public string coce_cost_centre_address { get; set; }
        public string coce_cost_centre_name { get; set; }
        public string excg_comp_code { get; set; }
        public string excg_curcy_code { get; set; }
        public string excg_curcy_desc { get; set; }
        public string currency_desc { get; set; }
        public string comp_code { get; set; }
        public string finance_year { get; set; }
    }

    public class Fin220
    {
        public string opr { get; set; }
        public string coce_cost_centre_code { get; set; }
        public string coce_cost_centre_name { get; set; }
        public string codp_comp_code { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string codp_dept_type { get; set; }
        public string glac_acct_code { get; set; }
        public string glac_name { get; set; }
        public string glac_type { get; set; }
        public string glco_acct_code { get; set; }
        public string glco_comp_code { get; set; }
        public string glco_cost_centre_code { get; set; }
        public string glco_dept_no { get; set; }
        public bool glco_status { get; set; }
        public string glco_year { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string glma_dept_no { get; set; }
        public string comp_code { get; set; }
        public string finance_year { get; set; }
        public string codp_dept_no_old1 { get; set; }
        public string glac_acct_code_old1 { get; set; }
        public string glco_op_bal_amt { get; set; }
        public string money { get; set; }
    }

    public class AVQA01
    {
        public string comp_code { get; set; }
        public string glac_name { get; set; }
        public string gltr_dept_no { get; set; }
        public string codp_dept_name { get; set; }
        public string gltr_acct_code { get; set; }
       
        public string account_Type { get; set; }
        public string debit_amt { get; set; }
        public string sims_financial_year_description { get; set; }
        public string sims_financial_year_status { get; set; }
        public string sims_financial_year { get; set; }
        public string credit_amt { get; set; }
        public string balance { get; set; }
        public string opr { get; set; }

        public string sims_financial_year_start_date { get; set; }
        public string sims_financial_year_end_date { get; set; }
        public string gltr_year { get; set; }

        public string comp_name { get; set; }

        public string glac_type { get; set; }

        public string glac_type_code { get; set; }

        public string glac_sched_code { get; set; }

        public string glac_sched_code1 { get; set; }

        public string glac_sched_desc { get; set; }

        public string glac_year { get; set; }

        public string codp_dept_no { get; set; }
        
    }


}