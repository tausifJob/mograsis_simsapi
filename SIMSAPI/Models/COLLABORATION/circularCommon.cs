﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COLLABORATION
{
         public class circularByParent
         {
             public string comn_appl_parameter { get; set; }
             public string comn_appl_form_field_value1 { get; set; }

             public string sims_circular_number	{ get; set; }
             public string sims_circular_title	{ get; set; }
             public string sims_circular_short_desc	{ get; set; }
             public string sims_circular_user_group_code	{ get; set; }
             public string sims_academic_year	{ get; set; }
             public string parent_name { get; set; }
             public string sims_circular_user_name	{ get; set; }
             public string circular_status { get; set; }


             ////   circular user status 
             public string comn_user_group_code { get; set; }
             public string comn_user_group_name { get; set; }

             public string sims_cur_code { get; set; }
             public string sims_circular_date { get; set; }
             public string sims_circular_publish_date { get; set; }
             public string sims_circular_expiry_date { get; set; }
             public string sims_circular_desc { get; set; }
             public string sims_circular_user_alias_name { get; set; }
             public string sims_circular_status { get; set; }
             public string sims_circular_status_desc { get; set; }

             //communication report

            public string sims_comm_subject { get; set; }
            public string sender_id { get; set; }
            public string sender_name { get; set; }
            public string rece_id { get; set; }
            public string receiver_name { get; set; }
            public string sims_comm_message { get; set; }
            public string sims_comm_tran_date { get; set; }


             //email status report

             
             public string sender { get; set; }
             public string date1	{ get; set; }
             public string subject1	{ get; set; }
             public string status1	{ get; set; }
             public string status	{ get; set; }
             public string receiver	{ get; set; }
             public string parent_id	{ get; set; }
             public string sims_enroll_number { get; set; }

             public string sims_appl_parameter { get; set; }
             public string sims_appl_form_field_value1 { get; set; }


             //News Report By User

             public string group_name { get; set; }
             public string sims_news_number { get; set; }
             public string sims_news_title { get; set; }
             public string sims_news_desc { get; set; }
             public string sims_news_date { get; set; }
             public string sims_news_expiry_date { get; set; }

             //Student House List Report  for Various Age Group
             public string student_name { get; set; }
             public string sims_grade_name_en { get; set; }
             public string sims_section_name_en { get; set; }
             public string sims_student_dob { get; set; }
             public string house_name { get; set; }
             public string age_calculate { get; set; }

             //enrolled student in transport

             public string grade_name { get; set; }
             public string section_name { get; set; }
             public string sims_student_enroll_number { get; set; }
             public string address_details { get; set; }
             public string sims_parent_father_mobile { get; set; }
             public string sims_parent_mother_mobile { get; set; }
             public string bus_no { get; set; }
         }

}