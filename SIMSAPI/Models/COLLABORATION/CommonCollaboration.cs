﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COLLABORATION
{
    public class CommonCollaboration
    {

        public string sims_circular_type_desc { get; set; }
        public string sims_circular_category_desc { get; set; }
        public string sims_circular_publish_date { get; set; }
        public string sims_circular_expiry_date { get; set; }
        public string sims_circular_created_by { get; set; }
        public string sims_circular_number { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_circular_date { get; set; }
        public string sims_circular_title { get; set; }
        public string sims_circular_short_desc { get; set; }
        public string sims_circular_desc { get; set; }
        public string sims_circular_file_path1 { get; set; }
        public string sims_circular_file_path2 { get; set; }
        public string sims_circular_file_path3 { get; set; }
        public string sims_circular_type { get; set; }
        public string sims_circular_category { get; set; }
        public string sims_circular_created_user_code { get; set; }
        public string sims_circular_display_order { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string publish_month { get; set; }
         public string circular_status { get; set; }
         public string circular_active_for { get; set; }

        //circular by user


         public string group_name { get; set; }
         public string sims_circular_user_name { get; set; }
         public string name { get; set; }
         public string sims_circular_remark { get; set; }
         public string comn_user_group_code { get; set; }
         public string comn_user_group_name { get; set; }
         public string comn_appl_form_field_value1 { get; set; }
         public string comn_appl_parameter { get; set; }
         public string circular_ack { get; set; }
         public string sims_circular_status { get; set; }
         public string sims_circular_user_group_code { get; set; }
















         public string sims_grade_name_en { get; set; }

         public string sims_section_name_en { get; set; }

         public string student_name { get; set; }

         public string Initial_sender_name { get; set; }

         public string Initial_receiver_name { get; set; }

         public string sender_name { get; set; }

         public string sims_grade_code { get; set; }

         public string receiver_name { get; set; }

         public string initial_send { get; set; }

         public string sims_section_code { get; set; }

         public string initial_receive { get; set; }

         public string sims_comm_number { get; set; }

         public string sims_comm_tran_number { get; set; }

         public string sims_comm_date { get; set; }

         public string sims_comm_tran_date { get; set; }

         public string sims_comm_category { get; set; }

         public string sims_comm_user_code { get; set; }

         public string sims_subject_id { get; set; }

         public string sims_comm_subject { get; set; }

         public string sender_id { get; set; }

         public string rece_id { get; set; }

         public string sims_comm_message { get; set; }

         public string status { get; set; }
    }
}
