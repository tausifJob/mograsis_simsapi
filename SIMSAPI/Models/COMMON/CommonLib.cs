﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.CommonLib
{
    public class LIBR09
    {

        //Available Book Stock
        public string sims_library_item_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_desc { get; set; }
        public string sims_library_category_name { get; set; }
        public string sims_library_subcategory_name { get; set; }
        public string number_of_copies { get; set; }
        public string total_return_qty { get; set; }
        public string sims_library_category_code { get; set; }
        public string sims_library_category_description { get; set; }
        public string sims_library_category_item_issue { get; set; }
        public string sims_library_subcategory_code { get; set; }
        public string total { get; set; }

        //Library Membership By Grade
        public string sims_library_user_number { get; set; }
        public string sims_login_code { get; set; }
        public string sims_user_name { get; set; }
        public string comn_user_group_name { get; set; }
        public string sims_library_joining_date { get; set; }
        public string sims_library_expiry_date { get; set; }
        public string sims_library_renewed_date { get; set; }
                //privilage
        public string sims_library_membership_type { get; set; }
        public string sims_library_privilege_name { get; set; }
                //membership
         public string code1 { get; set; }
         public string descri { get; set; }

    //Library Item
        public string lib_location { get; set; }
        public string sims_library_catalogue_name { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_name_of_publisher { get; set; }
        public string sims_library_item_author1 { get; set; }

        public string sims_library_location_code { get; set; }
        public string sims_library_location_name { get; set; }
        public string sims_library_location_address1 { get; set; }
        public string sims_library_location_address2 { get; set; }
        public string sims_library_location_contact_person { get; set; }
        public string sims_library_location_phone { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
          public string sims_library_item_status { get; set; }
          public string count_value { get; set; }

        //library transaction report
        public string trans_date { get; set; }
        public string ret_date { get; set; }
        public string sims_library_transaction_user_code { get; set; }
        public string member_name { get; set; }
        public string transaction_status { get; set; }
        public string exp_ret_date { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string grade_name { get; set; }
        public string section_name { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_fine_description { get; set; }
        public string sims_fine_expected_amount { get; set; }
        public string sims_library_user_group_type { get; set; }
        public string sims_library_item_issue_date { get; set; }
        public string em_designation { get; set; }
        
        public string department { get; set; }



























































    }
}
      


        








       
       
      

        		
        


        			



        








        
        






      
           


								
       
        	
      
        	
        	 	
        	 	
        	 	
        



        	 	


        	
	
	
	


        


        


	
	
	
	
	
	
	
	
	
	
	



   
