﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FleetCommonReport
{
    public class SBVQ01
    {
        public string sims_transport_route_student_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_transport_academic_year { get; set; }
        public string sims_transport_enroll_number { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_route_direction_name { get; set; }
        public string sims_transport_pickup_stop_code { get; set; }
        public string sims_transport_pickup_point { get; set; }
        public string sims_transport_drop_stop_code { get; set; }
        public string sims_transport_drop_point { get; set; }
        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_vehicle_registration_number { get; set; }
        public string sims_transport_vehicle_name_plate { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_transport_route_student_status { get; set; }
        public string sims_transport_route_student_stop_lat { get; set; }
        public string sims_transport_route_student_stop_long { get; set; }
        public string sims_transport_share_location { get; set; }
        public string pick_up_point { get; set; }
        public string sims_transport_route_stop_expected_time { get; set; }
        public string drop_point { get; set; }
        public string grade_name { get; set; }
        public string section_name { get; set; }


    }
    public class TSRQ01
    {
        public string sims_transport_maintenance_doc_no { get; set; }
        public string sims_transport_maintenance_vehicle_code { get; set; }
        public string sims_transport_maintenance_date { get; set; }
        public string sims_transport_maintenance_service_station { get; set; }
        public string sims_transport_maintenance_purpose { get; set; }
        public string sims_transport_maintenance_total_amount { get; set; }
        public string sims_transport_maintenance_approve_by { get; set; }
        public string sims_transport_maintenance_driver_code { get; set; }
        public string sims_transport_maintenance_next_date { get; set; }
        public string sims_transport_maintenance_status { get; set; }
        public string sims_transport_maintenance_Quantity { get; set; }

        public string sims_transport_maintenance_amount { get; set; }
        public string sims_transport_maintenance_desc { get; set; }

        public string sims_transport_vehicle_registration_number { get; set; }
        public string sims_transport_vehicle_name { get; set; }
        public string sims_transport_vehicle_code { get; set; }

        public string sims_driver_code { get; set; }
        public string sims_driver_name { get; set; }

      





    }


    public class FTLQR1
    {

        public string sims_teacher_code { get; set; }
        public string sims_employee_code { get; set; }
        public string teacher_name { get; set; }  
        public string qualification { get; set; }
        public string sims_bell_day_code { get; set; }
        public string sims_bell_slot_group { get; set; }
        public string sims_bell_slot_group_desc { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_desc { get; set; }
        public string sims_bell_day_desc { get; set; }

        //supervisor list

        public string em_login_code { get; set; }                                
        public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_last_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }


        //lecture time conflict report

        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_slot_code { get; set; }
        public string grade { get; set; }
        public string section { get; set; }
        public string sub { get; set; }
        public string bell { get; set; }
        public string day_desc { get; set; }
        public string sims_bell_start_time { get; set; }


    }
}