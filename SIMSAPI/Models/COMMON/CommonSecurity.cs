﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COMMON
{
    public class CommonSecurity

    {
        //role application report
        public string comn_appl_code { get; set; }
        public string comn_role_name { get; set; }
        public string comn_role_code { get; set; }
        public string comn_appl_name_en { get; set; }

        // role assignment

        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string comn_user_appl_code { get; set; }
        public string appName { get; set; }
        public string comn_user_code { get; set; }
        public string comn_user_name { get; set; }
        public string comn_appl_user_name { get; set; }
        public string comn_mod_name_en { get; set; }
        public string appl_assigned_user_name { get; set; }
        public string appl_assigned_user_id { get; set; }
        public string comn_user_appl_assigned_by_user_code { get; set; }
        public string comn_user_appl_assigned_date { get; set; }

        //user status report

        public string comn_user_emp_code { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string name { get; set; }

        public string sims_appl_parameter { get; set; }

        //user login details
           public string comn_user_alias { get; set; }
        public string comn_user_last_login { get; set; }
        public string group_name { get; set; }
       public string comn_appl_parameter { get; set; }
       public string comn_appl_form_field_value1 { get; set; }

        // Common User Application
          
         public string comn_appl_type { get; set; }
         public string user_count { get; set; }
        public string comn_audit_start_time { get; set; }
        public string comn_audit_end_time { get; set; }
        public string application_type { get; set; }
        public string assign_by { get; set; }
        public string sims_appl_code { get; set; }
      public string sims_appl_name { get; set; }


      public string module_code { get; set; }
      public string app_type { get; set; }
      public string app_code { get; set; }
      public string from_date { get; set; }
      public string to_date { get; set; }
      public string search { get; set; }
     


        













       
    }
}