﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COMMON
{
    public class CommonLibrary
    {

        public string sims_library_item_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_item_desc { get; set; }
        public string transaction_status { get; set; }
        public string sims_login_code { get; set; }
        public string member_name { get; set; }
        public string sims_library_transaction_date { get; set; }
        public string sims_library_transaction_number { get; set; }

         
       //libaray transaction detail

        public string sims_appl_form_field_value1 { get; set; }
        public string sims_library_item_qty { get; set; }
        public string sims_library_item_actual_return_date { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_issue_date { get; set; }
        public string sims_library_subcategory_name { get; set; }
        public string sims_library_category_name { get; set; }
        public string sims_appl_parameter { get; set; }


        //library membership by user group

        public string sims_student_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_library_user_group_type { get; set; }
        public string comn_user_group_name{ get; set; }
        public string sims_library_user_number { get; set; }
        public string sims_library_membership_type { get; set; }
        public string sims_library_privilege_name { get; set; }
        public string sims_grade_code { get; set; }
        public string grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string section_name { get; set; }
        public string sims_library_joining_date { get; set; }
        public string sims_library_expiry_date { get; set; }
        public string sims_library_renewed_date { get; set; }
        public string sims_library_status { get; set; }
        public string sims_user_name { get; set; }
        public string comn_user_group_code { get; set; }
        public string sims_library_transaction_user_code { get; set; }
    }

}