﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FeesCommonReport
{
    public class SFSQ01
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_Section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string class_name { get; set; }
        public string family_id { get; set; }
        public string father_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string mother_name { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string code { get; set; }
        public string desc { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
    }
}