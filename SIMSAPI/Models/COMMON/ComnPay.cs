﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COMMON
{
    public class ComnPay
    {
        //salary increment report
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string dg_code { get; set; }
        public string dg_desc { get; set; }
        public string em_login_code { get; set; }
        public string emp_name { get; set; }
        public string sd_year_month { get; set; }
        public string cp_desc { get; set; }
        public string sd_amount { get; set; }
        public string inc_month { get; set; }
        public string pai_increment_type { get; set; }
        public string inc_amt { get; set; }


        //Air Fare Report
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string current_year { get; set; }
        public string paysheet_month { get; set; }
        public string paysheet_month_name { get; set; }
        public string afepd_em_number { get; set; }
        public string ename { get; set; }
        public string gr_desc { get; set; }
        public string ds_name { get; set; }
        public string month_en { get; set; }
        public string afepd_unit_rate { get; set; }    
        
    }
}