﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.HealthCommonClass
{
    public class StudRpt
    {
            public string sims_cur_code	{ get; set; }
            public string sims_academic_year	{ get; set; }
            public string sims_grade_code	{ get; set; }
            public string sims_section_code	{ get; set; }
            public string sims_grade_name_en	{ get; set; }
            public string sims_section_name_en	{ get; set; }
            public string sims_enrollment_number	{ get; set; }
            public string student_name	{ get; set; }
            public string sims_susceptibility_desc	{ get; set; }
            public string sims_complaint_desc	{ get; set; }
            public string sims_immunization_desc	{ get; set; }
            public string medicines { get; set; }
            public string sims_medical_visit_number { get; set; }

    }
    public class Camp101
    {

        public string sims_health_camp_name_en { get; set; }

        public string sims_health_package_code { get; set; }

        public string sims_health_cur_code { get; set; }

        public string sims_health_academic_year { get; set; }

        public string sims_health_camp_date { get; set; }

        public string sims_health_camp_status { get; set; }

        public string sims_health_package_name_en { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string em_login_code { get; set; }

        public string em_number { get; set; }

        public string em_last_name { get; set; }

        public string em_first_name { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string opr { get; set; }

        public string finance_year { get; set; }

        public string sims_health_camp_desc { get; set; }

        public string sims_health_camp_img1 { get; set; }

        public string sims_health_camp_img4 { get; set; }

        public string sims_health_camp_location { get; set; }

        public string sims_health_camp_img3 { get; set; }

        public string sims_health_camp_img2 { get; set; }

        public string sims_health_camp_price { get; set; }

        public string user { get; set; }

        public string sims_health_camp_name_ot { get; set; }

        public string sims_health_camp_start_date { get; set; }

        public string sims_health_camp_end_date { get; set; }

        public string sims_health_camp_registration_start_date { get; set; }

        public string sims_health_camp_registration_end_date { get; set; }

        public string sims_health_camp_created_by { get; set; }

        public object sims_health_camp_code { get; set; }

        public string sims_health_camp_employee_start_date { get; set; }

        public string sims_health_camp_employee_end_date { get; set; }

        public object sims_health_camp_employee_code { get; set; }

        public object sims_health_camp_user_created_by { get; set; }

        public string sims_health_camp_user_status { get; set; }
    }
}