﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.CommonFleet
{
    public class CommonFleetData{
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_transport_route_code { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_stop_code { get; set; }
        public string sims_transport_stop_name { get; set; }
        public string sims_transport_stop_landmark { get; set; }
        public string sims_transport_route_stop_expected_time { get; set; }
        public string sims_transport_route_stop_waiting_time { get; set; }
    



    }

    public class CommonFleet
    {
        // enrolled students in report

        public string grade_name { get; set; }
        public string section_name { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string student_name { get; set; }
        public string address_details { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string bus_no { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field_value1 { get; set; }


        //driver deyails
        public string sims_user_code { get; set; }
        public string sims_driver_name { get; set; }
        public string sims_driver_driving_license_number { get; set; }
        public string sims_driver_license_expiry_date { get; set; }
        public string sims_driver_license_vehicle_categoryname { get; set; }
        public string sims_transport_vehicle_name_plate { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_driver_type { get; set; }
        public string sims_driver_type_name { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string driver_status { get; set; }
        public string status_Desc { get; set; }

        //attendance report for students transport


        public string sims_enroll_number { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_attendance_day_attendance_code { get; set; }
        public string parent_no { get; set; }
        public string sims_attendance_short_desc { get; set; }
        public string sims_attendance_description { get; set; }
        public string sims_cur_code { get; set; }


        //students by bus

        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_vehicle_name_platebus { get; set; }
        public string sims_transport_route_direction_name { get; set; }
        public string cnt { get; set; }


        // Transport alert report

        public string comn_alert_user_code { get; set; }
        public string comn_alert_date { get; set; }
        public string comn_alert_message { get; set; }
        public string priority { get; set; }
        public string comn_alert_type_code { get; set; }
        public string user_name { get; set; }
        public string comn_alert_status { get; set; }
        

		//supriya
	
			
			
			
		


    }


}