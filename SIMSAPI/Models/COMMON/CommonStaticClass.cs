﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.Common
{
    public class CommonStaticClass
    {

        #region Comman Field...

        public static string Flag_ = "Y";
        public static string Current_Financial_Year = "C";
        public static string Back_Financial_Year = "B";
        public static string Cfyear = "Current Year";
        public static string Bfyear = "Back Year";
        public static string Fin060_comn_user_appl_code = "Fin150";
        public static string Fin060_fins_comp_status = "A";
        public static string Finn_year = "2014";

        #endregion

        #region Search User Control Uccw001
        public static string sims_appl_code_teacher = "Per099";
        public static string teacher_type = "Teacher Type";
        #endregion

        #region Finn016(Subledger Masters)
        public static string Finn016_fins_appl_code1 = "Fin016";
        public static string Finn016_fins_appl_code2 = "Uccw32";
        public static string Finn016_fins_appl_form_field_addr_type = "Address Type";
        public static string Finn016_fins_appl_form_field_party_class = "Party Class";
        public static string Finn016_fins_appl_form_field_party_type = "Party Type";
        #endregion

        public static string appName = "Com010";
        public static string simsapplformfield1 = "Setup";
     
        #region Sims036(Section Fee)
        public static string Sims036_sims_appl_code = "Sim036";
        public static string Sims036_sims_appl_form_field = "FeeFrequency";
        public static string Sims036_sims_appl_form_field_fee_code_type = "FeeCodeType";
        public static string Sims036_sims_appl_form_field_fee_frequency = "FeeFrequency";
        public static string Sims036_transport_fee_both = "Fee10";
        public static string Sims036_transport_fee_in = "Fee11";
        public static string Sims036_transport_fee_out = "Fee12";
        #endregion


       

        public static string imgpath = "http://sjs.mograsys.com";
        public static string school_code = "01";
        #region Invs006 (Supplier Master)
        public static string Invs006_appl_code = "Inv006";
        public static string Invs006_invs_appl_form_field_location = "Supplier Location";
        public static string Invs006_invs_appl_form_field_sup_type = "Supplier Type";
        public static string Invs006_staff_ledger_code = "03";
        public static string Invs006_student_ledger_code = "04";
        #endregion
        #region Sims042

        public static string Sims042_appl_code = "Per099";
        public static string Sims042_appl_form_field = "Gender";
        public static string Sims042_appl_form_field1 = "Visa Type";
        public static string Sims042_appl_form_field2 = "Secret Question";

        public static string Sims042_appl_code1 = "Sim010";
        public static string Sims042_appl_form_field3 = "Legal";
        public static string Sims042_appl_form_field9 = "BloodGroup";
        public static string Sims042_appl_form_field_1 = "LangProfieciency";

        public static string Sims042_appl_code2 = "Sim042";
        public static string Sims042_appl_form_field4 = "Parent Status";
        public static string Sims042_appl_form_field5 = "Academic Status";
        public static string Sims042_appl_form_field6 = "Financial Status";
        public static string Sims042_appl_form_field7 = "Register Status";
        public static string Sims042_appl_form_field8 = "Transfer Status";

        #endregion

        #region pays_shift_master
        public static string shift_mod_code = "004";
        public static string shift_appl_code = "Per092";
        public static string shift_cur_code = "02";
        #endregion

        public static string login_code = "EMP177";
       // public static string Cfyear = "Current Year";
        public static string sims_appl_code_incidence_action = "Sim150";
        public static string sims_appl_form_field_incidence_action = "Action Type";
        public static string sims_appl_form_field_incidence_action_1 = "Action Color Type";
        public static string dep_code = "10";
        public static string com_code = "1";
      //  public static string Current_Financial_Year = "C";
       
        public static string sims_appl_form_field_incidence_consequence = "Incidence Type";

        public static string sims_mod_code_incidence = "017";
        public static string sims_appl_code_incidence_consequence = "Sim152";
        public static string sims_appl_code_incidence = "Sim149";
        public static string sims_appl_form_field_incidence = "Incidence Action Type";
        public static string sims_appl_form_field_incidence_1 = "Consequence Action Type";

        public static string Sims032_appl_code = "Sim010";
        public static string Sims032_appl_form_field = "Salutation";
        public static string Sims032_appl_code1 = "Sim032";
        public static string Sims032_appl_form_field1 = "Relationship";

        public static string fins001_comp_code = "1";
        public static string fins001_mod_code = "005";
        public static string fins001_appl_code = "Fin001";
        public static string fins001_appl_form_field = "Account Type";
        public static string fins001_glac_status = "A";


        //Application-Comn001
        public static string mod_code = "001";
        public static string appl_code = "com001";
        public static string field = "status";

        public static string sims_mod_code = "000";
        public static string sims_appl_code = "Common";
        public static string sims_appl_form_field = "App Mode";
        public static string sims_appl_form_field1 = "App Type";
        public static string sims_appl_form_field2 = "App Tag";


        #region sims166
        public static string sims166_app_code = "Sim166";
        public static string sims166_cur_code = "01";
        public static string sims166_mod_code = "003";
        public static string sims166_type_code = "Alert Type";
        public static string sims166_priority_code = "Alert Priority";
        public static string sims166_Alert_type_Alert = "A";
        public static string sims166_Alert_type_Message = "M";
        public static string sims166_Alert_type_Notification = "N";
        #endregion


        //teacher mappning
        public static string eta_appl_code = "Per099";
        public static string eta_appl_form_field = "Teacher Type";

        public static string comn013_mod_code = "011";
        public static string comn013_appl_code = "Com013";
        public static string comn013_appl_form_field = "Stored Procedures Type";


        #region Forward Agent(Invs135)
        public static string Invs135_mod_code = "006";
        public static string Invs135_sequence_code = "FAG";
        public static string Invs135_appl_code = "Inv135";
        public static string Invs135_cur_code = "02";
        #endregion

        #region finsParameter
        public static string finsModCode = "(005)";
        #endregion

        #region Custom Clearance(Invs133)
        public static string Invs133_mod_code = "006";
        public static string Invs133_sequence_code = "CUS";
        public static string Invs133_appl_code = "Inv133";
        public static string Invs133_cur_code = "02";
        #endregion

        

        #region Finn010
        public static string finn010_appl_form_field = "Department Type";
        public static string finn010_Department_Name = "Duplicate";
        public static string finn010_Department_short_name = "dup";
        public static string finn010_Department_Type = "dup";
        public static string finn010_comp_code = "1";
        public static string finn010_appl_code = "Fin010";
        public static string finn010_codp_status = "A";
        #endregion






        //Invs052
        public static string Invs052_comp_code = "1";
        public static string Invs052_appl_code = "Inv052";

        //Invs027
        public static string Invs027_ledger_code = "00";

        //Invs126
        public static string Invs126_ledger_code = "00";

        //Invs128
        public static string Invs128_ledger_code = "00";

        //sanjay new
        public static string Sims012_app_code = "Sim012";
        public static string Sims012_criteria_name = "Criteria Type";
        //new included for fins138 class
        public static string comp_code = "1";
        public static string Schedule_code = "710";
        public static string year = "Current Year";
        //Sims101
        public static string Sims101_appl_code = "Sim101";
        public static string Sims101_sequence_code = "Mvn";
        #region Invs046 Invs005
        public static string Invs005_ledger_code = "03";
        #endregion
        //end


        //sim001
        public static string sims_status_mod_code = "001";
        public static string sims_status_appl_code = "sim001";
        public static string sims_status_appl_form_field1 = "Academic Year";

        public static string Sims017_appl_code = "Sim017";
        public static string Sims017_sequence_code = "CN";
        public static string Sims017_concession_type = "Concession Type";
        public static string Sims017_applicable_on = "Concession Applicable On";
        public static string Sims017_discount_type = "Concession Discount Type";
        public static string Sims017_applicable_to = "Concession Applicable To";

        public static string Sims018_appl_code = "Sim018";
        public static string Sims018_sequence_code = "CTN";


      public static string sims_mod_code3 = "003";
        public static string sims_appl_code3 = "Sim035";
        public static string sims_appl_form_field3 = "Gender";

        public static string sims_mod_code4 = "003";
        public static string sims_appl_code4 = "Sim035";
        public static string sims_appl_form_field4 = "Setup";
    }
}