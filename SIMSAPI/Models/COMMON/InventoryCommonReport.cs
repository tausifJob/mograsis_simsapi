﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COMMON
{
    public class InventoryCommonReport

    {

        public string im_item_code { get; set; }

        public string im_inv_no { get; set; }

        public string im_desc { get; set; }

        public string loc_code { get; set; }

        public string loc_name { get; set; }

        public string il_cur_qty { get; set; }

        public string pc_code { get; set; }

        public string pc_desc { get; set; }

            
        public string invs_appl_form_field_value1 { get; set; }

        public string invs_appl_parameter { get; set; }

        public string dep_name { get; set; }

        public string dep_code { get; set; }
       
        // request summary
          public string req_no { get; set; }

        public string req_type { get; set; }

        public string rd_item_desc { get; set; }

            
        public string req_date { get; set; }

        public string sm_name { get; set; }

        public string req_status { get; set; }

  //sales document listdoc_date = dr["doc_date"].ToString();
                            

        public string comp_code { get; set; }

        public string comp_name { get; set; }

        public string doc_prov_no { get; set; }

        public string sal_name { get; set; }

        public string student_name { get; set; }

            

        public string doc_total_amount { get; set; }

        public string status { get; set; }

        public string doc_date { get; set; }

        public string dt_desc { get; set; }

  
	
	
	
	
		
			
					
					
				
    }
}