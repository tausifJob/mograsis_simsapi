﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ComnFile
{
    public class ComnClassFile
    {
        //Alert
        public string comn_alert_user_code { get; set; }
        public string user_name { get; set; }
        public string comn_alert_message { get; set; }
        public string priority { get; set; }
        public string comn_alert_date { get; set; }
        public string comn_alert_status { get; set; }


        //Teacher Observation Query Report
        public string sims_subject_name_en { get; set; }
        public string observation { get; set; }
        public string outstanding { get; set; }
        public string very_good { get; set; }
        public string acceptable { get; set; }
        public string good { get; set; }
        public string very_weak { get; set; }
        public string weak { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_lesson_creation_date { get; set; }
        public string sims_comment_description { get; set; }
        public string sims_lesson_created_by { get; set; }
        public string sims_lesson_created_for { get; set; }

        public string sims_cur_code { get; set; }
        public string display_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_lesson_subject_code { get; set; }

        public string sims_comment_template_id { get; set; }

        public string sims_drop_down_display_text { get; set; }

        public string sims_drop_down_display_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_comment_template_name { get; set; }
    }
}