﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.COMMON
{
    public class sims_admission_status_rpt_param
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_start_date { get; set; }
        public string sims_end_date { get; set; }
        public string admission_no { get; set; }

        public object sims_application_status_code { get; set; }
    }
    public class sims_admission_status_rpt
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_start_date { get; set; }
        public string sims_end_date { get; set; }
        public string admission_no { get; set; }

        public string sims_application_status_code { get; set; }
        public string sims_application_status_name { get; set; }

        public string admission_date { get; set; }
        public string student_name { get; set; }
        public string student_enrollno { get; set; }
        public string admission_grade { get; set; }
        public string admission_section { get; set; }
        public string gender { get; set; }
        public string isFeePaid { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string mobile_no { get; set; }
        public string email { get; set; }
        public string student_dob { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string sibling_name { get; set; }
        public string sibling_grade { get; set; }


        public string sibling_section { get; set; }

        public string parent_email { get; set; }
    }

    public class CommonClass
    {
    }
    #region portal_communication
    public class portal_communication_his
    {
        public string sims_communication_number { get; set; }
        public string sims_communication_date { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_id { get; set; }
        public string sims_subject_new_message_count { get; set; }
        public string sims_comm_sender_id { get; set; }
        public string sims_comm_recepient_id { get; set; }

        public string sims_subject { get; set; }
        public string sims_subject_name1 { get;  set; }
        public string sims_message_file_name { get; set; }
        public List<message_doc> msgdoc = new List<message_doc>();
    }

    public class message_doc
    {
       public string sims_message_file_name { get; set; }
    }


    public class empSearch
    {

        public object em_login_code { get; set; }

        public object EmpName { get; set; }

        public string em_number { get; set; }

        public string dept_name { get; set; }

        public string desg_name { get; set; }

        public string em_mobile { get; set; }

        public string em_email { get; set; }

        public string em_service_status { get; set; }

        public string em_service_status_name { get; set; }
    }

    public class portal_communication_lst
    {
        public List<portal_communication> comn_lst { get; set; }


        public string totalCount { get; set; }
    }

    public class portal_communication
    {
        public portal_communication()
        {
            List<portal_communication_his> comnn_histr = new List<portal_communication_his>();
        }
        public string sims_communication_number { get; set; }
        public string sims_communication_date { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_id { get; set; }
        public string sims_subject_new_message_count { get; set; }
        public string sims_comm_sender_id { get; set; }
        public string sims_comm_recepient_id { get; set; }
        public List<portal_communication_his> comnn_histr { get; set; }

        public string sims_comm_user_name { get; set; }
        public string sims_full_name { get; set; }

        public string sims_subject_status { get; set; }

        public string sims_grade_section_name { get; set; }

        public string sims_grade_section_code { get; set; }

        public string sims_comn_tran_no { get; set; }

        public string sims_comm_message { get; set; }

        public string sims_communication_date1 { get; set; }

        public string sims_receipient_name { get; set; }

        public string sims_comm_recepient_name { get; set; }

        public bool sims_is_stud { get; set; }

        public string parent_id { get; set; }
        public string sims_comm_recepient_name1 { get;  set; }
    }

    #endregion

    #region common_menu
    public class commn_appl_type
    {
        public string comn_user_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
    }
    public class comomn_menu
    {
        public comomn_menu()
        {
            appl_type = new List<commn_appl_type>();
        }
        public List<commn_appl_type> appl_type { get; set; }
        public string comn_user_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_name_ar { get; set; }
        public string comn_appl_type { get; set; }
        public string comn_appl_type_name { get; set; }
        public string comn_mod_location { get; set; }
        public string comn_appl_mod_code { get; set; }

        public string comn_appl_image { get; set; }

        public string comn_mod_name_en { get; set; }

        public string comn_mod_name_ar { get; set; }

        public string comn_mod_color { get; set; }

        public string comn_mod_img { get; set; }

        public string comn_appl_location { get; set; }

        public string comn_appl_type_name_ar { get; set; }

        public string comn_appl_type_color { get; set; }

        public string comn_appl_display_order { get; set; }
    }
    public class common_notifications
    {
        public string common_notification_type { get; set; }
        public string common_notification_subject { get; set; }
        public string common_notification_date { get; set; }
    }

    #endregion

    #region (Email-SMS)//By Shweta


    public class Com052
    {

        public string sims_grade_code { get; set; }

        public string section_code { get; set; }

        public string section_name { get; set; }

        public string sims_grade_name { get; set; }

        public string grade_section_name { get; set; }

        public string grade_section_code { get; set; }

        public string designation { get; set; }

        public string designation_name { get; set; }

        public string em_dept_code { get; set; }

        public string em_dept_name { get; set; }

        public bool sims_father { get; set; }

        public bool sims_mother { get; set; }

        public bool sims_guardian { get; set; }

        public bool sims_active { get; set; }
    }

    public class Comn_email1
    {
        public string subject { get; set; }
        public string body { get; set; }
        public string emailsendto { get; set; }
    }

    public class Comn_email
    {
        public string username { get; set; }
        public string enrollnumber { get; set; }
        public string flag { get; set; }
        public string emailnumber { get; set; }
        public string sender_emailid { get; set; }
        public string emp_emailid { get; set; }
        public string father_emailid { get; set; }
        public string mother_emailid { get; set; }
        public string guardian_emailid { get; set; }
        public string emaildate { get; set; }
        public string errorcode { get; set; }
        public string status { get; set; }
        public string fflag { get; set; }
        public string mflag { get; set; }
        public string gflag { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string emailsendto { get; set; }

        //For email schedule 
        public string sims_email_message { get; set; }
        public string sims_recepient_id { get; set; }
        public string sims_recepient_bcc_id { get; set; }
        public string sims_recepient_cc_id { get; set; }
        public string sims_recepient_search_id { get; set; }
        public string sims_email_subject { get; set; }
        public string sims_email_attachment { get; set; }
        public string sims_email_schedule_date { get; set; }
        public string sims_email_recurrance_id { get; set; }


        public List<comn_email_attachments> comn_email_attachments { get; set; }

        public string sims_smtp_address { get; set; }
        public string sims_smtp_port { get; set; }
        public string sims_smtp_password { get; set; }
        public string sims_sslReq { get; set; }
        public string sims_msg_signature { get; set; }
        public string sims_admission_parent_REG_id { get; set; }
        public string sims_admission_password { get; set; }

        public string sims_senderid { get; set; }

        public string name { get; set; }
    }

    public class comn_email_attachments
    {
        public string attFilename { get; set; }
        public string attFilePath { get; set; }
    }

    public class Comn_email_smtp
    {
        public string sims_school_code { get; set; }
        public string sims_smtp_code { get; set; }
        public string sims_mod_code { get; set; }
        public string sims_smtp_serverip { get; set; }
        public string sims_smtp_port { get; set; }
        public string sims_smtp_username { get; set; }
        public string sims_smtp_password { get; set; }
        public string sims_smtp_from_email { get; set; }
        public string sims_smtp_status { get; set; }
        public string sims_smtp_auth_required { get; set; }
        public string sims_smtp_ssl_required { get; set; }
        public string sims_smtp_address { get; set; }
        public string sims_smtp_max_mail_hour { get; set; }
        public string sims_smtp_max_mail_day { get; set; }
        public string sims_smtp_encrypt_mode { get; set; }
        public string sims_smtp_from_email_name { get; set; }
        public string sims_smtp_config_id { get; set; }
        public string sims_smtp_signature { get; set; }
    }

    public class FileAttachment
    {
        public string FileContentBase64 { get; set; }
        public FileInfo Info { get; set; }
    }
    public class acyr
    {       
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
    }
    public class Comn_SMS
    {
        // Enter your message below (max 1000 chars.1 sms is 160 characters.)
        //These special characters \][}{|~^ will be treated as two character
        public bool arabicsms { get; set; }
        public string user_name { get; set; }
        public string enroll_number { get; set; }
        public string sms_number { get; set; }
        public string sms_contact_number { get; set; }
        public string sms_father_flag { get; set; }
        public string sms_father_contact_number1 { get; set; }
        public string sms_father_contact_number11 { get; set; }
        public string sms_father_contact_number111 { get; set; }
        public string sms_mother_flag { get; set; }
        public string sms_mother_contact_number2 { get; set; }
        public string sms_mother_contact_number22 { get; set; }
        public string sms_mother_contact_number222 { get; set; }
        public string sms_guardian_flag { get; set; }
        public string sms_guardian_contact_number3 { get; set; }
        public string sms_guardian_contact_number33 { get; set; }
        public string sms_guardian_contact_number333 { get; set; }
        public string sms_emergency_flag { get; set; }
        public string sms_emergency_contact_number4 { get; set; }
        public string sms_emergency_contact_number44 { get; set; }
        public string sms_emergency_contact_number444 { get; set; }
        public string sms_date { get; set; }
        public string sms_error_code { get; set; }
        public string sms_status { get; set; }
        public string sms_smstext { get; set; }
        public string sms_smssendto { get; set; }

        public string sms_sender { get; set; }
        public string sms_httpstring { get; set; }
        public string sms_password { get; set; }
        public string sims_smstext { get; set; }
        public string usercode { get; set; }
        public string senderid { get; set; }
        public string recepient_id { get; set; }
        public string recepient_searchid { get; set; }
        public string sms_message { get; set; }
        public string scheduledate { get; set; }
        public string balance { get; set; }
        public string status { get; set; }

        public object smssendto { get; set; }

        public string name { get; set; }

        public string username { get; set; }

        public object sims_recepient_cc_id { get; set; }

        public object sims_recepient_search_id { get; set; }

        
    }
    #endregion


    public class CommonUserControlClass
    {
        public bool ischecked { get; set; }
        public string search_std_enroll_no { get; set; }

        public string search_std_name { get; set; }

        public string search_std_grade_name { get; set; }

        public string search_std_section_name { get; set; }

        public string search_std_passport_no { get; set; }

        public string sims_nationality_name_en { get; set; }

        public string std_national_id { get; set; }

        public string s_cur_code { get; set; }

        public object sims_academic_year { get; set; }

        public string s_enroll_no { get; set; }

        public string search_std_family_name { get; set; }

        public string s_sname_in_english { get; set; }

        public string s_class { get; set; }

        public string s_parent_name { get; set; }

        public string s_parent_id { get; set; }

        public string s_sname_in_arabic { get; set; }

        public string user_name { get; set; }

        public string Teacher_Type { get; set; }

        public string search_std_image { get; set; }

        public string search_parent_Summary_address { get; set; }

        public string name { get; set; }

        public string search_parent_first_name { get; set; }

        public string search_parent_last_name { get; set; }

        public string search_parent_midd_name { get; set; }

        public string grade_code { get; set; }

        public string section_code { get; set; }

        public string status_name { get; set; }

        public string Mail_id { get; set; }

        public string Mobile_no { get; set; }

        public string code { get; set; }

        public string sims_section_code { get; set; }

        public string search_parent_id { get; set; }

        public string search_parent_name { get; set; }

        public string search_parent_email_id { get; set; }

        public string search_parent_mobile_no { get; set; }

        public string search_teacher_id { get; set; }

        public string search_teacher_name { get; set; }

        public string search_par_image { get; set; }

        public string mother_name { get; set; }

        public string mother_email_id { get; set; }

        public string mother_mobile_no { get; set; }

        public string guardian_name { get; set; }

        public string guardian_email_id { get; set; }

        public string guardian_mobile_no { get; set; }

        public string grade_name { get; set; }

        public string section_name { get; set; }

        public string Teacher_Name { get; set; }

        public string Teacher_id { get; set; }

        public string search_user_name { get; set; }

        public string search_user_email_id { get; set; }

        public string user_code { get; set; }

        public string user_group_id { get; set; }

        public string user_date_created { get; set; }

        public bool user_captcha_status { get; set; }

        public string user_last_login { get; set; }

        public bool user_status { get; set; }

        public string user_expiry_date { get; set; }

        public string designation { get; set; }

        public string designation_name { get; set; }

        public string Sdate { get; set; }

        public string search_code { get; set; }

        public string search_query { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string sims_cur_level_name_en { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string sims_student_enroll_number { get; set; }

        public string sims_student_passport_name_en { get; set; }

        public string sims_student_nickname { get; set; }

        public string sims_student_gender { get; set; }

        public string sims_religion_name_en { get; set; }

        public string sims_parent_guardian_email { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_parent_mother_email { get; set; }

        public string sims_parent_father_email { get; set; }

        public string sims_parent_guardian_mobile { get; set; }

        public string sims_parent_mother_mobile { get; set; }

        public string sims_parent_father_mobile { get; set; }

        public string sims_parent_guardian_family_name { get; set; }

        public string sims_parent_mother_family_name { get; set; }

        public string sims_parent_father_family_name { get; set; }

        public string sims_parent_guardian_name { get; set; }

        public string sims_student_remark { get; set; }

        public string sims_house_name { get; set; }

        public string sims_parent_father_name { get; set; }

        public string sims_parent_mother_name { get; set; }

        public string status { get; set; }

        public string curr_code { get; set; }

        public string acad_yr { get; set; }
    }

    


    //Common Sequence
    public class Comn004
    {
        public string sims_cur_short_name_en { get; set; }
        public string comn_application_code { get; set; }
        public string comn_mod_code { get; set; }
        public string comn_squence_code { get; set; }
        public string comn_description { get; set; }
        public string comm_cur_code { get; set; }
        public string comn_academic_year { get; set; }
        public string comn_character_count { get; set; }
        public string comn_character_squence { get; set; }
        public string comm_number_count { get; set; }
        public string comn_number_squence { get; set; }
        public bool comn_parameter_ref { get; set; }
        public string comn_application_name { get; set; }
        public string comn_mod_name_en { get; set; }
        public string comn_number_count { get; set; }
        public string comn_academic_year_desc { get; set; }

        public string opr { get; set; }

        public string comm_cur_name { get; set; }
    }

    //Comn006
    public class Comn006
    {
        public string comn_user_code { get; set; }
        public string comn_user_appl_code { get; set; }
        public string comn_user_appl_name { get; set; }
        public string comn_user_role_id { get; set; }
        public string comn_user_role_name { get; set; }
        public string comn_user_appl_fav { get; set; }
        public string comn_user_appl_count { get; set; }
        public bool comn_user_read { get; set; }
        public bool comn_user_insert { get; set; }
        public bool comn_user_update { get; set; }
        public bool comn_user_delete { get; set; }
        public bool comn_user_massupdate { get; set; }
        public bool comn_user_admin { get; set; }
        public bool all { get; set; }
        public bool comn_user_appl_status { get; set; }
        public string comn_role_name { get; set; }


        public string comn_role_code { get; set; }

        public string users { get; set; }

        public string comn_user_name { get; set; }
    }


    public class commonClass
    {
        //Variables of Comn003
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_role_code { get; set; }
        public string comn_audit_mod_code { get; set; }



        //comn_role_application
        public string comn_role_appl_id { get; set; }

        //comn_user
        public string lst_comn_user_code { get; set; }
        public string lst_comn_user_name { get; set; }

        //comn_user_role
        public string lst_comn_role_code { get; set; }
        public string lst_comn_role_name { get; set; }


        //comn_user_applications
        public string selected_application_code { get; set; }
        public string selected_application_name { get; set; }


        //BreadCrumb
        public class BreadCrumb
        {
            public string Header { get; set; }
            public string Modpath { get; set; }
        }


        public string comn_role_code_o { get; set; }
    }

    //User languges
    public class Comn007
    {
         public string opr { get; set; }

        public string id { get; set; }

         public object comn_application_code { get; set; }

         public object comn_mod_code { get; set; }

         public object sims_english { get; set; }

         public object sims_english_en { get; set; }

         public object sims_arabic_en { get; set; }

         public string comn_application_name { get; set; }

         public string comn_mod_name_en { get; set; }
    }

    public class Comn505
    {
        public string opr { get; set; }


        public object comn_application_code { get; set; }

        public object comn_mod_code { get; set; }

        public object sims_english { get; set; }

        public object sims_english_en { get; set; }

        public object sims_arabic_en { get; set; }

        public string comn_application_name { get; set; }

        public string comn_mod_name_en { get; set; }
         
    }
    #region Ganesh

    #region Exceptions

    public class Comn012
    {
        public Int64 ed_exception_id { get; set; }
        public string ed_exception_type { get; set; }
        public string ed_appl_name { get; set; }
        public string ed_db_tbl_name { get; set; }
        public string ed_exception_msg { get; set; }
        public string ed_inner_exception_msg { get; set; }
        public string ed_stack_strace_msg { get; set; }
        public string ed_keycode { get; set; }
        public string ed_reg_date { get; set; }
    }
    #endregion

    #region Comn050


    public class Comn050
    {
        public int UserCount { get; set; }
        public string comn_alert_number { get; set; }
        public string comn_alert_user_code { get; set; }
        public string comn_alert_mod_code { get; set; }
        public string comn_alert_date { get; set; }
        public DateTime alert_date { get; set; }
        public string comn_alert_message { get; set; }
        public string comn_alert_type_code { get; set; }
        public string comn_alert_sender_type { get; set; }
        public string comn_alert_priority_code { get; set; }
        public bool comn_alert_status { get; set; }

        public string mod_name { get; set; }
        public string mod_code { get; set; }
        public string alert_priority { get; set; }
        public string alert_priority_code { get; set; }
        public string alert_code { get; set; }
        public string alert_type { get; set; }

        public string user_code { get; set; }
        public string user_name { get; set; }
        public object opr { get; set; }
        //  public string u_name { get; set; }

    }

    #endregion

    #endregion

    #region(Pooja)
    //Application
    public class Comn001
    {
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_name_ar { get; set; }
        public string comn_appl_name_fr { get; set; }
        public string comn_appl_name_ot { get; set; }
        public string comn_appl_mod_code { get; set; }
        public string comn_appl_mod_name { get; set; }
        public string comn_app_mode { get; set; }
        public string comn_app_mode_code { get; set; }
        public string comn_appl_type { get; set; }
        public string comn_appl_type_code { get; set; }
        public string comn_appl_keywords { get; set; }
        public bool comn_appl_status { get; set; }
        public string comn_appl_version { get; set; }
        public string comn_appl_location { get; set; }
        public string comn_appl_logic { get; set; }
        public int comn_appl_display_order { get; set; }
        public string comn_appl_tag { get; set; }
        public string comn_appl_tag_code { get; set; }
        public string comn_appl_help_url { get; set; }
        public string comn_appl_video_url { get; set; }
        public string comn_appl_location_html { get; set; }

        public object opr { get; set; }
    }
    #endregion

}