﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.CommonSche
{
    public class CommonScheduling
    {
        //Exceeding Teacher Workload
        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_teacher_emp_code { get; set; }
        public string sims_bell_teacher_desc { get; set; }
        public string section_bell_teacher { get; set; }
        public string cnt { get; set; }
        public string sims_bell_academic_year { get; set; }
        public string sims_bell_desc { get; set; }
        public string sims_bell_status { get; set; }
         public string sims_bell_version_name { get; set; }
         public string sims_bell_code { get; set; }
        public string value { get; set; }
        public string desc { get; set; }



    }
}