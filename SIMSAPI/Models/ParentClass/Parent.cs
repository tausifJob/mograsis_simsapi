﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ParentClass
{
    public enum MessageType { Error, Success, Warning };
    public class Message
    {
        public MessageType messageType;
        public string strMessage { get; set; }
        public string systemMessage { get; set; }

        public bool status { get; set; }
    }
    //public class Parent
    //{

    //}

    public class Invs002
    {
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string uom_code_belongs { get; set; }
        public string uom_desc { get; set; }
        public decimal uc_factor { get; set; }
    }

    public class parentClass
    {
        public string comn_appl_name_en { get; set; }
        public string comn_appl_location { get; set; }
        public string comn_appl_display_order { get; set; }


        public string sims_student_passport_fullname { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_acad_yr { get; set; }
        public string status { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_student_img { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_sibling_student_enroll_number { get; set; }
        public string sims_student_img_path { get; set; }
        public string sims_student_full_name { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_enroll_no { get; set; }

        //Parent image
        public string sims_admission_father_first_name { get; set; }
        public string sims_admisison_father_image { get; set; }
        //mother
        public string sims_admission_mother_first_name { get; set; }
        public string sims_admisison_mother_image { get; set; }
        //guardian
        public string sims_admission_guardian_first_name { get; set; }
        public string sims_admisison_guardian_image { get; set; }

        //StudentApplications
        public string PageName { get; set; }
        public string PageLocation { get; set; }


        public string sims_transport_route_code { get; set; }

        public string sims_transport_route_short_name { get; set; }
        public string sims_academic_year_description { get; set; }
    }

    public class PP006
    {
        public string sims_teacher_img { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_teacher_type { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_teacher_login_code { get; set; }

    }

    #region SMS details

    public class comn053
    {
        public string sims_enroll_number { get; set; }

        public string sims_sms_subject { get; set; }

        public string sims_sms_message { get; set; }

        public string sims_user_name { get; set; }

        public string sims_sms_date { get; set; }

        public string sims_sms_status { get; set; }

        public string sims_sender_id { get; set; }



        public string sims_sms_number { get; set; }

        public string sims_sender_sms_id { get; set; }
    }

    #endregion


    #region Email details

    public class comn052
    {
        public string sims_user_name { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_sender_email_id { get; set; }

        public string sims_email_father_email_id1 { get; set; }

        public string sims_email_date { get; set; }

        public string sims_email_subject { get; set; }

        public string sims_email_message { get; set; }

        public string sims_email_status { get; set; }


        public string sims_email_number { get; set; }
    }
    #endregion

    public class common
    {

        public string lic_school_name { get; set; }

        public string lic_website_url { get; set; }

        public string lic_school_logo { get; set; }

        public string lic_lms_url { get; set; }
    }

    public class Medicine
    {
        public string Sims_Medicine_Code { get; set; }
        public string Sims_Medicine_Name { get; set; }
        public string Sims_Medicine_Type { get; set; }
        public string Sims_Medicine_ExpiryAlert { get; set; }
        public string Sims_Medicine_BatchNo { get; set; }
        public string Sims_Medicine_Prod_Date { get; set; }
        public string Sims_Medicine_Exp_Date { get; set; }
        public string Sims_Medicine_Type_Name { get; set; }
        public string Sims_Medicine_AlertName { get; set; }
        public string Sims_Medicine_Alert { get; set; }
    }

    public class AdmissionCriteria
    {
        public string Sims_Criteria_code { get; set; }
        public string Sims_Criteria_Name_en { get; set; }
        public string Sims_Criteria_Name_ar { get; set; }
        public string Sims_Criteria_Name_fr { get; set; }
        public string Sims_Criteria_Name_ot { get; set; }
        public string Sims_Criteria_Type { get; set; }
        public string Sims_Criteria_Type_Name { get; set; }
        public string Sims_Criteria_status { get; set; }
        public string Sims_Criteria_Mandatory { get; set; }
    }




    #region vehicle_consumption
    public class ExcelData
    {

        public string opr { get; set; }

        public string sup_code { get; set; }
        public bool sup_code_flag { get; set; }

        public string sup_location_code { get; set; }
        public bool sup_location_code_flag { get; set; }

        public string vehicle_code { get; set; }
        public bool vehicle_code_flag { get; set; }

        public string im_inv_no { get; set; }
        public bool im_inv_no_flag { get; set; }

        public DateTime sale_time { get; set; }

        public string unit_price { get; set; }
        public string Total_Amount { get; set; }
        public string sale_qty { get; set; }

        public string sale_period { get; set; }

        public string driver_code { get; set; }

        public bool driver_code_flag { get; set; }
    }
    #endregion

    #region Calender
    public class Uccw008
    {
        public bool sims_calendar_event_priority1 { get; set; }
        public string sims_appl_parameter_calender { get; set; }
        public string sims_appl_form_field_value1_calender { get; set; }
        public string sims_appl_form_field_value2_calender { get; set; }
        public string sims_appl_form_field_value3_calender { get; set; }

        public string sims_calendar_number { get; set; }
        public string sims_calendar_date { get; set; }
        public string sims_calendar_event_start_time { get; set; }
        public string sims_calendar_event_end_time { get; set; }
        public string sims_calendar_event_type { get; set; }
        public string sims_calendar_event_category { get; set; }
        public bool sims_calendar_event_priority { get; set; }
        public string sims_calendar_event_short_desc { get; set; }
        public string sims_calendar_event_desc { get; set; }
        public bool sims_calendar_event_status { get; set; }
        public string sims_calendar_time_marker { get; set; }

        public string sims_calendar_event_time { get; set; }
        public string sims_calendar_event_type1 { get; set; }
        public string sims_calendar_event_color { get; set; }
        public string sims_calendar_end_date { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_parameter { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value1 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value2 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value3 { get; set; }



        public string id { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string title { get; set; }

        public string sims_event_color { get; set; }

        public string sims_calendar_event_category_desc { get; set; }

        public string sims_appl_form_field { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string color_code { get; set; }

        public object opr { get; set; }
    }
    #endregion


    #region ParentCirculer
    public class Uccw007
    {
        public string sims_circular_date { get; set; }
        public string sims_circular_publish_date { get; set; }
        public string sims_circular_title { get; set; }
        public string sims_circular_short_desc { get; set; }
        public string sims_circular_desc { get; set; }
        public string sims_circular_file_name1 { get; set; }
        public string sims_circular_file_name2 { get; set; }
        public string sims_circular_file_name3 { get; set; }
        public string sims_circular_file_path1 { get; set; }
        public string sims_circular_file_path2 { get; set; }
        public string sims_circular_file_path3 { get; set; }
        public bool ciculerfileflag { get; set; }
        public string sims_circular_number { get; set; }
    }
    #endregion

    //#region Student(Sims042)
    //public class Sims042
    //{
    //    public string sims_parent_father_state_code;
    //    public string sims_parent_father_city_code;
    //    //public string sims_student_enroll_number { get; set; }
    //    public string student_enroll_no { get; set; }
    //    public string sims_parent_number { get; set; }
    //    public string sims_student_cur_code { get; set; }
    //    public string sims_student_passport_first_name_en { get; set; }
    //    public string sims_student_passport_middle_name_en { get; set; }
    //    public string sims_student_passport_last_name_en { get; set; }
    //    public string sims_student_family_name_en { get; set; }
    //    public string sims_student_nickname { get; set; }
    //    public string sims_student_passport_first_name_ot { get; set; }
    //    public string sims_student_passport_middle_name_ot { get; set; }
    //    public string sims_student_passport_last_name_ot { get; set; }
    //    public string sims_student_family_name_ot { get; set; }
    //    public string sims_student_gender { get; set; }
    //    public string sims_student_gender_code { get; set; }
    //    public string sims_student_religion_code { get; set; }
    //    public string sims_student_religion { get; set; }
    //    public string sims_student_dob { get; set; }
    //    public string sims_student_birth_country_code { get; set; }
    //    public string sims_student_birth_country { get; set; }
    //    public string sims_student_nationality_code { get; set; }
    //    public string sims_student_ethnicity_code { get; set; }
    //    public string sims_student_ethnicity { get; set; }
    //    public string sims_student_visa_number { get; set; }
    //    public string sims_student_visa_issue_date { get; set; }
    //    public string sims_student_visa_expiry_date { get; set; }
    //    public string sims_student_visa_issuing_place { get; set; }
    //    public string sims_student_visa_issuing_authority { get; set; }
    //    public string sims_student_visa_type { get; set; }
    //    public string sims_student_visa_type_code { get; set; }
    //    public string sims_student_national_id { get; set; }
    //    public string sims_student_national_id_issue_date { get; set; }
    //    public string sims_student_national_id_expiry_date { get; set; }
    //    public string sims_student_main_language_code { get; set; }
    //    public string sims_student_main_language { get; set; }
    //    public string sims_student_main_language_r_code { get; set; }
    //    public string sims_student_main_language_r { get; set; }
    //    public string sims_student_main_language_w_code { get; set; }
    //    public string sims_student_main_language_w { get; set; }
    //    public string sims_student_main_language_s_code { get; set; }
    //    public string sims_student_main_language_s { get; set; }
    //    public string sims_student_main_language_m { get; set; }
    //    public string sims_student_main_language_m_code { get; set; }
    //    public string sims_student_other_language { get; set; }
    //    public string sims_student_primary_contact_code { get; set; }
    //    public string sims_student_primary_contact_pref { get; set; }
    //    public string sims_student_fee_payment_contact_pref { get; set; }
    //    public string sims_student_transport_status { get; set; }
    //    public string sims_student_parent_status_code { get; set; }
    //    public string sims_student_parent_status { get; set; }
    //    public string sims_student_legal_custody { get; set; }
    //    public string sims_student_emergency_contact_name1 { get; set; }
    //    public string sims_student_emergency_contact_name2 { get; set; }
    //    public string sims_student_emergency_contact_number1 { get; set; }
    //    public string sims_student_emergency_contact_number2 { get; set; }
    //    public string sims_student_language_support_status { get; set; }
    //    public string sims_student_language_support_desc { get; set; }
    //    public string sims_student_behaviour_status { get; set; }
    //    public string sims_student_behaviour_desc { get; set; }
    //    public string sims_student_gifted_status { get; set; }
    //    public string sims_student_gifted_desc { get; set; }
    //    public string sims_student_music_status { get; set; }
    //    public string sims_student_music_desc { get; set; }
    //    public string sims_student_sports_status { get; set; }
    //    public string sims_student_sports_desc { get; set; }
    //    public string sims_student_date { get; set; }
    //    public string sims_student_commence_date { get; set; }
    //    public string sims_student_remark { get; set; }
    //    public string sims_student_login_id { get; set; }
    //    public string sims_student_current_school_code { get; set; }
    //    public string student_current_school_code { get; set; }
    //    public string sims_student_employee_comp_code { get; set; }
    //    public string sims_student_employee_code { get; set; }
    //    public string sims_student_last_login { get; set; }
    //    public string sims_student_secret_question_code { get; set; }
    //    public string sims_student_secret_question { get; set; }
    //    public string sims_student_secret_answer { get; set; }
    //    public string sims_student_academic_status { get; set; }
    //    public string sims_student_academic_status_code { get; set; }
    //    public string sims_student_financial_status { get; set; }
    //    public string sims_student_financial_status_code { get; set; }
    //    public string sims_student_house { get; set; }
    //    public string student_house { get; set; }
    //    public string sims_student_class_rank { get; set; }
    //    public string sims_student_honour_roll { get; set; }
    //    public string sims_student_ea_number { get; set; }
    //    public string sims_student_ea_registration_date { get; set; }
    //    public string sims_student_ea_transfer { get; set; }
    //    public string sims_student_ea_transfer_code { get; set; }
    //    public string sims_student_ea_status { get; set; }
    //    public string sims_student_ea_status_code { get; set; }
    //    public string sims_student_passport_number { get; set; }
    //    public string sims_student_passport_issue_date { get; set; }
    //    public string sims_student_passport_expiry_date { get; set; }
    //    public string sims_student_passport_issuing_authority { get; set; }
    //    public string sims_student_passport_issue_place { get; set; }
    //    public string sims_student_section_strength { get; set; }
    //    public string sims_student_image { get; set; }

    //    // Visiable
    //    public bool v_student_enroll_no { get; set; }
    //    public bool v_sims_parent_number { get; set; }
    //    public bool v_sims_student_cur_code { get; set; }
    //    public bool v_sims_student_passport_first_name_en { get; set; }
    //    public bool v_sims_student_passport_middle_name_en { get; set; }
    //    public bool v_sims_student_passport_last_name_en { get; set; }
    //    public bool v_sims_student_family_name_en { get; set; }
    //    public bool v_sims_student_nickname { get; set; }
    //    public bool v_sims_student_passport_first_name_ot { get; set; }
    //    public bool v_sims_student_passport_middle_name_ot { get; set; }
    //    public bool v_sims_student_passport_last_name_ot { get; set; }
    //    public bool v_sims_student_family_name_ot { get; set; }
    //    public bool v_sims_student_gender { get; set; }
    //    public bool v_sims_student_gender_code { get; set; }
    //    public bool v_sims_student_religion_code { get; set; }
    //    public bool v_sims_student_religion { get; set; }
    //    public bool v_sims_student_dob { get; set; }
    //    public bool v_sims_student_birth_country_code { get; set; }
    //    public bool v_sims_student_birth_country { get; set; }
    //    public bool v_sims_student_nationality_code { get; set; }
    //    public bool v_sims_student_ethnicity_code { get; set; }
    //    public bool v_sims_student_ethnicity { get; set; }
    //    public bool v_sims_student_visa_number { get; set; }
    //    public bool v_sims_student_visa_issue_date { get; set; }
    //    public bool v_sims_student_visa_expiry_date { get; set; }
    //    public bool v_sims_student_visa_issuing_place { get; set; }
    //    public bool v_sims_student_visa_issuing_authority { get; set; }
    //    public bool v_sims_student_visa_type { get; set; }
    //    public bool v_sims_student_visa_type_code { get; set; }
    //    public bool v_sims_student_national_id { get; set; }
    //    public bool v_sims_student_national_id_issue_date { get; set; }
    //    public bool v_sims_student_national_id_expiry_date { get; set; }
    //    public bool v_sims_student_main_language_code { get; set; }
    //    public bool v_sims_student_main_language { get; set; }
    //    public bool v_sims_student_main_language_r_code { get; set; }
    //    public bool v_sims_student_main_language_r { get; set; }
    //    public bool v_sims_student_main_language_w_code { get; set; }
    //    public bool v_sims_student_main_language_w { get; set; }
    //    public bool v_sims_student_main_language_s_code { get; set; }
    //    public bool v_sims_student_main_language_s { get; set; }
    //    public bool v_sims_student_main_language_m { get; set; }
    //    public bool v_sims_student_main_language_m_code { get; set; }
    //    public bool v_sims_student_other_language { get; set; }
    //    public bool v_sims_student_primary_contact_code { get; set; }
    //    public bool v_sims_student_primary_contact_pref { get; set; }
    //    public bool v_sims_student_fee_payment_contact_pref { get; set; }
    //    public bool v_sims_student_transport_status { get; set; }
    //    public bool v_sims_student_parent_status_code { get; set; }
    //    public bool v_sims_student_parent_status { get; set; }
    //    public bool v_sims_student_legal_custody { get; set; }
    //    public bool v_sims_student_emergency_contact_name1 { get; set; }
    //    public bool v_sims_student_emergency_contact_name2 { get; set; }
    //    public bool v_sims_student_emergency_contact_number1 { get; set; }
    //    public bool v_sims_student_emergency_contact_number2 { get; set; }
    //    public bool v_sims_student_language_support_status { get; set; }
    //    public bool v_sims_student_language_support_desc { get; set; }
    //    public bool v_sims_student_behaviour_status { get; set; }
    //    public bool v_sims_student_behaviour_desc { get; set; }
    //    public bool v_sims_student_gifted_status { get; set; }
    //    public bool v_sims_student_gifted_desc { get; set; }
    //    public bool v_sims_student_music_status { get; set; }
    //    public bool v_sims_student_music_desc { get; set; }
    //    public bool v_sims_student_sports_status { get; set; }
    //    public bool v_sims_student_sports_desc { get; set; }
    //    public bool v_sims_student_date { get; set; }
    //    public bool v_sims_student_commence_date { get; set; }
    //    public bool v_sims_student_remark { get; set; }
    //    public bool v_sims_student_login_id { get; set; }
    //    public bool v_sims_student_current_school_code { get; set; }
    //    public bool v_student_current_school_code { get; set; }
    //    public bool v_sims_student_employee_comp_code { get; set; }
    //    public bool v_sims_student_employee_code { get; set; }
    //    public bool v_sims_student_last_login { get; set; }
    //    public bool v_sims_student_secret_question_code { get; set; }
    //    public bool v_sims_student_secret_question { get; set; }
    //    public bool v_sims_student_secret_answer { get; set; }
    //    public bool v_sims_student_academic_status { get; set; }
    //    public bool v_sims_student_academic_status_code { get; set; }
    //    public bool v_sims_student_financial_status { get; set; }
    //    public bool v_sims_student_financial_status_code { get; set; }
    //    public bool v_sims_student_house { get; set; }
    //    public bool v_student_house { get; set; }
    //    public bool v_sims_student_class_rank { get; set; }
    //    public bool v_sims_student_honour_roll { get; set; }
    //    public bool v_sims_student_ea_number { get; set; }
    //    public bool v_sims_student_ea_registration_date { get; set; }
    //    public bool v_sims_student_ea_transfer { get; set; }
    //    public bool v_sims_student_ea_transfer_code { get; set; }
    //    public bool v_sims_student_ea_status { get; set; }
    //    public bool v_sims_student_ea_status_code { get; set; }
    //    public bool v_sims_student_passport_number { get; set; }
    //    public bool v_sims_student_passport_issue_date { get; set; }
    //    public bool v_sims_student_passport_expiry_date { get; set; }
    //    public bool v_sims_student_passport_issuing_authority { get; set; }
    //    public bool v_sims_student_passport_issue_place { get; set; }
    //    public bool v_sims_student_section_strength { get; set; }
    //    public bool v_sims_student_image { get; set; }

    //    //disabbool
    //    public bool d_student_enroll_no { get; set; }
    //    public bool d_sims_parent_number { get; set; }
    //    public bool d_sims_student_cur_code { get; set; }
    //    public bool d_sims_student_passport_first_name_en { get; set; }
    //    public bool d_sims_student_passport_middle_name_en { get; set; }
    //    public bool d_sims_student_passport_last_name_en { get; set; }
    //    public bool d_sims_student_family_name_en { get; set; }
    //    public bool d_sims_student_nickname { get; set; }
    //    public bool d_sims_student_passport_first_name_ot { get; set; }
    //    public bool d_sims_student_passport_middle_name_ot { get; set; }
    //    public bool d_sims_student_passport_last_name_ot { get; set; }
    //    public bool d_sims_student_family_name_ot { get; set; }
    //    public bool d_sims_student_gender { get; set; }
    //    public bool d_sims_student_gender_code { get; set; }
    //    public bool d_sims_student_religion_code { get; set; }
    //    public bool d_sims_student_religion { get; set; }
    //    public bool d_sims_student_dob { get; set; }
    //    public bool d_sims_student_birth_country_code { get; set; }
    //    public bool d_sims_student_birth_country { get; set; }
    //    public bool d_sims_student_nationality_code { get; set; }
    //    public bool d_sims_student_ethnicity_code { get; set; }
    //    public bool d_sims_student_ethnicity { get; set; }
    //    public bool d_sims_student_visa_number { get; set; }
    //    public bool d_sims_student_visa_issue_date { get; set; }
    //    public bool d_sims_student_visa_expiry_date { get; set; }
    //    public bool d_sims_student_visa_issuing_place { get; set; }
    //    public bool d_sims_student_visa_issuing_authority { get; set; }
    //    public bool d_sims_student_visa_type { get; set; }
    //    public bool d_sims_student_visa_type_code { get; set; }
    //    public bool d_sims_student_national_id { get; set; }
    //    public bool d_sims_student_national_id_issue_date { get; set; }
    //    public bool d_sims_student_national_id_expiry_date { get; set; }
    //    public bool d_sims_student_main_language_code { get; set; }
    //    public bool d_sims_student_main_language { get; set; }
    //    public bool d_sims_student_main_language_r_code { get; set; }
    //    public bool d_sims_student_main_language_r { get; set; }
    //    public bool d_sims_student_main_language_w_code { get; set; }
    //    public bool d_sims_student_main_language_w { get; set; }
    //    public bool d_sims_student_main_language_s_code { get; set; }
    //    public bool d_sims_student_main_language_s { get; set; }
    //    public bool d_sims_student_main_language_m { get; set; }
    //    public bool d_sims_student_main_language_m_code { get; set; }
    //    public bool d_sims_student_other_language { get; set; }
    //    public bool d_sims_student_primary_contact_code { get; set; }
    //    public bool d_sims_student_primary_contact_pref { get; set; }
    //    public bool d_sims_student_fee_payment_contact_pref { get; set; }
    //    public bool d_sims_student_transport_status { get; set; }
    //    public bool d_sims_student_parent_status_code { get; set; }
    //    public bool d_sims_student_parent_status { get; set; }
    //    public bool d_sims_student_legal_custody { get; set; }
    //    public bool d_sims_student_emergency_contact_name1 { get; set; }
    //    public bool d_sims_student_emergency_contact_name2 { get; set; }
    //    public bool d_sims_student_emergency_contact_number1 { get; set; }
    //    public bool d_sims_student_emergency_contact_number2 { get; set; }
    //    public bool d_sims_student_language_support_status { get; set; }
    //    public bool d_sims_student_language_support_desc { get; set; }
    //    public bool d_sims_student_behaviour_status { get; set; }
    //    public bool d_sims_student_behaviour_desc { get; set; }
    //    public bool d_sims_student_gifted_status { get; set; }
    //    public bool d_sims_student_gifted_desc { get; set; }
    //    public bool d_sims_student_music_status { get; set; }
    //    public bool d_sims_student_music_desc { get; set; }
    //    public bool d_sims_student_sports_status { get; set; }
    //    public bool d_sims_student_sports_desc { get; set; }
    //    public bool d_sims_student_date { get; set; }
    //    public bool d_sims_student_commence_date { get; set; }
    //    public bool d_sims_student_remark { get; set; }
    //    public bool d_sims_student_login_id { get; set; }
    //    public bool d_sims_student_current_school_code { get; set; }
    //    public bool d_student_current_school_code { get; set; }
    //    public bool d_sims_student_employee_comp_code { get; set; }
    //    public bool d_sims_student_employee_code { get; set; }
    //    public bool d_sims_student_last_login { get; set; }
    //    public bool d_sims_student_secret_question_code { get; set; }
    //    public bool d_sims_student_secret_question { get; set; }
    //    public bool d_sims_student_secret_answer { get; set; }
    //    public bool d_sims_student_academic_status { get; set; }
    //    public bool d_sims_student_academic_status_code { get; set; }
    //    public bool d_sims_student_financial_status { get; set; }
    //    public bool d_sims_student_financial_status_code { get; set; }
    //    public bool d_sims_student_house { get; set; }
    //    public bool d_student_house { get; set; }
    //    public bool d_sims_student_class_rank { get; set; }
    //    public bool d_sims_student_honour_roll { get; set; }
    //    public bool d_sims_student_ea_number { get; set; }
    //    public bool d_sims_student_ea_registration_date { get; set; }
    //    public bool d_sims_student_ea_transfer { get; set; }
    //    public bool d_sims_student_ea_transfer_code { get; set; }
    //    public bool d_sims_student_ea_status { get; set; }
    //    public bool d_sims_student_ea_status_code { get; set; }
    //    public bool d_sims_student_passport_number { get; set; }
    //    public bool d_sims_student_passport_issue_date { get; set; }
    //    public bool d_sims_student_passport_expiry_date { get; set; }
    //    public bool d_sims_student_passport_issuing_authority { get; set; }
    //    public bool d_sims_student_passport_issue_place { get; set; }
    //    public bool d_sims_student_section_strength { get; set; }
    //    public bool d_sims_student_image { get; set; }


    //    //PP001
    //    public string sims_student_parameter_code { get; set; }
    //    public string student_full_name { get; set; }
    //    public string student_full_name_ot { get; set; }
    //    public string sims_parent_country_code { get; set; }
    //    public string sims_parent_father_summary_address { get; set; }
    //    public string sims_parent_father_city { get; set; }
    //    public string sims_parent_father_state { get; set; }
    //    public string sims_parent_father_po_box { get; set; }
    //    public string sims_parent_father_phone { get; set; }
    //    public string sims_parent_father_mobile { get; set; }
    //    public string sims_parent_father_email { get; set; }
    //    public string sims_parent_father_fax { get; set; }

    //    public string sims_sibling_student_img_path { get; set; }
    //    public string sims_sibling_enroll_number { get; set; }
    //    public string sims_sibling_name { get; set; }
    //    public string sims_sibling_student_nickname { get; set; }
    //    public string sims_sibling_academic_year { get; set; }
    //    public string sims_sibling_grade_name_en { get; set; }
    //    public string sims_sibling_section_name_en { get; set; }
    //    public string sims_grade_name { get; set; }
    //    public string sims_grade_code { get; set; }
    //    public string sims_section_code { get; set; }
    //    public string sims_section_name { get; set; }
    //    public string EditButtonVisibility { get; set; }
    //    public bool v_sims_parent_father_summary_address { get; set; }
    //    public bool d_sims_parent_father_summary_address { get; set; }
    //    public bool d_sims_parent_father_city { get; set; }
    //    public bool v_sims_parent_father_city { get; set; }
    //    public bool v_sims_parent_father_state { get; set; }
    //    public bool d_sims_parent_father_state { get; set; }
    //    public bool v_sims_parent_country_code { get; set; }
    //    public bool d_sims_parent_country_code { get; set; }
    //    public bool v_sims_parent_father_po_box { get; set; }
    //    public bool d_sims_parent_father_po_box { get; set; }
    //    public bool v_sims_parent_father_phone { get; set; }
    //    public bool d_sims_parent_father_phone { get; set; }
    //    public bool v_sims_parent_father_mobile { get; set; }
    //    public bool d_sims_parent_father_mobile { get; set; }
    //    public bool v_sims_parent_father_email { get; set; }
    //    public bool d_sims_parent_father_email { get; set; }
    //    public bool v_sims_parent_father_fax { get; set; }
    //    public bool d_sims_parent_father_fax { get; set; }
    //}
    //#endregion




    #region Sims527(Agenda)
    public class Sims527
    {
        public string sims_agenda_number { get; set; }
        public string sims_agenda_name { get; set; }
        public string sims_agenda_desc { get; set; }
        public string sims_agenda_date { get; set; }
        public string day { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_acaedmic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }



        public string sims_academic_year { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_academic_year_status { get; set; }

        public string sims_report_filename { get; set; }

        public string exam { get; set; }
    }
    #endregion

    #region Photo gallary...
    public class PP003
    {
        public string sims_album_cur_code { get; set; }
        public string sims_album_code { get; set; }
        public string sims_album_name { get; set; }
        public string sims_album_publish_date { get; set; }
        public string sims_album_photo_description { get; set; }
        public string sims_album_photo_price { get; set; }
        public string sims_album_photo_path { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_sell_flag { get; set; }
    }

    #endregion

    #region Medical History
    public class PP009
    {
        public string health_card_number { get; set; }
        public string health_card_issue_date { get; set; }
        public string health_card_expiry_date { get; set; }
        public string health_card_issuing_authority { get; set; }
        public string student_blood_group_code { get; set; }
        public string blood_group { get; set; }
        public string student_health_restriction_status { get; set; }
        public string student_disability_status { get; set; }
        public string disability_desc { get; set; }
        public string student_medication_status { get; set; }
        public string health_other_status { get; set; }
        public string health_hearing_status { get; set; }
        public string health_vision_status { get; set; }
        public string regular_doctor_name { get; set; }
        public string regular_doctor_phone { get; set; }
        public string regular_hospital_name { get; set; }
        public string regular_hospital_phone { get; set; }
        public string student_height { get; set; }
        public string student_wieght { get; set; }
        public string student_teeth { get; set; }
        public string medical_visit_date { get; set; }
        public string medical_attended_by { get; set; }
        public string medical_complaint_desc { get; set; }
        public string medical_attendant_observation { get; set; }
        public string medical_health_education { get; set; }
        public string medical_followup_comment { get; set; }
        public string sims_student_image { get; set; }

        public bool visible { get; set; }

        public bool enable { get; set; }
    }
    #endregion

    //#region Transport_Route(PP011)
    //public class PP011
    //{
    //    public string sims_student_name { get; set; }
    //    public string sims_transport_enroll_number { get; set; }
    //    public string sims_sibling_parent_number { get; set; }
    //    public string sims_transport_route_code_name { get; set; }
    //    public string sims_transport_route_direction { get; set; }
    //    public string sims_transport_academic_year { get; set; }
    //    public string sims_transport_route_code { get; set; }
    //    public string sims_transport_effective_from { get; set; }
    //    public string sims_transport_effective_upto { get; set; }
    //    public string sims_transport_pickup_stop_code { get; set; }
    //    public string sims_transport_pickup_stop_name { get; set; }
    //    public string sims_transport_drop_stop_code { get; set; }
    //    public string sims_transport_drop_stop_name { get; set; }
    //    public string sims_student_img { get; set; }
    //}
    //#endregion

    #region Sims149...(Sims_Incidance)
    public class Sims149
    {

        public string sims_student_passport_first_name_en { get; set; }

        public string sims_incidence_warning_code { get; set; }
        public string sims_incidence_warning_desc { get; set; }
        public string sims_incidence_consequence_code { get; set; }
        public string sims_incidence_consequence_code_name {get;set;}
        public string sims_incidence_consequence_desc { get; set; }
        public string sims_incidence_consequence_type { get; set; }
        public string sims_incidence_consequence_type_desc { get; set; }
        public string sims_incidence_consequence_point { get; set; }
        public string sims_incidence_action_code { get; set; }
        public string sims_incidence_action_code_name { get; set; }
        public string sims_incidence_number { get; set; }
        public string sims_incidence_desc { get; set; }
        public string sims_incidence_enroll_number { get; set; }
        public string sims_incidence_building_code { get; set; }
        public string sims_incidence_building_desc { get; set; }
        public string sims_incidence_location_code { get; set; }
        public string sims_incidence_location_desc { get; set; }
        public string sims_incidence_action_type { get; set; }
        public string sims_incidence_action_type_desc { get; set; }
        public string sims_incidence_action_desc { get; set; }
        public decimal sims_incidence_action_point { get; set; }
        public string sims_incidence_user_code { get; set; }
        public string sims_incidence_user_code_updated { get; set; }
        public string sims_incidence_date { get; set; }
        public string sims_incidence_consequence_serve_start_date { get; set; }
        public bool sims_incidence_action_escalation_flag { get; set; }
        public bool sims_incidence_action_sms_flag { get; set; }
        public bool sims_incidence_action_email_flag { get; set; }
        public bool sims_incidence_action_portal_flag { get; set; }
        public bool sims_incidence_action_roll_over_flag { get; set; }
        public bool sims_incidence_consequence_serve_close_flag { get; set; }
        public bool sims_incidence_status { get; set; }
        public string sims_student_img { get; set; }

        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }

        public string opr { get; set; }
        public string sims_incidence_user_code_name { get; set; }
        public string sims_incidence_user_code_updated_name { get; set; }
        public string sims_teacher_code { get; set; }

        public string sims_detention_name { get; set; }

        public string sims_detention_description { get; set; }

        public string sims_detention_point { get; set; }

        public string sims_detention_transaction_created_date { get; set; }

        public string sims_academic_year { get; set; }
    }
    #endregion

    #region Library
    public class Sims136
    {
        public int sims_library_transaction_number { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_qty { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_reserve_release_date { get; set; }
        public decimal sims_library_item_rate { get; set; }
        public decimal sims_library_item_line_total { get; set; }
        public bool sims_library_item_transaction_status { get; set; }


        //PP028
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_author1 { get; set; }

        public string sims_library_item_actual_return_date { get; set; }
    }
    #endregion

    #region Sims136(Library Transaction Detail)
    public class sims136Detail
    {
        public string sims_library_transaction_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_transaction_type { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_transaction_date { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_user_number { get; set; }
        public string sims_library_transaction_remarks { get; set; }
        public string sims_library_transaction_total { get; set; }
        public string sims_library_transaction_created_by { get; set; }
        public bool isNew { get; set; }
        public bool isReturn { get; set; }
        public bool isReIssue { get; set; }
        public string sims_library_bnn_number { get; set; }
        public string sims_library_item_dewey_number { get; set; }
        public string sims_library_item_desc { get; set; }
        public string sims_library_item_issue_date { get; set; }
        
    }
    #endregion

    #region fee details
    public class PP008
    {
        public double sims_expected_fee { get; set; }
        public double sims_paid_fee { get; set; }
        public double sims_remaining_fee { get; set; }
        public string sims_img_path { get; set; }
        public double sims_con_fee { get; set; }
        public double sims_con_fee_paid { get; set; }
        public string sims_term_desc { get; set; }
        public double sims_expected_total { get; set; }
        public double sims_paid_total { get; set; }
        public double sims_remaining_total { get; set; }
        public string sims_fee_code_desctiption { get; set; }
        public string sims_date { get; set; }
        public string sims_name { get; set; }
        public string sims_doc_no { get; set; }
        public string sims_fee_term_code { get; set; }
        public string sims_fee_term_desc { get; set; }
    }
    #endregion

    //#region Sims010(Admission)
    //public class admissionClass
    //{

    //    public string sims_admission_login_name { get; set; }
    //    public string sims_admission_guardian_family_name_en { get; set; }
    //    public string sims_admission_mother_family_name_en { get; set; }
    //    public string sims_admission_student_enroll_number { get; set; }
    //    public string sims_admission_blood_group_code { get; set; }
    //    public string sims_admission_blood_group_name { get; set; }
    //    public DateTime? sims_admission_commencement_date { get; set; }
    //    public string sims_admission_cur_code { get; set; }
    //    public string sims_admission_cur_name { get; set; }
    //    public string sims_admission_current_school_address { get; set; }
    //    public string sims_admission_current_school_city { get; set; }
    //    public string sims_admission_current_school_country_code { get; set; }
    //    public string sims_admission_current_school_cur { get; set; }
    //    public string sims_admission_current_school_enroll_number { get; set; }
    //    public string sims_admission_current_school_fax { get; set; }
    //    public string sims_admission_current_school_from_date { get; set; }
    //    public string sims_admission_current_school_grade { get; set; }
    //    public string sims_admission_current_school_head_teacher { get; set; }
    //    public string sims_admission_current_school_language { get; set; }
    //    public string sims_admission_current_school_name { get; set; }
    //    public string sims_admission_current_school_phone { get; set; }
    //    public string sims_admission_current_school_status { get; set; }
    //    public string sims_admission_current_school_to_date { get; set; }
    //    public string sims_admission_date { get; set; }
    //    public string sims_admission_declaration_status { get; set; }
    //    public string sims_admission_disability_desc { get; set; }
    //    public string sims_admission_disability_status { get; set; }
    //    public string sims_admission_dns { get; set; }
    //    public string sims_admission_dob { get; set; }
    //    public string sims_admission_employee_code { get; set; }
    //    public string sims_admission_employee_school_code { get; set; }
    //    public string sims_admission_employee_type { get; set; }
    //    public string sims_admission_ethnicity_code { get; set; }
    //    public string sims_admission_family_name_en { get; set; }
    //    public string sims_admission_family_name_ot { get; set; }
    //    public string sims_admission_father_appartment_number { get; set; }
    //    public string sims_admission_father_area_number { get; set; }
    //    public string sims_admission_father_building_number { get; set; }
    //    public string sims_admission_father_city { get; set; }
    //    public string sims_admission_father_city_code { get; set; }
    //    public string sims_admission_father_company { get; set; }
    //    public string sims_admission_father_country_code { get; set; }
    //    public string sims_admission_father_email { get; set; }
    //    public string sims_admission_father_family_name { get; set; }
    //    public string sims_admission_father_fax { get; set; }
    //    public string sims_admission_father_first_name { get; set; }
    //    public string sims_admission_father_last_name { get; set; }
    //    public string sims_admission_father_middle_name { get; set; }
    //    public string sims_admission_father_full_name { get; set; }
    //    public string sims_admission_father_mobile { get; set; }
    //    public string sims_admission_father_name_ot { get; set; }
    //    public string sims_admission_father_nationality1_code { get; set; }
    //    public string sims_admission_father_nationality2_code { get; set; }
    //    public string sims_admission_father_occupation { get; set; }

    //    public string sims_admission_father_passport_number { get; set; }
    //    public string sims_parent_father_passport_issue_date { get; set; }
    //    public string sims_parent_father_passport_expiry_date { get; set; }
    //    public string sims_parent_father_national_id { get; set; }
    //    public string sims_parent_father_national_id_issue_date { get; set; }
    //    public string sims_parent_father_national_id_expiry_date { get; set; }


    //    public string sims_admission_father_phone { get; set; }
    //    public string sims_admission_father_po_box { get; set; }
    //    public string sims_admission_father_salutation_code { get; set; }
    //    public string sims_admission_father_state { get; set; }
    //    public string sims_admission_father_state_code { get; set; }
    //    public string sims_admission_father_street_number { get; set; }
    //    public string sims_admission_father_summary_address { get; set; }
    //    public string sims_admission_fee_payment_contact_pref { get; set; }
    //    public string sims_admission_fees_paid_status { get; set; }
    //    public string sims_admission_gender { get; set; }
    //    public string sims_admission_gender_name { get; set; }
    //    public string sims_admission_gifted_desc { get; set; }
    //    public string sims_admission_gifted_status { get; set; }
    //    public string sims_admission_grade_code { get; set; }
    //    public string sims_admission_guardian_appartment_number { get; set; }
    //    public string sims_admission_guardian_area_number { get; set; }
    //    public string sims_admission_guardian_building_number { get; set; }
    //    public string sims_admission_guardian_city { get; set; }
    //    public string sims_admission_guardian_company { get; set; }
    //    public string sims_admission_guardian_country_code { get; set; }
    //    public string sims_admission_guardian_email { get; set; }
    //    public string sims_admission_guardian_family_name { get; set; }
    //    public string sims_admission_guardian_fax { get; set; }
    //    public string sims_admission_guardian_full_name { get; set; }
    //    public string sims_admission_guardian_first_name { get; set; }
    //    public string sims_admission_guardian_last_name { get; set; }
    //    public string sims_admission_guardian_middle_name { get; set; }
    //    public string sims_admission_guardian_mobile { get; set; }
    //    public string sims_admission_guardian_name_ot { get; set; }
    //    public string sims_admission_guardian_nationality1_code { get; set; }
    //    public string sims_admission_guardian_nationality2_code { get; set; }
    //    public string sims_admission_guardian_occupation { get; set; }

    //    public string sims_admission_guardian_passport_number { get; set; }
    //    public string sims_parent_guardian_passport_issue_date { get; set; }
    //    public string sims_parent_guardian_passport_expiry_date { get; set; }
    //    public string sims_parent_guardian_national_id { get; set; }
    //    public string sims_parent_guardian_national_id_issue_date { get; set; }
    //    public string sims_parent_guardian_national_id_expiry_date { get; set; }

    //    public string sims_admission_guardian_phone { get; set; }
    //    public string sims_admission_guardian_po_box { get; set; }
    //    public string sims_admission_guardian_relationship_code { get; set; }
    //    public string sims_admission_guardian_salutation_code { get; set; }
    //    public string sims_admission_guardian_state { get; set; }
    //    public string sims_admission_guardian_street_number { get; set; }
    //    public string sims_admission_guardian_summary_address { get; set; }
    //    public string sims_admission_health_card_expiry_date { get; set; }
    //    public string sims_admission_health_card_issue_date { get; set; }
    //    public string sims_admission_health_card_issuing_authority { get; set; }
    //    public string sims_admission_health_card_number { get; set; }
    //    public string sims_admission_health_hearing_desc { get; set; }
    //    public string sims_admission_health_hearing_status { get; set; }
    //    public string sims_admission_health_other_desc { get; set; }
    //    public string sims_admission_health_other_status { get; set; }
    //    public string sims_admission_health_restriction_desc { get; set; }
    //    public string sims_admission_health_restriction_status { get; set; }
    //    public string sims_admission_health_vision_desc { get; set; }
    //    public string sims_admission_health_vision_status { get; set; }
    //    public string sims_admission_ip { get; set; }
    //    public string sims_admission_language_support_desc { get; set; }
    //    public string sims_admission_language_support_status { get; set; }
    //    public string sims_admission_legal_custody { get; set; }
    //    public string sims_admission_legal_custody_name { get; set; }
    //    public string sims_admission_main_language_code { get; set; }
    //    public string sims_admission_main_language_m { get; set; }
    //    public string sims_admission_main_language_r { get; set; }
    //    public string sims_admission_main_language_s { get; set; }
    //    public string sims_admission_main_language_w { get; set; }
    //    public string sims_admission_marketing_code { get; set; }
    //    public string sims_admission_marketing_description { get; set; }
    //    public string sims_admission_medication_desc { get; set; }
    //    public string sims_admission_medication_status { get; set; }
    //    public string sims_admission_mother_appartment_number { get; set; }
    //    public string sims_admission_mother_area_number { get; set; }
    //    public string sims_admission_mother_building_number { get; set; }
    //    public string sims_admission_mother_city { get; set; }
    //    public string sims_admission_mother_company { get; set; }
    //    public string sims_admission_mother_country_code { get; set; }
    //    public string sims_admission_mother_email { get; set; }
    //    public string sims_admission_mother_family_name { get; set; }
    //    public string sims_admission_mother_fax { get; set; }
    //    public string sims_admission_mother_full_name { get; set; }
    //    public string sims_admission_mother_first_name { get; set; }
    //    public string sims_admission_mother_last_name { get; set; }
    //    public string sims_admission_mother_middle_name { get; set; }
    //    public string sims_admission_mother_mobile { get; set; }
    //    public string sims_admission_mother_name_ot { get; set; }
    //    public string sims_admission_mother_nationality1_code { get; set; }
    //    public string sims_admission_mother_nationality2_code { get; set; }
    //    public string sims_admission_mother_occupation { get; set; }

    //    public string sims_admission_mother_passport_number { get; set; }
    //    public string sims_parent_mother_passport_issue_date { get; set; }
    //    public string sims_parent_mother_passport_expiry_date { get; set; }
    //    public string sims_parent_mother_national_id { get; set; }
    //    public string sims_parent_mother_national_id_issue_date { get; set; }
    //    public string sims_parent_mother_national_id_expiry_date { get; set; }

    //    public string sims_admission_mother_phone { get; set; }
    //    public string sims_admission_mother_po_box { get; set; }
    //    public string sims_admission_mother_salutation_code { get; set; }
    //    public string sims_admission_mother_state { get; set; }
    //    public string sims_admission_mother_street_number { get; set; }
    //    public string sims_admission_mother_summary_address { get; set; }
    //    public string sims_admission_music_desc { get; set; }
    //    public string sims_admission_music_status { get; set; }
    //    public string sims_admission_national_id { get; set; }
    //    public string sims_admission_national_id_expiry_date { get; set; }
    //    public string sims_admission_national_id_issue_date { get; set; }
    //    //  public string sims_admission_national_id_expiry_date { get; set; }
    //    // public string sims_admission_national_id_issue_date { get; set; }
    //    public string sims_admission_nationality_code { get; set; }
    //    public string sims_admission_nickname { get; set; }
    //    public string sims_admission_number { get; set; }
    //    public string sims_admission_other_language { get; set; }
    //    public string sims_admission_parent_id { get; set; }
    //    public string sims_admission_parent_status_code { get; set; }
    //    public string sims_admission_passport_expiry_date { get; set; }
    //    public string sims_admission_passport_first_name_en { get; set; }
    //    public string sims_admission_passport_first_name_ot { get; set; }
    //    public string sims_admission_passport_issue_date { get; set; }
    //    public string sims_admission_passport_issue_place { get; set; }
    //    public string sims_admission_passport_issuing_authority { get; set; }
    //    public string sims_admission_passport_last_name_en { get; set; }
    //    public string sims_admission_passport_last_name_ot { get; set; }
    //    public string sims_admission_passport_middle_name_en { get; set; }
    //    public string sims_admission_passport_middle_name_ot { get; set; }
    //    public string sims_admission_passport_number { get; set; }
    //    public string sims_admission_primary_contact_code { get; set; }
    //    public string sims_admission_primary_contact_pref { get; set; }
    //    public string sims_admission_pros_application_number { get; set; }
    //    public string sims_admission_pros_number { get; set; }
    //    public string sims_admission_religion_code { get; set; }
    //    public string sims_admission_school_code { get; set; }
    //    public string sims_admission_sibling_dob { get; set; }
    //    public string sims_admission_sibling_enroll_number { get; set; }
    //    public string sims_admission_sibling_name { get; set; }
    //    public string sims_admission_sibling_school_code { get; set; }
    //    public string sims_admission_sibling_status { get; set; }
    //    public string sims_admission_sports_desc { get; set; }
    //    public string sims_admission_sports_status { get; set; }
    //    public string sims_admission_status { get; set; }
    //    public DateTime? sims_admission_tentative_joining_date { get; set; }
    //    public string sims_admission_term_code { get; set; }
    //    public string sims_admission_transport_desc { get; set; }
    //    public string sims_admission_transport_status { get; set; }
    //    public string sims_admission_user_code { get; set; }
    //    public string sims_admission_visa_expiry_date { get; set; }
    //    public string sims_admission_visa_issue_date { get; set; }
    //    //public string sims_admission_visa_expiry_date { get; set; }
    //    //public string sims_admission_visa_issue_date { get; set; }
    //    public string sims_admission_visa_issuing_authority { get; set; }
    //    public string sims_admission_visa_issuing_place { get; set; }
    //    public string sims_admission_visa_number { get; set; }
    //    public string sims_admission_visa_type { get; set; }
    //    public string sims_admission_section_code { get; set; }
    //    public string sims_admission_fee_category { get; set; }
    //    public string sims_admission_fee_category_code { get; set; }

    //    //student login
    //    public string sims_admisison_student_user_sequnce_code { get; set; }
    //    public string sims_admisison_student_user_password { get; set; }
    //    public string sims_admisison_student_user_group_code { get; set; }
    //    public string sims_admisison_student_user_date_created { get; set; }

    //    //parent details-login
    //    public string sims_admisison_parent_sequnce_code { get; set; }
    //    public string sims_admisison_parent_user_sequnce_code { get; set; }
    //    public string sims_admisison_parent_user_password { get; set; }
    //    public string sims_admisison_parent_user_group_code { get; set; }
    //    public string sims_admisison_parent_user_date_created { get; set; }

    //    //student image
    //    public string sims_admisison_student_image { get; set; }
    //    //Parent image
    //    public string sims_admisison_father_image { get; set; }
    //    //mother
    //    public string sims_admisison_mother_image { get; set; }
    //    //guardian
    //    public string sims_admisison_guardian_image { get; set; }

    //    public string active_member { get; set; }


    //    public string lic_school_code { get; set; }

    //    public string lic_school_name { get; set; }

    //    public string sims_cur_code { get; set; }

    //    public string sims_cur_full_name_en { get; set; }

    //    public string sims_country_code { get; set; }

    //    public string sims_country_name_en { get; set; }

    //    public string sims_nationality_code { get; set; }

    //    public string sims_nationality_name_en { get; set; }

    //    public string sims_language_code { get; set; }

    //    public string sims_language_name_en { get; set; }

    //    public string sims_ethnicity_code { get; set; }

    //    public string sims_ethnicity_name_en { get; set; }

    //    public string sims_religion_code { get; set; }

    //    public string sims_religion_name_en { get; set; }

    //    public string sims_appl_parameter { get; set; }

    //    public string sims_appl_form_field_value1 { get; set; }

    //    public string sims_cur_short_name_en { get; set; }

    //    public string sims_academic_year { get; set; }

    //    public string sims_grade_name_en { get; set; }

    //    public string sims_term_desc_en { get; set; }

    //    public string sims_section_name_en { get; set; }

    //    public string sims_appl_form_field { get; set; }

    //    public string sims_student_legal_custody { get; set; }

    //    public string sims_admission_application_number { get; set; }

    //    public string sims_admission_academic_year { get; set; }

    //    public string sims_admission_birth_country_code { get; set; }

    //    public string sims_admission_behaviour_status { get; set; }

    //    public string sims_admission_behaviour_desc { get; set; }

    //    public string sims_admission_doc_admission_number { get; set; }

    //    public string sims_admission_doc_cur_code { get; set; }

    //    public string sims_admission_doc_academic_year { get; set; }

    //    public string sims_admission_doc_section_code { get; set; }

    //    public string sims_admission_doc_grade_code { get; set; }

    //    public string sims_admission_doc_criteria_code { get; set; }

    //    public string sims_admission_doc_path { get; set; }

    //    public string sims_admission_doc_status { get; set; }

    //    public string sims_admission_doc_verify { get; set; }

    //    public string sims_criteria_name_en { get; set; }

    //    public string student_full_name { get; set; }

    //    public string student_full_name_ot { get; set; }

    //    public string sims_student_gender { get; set; }

    //    public string pp001_student_dob { get; set; }

    //    public string sims_student_birth_country_code { get; set; }

    //    public string student_main_language_code { get; set; }

    //    public string student_other_language { get; set; }

    //    public string student_language_prof_code { get; set; }

    //    public string student_language_prof_desc { get; set; }

    //    public string student_primary_contact_pref { get; set; }

    //    public string sims_student_fee_payment_contact_pref { get; set; }

    //    public string student_transport_status { get; set; }

    //    public string student_emergency_contact_name1 { get; set; }

    //    public string student_emergency_contact_number1 { get; set; }

    //    public string student_emergency_contact_name2 { get; set; }

    //    public string student_emergency_contact_number2 { get; set; }

    //    public string student_date { get; set; }

    //    public string student_commence_date { get; set; }

    //    public string student_remark { get; set; }

    //    public string student_current_school_code { get; set; }

    //    public string student_last_login { get; set; }

    //    public string student_academic_status { get; set; }

    //    public string student_class_rank { get; set; }

    //    public string sims_student_ea_number { get; set; }

    //    public string sims_student_ea_registration_date { get; set; }

    //    public string sims_student_ea_transfer { get; set; }

    //    public string student_house { get; set; }

    //    public string sims_student_ea_status { get; set; }

    //    public string sims_student_passport_number { get; set; }

    //    public string sims_student_passport_issue_date { get; set; }

    //    public string sims_student_passport_expiry_date { get; set; }

    //    public string sims_student_passport_issuing_authority { get; set; }

    //    public string sims_student_passport_issue_place { get; set; }

    //    public string sims_parent_father_city { get; set; }

    //    public string sims_parent_father_state { get; set; }

    //    public string sims_parent_country_code { get; set; }

    //    public string sims_parent_father_po_box { get; set; }

    //    public string sims_parent_father_phone { get; set; }

    //    public string sims_parent_father_mobile { get; set; }

    //    public string sims_parent_father_email { get; set; }

    //    public string sims_parent_father_fax { get; set; }

    //    public string sims_student_section_strength { get; set; }

    //    public string sims_parent_number { get; set; }

    //    public string sims_parent_father_occupation_local_language { get; set; }

    //    public string sims_parent_father_occupation_location_local_language { get; set; }

    //    public string sims_parent_mother_occupation_local_language { get; set; }

    //    public string sims_parent_guardian_occupation_local_language { get; set; }

    //    public string sims_parent_guardian_occupation_location_local_language { get; set; }

    //    public string sims_parent_guardian_relationship_code { get; set; }

    //    public string sims_parent_is_employment_status { get; set; }

    //    public string sims_parent_is_employement_comp_code { get; set; }

    //    public string sims_parent_is_employment_number { get; set; }

    //    public string sims_student_nationality_code { get; set; }
    //    public string sims_student_nationality { get; set; }



    //    public string sims_academic_year_description { get; set; }
    //}
    //#endregion


    public class feedback
    {
        public string sims_feedback_code { get; set; }
        public string sims_feedback_type { get; set; }
        public string sims_feedback_subject { get; set; }
        public string sims_feedback_message { get; set; }
        public string sims_feedback_rating { get; set; }
        public string sims_feedback_user_code { get; set; }
        public string sims_feedback_user_code_from { get; set; }
        public string sims_feedback_date { get; set; }
        public string sims_feedback_acknowledge_status { get; set; }
        public string sims_feedback_acknowledge_date { get; set; }
        public string sims_feedback_status { get; set; }
    }


    #region Attendance
    public class PP002
    {
        public string sims_enrollment_number { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_att_code { get; set; }
        public string sims_att_desc { get; set; }
        public string sims_att_comment { get; set; }

        public string sims_attendance_color_code { get; set; }
    }
    #endregion

    public class comnAlert
    {

        public string Message_Type { get; set; }

        public string User_Message { get; set; }

        public string comn_alert_date { get; set; }

        public string Message_Sender { get; set; }

        public string Message_Status { get; set; }
    }

    public class ResetPassowrd
    {
        public string OldPasscode { get; set; }
        public string NewPasscode { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }

    public class UserGroup
    {
        public string Comn_User_Group_Code { get; set; }
        public string Comn_User_Group_Name { get; set; }

        public string user_group_code { get; set; }

        public string user_group_name { get; set; }
    }

    public class Country
    {
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string sims_country_name_ar { get; set; }
        public string sims_country_name_fr { get; set; }
        public string sims_country_name_ot { get; set; }
        public string sims_continent { get; set; }
        public string sims_region_code { get; set; }
        public string sims_region_code_name { get; set; }
        public string sims_currency_code { get; set; }
        public string sims_currency_code_name { get; set; }
        public string sims_currency_dec { get; set; }
        public string sims_country_img { get; set; }
        public bool sims_status { get; set; }
    }    

    #region
    public class Sims511
    {
        public string sims_assignment_number { get; set; }
        public string sims_assignment_subject_code { get; set; }
        public string sims_assignment_title { get; set; }
        public string sims_assignment_desc { get; set; }
        public string sims_assignment_start_date { get; set; }
        public string sims_assignment_submission_date { get; set; }
        public string sims_assignment_freeze_date { get; set; }
        public string sims_assignment_status { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_assignment_enroll_number { get; set; }
        public string sims_assignment_student_line_no { get; set; }
        public string sims_assignment_student_doc_path { get; set; }
        public string sims_assignment_student_doc_date { get; set; }
        public string sims_assignment_student_doc_teacher_remark { get; set; }
        public bool sims_assignment_student_doc_status { get; set; }
        public bool sims_assignment_student_doc_verify { get; set; }
        public string Stud_code { get; set; }
        public string sims_assignment_teacher_code { get; set; }
        public string Stud_name { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_subject_name { get; set; }
    }
    #endregion

    public class UserRole
    {
        public string Comn_User_Role_Code { get; set; }
        public string Comn_User_Role_Name { get; set; }
        public string Comn_User_Role_Status { get; set; }

        public bool help { get; set; }

        public bool video_help { get; set; }

        public bool dependancy { get; set; }
    }

    public class Language
    {
        public string sims_Language_Code { get; set; }
        public string sims_Language_Name_En { get; set; }
        public string sims_Language_Name_Ar { get; set; }
        public string sims_Language_Name_Fr { get; set; }
        public string sims_Language_Name_Ot { get; set; }
        public string sims_Language_Status { get; set; }
    }

   /* #region Sims008(Nationality)
    public class Sims008
    {
        public string nationality_code { get; set; }
        public string nationality_country_code { get; set; }
        public string nationality_country_name { get; set; }
        public string nationality_name_eng { get; set; }
        public string nationality_name_ar { get; set; }
        public string nationality_name_fr { get; set; }
        public string nationality_name_ot { get; set; }
        public bool nationality_status { get; set; }

        public object opr { get; set; }
    }
    #endregion
    */
    

    //#region  Sims055(State)
    //public class Sims055
    //{
    //    public string sims_state_code { get; set; }
    //    public string sims_country_code { get; set; }
    //    public string sims_country_code_name { get; set; }
    //    public string sims_state_name_en { get; set; }
    //    public string sims_state_name_ar { get; set; }
    //    public string sims_state_name_fr { get; set; }
    //    public string sims_state_name_ot { get; set; }
    //    public bool sims_state_status { get; set; }
    //}

    //#endregion

    #region Sims056(City)
    public class Sims056
    {
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string city_code { get; set; }
        public string state_code { get; set; }
        public string state_name { get; set; }
        public string city_name_eng { get; set; }
        public string city_name_ar { get; set; }
        public string city_name_fr { get; set; }
        public string city_name_ot { get; set; }
        public bool city_status { get; set; }

    }
    #endregion

    #region Document_Master
    public class Sims020
    {
        public string _sims_cur_cur_name
        {
            get;
            set;
        }
        public bool _sims_doc_status
        {
            get;
            set;
        }

        public string _sims_cur_code
        {
            get;
            set;
        }

        public string _sims_doc_mod_code
        {
            get;
            set;
        }

        public string _sims_doc_code
        {
            get;
            set;
        }

        public string _sims_doc_desc
        {
            get;
            set;
        }

        public DateTime? _sims_doc_create_date
        {
            get;
            set;
        }

        public string _sims_doc_created_by
        {
            get;
            set;
        }

        public string _sims_mod_name
        {
            get;
            set;
        }
        public string pays_doc_mod_code { get; set; }
        public string pays_doc_code { get; set; }
        public string pays_doc_desc { get; set; }
        public string pays_doc_create_date { get; set; }
        public string pays_doc_created_by { get; set; }
        public bool pays_doc_status { get; set; }
        public string opr { get; set; }
    }

    #endregion

    //#region Sims083 (Transport Route)
    //public class Sims083
    //{
    //    public string sims_academic_year { get; set; }
    //    public string sims_transport_route_code { get; set; }
    //    public string sims_transport_route_direction { get; set; }
    //    public string sims_transport_route_direction_name { get; set; }
    //    public string sims_transport_route_short_name { get; set; }
    //    public string sims_transport_route_name { get; set; }
    //    public string sims_transport_route_vehicle_code { get; set; }
    //    public string sims_transport_route_vehicle_codename { get; set; }
    //    public string sims_transport_route_driver_code { get; set; }
    //    public string sims_transport_route_driver_codename { get; set; }
    //    public string sims_transport_route_caretaker_code { get; set; }
    //    public string sims_transport_route_caretaker_codename { get; set; }
    //    public bool sims_transport_route_status { get; set; }
    //    public string sims_driver_code { get; set; }
    //    public string sims_driver_name { get; set; }
    //}
    //#endregion

    #region Sims093(Medical Examination)
    public class Sims093
    {
        public string opr { get; set; }
        public string sims_visit_number { get; set; }
        public int sims_serial_number { get; set; }
        public string sims_examination_code { get; set; }
        public string sims_examination_name { get; set; }
        public string sims_examination_value { get; set; }
    }
    #endregion

    #region Sims094(Medical Immunization)
    public class Sims094
    {
        public string sims_enroll_number { get; set; }
        public int sims_immunization_code { get; set; }
        // public string sims_immunization_code { get; set; }
        public string sims_immunization_age_group_name { get; set; }
        public string sims_immunization_age_group_code { get; set; }
        public string sims_immunization_student_code { get; set; }
        public string sims_vaccination_date { get; set; }
        public string sims_vaccination_renewal_date { get; set; }
        public string sims_immunization_desc { get; set; }
        public string sims_immunization_dosage1 { get; set; }
        public string sims_immunization_dosage2 { get; set; }
        public string sims_immunization_dosage3 { get; set; }
        public string sims_immunization_dosage4 { get; set; }
        public string sims_immunization_dosage5 { get; set; }
        public bool sims_immunization_status { get; set; }
    }
    #endregion

    #region sims028(Honour)


    public class subject_excel_sheet
    {
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_mark { get; set; }
        public string sims_gb_cat_assign_max_score { get; set; }
    }

    public class enar
    {
        public string sims_english { get; set; }

        public string sims_arabic { get; set; }
    }

    public class age
    {
        public string sims_birth_date_from { get; set; }

        public string sims_birth_date_to { get; set; }

        public string referal_code { get; set; }

        public string referal_name { get; set; }
    }
    public class Sims028
    {

        public List<subject_excel_sheet> subjects = new List<subject_excel_sheet>();
        public string sims_academic_year { get; set; }
        public string sims_honour_enroll_number { get; set; }
        public string sims_honour_student_first_name { get; set; }
        public string sims_honour_grade { get; set; }
        public string sims_honour_student_middle_name { get; set; }
        public string sims_honour_student_last_name { get; set; }
        public string sims_honour_student_name { get; set; }
        public string sims_honour_code { get; set; }
        public string sims_honour_cur_code { get; set; }
        public string sims_honour_academic_year { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }


        public string sims_student_honour_roll { get; set; }

        public string screening_grade_code { get; set; }

        public string sims_level_code { get; set; }

        public string sims_report_card_name { get; set; }

        public string sims_report_card_code { get; set; }

        public string sims_report_card_description { get; set; }

        public string sims_report_card_level { get; set; }

        public string sims_report_card_level_name { get; set; }

        public string sims_gb_cat_code { get; set; }

        public string sims_gb_cat_assign_number { get; set; }

        public string sims_gb_cat_assign_name { get; set; }

        public string sims_gb_number { get; set; }

        public string sims_gb_cat_name { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_subject_group_code { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string report_name { get; set; }

        public string sims_gb_name { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_gb_cat_assign_max_score { get; set; }

        public string sims_gb_cat_assign_mark { get; set; }

        public string opr { get; set; }

        public string count { get; set; }

        public string ac_year { get; set; }

        public bool ac__year_flag { get; set; }

        public string cur_code { get; set; }

        public bool cur_code_flag { get; set; }

        public string grade_code { get; set; }

        public bool grade_code_flag { get; set; }

        public string section_code { get; set; }

        public bool section_code_flag { get; set; }

        public string gb_number { get; set; }

        public bool gb_number_flag { get; set; }

        public string enroll_number { get; set; }

        public bool enroll_number_flag { get; set; }

        public string sims_cur_name_en { get; set; }

        public bool max_score_flag { get; set; }

        public string max_score { get; set; }

        public string assign_name { get; set; }

        public bool assign_name_flag { get; set; }
    }
    #endregion
}

public class Sims0288
{
    public string comp_code{ get; set; }
    public string comp_curcy_code{ get; set; }
    public string comp_curcy_dec { get; set; }
}

public class details
{
    public string firstName { get; set; }
    public string surName { get; set; }
    public string emailAddress { get; set; }
    public string city { get; set; }
    public string country { get; set; }
    public string grade { get; set; }
    public string userPicture { get; set; }
    public string schoolName { get; set; }
    public string boardName { get; set; }

}
