﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.Attendance
{
    public class AttendanceCommon
    {

        public string sims_calendar_exception_description { get; set; }
        public string exception_type { get; set; }
        public string sims_from_date { get; set; }
        public string sims_to_date { get; set; }
        public string no_of_days { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string comn_appl_parameter { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        
        
            //attendance not marked	
	
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }


        //variance report for teacher daily attendance
      
        public string grade { get; set; }
        public string section { get; set; }
        public string sims_employee_code { get; set; }
        public string class_teacher { get; set; }
        public string att_teacher { get; set; }
        public string sims_attendance_day_comment { get; set; }
        public string sims_attendance_day_teacher_code { get; set; }

        //student alert notification

        public string sims_enrollment_number { get; set; }
        public string student_name { get; set; }
        public string class1 { get; set; }
        public string comn_alert_message { get; set; }
        public string comn_alert_date { get; set; }
        public string time { get; set; }
        public string attendance_by { get; set; }
        public string notification_send_by { get; set; }
        public string emp_id { get; set; }

        //Fire Register
        public string sims_attendance_day_attendance_code { get; set; }
        public string sims_attendance_day_am_attendance_code { get; set; }
        public string sims_attendance_day_pm_attendance_code { get; set; }
        public string out_of_school { get; set; }


        //attendance summary
        public string sims_attendance_code { get; set; }
        public string sims_attendance_description { get; set; }
        //public string sims_attendance_day_pm_attendance_code { get; set; }
        //public string out_of_school { get; set; }

      // student leave slip


//Merit Demerit form

        public string sims_detention_level_code { get; set; }
        public string sims_detention_desc_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string class_name { get; set; }
        public string sims_detention_name { get; set; }
        public string sims_detention_description { get; set; }
        public string sims_detention_point { get; set; }
        public string sims_detention_remark { get; set; }
        public string date { get; set; }
        public string reg_by { get; set; }
        public object sims_acad_year { get; set; }
        public object sims_slot_code { get; set; }
        public object issued_by { get; set; }
        public object remark { get; set; }
        public object att_time { get; set; }
        public string sims_detention_transaction_number { get; set; }
        public string sims_detention_transaction_created_date { get; set; }
        public string demerit_points { get; set; }
        public string merit_points { get; set; }
        public string student_arabic_name { get; set; }
    }
}