﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.AttendanceClass
{
    public class AttendanceClass
    {
    }
    public class AttendanceCodes
    {

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string sims_category_code { get; set; }

        public string sims_attendance_code { get; set; }

        public string sims_attendance_code_numeric { get; set; }

        public string sims_attendance_short_desc { get; set; }

        public string sims_attendance_description { get; set; }

        public string sims_attendance_present_value { get; set; }

        public string sims_attendance_absent_value { get; set; }

        public string sims_attendance_tardy_value { get; set; }

        public string sims_attendance_type_value { get; set; }

        public bool sims_attendance_unexcused_flag { get; set; }

        public bool sims_attendance_teacher_can_use_flag { get; set; }

        public string sims_attendance_color_code { get; set; }

        public bool sims_attendance_status { get; set; }

        public string sims_attendance_status_inactive_date { get; set; }

        public string colorCode { get; set; }

        public object opr { get; set; }
    }

    public class classwiseattendance
    {
        public string sims_cur_code { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string stuStandard { get; set; }

        public object Enrollment_list { get; set; }

        public string attendance_code { get; set; }

        public string attedance_date { get; set; }
    }

    public class studentattendance
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_cur_full_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_attendance_type_code { get; set; }
        public string sims_attendance_type_name { get; set; }
        public string sims_attendance_type_details_code { get; set; }
        public string sims_attendance_type_details_name { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_attendance_sort_code { get; set; }
        public string sims_attendance_sort_name { get; set; }

        public string sims_attendance_code { get; set; }

        public string sims_attendance_code_sort_name { get; set; }

        public string sims_attendance_code_name { get; set; }

        public string sims_student_name { get; set; }

        public string sims_student_fname { get; set; }

        public string sims_student_nickname { get; set; }

        public string sims_student_img { get; set; }

        public string sims_student_enroll { get; set; }

        public string sims_attendance_color { get; set; }

        public bool sims_student_bday { get; set; }

        public string homeroombatch { get; set; }
        public string sims_homeroom_code { get; set; }
        public string sims_batch_code { get; set; }
        public string attednacedate { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_bell_subject_code { get; set; }

        public bool sims_isholiday { get; set; }

        public string sims_bus_number { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_class_name { get; set; }

        public string sims_present { get; set; }

        public string sims_absent { get; set; }

        public string sims_absent_excused { get; set; }

        public string sims_Tardy { get; set; }

        public string sims_Unmarked { get; set; }

        public string sims_total { get; set; }

        public string sims_TotalAttendanceDays { get; set; }

        public string sims_Percentage { get; set; }
        public string sims_category_code { get; internal set; }
        public string sims_from_date { get;  set; }
        public string sims_attendance_day_comment { get;  set; }

        public string fmobile { get; set; }

        public string mmobile { get; set; }

        public string sims_enrollment_number { get; set; }

        public string std_name { get; set; }

        public string attendace_code { get; set; }

        public string subject_code { get; set; }

        public string teacher_code { get; set; }

        public string slot { get; set; }

        public string comment { get; set; }

        public string sims_att_color_code { get; set; }

        public string sims_homeroom_name { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string HomeroomEmployeeCode { get; set; }

        public string teacher_name { get; set; }

        public string slot_name { get; set; }

        public string sims_roll_number { get; set; }

        public int sims_enrollment_number1 { get; set; }

        public int sims_roll_number1 { get; set; }

        public string name { get; set; }

        public string code { get; set; }

        public string absent_excused { get; set; }
        public string present { get; set; }
        public string absent { get; set; }
        public string late { get; set; }
        public string late_excused { get; set; }

        public string unmarked { get; set; }

        public string not_applicable { get; set; }

        public string weeked { get; set; }

        public string holiday { get; set; }

        public string leave_early { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_appl_form_field_value1 { get; set; }
    }


    public class studentattendance_new
    {

        public object cur_code { get; set; }

        public object ayear { get; set; }

        public object grade { get; set; }

        public object section { get; set; }

        public object enrollNumber { get; set; }

        public object att_code { get; set; }

        public string attednacedate { get; set; }

        public string user_name { get; set; }
    }

    public class studentattendance_rpt
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_class_name { get; set; }
        public string sims_student_name { get; set; }
        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        

        public string sims_present { get; set; }

        public string sims_absent { get; set; }

        public string sims_absent_excused { get; set; }

        public string sims_Tardy { get; set; }

        public string sims_Unmarked { get; set; }

        public string sims_total { get; set; }

        public string sims_TotalAttendanceDays { get; set; }

        public string sims_Percentage { get; set; }

        public string sims_category_code { get; set; }

        public string sims_from_date { get; set; }

        public string sims_attendance_day_comment { get; set; }
    }

    public class studentmonthlyattendance
    {
        public List<studentmonthlyattendance_sub> sublist1 = new List<studentmonthlyattendance_sub>();

        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public string sims_term_name { get; set; }
        public string sims_month_no { get; set; }
        public string sims_month_name { get; set; }
        public string sims_year { get; set; }

        public string sims_student_name { get; set; }
        public string sims_enroll_number { get; set; }

        //Attendance Codes
        public string attendance_day1_code { get; set; }
        public string attendance_day2_code { get; set; }
        public string attendance_day3_code { get; set; }
        public string attendance_day4_code { get; set; }
        public string attendance_day5_code { get; set; }
        public string attendance_day6_code { get; set; }
        public string attendance_day7_code { get; set; }
        public string attendance_day8_code { get; set; }
        public string attendance_day9_code { get; set; }
        public string attendance_day10_code { get; set; }
        public string attendance_day11_code { get; set; }
        public string attendance_day12_code { get; set; }
        public string attendance_day13_code { get; set; }
        public string attendance_day14_code { get; set; }
        public string attendance_day15_code { get; set; }
        public string attendance_day16_code { get; set; }
        public string attendance_day17_code { get; set; }
        public string attendance_day18_code { get; set; }
        public string attendance_day19_code { get; set; }
        public string attendance_day21_code { get; set; }
        public string attendance_day22_code { get; set; }
        public string attendance_day23_code { get; set; }
        public string attendance_day24_code { get; set; }
        public string attendance_day25_code { get; set; }
        public string attendance_day26_code { get; set; }
        public string attendance_day27_code { get; set; }
        public string attendance_day28_code { get; set; }
        public string attendance_day29_code { get; set; }
        public string attendance_day30_code { get; set; }
        public string attendance_day31_code { get; set; }
        //Attendance Color Codes
        public string attendance_day1_color_code { get; set; }
        public string attendance_day2_color_code { get; set; }
        public string attendance_day3_color_code { get; set; }
        public string attendance_day4_color_code { get; set; }
        public string attendance_day5_color_code { get; set; }
        public string attendance_day6_color_code { get; set; }
        public string attendance_day7_color_code { get; set; }
        public string attendance_day8_color_code { get; set; }
        public string attendance_day9_color_code { get; set; }
        public string attendance_day10_color_code { get; set; }
        public string attendance_day11_color_code { get; set; }
        public string attendance_day12_color_code { get; set; }
        public string attendance_day13_color_code { get; set; }
        public string attendance_day14_color_code { get; set; }
        public string attendance_day15_color_code { get; set; }
        public string attendance_day16_color_code { get; set; }
        public string attendance_day17_color_code { get; set; }
        public string attendance_day18_color_code { get; set; }
        public string attendance_day19_color_code { get; set; }
        public string attendance_day21_color_code { get; set; }
        public string attendance_day22_color_code { get; set; }
        public string attendance_day23_color_code { get; set; }
        public string attendance_day24_color_code { get; set; }
        public string attendance_day25_color_code { get; set; }
        public string attendance_day26_color_code { get; set; }
        public string attendance_day27_color_code { get; set; }
        public string attendance_day28_color_code { get; set; }
        public string attendance_day29_color_code { get; set; }
        public string attendance_day30_color_code { get; set; }
        public string attendance_day31_color_code { get; set; }

        public string Yearno { get; set; }

        public string monthYear { get; set; }

        public string FirstName { get; set; }

        public string NickName { get; set; }

        public string attendance_day20_code { get; set; }

        public string attendance_day20_color_code { get; set; }

        public object sims_attendance_code { get; set; }

        public string attednacedate { get; set; }

        public string Transport { get; set; }
        public string user_status { get; set; }

        public string BusNo { get; set; }

        public object Current_Username { get; set; }

        public bool t { get; set; }

        public string studentDOB { get; set; }

        public string sims_attendance_day_comment { get; set; }

        public string user_name { get; set; }

        public string sims_roll_number { get; set; }

        public int sims_roll_number1 { get; set; }
    }


    public class studentmonthlyattendance_sub
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public string sims_term_name { get; set; }
        public string sims_month_no { get; set; }
        public string sims_month_name { get; set; }
        public string sims_year { get; set; }

        public string sims_student_name { get; set; }
        public string sims_enroll_number { get; set; }

        //Attendance Codes
        public string attendance_day1_code { get; set; }
        public string attendance_day2_code { get; set; }
        public string attendance_day3_code { get; set; }
        public string attendance_day4_code { get; set; }
        public string attendance_day5_code { get; set; }
        public string attendance_day6_code { get; set; }
        public string attendance_day7_code { get; set; }
        public string attendance_day8_code { get; set; }
        public string attendance_day9_code { get; set; }
        public string attendance_day10_code { get; set; }
        public string attendance_day11_code { get; set; }
        public string attendance_day12_code { get; set; }
        public string attendance_day13_code { get; set; }
        public string attendance_day14_code { get; set; }
        public string attendance_day15_code { get; set; }
        public string attendance_day16_code { get; set; }
        public string attendance_day17_code { get; set; }
        public string attendance_day18_code { get; set; }
        public string attendance_day19_code { get; set; }
        public string attendance_day21_code { get; set; }
        public string attendance_day22_code { get; set; }
        public string attendance_day23_code { get; set; }
        public string attendance_day24_code { get; set; }
        public string attendance_day25_code { get; set; }
        public string attendance_day26_code { get; set; }
        public string attendance_day27_code { get; set; }
        public string attendance_day28_code { get; set; }
        public string attendance_day29_code { get; set; }
        public string attendance_day30_code { get; set; }
        public string attendance_day31_code { get; set; }
        //Attendance Color Codes
        public string attendance_day1_color_code { get; set; }
        public string attendance_day2_color_code { get; set; }
        public string attendance_day3_color_code { get; set; }
        public string attendance_day4_color_code { get; set; }
        public string attendance_day5_color_code { get; set; }
        public string attendance_day6_color_code { get; set; }
        public string attendance_day7_color_code { get; set; }
        public string attendance_day8_color_code { get; set; }
        public string attendance_day9_color_code { get; set; }
        public string attendance_day10_color_code { get; set; }
        public string attendance_day11_color_code { get; set; }
        public string attendance_day12_color_code { get; set; }
        public string attendance_day13_color_code { get; set; }
        public string attendance_day14_color_code { get; set; }
        public string attendance_day15_color_code { get; set; }
        public string attendance_day16_color_code { get; set; }
        public string attendance_day17_color_code { get; set; }
        public string attendance_day18_color_code { get; set; }
        public string attendance_day19_color_code { get; set; }
        public string attendance_day21_color_code { get; set; }
        public string attendance_day22_color_code { get; set; }
        public string attendance_day23_color_code { get; set; }
        public string attendance_day24_color_code { get; set; }
        public string attendance_day25_color_code { get; set; }
        public string attendance_day26_color_code { get; set; }
        public string attendance_day27_color_code { get; set; }
        public string attendance_day28_color_code { get; set; }
        public string attendance_day29_color_code { get; set; }
        public string attendance_day30_color_code { get; set; }
        public string attendance_day31_color_code { get; set; }

        public string Yearno { get; set; }

        public string monthYear { get; set; }

        public string FirstName { get; set; }

        public string NickName { get; set; }

        public string attendance_day20_code { get; set; }

        public string attendance_day20_color_code { get; set; }

        public object sims_attendance_code { get; set; }

        public string attednacedate { get; set; }

        public string Transport { get; set; }
        public string user_status { get; set; }

        public string BusNo { get; set; }

        public object Current_Username { get; set; }

        public bool t { get; set; }

        public string studentDOB { get; set; }

        public string sims_attendance_day_comment { get; set; }

        public string user_name { get; set; }

        public string slot_code { get; set; }
    }

    public class subject1
    {
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_attedance_slot { get; set; }

        public string sims_attedance_code { get; set; }
        public string sims_att_color_code { get; set; }

    }


    public class subject_wise_attendance
    {

        public List<subject1> sublist1 = new List<subject1>();


        public string sims_grade_code { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_attedance_date { get; set; }

        public string sims_teacher_code { get; set; }

        public string sims_attedance_slot { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_student_name { get; set; }

        public string sims_subject_name { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_attedance_code { get; set; }

        public string sims_att_color_code { get; set; }

        public string sims_attendance_day_comment { get; set; }

        public string homeroom_code { get; set; }

        public string type { get; set; }
    }

    public class AttendanceDaily
    {
        public string emp_id { get; set; }

        public string att_date { get; set; }

        public string s1in { get; set; }

        public string s1out { get; set; }

        public string comment { get; set; }

        public string att_flag { get; set; }

        public string ac_no { get; set; }

        public string no { get; set; }

        public string name { get; set; }

        public string opr { get; set; }
    }

    public class piechart
    {
        public int value { get; set; }
        public string color { get; set; }
        public string highlight { get; set; }
        public string label { get; set; }

    }
    public class attnDash
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_nm { get; set; }

        public string sims_section_code { get; set; }
        public string sims_section_nm { get; set; }

        public string cnt { get; set; }
        public string sims_attendance_date_sd { get; set; }
        public string sims_attendance_date_ed { get; set; }

        public string present { get; set; }
        public string absent { get; set; }
        public string trady { get; set; }
        public string tradyExcused { get; set; }
        public string Unmarked { get; set; }

        public string sims_std_nm { get; set; }
        public string sims_Enroll { get; set; }


        public double Intensity { get; set; }

        public string grade_name_ot { get; set; }
    }

    public class admissionDash
    {

        public string sims_grade_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public string total_stregth { get; set; }

        public string allocated { get; set; }

        public string confirm { get; set; }

        //   public string value { get; set; }

        public string label { get; set; }

        public string cnt { get; set; }

        public string rejected { get; set; }

        public string value { get; set; }
    }

    #region Attendanc Machine class

    public class att_data
    {
        public string att_emp_id { get; set; }
        public string empname { get; set; }
        public string att_date { get; set; }
        public string att_punch_flag { get; set; }
        public string att_mach_id { get; set; }
        public string att_push_flag { get; set; }
        public string att_shift1_in { get; set; }
        public string att_shift1_out { get; set; }
        public string att_sdate { get; set; }
        public string att_edate { get; set; }
        public string att_shift2_in { get; set; }
        public string att_shift2_out { get; set; }
        public string em_login_code { get; set; }

        public object opr { get; set; }
    }
    #endregion

    public class studentattendancerought
    {

        public string sims_transport_route_code { get; set; }

        public string sims_transport_route_name { get; set; }

        public string sims_rought_parameter { get; set; }

        public string sims_rought_field_value1 { get; set; }

        public string sims_transport_parameter { get; set; }

        public string sims_transport_field_value1 { get; set; }

        public string sims_transport_route_direction { get; set; }

        public string sims_transport_route_short_name { get; set; }

        public string sims_transport_route_vehicle_code { get; set; }

        public string sims_transport_route_driver_code { get; set; }

        public string sims_transport_route_caretaker_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_transport_date { get; set; }

        public string sims_transport_enrollment_number { get; set; }

        public string sims_transport_studentname { get; set; }

        public string sims_transport_firstname { get; set; }

        public string sims_transport_middlename { get; set; }

        public string sims_transport_lastname { get; set; }

        public string sims_emergency_contact_number { get; set; }

        public string sims_student_img { get; set; }

        public string sims_attendance_code { get; set; }

        public string sims_attendance_am_pickup_time_flag { get; set; }

        public string sims_attendance_am_drop_time_flag { get; set; }

        public string sims_attendance_pm_pickup_time_flag { get; set; }

        public string sims_attendance_pm_drop_time_flag { get; set; }

        public object opr { get; set; }

        public object subopr { get; set; }

        public object ssubopr { get; set; }

        public object ayear { get; set; }

        public object sims_attendance_pickup_drop { get; set; }

        public object caretaker_code { get; set; }

        public object sims_attendance_pickup_drop_time { get; set; }

        public string currentdate { get; set; }

        public string sims_attendance_code_sort_name { get; set; }

        public string sims_attendance_code_name { get; set; }

        public string sims_attendance_color { get; set; }

        public string attendance_mark_code { get; set; }

        public string attendance_day1_color_code { get; set; }

        public string sims_attendance_remark { get; set; }
    }
}