﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.setupClass
{
    public class setupClass
    {
    }

    public class CalendarException
    {

        public string sims_calendar_exception_number { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_calendar_exception_description { get; set; }

        public string sims_from_date { get; set; }

        public string sims_to_date { get; set; }

        public string sims_calendar_exception_type { get; set; }

        public string sims_calendar_exception_type_name { get; set; }

        public bool sims_calendar_exception_attendance_flag { get; set; }

        public bool sims_calendar_exception_instruction_flag { get; set; }

        public string sims_calendar_exception_day_value { get; set; }

        public bool sims_status { get; set; }

        public string sims_calendar_exception_color_code { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string day_code { get; set; }

        public string day_desc { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string section_name { get; set; }

        public string comn_user_group_code { get; set; }

        public string comn_user_group_name { get; set; }

        public bool sims_calendar_exception_student_flag { get; set; }

        public bool sims_calendar_exception_emp_flag { get; set; }

        public string weekendNames { get; set; }

        public string sims_employee_group { get; set; }

        public string sims_section_code_list { get; set; }

        public string org_from_date { get; set; }

        public string org_to_date { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string total_working_days { get; set; }

        public string sims_attendance_date { get; set; }
        public string sims_calendar_exception_staff_type { get; set; }
    }

    #region Sims026(Holiday_Employee)
    public class Sims026
    {
        public string sims_holiday_number { get; set; }
        public string sims_holiday_desc { get; set; }
        public string sims_employee_group_name { get; set; }
        public string sims_employee_group_code { get; set; }
        public bool sims_holiday_status { get; set; }

        public string sims_employee_group { get; set; }

        public bool sims_status { get; set; }

        public string opr { get; set; }

        public string sims_employee_group_nm { get; set; }
    }
    #endregion


    public class Sims027
    {
        public string _sims_cur_cur_name
        {
            get;
            set;
        }
        public bool _sims_doc_status
        {
            get;
            set;
        }

        public string _sims_holiday_holiday_desc
        {
            get;
            set;
        }
        public string _sims_grade_name
        {
            get;
            set;
        }
        public string _sims_section_name
        {
            get;
            set;
        }


        //for sims_holiday_student
        private string sims_holiday_holiday_desc;
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }



        public string sims_calendar_exception_number { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string opr { get; set; }
    }

   
    #region listbox
    public class listbox
    {
        public string title { get; set; }
        public string value { get; set; }

        public string text { get; set; }
    }
    #endregion

    //Comn006
    public class Comn006
    {
        public string comn_user_code { get; set; }
        public string comn_user_appl_code { get; set; }
        public string comn_user_appl_name { get; set; }
        public string comn_user_role_id { get; set; }
        public string comn_user_role_name { get; set; }
        public string comn_user_appl_fav { get; set; }
        public string comn_user_appl_count { get; set; }
        public bool comn_user_read { get; set; }
        public bool comn_user_insert { get; set; }
        public bool comn_user_update { get; set; }
        public bool comn_user_delete { get; set; }
        public bool comn_user_massupdate { get; set; }
        public bool comn_user_admin { get; set; }
        public bool all { get; set; }
        public bool comn_user_appl_status { get; set; }
        public string comn_role_name { get; set; }


        public string comn_role_code { get; set; }

        public string users { get; set; }
    }


    public class Comn005
    {
        public string Group_Code { get; set; }
        public string User_Name { get; set; }
        public string Enrollment_Number { get; set; }
        public string Employee_code { get; set; }
        public string Email_id { get; set; }
        public string Date_Creation { get; set; }
        public string User_Code { get; set; }
        public string User_password { get; set; }
        public string Expiry_Date { get; set; }
        public string Last_Login { get; set; }
        public bool Captcha_Status { get; set; }
        public bool User_Status { get; set; }
        public string User_State { get; set; }
        public string user_image { get; set; }
        public string userstatue_code { get; set; }
        public string UserAlertCount { get; set; }
        public string UserCircularCount { get; set; }
        public string messageCount { get; set; }

        public string Group_Nm { get; set; }
        public string FullNameEn { get; set; }
        public string Image { get; set; }
        public string date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string comn_user_phone_number { get; set; }
        public string comn_user_alias { get; set; }

        public string Designation { get; set; }
        public string DesigLtr { get; set; }
        public string paglink { get; set; }
        public bool isAdmin { get; set; }
        // Class For comn user
        public string comn_user_code { get; set; }
        public string comn_user_name { get; set; }
        public string comn_user_password { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string comn_user_date_created { get; set; }
        public bool comn_user_captcha_status { get; set; }
        public string comn_user_email { get; set; }
        public string comn_user_secret_question_code { get; set; }
        public string comn_user_secret_question { get; set; }
        public string comn_user_secret_answer { get; set; }
        public string comn_user_status { get; set; }
        public string comn_user_status_name { get; set; }
        public string comn_user_expiry_date { get; set; }
        public string comn_user_remark { get; set; }
        public string opr { get; set; }
    }

    #region album sims078
    public class Sims078
    {
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_album_code { get; set; }
        public string academic_year { get; set; }
        public string sims_album_name { get; set; }
        public string sims_album_description { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_path { get; set; }
        public string sims_album_ImageName { get; set; }
        public string sims_album_photo_description { get; set; }
        public string sims_album_thumbnail_path { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public decimal sims_album_photo_price { get; set; }
        public string sims_album_photo_status { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_album_publish_date { get; set; }
        public string sims_album_expiry_date { get; set; }
        public bool sims_album_visible_to_portal { get; set; }
        public bool sims_album_status { get; set; }
        public string sims_tile_color { get; set; }
        public string Classes { get; set; }
        public string UserGroups { get; set; }
        public List<GRD> ClassList { get; set; }
        public List<UserGroup1> UserGroupList { get; set; }

        public string image { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public bool IsSelected { get; set; }
        public string sims_academic_start_date { get; set; }
        public string sims_academic_end_date { get; set; }

        public string tname { get; set; }

        public string grade { get; set; }
        public string AlbumCount { get; set; }

        public string ClassName { get; set; }

        public string ClassCode { get; set; }

        public string sims_album_photo_url { get; set; }
    }

    public class GRD
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public List<SEC> Sections { get; set; }
    }

    public class SEC
    {
        public string sims_gradeCode_s { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
    }

    public class UserGroup1
    {
        public string user_group_code { get; set; }
        public string user_group_name { get; set; }
    }
    #endregion


    #region album sims078
    public class Sims078Html
    {
        public string sims_album_code { get; set; }
        public string academic_year { get; set; }
        public string sims_album_name { get; set; }
        public string sims_album_description { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_path { get; set; }
        public string sims_album_ImageName { get; set; }
        public string sims_album_photo_description { get; set; }
        public string sims_album_thumbnail_path { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public decimal sims_album_photo_price { get; set; }
        public string sims_album_photo_status { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_album_publish_date { get; set; }
        public string sims_album_expiry_date { get; set; }
        public bool sims_album_visible_to_portal { get; set; }
        public bool sims_album_status { get; set; }
        public string sims_tile_color { get; set; }
        public string Classes { get; set; }
        public string UserGroups { get; set; }

        public string sims_academic_year { get; set; }


        public string image { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public bool IsSelected { get; set; }
        public string sims_academic_start_date { get; set; }
        public string sims_academic_end_date { get; set; }

        public string tname { get; set; }

        public string grade { get; set; }
        public string AlbumCount { get; set; }

        public string ClassName { get; set; }

        public string ClassCode { get; set; }

        public string opr { get; set; }
    }


    public class Sims078HtmlS
    {
           public string sims_album_code{ get; set; }
           public string sims_cur_code{ get; set; }
           public string sims_album_name{ get; set; }
           public string sims_album_description{ get; set; }
           public string sims_album_publish_date{ get; set; }
           public string UserGroups{ get; set; }
           public string Classes{ get; set; }
           public string sims_album_expiry_date{ get; set; }
           public bool sims_album_visible_to_portal{ get; set; }
           public bool sims_album_status{ get; set; }
           public string sims_academic_year{ get; set; }
           public string opr { get; set; }
           public List<string> sims_album_photo_path_lst { get; set; }
    }


    public class Sims078Ht
    {
        public string sims_album_code { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_description { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public string sims_album_photo_price { get; set; }
        public string sims_album_photo_path { get; set; }
        public string opr { get; set; }
        public string sims_album_photo_status { get; set; }
        public List <string> sims_album_photo_path_lst { get; set; }


        public string sims_album_photo_url { get; set; }
    }
    #endregion


    public class country1
    {
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string sims_country_name_ar { get; set; }
        public string sims_country_name_fr { get; set; }
        public string sims_country_name_ot { get; set; }
        public string sims_continent { get; set; }
        public string sims_region_code { get; set; }
        public string sims_region_code_name { get; set; }
        public string sims_currency_code { get; set; }
        public string sims_currency_code_name { get; set; }
        public string sims_currency_dec { get; set; }
        public string sims_country_img { get; set; }
        public bool sims_status { get; set; }
    }

    #region(Pooja)
    //Application
    public class Comn001
    {
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_name_ar { get; set; }
        public string comn_appl_name_fr { get; set; }
        public string comn_appl_name_ot { get; set; }
        public string comn_appl_mod_code { get; set; }
        public string comn_appl_mod_name { get; set; }
        public string comn_app_mode { get; set; }
        public string comn_app_mode_code { get; set; }
        public string comn_appl_type { get; set; }
        public string comn_appl_type_code { get; set; }
        public string comn_appl_keywords { get; set; }
        public bool comn_appl_status { get; set; }
        public string comn_appl_version { get; set; }
        public string comn_appl_location { get; set; }
        public string comn_appl_logic { get; set; }
        public int comn_appl_display_order { get; set; }
        public string comn_appl_tag { get; set; }
        public string comn_appl_tag_code { get; set; }
    }
    #endregion


    #region sims171

    public class Sims171 
    {
     
        public string sims_timetable_number { get; set; }
        public string sims_timetable_filename { get; set; }
        public bool sims_timetable_status { get; set; }

        public string sims_timetable_cur_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_section_name { get; set; }

        public string opr { get; set; }

        public string sims_grade { get; set; }

        public string sims_section { get; set; }

        public string sims_timetable_cur { get; set; }
    }

    #endregion



    #region Sims093(Medical Examination)
    public class Sims093
    {
        public string sims_visit_number { get; set; }
        public string sims_serial_number { get; set; }
        //public string sims_examination_code { get; set; }
        public string sims_examination_name { get; set; }
        public string sims_examination_value { get; set; }

        public string sims_examination_code { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims094(Medical Immunization)
    public class Sims094
    {
        public string sims_enroll_number { get; set; }
        public string sims_immunization_code { get; set; }
        // public string sims_immunization_code { get; set; }
        public string sims_immunization_age_group_name { get; set; }
        public string sims_immunization_age_group_code { get; set; }
        public string sims_immunization_student_code { get; set; }
        public string sims_vaccination_date { get; set; }
        public string sims_vaccination_renewal_date { get; set; }
        public string sims_immunization_desc { get; set; }
        public string sims_immunization_dosage1 { get; set; }
        public string sims_immunization_dosage2 { get; set; }
        public string sims_immunization_dosage3 { get; set; }
        public string sims_immunization_dosage4 { get; set; }
        public string sims_immunization_dosage5 { get; set; }

        public string sims_enroll_numbers { get; set; }
        public bool sims_immunization_status { get; set; }

        public string sims_immunization_age_group_desc { get; set; }

        public string opr { get; set; }
    }
    #endregion

    public class Sims094New
    {
        public string sims_enroll_number { get; set; }
        public string sims_immunization_code { get; set; }
        public string sims_immunization_age_group_code { get; set; }
    }

    #region Sims095(Medical Immunization Group)
    public class Sims095
    {

        public string sims_immunization_age_group_code { get; set; }
        public string sims_immunization_age_group_desc { get; set; }
        public bool sims_immunization_age_group_status { get; set; }

        public string opr { get; set; }
    }
    #endregion



    public class Sims564
    {
        public string student_immunization_code { get; set; }

        public string student_immunization_desc { get; set; }

        public string student_age_group { get; set; }

        public string student_immunization_dosage1 { get; set; }
        public string student_immunization_dosage2 { get; set; }
        public string student_immunization_dosage3 { get; set; }

        public string student_immunization_dosage4 { get; set; }

        public string student_immunization_dosage5 { get; set; }



        public string sims_immunization_age_group_code { get; set; }

        public string student_name { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_vaccination_date { get; set; }

        public string sims_vaccination_renewal_date { get; set; }
        public string opr { get; set; }

        
    }

    
    #region Sims097(Medical Medicine)
    public class Sims097
    {

        public string sims_appl_form_field_value1 { get; set; }
       // public string section_list { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_teacher_name { get; set; }
            public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_activity_symbol { get; set; }
        public string sims_class_name { get; set; }
        public string sims_medicine_code { get; set; }
        public string sims_medicine_name { get; set; }
        public string sims_medicine_type { get; set; }
        public string sims_medicine_typename { get; set; }
        public string sims_medicine_batch_number { get; set; }
        public string sims_medicine_production_date { get; set; }
        public string sims_medicine_expirey_date { get; set; }
        public string sims_medicine_expirey_alert { get; set; }
        public string sims_medicine_expirey_alertname { get; set; }

        public string opr { get; set; }

        //gracing marks

        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_activity_number { get; set; }
        public string sims_activity_name { get; set; }
        public string sims_activity_max_point { get; set; }
        public string sims_activity_max_point_subject { get; set; }
        public string sims_activity_priority { get; set; }
        public string sims_activity_Prefrred_teacher_code { get; set; }
        public Boolean sims_activity_status { get; set; }
        public string sims_gracing_type { get; set; }
        

    }
    #endregion

    #region Sims099(Medical Student Susceptibility)
    public class Sims099
    {
        public string sims_enrollment_number { get; set; }
        public string sims_susceptibility_code { get; set; }
        public string sims_susceptibility_desc { get; set; }
        public string susceptibility { get; set; }

        public string opr { get; set; }
    }
    #endregion


    #region Sims102(Medical Visit Examination)
    public class Sims102
    {
        public string sims_medicine_name { get; set; }
        public string sims_medicine_code { get; set; }
        public string sims_examination_code { get; set; }
        public string sims_examination_name { get; set; }
        public string sims_examination_desc { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims100(Medical Susceptibility)
    public class Sims100
    {
        public string sims_susceptibility_code { get; set; }
        public string sims_susceptibility_desc { get; set; }
        public bool sims_susceptibility_status { get; set; }


        public string opr { get; set; }

        public object sims_medical_excuses_name { get; set; }

        public object username { get; set; }

        public bool sims_medical_excuses_status { get; set; }

        public string sims_medical_excuses_no { get; set; }
    }
    #endregion

    #region Sims101(Medical Visit)
    public class Sims101
    {
        public string sims_enroll_number { get; set; }
        public string sims_medical_visit_number { get; set; }
        public string medical_visit_date { get; set; }
        public string medical_attended_by { get; set; }
        public string medical_complaint_desc { get; set; }
        public string medical_attendant_observation { get; set; }
        public string medical_health_education { get; set; }
        public string medical_followup_comment { get; set; }
        public string student_enroll_no { get; set; }
        public string sims_employee_number { get; set; }
        public string sims_user_code_created_by { get; set; }
        public bool sims_enrollno { get; set; }
        public bool sims_empno { get; set; }
        public string sims_student_name { get; set; }
        public string sims_Emp_name { get; set; }
        public string sims_CreatedBy_name { get; set; }
        public string sims_AttendedBy_name { get; set; }
        public string sims_medical_examination_name { get; set; }
        

        public string opr { get; set; }
    }
    #endregion

    #region Sims103(Medical Visit Medicine)
    public class Sims103
    {
        public string sims_medical_visit_number { get; set; }
        public string sims_medical_visit_numberName { get; set; }
        public string sims_serial_number { get; set; }
        //public string sims_medicine_code { get; set; }
        public string sims_medicine_name { get; set; }
        public string sims_medicine_dosage { get; set; }
        public string sims_medicine_duration { get; set; }
        public bool sims_status { get; set; }

        public string opr { get; set; }

        public string sims_medicine_code { get; set; }

        public string sims_medical_visit_date { get; set; }

        public string student_name { get; set; }

        public string medicine_given_by { get; set; }

        public string sims_enroll_number { get; set; }
    }

    #endregion

    #region Sims564 (Student Immunization)
    public class Sims444
    {
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string student_name { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_age_group { get; set; }
        public string sims_vaccination_renewal_date { get; set; }
        public string sims_immunization_age_group_code { get; set; }
        public string sims_vaccination_date { get; set; }
        public string student_immunization_code { get; set; }
        public string student_immunization_desc { get; set; }
        public string student_immunization_dosage1 { get; set; }
        public string student_immunization_dosage2 { get; set; }
        public string student_immunization_dosage3 { get; set; }
        public string student_immunization_dosage4 { get; set; }
        public string student_immunization_dosage5 { get; set; }
    }

    #endregion

    #region Sims690 (Medical Consent)
    public class Sims690
    {
        public string opr { get; set; }
        public string sims_medical_consent_srl_no { get; set; } 
		public string sims_medical_consent_cur_code { get; set; } 
		public string sims_medical_consent_enroll_number { get; set; } 
		public string sims_medical_consent_parent_number { get; set; }
		public bool sims_medical_consent_health_records { get; set; }
		public bool sims_medical_consent_immunization { get; set; }
		public bool sims_medical_consent_emergency_treatment { get; set; }
        public bool sims_medical_consent_medical_treatment { get; set; }
		public bool sims_medical_consent_over_the_counter_medicine { get; set; }
		public bool sims_medical_consent_medicine_allowed { get; set; }
		public string sims_medical_consent_attr1 { get; set; }
		public string sims_medical_consent_attr2 { get; set; }
		public string sims_medical_consent_attr3 { get; set; }
		public string sims_medical_consent_attr4 { get; set; }
		public string sims_medical_consent_attr5 { get; set; }
		public string sims_medical_consent_remark { get; set; }
		public string sims_medical_consent_applicable_from_acad_year { get; set; }
		public string sims_medical_consent_updated_on { get; set; }
        public string sims_medical_consent_approved_by { get; set; }

        public string approved_by { get; set; }

        public string parent_name { get; set; }

        public string student_name { get; set; }

        public string cur_name { get; set; }

        public string current_class { get; set; }
    }
    #endregion

    #region Sims202(Medical Visit)
    public class Sims202
    {
        public string sims_medicine_name { get; set; }
        public string sims_medicine_code { get; set; }
        

        public string opr { get; set; }
    }
    #endregion

    #region Sims231(insert grace marks)
    public class Sims231
    {
        public string sims_act_number { get; set; }
        public string sims_section_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_gracing_type { get; set; }
        public string sims_activity_number { get; set; }
        public string sims_activity_name { get; set; }
        public string sims_activity_symbol { get; set; }
        public string sims_activity_max_point { get; set; }
        public string sims_activity_max_point_subject { get; set; }
        public string sims_activity_priority { get; set; }
        public Boolean sims_activity_status { get; set; }
        public string sims_activity_Prefrred_teacher_code { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }


        public string opr { get; set; }
    }
    #endregion

    #region

    public class SSQR01
    {
        public string sims_survey_code { get; set; }
        public string sims_survey_subject { get; set; }
        public string user_group { get; set; }
        public string single_choice { get; set; }
        public string subjective { get; set; }
        public string objective { get; set; }
        public string rating { get; set; }
        public string true_false { get; set; }
        public string attempt_completed { get; set; }
        public string choice_report_status { get; set; }
        public string text_report_status { get; set; }
        public string sims_survey_user_group_code { get; set; }
        public string text_report_location { get; set; }
        public string choice_report_location { get; set; }
        public string sims_survey_acad_year { get; set; }
        public string sims_survey_grade_code { get; set; }
        public string sims_survey_section_code { get; set; }
    }
    #endregion

    #region importData
    public class IMPDOC {
        public string sims_school_region { get; set; }
        public string sims_school_region_code { get; set; }
        public string sims_school_district { get; set; }
        public string sims_school_area { get; set; }
        public string sims_school_area_code { get; set; }
        public string sims_school_type { get; set; }
        public string sims_school_board { get; set; }
        public string sims_school_name { get; set; }
        public string sims_school_full_postal_address { get; set; }
        public string sims_school_phone_no { get; set; }
        public string sims_school_pincode { get; set; }
        public string sims_school_email_id { get; set; }
        public string sims_school_name_incharge { get; set; }
        public string sims_school_name_incharge_mobile { get; set; }
        public string sims_school_principal_name { get; set; }
        public string sims_school_hm_mobile_no { get; set; }
        public string sims_school_status { get; set; }
        public string sims_uploaded_by { get; set; }
        public string sims_academic_year { get; set; }
    }
    #endregion


}