﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SIMSAPI.Models.FEENEW
{

    #region PayingAgent And Invoice
    public class sims_invoice_documents
    {
        public string in_no { get; set; }
        public string in_date { get; set; }
        public string in_final_amount { get; set; }
        public string in_status_code { get; set; }
        public string in_status_name { get; set; }
        public string in_created_by { get; set; }
        public string in_enroll_number { get; set; }
        public string in_created_date { get; set; }
        public bool in_sel_status { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }

    }

    public class sims541 ///Invoice Generation For Student
    {
        public sims541()
        {
            iv_documents = new List<sims_invoice_documents>();
        }

        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }

        //student Details
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_parent_name { get; set; }
        public string sims_student_class { get; set; }
        public string sims_expected_amount { get; set; }
        public string sims_paid_amount { get; set; }
        public string sims_invoice_amount { get; set; }

        //Bool Variables
        public bool sims_invoice_status { get; set; }
        public bool sims_invoice_student_status { get; set; }

        //Fee Variables
        public string sims_fee_code { get; set; }
        public string sims_period_code { get; set; }

        //Paying Agent Variables
        public string sims_acc_no { get; set; }
        public string slma_slgldrg_no { get; set; }
        public string sims_transacation_no { get; set; }
        public string sims_transacation_line_no { get; set; }


        public string sims_invoce_mode { get; set; }
        public string sims_invoce_mode_code { get; set; }

        public string sims_invoce_frq_name { get; set; }
        public string sims_invoce_frq_name_value { get; set; }

        public string sims_start_date { get; set; }
        public string sims_end_date { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_ldgrctl_name { get; set; }
        public string coad_pty_full_name { get; set; }
        public string in_no { get; set; }
        public string in_date { get; set; }
        public string in_status { get; set; }
        public string in_status_code { get; set; }

        public List<sims_invoice_documents> iv_documents;

        //Emailids
        public string father_email { get; set; }
        public string mother_email { get; set; }

        public string sims_fee_code_desc { get; set; }
    }
    #endregion

    #region CFR
    public class CFR
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_enrollment_nos { get; set; }
        public string sims_start_date { get; set; }
        public string sims_end_date { get; set; }



        public string sims_student_name { get; set; }
        public string sims_receipt_no { get; set; }
        public string sims_receipt_date { get; set; }
        public string sims_receipt_amount { get; set; }
        public string sims_receipt_status { get; set; }
        public string sims_receipt_status_desc { get; set; }
        public string sims_receipt_genrated_by { get; set; }
        public string sims_receipt_dt_code { get; set; }
        public string IsSiblingReceipt { get; set; }

        public string sims_receipt_narration { get; set; }

        public string sims_academic_year_status { get; set; }
    }
    #endregion

    #region late_fee_rules
    public class sims_late_fee_rule_details
    {
        public string sims_late_rule_code { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public bool sims_late_rule_details_status { get; set; }
    }
    public class sims_late_fee_rule
    {
        public sims_late_fee_rule()
        {
            details = new List<sims_late_fee_rule_details>();
        }
        public List<sims_late_fee_rule_details> details { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_full_name { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year_start_date { get; set; }
        public string sims_academic_year_end_date { get; set; }
        public string sims_academic_year_status { get; set; }
        public string sims_late_rule_code { get; set; }
        public string sims_late_rule_description { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_term_code { get; set; }
        public string sims_fee_term_desc_en { get; set; }
        public bool sims_late_rule_status { get; set; }
        public string sims_fee_code_list { get; set; }

        public string sims_late_rule_min_days { get; set; }

        public string sims_late_rule_max_days { get; set; }

        public string sims_late_rule_formula_amount { get; set; }

        public bool sims_late_rule_per_day_flag { get; set; }

        public bool sims_late_rule_formula_status { get; set; }

        public string sims_late_rule_formula_sr { get; set; }
    }



    public class Sims036
    {
        public string transport_fee_category_name { get; set; }
        public string transport_fee_code_desctiption { get; set; }
        public string transport_fee_frequency_name { get; set; }
        public string sims_library_fee_frequency { get; set; }

        public string section_fee_number { get; set; }
        public string section_fee_cur_code { get; set; }
        public string section_fee_cur_code_name { get; set; }
        //public string section_fee_academic_year { get; set; }
        public string section_fee_grade_code { get; set; }
        public string section_fee_grade_name { get; set; }
        public string section_fee_section_code { get; set; }
        public string section_fee_section_name { get; set; }
        public string section_fee_code { get; set; }
        public string section_fee_code_desctiption { get; set; }
        public string section_fee_code_type { get; set; }
        public string section_fee_code_type_name { get; set; }
        public bool section_fee_refundable { get; set; }
        public bool section_fee_manual_receipt { get; set; }
        public string section_fee_frequency { get; set; }
        public string section_fee_frequency_name { get; set; }
        public bool section_fee_mandatory { get; set; }
        public string section_fee_amount { get; set; }
        public bool section_fee_installment_mode { get; set; }
        public string section_fee_installment_min_amount { get; set; }
        public string section_fee_period1 { get; set; }
        public string section_fee_period2 { get; set; }
        public string section_fee_period3 { get; set; }
        public string section_fee_period4 { get; set; }
        public string section_fee_period5 { get; set; }
        public string section_fee_period6 { get; set; }
        public string section_fee_period7 { get; set; }
        public string section_fee_period8 { get; set; }
        public string section_fee_period9 { get; set; }
        public string section_fee_period10 { get; set; }
        public string section_fee_period11 { get; set; }
        public string section_fee_period12 { get; set; }
        public bool section_fee_status { get; set; }
        public string section_fee_category { get; set; }
        public string section_fee_category_name { get; set; }
        //End of Sims036

        public string section_fee_cur_code_name_to { get; set; }
        public string section_fee_level_code_name { get; set; }
        public string section_fee_academic_year_to { get; set; }
        public bool copyto { get; set; }
        public string section_fee_grade_name_to { get; set; }
        public string section_fee_grade_code_to { get; set; }
        public string section_fee_section_name_to { get; set; }
        public string section_fee_section_code_to { get; set; }
        public string section_fee_category_name_to { get; set; }
        public bool overwrite { get; set; }

        public bool section_fee_split_flag { get; set; }

        public string section_fee_academic_year { get; set; }

        public object section_name { get; set; }

        public object cur_name { get; set; }

        public object academic_year { get; set; }

        public object grade_name { get; set; }

        public object fee_category { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name { get; set; }

        public string sims_cur_short_name { get; set; }
    }
    public class Fee_code_list
    {
        public string sims_fee_category { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_fee_code_description { get; set; }

        public bool isassigned { get; set; }

        public bool sims_fee_status { get; set; }
    }


    public class concessiondetails
    {

        public string sims_concession_no { get; set; }

        public string sims_concession_desc { get; set; }

        public string sims_concession_type { get; set; }

        public string sims_conession_fee_type { get; set; }

        public string sims_conession_to_date { get; set; }

        public bool sims_conession_status { get; set; }

        public string sims_fee_period1_concession { get; set; }

        public string sims_fee_period1_concession_paid { get; set; }

        public string sims_fee_period2_concession { get; set; }

        public string sims_fee_period2_concession_paid { get; set; }

        public string sims_fee_period3_concession { get; set; }

        public string sims_fee_period3_concession_paid { get; set; }

        public string sims_fee_period4_concession { get; set; }

        public string sims_fee_period4_concession_paid { get; set; }

        public string sims_fee_period5_concession { get; set; }

        public string sims_fee_period5_concession_paid { get; set; }

        public string sims_fee_period6_concession { get; set; }

        public string sims_fee_period6_concession_paid { get; set; }

        public string sims_fee_period7_concession { get; set; }

        public string sims_fee_period7_concession_paid { get; set; }

        public string sims_fee_period8_concession_paid { get; set; }

        public string sims_fee_period8_concession { get; set; }

        public string sims_fee_period9_concession { get; set; }

        public string sims_fee_period9_concession_paid { get; set; }

        public string sims_fee_period10_concession { get; set; }

        public string sims_fee_period10_concession_paid { get; set; }

        public string sims_fee_period11_concession { get; set; }

        public string sims_fee_period11_concession_paid { get; set; }

        public string sims_fee_period12_concession { get; set; }

        public string sims_fee_period12_concession_paid { get; set; }

        public bool sims_fee_status { get; set; }
    }

    public class Sims052
    {

        public List<Fee_code_list> sublist = new List<Fee_code_list>();
        public List<concessiondetails> concessiondetails = new List<concessiondetails>();
        public string sims_enroll_number { get; set; }

        public string sims_fee_cur_code { get; set; }

        public string sims_fee_academic_year { get; set; }

        public string sims_fee_grade_code { get; set; }

        public string sims_fee_section_code { get; set; }

        public string sims_fee_category { get; set; }

        public string Concession { get; set; }

        public string studentName { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_fee_code_description { get; set; }

        public bool isassigned { get; set; }

        public string sims_fee_number { get; set; }

        public string sims_fee_frequency { get; set; }

        public string sims_fee_amount { get; set; }

        public bool sims_fee_installment_mode { get; set; }

        public string sims_fee_installment_min_amount { get; set; }

        public string sims_fee_period1 { get; set; }

        public string sims_fee_period2 { get; set; }

        public string sims_fee_period3 { get; set; }

        public string sims_fee_period4 { get; set; }

        public string sims_fee_period5 { get; set; }

        public string sims_fee_period6 { get; set; }

        public string sims_fee_period7 { get; set; }

        public string sims_fee_period8 { get; set; }

        public string sims_fee_period9 { get; set; }

        public string sims_fee_period10 { get; set; }

        public string sims_fee_period11 { get; set; }

        public string sims_fee_period12 { get; set; }

        public bool sims_fee_status { get; set; }
        public string sims_fee_category_description { get; set; }
    }

    public class sims_late_fee_students
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_full_name { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year_start_date { get; set; }
        public string sims_academic_year_end_date { get; set; }
        public string sims_academic_year_status { get; set; }
        public string sims_late_rule_code { get; set; }
        public string sims_late_rule_description { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_term_code { get; set; }
        public string sims_fee_term_desc_en { get; set; }
        public bool sims_late_rule_status { get; set; }
        public string sims_fee_code_list { get; set; }

        public object sims_grade_code { get; set; }

        public object sims_section_code { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_late_fee_amount { get; set; }

        public string sims_student_name { get; set; }

        public string sims_fee_amount_transpfer_code { get; set; }
    }

    #endregion

    public class StudentFee
    {
        public string StudDEnroll { get; set; }
        public string StudDName { get; set; }

        public string StudCurr { get; set; }
        public string StudAcademic { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }

        public string StudGradeName { get; set; }
        public string StudSectionName { get; set; }

        public string ParentName { get; set; }
        public string ParentID { get; set; }
        public string ParentEmpID { get; set; }
        public string ParentMobileNo { get; set; }

        public List<FeeDetails> FeeDetails { get; set; }
        public List<OtherFee> OtherFeeDetails { get; set; }
        public List<non_mandetory_fees> non_mandetory_fee { get; set; }
        //public List<DiscountDetails> ConcessionDetails { get; set; }
        //public List<PayingAgents> PayingAgentDetails { get; set; }

        public string PayingAgentName { get; set; }
        public string payment_plan_desc { get; set; }
        public string payment_plan_code { get; set; }
        public string payment_plan_remark { get; set; }
    }

    public class FeeDetails
    {
        public string sims_fee_term_code { get; set; }
        public string sims_fee_term_desc_en { get; set; }
        public string sims_fee_term_start_date { get; set; }
        public string sims_fee_term_end_date { get; set; }
        public string SrNo { get; set; }
        public string StudEnroll { get; set; }
        public string StudCurr { get; set; }
        public string StudAcademic { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }
        public string rem { get; set; }

        public string StudFeeNumber { get; set; }

        public string StudFeeCode { get; set; }
        public string StudFeeDesc { get; set; }

        public string StudFeeCategoryCode { get; set; }
        public string StudFeeCategoryDesc { get; set; }

        public string StudFeeFrequencyCode { get; set; }
        public string StudFeeFrequencyDesc { get; set; }

        public string StudFeePeriodNo { get; set; }
        public string StudFeePeriodDesc { get; set; }

        public string StudFeeTermCode { get; set; }
        public string StudFeeTermDesc { get; set; }

        public string StudFeeInstallmentMode { get; set; }
        public string StudFeeInstallmentMinAMT { get; set; }

        public string StudTotalFeeAMT { get; set; }
        public string StudExpectedFeeAMT { get; set; }
        public string StudConcessionAMT { get; set; }
        public string StudConcessionPaidAMT { get; set; }
        public string StudTotalPaidAMT { get; set; }
        public string StudBalanceAMT { get; set; }
        public string StudBalancePayingAMT { get; set; }

        public string StudInvoiceNO { get; set; }
        public string StudInvoiceTransNO { get; set; }
        public string StudInvoiceTransLineNO { get; set; }
        public string pref_id { get; set; }
        public string term_flag { get; set; }
        public string valid { get; set; }
        public string studFeetype { get; set; }//NEw Field to Indentify Other fee Code
        public string payingAgent { get; set; }
        public List<FeeDetails> subItems { get; set; }


        public string stud_vat_per { get; set; }

        public string stud_vat_calc_amt { get; set; }
    }

    public class DiscountDetails
    {
        public string DisNumber { get; set; }
        public string DisDesc { get; set; }
        public string DisType { get; set; }
        public string DisEnrollNumber { get; set; }
        public string DisTypeDesc { get; set; }
        public string DisDiscountType { get; set; }
        public string DisDiscountTypeDesc { get; set; }
        public string DisDiscountValue { get; set; }
        public string DisApplicableON { get; set; }
        public string DisCorpBilling { get; set; }
        public string DisAcademicYear { get; set; }
        public string DisApplicableTo { get; set; }
        public string DisOnwardChild { get; set; }
        public string DisFeeCode { get; set; }
        public string DisStatus { get; set; }
        public List<ConcessionTransaction> ConT { get; set; }
    }

    public class ConcessionTransaction
    {
        public string ConTTransNumber { get; set; }
        public string ConTCurCode { get; set; }
        public string ConTEnrollNumber { get; set; }
        public string ConTNumber { get; set; }
        public string ConTFromDate { get; set; }
        public string ConToDate { get; set; }
        public string ConTCreatedBy { get; set; }
        public string ConTCreatedDate { get; set; }
        public string ConTStatus { get; set; }
    }

    public class PayingAgents
    {
        public string PATransNum { get; set; }
        public string PATransLineNum { get; set; }
        public string PAAcademic { get; set; }
        public string PAEnroll { get; set; }
        public string PAFeeCode { get; set; }
        public string PADiscountType { get; set; }
        public string PADiscountTypeDesc { get; set; }
        public string PADiscountVal { get; set; }
        public string PADiscountValUsed { get; set; }
        public string PALDGRCode { get; set; }
        public string PAStatus { get; set; }
        public string PADiscountPer { get; set; }
    }

    public class PaymentModes
    {
        public string PMType { get; set; }
        public string PMDescShort { get; set; }
        public string PMDesc { get; set; }
    }

    public class OtherFee
    {
        public string OF_code { get; set; }
        public string OF_Fee_code { get; set; }
        public string OF_Fee_cat { get; set; }
        public string OF_Desc { get; set; }
        public string OF_AMT { get; set; }

        public string vat_per { get; set; }

        public string vat_calc_amount { get; set; }
    }

    public class non_mandetory_fees
    {
        public string non_fee_code { get; set; }
        public string non_fee_desc { get; set; }
        public string non_fee_category { get; set; }
        public string non_fee_amt { get; set; }
        public string non_vat_per { get; set; }
        public string non_vat_calc_amount { get; set; }
    }

    public class FeePeriods
    {
        public string feeNumber { get; set; }
        public string Pno { get; set; }
        public string FPColName { get; set; }
        public string FPamt { get; set; }
        public string Vat { get; set; }
        public string Vat_rnd { get; set; }

        public string FCColName { get; set; }
        public string FCAmt { get; set; }
        public string PayMode { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDt { get; set; }
        public string BankCode { get; set; }
        public string TXNNo { get; set; }
        public string Enroll { get; set; }
        public string ParentID { get; set; }
        public string payingAgentTransNo { get; set; }
        public string payingAgentTransLineNo { get; set; }
        public string temp_Fee_num { get; set; }
        public bool parsed = false;
        public string DisNumber { get; set; }
        public string getCol()
        {
            string str = "";
            if (!string.IsNullOrEmpty(Pno))
            {

                if (!string.IsNullOrEmpty(FPamt))
                {
                    //str = string.Format("sims_fee_period{0}_paid = sims_fee_period{0}_paid+{1} ,", Pno, FPamt);
                    str = string.Format("sims_fee_period_paid_amount= sims_fee_period_paid_amount+{1} ,", Pno, FPamt);

                    parsed = true;
                }
                //if (!string.IsNullOrEmpty(FCAmt))
                //{
                //    str += string.Format("sims_fee_period{0}_concession_paid = sims_fee_period{0}_concession_paid+{1} ,", Pno, FCAmt);
                //    parsed = true;
                //}
                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public string getInsCols()
        {
            StringBuilder str = new StringBuilder();
            if (!string.IsNullOrEmpty(Pno))
            {
                if (!string.IsNullOrEmpty(FPamt))
                {
                    str.Append(string.Format("sims_fee_period_amount, sims_fee_period_paid_amount,", Pno));
                    parsed = true;
                }
            }
            return str.ToString();
        }

        public string getInsValues()
        {
            StringBuilder str = new StringBuilder();
            this.temp_Fee_num = "FP" + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond;
            if (!string.IsNullOrEmpty(Pno))
            {
                if (!string.IsNullOrEmpty(FPamt))
                {
                    str.Append(string.Format("{0}, {0},", FPamt));
                    parsed = true;
                }
            }
            return str.ToString();
        }

        public string DBYYYYMMDDformat(string dt)
        {
            var uigetdate = dt;
            string dbgetformatDate = null;
            if (uigetdate == null || uigetdate == "")
            {
                return dbgetformatDate;
            }
            else
            {
                var day = uigetdate.Split('-')[0];
                var month = uigetdate.Split('-')[1];
                var year = uigetdate.Split('-')[2];

                dbgetformatDate = year + "-" + month + "-" + day;


            }
            return dbgetformatDate;
        }
        public string getDocDetails()
        {
            StringBuilder str = new StringBuilder();
            if (!string.IsNullOrEmpty(Pno))
            {
                if (!string.IsNullOrEmpty(FPamt))
                {
                    str.Append("insert into ##stdDocDetailTBL_TEMP ");
                    //str.Append(string.Format("select distinct null,'T','{0}','{1}',{2},{3},{2}-{3},'{4}','{5}','{6}','{7}',null,null,null,null,'{8}','{12}','{9}','N','{10}','{11}'", feeNumber, Pno, FPamt, (string.IsNullOrEmpty(FCAmt) ? "0" : FCAmt), PayMode, ChequeNo, ChequeDt, BankCode, TXNNo, Enroll, this.temp_Fee_num, ParentID, payingAgentTransLineNo));
                    str.Append(string.Format("select distinct null,'T','{0}','{1}',{2},{3},{2}-{3},'{4}','{5}','{6}','{7}','{8}',null,null,null,null,'{12}','{9}','N','{10}','{11}','{13}'", feeNumber, Pno, FPamt, (string.IsNullOrEmpty(FCAmt) ? "0" : FCAmt), PayMode, ChequeNo, DBYYYYMMDDformat(ChequeDt), BankCode, TXNNo, Enroll, this.temp_Fee_num, ParentID, payingAgentTransLineNo, DisNumber));
                }
                //str = str.Remove(0, str.Length - 1);
            }
            return str.ToString();
        }

        public string getDocDetails_VAT()
        {
            StringBuilder str = new StringBuilder();
            if (!string.IsNullOrEmpty(Pno))
            {
                if (!string.IsNullOrEmpty(FPamt))
                {
                    str.Append("insert into ##stdDocDetailTBL_TEMP ");
                    //str.Append(string.Format("select distinct null,'T','{0}','{1}',{2},{3},{2}-{3},'{4}','{5}','{6}','{7}',null,null,null,null,'{8}','{12}','{9}','N','{10}','{11}'", feeNumber, Pno, FPamt, (string.IsNullOrEmpty(FCAmt) ? "0" : FCAmt), PayMode, ChequeNo, ChequeDt, BankCode, TXNNo, Enroll, this.temp_Fee_num, ParentID, payingAgentTransLineNo));
                    str.Append(string.Format("select distinct null,'T','{0}','{1}',{2},{3},{2}-{3},'{4}','{5}','{6}','{7}','{8}',null,null,null,null,'{12}','{9}','N','{10}','{11}','{13}','{14}','{15}'", feeNumber, Pno, FPamt, (string.IsNullOrEmpty(FCAmt) ? "0" : FCAmt), PayMode, ChequeNo, DBYYYYMMDDformat(ChequeDt), BankCode, TXNNo, Enroll, this.temp_Fee_num, ParentID, payingAgentTransLineNo, DisNumber, Vat, Vat_rnd));
                }
                //str = str.Remove(0, str.Length - 1);
            }
            return str.ToString();
        }
    }

    public class FeeNumber
    {
        public string Fee_number { get; set; }
        public string Fee_Category { get; set; }
        public string Fee_Code { get; set; }
        public string StudCurr { get; set; }
        public string StudAcademic { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }
        public string StudDEnroll { get; set; }
        public string temp_fee_num { get; set; }
        public string ParentID { get; set; }
        public List<FeePeriods> Periods { get; set; }
        public string GetRecord()
        {
            string str = "";
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(Fee_number))
            {
                sb.Append(@"insert into ##stdFeeTBL_TEMP (sims_enroll_number,sims_fee_number,sims_fee_cur_code,sims_fee_academic_year,sims_fee_grade_code,sims_fee_section_code,sims_fee_category,sims_fee_code,sims_fee_frequency,sims_fee_amount,sims_fee_installment_mode,sims_fee_installment_min_amount,");
                foreach (var item in Periods)
                {
                    sb.Append(item.getInsCols());
                }
                sb.Append("temp_fee_num,isupdated,parentNumber) ");
                sb.Append(string.Format(" select distinct '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','O',0,'N',0,", StudDEnroll, "{FENO}", StudCurr, StudAcademic, StudGrade,
                    StudSection, Fee_Category, Fee_Code));
                foreach (var item in Periods)
                {
                    sb.Append(item.getInsValues());
                    temp_fee_num = item.temp_Fee_num;
                }
                sb.Append("'").Append(temp_fee_num).Append("','N','").Append(ParentID + "'");
                str = sb.ToString();
                //foreach (var p in Periods)
                //{
                //    p.Enroll = this.StudDEnroll;
                //    p.ParentID = this.ParentID;
                //}
            }
            else
            {
                str = "update ##stdFeeTBL_TEMP set  ";
                foreach (var p in Periods)
                {
                    p.feeNumber = this.Fee_number;
                    p.Enroll = this.StudDEnroll;
                    p.ParentID = this.ParentID;
                    str = str + p.getCol() + ",";
                }
                str = str.Substring(0, str.Length - 1);
                str = str + string.Format(" where sims_enroll_number='{0}' and sims_fee_number='{1}' and sims_fee_cur_code+sims_fee_academic_year='{2}' and sims_fee_grade_code+sims_fee_section_code='{3}' and parentNumber='{4}'",// AND sims_fee_period_code='{5}'",
                                        StudDEnroll, Fee_Code, StudCurr + StudAcademic, StudGrade + StudSection, ParentID);
            }
            return str;
            
        }
        public string GetRecordNew(string Fee_number_param, FeePeriods p)
        {
            string str = "";
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(Fee_number_param))
            {

                sb.Append(@"insert into ##stdFeeTBL_TEMP (sims_fee_period_code,sims_enroll_number,sims_fee_number,sims_fee_cur_code,sims_fee_academic_year,sims_fee_grade_code,sims_fee_section_code,sims_fee_category,sims_fee_code,sims_fee_status,sims_fee_frequency,sims_fee_amount,sims_fee_installment_mode,sims_fee_installment_min_amount,");
                foreach (var item in Periods)
                {
                    sb.Append(item.getInsCols());
                }
                sb.Append("temp_fee_num,isupdated,parentNumber) ");
                sb.Append(string.Format(" select distinct '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','A','O',0,'N',0,",p.Pno, StudDEnroll, "{FENO}", StudCurr, StudAcademic, StudGrade,
                    StudSection, Fee_Category, Fee_Code));
                foreach (var item in Periods)
                {
                    sb.Append(item.getInsValues());
                    temp_fee_num = item.temp_Fee_num;
                }
                sb.Append("'").Append(temp_fee_num).Append("','N','").Append(ParentID + "'");
                str = sb.ToString();
                //foreach (var p in Periods)
                //{
                //    p.Enroll = this.StudDEnroll;
                //    p.ParentID = this.ParentID;
                //}
            }
            else
            {
                str = "update ##stdFeeTBL_TEMP set  ";
                //foreach (var p in Periods)
                //{
                //    p.feeNumber = this.Fee_number;
                //    p.Enroll = this.StudDEnroll;
                //    p.ParentID = this.ParentID;
                //    str = str + p.getCol() + ",";
                //}
                str = str + p.getCol() + ",";
                str = str.Substring(0, str.Length - 1);
                str = str + string.Format(" where sims_enroll_number='{0}' and sims_fee_number='{1}' and sims_fee_cur_code+sims_fee_academic_year='{2}' and sims_fee_grade_code+sims_fee_section_code='{3}' and parentNumber='{4}' AND sims_fee_period_code='{5}'",
                                        StudDEnroll, Fee_Code, StudCurr + StudAcademic, StudGrade + StudSection, ParentID, p.Pno);
            }
            return str;
        }
    }

    public class param
    {
        public string ParentID { get; set; }
        public string ReceiptDate { get; set; }
        public string UserName { get; set; }
        public string Remark { get; set; }
        public List<Student> Students { get; set; }
    }

    public class Student
    {
        public string Enroll { get; set; }
        public string ParentID { get; set; }
        public string StudCurr { get; set; }
        public string StudAcademic { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }
        public List<FeeNumber> Fees { get; set; }
    }

    public class TodaysCollectionDetails
    {
        public string TotalCollection { get; set; }
        public string validReceipts { get; set; }
        public string cancelledReceipt { get; set; }
    }

    public class BankDet
    {
        public string bankCode { get; set; }
        public string bankName { get; set; }
        public string glAcno { get; set; }
        public string slno { get; set; }
    }

    public class FeeDetailsPR
    {
        public string StudEnroll { get; set; }
        public string StudCurr { get; set; }
        public string StudAcademic { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }
        public string rem { get; set; }
        public string StudFeeTermCode { get; set; }
        public string PACODE { get; set; }
        public string payingAgent { get; set; }
        public string valid { get; set; }
    }

    public class Sim531
    {
        internal string Name;

        public string discount_description { get; set; }
        public string fee_description { get; set; }
        public string opr { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_concession_desc { get; set; }
        public string sims_concession_type { get; set; }
        public string sims_discount_desc { get; set; }
        public string sims_discount_type { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_fee_academic_year { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_cur_code { get; set; }
        public string sims_fee_grade_code { get; set; }
        public string sims_fee_section_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_ledger_ac_no { get; set; }
        public string sims_ledger_code { get; set; }
        public string sims_paying_agent_academic_year { get; set; }
        public bool sims_paying_agent_corporate_billing { get; set; }
        public string sims_paying_agent_description { get; set; }
        public string sims_paying_agent_discount_type { get; set; }
        public string sims_paying_agent_discount_value { get; set; }
        public string sims_paying_agent_fee_code { get; set; }
        public string sims_paying_agent_name { get; set; }
        public string sims_paying_agent_name_plus_acno { get; set; }
        public string sims_paying_agent_name_plus_ledger_code { get; set; }
        public string sims_paying_agent_number { get; set; }
        public string sims_paying_agent_slma_ldgr { get; set; }
        public string sims_paying_created_date { get; set; }
        public string sims_paying_ldgrctl_code { get; set; }
        public string sims_section_name { get; internal set; }
        public string sims_sip_academic_year { get; set; }
        public string sims_sip_academic_year_desc { get; set; }
    }

    //Mapping
    public class Sims531
    {
        public Sims531()
        {
            List<Sims531_mapping> mapping = new List<Sims531_mapping>();
        }

        public string sims_paying_ldgrctl_code { get; set; }
        public string sims_agent_name { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_student_class { get; set; }
        public string sims_pagt_name { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_acno { get; set; }
        public List<Sims531_mapping> mapping { get; set; }

        public string sims_paying_agent_number { get; set; }
        public string agent_tran_no { get; internal set; }
        public int count { get; set; }
        public string sims_paying_agent_slma_code { get; set; }
        public string fins_appl_form_field_value3 { get; set; }
        public string coad_pty_full_name { get; set; }
        public string sims_fee_code_description { get; set; }
        public string student_count { get; set; }


        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }

        public string sims_fee_frequency { get; set; }
        public string sims_late_fee_applicable_after_days { get; set; }
        public bool sims_late_fee_end_date_flag { get; set; }
        public bool sims_late_fee_status { get; set; }
        public string opr { get; set; }
        public string sims_late_fee_master_sr_no { get; set; }
        public string sims_fee_frequency_name { get; set; }
    }
    public class Sims531_mapping
    {

        public string sims_pa_transaction_number { get; set; }
        public string sims_pa_transaction_line_no { get; set; }
        public string sims_paying_agent_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_enroll_number { get; set; }
        public bool sims_status { get; set; }
        public string sims_paying_ldgrctl_code { get; set; }
        public string sims_paying_agent_slma_ldgr { get; set; }
        public string sims_paying_agent_discount_type { get; set; }
        public string sims_paying_agent_discount_type_dsc { get; set; }
        public string sims_paying_agent_discount_value { get; set; }
        public string sims_academic_year { get; set; }
    }

}