﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.FEE
{
    public class FeeCategory
    {
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }

        public string sims_fee_academic_year { get; set; }
        public string sims_fee_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_fee_section_code { get; set; }

        public string sims_section_name_en { get; set; }
        public string sims_fee_category { get; set; }

        public string sims_fee_category_description { get; set; }
        
	
        // paying details


        public string sims_paying_agent_number { get; set; }
        public string sims_paying_agent_description { get; set; }
        public string sims_paying_agent_discount_type { get; set; }
        public string discount_type { get; set; }

        public string sims_paying_agent_discount_value { get; set; }
        public string sims_paying_agent_fee_code { get; set; }

        public string sims_paying_ldgrctl_code { get; set; }
        public string fee_type { get; set; }
        

        //student fee detail summary

	    public string sims_sibling_parent_number { get; set; }
        public string class1 { get; set; }

        public string debit { get; set; }
        public string credit { get; set; }

        public string pc_amt { get; set; }
        public string net_amount { get; set; }
			
		//fee details



        public string expected_amount { get; set; }
        public string fee_paid { get; set; }

        public string doc_no { get; set; }
        public string doc_date { get; set; }

		// paying transaction detail

        public string doc_status { get; set; }
        public string dd_fee_amount_final { get; set; }

        //expected invoic dif

        public string status_desc { get; set; }
        public string status { get; set; }
        public string sims_fee_code_description { get; set; }
        public string name { get; set; }
        public string sims_expected_amount { get; set; }
        public string sims_invoice_amount { get; set; }
        public string sims_difference { get; set; }
        //supriya

        // Fee Concession Report

        
                           
                 
        public string sims_student_name_en { get; set; }
        public string sims_concession_description { get; set; }
        public string sims_fee_code_desc { get; set; }
        public string sims_concession_from_date { get; set; }
        public string sims_concession_to_date { get; set; }
        public string total_fee { get; set; }
        public string concession_amount { get; set; }
        public string sims_concession_approved_by { get; set; }

          public string sims_concession_number { get; set; }


      
      
      //supriya           

    }
}