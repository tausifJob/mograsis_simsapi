﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ComnInvData
{
    public class CommonINV
    {
        //Item Reorder List
        public string dep_name { get; set; }
        public string im_inv_no { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string im_approximate_lead_time { get; set; }
        public string im_average_month_demand { get; set; }
        public string im_reorder_level { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string pc_parent_code { get; set; }
        public string pc_category_group { get; set; }
        public string dep_code { get; set; }
        public string com_code { get; set; }
        		
        //Item Stock Report
        public string id_yob_qty { get; set; }
        public string im_malc_rate { get; set; }
        public string ad_qty { get; set; }
        public string dd_qty { get; set; }
        public string toatl_value { get; set; }
        public string toatl_value_date { get; set; }
        public string net_balance { get; set; }
        public string net_value { get; set; }
        //cancelled Sale Document List
        public string doc_prov_no { get; set; }
        public string doc_date { get; set; }
        public string dt_desc { get; set; }
        public string sal_name { get; set; }
        public string student_name { get; set; }
        public string sm_name { get; set; }
        public string doc_total_amount { get; set; }
        //Sales Report By Item
        public string grade_name { get; set; }
        public string section_name { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string name { get; set; }
        public string book_status { get; set; }
        public string dd_sell_price { get; set; }
        //Costing Sheet Summary
        public string cs_no { get; set; }
        public string cs_prov_date { get; set; }
        public string supplier { get; set; }
        public string gross { get; set; }
        public string od_total_discount { get; set; }
        public string od_total_expense { get; set; }
        public string cs_landed_cost { get; set; }
        //sales document list salesman
        public string status { get; set; }
        public string doc_reference_prov_no { get; set; }
        public string sm_code { get; set; }
        public string doc_status { get; set; }
        //sales aging report
        public string item { get; set; }
        public string zero_thirty { get; set; }
        public string zero_thirty_P { get; set; }
        public string thirtyone_sixty { get; set; }
        public string thirtyone_sixty_P { get; set; }
        public string sixtyone_ninty { get; set; }
        public string sixtyone_ninty_P { get; set; }
        public string nintyone_onetwenty { get; set; }
        public string nintyone_onetwenty_P { get; set; }
        public string onetwenty_plus { get; set; }
        public string onetwenty_plus_P { get; set; }
        public string total { get; set; }
        //Inventory voluation report
        public string current_qty { get; set; }
        public string current_wac_cost { get; set; }
        public string current_value { get; set; }
        public string current_sell_price { get; set; }
        public string sale_value { get; set; }
       
        //service material status report
        public string item_name { get; set; }
        public string doc_prov_date { get; set; }
        public string req_type { get; set; }
        public string comn_requestors_name { get; set; }
        public string doc_approval_status { get; set; }
        public string id_cur_qty { get; set; }
        public string req_desc { get; set; }
        public string req_status { get; set; }




















































    }
}