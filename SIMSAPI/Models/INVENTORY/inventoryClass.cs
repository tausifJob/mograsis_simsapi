﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.inventoryClass
{
    public class InvsClass
    {
        public string sup_code { get; set; }

        public string sup_name { get; set; }

        public string item_name { get; set; }

        public string dep_code { get; set; }

        public string dep_name { get; set; }

        public string item_code_value1 { get; set; }

        public string inv_code { get; set; }

        public string curr_name { get; set; }

        public string curr_code { get; set; }

        public string uom_name { get; set; }

        public string uom_code { get; set; }

        public string im_item_code { get; set; }

        public string im_inv_no { get; set; }
    }

    public class Invs036
    {
        #region
        public string ic_account_no { get; set; }
        public string ic_account_name { get; set; }
        public string ic_status { get; set; }
        #endregion

        public string opr { get; set; }

        public string ic_status_code { get; set; }
    }

    public class FeePPM
    {
        public string invs_company_code { get; set; }
        public string invs_company_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public bool invs_fee_posting_status { get; set; }
        public string opr { get; set; }
        public string sims_appl_parameter { get; set; }
        public string acno { get; set; }
        public string sd_payment_type_desc { get; set; }
        public string sd_payment_type { get; set; }
        public string sd_payment_scno { get; set; }
        public bool sd_status { get; set; }
        public string sd_fin_year { get; set; }
        public string invs_fee_acno_old { get; set; }
    }

    public class itemgradesec
    {
        public bool isselect { get; set; }
        public string cur { get; set; }
        public string cur_name { get; set; }
        public string aca { get; set; }
        public string aca_name { get; set; }
        public string grade { get; set; }
        public string grade_name { get; set; }
        public string section { get; set; }
        public string section_name { get; set; }
        public string item_no { get; set; }
        public string item_desc { get; set; }
        public string item_qty { get; set; }
        public string qty_for_parent { get; set; }
        public bool qty_flag { get; set; }
        public string cat { get; set; }
        public string cat_name { get; set; }
        public string sub_cat { get; set; }
        public string sub_cat_name { get; set; }
        public string dep_name { get; set; }

    }
    //public class Invs058_item
    //{

    //    public decimal req_no { get; set; }

    //    public string req_type { get; set; }

    //    public string dep_code { get; set; }

    //    public string req_status { get; set; }

    //    public string dep_name { get; set; }

    //    public string req_type_name { get; set; }

    //    public string req_status_name { get; set; }

    //    public string dm_code { get; set; }

    //    public decimal req_discount_pct { get; set; }

    //    public string req_requester_dep { get; set; }

    //    public string req_remarks { get; set; }

    //    public string req_agency_flag { get; set; }

    //    public string dm_name { get; set; }

    //    public string sm_code { get; set; }

    //    public string im_inv_no { get; set; }

    //    public string sg_name { get; set; }

    //    public string im_item_code { get; set; }

    //    public string im_desc { get; set; }

    //    public string category { get; set; }

    //    public string subCategory { get; set; }

    //    public string sec_code { get; set; }

    //    public string sup_code { get; set; }

    //    public string uom_code { get; set; }

    //    public string im_malc_rate { get; set; }

    //    public string im_sell_price { get; set; }

    //    public string sec_name { get; set; }

    //    public string im_assembly_ind { get; set; }

    //    public string sg_desc { get; set; }

    //    public string uom_name { get; set; }

    //    public string sup_name { get; set; }

    //    public string curr_qty { get; set; }

    //    public string do_qty { get; set; }

    //    public string ava_aty { get; set; }

    //    public string request_type_code { get; set; }

    //    public string request_type_name { get; set; }

    //    public string request_status_code { get; set; }

    //    public string request_status_name { get; set; }

    //    public string salesman_code { get; set; }

    //    public string salesman_name { get; set; }

    //    public string dept_code { get; set; }

    //    public string dept_name { get; set; }

    //    public string opr { get; set; }

    //    public string rd_date_required { get; set; }

    //    public string rd_quantity { get; set; }

    //    public string rd_remarks { get; set; }

    //    public string excg_curcy_code { get; set; }

    //    public string excg_curcy_desc { get; set; }

    //    public string req_date { get; set; }

    //    public string sm_name { get; set; }

    //    public string rd_item_desc { get; set; }

    //    public string rd_estimated_price { get; set; }

    //    public string rd_line_no { get; set; }

    //    public string cur_code { get; set; }

    //    public string rd_discount_pct { get; set; }

    //    public string co_no { get; set; }

    //    public string rd_status { get; set; }

    //    public string id_cur_qty { get; set; }
    //}

    public class invs_suppliers
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
    }


    #region Invs044_request
    public class Invs044_requestdetails
    {
        public string randomnumber { get; set; }
        public bool servicevalue { get; set; }
        public List<invs_suppliers> lst_suppliers { get; set; }

        public string invs058_req_no { get; set; }
        public string invs058_rd_line_no { get; set; }
        public string invs058_im_inv_no { get; set; }
        public string invs058_sup_code { get; set; }
        public string invs058_uom_code { get; set; }
        public string invs058_rd_quantity { get; set; }
        public string invs058_rd_estimated_price { get; set; }
        public string invs058_cur_code { get; set; }
        public string invs058_rd_discount_pct { get; set; }
        public string invs058_date_required { get; set; }
        public string invs058_co_no { get; set; }
        public bool invs058_rd_status { get; set; }
        public string invs058_rd_remarks { get; set; }
        public string invs058_rd_item_desc { get; set; }
        public string invs058_dep_name { get; set; }
        public string Dep_Name { get; set; }
        public string sup_name { get; set; }
        public string im_item_code { get; set; }
        public string id_cur_qty { get; set; }
        public bool updatedRD { get; set; }
        public string new_quantity { get; set; }
        public double total { get; set; }
        public string ord_no { get; set; }
        public string od_line_no { get; set; }

        public string invs058_req_date { get; set; }
        public string invs058_req_type { get; set; }
        public string invs058_dm_name { get; set; }
        public string invs058_req_remarks { get; set; }
        public bool chk { get; set; }

        public string opr { get; set; }

        public string ordno { get; set; }

        public string order_ord_date { get; set; }

        public string order_dep_code { get; set; }

        public string order_sup_code { get; set; }

        public string order_cur_code { get; set; }

        public string order_fa_code { get; set; }

        public string req_remarks { get; set; }

        public string order_ord_agency_flag { get; set; }

        public string order_dm_code { get; set; }

        public string order_trt_code { get; set; }

        public string order_pm_code { get; set; }

        public string order_lc_no { get; set; }

        public string order_ord_supl_quot_ref { get; set; }

        public string order_ord_shipment_date { get; set; }

        public string order_ord_loading_note_no { get; set; }

        public string order_ord_loading_note_date { get; set; }

        public string order_ord_confirmation_no { get; set; }

        public string order_ord_status { get; set; }

        public string order_ord_confirmation_date { get; set; }

        public string order_d_ord_no { get; set; }

        public string ordlineno { get; set; }

        public string order_d_req_no { get; set; }

        public string order_d_rd_line_no { get; set; }

        public string order_d_im_inv_no { get; set; }

        public string order_d_uom_code { get; set; }

        public string order_d_od_order_qty { get; set; }

        public string order_d_od_supplier_price { get; set; }

        public string order_d_od_discount_pct { get; set; }

        public string order_d_od_shipped_qty { get; set; }

        public string order_d_od_received_qty { get; set; }

        public string order_d_od_orig_line_no { get; set; }

        public string order_d_od_status { get; set; }

        public string order_d_od_remarks { get; set; }

        public string invs058_ordered_quantity { get; set; }

        public string invs058_sup_price { get; set; }

        public string invs058_discount { get; set; }

        public object dep_code { get; set; }

        public string rd_from_date { get; set; }

        public string rd_up_date { get; set; }

        public object rd_item_desc { get; set; }

        public object request_mode_code { get; set; }

        public object req_range_no { get; set; }

        //---------------------------------------New  Objects
        public string invs058_sm_name { get; set; }

        public string invs058_sm_code { get; set; }

        public string invs058_req_discount_pct { get; set; }

        public string invs058_req_requester_dep { get; set; }

        public bool invs058_req_agency_flag { get; set; }

        public string invs058_dep_code { get; set; }

        public string currentqty { get; set; }

        public string orderedqty { get; set; }

        public string invs058_rqvalue { get; set; }
        public string loginuser { get; set; }
        public string request_issuestatus { get; set; }

        //newly added
        public string invs058_discount_amt { get; set; }//line wise amt
        public string invs058_gross_amt { get; set; }
        public string invs058_heading { get; set; }
        public string invs058_notes { get; set; }
        public string invs058_order_apply_aexp { get; set; }
        public string invs058_order_createdby { get; set; }

        public string invs058_ord_addexps { get; set; }

        public string invs058_ord_dispct { get; set; }

        public string invs058_ord_disamt { get; set; }

        public string invs058_od_total_discount { get; set; }

        public string invs058_od_total_expense { get; set; }

        public string req_imn_no_totqty { get; set; }

        //For vehicle plate no
        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_vehicle_registration_number { get; set; }
        public string sims_transport_vehicle_name_plate { get; set; }

        //New added
        public List<Inv044_OrderDocs> reqdocs { get; set; }

        public string im_item_vat_percentage { get; set; }
        public string im_sell_price { get; set; }
        public string ord_vat_total_amount { get; set; }
        public string od_item_vat_per { get; set; }
        public string od_item_vat_amount { get; set; }
        public string im_malc_rate { get; set; }
        public string im_malc_rate_old { get; set; }
    }

    public class Inv044_OrderDocs
    {
        public string comcode { get; set; }
        public string vouchernum { get; set; }
        public string vouchertype { get; set; }
        public string doclineno { get; set; }
        public string filename { get; set; }
        public string filename_en { get; set; }
        public string orddate { get; set; }
        public string filepath { get; set; }

    }

    public class Invs044_request
    {
        public string invs058_dep_name;
        //public Invs044_request()
        //{
        //    req_details = new List<Invs044_requestdetails>();
        //}
        public string invs058_req_no { get; set; }
        public string invs058_req_date { get; set; }
        public string invs058_req_type { get; set; }
        public string invs058_dep_code { get; set; }
        public string invs058_dm_code { get; set; }
        public string invs058_dm_name { get; set; }
        public string deliveryMode { get; set; }
        public bool invs058_req_agency_flag { get; set; }
        public string invs058_req_discount_pct { get; set; }
        public string invs058_req_requester_dep { get; set; }
        public string invs058_req_remarks { get; set; }
        public string invs058_req_status { get; set; }
        public string invs058_sm_code { get; set; }
        public string invs058_sm_name { get; set; }
        public string salesMan { get; set; }
        public bool updatedR { get; set; }
        public string expecteddate { get; set; }
        public List<Invs044_requestdetails> req_details { get; set; }

        public string im_manufact_serial_no { get; set; }
        public string im_desc { get; set; }
        public string im_model_name { get; set; }


        public string sup_code { get; set; }

        public string sup_name { get; set; }

        public string dm_code { get; set; }

        public string dm_name { get; set; }

        public string pm_code { get; set; }

        public string pm_desc { get; set; }

        public string trt_code { get; set; }

        public string trt_desc { get; set; }

        public string lc_no { get; set; }

        public string bk_code { get; set; }

        public string fa_code { get; set; }

        public string fa_name { get; set; }

        public string excg_curcy_code { get; set; }

        public string excg_curcy_desc { get; set; }

        public string dep_code { get; set; }

        public string dep_name { get; set; }

        public string servicetype { get; set; }

        public string servicevalue { get; set; }

        //////////////New Fields
        public string or_atdesc { get; set; }
        public string or_atvalue { get; set; }
        public string or_atstatus { get; set; }
        public string or_atamount { get; set; }
        public string or_orderno { get; set; }

        public string or_compcurrency { get; set; }

        public string glpr_acct_code { get; set; }
        public string glac_name { get; set; }
        public string glpr_bud_amt { get; set; }
        public string glpr_dr_amt { get; set; }
        public string glpr_cr_amt { get; set; }
        public string transaction { get; set; }
        public string available_balance { get; set; }
    }

    #endregion

    public class requestforquotation
    {
        public string rfq_no { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string rfq_date { get; set; }
        public string rfq_type { get; set; }
        public string requesttype { get; set; }
        public string rfq_dep_code { get; set; }
        public string rfq_dep_name { get; set; }
        public string rfq_quote_ref { get; set; }
        public string rfq_quote_date { get; set; }
        public string rfq_close_date { get; set; }
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string rfq_remarks { get; set; }
        public string rfq_create_user_code { get; set; }
        public string rfq_create_user_name { get; set; }
        public string rfq_creation_date { get; set; }
        public string rfq_approve_user_code { get; set; }
        public string rfq_approve_user_name { get; set; }
        public string rfq_approval_date { get; set; }
        public string rfq_approval_remark { get; set; }
        public string rfq_status { get; set; }
        public bool req_no1 { get; set; }

        public List<requestforquotation_details> lst_rfq_details { get; set; }

        public string im_inv_no { get; set; }
    }

    public class requestforquotation_details
    {
        //********Purchase RFQ_Details Fields*********//
        public string rfq_no { get; set; }
        public string req_no { get; set; }
        public string rfq_line_no { get; set; }
        public string rfq_type { get; set; }
        public string im_inv_no { get; set; }
        public string im_desc { get; set; }
        public string im_item_code { get; set; }
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string rfq_rd_quantity { get; set; }
        public string pqd_quoted_qty { get; set; }
        public string rfq_supply_wanted_date { get; set; }
        public string rfq_remarks_details { get; set; }
        public string pqd_quoted_price { get; set; }
        public string pqd_quoted_supply_date { get; set; }
        public string rfq_status_details { get; set; }
        public string terms { get; set; }

        public string sup_code { get; set; }
        public string sup_name { get; set; }

        public List<q_det> lstqdetails { get; set; }

        public string depcode { get; set; }
    }

    public class q_det
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string pqd_quoted_qty { get; set; }
        public string pqd_quoted_price { get; set; }
        public string pqd_supplydate { get; set; }
        public string pqd_remarks { get; set; }
        public bool sup_itemchk { get; set; }

        public string im_inv_no { get; set; }
        public string req_no { get; set; }
        public string req_type { get; set; }

    }
    

    public class Inv020
    {

        public string comp_para_code { get; set; }

        public string comp_para_desc { get; set; }

        public string comp_para_value { get; set; }
        public string opr { get; set; }
        public int sr_no { get; set; }
        public string inv_comp_code { get; set; }
        public string inv_appl_type { get; set; }
        public string vat_code { get; set; }
        public string vat_desc { get; set; }
        public string vat_value_type { get; set; }
        public string vat_per_value { get; set; }
        public bool vat_included_in_price { get; set; }
        public string vat_from_date { get; set; }
        public string vat_to_date { get; set; }
        public string vat_invoice_acno { get; set; }
        public string vat_refund_acno { get; set; }
        public bool vat_status { get; set; }
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_mod_code { get; set; }
        public string comn_appl_type { get; set; }
        public string comn_app_mode { get; set; }
        public bool comn_appl_status { get; set; }
    }



    public class InvO65
    {
        public string Descr { get; set; }

        public string category { get; set; }
        public string ia_component_qty { get; set; }
        public string im_desc { get; set; }
        public string im_inv_no { get; set; }
        public string im_inv_no_for { get; set; }
        public string im_item_code { get; set; }
        public string opr { get; set; }
        public string subCategory { get; set; }

        public string excg_curcy_code { get; set; }
        public string excg_curcy_desc { get; set; }
        public string fa_code{ get; set; }
	    public string fa_name{ get; set; }
	    public string cur_code { get; set; }
	    public string fa_location_ind{ get; set; }
	    public string fa_orig_code{ get; set; }
	    public string fa_address1{ get; set; }
	    public string fa_address2 { get; set; }
	    public string fa_address3 { get; set; }
	    public string fa_city { get; set; }
	    public string con_code { get; set; }
	    public string fa_telephone_no { get; set; }
	    public string fa_fax_no { get; set; }
	    public string fa_telex_no{ get; set; }
	    public string fa_email_address { get; set; }
	    public string fa_contact_name{ get; set; }
	    public string fa_contact_designation { get; set; }
	    public string fa_discount_pct { get; set; }
	    public string fa_credit_period{ get; set; }
        public string fa_remarks { get; set; }
        public string agent_code { get; set; }
        public string currency_name{ get; set; }
        public string country_name { get; set; }




    }




    public class Inv071
    {

        public string doc_type_code { get; set; }

        public string doc_type_desc { get; set; }

        public bool doc_type_status { get; set; }

        public string opr { get; set; }
    }

    public class Inv018
    {

        public string excg_curcy_code { get; set; }

        public string excg_curcy_desc { get; set; }

        public string comp_code { get; set; }

        public string comp_name { get; set; }

        public string com_code { get; set; }

        public string cur_code { get; set; }

        public string com_name { get; set; }

        public string opr { get; set; }

        public object company_name { get; set; }

        public string excg_curc_desc { get; set; }
    }

    public class Inv131
    {

        public string im_inv_no { get; set; }

        public string im_item_code { get; set; }

        public string im_desc { get; set; }



        public string im_min_level { get; set; }

        public string im_max_level { get; set; }

        public string im_reorder_level { get; set; }

        public string im_malc_rate { get; set; }

        public string im_mark_up { get; set; }
        public string im_sell_price { get; set; }
        public string im_sellprice_special { get; set; }
        public string opr { get; set; }
        public string opr_upd { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
        public string im_average_month_demand { get; set; }
        public string im_approximate_lead_time { get; set; }
    }

    #region  Invs008

    public class Invs008
    {
        public string sup_code { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string sup_name { get; set; }

        public string opr { get; set; }
    }



    #endregion

    #region

    //public class Inv008
    //{

    //    public string sup_code { get; set; }

    //    public string dep_code { get; set; }

    //    public string sup_name { get; set; }

    //    public string dep_name { get; set; }

    //    public string sims_cur_code { get; set; }

    //    public string sims_cur_full_name_en { get; set; }
    //}

    public class Inv011
    {
        public string sg_name { get; set; }

        public string old_sg_name { get; set; }

        public string sg_desc { get; set; }

        public string opr { get; set; }

        public string sg_type { get; set; }

        public string sg_type_nm { get; set; }
    }

    public class Inv017
    {

        public string dm_code { get; set; }

        public string dm_name { get; set; }

        public string dm_lead_time { get; set; }

        public string dm_type { get; set; }

        public string dm_type_desc { get; set; }


        public string opr { get; set; }


        public string dm_type_name { get; set; }
    }

    public class Pers138
    {
        public string dr_grade_code { get; set; }
        public string dr_dept_code { get; set; }
        public string dr_company_code { get; set; }
        public string dr_ot_formula { get; set; }

        public decimal dr_ot_rate { get; set; }


        public string dr_holiday_ot_formula { get; set; }
        public decimal dr_holiday_ot_rate { get; set; }
        public string dr_special_ot_formula { get; set; }
        public decimal dr_special_ot_rate { get; set; }
        public string dr_special_holi_ot_formula { get; set; }
        public decimal dr_special_holi_ot_rate { get; set; }
        public decimal dr_travel_rate { get; set; }

        public string comp_code { get; set; }

        public string co_company_code { get; set; }

        public string co_desc { get; set; }

        public string codp_dept_name { get; set; }

        public string dr_grade_desc { get; set; }

        public string dr_ot_formula_code { get; set; }

        public string dr_holiday_ot_formula_code { get; set; }

        public string dr_special_ot_formula_code { get; set; }

        public string dr_special_holi_ot_formula_code { get; set; }


        public string opr { get; set; }


        public string dr_company_name { get; set; }

        public string dr_dept_name { get; set; }

        public string dr_grade_name { get; set; }

        public string dr_ot_formula_name { get; set; }

        public string dr_holiday_ot_formula_name { get; set; }

        public string dr_special_ot_formula_name { get; set; }

        public string dr_special_holi_ot_formula_name { get; set; }
    }

    public class Sim110
    {
        public string opr;

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string sims_certificate_number { get; set; }

        public string sims_certificate_name { get; set; }

        public string sims_user_group_code { get; set; }

        public string comn_user_group_name { get; set; }

        public bool sims_user_group_enable { get; set; }

        public string comn_user_group_code { get; set; }
    }



    #endregion



    #region Invs140
    public class Invs140
    {
        public string invs140_sup_code { get; set; }
        public string invs140_sup_name { get; set; }
        public string supCode { get; set; }
        public string invs140_pi_no { get; set; }
        public string invs140_cs_prov_no { get; set; }
        public string invs140_fa_code { get; set; }
        public string invs140_fa_name { get; set; }
        public string fa_name { get; set; }
        public string invs140_cur_code { get; set; }
        public string invs140_cur_name { get; set; }
        public string currency { get; set; }
        public string invs140_pi_exchange_rate { get; set; }
        public string invs140_pi_invoice_no { get; set; }
        public string invs140_pi_invoice_date { get; set; }
        public string invs140_pi_amount { get; set; }
        public string invs140_pi_discount_amount { get; set; }
        public string invs140_pi_freight { get; set; }
        public string invs140_pi_insurance { get; set; }
        public string invs140_pi_other_charge { get; set; }
        public string invs140_pi_payment_due_date { get; set; }
        public string invs140_pi_indicator { get; set; }
        public bool invs140_pi_paid_flag { get; set; }
        public string invs140_fp_no { get; set; }
        public string invs140_pm_code { get; set; }
        public string invs140_pm_desc { get; set; }
        public string payment_mode { get; set; }
        public string invs140_pi_remarks { get; set; }
        public string invs140_pi_debit_note_no { get; set; }
        public string invs140_pi_debit_note_date { get; set; }
        public string invs140_pi_invoice_received_date { get; set; }
        public string invs140_pi_creation_date { get; set; }
        public int Count { get; set; }
    }

    public class Invs140_Order_Details
    {
        public string invs140_cs_prov_no { get; set; }
        public string order_no { get; set; }
        public string order_line_no { get; set; }
        public string im_inv_no { get; set; }
        public string Item_code { get; set; }
        public string Item_desc { get; set; }
        public string UOM { get; set; }
        public string Inv_qty { get; set; }
        public string FOC_qty { get; set; }
        public string supl_unit_price { get; set; }
        public string disc { get; set; }
        public string total_price { get; set; }
        public string csd_line_no { get; set; }
        public string pi_no { get; set; }
        public string loc_code { get; set; }
        public string uom_code { get; set; }
        // public string csd_invoiced_qty { get; set; }
        public string csd_binned_qty { get; set; }
        public string csd_received_qty { get; set; }
        public string csd_accepted_qty { get; set; }
        //public string csd_supl_unit_price { get; set; }
        // public string csd_discount_pct { get; set; }
        public string csd_landed_cost { get; set; }
        public string csd_box_no { get; set; }
        public string csd_status { get; set; }
        //public string csd_foc_qty { get; set; }
        public string sup_price { get; set; }
        public string pct_discount { get; set; }
        public string rec_qty { get; set; }
        public bool updated { get; set; }
        public bool btnenable { get; set; }
        public bool btnshortage { get; set; }
    }
    #endregion

    #region Invs028
    public class Invs028
    {
        public string pi_no { get; set; }
        public string cs_prov_no { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string fa_code { get; set; }
        public string fa_name { get; set; }
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string pi_exchange_rate { get; set; }
        public string pi_invoice_no { get; set; }
        public string pi_invoice_date { get; set; }
        public string pi_amount { get; set; }
        public string pi_discount_amount { get; set; }
        public string pi_freight { get; set; }
        public string pi_insurance { get; set; }
        public string pi_other_charge { get; set; }
        public string pi_payment_due_date { get; set; }
        public bool pi_indicator { get; set; }
        public bool pi_paid_flag { get; set; }
        public string fp_no { get; set; }
        //public string fp_desc { get; set; }
        public string pm_code { get; set; }
        public string pm_desc { get; set; }
        public string pi_remarks { get; set; }
        public string pi_debit_note_no { get; set; }
        public string pi_debit_note_date { get; set; }
        public string pi_invoice_received_date { get; set; }
        public string pi_creation_date { get; set; }
        public string sup_sblgr_acno { get; set; }
        public string opr { get; set; }
        public string sup_desc { get; set; }
        public string fa_desc { get; set; }
        public string cur_desc { get; set; }

        public string excg_curcy_code { get; set; }

        public string excg_curc_desc { get; set; }
    }
    #endregion

    #region Invs047(Adjusments)

    public class invsItem
    {
        public string adj_doc_no { get; set; }
        public string dept_code { get; set; }
        public string final_qnt { get; set; }

        public int im_inv_no { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public int item_quantity { get; set; }
        public decimal im_sell_price { get; set; }
    }

    public class invs047
    {
        public string adj_inv_no { get; set; }
        public string item_code { get; set; }
        public string item_desc { get; set; }
        public string oldQty { get; set; }
        public string oldRate { get; set; }
        public string oldVal { get; set; }
        public string newQty { get; set; }
        public string newRate { get; set; }
        public string newValue { get; set; }

        public string dttype { get; set; }
        public string remark { get; set; }
        public string tdate { get; set; }

        public string ar_desc { get; set; }//reason description
        public string ar_code { get; set; }//reasoncode

        public string ad_line_no { get; set; }//adline no.

        public string dco_code { get; set; }//doccode
        public string dco_desc { get; set; }//doctype

        public string loc_code { get; set; }//doccode
        public string loc_name { get; set; }//doctype

        public string sup_name { get; set; }//doctype
        public string sup_sblgr_acno { get; set; }//doctype

        public string adj_grv_no { get; set; }//doctype


        public string sup_code { get; set; }


        public string code { get; set; }
        public string name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string com_code { get; set; }
        public string code_value1 { get; set; }

        public string adj_doc_no { get; set; }

        public string dept_code { get; set; }
        public string loc_code_to { get; set; }        
        public string ad_add_remove_status{get;set;}
        public string new_malc { get; set; }
    }

    #region Invs669 Item Location...

    public class Invs669
    {
        public string opr { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string sup_location_code { get; set; }
        public string sup_location_desc { get; set; }
        public string sup_location_address { get; set; }
        public string sup_location_landmark { get; set; }
        public string sup_location_contact_no { get; set; }
        public string sup_location_contact_person { get; set; }
        public bool sup_location_status { get; set; }
    }

    #endregion

    public class invsItemdetails
    {
        public string im_inv_no { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string item_quantity { get; set; }
        public decimal im_sell_price { get; set; }

        public string dep_code { get; set; }
        public string id_cur_qty { get; set; }
    }


    public class Invs047_it
    {
        public string subcategory_code { get; set; }
        //the variables prefixed by old_ are used for storing codes of respective variable
        public string im_inv_no { get; set; }
        public string dep_code { get; set; }
        public string old_sg_name { get; set; }
        public string invs021_sg_name { get; set; }

        public string im_item_code { get; set; }
        public string im_trade_cat { get; set; }
        public string im_desc { get; set; }

        public string old_dep_code { get; set; }
        public string invs021_dep_code { get; set; }

        public string old_sec_code { get; set; }
        public string sec_code { get; set; }

        public string old_pc_code { get; set; }
        public string pc_code { get; set; }

        public string old_sup_code { get; set; }
        public string invs021_sup_code { get; set; }

        public string old_uom_code { get; set; }
        public string uom_code { get; set; }

        public string old_uom_code_has { get; set; }
        public string uom_code_has { get; set; }

        public string im_model_name { get; set; }
        public bool im_one_time_flag { get; set; }
        public bool im_agency_flag { get; set; }
        public bool im_assembly_ind { get; set; }
        public bool im_supersed_ind { get; set; }
        public string im_supl_catalog_price { get; set; }//
        public string im_supl_price_date { get; set; }
        public string im_supl_min_qty { get; set; }//
        public string im_malc_rate { get; set; }//
        public string im_mark_up { get; set; }//
        public string im_sell_price { get; set; }//
        public string im_sellprice_special { get; set; }//
        public string im_sell_min_qty { get; set; }//
        public bool im_sellprice_freeze_ind { get; set; }
        public string im_approximate_price { get; set; }//
        public string im_estimated_price { get; set; }//
        public string im_malc_rate_old { get; set; }//
        public string im_sell_price_old { get; set; }//
        public string im_supl_buying_price_old { get; set; }//
        public string im_min_level { get; set; }//
        public string im_max_level { get; set; }//
        public string im_reorder_level { get; set; }//
        public string im_economic_order_qty { get; set; }//
        public string im_creation_date { get; set; }
        public bool im_obsolete_excess_ind { get; set; }
        public bool im_status_ind { get; set; }
        public string im_status_date { get; set; }
        public bool im_invest_flag { get; set; }
        public string im_stock_check_date { get; set; }
        public string im_last_receipt_date { get; set; }
        public string im_last_issue_date { get; set; }
        public string im_rso_qty { get; set; }//
        public string im_average_mounth_demand { get; set; }//
        public string im_last_supp_code { get; set; }
        public string im_last_supp_cur { get; set; }
        public string im_supl_buying_price { get; set; }//
        public bool im_proprietary_flag { get; set; }
        public int im_approximate_lead_time { get; set; }
        public bool im_reusable_flag { get; set; }
        public string im_no_of_packing { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string qty_from { get; set; }//
        public string qty_to { get; set; }//
        public string model_name { get; set; }
        public string im_img { get; set; }
        public string remark { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string oldQty { get; set; }
        public string oldRate { get; set; }
        public string oldVal { get; set; }
        public string newQty { get; set; }
        public string newRate { get; set; }
        public string newValue { get; set; }
        public string id_cur_qty { get; set; }
        public string new_calqty { get; set; }
        public string new_calmalcrate { get; set; }
        public string ad_line_no { get; set; }
        public string adj_doc_no { get; set; }
    }
    #endregion


    //pooja

    #region Invs116(Department Constant)
    public class Invs116
    {

        public string dep_code { get; set; }

        public string dep_name { get; set; }

        public string dco_desc { get; set; }

        public string dco_value { get; set; }

        public string dco_code { get; set; }
    }
    #endregion

    #region ServiceMaster
    #region Inv021(Item_Master)
    public class Inv021_Sevice
    {

        public bool im_one_time_flag { get; set; }
        public bool im_agency_flag { get; set; }
        public bool im_assembly_ind { get; set; }
        public bool im_proprietary_flag { get; set; }
        public bool im_reusable_flag { get; set; }
        public bool im_supersed_ind { get; set; }
        public bool im_obsolete_excess_ind { get; set; }
        public bool im_invest_flag { get; set; }
        public bool im_status_ind { get; set; }


        public string dep_code { get; set; }
        public string im_approximate_lead_time { get; set; }
        public string im_approximate_price { get; set; }
        public string im_average_month_demand { get; set; }
        public string im_creation_date { get; set; }
        public string im_desc { get; set; }
        public string im_economic_order_qty { get; set; }
        public string im_estimated_price { get; set; }
        public string im_img { get; set; }
        public string im_inv_no { get; set; }
        public string im_item_code { get; set; }
        public string im_last_issue_date { get; set; }
        public string im_last_receipt_date { get; set; }
        public string im_last_supp_code { get; set; }
        public string im_last_supp_cur { get; set; }
        public string im_malc_rate { get; set; }
        public string im_malc_rate_old { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string im_mark_up { get; set; }
        public string im_max_level { get; set; }
        public string im_min_level { get; set; }
        public string im_model_name { get; set; }
        public string im_no_of_packing { get; set; }
        public string im_reorder_level { get; set; }
        public string im_rso_qty { get; set; }
        public bool im_sellprice_freeze_ind { get; set; }
        public string im_sellprice_special { get; set; }
        public string im_sell_min_qty { get; set; }
        public string im_sell_price { get; set; }
        public string im_sell_price_old { get; set; }
        public string im_status_date { get; set; }
        public string im_stock_check_date { get; set; }
        public string im_supl_buying_price { get; set; }
        public string im_supl_buying_price_old { get; set; }
        public string im_supl_catalog_price { get; set; }
        public string im_supl_min_qty { get; set; }
        public string im_supl_price_date { get; set; }
        public string im_trade_cat { get; set; }
        public string opr { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string sec_code { get; set; }
        public string sec_name { get; set; }
        public string sg_desc { get; set; }
        public string sg_name { get; set; }
        public string sup_code { get; set; }
        public string trade_cat_name { get; set; }
        public string uom_code { get; set; }
        public string uom_code_has { get; set; }
        public string uom_name { get; set; }
        public string dep_name { get; set; }
        public string sup_name { get; set; }
        public string uom_name_has { get; set; }

        public string cur_code { get; set; }

        public string comp_code { get; set; }
        public string fin_year { get; set; }
        public string cost_account { get; set; }
        public string dis_account { get; set; }

        public string compname { get; set; }
        public string cost_accountname { get; set; }
        public string dis_accountname { get; set; }
        public string im_item_vat_percentage { get; set; }


    }
    #endregion

    #endregion

    #region Invs051
    public class Invs051
    {
        public Invs051()
        {
            OrderDetails = new List<Invs051_OrderDetails>();
            ShipmentOrder = new List<Invs051_shipmentOrder>();
        }

        public string Invs051_sr_shipment_no { get; set; }
        public string Invs051_sup_code { get; set; }
        public string Invs051_sup_name { get; set; }
        public string deliveryMode { get; set; }
        public string Invs051_dm_code { get; set; }
        public string Invs051_dm_name { get; set; }
        public string supCode { get; set; }
        public string Invs051_sr_bill_of_lading { get; set; }
        public string Invs051_sr_house_bill_of_lading { get; set; }
        public string Invs051_sr_bill_of_lading_date { get; set; }
        public string Invs051_sr_shipment_port { get; set; }
        public string Invs051_sr_port_via { get; set; }
        public DateTime? Invs051_sr_expected_departure { get; set; }
        public DateTime? Invs051_sr_expected_arrival { get; set; }
        public string Invs051_sr_vessel_name { get; set; }
        public string Invs051_sr_voyage_no { get; set; }
        public string Invs051_sr_container_no { get; set; }
        public string Invs051_sr_consignment_weight { get; set; }
        public string Invs051_sr_consignment_size { get; set; }
        public string Invs051_sr_party_ref { get; set; }
        public string Invs051_sr_party_ref_date { get; set; }
        public string Invs051_sr_remarks { get; set; }
        public string Invs051_so_shipped_qty { get; set; }
        public string Invs051_im_inv_no { get; set; }
        public string Invs051_im_item_code { get; set; }
        public string Invs051_im_desc { get; set; }
        public string Invs051_uom_code { get; set; }
        public string Invs051_od_shipped_qty { get; set; }
        public string Invs051_dep_code { get; set; }
        public string Invs051_dep_name { get; set; }
        public string Invs051_fa_code { get; set; }
        public string Invs051_trtCode { get; set; }
        public string Invs051_trt_desc { get; set; }
        public string Invs051_pm_code { get; set; }
        public string Invs051_pm_desc { get; set; }
        public string Invs051_Ord_Date { get; set; }
        public string Invs051_lc_no { get; set; }
        public string Invs051_ord_no { get; set; }
        public string Invs051_od_line_no { get; set; }
        public List<Invs051_OrderDetails> OrderDetails;
        public List<Invs051_shipmentOrder> ShipmentOrder;
    }

    public class Invs051_shipmentOrder
    {
        public string old_sr_shipment_no { get; set; }
        public string Invs051_sr_shipment_no { get; set; }
        public string ord_no { get; set; }
        public string od_line_no { get; set; }
        public string od_shipped_qty { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string uom_code { get; set; }
        public bool upd { get; set; }
        public bool ischecked { get; set; }

        public string od_sup_code { get; set; }

        public string od_sup_name { get; set; }

        public string od_order_qty { get; set; }
        public string sr_party_ref { get; set; }
        public string dm_name { get; set; }
        public string sr_remarks { get; set; }
        public string sr_bill_of_lading_date { get; set; }
    }

    public class Invs051_OrderDetails
    {
        public string old_sr_shipment_no { get; set; }
        public string Invs051_sr_shipment_no { get; set; }
        public string ord_no { get; set; }
        public string od_line_no { get; set; }
        public string req_no { get; set; }
        public string rd_line_no { get; set; }
        public string im_inv_no { get; set; }
        public string uom_code { get; set; }
        public string od_order_qty { get; set; }
        public string od_supplier_price { get; set; }
        public string od_discount_pct { get; set; }
        public string od_shipped_qty { get; set; }
        public string od_received_qty { get; set; }
        public string od_orig_line_no { get; set; }
        public string status { get; set; }
        public string od_remarks { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public bool upd { get; set; }

        public string od_sup_code { get; set; }

        public string od_sup_name { get; set; }
    }

    #endregion

    #region Invs040
    public class Invs040
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string im_inv_no { get; set; }
        public string item_name { get; set; }
        public string is_uom { get; set; }
        public string uom_name { get; set; }
        public string is_cur_code { get; set; }
        public string is_cur_name { get; set; }
        public string is_price { get; set; }
        public string is_price_date { get; set; }
        public int is_economic_order_qty { get; set; }
        public string im_item_code { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Invs059
    public class Invs059
    {
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string sm_code { get; set; }
        public string sm_name { get; set; }
        public string sm_password { get; set; }
        public string sm_min_comm { get; set; }
        public string sm_max_comm { get; set; }
        public string sm_max_discount { get; set; }
        public string sm_max_disc_pass { get; set; }
        public string sm_target_level_1 { get; set; }
        public string sm_target_level_2 { get; set; }
        public string sm_target_level_3 { get; set; }
        public string sm_pct_1_comm { get; set; }
        public string sm_pct_2_comm { get; set; }
        public bool sm_status { get; set; }
        public bool smStatus { get; set; }
        public string username { get; set; }
        public string em_number { get; set; }
        public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_last_name { get; set; }

        public object opr { get; set; }
    }
    #endregion

    #region Invs133
    public class Invs133
    {
        public string invs133_ccl_reg_no { get; set; }
        public string invs133_ccl_date { get; set; }
        public string invs133_sr_shipment_no { get; set; }
        public string invs133_ccl_customs_entry_no { get; set; }
        public string invs133_ccl_customs_entry_date { get; set; }
        public string invs133_ccl_remarks { get; set; }
        public string invs133_ccl_bill_of_lading_date { get; set; }
        public string invs133_ccl_bill_of_lading { get; set; }
    }
    #endregion

    #region Invs128
    public class Invs128
    {
        public string invs128_dep_code { get; set; }
        public string invs128_sal_type { get; set; }
        public string invs128_sal_name { get; set; }
        public string invs128_stm_indicator { get; set; }
        public string invs128_stm_rate { get; set; }
        public string invs128_stm_sale_acno { get; set; }
        public string invs128_stm_cost_acno { get; set; }
        public string invs128_SaleAccountName { get; set; }
        public string invs128_CostAccountName { get; set; }
        public string invs128_DepName { get; set; }
        public string glma_acct_name { get; set; }
        public string glma_acct_name1 { get; set; }
        public string dep_code { get; set; }
        public string sal_type { get; set; }
        public string sal_name { get; set; }
        public string Dep_Name { get; set; }
        public string Saleaccount { get; set; }
        public string Costaccount { get; set; }
        public bool updateflag { get; set; }

    }
    #endregion

    #region Invs126
    public class Invs126
    {
        public string invs126_sal_type { get; set; }
        public string invs126_sal_name { get; set; }
        public string invs126_sal_indicator { get; set; }
        public bool invs126_S_indicator { get; set; }
        public bool invs126_C_indicator { get; set; }
        public string invs126_sale_acno { get; set; }
        public string invs126_cost_acno { get; set; }
        public string invs126_sale_acno1 { get; set; }
        public string invs126_cost_acno1 { get; set; }
        public string invs126_SaleAccountName { get; set; }
        public string invs126_CostAccountName { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string glma_acct_name1 { get; set; }
        public string Saleaccount { get; set; }
        public string Costaccount { get; set; }


    }
    #endregion

    #region Invs027
    public class Invs027
    {
        public string invs027_pet_code { get; set; }
        public string invs027_pet_desc { get; set; }
        public string invs027_pet_expense_acno { get; set; }
        public bool invs027_pet_flag { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string Expenseaccount { get; set; }
    }
    #endregion

    #region Invs135
    public class Invs135
    {
        public string invs135_fa_code { get; set; }
        public string invs135_fa_name { get; set; }
        public string invs135_cur_code { get; set; }
        public string invs135_fa_location_ind { get; set; }
        public string invs135_orig_code { get; set; }
        public string invs135_address1 { get; set; }
        public string invs135_address2 { get; set; }
        public string invs135_address3 { get; set; }
        public string invs135_fa_city { get; set; }
        public string invs135_con_code { get; set; }
        public string invs135_fa_telephone_no { get; set; }
        public string invs135_fa_fax_no { get; set; }
        public string invs135_fa_telex_no { get; set; }
        public string invs135_email_address { get; set; }
        public string invs135_contact_name { get; set; }
        public string invs135_contact_designation { get; set; }
        public string invs135_discount_pct { get; set; }
        public string invs135_credit_period { get; set; }
        public string invs135_remarks { get; set; }
        public string invs135_country_name { get; set; }
        public string invs135_location { get; set; }
        public string location { get; set; }
        public string currency { get; set; }
        public string invs135_cur_name { get; set; }
    }
    #endregion

    #region Invs130
    public class Invs130
    {
        public string im_inv_no { get; set; }
        public string item_name { get; set; }
        public string im_desc { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string eit_code { get; set; }
        public string eit_desc { get; set; }
        public string eil_create_date { get; set; }
        public string eil_yob_qty { get; set; }
        public string eil_current_qty { get; set; }
        public string eil_alloc_qty { get; set; }
        public string eil_bin_location { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
    }

    #endregion

    #region Ganesh

    #region Unit Of Measurement
    public class Invs001
    {
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public object opr { get; set; }
    }
    #endregion

    #region Uom Conversions
    public class Invs002
    {
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string uom_code_belongs { get; set; }
        public string uom_code_belongs_desc { get; set; }
        public decimal uc_factor { get; set; }
        public object opr { get; set; }
    }

    #endregion

    #region Invs009

    public class Invs009
    {
        public string sdt_type { get; set; }
        public string sdt_desc { get; set; }
        public object opr { get; set; }
    }

    #endregion

    #region Invs010

    public class Invs010
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string sdt_type { get; set; }
        public string sdt_desc { get; set; }
        public string sd_desc { get; set; }
        public object opr { get; set; }
    }

    #endregion

    #region invs038
    public class invs038
    {
        public string opr { get; set; }
        public int im_inv_no { get; set; }
        public string im_desc { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public int id_cur_qty { get; set; }
        public int id_do_qty { get; set; }
        public int id_alloc_qty { get; set; }
        public int id_yob_qty { get; set; }
        public int id_foc_qty { get; set; }

        public string il_bin_loc1 { get; set; }
        public string il_bin_loc2 { get; set; }

        public string im_item_code { get; set; }

        public string loc_name { get; set; }
        public string loc_code { get; set; }

        public string category_name { get; set; }
        public string category_code { get; set; }

        public string subcategory_name { get; set; }
        public string subcategory_code { get; set; }
    }
    #endregion


    #region Invs098(User Profile)
    public class Invs098
    {
        public string up_name { get; set; }
        public string dep_code_belong { get; set; }
        public string depname { get; set; }
        public string sec_code { get; set; }
        public string sec_name { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public bool up_issue_multi_dept { get; set; }
        public bool up_issue_multi_section { get; set; }
        public bool up_issue_multi_loc { get; set; }
        public bool up_agency_flag { get; set; }
        public bool up_status { get; set; }
        public string user_name { get; set; }
    }
    #endregion

    #region Invs145(LC REV)
    public class Invs145_lc
    {
        public string lc_no { get; set; }
        public string lc_revision_date { get; set; }
        public string old_lc_revision_date { get; set; }
        public string lc_exchange_rate { get; set; }
        public string lc_revised_amount { get; set; }
        public string lc_new_revision_amount { get; set; }
        public string lc_bank_charges { get; set; }
        public string lc_valid_date { get; set; }
        public string lc_new_valid_date { get; set; }
    }

    #endregion

    #region Invs052
    public class Invs052
    {
        public string dep_code { get; set; }

        public string dep_name { get; set; }

        public string sec_code { get; set; }

        public string sec_name { get; set; }

        public string sec_stock_account_no { get; set; }

        public string sec_stocking_type { get; set; }

        public string sec_stocking_type_name { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Invs094
    public class Invs094
    {
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string im_inv_no { get; set; }
        public string im_inv_no_for { get; set; }
        public DateTime si_create_date { get; set; }
        public bool si_super_ind { get; set; }
        public string qty_from { get; set; }
        public string qty_to { get; set; }

        public string im_desc { get; set; }

        public string im_item_code1 { get; set; }

        public string im_item_code2 { get; set; }

        public object si_create_date_str { get; set; }

        public string im_item_code { get; set; }
    }
    #endregion
    //end(pooja)

    #region Search Costing Sheet(Inv142)

    public class invs132_cs
    {
        public string cs_prov_date { get; set; }
        public string cs_no { get; set; }
        public string cs_date { get; set; }
        public string cs_authorized_date { get; set; }
        public string dep_code { get; set; }
        public string cs_costaccount { get; set; }
        public string sup_code { get; set; }
        public string cur_code { get; set; }
        public string cs_exchange_rate { get; set; }
        public string fa_code { get; set; }
        public string cur_code_have { get; set; }
        public string cs_forward_exchange_rate { get; set; }
        public string trt_code { get; set; }
        public string dm_code { get; set; }
        public string loc_code { get; set; }
        public string cs_agency_flag { get; set; }
        public string ccl_reg_no { get; set; }
        public string cs_desc { get; set; }
        public string cs_payment_due_date { get; set; }
        public string cs_loading_note_no { get; set; }
        public string cs_loading_note_date { get; set; }
        public string cs_loading_port { get; set; }
        public string cs_carrier_name { get; set; }
        public string cs_supl_basic_price { get; set; }
        public string cs_supl_discount_amount { get; set; }
        public string cs_supl_freight { get; set; }
        public string cs_supl_insurance { get; set; }
        public string cs_supl_other_charge { get; set; }
        public string cs_forward_agent_charge { get; set; }
        public string cs_local_expense { get; set; }
        public string cs_git_pct { get; set; }
        public string cs_git_amount { get; set; }
        public string cs_convertion_factor { get; set; }
        public string cs_landed_cost { get; set; }
        public string cs_received_location { get; set; }
        public string cs_received_by { get; set; }
        public string cs_remarks { get; set; }
        public string cs_status { get; set; }
        public string lc_no { get; set; }

        public string supname { get; set; }
        public string faname { get; set; }
        public string cs_prov_no { get; set; }
        public string status_para { get; set; }
        public string dep_name { get; set; }

        public string sup_name { get; set; }
        public string cs_st_date { get; set; }
        public string cs_ed_date { get; set; }
        public string trt_desc { get; set; }
        public string dm_name { get; set; }

        public string cs_status_nm { get; set; }
        public string order_by { get; set; }
    }


    public class Inv160
    {
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string doc_date { get; set; }
        public string doc_delivery_remarks { get; set; }
        public string doc_no { get; set; }
        public string doc_prov_no { get; set; }
        public string doc_status { get; set; }
        public string doc_total_amount { get; set; }
        public string doc_validity_remarks { get; set; }
        public string dr_code { get; set; }
        public string dt_code { get; set; }
        public string dt_desc { get; set; }
        public string sal_name { get; set; }
        public string sal_type { get; set; }
        public string sm_code { get; set; }
        public string sm_name { get; set; }
        public string sm_user_code { get; set; }
        public string student_name { get; set; }
        public string invs_appl_parameter { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public string cus_account_no { get; set; }
    }
    public class Ucc254
    {
        public string cs_prov_no { get; set; }
        public string csd_line_no { get; set; }
        public string ord_no { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string od_line_no { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string csd_invoiced_qty { get; set; }
        public string csd_foc_qty { get; set; }
        public string uom_code { get; set; }
        public string csd_binned_qty { get; set; }
        public string csd_supl_unit_price { get; set; }



    }


    public class Uccw253_CC
    {
        //invs_custom_clearance_deatils
        public string sup_code { get; set; }
        public string dm_code { get; set; }
        public string sr_bill_of_lading { get; set; }
        public string sr_house_bill_of_lading { get; set; }
        public string sr_bill_of_lading_date { get; set; }
        public string sr_shipment_port { get; set; }
        public string sr_port_via { get; set; }
        public string sr_expected_departure { get; set; }
        public string sr_expected_arrival { get; set; }
        public string sr_vessel_name { get; set; }
        public string sr_voyage_no { get; set; }
        public string sr_container_no { get; set; }
        public string sr_consignment_weight { get; set; }
        public string sr_consignment_size { get; set; }
        public string sr_party_ref { get; set; }
        public string sr_party_ref_date { get; set; }
        public string sr_remarks { get; set; }
        public string sr_shipment_no { get; set; }

        public string ccl_reg_no { get; set; }
        public string ccl_date { get; set; }
        public string ccl_customs_entry_no { get; set; }
        public string ccl_customs_entry_date { get; set; }
        public string ccl_remarks { get; set; }
        public string ccl_bill_of_lading_date { get; set; }
        public string ccl_bill_of_lading { get; set; }
    }


    public class costing_sheet_expenses
    {
        public string cs_prov_no { get; set; }
        public string cse_doc_date { get; set; }
        public string pet_desc { get; set; }
        public string cse_amount { get; set; }
        public string cse_doc_no { get; set; }

    }

    public class Ucw257
    {
        //custom expenses details

        public string ccl_reg_no { get; set; }
        public string pet_code { get; set; }
        public string pet_desc { get; set; }
        public string ce_ref_no { get; set; }
        public string ce_ref_date { get; set; }
        public string ce_amount { get; set; }
        public string cc_no { get; set; }
    }

    #endregion


    #region Invs138
    public class Invs138
    {
        public string lc_no { get; set; }
        public string lc_date { get; set; }
        public string bk_code { get; set; }
        public string cur_code { get; set; }
        public string lc_exchange_rate { get; set; }
        public string sup_code { get; set; }
        public string lc_bank_lc_no { get; set; }
        public string lc_open_date { get; set; }
        public string lc_beneficiary { get; set; }
        public string lc_amount { get; set; }
        public string lc_revised_amount { get; set; }
        public string lc_utilized_amount { get; set; }
        public string lc_cancelled_amount { get; set; }
        public string lc_bank_charges { get; set; }
        public string lc_valid_date { get; set; }
        public string lc_terms { get; set; }
        public string lc_remarks { get; set; }
        public string lc_close_date { get; set; }
        public string Lc_no1 { get; set; }
        public string bk_name { get; set; }
        public string sup_name { get; set; }
        public string excg_curcy_desc { get; set; }
        public string excg_curcy_code { get; set; }
    }
    #endregion

    #endregion
    public class Invs018
    {
        public string company_code { get; set; }
        public string company_short_name { get; set; }
        public string company_name { get; set; }
        public string comp_currency_name { get; set; }
        public string comp_currency_code { get; set; }
    }

    //Invs020
    public class Invs020
    {
        public string cp_code { get; set; }
        public string cp_desc { get; set; }
        public string cp_value { get; set; }

    }

    #region  Invs005
    public class Invs005
    {

        public string dr_code { get; set; }
        public string dept_code { get; set; }
        public string dr_desc { get; set; }
        public string dr_account_no { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string Saleaccount { get; set; }
        public string glma_acct_name { get; set; }
        public string account_code { get; set; }
    }

    #endregion

    #region Invs006 (Supplier Master)
    public class Invs006
    {
        public string sup_code { get; set; }
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
        public string sup_name { get; set; }
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string sup_location_ind { get; set; }
        public string sup_location_ind_name { get; set; }
        public string sup_orig_code { get; set; }
        public string sup_contact_name { get; set; }
        public string sup_contact_designation { get; set; }
        public string sup_address1 { get; set; }
        public string sup_address2 { get; set; }
        public string sup_address3 { get; set; }
        public string sup_state { get; set; }
        public string sup_state_name { get; set; }
        public string sup_city { get; set; }
        public string sup_city_name { get; set; }
        public string con_code { get; set; }
        public string con_name { get; set; }
        public string sup_tel_no { get; set; }
        public string sup_fax_no { get; set; }
        public string sup_telex_no { get; set; }
        public string sup_mobile_no { get; set; }
        public string sup_email_address { get; set; }
        public string sup_web_site { get; set; }
        public string sup_merchandise_desc { get; set; }
        public int sup_credit_period { get; set; }
        public int sup_discount_pct { get; set; }
        public int sup_lead_time_air { get; set; }
        public int sup_lead_time_sea { get; set; }
        public int sup_lead_time_road { get; set; }
        public int sup_lead_time_courier { get; set; }
        public string sup_type { get; set; }
        public string sup_type_name { get; set; }
        public decimal sup_agent_comm_pct { get; set; }
        public string sup_remarks { get; set; }
        public string sup_rating { get; set; }
        public string sup_sblgr_acno { get; set; }
        public string sup_sblgr_name { get; set; }
        public string opr { get; set; }

        public string slac_ldgr_code { get; set; }
        public string supplier_desc_type { get; set; }

        public string sup_contact_person { get; set; }

        public string sup_location_desc { get; set; }
        public string sup_location_address { get; set; }
        public string sup_location_landmark { get; set; }

        public string gldd_acct_code { get; set; }

        public string gldd_acct_name { get; set; }

        public string gldd_party_ref_no { get; set; }

        public string gldd_dept_code { get; set; }

        public string gldd_dept_name { get; set; }
    }
    #endregion

    #region  Invs017
    public class Invs017
    {
        public string invs_code { get; set; }
        public string invs_name { get; set; }
        public decimal invs_lead_time { get; set; }
        public string invs_type { get; set; }
    }

    #endregion

    #region Invs011 (Supplier Group)

    public class Invs011
    {
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
    }


    #endregion

    #region Invs040(Item Supplier)
    public class invs040
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public int im_inv_no { get; set; }
        public string item_name { get; set; }
        public string is_uom { get; set; }
        public string uom_name { get; set; }
        public string is_cur_code { get; set; }
        public string is_cur_name { get; set; }
        public decimal is_price { get; set; }
        public DateTime is_price_date { get; set; }
        public int is_economic_order_qty { get; set; }
        public string im_item_code { get; set; }
    }
    #endregion

    #region Invs042 (Bank)
    public class Invs042
    {
        public string bk_code { get; set; }
        public string bk_name { get; set; }
        public string bk_orig_code { get; set; }
        public string bk_address1 { get; set; }
        public string bk_address2 { get; set; }
        public string bk_address3 { get; set; }
        public string bk_city { get; set; }
        public string bk_phone_no { get; set; }
        public string bk_fax_no { get; set; }
        public string bk_telex_no { get; set; }
        public string bk_contact_person { get; set; }
        public string bk_designation { get; set; }
        public string con_code { get; set; }
        public string con_name { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Invs053(RSO Parameter)
    public class invs053
    {
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public decimal rp_storage_cost { get; set; }
        public decimal rp_ordering_cost { get; set; }
        public decimal rp_annual_holding_cost { get; set; }
        public int rp_ordering_cycle { get; set; }
        public int rp_reserve_days { get; set; }
    }
    #endregion

    #region Invs054(RSO Schedule)
    public class invs054
    {
        public string rs_date { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string old_rs_date { get; set; }
        public string old_sup_code { get; set; }


    }
    #endregion

    #region Invs060 (Valid customers)
    public class Invs060
    {
        public string dep_code { get; set; }
        public string ic_account_no { get; set; }
        public string sal_type { get; set; }
        public string sal_name { get; set; }
        public string dep_name { get; set; }
        public string ic_account_name { get; set; }
        public string sal_indicator { get; set; }
        public string sale_acno { get; set; }
        public string cost_acno { get; set; }
    }
    #endregion

    #region Invs062 (Location)
    public class Invs062
    {
        public string location_name { get; set; }
        public string location_code { get; set; }
        public string contact_person { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string phone { get; set; }
        public string fax_no { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Invs063(ProductCode)

    public class Invs063
    {
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string pc_budget_code { get; set; }
        public string gl_item_no { get; set; }
        public string gl_accname { get; set; }
        public string vat_profile_no { get; set; }
        public string vat_profile_type { get; set; }
        public string vat_profile_percentage { get; set; }
        public string vat_profile_desc { get; set; }
    }

    #endregion

    #region Invs064(Trade Term)
    public class Invs064
    {
        public string trt_code { get; set; }
        public string trt_desc { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Invs118(Department)

    public class Invs118
    {
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
    }

    #endregion

    #region Invs124 (InvsParameter)
    public class invs124
    {
        public string invs_comp_code { get; set; }
        public string invs_comp_name { get; set; }
        public string invs_appl_code { get; set; }
        public string invs_appl_name { get; set; }
        public string invs_appl_form_field { get; set; }
        public string invs_appl_parameter { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public string invs_appl_form_field_value2 { get; set; }
        public string invs_appl_form_field_value3 { get; set; }
        public string invs_appl_form_field_value4 { get; set; }
        public string invs_appl_mod_code { get; set; }
        public string invs_appl_mod_name { get; set; }
    }

    #endregion

    public class invsClass
    {
        public string code { get; set; }
        public string name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string com_code { get; set; }
        public string code_value1 { get; set; }
    }

    #region Invs058
    public class Invs058_item
    {
        public string req_mode { get; set; }
        public string req_mode_name { get; set; }

        public string request_mode_code { get; set; }
        public string request_mode_name { get; set; }

        public string rd_estimate_price { get; set; }
        public string reqQtyespricetotal { get; set; }


        public object category_code { get; set; }

        public object subcategory_code { get; set; }

        public string category_name { get; set; }

        public string subcategory_name { get; set; }

        public string quantity_assembly { get; set; }

        public string excg_curcy_code { get; set; }

        public decimal req_no { get; set; }

        public string sup_code { get; set; }

        public string rd_date_required { get; set; }

        public string rd_quantity { get; set; }

        public string rd_remarks { get; set; }

        public string opr { get; set; }

        public string dept_code { get; set; }

        public string dept_name { get; set; }
        public string im_item_vat_percentage { get; set; }
        public string req_type { get; set; }
        public string dep_code { get; set; }

        public string req_status { get; set; }

        public string req_type_name { get; set; }

        public string req_status_name { get; set; }

        public string dm_code { get; set; }

        public decimal req_discount_pct { get; set; }

        public string req_requester_dep { get; set; }

        public string req_remarks { get; set; }

        public string req_agency_flag { get; set; }

        public string dm_name { get; set; }

        public string sm_code { get; set; }

        public string category { get; set; }

        public string subCategory { get; set; }

        public string curr_qty { get; set; }

        public string do_qty { get; set; }

        public string ava_aty { get; set; }

        public string request_status_code { get; set; }

        public string request_status_name { get; set; }

        public string request_type_code { get; set; }

        public string request_type_name { get; set; }

        public string req_date { get; set; }

        public string sm_name { get; set; }

        public string salesman_code { get; set; }

        public string salesman_name { get; set; }

        public string excg_curcy_desc { get; set; }

        public bool im_assembly_ind { get; set; }

        //the variables prefixed by old_ are used for storing codes of respective variable
        public string im_inv_no { get; set; }
        public string im_req_no { get; set; }
        public string im_line_no { get; set; }
        public string old_sg_name { get; set; }
        public string invs021_sg_name { get; set; }

        public string im_item_code { get; set; }
        public string im_trade_cat { get; set; }
        public string im_desc { get; set; }

        public string old_dep_code { get; set; }
        public string invs021_dep_code { get; set; }

        public string old_sec_code { get; set; }
        public string sec_code { get; set; }

        public string old_pc_code { get; set; }
        public string pc_code { get; set; }

        public string old_sup_code { get; set; }
        public string invs021_sup_code { get; set; }

        public string old_uom_code { get; set; }
        public string uom_code { get; set; }

        public string old_uom_code_has { get; set; }
        public string uom_code_has { get; set; }

        public string im_model_name { get; set; }
        public bool im_one_time_flag { get; set; }
        public bool im_agency_flag { get; set; }
        //public bool im_assembly_ind { get; set; }
        public bool im_supersed_ind { get; set; }
        public string im_supl_catalog_price { get; set; }//
        public string im_supl_price_date { get; set; }
        public string im_supl_min_qty { get; set; }//
        public string im_malc_rate { get; set; }//
        public string im_mark_up { get; set; }//
        public string im_sell_price { get; set; }//
        public string im_sellprice_special { get; set; }//
        public string im_sell_min_qty { get; set; }//
        public bool im_sellprice_freeze_ind { get; set; }
        public string im_approximate_price { get; set; }//
        public string im_estimated_price { get; set; }//
        public string im_malc_rate_old { get; set; }//
        public string im_sell_price_old { get; set; }//
        public string im_supl_buying_price_old { get; set; }//
        public string im_min_level { get; set; }//
        public string im_max_level { get; set; }//
        public string im_reorder_level { get; set; }//
        public string im_economic_order_qty { get; set; }//
        public string im_creation_date { get; set; }
        public bool im_obsolete_excess_ind { get; set; }
        public bool im_status_ind { get; set; }
        public string im_status_date { get; set; }
        public bool im_invest_flag { get; set; }
        public string im_stock_check_date { get; set; }
        public string im_last_receipt_date { get; set; }
        public string im_last_issue_date { get; set; }
        public string im_rso_qty { get; set; }//
        public string im_average_mounth_demand { get; set; }//
        public string im_last_supp_code { get; set; }
        public string im_last_supp_cur { get; set; }
        public string im_supl_buying_price { get; set; }//
        public bool im_proprietary_flag { get; set; }
        public int im_approximate_lead_time { get; set; }
        public bool im_reusable_flag { get; set; }
        public string im_no_of_packing { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string qty_from { get; set; }//
        public string qty_to { get; set; }//
        public string model_name { get; set; }
        public string im_img { get; set; }
        public string im_assembly_ind_s { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string oldQty { get; set; }
        public string oldRate { get; set; }
        public string oldVal { get; set; }
        public string newQty { get; set; }
        public string newRate { get; set; }
        public int newValue { get; set; }
        public string newRemark { get; set; }
        public string id_cur_qty { get; set; }
        public string Req_Date { get; set; }
        public string new_calqty { get; set; }
        public string new_calmalcrate { get; set; }
        public string sg_desc { get; set; }
        public string sup_name { get; set; }
        public string dep_name { get; set; }
        public string uom_name { get; set; }
        public string sec_name { get; set; }
        public bool updated { get; set; }

        //For Workflow
        public string status { get; set; }

        //For Sales Document

        public string il_cur_qty { get; set; }
        public string item_location_name { get; set; }
        public string sg_name { get; set; }
        public int dd_qty { get; set; }
        public decimal dd_sell_price { get; set; }
        public decimal ia_component_qty { get; set; }
        
        public string req_approved_by_user_code { get; set; }

        public string doc_no { get; set; }
        public string req_approved_date { get; set; }
        public string doc_line_no { get; set; }
        public string item_inv_no { get; set; }
        public string grn_inv_no { get; set; }
        public string r_qty { get; set; }
        public string grv_no { get; set; }
        public string t_qty { get; set; }
        public string adj_status { get; set; }
        public string rd_line_no { get; set; }
        public string remark { get; set; }
        public string ucode { get; set; }
        public string typ { get; set; }
        public string invs_appl_form_field { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public decimal im_sell_price_including_vat { get; set; }
        public string im_revenue_acno { get; set; }
        public string im_cost_acno { get; set; }
        public string im_vat_sale_profile { get; set; }
    }

    public class Invs058
    {
        public Invs058()
        {
            req_details = new List<Invs058_item>();
        }
        public string invs058_req_no { get; set; }
        public string invs058_req_date { get; set; }
        public string invs058_req_type { get; set; }
        public string invs058_dep_code { get; set; }
        public string invs058_dm_code { get; set; }
        public string invs058_dm_name { get; set; }
        public string deliveryMode { get; set; }
        public bool invs058_req_agency_flag { get; set; }
        public string invs058_req_discount_pct { get; set; }
        public string invs058_req_requester_dep { get; set; }
        public string invs058_req_remarks { get; set; }
        public string invs058_req_status { get; set; }
        public string invs058_sm_code { get; set; }
        public string invs058_sm_name { get; set; }
        public string salesMan { get; set; }
        public string invs058_rd_line_no { get; set; }
        public string invs058_im_inv_no { get; set; }
        public string invs058_sup_code { get; set; }
        public string invs058_uom_code { get; set; }
        public string invs058_rd_quantity { get; set; }
        public string invs058_rd_estimated_price { get; set; }
        public string invs058_cur_code { get; set; }
        public string invs058_rd_discount_pct { get; set; }
        public DateTime? invs058_date_required { get; set; }
        public string invs058_co_no { get; set; }
        public bool invs058_rd_status { get; set; }
        public string invs058_rd_remarks { get; set; }
        public string invs058_rd_item_desc { get; set; }
        public string invs058_dep_name { get; set; }
        public string Dep_Name { get; set; }
        public List<Invs058_item> req_details;

    }

    public class Invs058_request_Parameters
    {
        public string req_type_code { get; set; }
        public string req_type_name { get; set; }
        public string req_agency_flag_code { get; set; }
        public string req_agency_flag_name { get; set; }
        public string req_status_code { get; set; }
        public string req_status_name { get; set; }
        public string requst_status { get; set; }
        public string request_type { get; set; }
    }
    #endregion

    #region ItemSerive Search

    public class ItemSerive
    {
        public string req_no { get; set; }
        public string dept_code { get; set; }
        public string rd_from_required { get; set; }
        public string rd_up_required { get; set; }
        public string rd_item_desc { get; set; }
        public string req_type { get; set; }
        public string request_mode_code { get; set; }
        public string req_range_no { get; set; }
        public string val { get; set; }
        public string rd_quantity { get; set; }
        public string final_qnt { get; set; }
        public string im_inv_no { get; set; }
        public string rd_line_no { get; set; }
        public string username { get; set; }
        public string sm_code { get; set; }
    }

    #endregion

    #region Invs021 (Item Master)
    public class Invs021
    {
        //the variables prefixed by old_ are used for storing codes of respective variable
        public string im_inv_no { get; set; }

        public string old_sg_name { get; set; }
        public string invs021_sg_name { get; set; }

        public string im_item_code { get; set; }
        public string im_trade_cat { get; set; }
        public string im_desc { get; set; }

        public string old_dep_code { get; set; }
        public string invs021_dep_code { get; set; }

        public string old_sec_code { get; set; }
        public string sec_code { get; set; }

        public string old_pc_code { get; set; }
        public string pc_code { get; set; }

        public string old_sup_code { get; set; }
        public string invs021_sup_code { get; set; }

        public string old_uom_code { get; set; }
        public string uom_code { get; set; }

        public string old_uom_code_has { get; set; }
        public string uom_code_has { get; set; }

        public string im_model_name { get; set; }
        public bool im_one_time_flag { get; set; }
        public bool im_agency_flag { get; set; }
        public bool im_assembly_ind { get; set; }
        public bool im_supersed_ind { get; set; }
        public string im_supl_catalog_price { get; set; }//
        public string im_supl_price_date { get; set; }
        public string im_supl_min_qty { get; set; }//
        public string im_malc_rate { get; set; }//
        public string im_mark_up { get; set; }//
        public string im_sell_price { get; set; }//
        public string im_sellprice_special { get; set; }//
        public string im_sell_min_qty { get; set; }//
        public bool im_sellprice_freeze_ind { get; set; }
        public string im_approximate_price { get; set; }//
        public string im_estimated_price { get; set; }//
        public string im_malc_rate_old { get; set; }//
        public string im_sell_price_old { get; set; }//
        public string im_supl_buying_price_old { get; set; }//
        public string im_min_level { get; set; }//
        public string im_max_level { get; set; }//
        public string im_reorder_level { get; set; }//
        public string im_economic_order_qty { get; set; }//
        public string im_creation_date { get; set; }
        public bool im_obsolete_excess_ind { get; set; }
        public bool im_status_ind { get; set; }
        public string im_status_date { get; set; }
        public bool im_invest_flag { get; set; }
        public string im_stock_check_date { get; set; }
        public string im_last_receipt_date { get; set; }
        public string im_last_issue_date { get; set; }
        public string im_rso_qty { get; set; }//
        public string im_average_mounth_demand { get; set; }//
        public string im_last_supp_code { get; set; }
        public string im_last_supp_cur { get; set; }
        public string im_supl_buying_price { get; set; }//
        public bool im_proprietary_flag { get; set; }
        public int im_approximate_lead_time { get; set; }
        public bool im_reusable_flag { get; set; }
        public string im_no_of_packing { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string qty_from { get; set; }//
        public string qty_to { get; set; }//
        public string model_name { get; set; }
        public string im_img { get; set; }


        public string category_name { get; set; }

        public string category_code { get; set; }

        public string subcategory_name { get; set; }

        public string subcategory_code { get; set; }
    }
    #endregion








    #region FinalPayment
    public class Invs137
    {
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string excg_curcy_code { get; set; }
        public string excg_curcy_desc { get; set; }
        public string pm_code { get; set; }
        public string pm_desc { get; set; }
        public string fp_no { get; set; }
        public string fp_date { get; set; }
        public string fa_code { get; set; }
        public string fp_exchange_rate { get; set; }
        public string fp_document_no { get; set; }
        public string fp_chargable_to { get; set; }
        public string fp_remarks { get; set; }
        public string fp_amount { get; set; }
        public string fa_name { get; set; }
        public string fp_document_date { get; set; }

    }
    #endregion




    #region CustomExp
    public class Invs134
    {
        public string pet_code { get; set; }
        public string pet_desc { get; set; }

        public string ccl_reg_no { get; set; }
        public string cc_no { get; set; }
        public string ce_ref_no { get; set; }
        public string ce_ref_date { get; set; }
        public string ce_amount { get; set; }
    }
    #endregion

    #region CustomCheque
    public class Invs136
    {
        public string bk_code { get; set; }
        public string bk_name { get; set; }
        public string ccl_reg_no { get; set; }
        public string cc_no { get; set; }
        public string cc_date { get; set; }

        public string cc_not_exceeding_amt { get; set; }

        public string cc_cleark { get; set; }

        public string cc_amount { get; set; }


    }
    #endregion

    #region Library
    public class Sims136
    {
        public int sims_library_transaction_number { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_qty { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_reserve_release_date { get; set; }
        public decimal sims_library_item_rate { get; set; }
        public decimal sims_library_item_line_total { get; set; }
        public bool sims_library_item_transaction_status { get; set; }


        //PP028
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_author1 { get; set; }

        public string sims_library_item_actual_return_date { get; set; }
    }
    #endregion

    #region InterstType
    public class Invs143
    {
        public string int_code { get; set; }
        public string int_desc { get; set; }
    }
    #endregion

    public class Inv100
    {
        public string bin_code { get; set; }
        public string bin_desc { get; set; }
        public object opr { get; set; }
        public string ord_no { get; set; }
    }
    public class InvSto
    {
        public List<InvSto_sub_list> sublist1 { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_acaedmic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string invoice_no { get; set; }
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string sell_amt { get; set; }     
        
        public string dd_issue_qty { get; set; }
        public string doc_issue_date { get; set; }
        public string creation_user { get; set; }
        public string sims_icon { get; set; }
        public string doc_prov_date { get; set; }
        public string doc_reference_prov_no { get; set; }

        public string doc_creation_date { get; set; }
        public string doc_dept{ get; set; }

        public string doc_sr_code { get; set; }
        public string sims_appl_form_field_value1 { get; set; }

        public string dt_code { get; set; }

        public string im_inv_no { get; set; }
    }


    public class InvSto_sub_list
    {
        public string invoice_no { get; set; }
        public string im_item_code { get; set; }
        public string dd_issue_qty { get; set; }
        public string im_desc { get; set; }
        public string id_cur_qty { get; set; }
        public string dd_qty { get; set; }
        public string doc_issue_date { get; set; }
        public string issued_dd_issue_qty { get; set; }
        public string creation_date { get; set; }
        public string issue_quantity { get; set; }
        public string remarks { get; set; }

        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string sell_amt { get; set; }

        public string creation_user { get; set; }
        public string sims_icon { get; set; }
        public string doc_prov_date { get; set; }
        public string doc_creation_date { get; set; }

        public string doc_dept { get; set; }
        public string im_inv_no { get; set; }
        public string loc_code { get; set; }
        public string dd_physical_qty { get; set; }
        public string im_item_vat_percentage { get; set; }
        public string dd_item_vat_amount { get; set; }
        public string dd_sell_value_final { get; set; }
        public string dd_sell_price_final { get; set; }
        public string dd_sell_price { get; set; }
        public string dd_sell_price_discounted { get; set; }
        public string dd_line_total { get; set; }
        public string dd_line_discount_pct { get; set; }
        public string dd_line_discount_amount { get; set; }
        public string doc_other_charge_amount { get; set; }
        public string doc_discount_pct { get; set; }
        public string doc_discount_amount { get; set; }
        public string doc_vat_total_amount { get; set; }
        public string doc_round_value { get; set; }
        public string dd_payment_mode { get; set; }
        public string sg_name { get; set; }
        public string dt_code { get; set; }
        public string sal_type { get; set; }
        public string sm_code { get; set; }
        public string doc_order_ref_no { get; set; }
        public string doc_remark { get; set; }
        public string dd_cheque_number { get; set; }
        public string dd_cheque_date { get; set; }
        public string dd_cheque_bank_code { get; set; }
        public string dd_credit_card_code { get; set; }
        public string doc_total_amount { get; set; }
        public string dd_item_vat_per { get; set; }
        public string doc_status { get; set; }
        public string dd_status { get; set; }


        public string report_im_invoice_no { get; set; }
        public string report_invoice_date { get; set; }
        public string report_invoice_voucher_no { get; set; }
        public string report_particulars { get; set; }
        public string report_invoice_qty { get; set; }
        public string report_issued_qty { get; set; }



    }
    public class Inv126
    {
        public string cost_account_name { get; set; }
        public string cost_acno { get; set; }
        public string full_account_naame { get; set; }
        public string full_account_name { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public object opr { get; set; }
        public string sale_acno { get; set; }
        public string sal_account_name { get; set; }
        public string salesind { get; set; }
        public string sal_name { get; set; }
        public string sal_type { get; set; }
        public string sal_indicator { get; set; }
        public string glma_acct_code1 { get; set; }
        public string ar_account_no { get; set; }
        public string acno1 { get; set; }
        public string dep_code { get; set; }
        public string acno { get; set; }
    }

    public class Inv046
    {
        public object opr { get; set; }
        public string ar_account_no { get; set; }
        public string ar_code { get; set; }
        public string ar_desc { get; set; }
        public bool ar_indicator { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string acno { get; set; }
        public string glma_acct_name { get; set; }
    }

    public class Inv128
    {
        public string cost_acno { get; set; }
        public string depcodewithname { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string full_depart_name { get; set; }
        public string full_sal_name { get; set; }
        public string old_sal_type { get; set; }
        public object opr { get; set; }
        public string saletypewthname { get; set; }
        public string sale_acno { get; set; }
        public string sal_indicator { get; set; }
        public string sal_name { get; set; }
        public string sal_type { get; set; }
        public string stm_cost_acno { get; set; }
        public string stm_indicator { get; set; }
        public string stm_rate { get; set; }
        public string stm_sale_acno { get; set; }
        public bool updateflag { get; set; }
    }


    public class invs035
    {
        public string servicevalue { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string doc_prov_no { get; set; }
        public string doc_prov_date { get; set; }
        public int doc_no { get; set; }
        public string doc_date { get; set; }
        public string dt_code { get; set; }
        public string dt_desc { get; set; }
        public string dr_code { get; set; }
        public string sm_code { get; set; }
        public string sm_name { get; set; }
        public string up_name { get; set; }
        public string sal_type { get; set; }
        public string sal_type_name { get; set; }
        public decimal doc_total_amount { get; set; }
        public string doc_status { get; set; }
        public string doc_status_name { get; set; }
        public string cus_account_no { get; set; }
        public string ic_account_no { get; set; }
        public string ic_account_name { get; set; }
        public string doc_special_name { get; set; }
        public string dep_code_caused_by { get; set; }
        public string doc_order_ref_no { get; set; }
        public string doc_other_charge_desc { get; set; }
        public decimal doc_other_charge_amount { get; set; }
        public decimal doc_discount_pct { get; set; }
        public decimal doc_discount_amount { get; set; }
        public string doc_delivery_remarks { get; set; }
        public string doc_validity_remarks { get; set; }
        public string ow_id { get; set; }
        public string doc_jobcard_dept { get; set; }
        public string doc_jobcard_loc { get; set; }
        public string doc_jobcard_no { get; set; }
        public string sd_icc_code { get; set; }
        public string doc_narration { get; set; }
        public string sec_code { get; set; }
        public string creation_date { get; set; }
        public string creation_user { get; set; }
        public string creation_term { get; set; }
        public string modification_date { get; set; }
        public string modification_user { get; set; }
        public string modification_term { get; set; }
        public string doc_cheque_no { get; set; }
        public string doc_cheque_date { get; set; }
        public string doc_cheque_bank { get; set; }
        public string sm_password { get; set; }
        public decimal sm_max_discount { get; set; }
        //Sales_document_details variables
        public string dd_line_no { get; set; }
        public int im_inv_no { get; set; }
        public string loc_code { get; set; }
        public decimal dd_sell_price { get; set; }
        public decimal dd_sell_price_discounted { get; set; }
        public decimal dd_sell_price_final { get; set; }
        public decimal dd_sell_value_final { get; set; }
        public decimal dd_sell_cost_total { get; set; }
        public decimal dd_qty { get; set; }
        public decimal dd_outstanding_qty { get; set; }
        public string dd_line_no_have_previous { get; set; }
        public int doc_prov_no_have_previous { get; set; }
        public string dep_code_have_previous { get; set; }
        public decimal dd_line_discount_pct { get; set; }
        public decimal dd_line_discount_amount { get; set; }
        public string dd_core_exchange { get; set; }
        public string dd_discount_type { get; set; }
        public string dd_status { get; set; }
        public string test { get; set; }
        public decimal dd_line_total { get; set; }
        public decimal dd_physical_qty { get; set; }
        public string dd_remarks { get; set; }
        public string doc_type_have_previous { get; set; }
        public int doc_no_have_previous { get; set; }
        //end of sales_document_details
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string im_invest_flag { get; set; }
        public string item_location_code { get; set; }
        public string item_location_name { get; set; }
        public string sg_name { get; set; }
        public string sg_desc { get; set; }
        public int item_quantity { get; set; }
        public decimal im_sell_price { get; set; }
        public string im_agency_flag { get; set; }
        public decimal im_malc_rate { get; set; }
        public int im_mark_up { get; set; }
        public string core_discnt { get; set; }
        public string core_discnt_name { get; set; }
        public bool exclude { get; set; }
        public decimal discnt_price { get; set; }
        public decimal discnt_percentage { get; set; }
        public decimal final_price { get; set; }
        public decimal final_value { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public decimal slma_cr_limit { get; set; }
        public bool slma_cash_crd_ind { get; set; }
        public bool slma_stop_cr_ind { get; set; }
        public int slma_credit_prd { get; set; }
        public decimal slma_int_rate { get; set; }
        public int slma_int_prd { get; set; }
        public string slma_reval_date { get; set; }
        public string slma_credit_remark { get; set; }
        public decimal stm_rate { get; set; }
        public string stm_indicator { get; set; }

        public string code { get; set; }
        public decimal doc_vat_total_amount { get; set; }

        //For sale documents
        public string original_qty { get; set; }

        public string studentenroll { get; set; }
        public string studentname { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }

        public string dd_payment_mode { get; set; }
        public string dd_cheque_number { get; set; }
        public string dd_cheque_date { get; set; }
        public string dd_cheque_bank_code { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string grade_name { get; set; }
        public string section_name { get; set; }
        public string house_name { get; set; }

        //Variables for external customer
        public string up_num { get; set; }
        public string up_actual_name { get; set; }

        public decimal returned_qty { get; set; }
        public decimal returned_price { get; set; }

        public string search_dtcode { get; set; }
        public string search_smcode { get; set; }
        public string search_icaccno { get; set; }

        public string name { get; set; }

        public string category_name { get; set; }

        public string category_code { get; set; }

        public string subcategory_name { get; set; }

        public string subcategory_code { get; set; }

        public string bk_code { get; set; }

        public string bk_name { get; set; }

        public string pc_code { get; set; }

        public string sup_code { get; set; }

        public string doc_remark { get; set; }
        public string im_item_vat_percentage { get; set; }
        public decimal dd_item_vat_per { get; set; }
        public decimal dd_item_vat_amount { get; set; }

        //Newly added for item request
        public string doc_require_date { get; set; }

        public string pc_grade_code { get; set; }

        public string grade_code { get; set; }


        public string doc_round_value { get; set; }
        public string old_doc_prov_no { get; set; }
        public string doc_reference_prov_no { get; set; }
        public string save_finalize_flag { get; set; }
        public string uom_code { get; set; }
        public string uom_name { get; set; }
        public string invs_appl_parameter { get; set; }
        public string invs_appl_form_field_value1 { get; set; }

        public string doc_cash_amount { get; set; }
        public string doc_check_amount { get; set; }
        public string doc_credit_card_amount { get; set; }
        public string doc_dd_amount { get; set; }
        public string doc_bank_transfer_amount { get; set; }
        public string doc_card_payment_amount { get; set; }
        public string dd_demand_draft_number { get; set; }
        public string dd_demand_draft_date { get; set; }
        public string dd_demand_draft_bank_code { get; set; }
        public string dd_credit_card_number { get; set; }
        public string dd_credit_card_date { get; set; }
        public string dd_credit_card_bank_code { get; set; }
        public string dd_bank_transfer_trans_number { get; set; }
        public string dd_bank_transfer_trans_date { get; set; }
        public string dd_bank_transfer_trans_bank_code { get; set; }
        public string dd_online_payemnt_trans_number { get; set; }
        public string payment_type { get; set; }
        public decimal im_sell_price_including_vat { get; set; }
        public string im_revenue_acno { get; set; }
        public string im_cost_acno { get; set; }
        public string im_vat_sale_profile { get; set; }
    }

    public class ApprovedIDTList
    {
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string doc_prov_no { get; set; }
        public string doc_prov_date { get; set; }
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string dt_code { get; set; }
        public string dt_desc { get; set; }
        public string dr_code { get; set; }
        public string sm_code { get; set; }
        public string sm_name { get; set; }
        public string up_name { get; set; }
        public string sal_type { get; set; }
        public string sal_type_name { get; set; }
        public string doc_total_amount { get; set; }
        public string doc_status { get; set; }
        public string doc_status_name { get; set; }
        public string cus_account_no { get; set; }
        public string ic_account_no { get; set; }
        public string ic_account_name { get; set; }
        public string doc_special_name { get; set; }
        public string dep_code_caused_by { get; set; }
        public string doc_order_ref_no { get; set; }
        public string doc_other_charge_desc { get; set; }
        public string doc_other_charge_amount { get; set; }
        public string doc_discount_pct { get; set; }
        public string doc_discount_amount { get; set; }
        public string doc_delivery_remarks { get; set; }
        public string doc_validity_remarks { get; set; }
        public string ow_id { get; set; }
        public string doc_jobcard_dept { get; set; }
        public string doc_jobcard_loc { get; set; }
        public string doc_jobcard_no { get; set; }
        public string sd_icc_code { get; set; }
        public string doc_narration { get; set; }
        public string sec_code { get; set; }
        public string creation_date { get; set; }
        public string creation_user { get; set; }
        public string creation_term { get; set; }
        public string modification_date { get; set; }
        public string modification_user { get; set; }

        //Sales_document_details variables
        public string dd_line_no { get; set; }
        public string im_inv_no { get; set; }
        public string loc_code { get; set; }
        public string dd_sell_price { get; set; }
        public string dd_sell_price_discounted { get; set; }
        public string dd_sell_price_final { get; set; }
        public string dd_sell_value_final { get; set; }
        public string dd_sell_cost_total { get; set; }
        public decimal dd_qty { get; set; }
        public string dd_outstanding_qty { get; set; }
        public string dd_line_no_have_previous { get; set; }
        public int doc_prov_no_have_previous { get; set; }
        public string dep_code_have_previous { get; set; }
        public string dd_line_discount_pct { get; set; }
        public string dd_line_discount_amount { get; set; }
        public string dd_core_exchange { get; set; }
        public string dd_discount_type { get; set; }
        public string dd_status { get; set; }
        public string test { get; set; }
        public string dd_line_total { get; set; }
        public string dd_physical_qty { get; set; }
        public string dd_remarks { get; set; }
        public string doc_type_have_previous { get; set; }
        public string doc_no_have_previous { get; set; }
        public string loc_name { get; set; }
        public string em_first_name { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string from_department { get; set; }
        public string to_department { get; set; }

    }

    public class ApprovedIDT
    {

        public List<ApprovedIDTList> AppIDTList = new List<ApprovedIDTList>();

        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string doc_prov_no { get; set; }
        public string doc_prov_date { get; set; }
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string dt_code { get; set; }
        public string dt_desc { get; set; }
        public string dr_code { get; set; }
        public string sm_code { get; set; }
        public string sm_name { get; set; }
        public string up_name { get; set; }
        public string sal_type { get; set; }
        public string sal_type_name { get; set; }
        public string doc_total_amount { get; set; }
        public string doc_status { get; set; }
        public string doc_status_name { get; set; }
        public string cus_account_no { get; set; }
        public string ic_account_no { get; set; }
        public string ic_account_name { get; set; }
        public string doc_special_name { get; set; }
        public string dep_code_caused_by { get; set; }
        public string doc_order_ref_no { get; set; }
        public string doc_other_charge_desc { get; set; }
        public string doc_other_charge_amount { get; set; }
        public string doc_discount_pct { get; set; }
        public string doc_discount_amount { get; set; }
        public string doc_delivery_remarks { get; set; }
        public string doc_validity_remarks { get; set; }
        public string ow_id { get; set; }
        public string doc_jobcard_dept { get; set; }
        public string doc_jobcard_loc { get; set; }
        public string doc_jobcard_no { get; set; }
        public string sd_icc_code { get; set; }
        public string doc_narration { get; set; }
        public string sec_code { get; set; }
        public string creation_date { get; set; }
        public string creation_user { get; set; }
        public string creation_term { get; set; }
        public string modification_date { get; set; }
        public string modification_user { get; set; }

        //Sales_document_details variables
        public string dd_line_no { get; set; }
        public string im_inv_no { get; set; }
        public string loc_code { get; set; }
        public string dd_sell_price { get; set; }
        public string dd_sell_price_discounted { get; set; }
        public string dd_sell_price_final { get; set; }
        public string dd_sell_value_final { get; set; }
        public string dd_sell_cost_total { get; set; }
        public decimal dd_qty { get; set; }
        public string dd_outstanding_qty { get; set; }
        public string dd_line_no_have_previous { get; set; }
        public string doc_prov_no_have_previous { get; set; }
        public string dep_code_have_previous { get; set; }
        public string dd_line_discount_pct { get; set; }
        public string dd_line_discount_amount { get; set; }
        public string dd_core_exchange { get; set; }
        public string dd_discount_type { get; set; }
        public string dd_status { get; set; }
        public string test { get; set; }
        public string dd_line_total { get; set; }
        public string dd_physical_qty { get; set; }
        public string dd_remarks { get; set; }
        public string doc_type_have_previous { get; set; }
        public int doc_no_have_previous { get; set; }
        //end of sales_document_details



        public string loc_name { get; set; }
        public string em_first_name { get; set; }
        public string im_item_code { get; set; }
        public string im_desc { get; set; }
        public string from_department { get; set; }
        public string to_department { get; set; }

        public string can_sale_date { get; set; }
    }




    #region Student-Fee(sims043)

    #region CancelReceipt
    public class cancel_receipt
    {
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string doc_amount { get; set; }
        public string doc_status { get; set; }
        public string doc_code { get; set; }
        public string enrollment_no { get; set; }
        public string studentName { get; set; }
        public string studentclass { get; set; }
        public string cur_code { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
        public bool isEnabled { get; set; }
        public string doc_reference_no { get; set; }
        public string doc_generated_by { get; set; }

    }



    #endregion

    public class siblingDetails
    {
        public string sib_name { get; set; }
        public string sib_enroll { get; set; }
        public string sib_admiss_date { get; set; }
        public string sib_photo { get; set; }
        public string sib_gender { get; set; }
        public string sib_class { get; set; }
        public string parent_name { get; set; }
        public string parent_number { get; set; }
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
    }

    public class sims043
    {
        public bool isSplitted { get; set; }
        public string feeTypeGroup { get; set; }
        public string auto_id { get; set; }
        public string FeeNumber { get; set; }
        public string FeeEnrollNo { get; set; }
        public string FeeType { get; set; }
        public bool FeeStatus { get; set; }
        public string FeePeriod { get; set; }
        public string FeeExpected { get; set; }
        public string FeePaid { get; set; }
        public string BalanceFee { get; set; }

        //Consession Fee-Discount Type Variables    
        public string fee_concession_number { get; set; }
        public string fee_concession_description { get; set; }
        public string fee_concession_type { get; set; }
        public string fee_concession_discount_type { get; set; }
        public string fee_concession_discount_value { get; set; }
        public string fee_concession_fee_code { get; set; }
        public string fee_concession_fee_code_description { get; set; }
        public string fee_concession_academic_year { get; set; }
        public string fee_concession_enroll_number { get; set; }
        public string fee_concession_dis_amt_cal { get; set; }
        public string fee_concession_fee_period_code { get; set; }
        //end-Consession Fee-Discount Type Variables    

        //Variables for Sibling Grid
        public bool stud_sib_select_status { get; set; }
        public string stud_sib_name { get; set; }
        public string stud_sib_cur_code { get; set; }
        public string stud_sib_academic_year { get; set; }
        public string stud_sib_grade_code { get; set; }
        public string stud_sib_grade_name { get; set; }
        public string stud_sib_section_code { get; set; }
        public string stud_sib_enroll_no { get; set; }
        public string stud_sib_stu_img { get; set; }


        //Variables of Sims043
        #region Sims043
        public string std_fee_number { get; set; }
        public string std_fee_enroll_number { get; set; }
        public string std_fee_Class { get; set; }
        public string std_fee_student_name { get; set; }
        public string std_fee_cur_code { get; set; }
        public string std_fee_academic_year { get; set; }
        public string std_fee_grade_code { get; set; }

        //public string  std_fee_grade_Name { get; set; }
        public string std_fee_section_code { get; set; }

        public string std_fee_section_Name { get; set; }
        public string std_fee_grade_name { get; set; }
        public string std_fee_section_name { get; set; }
        public string std_fee_parent_id { get; set; }
        public string std_fee_parent_name { get; set; }
        public string std_fee_amount { get; set; }
        public string std_fee_last_payment_date { get; set; }
        public bool std_fee_status { get; set; }
        public string std_fee_category_name { get; set; }
        public string std_fee_type { get; set; }
        public string std_fee_paid { get; set; }
        public decimal std_fee_remaining { get; set; }
        public string std_fee_paying { get; set; }
        public string std_fee_child_period { get; set; }
        public string std_fee_child_period_No { get; set; }
        public string std_fee_child_exp_amount { get; set; }
        public string std_fee_concession_amount { get; set; }
        public string std_fee_child_paid_amount { get; set; }
        public decimal std_fee_child_remaining_amount { get; set; }
        public decimal std_fee_child_paying_amount { get; set; }
        public string std_fee_bank_name { get; set; }
        public string std_fee_bank_id { get; set; }
        public string std_fee_id { get; set; }
        public string std_fee_code { get; set; }
        public string std_TotalPaid { get; set; }
        public string std_BalanceFee { get; set; }
        public bool FeeChekStatus { get; set; }
        public bool Installment_mode { get; set; }
        public bool Installment_mode_chk { get; set; }
        public string Installment_amnt { get; set; }

        //Payment Details
        public string paymentMode { get; set; }
        public string paymentMode_show { get; set; }
        public string chequeDDNo { get; set; }
        //public DateTime chequeDDDate { get; set; }
        public string check_amount { get; set; }
        public string chequeDDDate { get; set; }
        public string BankName { get; set; }
        public string TransactionID { get; set; }
        public string PayAmount { get; set; }
        public bool isTransaction { get; set; }
        public bool isCheque { get; set; }
        public bool isCash { get; set; }
        public bool isAmount { get; set; }
        public string id { get; set; }
        public bool current_flag { get; set; }
        public string PayDisAmount { get; set; }
        public string PayDisPeriodNo { get; set; }
        public string PayFeeCode { get; set; }
        #endregion

        #region sims_Fees_document
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string doc_total_amount { get; set; }
        public string doc_enroll_no { get; set; }
        public string doc_other_charge_desc { get; set; }
        public string doc_other_charge_amount { get; set; }
        public string doc_discount_pct { get; set; }
        public string doc_discount_amount { get; set; }
        public string doc_narration { get; set; }
        #endregion

        #region feeDisocunt
        public string std_fee_discount_type { get; set; }
        public string std_fee_discount_type_code { get; set; }
        public string std_fee_discount_paying_amount { get; set; }
        public string std_fee_other_type { get; set; }
        public string std_fee_other_type_code { get; set; }
        public decimal std_fee_other_paying_amount { get; set; }
        public decimal std_fee_discount_percent { get; set; }
        public bool std_fee_discount_inpercent { get; set; }
        public string std_fee_other_enrollNo { get; set; }

        #endregion

        #region sims_Fees_document_details
        public string dd_line_no { get; set; }
        public string dd_fee_number { get; set; }
        public string dd_fee_amount { get; set; }
        public string dd_fee_amount_discounted { get; set; }
        public string dd_fee_amount_final { get; set; }
        #endregion

        public string sims_fee_collection_mode { get; set; }

        public string std_fee_concession_amount_used { get; set; }

        public decimal std_fee_child_paying_amount_temp { get; set; }
    }
    #endregion

    #region My Code...

    #region Invs058_itemNew

    public class Invs058_itemNew
    {

        public bool im_assembly_ind { get; set; }

        public string im_inv_no { get; set; }

        public string invs021_sg_name { get; set; }

        public string im_item_code { get; set; }

        public string im_desc { get; set; }

        public string dept_code { get; set; }

        public string invs021_dep_code { get; set; }

        public string sec_code { get; set; }

        public string sup_sblgr_acno { get; set; }

        public string invs021_sup_code { get; set; }

        public string im_assembly_ind_s { get; set; }

        public string category_code { get; set; }

        public string subcategory_code { get; set; }

        public string uom_code { get; set; }
    }

    #endregion

    #endregion

    public class Inv200
    {
        public object opr { get; set; }
        public string vehicle_consumption_number { get; set; }
        public string sup_code { get; set; }
        public string sup_location_code { get; set; }
        public string vehicle_code { get; set; }
        public string im_inv_no { get; set; }
        public string sale_time { get; set; }
        public string unit_price { get; set; }
        public string sale_qty { get; set; }
        public string sale_period { get; set; }
        public string driver_code { get; set; }
        public string recorded_miles_reading { get; set; }
        public string sup_name { get; set; }
        public string cur_code { get; set; }
        public string sup_contact_designation { get; set; }
        public string sup_type { get; set; }
        public string sup_location_desc { get; set; }
        public string sup_location_address { get; set; }
        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_vehicle_registration_number { get; set; }

        public string sims_driver_name { get; set; }

        public string vehicle_name_plate { get; set; }

        public string vehicle_registration_number { get; set; }

        public string sims_employee_code { get; set; }
    }
    public class AdjRe
    {
        public object opr { get; set; }
        public string dateFrom { get; set; }
        public string dateUpto { get; set; }
        public string doc_no { get; set; }
        public string ar_account_no { get; set; }
        public string ar_code { get; set; }
        public string ar_desc { get; set; }
        public string sup_code { get; set; }
        public bool ar_indicator { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string acno { get; set; }
        public string glma_acct_name { get; set; }
        public string ord_no { get; set; }
    }

    public class ShipmentOrderDetails
    {
        public string orderNO { get; set; }
        public string orderLineNO { get; set; }
        public string shipmentNO { get; set; }
        public string req_type { get; set; }

        public string im_inv_no { get; set; }
        public string itemdesc { get; set; }
        public string debitacc { get; set; }
        public string creditacc { get; set; }
        public bool ifassetization { get; set; }

        public string itemdesc1 { get; set; }
        public string sup_code  { get; set; }
        public string od_received_qty { get; set; }
        public string suppliername { get; set; }
        public string od_grv_no { get; set; }
        public string ord_no { get; set; }
        public string grn_inv_no { get; set; }
        public string grn_inv_date { get; set; }
        public string invdate { get; set; }
        public string od_grv_date { get; set; }
        public string grndate { get; set; }
        public string od_grn_date { get; set; }
        public string csd_landed_cost { get; set; } 
    }
   
    public class ShipmentOrderDetailsSearch
    {
        public string ship_from_date { get; set; }
        public string ship_to_date { get; set; }
        public string ship_search_param { get; set; }
        public string OPR { get; set; }
        public string attr1 { get; set; }

    }


    public class ILDV01
    {       
        public string doc_prov_no { get; set; }
        public string dept_name { get; set; }
        public string em_designation { get; set; }
        public string doc_status { get; set; }
        public string status_desc { get; set; }
        public string doc_prov_date { get; set; }
        public String requested_by { get; set; }
        public String material_status { get; set; }
        

        public string requested_mob { get; set; }
        public string doc_no { get; set; }
        public string requested_email { get; set; }

        public string cus_account_no { get; set; }
        public string sims_appl_form_field { get; set; }

    }

}