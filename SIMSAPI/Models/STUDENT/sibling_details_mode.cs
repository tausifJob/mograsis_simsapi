﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.STUDENT.sibling_details_mode
{
    public class SIB10
    {
        public string sr_no { get; set; }
        public string parent_name { get; set; }
        public string sibling_no { get; set; }
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string stud_class { get; set; }

        public string enroll_number1 { get; set; }
        public string student_name1 { get; set; }



    }

    public class FEELEG
    {
        public string parent_no { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string grade_name { get; set; }
        public string section_name { get; set; }
    }

    public class ProRpt
    {
        public string parent_name { get; set; }
        public string parent_mob { get; set; }
        public string date_of_birth { get; set; }
        public  string gender { get; set; }
        public string acad_year { get; set; }
        public string parent_no { get; set; }
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string grade_name { get; set; }
        
    }

    public class FAMSTAT
    {
        public string date { get; set; }
        public string doc { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string sims_paid_fee { get; set; }
        public string sims_expected_fee { get; set; }
        public string balance { get; set; }
        public string balance2 { get; set; }
    }

    public class EMPSAL
    {
        public string paycode { get; set; }
        public string amount { get; set; }
        public string earn { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string year_code { get; set; }
        public string sd_year_month { get; set; }

        public string absents { get; set; }
        public string presents { get; set; }
        public string holidays { get; set; }
        public string worddays { get; set; }
        public string leaves { get; set; }
    }
    public class INDMO
    {
        public string opr { get; set; }
        public string name { get; set; }
        public string pass { get; set; }
    }
    public class LIBCAT
    {
        public string sims_library_category_code { get; set; }
        public string sims_library_category_name { get; set; }
        public string sims_library_subcategory_code { get; set; }
        public string sims_library_subcategory_name { get; set; }
        public string sims_library_catalogue_code { get; set; }
        public string sims_library_catalogue_name { get; set; }

        public string sims_library_item_number { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_bnn_number { get; set; }
        public string sims_library_item_dewey_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_desc { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_subcategory { get; set; }
        public string sims_library_item_catalogue_code { get; set; }
        public string sims_library_item_cur_code { get; set; }
        public string sims_library_item_remark { get; set; }
        public string sims_library_item_name_of_publisher { get; set; }
        public string sims_library_item_date_of_publication { get; set; }
        public string sims_library_item_purchase_price { get; set; }
        public string sims_library_item_number_of_copies { get; set; }
        public string sims_library_item_quantity { get; set; }
        public string sims_library_item_entry_date { get; set; }
        public string sims_library_item_category_name { get; set; }
        public string sims_library_item_subcategory_name { get; set; }
        public string sims_library_catelogue_name { get; set; }
        public string sims_library_item_location_name { get; set; }
        public string sims_library_bin_name { get; set; }
        public string sims_library_item_qr_code { get; set; }
        public string sims_library_item_bar_code { get; set; }
        public string member_name { get; set; }
        public string sims_library_transaction_date { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_actual_return_date { get; set; }
        public string transaction_status { get; set; }
        public string issue_return_status { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }

        public string earn { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string year_code { get; set; }
        public string sd_year_month { get; set; }

        public string absents { get; set; }
        public string presents { get; set; }
        public string holidays { get; set; }
        public string worddays { get; set; }
        public string leaves { get; set; }


        public string comn_audit_user_code { get; set; }
        public string comn_user_name { get; set; }
        public string comn_user_full_name { get; set; }
        public string comn_user_device_model { get; set; }
        public string comn_audit_user_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_audit_start_time { get; set; }
        public string comn_audit_end_time { get; set; }
        public string user_id_code { get; set; }
        public string user_mobile_number { get; set; }
        public string user_email_address { get; set; }
        public string comn_user_device_status { get; set; }
    }

     public class DETTRA
    {
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string em_login_code { get; set; }
        public string employee_name { get; set; }
        public string sims_detention_level_code { get; set; }
        public string sims_detention_name { get; set; }
        public string sims_detention_desc_code { get; set; }
        public string sims_detention_description { get; set; }
        public string sims_detention_point { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; } 

        public string  sims_detention_transaction_number { get; set; } 	
        public string  sims_detention_remark { get; set; } 
        public string  sims_detention_transaction_notified_user_code { get; set; } 
        public string  sims_detention_transaction_status { get; set; } 
        public string  sims_detention_transaction_created_user_code { get; set; } 
        public string  sims_detention_transaction_created_date { get; set; } 
        public string  sims_detention_transaction_updated_user_code { get; set; }
        public string sims_detention_transaction_updated_date { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_access_code { get; set; }
        public string sims_access_application_mapping_user_code { get; set; }

        public string sims_class { get; set; }
    }

     public class studentattendance
     {
         public string sims_cur_code { get; set; }
         public string sims_cur_name { get; set; }
         public string sims_academic_year { get; set; }
         public string sims_academic_year_desc { get; set; }
         public string sims_grade_code { get; set; }
         public string sims_grade_name { get; set; }
         public string sims_section_code { get; set; }
         public string sims_section_name { get; set; }
         public string sims_attendance_type_code { get; set; }
         public string sims_attendance_type_name { get; set; }
         public string sims_attendance_type_details_code { get; set; }
         public string sims_attendance_type_details_name { get; set; }
         public string sims_attendance_date { get; set; }
         public string sims_attendance_sort_code { get; set; }
         public string sims_attendance_sort_name { get; set; }

         public string sims_attendance_code { get; set; }

         public string sims_attendance_code_sort_name { get; set; }

         public string sims_attendance_code_name { get; set; }

         public string sims_student_name { get; set; }

         public string sims_student_fname { get; set; }

         public string sims_student_nickname { get; set; }

         public string sims_student_img { get; set; }

         public string sims_student_enroll { get; set; }

         public string sims_attendance_color { get; set; }

         public bool sims_student_bday { get; set; }

         public string homeroombatch { get; set; }
         public string sims_homeroom_code { get; set; }
         public string sims_batch_code { get; set; }
         public string attednacedate { get; set; }
         public string sims_subject_name_en { get; set; }
         public string sims_bell_subject_code { get; set; }

         public bool sims_isholiday { get; set; }

         public string sims_bus_number { get; set; }

         public string sims_term_code { get; set; }

         public string sims_term_desc_en { get; set; }

         public string sims_enroll_number { get; set; }

         public string sims_class_name { get; set; }

         public string sims_present { get; set; }

         public string sims_absent { get; set; }

         public string sims_absent_excused { get; set; }

         public string sims_Tardy { get; set; }

         public string sims_Unmarked { get; set; }

         public string sims_total { get; set; }

         public string sims_TotalAttendanceDays { get; set; }

         public string sims_Percentage { get; set; }
         public string sims_category_code { get; internal set; }
         public string sims_from_date { get; set; }
         public string sims_attendance_day_comment { get; set; }
     }

     public class studentattendance_rpt
     {
         public string sims_cur_code { get; set; }
         public string sims_cur_name { get; set; }
         public string sims_academic_year { get; set; }
         public string sims_academic_year_desc { get; set; }
         public string sims_grade_code { get; set; }
         public string sims_grade_name { get; set; }
         public string sims_section_code { get; set; }
         public string sims_section_name { get; set; }
         public string sims_enroll_number { get; set; }
         public string sims_class_name { get; set; }
         public string sims_student_name { get; set; }
         public string sims_term_code { get; set; }
         public string sims_report_name { get; set; }
         public string sims_term_desc_en { get; set; }



         public string sims_present { get; set; }

         public string sims_absent { get; set; }

         public string sims_absent_excused { get; set; }

         public string sims_Tardy { get; set; }

         public string sims_Unmarked { get; set; }

         public string sims_total { get; set; }

         public string sims_TotalAttendanceDays { get; set; }

         public string sims_Percentage { get; set; }

         public string sims_category_code { get; set; }

         public string sims_from_date { get; set; }

         public string sims_attendance_day_comment { get; set; }
     }


     public class HealthReportModel
     { 
        public string sims_cur_code							{ get; set; }
        public string sims_academic_year					{ get; set; }
        public string sims_grade_name_en					{ get; set; }
        public string sims_section_name_en					{ get; set; }
        public string sims_student_passport_first_name_en	{ get; set; }
        public string sims_student_passport_middle_name_en	{ get; set; }
        public string sims_student_passport_last_name_en	{ get; set; }
        public string sims_student_family_name_en			{ get; set; }
        public string sims_student_nickname					{ get; set; }
        public string sims_student_passport_first_name_ot	{ get; set; }
        public string sims_student_passport_middle_name_ot	{ get; set; }
        public string sims_student_passport_last_name_ot	{ get; set; }
        public string sims_student_family_name_ot			{ get; set; }
        public string sims_student_gender                   { get; set; }
        public string sims_sibling_parent_number			{ get; set; }

        public string sims_parent_father_email              { get; set; }
        public string sims_parent_father_mobile             { get; set; }
        public string sims_parent_mother_email              { get; set; }
        public string sims_parent_mother_mobile             { get; set; }
        public string sims_student_emergency_contact_name1  { get; set; }
        public string sims_student_emergency_contact_number1{ get; set; }
        public string sims_student_emergency_contact_name2  { get; set; }
        public string sims_student_emergency_contact_number2{ get; set; }

        public string sims_parent_guardian_email            { get; set; }
        public string sims_parent_guardian_mobile           { get; set; }
        public string sims_student_primary_contact_pref      { get; set; }

        public string sims_blood_group						{ get; set; }
        public string sims_enrollment_number				{ get; set; }
        public string sims_health_card_number				{ get; set; }
        public string sims_health_card_issue_date			{ get; set; }
        public string sims_health_card_expiry_date			{ get; set; }
        public string sims_health_card_issuing_authority	{ get; set; }
        public string sims_blood_group_code					{ get; set; }
        public string sims_health_restriction_status		{ get; set; }
        public string sims_health_restriction_desc			{ get; set; }
        public string sims_disability_status				{ get; set; }
        public string sims_disability_desc					{ get; set; }
        public string sims_medication_status				{ get; set; }
        public string sims_medication_desc					{ get; set; }
        public string sims_health_other_status				{ get; set; }
        public string sims_health_other_desc				{ get; set; }
        public string sims_health_hearing_status			{ get; set; }
        public string sims_health_hearing_desc				{ get; set; }
        public string sims_health_vision_status				{ get; set; }
        public string sims_health_vision_desc				{ get; set; }
        public string sims_regular_doctor_name				{ get; set; }
        public string sims_regular_doctor_phone				{ get; set; }
        public string sims_regular_hospital_name			{ get; set; }
        public string sims_regular_hospital_phone			{ get; set; }
        public string sims_height		                    { get; set; }
        public string sims_wieght							{ get; set; }
        public string sims_teeth							{ get; set; }
        public string sims_health_respiratory_status		{ get; set; }
        public string sims_health_respiratory_desc			{ get; set; }
        public string sims_health_hay_fever_status			{ get; set; }
        public string sims_health_hay_fever_desc			{ get; set; }
        public string sims_health_epilepsy_status			{ get; set; }
        public string sims_health_epilepsy_desc				{ get; set; }
        public string sims_health_skin_status				{ get; set; }
        public string sims_health_skin_desc					{ get; set; }
        public string sims_health_diabetes_status			{ get; set; }
        public string sims_health_diabetes_desc				{ get; set; }
        public string sims_health_surgery_status			{ get; set; }
        public string sims_health_surgery_desc				{ get; set; }
        public string sims_health_other_disability_status	{ get; set; }
        public string sims_health_other_disability_desc		{ get; set; }
        public string sims_health_bmi						{ get; set; }
        public string student_name { get; set; }

        public string sims_medical_consent_health_records { get; set; }
        public string sims_medical_consent_immunization { get; set; }
        public string sims_medical_consent_emergency_treatment { get; set; }
        public string sims_medical_consent_medical_treatment { get; set; }
        public string sims_medical_consent_over_the_counter_medicine { get; set; }
        public string sims_medical_consent_medicine_allowed { get; set; }
        public string sims_medical_consent_remark { get; set; }
        public string sims_medical_consent_applicable_from_acad_year { get; set; }
        public string sims_medical_consent_updated_on { get; set; }
        public string sims_medical_consent_approved_by { get; set; }


        public string start_day { get; set; }
     }

}