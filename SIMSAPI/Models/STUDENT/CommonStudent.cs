﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.STUDENT
{
    public class Common_Student
    {

            // promoted list

        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string sims_cur_code{ get; set; }
        public string sims_academic_year{ get; set; }
        public string current_year_grade_code{get; set; }
        public string current_year_grade_name_en { get; set; }
        public string current_year_section_code{ get; set; }
        public string current_year_section_name_en{ get; set; }
        public string next_year_grade_code { get; set; }
        public string next_year_grade_name_en{ get; set; }
        public string next_year_section_code { get; set; }
        public string next_year_section_name_en{ get; set; }
        public string sims_parent_mobile { get; set; }
        public string parent_name{ get; set; }
        public string parent_mail{ get; set; }
        public string promote_grade{ get; set; }
        public string sims_grade_code{ get; set; }
        public string sims_section_code{ get; set; }
         public string sims_allocation_status{ get; set; }
        public string sims_roll_number{ get; set; }
        public string sims_grade_name_en{ get; set; }
         public string sims_section_name_en{ get; set; }
        public string sims_student_academic_status{ get; set; }
        public string sims_certificate_number{ get; set; }
          public string sims_certificate_date_of_leaving{ get; set; }
          public string sims_certificate_reason_of_leaving { get; set; }

        // cancel summary
        

        public string comn_appl_form_field { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field_value1 { get; set; }

        // age wise list of student
        public string sims_student_enroll_number { get; set; }
        public string grade_name { get; set; }
        public string section_name{ get; set; }
        public string gender{ get; set; }
        public string sims_student_age{ get; set; }
        public string sims_student_dob{ get; set; }
        public string year{ get; set; }


        //admission list by grade and section
         public string sims_student_national_id { get; set; }
         public string sims_student_national_id_issue_date{ get; set; }
         public string sims_student_national_id_expiry_date{ get; set; }
           public string sims_student_ea_number{ get; set; }
        public string sims_student_ea_registration_date{ get; set; }
        public string  sims_student_ea_status	{ get; set; }
        public string  sims_student_ea_transfer_status	{ get; set; }

        public string  sims_student_passport_number { get; set; }
         public string  sims_student_passport_issue_place{ get; set; }
        
           public string  sims_student_passport_issuing_authority{ get; set; }
        public string sims_student_passport_issue_date{ get; set; }
        public string  sims_student_passport_expiry_date	{ get; set; }
       


        public string   sims_parent_father_summary_address_local_language { get; set; }
         public string  sims_parent_father_country_code{ get; set; }
         public string sims_parent_father_phone{ get; set; }
           public string  sims_parent_father_mobile{ get; set; }
        public string sims_parent_father_email{ get; set; }
        public string  sims_parent_father_fax{ get; set; }
        public string  sims_parent_father_po_box{ get; set; }

         public string   sims_parent_father_occupation { get; set; }
         public string  sims_parent_father_occupation_local_language{ get; set; }
         public string sims_parent_father_occupation_location_local_language{ get; set; }
           public string  sims_parent_father_company{ get; set; }
        public string sims_parent_father_passport_number{ get; set; }
        public string  mother_name{ get; set; }
        public string  sims_parent_mother_family_name{ get; set; }

        public string   sims_parent_mother_name_ot { get; set; }
         public string  mother_nationality1{ get; set; }
         public string mother_nationality2{ get; set; }
           public string  sims_parent_mother_summary_address{ get; set; }
        public string sims_parent_mother_country_code{ get; set; }
        public string  sims_parent_mother_phone{ get; set; }
        public string  sims_parent_mother_mobile{ get; set; }

         public string   sims_parent_mother_email { get; set; }
         public string  sims_parent_mother_fax{ get; set; }
         public string sims_parent_mother_occupation{ get; set; }
           public string  sims_parent_mother_occupation_local_language{ get; set; }
        public string sims_parent_mother_company{ get; set; }
        public string  sims_parent_mother_passport_number{ get; set; }
        public string guardian_name { get; set; }



        //student list by ethinicity
        
         
          
        public string sims_ethnicity_code { get; set; }
        public string  sims_student_ethnicity_code{ get; set; }
        public string sims_ethnicity_name_en { get; set; }
        public string caribbean { get; set; }
        public string  french{ get; set; }
        public string german { get; set; }
        public string asian { get; set; }
        public string  egyptians{ get; set; }

       

        //student immigration details
        public string sims_student_cur_code{ get; set; }
        public string pass_exp{ get; set; }
        public string nash_exp{ get; set; }
        public string visa_exp{ get; set; }
        public string sims_student_passport_issue_date1{ get; set; }
        public string sims_student_national_id_issue_date1{ get; set; }
        public string sims_student_visa_issue_date1{ get; set; }
        public string sims_student_visa_number{ get; set; }
        //student list by teacher
        public string sims_employee_code{ get; set; }
        public string sims_teacher_name{ get; set; }
        public string subject_name { get; set; }
        //student contact report
        public string sims_house_name{ get; set; }
        public string second_lan{ get; set; }
        public string third_lan{ get; set; }
        public string bus_in{ get; set; }
        public string bus_out { get; set; }

        //exam number mapping by teacher
         public string sims_class_teacher_code{ get; set; }
        public string sims_board_exam_enroll_number{ get; set; } 
        public string sims_board_code { get; set; }
        public string sims_board_short_name{ get; set; }
        public string sims_board_exam_code{ get; set; }
        public string sims_board_exam_short_name{ get; set; }

        public string next_grade{ get; set; }
        public string next_section{ get; set; } 
        public string father_name { get; set; }
        public string student_status { get; set; }
        public string sibling_detil { get; set; }
        public string BusNo { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }

        //class teacher list
        public string code { get; set; }
        public string descr { get; set; }
        //agenda report
        public string sims_agenda_name { get; set; }
        public string teacher_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }

        //student document status
        public string sims_admission_doc_admission_number { get; set; }
        public string Student_Name { get; set; }
        public string sims_criteria_name_en { get; set; }
        public string doc_status { get; set; }
        public string disc { get; set; }
        public string sims_criteria_code { get; set; }


 



	





        

        
            
        
            
            
            
        
        
        


        






	








	
        



        
        
        








	
	
	



        	

        	

			
			
		}

    public class CommonStudent
    {

        public string sims_student_enroll_number { get; set; }
        public string sims_student_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string section_name { get; set; }
        public string student_name { get; set; }
        public string sims_student_dob { get; set; }
        public string sims_student_nationality_code { get; set; }
        public string nationality_name { get; set; }
        public string sims_student_religion_code { get; set; }
        public string std_religion { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_teacher_code { get; set; }
        public string teacher_name { get; set; }
        public string sibling_detil { get; set; }
        public string sims_student_date { get; set; }
        public string sims_student_remark { get; set; }
        public string privious_school { get; set; }
        public string sims_parent_login_code { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        //list by grade

        public string sims_student_family_name_en { get; set; }
        public string sims_house_name { get; set; }

        public string sims_ethnicity_name_en { get; set; }

        //subject teacher list
        public string sims_bell_academic_year { get; set; }
        public string sims_bell_code { get; set; }
        public string bell_name { get; set; }
        public string sims_bell_section_code { get; set; }
        public string sims_bell_grade_code { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string employee_code { get; set; }
        public string sims_bell_subject_code { get; set; }
        public string subject_name { get; set; }

        //online admission


        public string sims_admission_number { get; set; }
        public string sims_admission_date { get; set; }
        public string student_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string gender { get; set; }
        public string fee_paid_status { get; set; }
        public string father_mobile { get; set; }
        public string father_email { get; set; }
        public string sims_admission_dob { get; set; }
        public string sims_religion_name_en { get; set; }
        public string sims_nationality_name_en { get; set; }
        public string sibling_name { get; set; }
        public string sibling_grade_name { get; set; }

        //student deatils

        public string sims_student_academic_status { get; set; }
        public string parent_mobile { get; set; }
        public string parent_email { get; set; }
        public string sims_student_commence_date { get; set; }
        public string sims_student_ea_status { get; set; }
        public string sims_student_ea_number { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string religion { get; set; }


        //cancelled student report

        public string sims_student_academic_year { get; set; }
        public string sims_student_status_updation_date { get; set; }
        public string sims_student_status_updation_user_code { get; set; }
        public string cancel_date { get; set; }
        public string status1 { get; set; }
        public string name { get; set; }
        public string class1 { get; set; }
        public string sims_student_financial_status { get; set; }
        //cancelled admission status

        public string sims_sr_no { get; set; }
        public string sims_finn_clr_status { get; set; }
        public string sims_clr_emp_status { get; set; }
        public string student_Name { get; set; }
        public string employee_name { get; set; }
        public string sims_lib_clr_status { get; set; }

        //parents not assigned to students
        public string sims_parent_number { get; set; }
        public string guardian_name { get; set; }


        //sibling report
        public string parent_name { get; set; }
        public string staff_type { get; set; }
        public string sims_student_count { get; set; }


        //Student House List Report  for Various Age Group
        public string age_calculate { get; set; }
        public string house_name { get; set; }



        // student list by homeroom

        public string sims_homeroom_code { get; set; }
        public string sims_homeroom_name { get; set; }
        public string sims_batch_code { get; set; }
        public string sims_batch_name { get; set; }
        public string sims_batch_enroll_number { get; set; }


       // prospects list
        public string sims_pros_date_created { get; set; }
        public string sims_pros_primary_contact_pref { get; set; }
        public string mother_mobile { get; set; }
        public string guardian_mobile { get; set; }
        public string guardian_email { get; set; }
        public string mother_email { get; set; }
        public string sims_pros_dob { get; set; }
      


    }
}


              
        
        
        
                                                                            
                                                                            	
                                                                                	
                                                                                
                                                                                	
                                                                                    	
                                                                                    	
                                                                                        	
                                                                                        	
                                                                                            	
                                                                                            	
                                                                                                	
                                                                                                	
   
    








        	
            
           
      	
                                                    
                                                        	
                                                        	
