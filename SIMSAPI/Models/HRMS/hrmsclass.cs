﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Controllers.hrms
{

    #region Pers400
    public class Pers400
    {
        public int srno { get; set; }

        public string leave_criteria_code { get; set; }

        public string leave_criteria_name { get; set; }

        public int leave_criteria_min_days { get; set; }

        public string leave_acct_code { get; set; }

        public string leave_bank_acct_code { get; set; }

        public string leave_cash_acct_code { get; set; }

        public string leave_airfare_acct_code { get; set; }

        public bool leave_status { get; set; }

        public object opr { get; set; }

        public object leave_pay_grade { get; set; }

        public string gr_code { get; set; }

        public string gr_desc { get; set; }

        public string leave_acct_name { get; set; }

        public string leave_bank_acct_name { get; set; }

        public string leave_cash_acct_name { get; set; }

        public string leave_airfare_acct_name { get; set; }

        public int year { get; set; }

        public int month { get; set; }

        public int days { get; set; }

        public string leave_prov_acct_code { get; set; }
        public string leave_prov_acct_name { get; set; }
        public string leave_formula_code { get; set; }
        public int leave_criteria_max_days { get; set; }
        public string leave_working_days { get; set; }
        public string leave_criteria_order { get; set; }

        public string leave_payable_days { get; set; }
    }
    #endregion

    //Pers400
    public class leavePolicy {

        public string staff_type_code { get; set; }

        public string lCode { get; set; }

        public string leave_code { get; set; }

        public string leave_policy_id { get; set; }

        public string staff_type_dec { get; set; }

        public string combination_of_leave { get; set; }

        public string combination_leave_Desc { get; set; }

        public string monthly_allowed { get; set; }
        
        public string termly_allowed { get; set; }

        public string yearly_allowed { get; set; }

        public string max_leave_per_Application { get; set; }

        public string is_prefix_holiday_allowed { get; set; }

        public string post_holiday_allowed { get; set; }

        public string prefix_weekend_allowed { get; set; }

        public string post_weekend_allowed { get; set; }

        public string combination_of_holiday_and_weekend_allowed { get; set; }

        public string pays_empl_leave_policy_status { get; set; }

        public string pays_empl_leave_policy_effective_from { get; set; }

        public string pays_empl_leave_policy_created_by { get; set; }

        public string Opr { get; set; }
    }


    public class HrmsClass
    {
        public string ca_company_code_name { get; set; }
        public string dg_company_code { get; set; }
        public string eg_company_code { get; set; }
        public string sh_company_code { get; set; }
        public string sh_company_name { get; set; }
        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
        public string sh_day_desc { get; set; }
        public string sh_day_code { get; set; }
        public bool sh_day_status { get; set; }

        public bool sh_template_status { get; set; }
        public string sh_template_name { get; set; }
        public string sh_template_id { get; set; }
        public string sh_shift_day1 { get; set; }
        public string sh_shift_day2 { get; set; }
        public string sh_shift_day3 { get; set; }
        public string sh_shift_day4 { get; set; }
        public string sh_shift_day5 { get; set; }
        public string sh_shift_day6 { get; set; }
        public string sh_shift_day7 { get; set; }
        public string sh_start_date { get; set; }
       // public DateTime? sh_start_date { get; set; }
        public string em_number { get; set; }
        public string em_desg_desc { get; set; }
        public string em_desg_code { get; set; }
        public string em_dept_code { get; set; }
        public string em_company_code { get; set; }
        public string em_img { get; set; }
        #region pays_employee_shift
        public string sh_emp_id { get; set; }
        public string sh_emp_name { get; set; }
        public bool sh_shift_status { get; set; }

        public List<shift_day> assign = new List<shift_day>();
        #endregion

        public string opr { get; set; }
        public int cnt { get; set; }
        public bool ischecked { get; set; }
        public string sh_attendence_date { get; set; }

        public string em_login_code { get; set; }

        public string sh_start_fromdate { get; set; }

        public string sh_to_date { get; set; }

        public string sh_from_date { get; set; }

        public string sh_end_date { get; set; }
    }

    #region Leave Details
    public class Pers300_le
    {
        public string leave_desc { get; set; }
        public string leave_tag_desc { get; set; }

        public string lt_no { get; set; }
        public string lt_no_cancel { get; set; }
        public string lt_no_update { get; set; }
        public string em_status { get; set; }
        public string em_email { get; set; }
        public string em_login_code { get; set; }
        public string em_number { get; set; }
        public string EmpName { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string img_name { get; set; }
        public string codp_dept_name { get; set; }
        public string dg_desc { get; set; }
        public string gr_desc { get; set; }

        public string leave_type { get; set; }
        public string leave_code { get; set; }
        public string maxleave_allowed { get; set; }

        public string pays_leave_doc_code { get; set; }
        public string pays_leave_doc_name { get; set; }
        public string pays_leave_doc_desc { get; set; }
        public string pays_leave_doc_is_mandatory { get; set; }
        public string pays_leave_doc_status { get; set; }
        public string em_doc_path { get; set; }
        public string lt_start_date { get; set; }
        public string lt_end_date { get; set; }
        public string lt_end_date_old { get; set; }
        public string lt_end_date_new { get; set; }
        public string lt_end_dateD { get; set; }

        public string lt_days { get; set; }
        public string lt_hours { get; set; }
        public string lt_mgr_confirm_tag { get; set; }
        public string lt_modified_on { get; set; }
        public string lt_remarks { get; set; }
        public string lt_converted_hours { get; set; }

        public string leavestatus { get; set; }
        public bool updated { get; set; }
        public string leavest { get; set; }
        public string requestdate { get; set; }
        public string ndays { get; set; }
        public string currentystart { get; set; }
        public string currentyend { get; set; }

        public string holidaystart { get; set; }
        public string holidayend { get; set; }

        public string holiday { get; set; }
        public string leavest_ex { get; set; }
        public string l_halfday { get; set; }
        public string lt_admin_remark { get; set; }
        public string lt_admin_cancel_remark { get; set; }
        public string emp_balance_leaves { get; set; }

        public string el_days_taken { get; set; }
        public string el_maximum_days { get; set; }
        public string el_extra_days { get; set; }
        public string blackoutdays { get; set; }
        public List<DateTime> BlackOutDates { get; set; }

        public DateTime sims_financial_year_start_date { get; set; }

        public DateTime sims_financial_year_end_date { get; set; }

        public object lt_request_date { get; set; }

        public string lt_approved_by_user_code { get; set; }

        public string lt_approval_date { get; set; }

        public string Actiononleave { get; set; }

        public string ConfirmTag_type { get; set; }

        public string ConfirmTag_code { get; set; }

        public string lstart_date { get; set; }

        public bool holiday_flag { get; set; }

        public string yearcurrentstart { get; set; }

        public string yearcurrentyend { get; set; }

        public string sims_param_role_id { get; set; }

        public string sims_param_role_field { get; set; }

        public string lt_status { get; set; }

        public string dept_no { get; set; }

        public string em_number1 { get; set; }
        public string em_name { get; set; }
        public string pe_number { get; set; }
        public string pe_status { get; set; }
        public string pl_finalize_date { get; set; }
        public string pl_finalize_status { get; set; }
        public string el_number { get; set; }
        public string cl_code { get; set; }
        public string cl_parent_id { get; set; }   
        public string cl_display_order { get; set; }
        public string maxLeaves { get; set; }
        public string balanceLeaves { get; set; }
        public string lt_leave_code { get; set; }
        public string leave_exists { get; set; }
        public string lt_substitute_employee { get; set; }
        public string lt_start_date_new { get; set; }
        public string allowed_paid_leave { get; set; }

        public string em_staff_type { get; set; }

        public string annual_leave_taken { get; set; }

        
    }

    public class PerGrt
    {
        public string em_login_code { get; set; }
        public string emp_name { get; set; }
        public string grt_amount { get; set; }
        public string experience { get; set; }
        public string em_date_of_join { get; set; }
        public string pa_amount { get; set; }
        public string pa_pay_code { get; set; }
        public string desg_name { get; set; }
        public decimal grt_total_amount { get; set; }
        public string total_grt_amount { get; set; }
        public string grt_criteria_code { get; set; }
        public string grt_working_days { get; set; }
        public string em_company_code { get; set; }
        public string yearmonth { get; set; }
        public string grtyearmonth { get; set; }
        public string username { get; set; }
        public string current_month_amount { get; set; }
        public int year_number { get; set; }
        public int month_number { get; set; }
        public int year_month_days { get; set; }
        public string grt_posting_date { get; set; }
        public string grt_posting_status { get; set; }


        public List<payCodeList1> paycodeList = new List<payCodeList1>();
    }
    public class payCodeList1
    {
        public string em_login_code { get; set; }        
        public decimal grt_total_amount { get; set; }
        public string pa_pay_code { get; set; }
        public string pa_amount { get; set; }
        public string pay_discription { get; set; }
        
    }
    public class TrnEmp
    {
        public string em_Company_Code { get; set; }
        public string em_Company_name { get; set; }
        public string em_Dept_code { get; set; }
        public string em_Dept_name { get; set; }
        public string em_Grade_code { get; set; }
        public string em_Grade_name { get; set; }
        public string em_Desg_code { get; set; }
        public string em_Desg_name { get; set; }
        public string em_Staff_type_code { get; set; }
        public string em_Staff_type_name { get; set; }
        public string em_login_code { get; set; }
        public string em_number { get; set; }
        public string em_dept_effect_from { get; set; }
        public string em_grade_effect_from { get; set; }
        public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_last_name { get; set; }
        public string em_family_name { get; set; }
        public string em_full_name { get; set; }
        public string em_desg_effect_from { get; set; }
        public string em_staff_type_effect_from { get; set; }
        public string em_company_effect_from { get; set; }
    }

    public class CAL
    {
        public string leave_code { get; set; }
        public string leave_desc { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_emp_name { get; set; }
        public string sims_start_date { get; set; }
        public string sims_end_date { get; set; }
        public string sims_total_days { get; set; }
        public string sims_remark { get; set; }
        public bool isActive { get; set; }
    }

    #endregion


    #region Per352
    public class Per352
    {
        public string opr { get; set; }
        public string ed_srno { get; set; }
        public string em_login_code { get; set; }
        public string em_name { get; set; }
        public string ed_sr_code { get; set; }
        public string ed_first_name_en { get; set; }
        public string ed_middle_name_en { get; set; }
        public string ed_last_name_en { get; set; }
        public string ed_dob { get; set; }
        public string ed_visa_sponsor_id { get; set; }
        public string ed_visa_sponsor_name { get; set; }
        public string ed_visa_number { get; set; }
        public string ed_visa_issue_date { get; set; }
        public string ed_visa_expiry_date { get; set; }
        public string ed_nation_id { get; set; }
        public string ed_nation_id_issue_date { get; set; }
        public string ed_nation_id_expiry_date { get; set; }
        public string ed_passport_id { get; set; }
        public string ed_passport_id_issue_date { get; set; }
        public string ed_passport_id_expiry_date { get; set; }
        public string ed_relation { get; set; }
        public string ed_visa_exp_rem_date { get; set; }
        public string ed_health_insurance_issuing_authority { get; set; }
        public string ed_health_insurance_no { get; set; }
        public string ed_health_insurance_effective_from_date { get; set; }
        public string ed_health_insurance_effective_upto_date { get; set; }
        public bool ed_health_insurance_status { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_field_value1 { get; set; }
    }
    #endregion

    #region PerAF
    public class PerAF
    {
        public string ds_code { get; set; }
        public string ds_name { get; set; }
        public string nation_code { get; set; }
        public string nation_name { get; set; }
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_value1 { get; set; }
        public string af_dest_code { get; set; }
        public string af_nation_code { get; set; }
        public string af_class { get; set; }
        public string af_class_name { get; set; }
        public string af_adult_rate { get; set; }
        public string af_child_rate { get; set; }
        public string af_infant_rate { get; set; }
        public object opr { get; set; }
        public string gr_code { get; set; }
        public string gr_desc { get; set; }

        public string af_grade_code { get; set; }

        public string pays_month_code { get; set; }
        public string pays_month_name { get; set; }
        public string pay_code { get; set; }
        public string pay_desc { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public bool af_payroll_flag { get; set; }
        public string af_applicable_frequency { get; set; }

        public string af_gl_acct_no { get; set; }        
        public string af_gl_acct_name { get; set; }
        public bool upgrade_flag { get; set; }
        public string update_after { get; set; }
        public string update_freq { get; set; }

        public string af_pay_code { get; set; }
        public string af_applicable_month { get; set; }
        public string af_default_applicable_month { get; set; }
    }
    #endregion

    public class EmpExp
    {
        public string Month_Number { get; set; }
        public string Month_Name { get; set; }

        public string co_company_code { get; set; }
        public string co_desc { get; set; }

        public string opr { get; set; }

        public string enroll_number { get; set; }
        public string prev_company_name { get; set; }
        public string prev_job_title { get; set; }
        public string from_year { get; set; }
        public string from_month { get; set; }
        public string to_year { get; set; }
        public string to_month { get; set; }
        public string prev_job_remark { get; set; }
        public string prev_job_responsibilities { get; set; }
        public string prev_job_salary { get; set; }
        public bool job_status { get; set; }

        public string em_login_code { get; set; }
        public string em_company_code { get; set; }
        public string em_previous_job_line_no { get; set; }
        public string em_previous_job_company { get; set; }
        public string em_previous_job_title { get; set; }
        public string em_previous_job_start_year_month { get; set; }
        public string em_previous_job_end_year_month { get; set; }
        public string em_previous_job_responsibilities { get; set; }
        public string em_previous_job_remark { get; set; }
        public string em_previous_job_salary_details { get; set; }
        public bool em_previous_job_status { get; set; }
        public string em_emp_full_name { get; set; }
        public string company_name { get; set; }
        public string total_experience { get; set; }
        public string em_applicant_id { get; set; }
    }

    public class PerAFEmp
    {

        public string afe_em_number { get; set; }

        public string afe_grade_code { get; set; }

        public string gr_desc { get; set; }

        public string afe_dest_code { get; set; }

        public string ds_name { get; set; }

        public string afe_nation_code { get; set; }

        public string sims_country_name_en { get; set; }

        public string afe_class { get; set; }

        public string afe_class_name { get; set; }

        public string afe_applicable_frequency { get; set; }

        public string afe_applicable_month { get; set; }

        public string afe_adult_rate { get; set; }

        public string afe_infant_rate { get; set; }

        public string afe_child_rate { get; set; }

        public bool afe_finalize_status { get; set; }

        public object opr { get; set; }

        public object afe_airport { get; set; }

        public string afe_travelling_date { get; set; }

        public string afe_returing_date { get; set; }

        public object afe_created_by { get; set; }

        public object afe_creation_date { get; set; }

        public object afe_old_frequency { get; set; }

        public object afe_reimbursement { get; set; }
        public string afe_amount_ticket { get; set; }
        public string afe_id { get; set; }
        public string afe_parent_id { get; set; }        
        
        public string gr_code { get; set; }
        public string afe_academic_year { get; set; }

        public string afe_em_name { get; set; }

    }
    #region Pers046
    public class Pers233
    {
        
        public string gr_company_code { get; set; }
        public string gr_code { get; set; }
        public string gr_desc { get; set; }
        public string gr_category_code { get; set; }
        public string gr_print_order { get; set; }
        public string gr_min_basic { get; set; }
        public string gr_max_basic { get; set; }

        public string opr { get; set; }

        public string gr_category_desc { get; set; }

        public string gr_company_desc { get; set; }

        public string gr_amt { get; set; }

        public string em_sr_no { get; set; }
        public string em_applicant_id { get; set; }
        public string em_vacancy_id { get; set; }
        public string dg_desc { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_application_date { get; set; }
        public string pays_vacancy_roles { get; set; }
        public string em_email { get; set; }
        public string em_mobile { get; set; }
        public string em_expected_date_of_join { get; set; }
        public string emp_name { get; set; }
        public string emailsendto { get; set; }
        public string body { get; set; }
        public string subject { get; set; }
        public string sender_emailid { get; set; }
        public string sims_recepient_id { get; set; }
        public string sims_recepient_cc_id { get; set; }
        public string cc_id { get; set; }
        public string em_sex { get; set; }
        public string em_dest_code { get; set; }
        public string em_staff_type { get; set; }
        public string em_grade_code { get; set; }
        public string em_agreement { get; set; }
        public string em_agreement_start_date { get; set; }
        public string em_agreement_exp_date { get; set; }
        public string em_visa_type { get; set; }
        public string en_labour_card_no { get; set; }
        public string em_desg_code { get; set; }
        public string offer_letter_doc { get; set; }
        public string offer_letter_path { get; set; }
        public string pays_sr_no { get; set; }
        public string pays_vacancy_id { get; set; }
        public string pays_vacancy_desc { get; set; }
        public string em_offer_letter_accepted_flag { get; set; }
        public string em_offer_letter_emailsend_flag { get; set; }
        public bool em_offer_letter_show_flag { get; set; }
        public string em_company_code { get; set; }
        public string em_dept_code { get; set; }
        public string em_nation_code { get; set; }
        public string em_salutation { get; set; }
        public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_last_name { get; set; }
        public string em_family_name { get; set; }
        public string em_name_ot { get; set; }
        public string em_marital_status { get; set; }
        public string em_religion_code { get; set; }
        public string em_ethnicity_code { get; set; }
        public string em_appartment_number { get; set; }
        public string em_building_number { get; set; }
        public string em_street_number { get; set; }
        public string em_area_number { get; set; }
        public string em_summary_address { get; set; }
        public string em_city { get; set; }
        public string em_state { get; set; }
        public string em_country_code { get; set; }
        public string em_phone { get; set; }
        public string em_fax { get; set; }
        public string em_po_box { get; set; }
        public string em_passport_number { get; set; }
        public string em_passport_issue_date { get; set; }
        public string em_passport_expiry_date { get; set; }
        public string em_passport_issuing_authority { get; set; }
        public string em_passport_issue_place { get; set; }
        public string em_visa_number { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_expiry_date { get; set; }
        public string em_visa_issuing_place { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string em_pan_no { get; set; }
        public string em_img { get; set; }
        public string em_social_address { get; set; }
        public string em_emergency_contact_name1 { get; set; }
        public string em_emergency_contact_name2 { get; set; }
        public string em_emergency_contact_number1 { get; set; }
        public string em_emergency_contact_number2 { get; set; }
        public string em_joining_ref { get; set; }
        public string em_handicap_status { get; set; }
        public string em_blood_group_code { get; set; }
        public string em_doc { get; set; }
        public string em_application_status { get; set; }
        public string em_password { get; set; }
        public string em_user_id { get; set; }
        public string em_doc_path { get; set; }
        public string pays_doc_recruitment_status { get; set; }
        public string em_probation_period_days { get; set;}
        public string em_agreement_Probation_completion_date { get; set; }
        public string em_agreement_confirm_date { get; set; }
        public string em_offer_letter_send_date { get; set; }        
        public string em_probation_completion_date { get; set; }
        public string em_probation_confirmation_date { get; set; }
    }

    #endregion

    #region Pers324
    public class Pers324
    {

        public string grt_criteria_code { get; set; }

        public string grt_criteria_name { get; set; }

        public string grt_applicable_days { get; set; }

        public bool grt_status { get; set; }

        public string grt_working_days { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Pers116
    public class Persleave_type
    {
        public string le_company_code { get; set; }
        public string le_leave_code { get; set; }
        public string le_formula_code { get; set; }
        public bool le_accumulated_tag { get; set; }
        public decimal le_max_days_allowed { get; set; }
        public string cl_desc { get; set; }

        public string le_leave_type { get; set; }

        public string le_company_name { get; set; }

        public string opr { get; set; }

        public string staff_code { get; set; }

        public string staff_type { get; set; }
        public string led_accural_type { get; set; }
        public string led_accural_type_desc { get; set; }
        public bool led_lwp_consider { get; set; }
        public bool led_accural_status { get; set; }
        public string led_accural_days { get; set; }
        public string led_created_user { get; set; }        
    }
    #endregion

    public class Per313
    {

        public string company_code { get; set; }

        public string company_name { get; set; }

        public string dept_code { get; set; }

        public string dept_name { get; set; }

        public string gr_code { get; set; }

        public string gr_desc { get; set; }

        public string staff_type_code { get; set; }

        public string staff_type_name { get; set; }

        public string dg_code { get; set; }

        public string dg_desc { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_academic_year_description { get; set; }

        public string pays_attendance_code { get; set; }

        public string pays_attendance_short_desc { get; set; }

        public string pays_attendance_description { get; set; }

        public string pays_attendance_color { get; set; }

        public string sort_code { get; set; }

        public string sort_name { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string pays_employee_name { get; set; }

        public string att_emp_id { get; set; }

        public string Yearno { get; set; }

        public string monthYear { get; set; }

        public string em_company_code { get; set; }

        public string em_dept_code { get; set; }

        public string em_grade_code { get; set; }

        public string em_staff_type { get; set; }

        public string em { get; set; }

        public string em_desg_code { get; set; }

        public string pays_attendance_day_1_att_code { get; set; }

        public string pays_attendance_day_2_att_code { get; set; }

        public string pays_attendance_day_3_att_code { get; set; }

        public string pays_attendance_day_4_att_code { get; set; }

        public string pays_attendance_day_5_att_code { get; set; }

        public string pays_attendance_day_7_att_code { get; set; }

        public string pays_attendance_day_6_att_code { get; set; }

        public string pays_attendance_day_8_att_code { get; set; }

        public string pays_attendance_day_9_att_code { get; set; }

        public string pays_attendance_day_10_att_code { get; set; }

        public string pays_attendance_day_11_att_code { get; set; }

        public string pays_attendance_day_12_att_code { get; set; }

        public string pays_attendance_day_13_att_code { get; set; }

        public string pays_attendance_day_14_att_code { get; set; }

        public string pays_attendance_day_15_att_code { get; set; }

        public string pays_attendance_day_16_att_code { get; set; }

        public string pays_attendance_day_17_att_code { get; set; }

        public string pays_attendance_day_18_att_code { get; set; }

        public string pays_attendance_day_22_att_code { get; set; }

        public string pays_attendance_day_21_att_code { get; set; }

        public string pays_attendance_day_20_att_code { get; set; }

        public string pays_attendance_day_19_att_code { get; set; }

        public string pays_attendance_day_23_att_code { get; set; }

        public string pays_attendance_day_25_att_code { get; set; }

        public string pays_attendance_day_27_att_code { get; set; }

        public string pays_attendance_day_28_att_code { get; set; }

        public string pays_attendance_day_24_att_code { get; set; }

        public string pays_attendance_day_26_att_code { get; set; }

        public string pays_attendance_day_29_att_code { get; set; }

        public string pays_attendance_day_30_att_code { get; set; }

        public string pays_attendance_day_31_att_code { get; set; }

        public string opr { get; set; }

        public string att_date { get; set; }

        public string att_flag { get; set; }

        public string em_login_code { get; set; }

        public string FromDateDay { get; set; }
        public string att_shift1_in { get; set; }
        public string att_shift1_out { get; set; }

        public string ToDateDay { get; set; }
        public object att_absent_flag { get; set; }
        public string pe_status { get; set; }
        public string pays_appl_form_field_value1 { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_attendance_teacher_can_use_flag { get; set; }
        public string pays_attendance_leave_code { get; set; }

        public string pays_attendance_day_1_att_exp_in { get; set; }
        public string pays_attendance_day_1_att_punch_in { get; set; }
        public string pays_attendance_day_1_att_exp_out { get; set; }
        public string pays_attendance_day_1_att_punch_out { get; set; }

        public string pays_attendance_day_2_att_exp_in { get; set; }
        public string pays_attendance_day_2_att_punch_in { get; set; }
        public string pays_attendance_day_2_att_exp_out { get; set; }
        public string pays_attendance_day_2_att_punch_out { get; set; }

        public string pays_attendance_day_3_att_exp_in { get; set; }
        public string pays_attendance_day_3_att_punch_in { get; set; }
        public string pays_attendance_day_3_att_exp_out { get; set; }
        public string pays_attendance_day_3_att_punch_out { get; set; }

        public string pays_attendance_day_4_att_exp_in { get; set; }
        public string pays_attendance_day_4_att_punch_in { get; set; }
        public string pays_attendance_day_4_att_exp_out { get; set; }
        public string pays_attendance_day_4_att_punch_out { get; set; }

        public string pays_attendance_day_5_att_exp_in { get; set; }
        public string pays_attendance_day_5_att_punch_in { get; set; }
        public string pays_attendance_day_5_att_exp_out { get; set; }
        public string pays_attendance_day_5_att_punch_out { get; set; }

        public string pays_attendance_day_6_att_exp_in { get; set; }
        public string pays_attendance_day_6_att_punch_in { get; set; }
        public string pays_attendance_day_6_att_exp_out { get; set; }
        public string pays_attendance_day_6_att_punch_out { get; set; }

        public string pays_attendance_day_7_att_exp_in { get; set; }
        public string pays_attendance_day_7_att_punch_in { get; set; }
        public string pays_attendance_day_7_att_exp_out { get; set; }
        public string pays_attendance_day_7_att_punch_out { get; set; }

        public string pays_attendance_day_8_att_exp_in { get; set; }
        public string pays_attendance_day_8_att_punch_in { get; set; }
        public string pays_attendance_day_8_att_exp_out { get; set; }
        public string pays_attendance_day_8_att_punch_out { get; set; }

        public string pays_attendance_day_9_att_exp_in { get; set; }
        public string pays_attendance_day_9_att_punch_in { get; set; }
        public string pays_attendance_day_9_att_exp_out { get; set; }
        public string pays_attendance_day_9_att_punch_out { get; set; }
        
        public string pays_attendance_day_10_att_exp_in { get; set; }
        public string pays_attendance_day_10_att_punch_in { get; set; }
        public string pays_attendance_day_10_att_exp_out { get; set; }
        public string pays_attendance_day_10_att_punch_out { get; set; }

        public string pays_attendance_day_11_att_exp_in { get; set; }
        public string pays_attendance_day_11_att_punch_in { get; set; }
        public string pays_attendance_day_11_att_exp_out { get; set; }
        public string pays_attendance_day_11_att_punch_out { get; set; }

        public string pays_attendance_day_12_att_exp_in { get; set; }
        public string pays_attendance_day_12_att_punch_in { get; set; }
        public string pays_attendance_day_12_att_exp_out { get; set; }
        public string pays_attendance_day_12_att_punch_out { get; set; }

        public string pays_attendance_day_13_att_exp_in { get; set; }
        public string pays_attendance_day_13_att_punch_in { get; set; }
        public string pays_attendance_day_13_att_exp_out { get; set; }
        public string pays_attendance_day_13_att_punch_out { get; set; }

        public string pays_attendance_day_14_att_exp_in { get; set; }
        public string pays_attendance_day_14_att_punch_in { get; set; }
        public string pays_attendance_day_14_att_exp_out { get; set; }
        public string pays_attendance_day_14_att_punch_out { get; set; }

        public string pays_attendance_day_15_att_exp_in { get; set; }
        public string pays_attendance_day_15_att_punch_in { get; set; }
        public string pays_attendance_day_15_att_exp_out { get; set; }
        public string pays_attendance_day_15_att_punch_out { get; set; }

        public string pays_attendance_day_16_att_exp_in { get; set; }
        public string pays_attendance_day_16_att_punch_in { get; set; }
        public string pays_attendance_day_16_att_exp_out { get; set; }
        public string pays_attendance_day_16_att_punch_out { get; set; }

        public string pays_attendance_day_17_att_exp_in { get; set; }
        public string pays_attendance_day_17_att_punch_in { get; set; }
        public string pays_attendance_day_17_att_exp_out { get; set; }
        public string pays_attendance_day_17_att_punch_out { get; set; }

        public string pays_attendance_day_18_att_exp_in { get; set; }
        public string pays_attendance_day_18_att_punch_in { get; set; }
        public string pays_attendance_day_18_att_exp_out { get; set; }
        public string pays_attendance_day_18_att_punch_out { get; set; }

        public string pays_attendance_day_19_att_exp_in { get; set; }
        public string pays_attendance_day_19_att_punch_in { get; set; }
        public string pays_attendance_day_19_att_exp_out { get; set; }
        public string pays_attendance_day_19_att_punch_out { get; set; }

        public string pays_attendance_day_20_att_exp_in { get; set; }
        public string pays_attendance_day_20_att_punch_in { get; set; }
        public string pays_attendance_day_20_att_exp_out { get; set; }
        public string pays_attendance_day_20_att_punch_out { get; set; }

        public string pays_attendance_day_21_att_exp_in { get; set; }
        public string pays_attendance_day_21_att_punch_in { get; set; }
        public string pays_attendance_day_21_att_exp_out { get; set; }
        public string pays_attendance_day_21_att_punch_out { get; set; }

        public string pays_attendance_day_22_att_exp_in { get; set; }
        public string pays_attendance_day_22_att_punch_in { get; set; }
        public string pays_attendance_day_22_att_exp_out { get; set; }
        public string pays_attendance_day_22_att_punch_out { get; set; }

        public string pays_attendance_day_23_att_exp_in { get; set; }
        public string pays_attendance_day_23_att_punch_in { get; set; }
        public string pays_attendance_day_23_att_exp_out { get; set; }
        public string pays_attendance_day_23_att_punch_out { get; set; }

        public string pays_attendance_day_24_att_exp_in { get; set; }
        public string pays_attendance_day_24_att_punch_in { get; set; }
        public string pays_attendance_day_24_att_exp_out { get; set; }
        public string pays_attendance_day_24_att_punch_out { get; set; }

        public string pays_attendance_day_25_att_exp_in { get; set; }
        public string pays_attendance_day_25_att_punch_in { get; set; }
        public string pays_attendance_day_25_att_exp_out { get; set; }
        public string pays_attendance_day_25_att_punch_out { get; set; }

        public string pays_attendance_day_26_att_exp_in { get; set; }
        public string pays_attendance_day_26_att_punch_in { get; set; }
        public string pays_attendance_day_26_att_exp_out { get; set; }
        public string pays_attendance_day_26_att_punch_out { get; set; }

        public string pays_attendance_day_27_att_exp_in { get; set; }
        public string pays_attendance_day_27_att_punch_in { get; set; }
        public string pays_attendance_day_27_att_exp_out { get; set; }
        public string pays_attendance_day_27_att_punch_out { get; set; }

        public string pays_attendance_day_28_att_exp_in { get; set; }
        public string pays_attendance_day_28_att_punch_in { get; set; }
        public string pays_attendance_day_28_att_punch_out { get; set; }
        public string pays_attendance_day_28_att_exp_out { get; set; }

        public string pays_attendance_day_29_att_exp_in { get; set; }
        public string pays_attendance_day_29_att_punch_in { get; set; }
        public string pays_attendance_day_29_att_exp_out { get; set; }
        public string pays_attendance_day_29_att_punch_out { get; set; }

        public string pays_attendance_day_30_att_exp_in { get; set; }
        public string pays_attendance_day_30_att_punch_in { get; set; }
        public string pays_attendance_day_30_att_exp_out { get; set; }
        public string pays_attendance_day_30_att_punch_out { get; set; }

        public string pays_attendance_day_31_att_exp_in { get; set; }
        public string pays_attendance_day_31_att_punch_in { get; set; }
        public string pays_attendance_day_31_att_exp_out { get; set; }
        public string pays_attendance_day_31_att_punch_out { get; set; }

        public string attendance_push_date { get; set; }

    }

    #region Pers323
    public class Pers323
    {
        public int srno { get; set; }

        public string grt_criteria_code { get; set; }

        public string grt_criteria_name { get; set; }

        public int grt_criteria_min_days { get; set; }

        public string gr_desc { get; set; }

        public string grt_acct_code { get; set; }

        public string grt_bank_acct_code { get; set; }

        public string grt_cash_acct_code { get; set; }

        public string grt_airfare_acct_code { get; set; }

        public bool grt_status { get; set; }

        public object opr { get; set; }

        public object grt_pay_grade { get; set; }

        public string gr_code { get; set; }

        public string grt_acct_name { get; set; }

        public string grt_bank_acct_name { get; set; }

        public string grt_cash_acct_name { get; set; }

        public string grt_airfare_acct_name { get; set; }

        public int year { get; set; }

        public int month { get; set; }

        public int days { get; set; }

        public string grt_prov_acct_code { get; set; }
        public string grt_prov_acct_name{get;set;}
        public string grt_formula_code { get; set; }
        public int grt_criteria_max_days { get; set; }
        public string grt_working_days { get; set; }
        public string grt_criteria_order { get; set; }
    }
    #endregion

    //#region Pers324(gratuity Calculation)
    //public class Pers324
    //{

    //    public string grt_criteria_code { get; set; }

    //    public string grt_criteria_name { get; set; }

    //    public string grt_applicable_days { get; set; }

    //    public string grt_working_days { get; set; }
    //    public bool grt_status { get; set; }

    //    public string opr { get; set; }

    //}
    //#endregion

    #region Pers306
    public class Pers306
    {
        public string em_number { get; set; }
        public string em_login_code { get; set; }
        public string sd_year_month { get; set; }
        public string em_full_name { get; set; }
        public string em_date_of_join { get; set; }
        public string dg_desc { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string reg_date { get; set; }
        public string reg_resion { get; set; }
        public string lo_amount { get; set; }
        public string lo_paid_amount { get; set; }
        public string opr { get; set; }
        public string grt_emp_leave_days { get; set; }
        public string grt_airfare_amount { get; set; }
        public string grt_amount { get; set; }
        public string grt_applicable_days { get; set; }
        public string grt_working_days { get; set; }
        public bool grt_leave_include_flag { get; set; }
        public bool grt_cash_bank_flag { get; set; }
        public bool grt_airfare_flag { get; set; }
        public bool ischecked { get; set; }

        public bool em_status { get; set; }

        public bool gratuaty_status { get; set; }
        public string activation_date { get; set; }
        public string remark { get; set; }
        public string grt_updated_user { get; set; }
        public string pe_year_month { get; set; }
        public string pe_number { get; set; }
        public string pe_status { get; set; }
        public string earning_amount { get; set; }
        public string deduction_amount { get; set; }
        public string net_amount { get; set; }
        public string sd_year_month_no { get; set; }
        public string sd_number { get; set; }
        public string sd_year { get; set; }
        public string leave_salary { get; set; }
        public string airfare { get; set; }
        public string netPayable { get; set; }
        public string totalEarning { get; set; }
        public string totalDeduction { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_field_value1 { get; set; }
        public string em_disengagement_type { get; set; }
    }
    #endregion

    //#region Pers400
    //public class Pers400
    //{
    //    public int srno { get; set; }

    //    public string leave_criteria_code { get; set; }

    //    public string leave_criteria_name { get; set; }

    //    public int leave_criteria_min_days { get; set; }

    //    public string leave_acct_code { get; set; }

    //    public string leave_bank_acct_code { get; set; }

    //    public string leave_cash_acct_code { get; set; }

    //    public string leave_airfare_acct_code { get; set; }

    //    public bool leave_status { get; set; }

    //    public object opr { get; set; }

    //    public object leave_pay_grade { get; set; }

    //    public string gr_code { get; set; }

    //    public string gr_desc { get; set; }

    //    public string leave_acct_name { get; set; }

    //    public string leave_bank_acct_name { get; set; }

    //    public string leave_cash_acct_name { get; set; }

    //    public string leave_airfare_acct_name { get; set; }

    //    public int year { get; set; }

    //    public int month { get; set; }

    //    public int days { get; set; }

    //    public string leave_prov_acct_code { get; set; }
    //    public string leave_prov_acct_name { get; set; }
    //    public string leave_formula_code { get; set; }
    //    public int leave_criteria_max_days { get; set; }
    //    public string leave_working_days { get; set; }
    //    public string leave_criteria_order { get; set; }
    //    public string leave_payable_days { get; set; }
    //}
    //#endregion

    public class shift_day
    {
        public bool sh_day_status { get; set; }
        public bool isdaychecked { get; set; }
        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
        public bool sh_template_status { get; set; }

        public string sh_template_id { get; set; }

        public string sh_day_code { get; set; }
    }

    #region Update Pyroll Status
    public class Per326
    {
        public string us_year { get; set; }
        public string us_month { get; set; }
        public string us_year_month_code { get; set; }
        public bool us_status { get; set; }
    }
    #endregion


    #region update employee hold status
    public class payrollOnholdStatus
    {
        public string pe_remark { get; set; }
        public string emp_code { get; set; }
        public string emp_name { get; set; }
        public string year_month { get; set; }
        public string year_month_name { get; set; }
        public bool onhold_status { get; set; }
        public string dep_name { get; set; }
        public string dep_code { get; set; }
        public string dg_code { get; set; }
        public string dg_name { get; set; }
    }
    #endregion


    #region dest_update(Employee Destination Update)
    public class dest_update
    {
        public string emp_name { get; set; }
        public string em_number { get; set; }
        public string emp_id { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string desg_code { get; set; }
        public string desg_name { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string dest_code { get; set; }
        public string dest_name { get; set; }
        public string frequency { get; set; }
    }
    #endregion

    public class Per256
    {
        public string pays_qualification_code { get; set; }
        public string pays_qualification_desc { get; set; }
        public string pays_qualification_desc_arabic { get; set; }
        public string pays_qualificaion_display_oder { get; set; }
        public bool pays_qualificaiton_status { get; set; }
        public string opr { get; set; }

        public string pays_leave_doc_code { get; set; }
        public string pays_leave_doc_name { get; set; }
        public string pays_leave_doc_desc { get; set; }
        public bool pays_leave_doc_is_mandatory { get; set; }
        public bool pays_leave_doc_status { get; set; }
    }
 public class V1
    {
        public string account_name1 { get; set; }
        public string debit_amt1 { get; set; }
        public string credit_amt1 { get; set; }
    }
    public class V2
    {
        public string account_name2 { get; set; }
        public string debit_amt2 { get; set; }
        public string credit_amt2 { get; set; }
    }
    #region
    public class Pers078
    {
        public List<V1> voucher1 { get; set; }
        public List<V2> voucher2 { get; set; }
        public string voucher_msg1 { get; set; }
        public string voucher_msg2 { get; set; }
        public string year_month_desc { get; set; }
        public string account_name1 { get; set; }
        public string debit_amt1 { get; set; }
        public string credit_amt1 { get; set; }
        public string account_name2 { get; set; }
        public string debit_amt2 { get; set; }
        public string credit_amt2 { get; set; }
        public string emp_list { get; set; }
        public string criteria_code { get; set; }
        public string criteria_name { get; set; }
        public string grt_amount { get; set; }
        public string grt_amount_old { get; set; }
        public string account_no { get; set; }
        public string account_name { get; set; }
        public string posting_date { get; set; }
        public string update_user { get; set; }

        public string cp_desc { get; set; }
        public string amount { get; set; }
        public string pd_debit_acno { get; set; }
        public string pd_credit_acno { get; set; }
        public string earn_ded_desc { get; set; }


        public string year_month { get; set; }
        public string fin_year { get; set; }
        public string fin_year_desc { get; set; }
        public bool finalize_status { get; set; }
        public bool holdstatus { get; set; }
        public string finalize_date { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string dg_name { get; set; }
        public string publishdata { get; set; }
        public string earn { get; set; }
        public string ded { get; set; }
        public string net_amount { get; set; }
        public string pe_bank_cash_tag { get; set; }
        public string pe_onhold_flag { get; set; }
        public string total { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string service_Status_code { get; set; }
        public string service_Status_desc { get; set; }
        public string gr_name { get; set; }
        public string gr_code { get; set; }
        public bool select_grade { get; set; }
        public string msg_code { get; set; }
        public string msg_desc { get; set; }
        public string bct_name { get; set; }
        public string bct_code { get; set; }
        public string emp_lst1 { get; set; }
        public string emp_lst2 { get; set; }


        public string sd_pay_code { get; set; }
        public string sd_number { get; set; }

        public string emp_name { get; set; }
        public string emp_number { get; set; }
        public string emp_login_code { get; set; }
        public string em_login_code { get; set; }

        public string sd_company_code { get; set; }
        public string sd_year_month { get; set; }
        public decimal sd_amount { get; set; }

        public string gr_desc { get; set; }

        public string em_bank_cash_tag { get; set; }

        public string pc_earn_dedn_tag { get; set; }

        public int total_employees { get; set; }
        public int active_employees { get; set; }
        public int employees_on_vacation { get; set; }
        public int payroll_generated { get; set; }
        public int currently_generated { get; set; }
        public string dg_desc { get; set; }

        public string EmployeeNameNo { get; set; }

        public string Company_code { get; set; }

        public string Loancode { get; set; }

        public string LoanDesc { get; set; }

        public string LoanAmmount { get; set; }

        public string Paid_amount { get; set; }

        public string Total_installments { get; set; }

        public string Repaid_installments { get; set; }

        public string sd_year_month_code { get; set; }

        public string sd_year_month_name { get; set; }

        public string em_grade_code { get; set; }

        public string dept_code { get; set; }

        public string dg_code { get; set; }
        
        public string dept_desc { get; set; }
        public string sd_remark { get; set; }
        public string pe_status { get; set; }
        public string comp_code { get; set; }
        public string comp_short_name { get; set; }
        public string comp_curcy_code { get; set; }
        public string comp_curcy_dec { get; set; }
        public bool include_air_fare { get; set; }
        public bool include_leave_salary { get; set; }

    }
    #endregion

    public class Per331
    {
        public string dept_name { get; set; }
        public string designation_name { get; set; }
        public string dg_code { get; set; }
        public string dg_desc { get; set; }
        public string pays_sr_no { get; set; }
        public string pays_vacancy_company_code { get; set; }
        public string pays_vacancy_dept_code { get; set; }
        public string pays_vacancy_desc { get; set; }
        public string pays_vacancy_doc { get; set; }
        public string pays_vacancy_expiry_date { get; set; }
        public string pays_vacancy_id { get; set; }
        public string pays_vacancy_level { get; set; }
        public string pays_vacancy_level_nm { get; set; }
        public string pays_vacancy_min_experience { get; set; }
        public string pays_vacancy_posting_date { get; set; }
        public string pays_vacancy_profile_desg_code { get; set; }
        public string pays_vacancy_roles { get; set; }
        public string pays_vacancy_status { get; set; }
        public string pays_vacancy_type { get; set; }
        public string pays_vacancy_type_nm { get; set; }
    }




    #region Pers072 Payroll Employee
    public class Pers072
    {
        public string pa_remark { get; set; }
        public string emp_amount { get; set; }
        public string inc_type { get; set; }
        public string inc_type_name { get; set; }
        public string inc_fixed_amout { get; set; }
        public string inc_per { get; set; }
        public string inc_period { get; set; }

        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string desg_code { get; set; }
        public string desg_name { get; set; }
        public string gr_code { get; set; }
        public string gr_name { get; set; }
        public string paycode_code { get; set; }
        public string paycode_name { get; set; }
        public string em_login_code { get; set; }
        public string emp_name { get; set; }
        public string pay_amount { get; set; }


        public bool hide_flag { get; set; }
        public string pd_remark { get; set; }
        public string pe_year_month { get; set; }
        public string em_number { get; set; }
        public string em_desg_desc { get; set; }
        public string sh_emp_name { get; set; }
        public string pe_doj { get; set; }
        public bool pe_status { get; set; }
        public bool pe_insert { get; set; }
        public string pa_effective_upto { get; set; }

        public string pc_earn_dedn_tag { get; set; }

        public decimal pd_amount { get; set; }

        public string pd_pay_code { get; set; }
        public string code { get; set; }

        public string cp_desc { get; set; }

        public string em_desg_code { get; set; }
        public string pc_earn_dedn_tag_name { get; set; }

        public string pa_effective_from { get; set; }

        public string em_company_code { get; set; }
        public string em_sponser_name { get; set; }
        public decimal ph_new_amount { get; set; }
        public decimal ph_old_amount { get; set; }
        public string ph_user_name { get; set; }
        public string pa_status { get; set; }
    }
    #endregion

    #region Pers092 pays_shift_master
    public class Pers092
    {
        public string sh_company_code { get; set; }
        public string sh_company_name { get; set; }
        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
        public string opr { get; set; }

        public int sh_shift1_in_greece_start { get; set; }

        public int sh_shift1_in_greece_end { get; set; }

        public int sh_shift1_out_greece_start { get; set; }

        public int sh_shift1_out_greece_end { get; set; }

        public int sh_shift2_in_greece_start { get; set; }

        public int sh_shift2_in_greece_end { get; set; }

        public int sh_shift2_out_greece_start { get; set; }

        public int sh_shift2_out_greece_end { get; set; }
    }
    #endregion


    public class Per610
    {
        public string co_desc { get; set; }
        public string co_company_code { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string dg_desc { get; set; }
        public string dg_code { get; set; }
        public string pays_doc_desc { get; set; }
        public string pays_doc_code { get; set; }
        public string em_login_code { get; set; }
        public string em_first_name { get; set; }
        public string pays_doc_srno { get; set; }
        public string pays_doc_path { get; set; }
        public string pays_doc_issue_date { get; set; }
        public string pays_doc_expiry_date { get; set; }
        public object pays_doc_mod_code { get; set; }
        public object pays_doc_empl_id { get; set; }
        public object pays_doc_status { get; set; }
        public object opr { get; set; }
    }


    #region Workflow
    public class Pers318_workflow
    {
        public string sims_workflow_det_empName;

        public string sims_balanceleaves { get; set; }
        public string sims_workflow_no { get; set; }
        public string sims_workflow_det_empno { get; set; }
        public string sims_workflow_transactionid { get; set; }
        public string sims_workflow_status { get; set; }
        public string sims_workflow_transdate { get; set; }
        public string sims_workflow_description { get; set; }
        public string sims_workflow_modcode { get; set; }
        public string sims_workflow_applcode { get; set; }

        public string sims_status { get; set; }
        public string leave_empname { get; set; }
        public string leave_compname { get; set; }
        public string leave_leavedesc { get; set; }
        public string leave_sdate { get; set; }
        public string leave_edate { get; set; }
        public string leave_days { get; set; }
        public string leave_remark { get; set; }

        public string EName { get; set; }
        public string Eorder { get; set; }
        public string EStatus { get; set; }
        public string Edate { get; set; }

        public string stVal { get; set; }

        public string Workflow_desc { get; set; }
        public string Workflow_sr_no { get; set; }
        public string Username { get; set; }
        public string Usercode { get; set; }
        public string det_empcode { get; set; }
        public string UserGrpCode { get; set; }

        public string sims_workflow_status_code { get; set; }

        public object sims_workflow_remark { get; set; }

        public string em_doc_path { get; set;}
        // public string sims_workflow_det_empName { get; internal set; }
    }

    #endregion

    public class empAttendance
    {
        public string emp_id { get; set; }
        public string att_date { get; set; }

        public string s1in { get; set; }
        public string s1out { get; set; }
        public string s2in { get; set; }
        public string s2out { get; set; }
        public string comment { get; set; }
        public string att_flag { get; set; }
    }

    #region grade change Pers109

    public class grade_change
    {

        public string gc_number { get; set; }
        public string gc_company_code { get; set; }
        public bool gc_mgr_confirm_tag { get; set; }
        public string gc_old_grade { get; set; }
        public string gc_new_grade { get; set; }
        public string gc_effect_from { get; set; }
        public string gc_retro_effect_from { get; set; }

        public string gc_grade_code { get; set; }

        public string first_name { get; set; }

        public object opr { get; set; }
    }
    #endregion

    #region Pers046
    public class Pers046
    {
        public string ds_code { get; set; }
        public string ds_name { get; set; }
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_value1 { get; set; }


        public string af_dest_code { get; set; }
        public string af_nation_code { get; set; }
        public string af_class { get; set; }
        public string af_adult_rate { get; set; }
        public string af_child_rate { get; set; }
        public string af_infant_rate { get; set; }



        public object opr { get; set; }
    }
    #endregion

    #region Pers309
    public class Pers309
    {
        public bool select_all { get; set; }
        public string login_id { get; set; }
        public string user_name { get; set; }
        public string deactivation_date { get; set; }
        public string status { get; set; }
        public string sims_student_status_updation_user_code { get; set; }
        public string remark { get; set; }
    }
    #endregion

    #region Pers315 Pays Attendance Code
    public class Pers315
    {
        public string pays_attendance_code { get; set; }
        public int pays_attendance_code_numeric { get; set; }
        public string pays_attendance_short_desc { get; set; }
        public string pays_attendance_description { get; set; }
        public string pays_attendance_present_value { get; set; }
        public string pays_attendance_absent_value { get; set; }
        public string pays_attendance_tardy_value { get; set; }
        public string pays_attendance_type_value { get; set; }
        public string pays_attendance_unexcused_flag { get; set; }
        public string pays_attendance_teacher_can_use_flag { get; set; }
        public string pays_attendance_color_code { get; set; }
        public string pays_attendance_status_inactive_date { get; set; }
        public bool pays_attendance_status { get; set; }
        //public DateTime pays_attendance_status_inactive_date { get; set;}
        public string opr { get; set; }
    }
    #endregion


    public class EmpDocMaster {

        public object pays_doc_code { get; set; }

        public object pays_doc_desc { get; set; }

        public object pays_doc_created_by { get; set; }

        public object opr { get; set; }

        public string pays_doc_mod_code { get; set; }

        public string pays_doc_create_date { get; set; }

        public bool pays_doc_status { get; set; }
    }

    #region Per312
    public class Per312
    {
        public string pays_doc_mod_code { get; set; }
        public string pays_doc_mod_name { get; set; }
        public string pays_doc_code { get; set; }
        public string pays_doc_srno { get; set; }
        public string pays_doc_empl_id { get; set; }
        public string pays_doc_desc { get; set; }
        public string pays_doc_desc1 { get; set; }
        public string pays_doc_path { get; set; }
        public string pays_doc_issue_date { get; set; }
        public string pays_doc_expiry_date { get; set; }
        public bool pays_doc_status { get; set; }
        public string pays_doc_empl_name { get; set; }
        public List<Per312> DocList { get; set; }
        public string opr { get; set; }


        public string company_code { get; set; }
        public string company_name { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string designation_code { get; set; }
        public string designation_name { get; set; }

        public string show { get; set; }

        public string pays_available_on_short_form { get; set; }
        public string pays_available_on_long_form { get; set; }
        public string pays_emp_visible_status { get; set; }
        public string pays_emp_upload_doc { get; set; }

    }
    #endregion
    public class Pers312
    {

        public string em_email { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_login_code { get; set; }
        public string em_mobile { get; set; }
        public string em_number { get; set; }
        public string em_status { get; set; }
        public string fullname { get; set; }
        public object opr { get; set; }
        public string pays_doc_code { get; set; }
        public string pays_doc_desc { get; set; }
        public string pays_doc_empl_id { get; set; }
        public string pays_doc_expiry_date { get; set; }
        public string pays_doc_issue_date { get; set; }
        public string pays_doc_mod_code { get; set; }
        public string pays_doc_path { get; set; }
        public string pays_doc_srno { get; set; }
        public bool pays_doc_status { get; set; }

        public object pays_doc_created_by { get; set; }


        public string pays_doc_create_date { get; set; }


    }


    public class DocDetails
    {
        public bool ischange { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_emp_name { get; set; }
        public bool sims_emp_status { get; set; }
        public bool sims_emp_cc { get; set; }
        public bool sims_emp_ca { get; set; }
        public string sims_teacher_type { get; set; }

        public object company_code { get; set; }

        public object dept_code { get; set; }

        public object desg_code { get; set; }

        public object grade_code { get; set; }

        public object pays_doc_empl_id { get; set; }

        public object pays_doc_desc { get; set; }
        public object pays_doc_expiry_date { get; set; }

        public object pays_doc_path { get; set; }


        public object pays_doc_issue_date { get; set; }

        public string codp_dept_name { get; set; }

        public string codp_comp_code { get; set; }

        public object co_desc { get; set; }

        public string co_company_code { get; set; }

        public string dg_desc { get; set; }

        public string em_desg_code { get; set; }

        public string em_dept_code { get; set; }

        public string codp_dept_no { get; set; }

        public string em_login_code { get; set; }

        public string em_first_name { get; set; }

        public string dg_code { get; set; }

        public string pays_doc_srno { get; set; }

        public string pays_doc_mod_code { get; set; }

        public string pays_doc_code { get; set; }

        public object pays_doc_status { get; set; }

        public string opr { get; set; }
    }


    public class Sim020
    {
        public string pays_doc_mod_code { get; set; }

        public string pays_doc_code { get; set; }

        public string pays_doc_desc { get; set; }

        public string pays_doc_create_date { get; set; }

        public object pays_doc_created_by { get; set; }

        public object opr { get; set; }

        public bool pays_doc_status { get; set; }
        public bool pays_recruitment_status { get; set; }
        public bool pays_available_on_short_form { get; set; }
        public bool pays_available_on_long_form { get; set; }
        public bool pays_emp_visible_status { get; set; }
        public bool pays_emp_upload_doc { get; set; }

    }


    #region Pers260
    public class Pers260
    {
        public string orderby_col { get; set; }
        public string orderby_name { get; set; }
        public string co_desc { get; set; }
        public string co_company_code { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string dg_desc { get; set; }
        public string dg_code { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string employee_name { get; set; }
        public string gr_code { get; set; }
        public string gr_desc { get; set; }
        public string em_date_of_join { get; set; }
        public string leave_type { get; set; }
        public string leave_code { get; set; }
        public string emp_code { get; set; }
        public bool select_flag { get; set; }
        public bool isenabled_flag { get; set; }

        public string leave_name { get; set; }

        public object emp_gender_code { get; set; }

        public string emp_leave_code { get; set; }

        public string emp_leave_name { get; set; }

        public string le_max_days_allowed { get; set; }

        public string le_days_taken { get; set; }

        public List<Leave> leavelist = new List<Leave>();

        public bool emp_leave_status { get; set; }

        public object comp_code { get; set; }



        public string sims_appl_parameter_gender { get; set; }

        public string sims_appl_form_field_value1_gender { get; set; }

        public object staff_code { get; set; }

        public string sims_religion_code { get; set; }
        public string emp_name { get; set; }

        public string sims_religion_name_en { get; set; }
        public string leave_delete_list { get; set; }
        public string user_name { get; set; }
    }

    public class Ucw240
    {

        public string sims_sip_achievement_type { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string sims_sip_achievement_desc { get; set; }

        public string sims_sip_achievement_point { get; set; }

        public string sims_sip_achievement_code { get; set; }

        public bool sims_sip_achievement_status { get; set; }

        public string sims_achievement_code { get; set; }

        public string sims_achievement_type { get; set; }

        public string opr { get; set; }
    }

    public class Ucw242
    {

        public string sims_sip_academic_year { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_sip_goal_code { get; set; }

        public string sims_sip_goal_desc { get; set; }

        public string sims_sip_goal_target_code { get; set; }

        public string sims_sip_goal_target_desc { get; set; }

        public string sims_sip_goal_target_start_date { get; set; }

        public string sims_sip_goal_target_end_date { get; set; }

        public string sims_sip_goal_target_min_point { get; set; }

        public string sims_sip_goal_target_max_point { get; set; }

        public bool sims_sip_goal_target_status { get; set; }

        public string opr { get; set; }

        public string sims_sip_academic_year_desc { get; set; }

        public string sims_sip_goal_start_date { get; set; }

        public string sims_sip_goal_end_date { get; set; }

        public string sims_sip_goal_max_point { get; set; }

        public string sims_sip_goal_min_point { get; set; }

        public string sims_sip_grading_scale { get; set; }

        public string sims_sip_goal_revision_type { get; set; }

        public string sims_sip_goal_revision_type_name { get; set; }

        public string sims_sip_goal_revision_type_period { get; set; }

        public bool sims_sip_goal_status { get; set; }
    }

    public class Ucw243
    {

        public string sims_sip_academic_year { get; set; }

        public string sims_sip_goal_code { get; set; }

        public string sims_sip_goal_desc { get; set; }

        public string sims_sip_goal_target_code { get; set; }

        public string sims_sip_goal_target_desc { get; set; }

        public string sims_sip_goal_target_kpi_code { get; set; }

        public string sims_sip_goal_target_kpi_desc { get; set; }

        public string sims_sip_goal_target_kpi_min_point { get; set; }

        public string sims_sip_goal_target_max_point { get; set; }

        public bool sims_sip_goal_target_kpi_status { get; set; }

        public string opr { get; set; }

        public string sims_sip_goal_target_kpi_max_point { get; set; }

        public string sims_sip_goal_target_kpi_measure_code { get; set; }
    }



    public class Per301
    {
        public string opr { get; set; }
        public string sims_sip_academic_year { get; set; }
        public string sims_sip_goal_code { get; set; }
        public string sims_sip_goal_target_code { get; set; }
        public string sims_sip_goal_target_kpi_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_user_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_reviewer1_achievement_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_reviewer2_achievement_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_reviewer3_achievement_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_reviewer4_achievement_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_reviewer5_achievement_code { get; set; }
        public string sims_sip_goal_target_kpi_measure_user_self_rating { get; set; }
        


        public string sims_sip_goal_target_reviewer1_strength { get; set; }
        public string sims_sip_goal_target_reviewer1_weakness { get; set; }
        public string sims_sip_goal_target_reviewer1_opportunity { get; set; }
        public string sims_sip_goal_target_reviewer1_threat { get; set; }



        public string sims_sip_reviewer_code { get; set; }
        public string sims_sip_reviewer_login_code { get; set; }
        public string emp_name { get; set; }
        public string sims_sip_reviewer_order { get; set; }
        public bool sims_sip_reviewer_status { get; set; }
     
    }

    public class Ucw244
    {

        public string sims_sip_academic_year { get; set; }

        public string sims_sip_goal_code { get; set; }

        public string sims_sip_goal_desc { get; set; }

        public string sims_sip_goal_target_code { get; set; }

        public string sims_sip_goal_target_desc { get; set; }

        public string sims_sip_goal_target_kpi_code { get; set; }

        public string sims_sip_goal_target_kpi_desc { get; set; }

        public string sims_sip_goal_target_kpi_measure_code { get; set; }

        public string sims_sip_goal_target_kpi_measure_desc { get; set; }

        public string sims_sip_goal_target_kpi_measure_min_point { get; set; }

        public string sims_sip_goal_target_kpi_measure_max_point { get; set; }

        public bool sims_sip_goal_target_kpi_measure_status { get; set; }

        public string opr { get; set; }

        public string sims_sip_goal_target_kpi_measure_auto_code { get; set; }
    }

    public class Pers059
    {
        public string company_code { get; set; }
        public string company_short_name { get; set; }
        public string company_desc { get; set; }
        public bool comp_currency_tag_S { get; set; }
        public bool comp_currency_tag_M { get; set; }
        public string comp_currency_tag { get; set; }
        public string comp_currency_name { get; set; }
        public string comp_currency_code { get; set; }
        public string comp_cut_off_day { get; set; }
        public string comp_effect_from { get; set; }
        public string comp_effect_to { get; set; }
        public string comp_hot_formula { get; set; }
        public string comp_hot_rate { get; set; }
        public string comp_normal_hrs { get; set; }
        public string comp_normal_ot_hrs { get; set; }
        public string comp_not_formula { get; set; }
        public string comp_not_rate { get; set; }
        public string comp_settlement_code { get; set; }
        public string comp_shot_formula { get; set; }
        public string comp_shot_rate { get; set; }
        public string comp_sot_formula { get; set; }
        public string comp_sot_rate { get; set; }
        public string se_formula_desc { get; set; }
        public string comp_special_hrs { get; set; }
        public string comp_travel_rate { get; set; }
        public string comp_weekly_off_day1_name { get; set; }
        public string comp_weekly_off_day1_code { get; set; }
        public string comp_weekly_off_day2_name { get; set; }
        public string comp_weekly_off_day2_code { get; set; }
        public string ca_company_code_name { get; set; }
        public string dg_company_code { get; set; }
        public string eg_company_code { get; set; }
        public string excg_curcy_code { get; set; }
        public string comp_not_formula_code { get; set; }
        public string comp_hot_formula_code { get; set; }
        public string comp_shot_formula_code { get; set; }
        public string comp_sot_formula_code { get; set; }
        public string se_formula_desc_code { get; set; }
        public string comp_short_name { get; set; }
        public string comp_not_formula_desc { get; set; }
        public string comp_hot_formula_desc { get; set; }
        public string comp_shot_formula_desc { get; set; }
        public string comp_sot_formula_desc { get; set; }
        public object opr { get; set; }
    }

    #region Employee Attendance Daily
    public class Pers326
    {
        public string emp_id { get; set; }
        public string codp_dept_name { get; set; }
        public string dg_desc { get; set; }
        public string att_description { get; set; }
        public string total_hours { get; set; }

        public string s1in { get; set; }

        public string s1out { get; set; }
        public string s2in { get; set; }

        public string s2out { get; set; }

        public string comment { get; set; }

        public string att_flag { get; set; }

        public string ac_no { get; set; }

        public string no { get; set; }

        public string name { get; set; }

        public string opr { get; set; }

        public string att_user_id { get; set; }
        public string att_emp_id { get; set; }
        public string emp_name { get; set; }
        public string att_shift_no { get; set; }
        public string att_date { get; set; }
        public bool emp_id_flag { get; set; }
        public string expected1In { get; set; }
        public string expected2In { get; set; }
        public string att1_in { get; set; }
        public string att1_out { get; set; }
        public string att2_in { get; set; }
        public string att2_out { get; set; }
        public string att_shift1_in { get; set; }
        public string att_shift2_in { get; set; }
        public string expected1Out { get; set; }
        public string expected2Out { get; set; }
        public string att_shift1_out { get; set; }
        public string att_shift2_out { get; set; }
        public string att1_delay { get; set; }
        public string att2_delay { get; set; }
        public string att1_early { get; set; }
        public string att2_early { get; set; }
        public string att_wrkd_hrs { get; set; }
        public string att_rem { get; set; }
        public string att_absent_flag { get; set; }

        public string dep_name { get; set; }
        public string dep_code { get; set; }
        public string dg_name { get; set; }
        public string dg_code { get; set; }
        public string leave_name { get; set; }
        public string leave_code { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string pays_attendance_color_code { get; set; }
        public List<leave_data> lst_leave { get; set; }


        public string s1_in { get; set; }
        public string s1_out { get; set; }
        public string s2_in { get; set; }
        public string s2_out { get; set; }

        public string att_remarks { get; set; }
    }
    public class leave_data
    {
        public string leave_pending { get; set; }
        public string leave_name { get; set; }
        public string leave_code { get; set; }
        public string emp_id { get; set; }
        public string pays_attendance_color_code { get; set; }
    }

    public class AttendanceDailyUpload
    {
        public string emp_id { get; set; }

        public string att_date { get; set; }

        public string s1in { get; set; }

        public string s1out { get; set; }

        public string comment { get; set; }

        public string att_flag { get; set; }

        public string ac_no { get; set; }

        public string no { get; set; }

        public string name { get; set; }

        public string opr { get; set; }
    }
    #endregion

    public class Leave
    {
        public bool emp_leave_status { get; set; }
        public string emp_leave_name { get; set; }
        public string emp_leave_code { get; set; }

        public bool ischange_new { get; set; }
    }
    #endregion
    public class Pers060
    {
        public int cn_code { get; set; }
        public string cn_desc { get; set; }
        public decimal cn_duration { get; set; }
        public bool cn_prorata_flat_tag { get; set; }
        public decimal cn_min_work_days { get; set; }
        public decimal cn_first_cl_working_days { get; set; }
        public decimal cn_first_cl_leave_days { get; set; }
        public decimal cn_cons_cl_working_days { get; set; }
        public decimal cn_cons_cl_leave_days { get; set; }


        public object dg_company_code { get; set; }

        public object opr { get; set; }

        public string dg_company_name { get; set; }
    }
    #region Pers308
    public class Pers308
    {
        public bool select_all { get; set; }
        public string login_id { get; set; }
        public string user_name { get; set; }
        public string activation_date { get; set; }
        public string status { get; set; }
        public string sims_student_status_updation_user_code { get; set; }
        public string em_number { get; set; }
        public string em_company_code { get; set; }
    }
    #endregion


    public class FormulaClass
    {

        public string opr { get; set; }
        public string company_code { get; set; }
        public string company_name { get; set; }
        public string formula_name { get; set; }
        public string pd_pay_code { get; set; }
        public string paysEarnDed { get; set; }
        public string pd_pay_code_nm { get; set; }
        public string pay_code_description { get; set; }
        public string serial_number { get; set; }
        public string percantage_number { get; set; }
        public string month_number { get; set; }
        public string operatore { get; set; }
        public string companyname { get; set; }
        public string pdpaycode { get; set; }
        public string formulaname { get; set; }

        public int percantagenumber { get; set; }
        public int monthnumber { get; set; }
        public string operatornumber { get; set; }
    }

    public class Pers099_new
    {

        public string em_house_name { get; set; }
        public string bank_name { get; set; }
        public string nationality_name { get; set; }
        public string chk_em_sponser_name { get; set; }
        public string chk_em_relation { get; set; }
        public string chk_em_occupation { get; set; }
        public string chk_em_workplace { get; set; }
        public string chk_em_office_phone { get; set; }
        public string chk_em_mobile1 { get; set; }
        public string chk_visa_num { get; set; }
        public string chk_visa_issue_date { get; set; }
        public string chk_visa_exp_date { get; set; }
        public string chk_em_attr1 { get; set; }
        public string chk_em_attr2 { get; set; }
        public string chk_em_attr3 { get; set; }
        public string chk_em_attr4 { get; set; }
        public string chk_em_attr5 { get; set; }
        public bool chk_em_status1 { get; set; }
        public string chk_em_national_id1 { get; set; }
        public string chk_em_accommodation { get; set; }
        public string chk_em_sponsership { get; set; }
        public string chk_em_visa_Type { get; set; }

        public string em_sponser_id { get; set; }
        public string em_sponser_name { get; set; }
        public string em_sponser_name_ar { get; set; }
        public string em_sponser_address_en { get; set; }
        public string em_sponser_address_ar { get; set; }
        public string em_workplace { get; set; }
        public string em_workplace_ar { get; set; }
       
        public string em_accommodation_status { get; set; }
        public string em_pwd { get; set; }
        public string em_login_code { get; set; }
        public string em_company_name { get; set; }
        public string em_dept_name { get; set; }

        public string em_dest_desc { get; set; }
        public string em_nation_name { get; set; }
        public string em_grade_name { get; set; }
        public string em_staff_type { get; set; }
        public string em_salutation { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_family_name { get; set; }
        ///public string em_house_name { get; set; }
        public string em_name_ot { get; set; }
        public string em_dept_effect_from { get; set; }
        public string em_grade_effect_from { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string em_service_status { get; set; }
        public string em_sex { get; set; }
        public string em_marital_status { get; set; }
        public bool em_bank_cash_tag { get; set; }
        public string em_ledger_name { get; set; }
        public string em_ledger_ac_no { get; set; }
        public string em_gpf_ac_no { get; set; }
        public string em_gosi_ac_no { get; set; }
        public string em_gosi_start_date { get; set; }
        public string em_pan_no { get; set; }
        public string em_labour_card_no { get; set; }
        public string em_iban_no { get; set; }
        public string em_route_code { get; set; }
        public string em_status { get; set; }
        public bool em_stop_salary_indicator { get; set; }
        public string em_leave_resume_date { get; set; }
        public string em_leave_resume_date1 { get; set; }
        public bool em_leave_tag { get; set; }
        public string em_modified_on { get; set; }
        public string em_modified_on1 { get; set; }
        public bool em_citi_exp_tag { get; set; }
        public string em_joining_ref { get; set; }
        public string em_bank_name { get; set; }
        public string em_bank_ac_no { get; set; }
        public string em_bank_swift_code { get; set; }
        public int em_dependant_full { get; set; }
        public int em_dependant_half { get; set; }
        public int em_dependant_infant { get; set; }
        public string em_left_date { get; set; }
        public string em_left_reason { get; set; }
        public bool em_handicap_status { get; set; }
        public string em_leave_start_date { get; set; }
        public string em_leave_end_date { get; set; }
        public string em_cl_resume_date { get; set; }
        public string em_leave_resume_ref { get; set; }
        public int em_over_stay_days { get; set; }
        public int em_under_stay_days { get; set; }
        public string em_stop_salary_from { get; set; }
        public string em_unpaid_leave { get; set; }
        public bool em_punching_status { get; set; }
        public string em_punching_id { get; set; }
        public string em_passport_no { get; set; }
        public DateTime? em_pssport_issue_date { get; set; }
        public string em_passport_issuing_authority { get; set; }
        public string em_passport_issue_place { get; set; }
        public string em_passport_ret_date { get; set; }
        public string em_passport_exp_date { get; set; }
        public string em_pssport_exp_rem_date { get; set; }
        public string em_visa_no { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_issuing_place { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_visa_exp_date { get; set; }
        public string em_visa_exp_rem_date { get; set; }
        public bool em_agreement { get; set; }
        public string em_agreement_start_date { get; set; }
        public string em_agreement_exp_date { get; set; }
        public string em_agreemet_exp_rem_date { get; set; }
        public string em_passport_issue_date { get; set; }
        public string em_visa_sponsor { get; set; }
        public string em_visa_type { get; set; }
        public string em_apartment_number { get; set; }
        public string em_building_number { get; set; }
        public string em_street_number { get; set; }
        public string em_area_number { get; set; }
        public string em_summary_address { get; set; }
        public string em_summary_address_local_language { get; set; }
        public string em_religion_name { get; set; }
        public string em_ethnicity_name { get; set; }
        public string em_city { get; set; }
        public string em_state { get; set; }
        public string em_country { get; set; }
        public string em_phone { get; set; }
        public string em_mobile { get; set; }
        public string spem_mobile { get; set; }
        public string em_email { get; set; }
        public string em_fax { get; set; }
        public string em_box { get; set; }
        public string em_po_fax { get; set; }
        public string em_Emp_Person_Email { get; set; }
        public string em_img1 { get; set; }
        public string em_emergency_contact_name1 { get; set; }
        public string em_emergency_contact_name2 { get; set; }
        public string em_emergency_contact_number1 { get; set; }
        public string em_emergency_contact_number2 { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string em_last_login { get; set; }
        public string em_secret_question_code { get; set; }
        public string em_secret_answer { get; set; }
        public string _sims_grade_name
        {
            get;
            set;
        }
        public string em_Country_Code { get; set; }
        public string em_Companny_Code { get; set; }
        public string em_Company_Short_Name { get; set; }
        public string em_Deptt_Code { get; set; }
        public string em_Dept_Short_Name { get; set; }
        public string em_Dept_Company_Code { get; set; }        ////1
        public string em_Nation_Code { get; set; }
        public string em_Gradee_Code { get; set; }
        public string em_Grade_Company_Code { get; set; }       /////2
        public string em_Staff_Type_Code { get; set; }
        public string em_Salutation_Code { get; set; }
        public string em_Sex_Code { get; set; }
        public string em_Marital_Status_Code { get; set; }
        public string em_Designation_Code { get; set; }
        public string em_Designation_Company_Code { get; set; }     ////3
        public string em_Dest_Code { get; set; }        /////////////////////
        public string em_Religion_code { get; set; }

        public string em_status_code { get; set; }

        public string em_number { get; set; }

        public string em_img { get; set; }

        public string em_desg_desc { get; set; }

        public string em_desg_code { get; set; }
        public string em_stat { get; set; }
        public string em_company_code { get; set; }
        public string em_qualification { get; set; }
        public string em_Qualification_Arabic { get; set; }
        public string code { get; set; }

        //New variables
        public string em_ethnicity_code { get; set; }

        public string em_city_code { get; set; }

        public string em_state_code { get; set; }

        public string em_bank_code { get; set; }

        public string em_service_status_code { get; set; }


        public int teacher_count { get; set; }

        public int class_teacher_count { get; set; }

        public int bell_section_subject_teacher_count { get; set; }

        public string em_full_name { get; set; }

        public string em_attendance_month_name { get; set; }
        public string em_attendance_working_days { get; set; }
        public string em_attendance_absent_days { get; set; }
        public string em_attendance_prasent_days { get; set; }
        public string em_attendance_leave_days { get; set; }

        public string leave_type { get; set; }
        public string leave_taken { get; set; }
        public string leave_max_allowed { get; set; }
        public string leaves_extra { get; set; }
        public string leaves_remaing { get; set; }
        //end

        public string sh_company_code { get; set; }

        public string sh_company_name { get; set; }

        public string em_expiry_date { get; set; }

        public string em_division_code { get; set; }

        public string em_grade_code { get; set; }

        //--Ministory
        public string em_adec_approval_number { get; set; }
        public string em_adec_approval_Date { get; set; }
        public string em_adec_approved_qualification { get; set; }
        public string em_adec_approved_designation { get; set; }
        public string em_adec_approved_subject { get; set; }
        public string em_adec_approved_level { get; set; }

        //--Helth
        public string em_health_card_no { get; set; }
        public string em_health_card_effective_upto_date { get; set; }
        public string em_health_card_effective_from_date { get; set; }
        public string em_heath_card_status { get; set; }
        public string em_Tot_exp { get; set; }
        public string em_date_of_Left { get; set; }

        public string em_voter_id { get; set; }


        public string em_qualification_inst_name { get; set; }

        public string adec_approved_qualification { get; set; }

        public string adec_approved_designation { get; set; }

        public string adec_approved_subject { get; set; }
        public string en_labour_card_expiry_date { get; set; }
    }




    public class Pers099
    {
        // new added variables on 30-11-2015
        public bool em_residency { get; set; }
        public string comn_user_name { get; set; }
        public string comn_user_email { get; set; }
        public string em_adec_approval_number { get; set; }        
        public string em_adec_approval_Date { get; set; }
        public string em_adec_approved_level { get; set; }
        public string em_adec_approved_qualification { get; set; }
        public string em_adec_approved_designation { get; set; }
        public string em_adec_approved_subject { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_passport_first_name_en{get; set;}
        public string student_name { get; set; }
        // old variables
        public Pers099()
        {
            qual_details = new List<Pers099_qual>();
        }
        public string em_pwd { get; set; }
        public string em_doc { get; set; }
        public string em_login_code { get; set; }
        public string em_number { get; set; }

        public string em_dept_effect_from { get; set; }
        public string em_grade_effect_from { get; set; }


        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_family_name { get; set; }
        public string em_full_name { get; set; }
        public string em_name_ot { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string em_service_status_code { get; set; }
        public string em_service_status { get; set; }
        public string em_marital_status { get; set; }
        public bool em_bank_cash_tag { get; set; }
        public string em_ledger_name { get; set; }
        public string em_ledger_ac_no { get; set; }
        public string em_gpf_ac_no { get; set; }
        public string em_gosi_ac_no { get; set; }
        public string em_gosi_start_date { get; set; }
        public string em_pan_no { get; set; }
        public string em_labour_card_no { get; set; }
        public string em_iban_no { get; set; }
        public string em_route_code { get; set; }
        public string em_status_code { get; set; }
        public bool em_status { get; set; }
        public string application_status { get; set; }
        public bool em_stop_salary_indicator { get; set; }
        public string em_leave_resume_date { get; set; }
        public bool em_leave_tag { get; set; }
        public string em_modified_on { get; set; }
        public bool em_citi_exp_tag { get; set; }
        public string em_joining_ref { get; set; }
        public string em_bank_name { get; set; }
        public string em_bank_ac_no { get; set; }
        public string em_bank_swift_code { get; set; }
        public string em_dependant_full { get; set; }
        public string em_dependant_half { get; set; }
        public string em_dependant_infant { get; set; }
        public string em_left_date { get; set; }
        public string em_left_reason { get; set; }
        public bool em_handicap_status { get; set; }
        public string em_leave_start_date { get; set; }
        public string em_leave_end_date { get; set; }
        public string em_cl_resume_date { get; set; }
        public string em_leave_resume_ref { get; set; }
        public string em_over_stay_days { get; set; }
        public string em_under_stay_days { get; set; }
        public string em_stop_salary_from { get; set; }
        public string em_unpaid_leave { get; set; }
        public bool em_punching_status { get; set; }
        public string em_punching_id { get; set; }
        public string em_passport_no { get; set; }
        public string em_pssport_issue_date { get; set; }
        public string em_passport_issuing_authority { get; set; }
        public string em_passport_issue_place { get; set; }
        public string em_passport_ret_date { get; set; }
        public string em_passport_exp_date { get; set; }
        public string em_passport_exp_rem_date { get; set; }
        public string em_visa_no { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_issuing_place { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_visa_exp_date { get; set; }
        public string em_visa_exp_rem_date { get; set; }
        public bool em_agreement { get; set; }

        public string em_agreement_code { get; set; }
        public string em_agreement_start_date { get; set; }
        public string em_agreement_exp_date { get; set; }
        public string em_agreemet_exp_rem_date { get; set; }

        public string em_passport_issue_date { get; set; }
        public string em_visa_sponsor { get; set; }
        public string em_visa_type { get; set; }
        public string em_visa_type_code { get; set; }
        public string em_apartment_number { get; set; }
        public string em_building_number { get; set; }
        public string em_street_number { get; set; }
        public string em_area_number { get; set; }
        public string em_summary_address { get; set; }
        public string em_summary_address_local_language { get; set; }
        public string em_religion_code { get; set; }
        public string em_ethnicity_name { get; set; }

        public string em_img { get; set; }
        public string em_emergency_contact_name1 { get; set; }
        public string em_emergency_contact_name2 { get; set; }
        public string em_emergency_contact_number1 { get; set; }
        public string em_emergency_contact_number2 { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string em_last_login { get; set; }
        public string em_secret_question_code { get; set; }
        public string em_secret_question { get; set; }
        public string em_secret_answer { get; set; }

        public string em_Country_Code { get; set; }
        public string em_Country_name { get; set; }

        public string em_State_Code { get; set; }
        public string em_State_name { get; set; }

        public string em_City_Code { get; set; }
        public string em_City_name { get; set; }



        public string em_blood_group_code { get; set; }
        public string em_blood_group_name { get; set; }

        public string em_ethnicity_name_en { get; set; }
        public string em_ethnicity_code { get; set; }


        public string em_company_code { get; set; }
        public string em_Company_Code { get; set; }
        public string em_Company_name { get; set; }
        public string em_Company_Short_Name { get; set; }

        public string em_Dept_Code { get; set; }
        public string em_Dept_name { get; set; }

        public string em_Dept_Short_Name { get; set; }
        public string em_Dept_Company_Code { get; set; }

        public string em_Nation_Code { get; set; }
        public string em_Nation_name { get; set; }

        public string em_Grade_Code { get; set; }
        public string em_Grade_name { get; set; }
        public string em_Grade_Company_Code { get; set; }


        public string em_staff_type { get; set; }
        public string em_Staff_Type_Code { get; set; }
        public string em_Staff_Type_name { get; set; }

        public string em_salutation { get; set; }
        public string em_Salutation_Code { get; set; }
        public string em_Salutation_name { get; set; }

        public string em_Sex_Code { get; set; }
        public string em_Sex_name { get; set; }



        public string em_Marital_Status_Code { get; set; }
        public string em_Marital_Status_name { get; set; }

        public string em_desg_name { get; set; }
        public string em_Designation_Code { get; set; }
        public string em_Designation_name { get; set; }
        public string em_Designation_Company_Code { get; set; }

        public string em_Dest_Code { get; set; }
        public string em_Dest_name { get; set; }



        public string em_Religion_Code { get; set; }
        public string em_Religion_name { get; set; }

        public string em_Role_Code { get; set; }
        public string em_Role_name { get; set; }
        public string em_Role_desg_code { get; set; }
        public string em_Role_desg_company_code { get; set; }

        //public string em_City_Code { get; set; }
        //public string em_City_name { get; set; }


        //public string em_State_Code { get; set; }
        //public string em_State_name { get; set; }

        public string em_Bank_Code { get; set; }
        public string em_Bank_name { get; set; }



        public string em_fax { get; set; }
        public string em_post { get; set; }

        public string em_phone { get; set; }
        public string em_mobile { get; set; }

        public string em_email { get; set; }




        public string em_attendance_month_name { get; set; }
        public string em_attendance_working_days { get; set; }
        public string em_attendance_absent_days { get; set; }
        public string em_attendance_prasent_days { get; set; }
        public string em_attendance_leave_days { get; set; }

        public string leave_type { get; set; }
        public string leave_taken { get; set; }
        public string leave_max_allowed { get; set; }
        public string leaves_extra { get; set; }
        public string leaves_remaing { get; set; }
        public List<Pers099_qual> qual_details;

        public string opr { get; set; }

        public string code { get; set; }

        public string em_dept_name { get; set; }

        public string em_desg_code { get; set; }

        public string em_desg_desc { get; set; }

        public string em_pssport_exp_rem_date { get; set; }

        public string comn_role_code { get; set; }

        public string comn_role_name { get; set; }

        public string lic_school_code { get; set; }

        public string lic_school_name { get; set; }

        public string lic_school_country { get; set; }

        public string subopr { get; set; }

        public string em_work_permit_issue_date { get; set; }

        public string em_work_permit_expiry_date { get; set; }

        public string em_emergency_relation1 { get; set; }

        public string em_emergency_relation2 { get; set; }

        public string sims_cur_level_code { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string pays_qualification_code { get; set; }
        public string pays_qualification_desc { get; set; }
        public string dg_code { get; set; }
        public string dg_desc { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_subject_code { get; set; }

        public string em_health_card_number { get; set; }
        public string em_health_card_issue_date { get; set; }
        public string em_health_card_expiry_date { get; set; }
        public string em_rta_number { get; set; }
        public string em_rta_issue_date { get; set; }
        public string em_rta_expiry_date { get; set; }

        public string em_resident_in_qatar_since { get; set; }
        public string em_remarks_awards { get; set; }
        public string em_name_of_spouse { get; set; }
        public string em_Spouse_Designation_Code { get; set; }
        public string em_spouse_organization { get; set; }
        public string em_spouse_qatar_id { get; set; }
        public string em_spouse_contact_number { get; set; }
        public string pays_work_type_code { get; set; }
        public string pays_work_type_name { get; set; }

        public string manual_em_login_code { get; set; }

        public string em_division_code { get; set; }

        public string gd_division_desc { get; set; }

        public string gd_division_code { get; set; }

        public string codp_comp_code { get; set; }

        public string codp_dept_no { get; set; }

        public string codp_year { get; set; }

        public string codp_dept_name { get; set; }

        public string codp_short_name { get; set; }

        public string codp_dept_type { get; set; }

        public string codp_status { get; set; }

        public string codp_division { get; set; }

        public string gr_code { get; set; }

        public string gr_desc { get; set; }

        public string gr_category_code { get; set; }


        public string em_personalemail { get; set; }

        public bool em_teacher_status { get; set; }

        public string em_schoolbranch_code { get; set; }

        public string em_schoolbranch_name { get; set; }
        
        public string em_health_card_no { get; set; }
        public string em_health_card_effective_from_date { get; set; }
        public string em_health_card_effective_upto_date { get; set; }        
        public bool em_heath_card_status { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_field_value1 { get; set; }
        public string em_accommodation_status { get; set; }
        public string em_sponser_id { get; set; }
        public string em_sponser_name { get; set; }
        public string em_sponser_name_ar { get; set; }
        public string sponser_em_mobile { get; set; }
        public string em_sponser_address_en { get; set; }
        public string em_sponser_address_ar { get; set; }
        public string em_workplace { get; set; }
        public string em_workplace_ar { get; set; }
        public string afe_airport { get; set; }
        public string afe_applicable_frequency { get; set; }
        public string afe_travelling_date { get; set; }
        public string afe_returing_date { get; set; }
        public string afe_reimbursement { get; set; }
        public string afe_class { get; set; }
        public string afe_applicable_month { get; set; }
        public string afe_adult_rate { get; set; }
        public string afe_child_rate { get; set; }
        public string afe_infant_rate { get; set; }
        public string afe_nation_code { get; set; }
        public string afe_Dest_Code { get; set; }
        public string em_voter_id { get; set; }
        public bool afe_finalize_status { get; set; }
        public string afe_id { get; set; }
        public string en_labour_card_expiry_date { get; set; }
        public string em_house { get; set; }
        public string em_Code { get; set; }
        public string em_national_id_expiry_rem_date { get; set; }
        public string offer_letter_doc { get; set; }
        public string em_expected_date_of_join { get; set; }
        public string em_offer_letter_send_date { get; set; }
        public string em_interview_date { get; set; }
        public string em_disengagement_type { get; set; }
        public string em_probation_period_days { get; set; }
        public string em_probation_completion_date { get; set; }
        public string em_probation_confirmation_date { get; set; }
        public bool em_khda_noc_required { get; set; }
        public bool em_khda_noc_submitted { get; set; }
        public bool em_labour_noc_required { get; set; }
        public bool em_labour_noc_submitted { get; set; }
        public string em_pro_visa_number { get; set; }
        public string em_visa_uid { get; set; }
        public string em_relation { get; set; }
        public string visa_num { get; set; }
        public string visa_issue_date { get; set; }
        public string visa_exp_date { get; set; }
        public string en_labour_card_issue_date { get; set; }
        public string em_applicant_id { get; set; }
        public string em_home_address { get; set; }
        public string pays_hobbies_code { get; set; }
        public string pays_hobbies_desc { get; set; }

        public string em_code { get; set; }
        public string em_desc { get; set; }
    }

    public class Per235
    {


        public string pays_comp_code { get; set; }
        public string pays_comp_name { get; set; }
        public string pays_appl_code { get; set; }

        public string pays_appl_name_en { get; set; }

        public string pays_appl_form_field { get; set; }

        public string pays_appl_parameter { get; set; }

        public string pays_appl_form_field_value1 { get; set; }

        public string pays_appl_form_field_value2 { get; set; }

        public string pays_appl_form_field_value3 { get; set; }

        public string pays_appl_form_field_value4 { get; set; }

        public string pays_application_code { get; set; }

        public string pays_application_name { get; set; }

        public string opr { get; set; }

        public string old_form { get; set; }

        public string old_parameter { get; set; }

        public string old_value1 { get; set; }
    }


    public class Pe078V
    {
        public string year1 { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public string invs_appl_parameter { get; set; }
        public string fins_appl_form_field_value1 { get; set; }
    }

    public class EmpDashboard
    {


        public string visa_type { get; set; }
        public string visa_type_count { get; set; }

        public string gender { get; set; }
        public string gender_count { get; set; }

        public string sims_country_longitude { get; set; }
        public string sims_country_latitude { get; set; }
        public string male_student_count { get; set; }
        public string female_student_count { get; set; }
        public string sims_country_code { get; set; }
        public string student_count { get; set; }
        public string sims_country_name_en { get; set; }
        public string total_count { get; set; }
        public string male { get; set; }
        public string female { get; set; }





        public string presents { get; set; }
        public string absents { get; set; }

        public string leavewithougthallowns { get; set; }
        public string leave { get; set; }
        public string unmark { get; set; }
        public string total { get; set; }

        public string fullname { get; set; }
        public string em_email { get; set; }
        public string em_mobile { get; set; }
        public string att_absent_flag { get; set; }
        public string em_login_code { get; set; }

        public string sims_field_code { get; set; }

        public string sims_field_name { get; set; }

        public string sims_field_iseditable { get; set; }

        public string sims_field_isvisible { get; set; }

        public string sims_field_attribute { get; set; }
        public string sims_field_attribute_html { get; set; }
        public string codp_dept_name { get; set; }
        public string em_sex { get; set; }

        public string counter { get; set; }








        public string emp_leave_code { get; set; }
        public string emp_leave_name { get; set; }

        public string emp_leavedays_code { get; set; }


        public string sims_subject_code { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_gb_name { get; set; }


        public string emp_leavedays_maxdays { get; set; }

        public string le_max_days_allowed { get; set; }

        public string emp_atten_flag { get; set; }

        public string em_first_name { get; set; }

        public object emp_att_date { get; set; }

        public object emp_att_emp_id { get; set; }

        public string le_days_taken { get; set; }

        public string em_check_in { get; set; }

        public string em_check_out { get; set; }

        public string emp_grade_name { get; set; }

        public string emp_grade_code { get; set; }

        public string emp_grade_strength { get; set; }

        public string emp_grade_percentage { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name { get; set; }

        public string emp_total_stud { get; set; }

        public string emp_present_stud { get; set; }

        public string attendance_code { get; set; }

        public string tardy_cnt { get; set; }

        public string emp_sub_code { get; set; }

        public string emp_sub_name { get; set; }

        public string emp_absent_stud { get; set; }

        public string emp_absent_stud_percentage { get; set; }

        public string emp_tardy_stud { get; set; }

        public string emp_tardy_stud_percentage { get; set; }

        public string sims_attendance_day_attendance_code { get; set; }

        public string sims_enrollment_number { get; set; }

        public string sims_student_name { get; set; }
        public string emp_unmark_cnt { get; set; }

        public string emp_unmark_percentage { get; set; }

        public string emp_absent_count { get; set; }

        //class For Staff Personal Data
        public string em_number { get; set; }

        public string emp_name { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string em_desg_code { get; set; }
        public string em_designation { get; set; }
        public string em_gender { get; set; }
        public string emp_marital_status { get; set; }
        public string em_division { get; set; }
        public string em_national_id { get; set; }
        public string em_dept_code { get; set; }
        public string em_department { get; set; }
        public string em_nationality { get; set; }
        public string em_service_status { get; set; }
        public string service_status { get; set; }
        public string em_religion_code { get; set; }
        public string religion_code { get; set; }
        public string gd_division_desc { get; set; }
        public string staff_count { get; set; }
        public string department { get; set; }
        public string em_left_reason { get; set; }
        public string em_left_date { get; set; }
        public string division { get; set; }
        public string em_grade_code { get; set; }
        public string grade_name { get; set; }



        public string comn_user_appl_code { get; set; }

        public string comn_appl_location_html { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_mod_code { get; set; }
        public string comn_mod_name_en { get; set; }
        public string comn_appl_type { get; set; }



        public string comn_appl_location { get; set; }

        public string comn_appl_name_ar { get; set; }
    }

    public class EmpDashboard_invs
    {
        public string item_req{ get; set; }
        public string service_req{ get; set; }
        public string req_item{ get; set; }
        public string req_service { get; set; }

        public string ord_no{ get; set; }
        public string orderedQty{ get; set; }
        public string receivedQty{ get; set; }
        public string totalOrderedAmount{ get; set; }
        public string totalReceivedAmount { get; set; }
        public string year{ get; set; }
        public string month{ get; set; }
        public string month_Name{ get; set; }
        public string total_Order_Count { get; set; }
        public string ordered_Count{ get; set; }
        public string received_Count { get; set; }

    }

    public class Per305
    {

        public string sims_goal_kpi_academic_year { get; set; }

        public string sims_goal_kpi_academic_year_desc { get; set; }

        public string sims_kpi_goal_code { get; set; }

        public string sims_kpi_goal_desc { get; set; }

        public string sims_kpi_goal_target_code { get; set; }

        public string sims_kpi_goal_target_desc { get; set; }

        public string sims_sip_goal_target_kpi_code { get; set; }

        public string sims_sip_goal_target_kpi_desc { get; set; }

        public string sims_kpi_measure_code { get; set; }

        public string sims_kpi_measure_desc { get; set; }

        public string sims_sip_achievement_code { get; set; }

        public string sims_sip_achievement_desc { get; set; }

        public string sims_achievement_category_code { get; set; }

        public string sims_achievement_category_desc { get; set; }

        public string sims_sip_academic_year { get; set; }

        public string sims_sip_goal_code { get; set; }

        public string sims_sip_goal_desc { get; set; }

        public string sims_sip_goal_target_code { get; set; }

        public string sims_sip_goal_target_desc { get; set; }

        public string sims_sip_goal_target_kpi_measure_desc { get; set; }

        public string sims_sip_goal_target_kpi_measure_code { get; set; }

        public string sims_sip_goal_target_kpi_achievement_code { get; set; }

        public string sims_sip_goal_target_kpi_achievement_category_code { get; set; }

        public bool sims_sip_goal_target_kpi_achievement_status { get; set; }

        public string sims_sip_goal_target_kpi_achievement_category_desc { get; set; }

        public string opr { get; set; }

        public object old_sims_sip_goal_target_kpi_measure_code { get; set; }

        public object old_sims_sip_goal_target_kpi_achievement_code { get; set; }

        public string sims_sip_goal_target_kpi_measure_code_m { get; set; }

        public string sims_sip_goal_target_kpi_measure_desc_m { get; set; }


    }

    #region update leave balance
    public class Per314
    {
        public string emp_code { get; set; }
        public string emp_name { get; set; }
        public string leave_name { get; set; }
        public string leave_code { get; set; }
        public string leave_taken { get; set; }
        public string leave_bal { get; set; }
        public string leave_max_old { get; set; }
        public string leave_max { get; set; }
        public string dep_name { get; set; }
        public string dep_code { get; set; }
        public string dg_code { get; set; }
        public string dg_name { get; set; }
        public string user_name { get; set; }
    }
    #endregion


    public class Per302
    {

        public string desg_company_code { get; set; }

        public string desg_company_code_name { get; set; }

        public string sip_company_code { get; set; }

        public string sip_company_code_name { get; set; }

        public string desg_code { get; set; }

        public string desg_name { get; set; }

        public string dept_code { get; set; }

        public string dept_name { get; set; }

        public string sip_user_name { get; set; }

        public string sims_sip_goal_target_schedule1 { get; set; }

        public string sims_sip_user_name { get; set; }

        public string sims_sip_goal_target_reviewer1_code { get; set; }

        public string emp_name1 { get; set; }

        public string sims_sip_goal_target_schedule2 { get; set; }

        public string sims_sip_goal_target_reviewer2_code { get; set; }

        public string emp_name2 { get; set; }

        public string sims_sip_goal_target_schedule3 { get; set; }

        public string sims_sip_goal_target_reviewer3_code { get; set; }

        public string emp_name3 { get; set; }

        public string sims_sip_goal_target_schedule4 { get; set; }

        public string sims_sip_goal_target_reviewer4_code { get; set; }

        public string emp_name4 { get; set; }

        public string sims_sip_goal_target_schedule5 { get; set; }

        public string sims_sip_goal_target_reviewer5_code { get; set; }

        public string emp_name5 { get; set; }

        public string sims_student_passport_first_name_en { get; set; }

        public string sims_student_passport_middle_name_en { get; set; }

        public string sims_student_passport_last_name_en { get; set; }

        public string sims_user_name { get; set; }

        public string sims_student_login_id { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_sip_reviewer_code { get; set; }

        public string sims_sip_reviewer_name { get; set; }

        public string opr { get; set; }

        public string sims_sip_academic_year { get; set; }

        public string sims_sip_goal_code { get; set; }

        public string sims_sip_goal_target_code { get; set; }

        public string sims_sip_goal_target_user_code { get; set; }

        public string sims_goal_kpi_academic_year { get; set; }

        public string sims_kpi_goal_code { get; set; }

        public string sims_kpi_goal_target_code { get; set; }

        public string sims_sip_user_code { get; set; }

        public string old_sims_sip_user_code { get; set; }

        public string em_login_code { get; set; }

        public string em_first_name { get; set; }
    }

    
    public class invs038
    {
        public string opr { get; set; }
        public int im_inv_no { get; set; }
        public string im_desc { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public int id_cur_qty { get; set; }
        public int id_do_qty { get; set; }
        public int id_alloc_qty { get; set; }
        public int id_yob_qty { get; set; }
        public int id_foc_qty { get; set; }

        public string il_bin_loc1 { get; set; }
        public string il_bin_loc2 { get; set; }

        public string im_item_code { get; set; }

        public string loc_name { get; set; }
        public string loc_code { get; set; }

        public string category_name { get; set; }
        public string category_code { get; set; }

        public string subcategory_name { get; set; }
        public string subcategory_code { get; set; }
    }
   

    public class Per234
    {

        public string desg_company_code { get; set; }
        public string desg_code { get; set; }
        public string desg_desc { get; set; }
        public string dis_order { get; set; }
        public bool desg_communication_status { get; set; }
        public string desg_company_code_name { get; set; }
       // public string mograsys{get;set}
        public string desg_arab { get; set; }
        public object opr { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string selValue { get; set; }
    }

    #region Pers109 Grade Change

    public class Pers109
    {
        public string pd_company_code { get; set; }
        public string company_name { get; set; }


        public string pd_grade_code { get; set; }
        public string pd_grade_code_nm { get; set; }

        public string pd_pay_code { get; set; }
        public string pd_pay_code_nm { get; set; }
        public string pd_formula_code { get; set; }
        public string pd_formula_nm { get; set; }

        public string pd_credit_acno { get; set; }
        public string pd_credit_acno_nm { get; set; }

        public string pd_debit_acno { get; set; }
        public string pd_debit_acno_nm { get; set; }

        public string pd_cash_acno { get; set; }
        public string pd_cash_acno_nm { get; set; }

        public string pd_bank_acno { get; set; }
        public string pd_bank_acno_nm { get; set; }

        public bool pd_percent_amount_tag { get; set; }
        public bool pd_daily_hourly_tag { get; set; }
        public string pd_amount { get; set; }

        public string acc_code { get; set; }
        public string acc_name { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string paysEarnDed { get; set; }
        public string gc_number { get; set; }
        public string gc_company_code { get; set; }
        public string gc_company_desc { get; set; }
        public bool gc_mgr_confirm_tag { get; set; }
        public string gc_old_grade { get; set; }
        public string gc_new_grade { get; set; }
        public string gc_effect_from { get; set; }
        public string gc_retro_effect_from { get; set; }
        public string gc_emp_name { get; set; }
        public string gr_desc { get; set; }
        public string gr_code { get; set; }
        public string gc_old_grade_desc { get; set; }
        public string gc_new_grade_desc { get; set; }
        public string opr { get; set; }

        public string pc_company_code { get; set; }
    }

    #endregion

    #region Vacancy Master...

    public class Pers319
    {
        public string pays_sr_no { set; get; }
        public string pays_vacancy_id { set; get; }

        public string pays_vacancy_roles { set; get; }

        public string pays_vacancy_min_experience { set; get; }
        public string pays_vacancy_posting_date { set; get; }
        public string pays_vacancy_expiry_date { set; get; }
        public string pays_vacancy_doc { set; get; }
        public string pays_vacancy_desc { set; get; }


        public string pays_vacancy_profile_desg_code { set; get; }
        public string pays_vacancy_profile_desg_name { set; get; }

        public string pays_vacancy_company_code { set; get; }
        public string pays_vacancy_company_nm { set; get; }

        public string pays_vacancy_dept_code { set; get; }
        public string pays_vacancy_dept_nm { set; get; }

        public string pays_vacancy_type { set; get; }
        public string pays_vacancy_type_nm { set; get; }

        public string pays_vacancy_level { set; get; }
        public string pays_vacancy_level_nm { set; get; }

        public bool pays_vacancy_status { set; get; }
        public string pays_vacancy_profile_desg_nm { set; get; }

        public string opr { set; get; }

    }

    #endregion

    public class Per299
    {
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string dest_code { get; set; }
        public string dest_name { get; set; }
        public object opr { get; set; }
    }

    public class Per258
    {
        public string emp_name_id { get; set; }
        public string emp_qual_institute { get; set; }
        public string emp_qual_type_code { get; set; }
        public string em_qual_from_year { get; set; }
        public string emp_qual_percentage { get; set; }

        public object opr { get; set; }

        public string emp_qual_company_code { get; set; }

        public string emp_qual_company_name { get; set; }

        public string emp_qual_dept_code { get; set; }

        public string emp_qual_dept_name { get; set; }

        public string emp_qual_qual_code { get; set; }

        public string emp_qual_qual_name { get; set; }

        public string emp_qual_em_name { get; set; }

        public string emp_qual_em_code { get; set; }

        public string em_qual_year { get; set; }

        public bool em_qual_status { get; set; }

        public string emp_qual_remark { get; set; }
        public string emp_qual_type_name { get; set; }
        public string em_qual_from_to_year { get; set; }
        public string em_qual_to_year { get; set; }
        public string em_qual_from_month { get; set; }
        public string em_qual_to_month { get; set; }

        public string  pays_qualification_code { get; set; }

        public string pays_qualification_desc { get; set; }
        public string pays_qualification_desc_arabic { get; set; }
        public bool pays_qualificaiton_status { get; set; }
        public string pays_qualification_applicant_id { get; set; }

        public string pays_qualificaion_display_oder { get; set; }
    }
    public class quali_list
    {
        public string quali_list1 { get; set; }
    }

    public class Pers099_qual
    {
        public List<Pers099_qual> educational_details;
        public string vacancy_id { get; set; }
        public string application_id { get; set; }
        public string pays_qualification_level_code { get; set; }
        public string pays_qualification_emp_id { get; set; }
        public string pays_qualification_level { get; set; }
        public string pays_qualification_code { get; set; }
        public string pays_qualification { get; set; }
        public string pays_qualification_year { get; set; }
        public string pays_qualification_remark { get; set; }
        public bool pays_qualification_emp_status { get; set; }
    }

    public class employeepayslip
    {
        public string year { get; set; }
        public string monthname { get; set; }
        public string month { get; set; }
        public string empid { get; set; }
        public string empname { get; set; }
        public string empemailid { get; set; }
        public string empmobile { get; set; }
        public string empdocname { get; set; }
    }

    public class MMSR20
    {
        public string sims_mom_type_code { get; set; }
        public string sims_mom_type_desc_en { get; set; }
        public string sims_mom_reference_number { get; set; }
        public string sims_mom_subject { get; set; }
        public string sims_mom_date { get; set; }
        public string requester { get; set; }
        public string chair_person { get; set; }
        public int no_of_attendees { get; set; }
        public string sims_mom_status { get; set; }
        public string status { get; set; }
        public string status_desc { get; set; }

    }

    public class AR_R20
    {
        public string sims_attendance_rule_code { get; set; }
        public string sims_attendance_rule_name { get; set; }
        public string sims_enrollment_number { get; set; }
        public string student_name { get; set; }
        public string class_name { get; set; }
        public string start_occ_date { get; set; }
        public string end_occ_date { get; set; }
        public int no_of_occ { get; set; }
        public string sims_mom_status { get; set; }
        public string status { get; set; }
        public string status_desc { get; set; }

    }





    public class EPSR01
    {
      
       
        public string earn { get; set; }
        public string ded { get; set; }
        public string net_amount { get; set; }
        public string pe_bank_cash_tag { get; set; }
        public string pe_onhold_flag { get; set; }
         public string sd_number { get; set; }
        public string sd_year_month_code { get; set; }

        public string sd_year_month_name { get; set; }
        public string  fin_year { get; set; }
        public string fin_year_desc { get; set; }

        public string emp_name { get; set; }
        public string em_login_code { get; set; }
        public string codp_dept_name { get; set; }
        public string dg_desc { get; set; }
        public string emp_number { get; set; }
        public Boolean holdstatus { get; set; }






    }



    public class payslip_byuser
    {
        public string paysheet_year { get; set; }
        public string paysheet_year_desc { get; set; }
        public string paysheet_month { get; set; }
        public string pe_number { get; set; }
        public string emp_name { get; set; }
        public string sd_amount { get; set; }
        public string paysheet_month_name { get; set; }
        public string pe_year_month { get; set; }
        public string pays_appl_form_field_value1 { get; set; }
    }
    public class EHM010
    {
        public string opr { get; set; }
        public string pays_hobbies_code { get; set; }
        public string pays_hobbies_desc { get; set; }
        public string pays_hobbies_display_oder { get; set; }
        public bool pays_hobbies_status { get; set; }

    }

}