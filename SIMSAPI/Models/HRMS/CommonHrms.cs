﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.HRMS
{
    public class CommonHrms
    {

        //Distribution of staff By designation And gender
        public string em_login_code { get; set; }
        public string em_desg_code { get; set; }
        public string dg_desc { get; set; }
        public string em_nation_code { get; set; }
        public string gender { get; set; }
        public string em_staff_type { get; set; }
        public string staff_type { get; set; }

        public string em_country_code { get; set; }
       
        public string sims_country_name_en { get; set; }
        public string sims_continent { get; set; }
        public string europian{ get; set; }
        public string asian{ get; set; }
        public string african{ get; set; }
        public string uAE{ get; set; }
        public string american{ get; set; }
        public string astralia{ get; set; }
        public string other_arab{ get; set; }
        public string other_gcc{ get; set; }
        public string total{ get; set; }


        //immigration Document
        	public string comn_appl_parameter{ get; set; }
            public string comn_appl_form_field_value1 { get; set; }
            public string seq { get; set; }
            public string codp_dept_no{ get; set; }
            public string codp_dept_name { get; set; }
            public string em_name { get; set; }
            public string col { get; set; }
            public string em_issue_date { get; set; }
            public string em_expiry_date { get; set; }
            public string em_visa_type { get; set; }
        	 	
        //employee list
        public string emp_name { get; set; }
        public string dept_name { get; set; }
        public string designation_name { get; set; }
        public string em_name_ot { get; set; }
        public string service_status { get; set; }
        public string emp_staff_type { get; set; }
        public string dg_code { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string em_dept_code { get; set; }
        //distribution by nationality and designation
        public string sims_nationality_name_en { get; set; }
        public string em_sex { get; set; }
        public string afghan { get; set; }
        public string albanian { get; set; }
        public string algerian { get; set; }
       
        public string armenian { get; set; }
        public string aruban { get; set; }
        public string australian { get; set; }
        public string austrian { get; set; }
        public string azeri { get; set; }
        public string bahamian { get; set; }
        public string bahraini { get; set; }
        public string bangladeshi { get; set;}
        public string barbadian { get; set; }
        public string british { get; set; }
        public string egyptian { get; set; }
        public string filipino { get; set; }
        public string icelander { get; set; }
        public string indian { get; set; }
        public string nepalese { get; set; }
        public string pakistani { get; set; }
        public string sriLankan { get; set; }
        public string syrian { get; set; }

        //strength report
        public string description1{ get; set; }
        public string count1 { get; set; }
        public string att_date{ get; set; }
        public string att_punch_flag { get; set; }
        public string em_company_code { get; set; }
        //Probation Report For Employee
        public string em_date_of_join { get; set; }
        public string probation_date { get; set; }
        //increment due report
        public string pai_number{ get; set; }
        public string ename { get; set; }
        public string department{ get; set; }
        public string cp_desc { get; set; }
        public string increment_type { get; set; }
        public string increment { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
         public string gr_code { get; set; }
         public string gr_desc { get; set; }
        //salary revision report
         public string pa_effective_from { get; set; }
         public string last_sal { get; set; }
         public string curr_sal { get; set; }
         public string paysheet_month { get; set; }
         public string paysheet_month_name { get; set; }
        //role wise user list
          public string Role_Name { get; set; }
         public string comn_user_name { get; set; }
         public string NAME { get; set; }
         public string designation { get; set; }
         public string comn_role_code { get; set; }
         public string comn_role_name { get; set; }




         public string em_city { get; set; }
         public string sims_city_name_en { get; set; }
         public string region { get; set; }
         public string pays_qualification_code { get; set; }
         public string pays_qualification_desc { get; set; }



         //emp leave summery
         public string lt_number { get; set; }
         public string cl_code { get; set; }
         public string lt_leave_code { get; set; }
         public string cl_desc { get; set; }
         public string lt_start_date { get; set; }
         public string lt_end_date { get; set; }
         public string el_maximum_days { get; set; }
         public string lt_days { get; set; }
         public string lt_balance { get; set; }
         public string rnk { get; set; }
         public string ballance_leave { get; set; }
         public string el_maximum_days1 { get; set; }
         //emp attendance summary
         public string att_absent_flag { get; set; }
         public string att_desc { get; set; }
         public string pays_attendance_code { get; set; }
         public string pays_attendance_description { get; set; }
         //employee doc type
         public string pays_doc_desc { get; set; }
         public string pays_doc_issue_date { get; set; }
         public string pays_doc_expiry_date { get; set; }
         public string pays_doc_code { get; set; }
       
         //MOE attendance Report
         public string total_working_days { get; set; }
         public string Absent_count { get; set; }
         public string present_count { get; set; }
        //supervisor report
         public string sims_grade_name_en { get; set; }
         public string sims_section_name_en { get; set; }
         public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
         public string em_last_name { get; set; }


        //employee experience report query
         public string em_emp_full_name { get; set; }
         public string company_name { get; set; }
         public string total_experience { get; set; }
         public string total_year { get; set; }
         public object comp { get; set; }
         public object dept { get; set; }
         public object desg { get; set; }













































         public string emp_count { get; set; }

         public string code { get; set; }

         public string emp_desc { get; set; }
    }
}
      								
       
        	
      
        	
        	 	
        	 	
        	 	
        



        	 	


        	
	
	
	


        


        


	
	
	
	
	
	
	
	
	
	
	


