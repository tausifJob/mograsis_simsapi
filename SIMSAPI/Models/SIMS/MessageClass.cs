﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.messageClass
{
    public class MessageClass
    {

    }

    public enum MessageType { Error, Success, Warning };
    public class Message
    {
        public MessageType messageType;
        public string strMessage { get; set; }
        public string systemMessage { get; set; }
    }
}