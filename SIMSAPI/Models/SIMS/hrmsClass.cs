﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SIMSAPI.Models.ERP.hrmsClass
{
    public class hrmsClass
    {
        public string ca_company_code_name { get; set; }
        public string dg_company_code { get; set; }
        public string eg_company_code { get; set; }
        public string sh_company_code { get; set; }
        public string sh_company_name { get; set; }

        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
        public string sh_day_desc { get; set; }
        public string sh_day_code { get; set; }
        public bool sh_day_status { get; set; }

        public bool sh_template_status { get; set; }
        public string sh_template_name { get; set; }
        public string sh_template_id { get; set; }
        public string sh_shift_day1 { get; set; }
        public string sh_shift_day2 { get; set; }
        public string sh_shift_day3 { get; set; }
        public string sh_shift_day4 { get; set; }
        public string sh_shift_day5 { get; set; }
        public string sh_shift_day6 { get; set; }
        public string sh_shift_day7 { get; set; }
        public DateTime? sh_start_date { get; set; }
        public string em_number { get; set; }
        public string em_desg_desc { get; set; }
        public string em_desg_code { get; set; }
        public string em_dept_code { get; set; }
        public string em_company_code { get; set; }
        public string em_img { get; set; }
        public string sh_emp_id { get; set; }
        public string sh_emp_name { get; set; }
        public bool sh_shift_status { get; set; }
    }

    #region Pers092 pays_shift_master
    public class Pers092
    {
        public string sh_company_code { get; set; }
        public string sh_company_name { get; set; }
        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
    }
    #endregion

    #region Pers046
    public class Pers046
    {
        public string ds_code { get; set; }
        public string ds_name { get; set; }
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string pays_appl_parameter { get; set; }
        public string pays_appl_form_value1 { get; set; }


        public string af_dest_code { get; set; }
        public string af_nation_code { get; set; }
        public string af_class { get; set; }
        public string af_adult_rate { get; set; }
        public string af_child_rate { get; set; }
        public string af_infant_rate { get; set; }


    }
    #endregion

    #region(Pers061)
    public class Pers061OtherLoans
    {
        public string Company_code { get; set; }
        public string Company_Name { get; set; }
        public string Loancode { get; set; }
        public string LoanDesc { get; set; }
        public string LoanNumber { get; set; }
        public string EmployeeNameNo { get; set; }
        public string EmployeeName { get; set; }
        public string LedgerName { get; set; }
        public string LedgerCode { get; set; }
        public string AccountNumber { get; set; }
    }
    

    public class Pers130_LoanAmt
    {
        public string Company_Code { get; set; }
        public string Company_Name { get; set; }
        public string Loan_Code { get; set; }
        public string Loan_Desc { get; set; }
        public string Company_Grade_Code { get; set; }
        public string Company_Grade_Desc { get; set; }
        public string Total_Installment { get; set; }
        public string Loan_Ammount { get; set; }
        public string Ledger_Code { get; set; }
        public string Ledger_Name { get; set; }
        public string Ledger_Name_Code { get; set; }
        public string Debit_Acc_No { get; set; }
        public string Debit_Acc_Name { get; set; }
        public string Loan_Acc_Type { get; set; }
        public string Company_Emp_Tag { get; set; }
    }

    public class Pers134LoanRegister
    {
        public string LoanNumber { get; set; }
        public string Company_code { get; set; }
        public string Company_Name { get; set; }
        public string Loancode { get; set; }
        public string LoanDesc { get; set; }
        public string Sanction_date { get; set; }
        public string LoanAmmount { get; set; }
        public string Paid_amount { get; set; }
        public string Fully_rec_tag { get; set; }
        public string Monthly_rec_tag { get; set; }
        public string Total_installments { get; set; }
        public string Repaid_installments { get; set; }
        public string ManagerConfirm_tag { get; set; }
        public string Reference { get; set; }
        public string Gaunt_no { get; set; }
        public string EmployeeNameNo { get; set; }
        public string Gaunt_name { get; set; }
        public string Gaunt_comp_name { get; set; }
        public string Gaunt_desg { get; set; }
        public string Gaunt_address1 { get; set; }
        public string Gaunt_address2 { get; set; }
        public string Gaunt_address3 { get; set; }
    }

    public class Pers130_LoanCode
    {
        public string Loan_Desc { get; set; }
        public string Company_Code { get; set; }
        public string Company_Name { get; set; }
        public string Loan_Code { get; set; }
        public string Ledger_Code { get; set; }
        public string Ledger_Name { get; set; }
        public string Ledger_Name_Code { get; set; }
        public string Debit_Acc_No { get; set; }
        public string Debit_Acc_Name { get; set; }
        public string Loan_Acc_Type { get; set; }
        public bool isS { get; set; }
        public bool isO { get; set; }
    }
    #endregion
    #region Pers306
    public class Pers306
    {
        public string em_number { get; set; }
        public string em_login_code { get; set; }
        public string sd_year_month { get; set; }
        public string em_full_name { get; set; }
        public string em_date_of_join { get; set; }
        public string dg_desc { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string reg_date { get; set; }
        public string reg_resion { get; set; }
        public string lo_amount { get; set; }
        public string lo_paid_amount { get; set; }
    }
    #endregion
  
    public class Pers243
    {
        public string cp_code { get; set; }
        public string cp_desc { get; set; }
        public string cp_change_desc { get; set; }
    }

  public class Pers021
    {
        public object opr { get; set; }
        public string eg_grade_code { get; set; }
        public string eg_grade_desc { get; set; }
        public string eg_min_basic { get; set; }
        public string eg_max_basic { get; set; }

        public string eg_company_code { get; set; }
        public string comp_code { get; set; }
        public string comp_short_name { get; set; }
    }
   
    public class Pers254
    {
        public object opr { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string comp_short_name { get; set; }
        public string ca_company_code { get; set; }
        public string ca_code { get; set; }
        public string ca_desc { get; set; }
        public bool ca_process_tag { get; set; }
    }

    public class Pers061
    {
        public string AccountNo { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string comp_short_name { get; set; }
        public string EMPNAME { get; set; }
        public string em_bank_ac_no { get; set; }
        public string em_name_ot { get; set; }
        public string em_number { get; set; }
        public string fullname { get; set; }
        public string lc_code { get; set; }
        public string lc_debit_acno_nm { get; set; }
        public string lc_desc { get; set; }
        public string LedgerCode { get; set; }
        public string LedgerName { get; set; }
        public string LoanCode { get; set; }
        public string LoanName { get; set; }
        public string LoanNumber { get; set; }
        public string ol_account_number { get; set; }
        public object opr { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
    }

    public class Pers090
    {
        public object opr { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string comp_short_name { get; set; }
        public string fm_code { get; set; }
        public string fm_desc { get; set; }
        public string se_company_code { get; set; }
        public string se_code { get; set; }
        public string se_formula_code { get; set; }
        public bool se_leave_tag { get; set; }
    }

    public class Pers255
    {
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string em_Dept_Code { get; set; }
        public string em_Dept_Company_Code { get; set; }
        public string em_Dept_name { get; set; }
        public string em_Dept_Short_Name { get; set; }
        public string em_first_name { get; set; }
        public string em_full_name { get; set; }
        public string em_Grade_Code { get; set; }
        public string em_Grade_Company_Code { get; set; }
        public string em_Grade_name { get; set; }
        public string em_img { get; set; }
        public string em_last_name { get; set; }
        public string em_number { get; set; }
        public string em_Staff_Type_Code { get; set; }
        public string em_Staff_Type_name { get; set; }
        public string gr_code { get; set; }
        public string gr_desc { get; set; }
        public object opr { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string pays_service_status_code { get; set; }
        public string pays_service_status_name { get; set; }
    }

    public class Pers312
    {
       
        public string em_email { get; set; }
        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_login_code { get; set; }
        public string em_mobile { get; set; }
        public string em_number { get; set; }
        public string em_status { get; set; }
        public string fullname { get; set; }
        public object opr { get; set; }
        public string pays_doc_code { get; set; }
        public string pays_doc_desc { get; set; }
        public string pays_doc_empl_id { get; set; }
        public string pays_doc_expiry_date { get; set; }
        public string pays_doc_issue_date { get; set; }
        public string pays_doc_mod_code { get; set; }
        public string pays_doc_path { get; set; }
        public string pays_doc_srno { get; set; }
        public bool pays_doc_status { get; set; }
    }



    public class EQR001
    {

        public string pays_qualification_emp_id { get; set; }
        public string ename { get; set; }
        public string em_company_code { get; set;}
        public string co_desc { get; set; }
        public string em_dept_code { get; set; }
        public string codp_dept_name { get; set; }
        public string pays_qualification_desc { get; set; }
        public string pays_qualification_code { get; set; }
        public string pays_qualification_institude_name { get; set; }
        public string pays_qualification_type { get; set; }
        public string qualification_type { get; set; }
        public string pays_qualification_year_start_year { get; set; }
        public string pays_qualification_year { get; set; }
        public string from_to_year { get; set; }
        public string pays_qualification_percentage { get; set; }
        public string pays_qualification_remark { get; set; }
        public string pays_qualification_emp_status { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_status { get; set; }
        public string codp_dept_no { get; set; }
        
    }
}