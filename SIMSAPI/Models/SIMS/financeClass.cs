﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.financeClass
{
    public class financeClass
    {
    }


 
    public class Fin116
    {
        public string codegroupdes { get; set; }
        public string glsccodename { get; set; }
        public string glsc_sched_code { get; set; }
        public string glsc_sched_name { get; set; }
        public string glsg_group_code { get; set; }
        public string glsg_group_description { get; set; }
        public string gsgd_comp_code { get;  set; }
        public string gsgd_group_code { get;  set; }
        public string gsgd_schedule_code { get;  set; }
        public bool gsgd_sign { get;  set; }
        public string opr { get; set; }
    }

    public class Fin117
    {
        public string fins_appl_form_field_value1 { get; set; }
        public string fins_appl_parameter { get; set; }
        public string glsg_comp_code { get; set; }
        public string glsg_group_code { get; set; }
        public string glsg_group_description { get; set; }
        public string glsg_group_ind { get; set; }
        public string glsg_group_type { get; set; }
        public string glsg_parent_group { get; set; }
        public string opr { get; set; }
    }

    public class Fin010
    {
        public string opr { get; set; }
        public string codp_comp_code { get; set; }
        public string codp_comp_name { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string codp_dept_type { get; set; }
        public string codp_short_name { get; set; }
        public bool codp_status { get; set; }
        public string codp_year { get; set; }
        public string fins_appl_form_field_value1 { get; set; }
        public string fins_appl_parameter { get; set; }
        public string codp_dep_desc { get; set; }
    }


    public class Fin206
    {
        public string gdua_comp_code { get; set; }

        public string gdua_year { get; set; }

        public string gdua_dept_no { get; set; }

        public string gdua_doc_code { get; set; }

        public string gdua_acct_code { get; set; }

        public string gdua_doc_user_code { get; set; }

        public string comn_user_name { get; set; }

        public bool gdua_status { get; set; }

        public string comn_user_group_code { get; set; }

        public string comp_name { get; set; }

        public string codp_dept_name { get; set; }

        public string gldc_doc_name { get; set; }

        public string glma_acct_name { get; set; }

        public string user_name { get; set; }



        public string comp_code { get; set; }

        public string gdua_dept_name { get; set; }

        public string glma_acct_code { get; set; }

        public string gldc_doc_code { get; set; }

        public object opr { get; set; }
    }

}