﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.ERP.LibraryClass
{
    #region Sims135(Library Transaction)
    public class sims135
    {
        public string sims_library_user_number { get; set; }
        public string sims_library_privilege_name { get; set; }
        public string sims_library_borrow_limit { get; set; }
        public string sims_library_allowed_limit { get; set; }
        public string sims_library_reservation_limit { get; set; }
        public string sims_library_duration { get; set; }
        public string sims_library_promote_flag { get; set; }
        public string sims_library_quota { get; set; }
        public string sims_library_no_issue_after { get; set; }
        public string sims_library_auto_renew_flag { get; set; }
        public string sims_library_grace_days { get; set; }
        public string sims_library_privilege_status { get; set; }
        public string sims_library_reservation_flag { get; set; }
        public string sims_library_reservation_release_limit { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_user_code { get; set; }
        public string sims_library_joining_date { get; set; }
        public string sims_library_expiry_date { get; set; }
        public bool sims_library_status { get; set; }
        public string sims_library_StudentName { get; set; }
        public string sims_library_TeacherName { get; set; }
        public string sims_library_LibraryGuestName { get; set; }



        public string sims_library_user_group_type { get; set; }
        public string comn_user_group_name { get; set; }
        public string sims_user_name { get; set; }
        public string sims_library_transaction_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_qty { get; set; }
        public string sims_library_item_issue_date { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_actual_return_date { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_category_name { get; set; }
        public string sims_library_item_subcategory { get; set; }
        public string sims_library_subcategory_name { get; set; }
        public string sims_library_item_catalogue_code { get; set; }
        public string sims_library_catalogue_name { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_transaction_status { get; set; }


        public string sims_library_user_photo { get; set; }

        public string sims_library_class { get; set; }
    }
    #endregion
    #region Sims121(Library Item Master)
    public class Sims121
    {
        public string sims_library_year_of_publication { get; set; }
        public string sims_library_item_status_update_date { get; set; }
        public string opr{ get; set; }
        public string insert_mode { get; set; }
        public int sims_library_item_qty{ get; set; }
        public string accno{ get; set; }
        public string title{ get; set; }
        public string sims_library_item_entry_date { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_bnn_number { get; set; }
        public string sims_library_item_dewey_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_desc { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_category_name { get; set; }
        public string sims_library_item_subcategory { get; set; }
        public string sims_library_item_subcategory_name { get; set; }
        public string sims_library_item_catalogue_code { get; set; }
        public string sims_library_item_catalogue_name { get; set; }
        public string sims_library_item_cur_code { get; set; }
        public string sims_library_item_cur_name { get; set; }
        public string sims_library_item_cur_level_code { get; set; }
        public string sims_library_item_cur_level_name { get; set; }
        public string sims_library_item_grade_code { get; set; }
        public string sims_library_item_grade_name { get; set; }
        public string sims_library_item_language_code { get; set; }
        public string sims_library_item_language_name { get; set; }
        public string sims_library_item_remark { get; set; }
        public string sims_library_item_author1 { get; set; }
        public string sims_library_item_author2 { get; set; }
        public string sims_library_item_author3 { get; set; }
        public string sims_library_item_author4 { get; set; }
        public string sims_library_item_author5 { get; set; }
        public string sims_library_item_age_from { get; set; }
        public string sims_library_item_age_to { get; set; }
        public string sims_library_item_edition_statement { get; set; }
        public string sims_library_item_place_of_publication { get; set; }
        public string sims_library_item_name_of_publisher { get; set; }
        public string sims_library_item_date_of_publication { get; set; }
        //public DateTime? sims_library_item_date_of_publication { get; set; }

        public string sims_library_item_extent_of_item { get; set; }
        public string sims_library_item_accompanying_item { get; set; }
        public string sims_library_item_printing_date { get; set; }
        //public DateTime? sims_library_item_printing_date { get; set; }

        public string sims_library_item_qr_code { get; set; }
        public string sims_library_item_bar_code { get; set; }
        public string sims_library_item_storage_hint { get; set; }
        public string sims_library_item_purchase_price { get; set; }
        public string sims_library_item_keywords { get; set; }
        public string sims_library_item_number_of_copies { get; set; }
        public string sims_library_item_known_as { get; set; }
        public string sims_library_item_book_style { get; set; }
        public string sims_library_item_book_style_name { get; set; }
        public string sims_library_item_status_code { get; set; }
        public string sims_library_item_status_name { get; set; }
        public string available_copies { get; set; }
        public string sims_library_item_accession_number_to { get; set; }
        public string sims_library_item_curcy_code { get; set; }
        public string sims_library_item_curcy_name { get; set; }
        public int no { get; set; }
        public bool isSelected { get; set; }
        public string sims_library_item_location_code { get; set; }
        public string sims_library_item_bin_code { get; set; }
        public string sims_library_item_location_nm { get; set; }
        public string sims_library_item_bin_nm { get; set; }

        public string sims_library_item_number_qty { get; set; }
        public double sims_library_item_amount { get; set; }
        public string sims_library_item_source_hint { get; set; }

        public bool items_check_status { get; set; }

        public string sims_library_request_number { get; set; }

        public string sims_library_user_code { get; set; }

        public string sims_library_request_date { get; set; }

        public string sims_library_request_type { get; set; }

        public string sims_library_request_remarks { get; set; }

        public string sims_library_request_status { get; set; }

        public string sims_library_request_typename { get; set; }

        public string sims_library_item_barcode { get; set; }

        public string sims_library_attribute_code { get; set; }

        public string sims_library_attribute_name { get; set; }

        public string sims_library_attribute_value { get; set; }

        public string sims_appl_form_field_value1{ get; set; }
        public string sims_appl_form_field_value2 { get; set; }
        public string sims_appl_parameter { get; set; }

        public string sims_library_item_Currency_purchase_price { get; set; }
        public string sims_library_item_supplier { get; set; }

        public bool sims_library_category_item_issue { get; set; }

        public string sims_library_item_request_status_code { get; set; }

        public string sims_library_item_request_status_name { get; set; }

        public string sims_library_item_approved_status_code { get; set; }

        public string sims_library_item_approved_status_name { get; set; }

        public string sims_library_item_quantity { get; set; }

        public string sims_library_item_request_date { get; set; }

        public string sims_library_item_requested_by { get; set; }

        public string sims_library_item_status_approve_date { get; set; }

        public string sims_library_item_approved_by { get; set; }

        public string sims_library_item_approved_status { get; set; }
        public bool insertedFlag { get; set; }

        public string sims_library_item_call_number { get; set; }
    }
    #endregion


    #region Sims136(Library Transaction Detail)
    public class sims136Detail
    {
        public string sims_library_transaction_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_transaction_type { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_transaction_date { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_user_number { get; set; }
        public string sims_library_transaction_remarks { get; set; }
        public string sims_library_transaction_total { get; set; }
        public string sims_library_transaction_created_by { get; set; }
        public bool isNew { get; set; }
        public bool isReturn { get; set; }
        public bool isReIssue { get; set; }

        public string sims_library_bnn_number { get; set; }

        public string sims_library_item_dewey_number { get; set; }

        public string sims_library_item_desc { get; set; }

        public string sims_library_item_issue_date { get; set; }

        public string sims_library_item_actual_return_date { get; set; }

        public string sims_library_user_name { get; set; }

        public string sims_library_item_status { get; set; }

        public string sims_library_transaction_user_code { get; set; }
        public string sims_library_transaction_status { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string studentname { get; set; }
        public string sims_library_item_transaction_status { get; set; }
        public string user_detail { get; set; }

        public object sims_library_item_bar_code { get; set; }
    }
    public class Sims136
    {
        public int sims_library_transaction_number { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_qty { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_reserve_release_date { get; set; }
        public decimal sims_library_item_rate { get; set; }
        public decimal sims_library_item_line_total { get; set; }
        public bool sims_library_item_transaction_status { get; set; }


        //PP028
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_author1 { get; set; }

        public string sims_library_item_actual_return_date { get; set; }
    }
    #endregion



    public class Sim115
    {
        public string opr { get;  set; }
        public string sims_library_attribute_code { get;  set; }
        public string sims_library_attribute_name { get;  set; }
        public Boolean sims_library_attribute_status { get;  set; }
        public string sims_library_transaction_user_code { get; set; }
        public string cur_code { get; set; }
        public string academic_year { get; set; }
        public string grade { get; set; }
        public string section { get; set; }
        public string name { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_issue_date { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_library_user_group_type { get; set; }
        public string duefor { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_expected_return_date { get; set; }
        public string sims_employee_code { get; set; }

    }
    
    public class Sim201
    {
        public string TypeName { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public bool sims_device_status { get; set; }
        public string sims_device_type { get; set; }
        public string sims_device_value { get; set; }
        public string sims_enroll_number { get; set; }
        public string opr { get; set; }
        public string enroll_number { get; set; }
    }

    public class Sim123
    {
        public string opr { get; set; }
        public string sims_library_location_address1 { get; set; }
        public string sims_library_location_address2 { get; set; }
        public string sims_library_location_code { get; set; }
        public string sims_library_location_contact_person { get; set; }
        public string sims_library_location_name { get; set; }
        public string sims_library_location_phone { get; set; }
    }


    public class Sim117
    {
        public string att_code { get;  set; }
        public string cat_code { get;  set; }
        public string opr { get;  set; }
        public string sims_library_attribute_code { get;  set; }
        public string sims_library_attribute_code_old { get;  set; }
        public string sims_library_attribute_name { get;  set; }
        public string sims_library_catalogue_code { get;  set; }
        public string sims_library_catalogue_code_old { get;  set; }
        public string sims_library_catalogue_name { get;  set; }
    }


    public class Sim119
    {
        public string opr { get;  set; }
        public string sims_library_catalogue_code { get;  set; }
        public string sims_library_catalogue_name { get;  set; }
        public bool sims_library_catalogue_status { get;  set; }
    }

    public class Sim126
    {
        public string opr { get; set; }
        public string sims_library_membership_type_code { get; set; }
        public string sims_library_membership_type_name { get; set; }
        public bool sims_library_membership_type_status { get; set; }
    }


    public class Sim127
    {
        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string opr { get; set; }
        public string sims_library_allowed_limit { get; set; }
        public bool sims_library_auto_renew_flag { get; set; }
        public string sims_library_borrow_limit { get; set; }
        public string sims_library_duration { get; set; }
        public string sims_library_grace_days { get; set; }
        public string sims_library_membership_type { get; set; }
        public string sims_library_membership_type1 { get; set; }
        public string sims_library_membership_type_code { get; set; }
        public string sims_library_membership_type_name { get; set; }
        public string sims_library_no_issue_after { get; set; }
        public string sims_library_privilege_name { get; set; }
        public string sims_library_privilege_number { get; set; }
        public bool sims_library_privilege_status { get; set; }
        public bool sims_library_promote_flag { get; set; }
        public string sims_library_quota { get; set; }
        public bool sims_library_reservation_flag { get; set; }
        public string sims_library_reservation_limit { get; set; }
        public string sims_library_reservation_release_limit { get; set; }
        public string sims_library_user_group_code { get; set; }
        public string sims_library_user_group_code1 { get; set; }

        public string sims_library_issue_limit_days { get; set; }
        public string sims_library_no_of_times_issue_limit { get; set; }
        public string sims_library_locking_period { get; set; }
    }

    public class Sim118
    {
        public string opr { get; set; }
        public string sims_library_category_code { get; set; }
        public string sims_library_category_name { get; set; }

        public bool sims_library_category_item_issue { get; set; }
    }

    public class Sim131
    {
        public string catagoryname { get; set; }
        public string opr { get; set; }
        public string sims_library_category_code { get; set; }
        public string sims_library_category_name { get; set; }
        public string sims_library_subcategory_code { get; set; }
        public string sims_library_subcategory_name { get; set; }
    }

    #region Sims116(Library Bin)
    public class Sims116
    {
        public int sims_library_bin_code { get; set; }
        public string sims_library_bin_desc { get; set; }
        public string opr { get; set; }
        public string sims_library_location_code { get;  set; }
        public string sims_library_location_name { get;  set; }
    }
    #endregion

    
    #region Sims122(Library Item Style)
    public class Sims122
    {
        public int sims_library_item_style_code { get; set; }
        public string sims_library_item_style_name { get; set; }
        public string opr { get; set; }

    }
    #endregion

    #region Sims128(Library Request)
    public class Sims128
    {
        public string sims_library_request_number { get; set; }
        public string sims_library_user_code { get; set; }
        public string sims_library_request_date { get; set; }
        public string sims_library_request_type { get; set; }
        public string sims_library_request_typename { get; set; }
        public string sims_library_request_remarks { get; set; }
        public string sims_library_request_status { get; set; }
        public string sims_library_request_statusname { get; set; }
        public string opr { get; set; }


    }
    #endregion

    #region Sims129(Library Request Detail)
    public class Sims129
    {
        public string sims_library_request_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_qty { get; set; }

        public string sims_library_request_line_number { get; set; }
        public string sims_library_item_accession_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_bnn_number { get; set; }
        public string sims_library_item_dewey_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_desc { get; set; }
        public double sims_library_item_amount { get; set; }
        public string sims_library_item_source_hint { get; set; }
        public string sims_library_item_status { get; set; }
        public string sims_library_item_statusname { get; set; }
        public string opr { get; set; }

        public string sims_library_request_date { get; set; }
    }
    #endregion

    #region Sims130(Library Status)
    public class Sims130
    {
        public string sims_library_status_code { get; set; }
        public string sims_library_status_name { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Sims134(Library Subscription Type)
    public class Sims134
    {
        public int sims_library_subscription_code { get; set; }
        public string sims_library_subscription_name { get; set; }
        public bool sims_library_subscription_status { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region Sims137(Library Transaction Type)
    public class Sims137
    {
        public int sims_library_transaction_type_code { get; set; }
        public string sims_library_transaction_type_name { get; set; }
        public string opr { get; set; }
    }
    #endregion

    #region BookBank Transaction
    public class SimsLib
    {
        public string StudentEnroll { get; set; }
        public string StudentName { get; set; }
        public string StudentLibUserNumber { get; set; }
        public int StudentLibBorrowLimit { get; set; }
        public int StudentAllowedLimit { get; set; }
        public int StudentBooksTaken { get; set; }
        public string StudentBookItemNumbers { get; set; }
        public string StudentLibTransactionNo { get; set; }
        public string StudentLibTransactionLineNo { get; set; }
        public string StudentCurr { get; set; }
        public string StudentAcademicYear { get; set; }
        public string StudentGrade { get; set; }
        public string StudentSection { get; set; }
        public string StudentRemark { get; set; }
        public string StudentBookIssueDate { get; set; }
        public string StudentBookExpReturnDate { get; set; }
        public int StudentBalance { get; set; }
        public string StudentTransType { get; set; }
        public string StudentTransTypeDesc { get; set; }
        public bool IsValid { get; set; }
        public bool IsChanged { get; set; }
        public int StudentGraceDays { get; set; }
        public string StudentLibMemStartDate { get; set; }
        public string StudentLibMemExpiryDate { get; set; }
        public string StuddentErrorMesssage { get; set; }
        public string StudentTransStatus { get; set; }
        public string StudentTransStatusDesc { get; set; }
        public string StudentBookQty { get; set; }

        //public string s_curr_code { get; set; }
        //public string search_std_grade_name { get; set; }
        //public string search_std_section_name { get; set; }
        //public string sims_academic_year { get; set; }
        public string bookcount { get; set; }
        public string Txn_no { get; set; }
        public string Item_no { get; set; }
        public string Issue_date { get; set; }
        public string Return_date { get; set; }
        public string Txn_status { get; set; }
        public string Item_name { get; set; }

        public string studentLibraryItem { get; set; }

        public string studentLibraryNumber { get; set; }

        public string studentLibraryDesc { get; set; }
    }

    public class SimsTrans
    {
        public string bks_remark { get; set; }
        public string count { get; set; }
        
        public string StudEnroll { get; set; }
        public string L_User_No { get; set; }
        public string StudCurr { get; set; }
        public string StudAcademicYear { get; set; }
        public string StudGrade { get; set; }
        public string StudSection { get; set; }
        public string TransType { get; set; }
        public string TransTypeDesc { get; set; }
        public string TransStatus { get; set; }
        public string TransNo { get; set; }
        public string TransLineNo { get; set; }
        public string ITEM_NO { get; set; }
        public string ITEM_Desc { get; set; }
        public string ITEM_QTY { get; set; }
        public string ISSUE_DATE { get; set; }
        public string RETURN_DATE { get; set; }
        public string REMARK { get; set; }
        public string CREATED_BY { get; set; }
        public string return_date { get; set; }
        public string RETURN_DATE1 { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_purchase_price { get; set; }
        public string opr { get; set; }
    }

    #endregion

    #region Sims503(Renew Library)
    public class Sims503
    {
        public string cur_code { get; set; }
        public string cur_name { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public List<Sims503> section_lst { get; set; }
        public string renew_date { get; set; }
        public string enroll_no { get; set; }
        public string student_name { get; set; }
        public string student_sex { get; set; }
        public string sims_student_dob { get; set; }
        public int count { get; set; }


        public string sims_library_user_number { get; set; }
        public string sims_library_user_first_name { get; set; }
        public string sims_library_user_middle_name { get; set; }
        public string sims_library_user_last_name { get; set; }
        public string sims_library_user_family_name { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string s_enroll_no { get; set; }
        public string sims_user_code { get; set; }
        public DateTime sims_library_user_birth_date { get; set; }
        public string sims_library_user_group_type { get; set; }
        public string sims_library_membership_type { get; set; }
        public string sims_library_joining_date { get; set; }
        public string sims_library_expiry_date { get; set; }
        public string sims_library_renewed_date { get; set; }
        public string sims_library_user_gender { get; set; }
        public string sims_library_user_address { get; set; }
        public string sims_library_user_mobile_number1 { get; set; }
        public string sims_library_user_mobile_number2 { get; set; }
        public string sims_library_faculty { get; set; }
        public string sims_library_email { get; set; }
        public string sims_library_banned_flag { get; set; }
        public string sims_library_remark { get; set; }
        public string sims_library_barcode_number { get; set; }
        public string sims_library_status { get; set; }
        public string opr { get; set; }

        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        //public string sims_employee_code { get; set; }
        //public string sims_user_code { get; set; }
        public string sims_teacher_type { get; set; }
        public bool sims_status { get; set; }

        public string em_number { get; set; }
        public string emp_name { get; set; }
        //public string sims_employee_code { get; set; }
        public string em_login_code { get; set; }
        public string em_sex { get; set; }
        public bool em_status { get; set; }

        public string s_sname_in_english { get; set; }
        public string s_class { get; set; }
        public string user_name { get; set; }
        public string mobile_no { get; set; }
        public string mail_id { get; set; }
        public string status_name { get; set; }

        public string sims_library_membership_type_code { get; set; }

        public string sims_library_membership_type_name { get; set; }

        public string sims_library_status_updated_on { get; set; }

        public object sims_library_status_updated_by { get; set; }

        public string sims_library_transaction_number { get; set; }
        public string sims_library_transaction_date { get; set; }
        public string sims_library_item_issue_date { get; set; }
        public string sims_teacher_full_name { get; set; }
        public object sims_library_item_expected_return_date { get; set; }
        public string sims_library_item_actual_return_date { get; set; }
       // public bool transaction_status { get; set; }
        public string transaction_status { get; set; }
        public string sims_library_transaction_line_number { get; set; }
        public string sims_library_item_number { get; set; }
        public string sims_library_item_name { get; set; }

        public string student_enroll { get; set; }

        public string sims_grade_name_en { get; set; }
    }
    #endregion

    #region Sims682(Renew Library)
    public class Sims682
    {
        public string sims_library_item_number { get; set; }
        public string sims_library_isbn_number { get; set; }
        public string sims_library_item_title { get; set; }
        public string sims_library_item_desc { get; set; }
        public string sims_library_item_category { get; set; }
        public string sims_library_item_subcategory { get; set; }
        public string sims_library_item_catalogue_code { get; set; }
        public string sims_library_item_author1 { set; get; }
        public string sims_library_item_name_of_publisher { get; set; }
        public string sims_library_item_bin_code { get; set; }
        public string sims_library_item_status_code { get; set; }

        public string sims_library_item_accession_number { get; set; }
        public string sims_library_bnn_number { get; set; }
        public string sims_library_item_dewey_number { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_category_name { get; set; }
        public string sims_library_item_subcategory_name { get; set; }
        public string sims_library_item_catalogue_name { get; set; }
        public string sims_library_item_cur_code { get; set; }
        public string sims_library_item_cur_name { get; set; }
        public string sims_library_item_cur_level_code { get; set; }
        public string sims_library_item_cur_level_name { get; set; }
        public string sims_library_item_grade_name { get; set; }
        public string sims_library_item_language_name { get; set; }
        public string sims_library_item_remark { get; set; }
        public string sims_library_item_date_of_publication { get; set; }
        public string sims_library_item_printing_date { get; set; }
        public string sims_library_item_storage_hint { get; set; }
        public string sims_library_item_purchase_price { get; set; }
        public string sims_library_item_number_of_copies { get; set; }
        public string sims_library_item_book_style_name { get; set; }
        public string sims_library_item_status_name { get; set; }
        public string sims_library_item_curcy_name { get; set; }
        public string sims_library_item_entry_date { get; set; }
        public string sims_library_item_bar_code { get; set; }
    }
    #endregion

    public class demo
    {
        public string opr { get; set; }

        public string id { get; set; }
        public string book_name { get; set; }
        public string book_authors_name { get; set; }
       public bool book_status { get; set; }
      
       public string empId { get; set; }
       public string empName { get; set; }
    }

    #region Sims (Check List Section )
    public class Sims555
    {
        public string opr { get; set; }

        public string sims_checklist_section_number { get; set; }
        public string sims_checklist_section_name { get; set; }

        public string em_login_code { get; set; }
        public string emp_name { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }


        public string sims_checklist_section_mapping_checklist_section_number { get; set; }
        public string sims_checklist_section_mapping_employee_code { get; set; }
        public string sims_checklist_section_mapping_type { get; set; }
        public string sims_checklist_section_mapping_apply_date { get; set; }
        public string sims_checklist_section_mapping_created_by { get; set; }
        public string sims_checklist_section_mapping_frequency { get; set; }
        public string sims_checklist_section_mapping_frequency_count { get; set; }
        public string sims_checklist_section_mapping_status { get; set; }

        public string sims_section_name { get; set; }

        
    }

    #endregion



    public class Sim148
    {
        public string opr { get; set; }

        public string sims_list_code { get; set; }
        public string sims_list_name { get; set; }
        public string sims_list_description { get; set; }
        public bool sims_list_status { get; set; }




    }

    public class Empdemo
    {
        public string opr { get; set; }

        public string empId { get; set; }
        public string empName { get; set; }
    }

    #region Sims ( List Section )
    public class Sims789
    {
        public object sims_checklist_section_response_type { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string opr { get; set; }

        public string id { get; set; }
        public string sims_checklist_section_parent_number { get; set; }
        public string sims_checklist_section_name { get; set; }

        public string sims_checklist_section_name_ot { get; set; }
        public string sims_checklist_section_desc { get; set; }
        public bool sims_checklist_section_status { get; set; }

        public string sims_checklist_section_parent_name { get; set; }
         

        public string sims_checklist_section_number { get; set; }

        public string sims_response_code { get; set; }

        public string sims_response_title { get; set; }

        public bool sims_response_status { get; set; }

        public string sims_response_detail { get; set; }

        public string sims_response_detail_ot { get; set; }

        public bool sims_response_detail_status { get; set; }
    }

    #endregion

    #region Sims ( List Evidences)
    public class Sims624
    {
        public string opr { get; set; }
        public string id { get; set; }
        public string sims_checklist_section_parameter_number { get; set; }
        public string sims_checklist_section_parameter_name { get; set; }

        public string sims_checklist_section_parameter_ot { get; set; }
        public string sims_checklist_section_parent_parameter_order { get; set; }
        public bool sims_checklist_section_status_parameter { get; set; }

        public string sims_checklist_section_number { get; set; }
        public string sims_checklist_section_name { get; set; }  



        public object sims_checklist_section_parent_number { get; set; }
    }
     #endregion
}

#region Sims ( List Review)
    public class Sims754
    {
        public string sims_checklist_section_response_type { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string opr { get; set; }
        public string id { get; set; }
        public string sims_checklist_section_review_number { get; set; }
        public string sims_checklist_section_number { get; set; }
        public string sims_checklist_section_name { get; set; }

        public string sims_checklist_section_parameter_number { get; set; }
        public string sims_checklist_section_parameter_name { get; set; }

          public object sims_checklist_section_parent_number { get; set; }
        public string sims_checklist_section_parent_parameter_order { get; set; }

          public string sims_checklist_section_review_checklist_section_number { get; set; }
          public string sims_checklist_section_review_date { get; set; }
          public string sims_checklist_section_review_by { get; set; }
          public string sims_checklist_section_review_parameter_number { get; set; }
          public string sims_checklist_section_review_parameter_response { get; set; }
          public string sims_checklist_section_review_parameter_action { get; set; }
          public string sims_checklist_section_review_transaction_date { get; set; }
          public string sims_checklist_section_review_approve_date { get; set; }
          public string sims_checklist_section_review_approve_by { get; set; }



          public string sims_response_code { get; set; }

          public string sims_response_title { get; set; }
    }

#endregion

#region Sims (Library Book Set )
    public class Sims693
    {
        public string opr { get; set; }
        public string sims_library_bookset_number { get; set; }

        public string sims_library_bookset_name { get; set; }

        public string sims_library_bookset_book_qty { get; set; }

        public bool sims_library_bookset_status { get; set; }
    }

#endregion

    #region Sims (Library Book Review )
    public class Sims845
    {
        public string approve_status;
        public string display_cmt_flag;
        public string opr { get; set; }


        public string sims_library_item_accession_number { get; set; }

        public string sims_library_item_title { get; set; }

        public string sims_library_item_author1 { get; set; }

        public bool sims_library_item_status_code { get; set; }

        public string sims_library_item_name_of_publisher { get; set; }

        public string id { get; set; }

        public string login_id { get; set; }

        public string rating { get; set; }

        public string comment { get; set; }

        public string create_on_date { get; set; }

        public object accession_number { get; set; }

        public string sims_library_isbn_number { get; set; }

        public string sims_library_category_name { get; set; }

        public string sims_library_subcategory_name { get; set; }

        public string sims_library_item_place_of_publication { get; set; }
    }

    #endregion

    #region Sims (Due Book Detail )
    public class LIBCAT
    {
        public string opr { get; set; }


        public string comn_audit_user_code { get; set; }

        public string comn_user_name { get; set; }

        public string comn_user_full_name { get; set; }

        public string comn_user_device_model { get; set; }

        public string comn_audit_user_appl_code { get; set; }

        public string comn_appl_name_en { get; set; }

        public string comn_audit_start_time { get; set; }

        public string comn_audit_end_time { get; set; }

        public string user_id_code { get; set; }

        public string user_mobile_number { get; set; }

        public string user_email_address { get; set; }

        public string comn_user_device_status { get; set; }

        public string sims_library_item_number { get; set; }

        public string sims_library_item_accession_number { get; set; }

        public string issue_return_status { get; set; }

        public string member_name { get; set; }

        public string transaction_status { get; set; }

        public string sims_library_item_actual_return_date { get; set; }

        public string sims_library_item_expected_return_date { get; set; }

        public string sims_library_transaction_date { get; set; }

        public string sims_library_isbn_number { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_library_bin_name { get; set; }

        public string sims_library_item_location_name { get; set; }

        public string sims_library_item_entry_date { get; set; }

        public string sims_library_item_quantity { get; set; }

        public string sims_library_item_number_of_copies { get; set; }

        public string sims_library_item_purchase_price { get; set; }

        public string sims_library_item_bar_code { get; set; }

        public string sims_library_item_qr_code { get; set; }

        public string sims_library_item_date_of_publication { get; set; }

        public string sims_library_item_name_of_publisher { get; set; }

        public string sims_library_item_remark { get; set; }

        public string sims_library_catelogue_name { get; set; }

        public string sims_library_item_subcategory_name { get; set; }

        public string sims_library_item_category_name { get; set; }

        public string sims_library_item_desc { get; set; }

        public string sims_library_item_title { get; set; }

        public string sims_library_item_name { get; set; }

        public string sims_library_item_dewey_number { get; set; }

        public string sims_library_bnn_number { get; set; }

        public string sims_library_catalogue_code { get; set; }

        public string sims_library_catalogue_name { get; set; }

        public string sims_library_category_code { get; set; }

        public string sims_library_category_name { get; set; }

        public string sims_library_subcategory_code { get; set; }

        public string sims_library_subcategory_name { get; set; }
        public string no_of_views { get; set; }
    }

    #endregion

    #region Sims (Due Book Detail )
    public class EMPSAL
    {
        public string opr { get; set; }


        public string paycode { get; set; }

        public string earn { get; set; }

        public string amount { get; set; }

        public string year_code { get; set; }

        public string year { get; set; }

        public string absents { get; set; }

        public string presents { get; set; }

        public string holidays { get; set; }

        public string worddays { get; set; }

        public string leaves { get; set; }

        public string sd_year_month { get; set; }

        public string month { get; set; }
    }

    #endregion

    #region Sims (Due Book Detail )
    public class INDMO
    {
        public string opr { get; set; }



        public string name { get; set; }

        public string pass { get; set; }
    }

    #endregion

    #region Sims (Library Book Review Approve )
    public class Sims849
    {
        public string id { get; set; }
        public string login_id { get; set; }
        public string rating { get; set; }
        public string comment { get; set; }
        public string accession_number { get; set; }
        public bool approve_status { get; set; }
        public bool display_cmt_flag { get; set; }
        public string sims_library_item_title { get; set; }
        public string opr { get; set; }

    

    }

    #endregion

    #region Sims (Library New Item Arrival )
    public class Sims720
    {
        public string opr { get; set; }
        public string sims_library_item_srl_no { get; set; }
	    public string sims_library_item_transaction_number { get; set; }
	    public string sims_library_item_number { get; set; }
	    public string sims_library_item_accession_number { get; set; }
	    public string sims_library_item_from_date { get; set; }
	    public string sims_library_item_to_date { get; set; }
	    public string sims_library_item_created_by { get; set; }
	    public string sims_library_item_created_date { get; set; }
	    public string sims_library_cur_code { get; set; }
        public string sims_library_cur_name { get; set; }
	    public string sims_library_academic_year { get; set; }
	    public string sims_library_grade_code { get; set; }
        public string sims_library_grade_name { get; set; }
	    public string sims_library_section { get; set; }
        public string sims_library_section_name { get; set; }
	    public string sims_library_item_transaction_date { get; set; }
        public bool sims_library_item_status { get; set; }
        public string sims_library_item_name { get; set; }
        public string sims_library_item_author { get; set; }
        public string sims_library_item_name_of_publisher { get; set; }
        public string transactionstatus { get; set; }
    }

    #endregion


    #region Sims (TelephonicConversationDetails)
    public class Sims664
    {
        
        public string opr { get; set; }


        public string sims_comm_message_remark { get; set; }

        public string sims_comm_message_feedback { get; set; }

        public string sims_comm_user_by { get; set; }

        public string sims_comm_date { get; set; }

        public string sims_comm_user_code { get; set; }

        public string sims_comm_number { get; set; }

        public string sims_follow_up_date { get; set; }

        public string willing_to_register { get; set; }

        public string sims_seat_cofirm_tran_id { get; set; }

        public string sims_academic_year { get; set; }
    }

    #endregion