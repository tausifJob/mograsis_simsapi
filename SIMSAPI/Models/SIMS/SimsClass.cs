﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Text;

namespace SIMSAPI.Models.SIMS.simsClass
{
    public class signaturepad
    {
        public string academic_year { get; set; }
        public string parent_id { get; set; }
        public string student_id { get; set; }
        public string signature_details { get; set; }
        public string signature_img_path { get; set; }
        public string takenby_user_name { get; set; }
    }

    public class TEPE01
    {
        public string opr { get; set; }
        public string sims_health_package_sr_no { get; set; }

        public string sims_health_package_code { get; set; }
        public string sims_health_package_name_en { get; set; }
        public string sims_health_package_name_ot { get; set; }
        public string sims_health_package_display_order { get; set; }
        public string sims_health_package_price { get; set; }
        public bool sims_health_package_status { get; set; }
        public string sims_health_test_created_by { get; set; }
        public bool sims_health_package_chargable { get; set; }
        public string sims_health_package_image { get; set; }
        public string user { get; set; }
        public string sims_health_test_code { get; set; }
        public string sims_health_test_name_en { get; set; }
        public string sims_health_package_created_by { get; set; }







    }


    public class TEST01
    {
        public string opr { get; set; }
        public string sims_health_test_sr_no { get; set; }
        public string sims_health_test_code { get; set; }
        public string sims_health_test_name_en { get; set; }
        public string sims_health_test_name_ot { get; set; }
        public string sims_health_test_display_order { get; set; }
        public string sims_health_test_price { get; set; }
        public bool sims_health_test_status { get; set; }
        public string sims_health_test_created_by { get; set; }
        public string sims_health_parameter_code { get; set; }
        public string sims_health_parameter_name_en { get; set; }
        public string user { get; set; }

    }
    public class BADD01
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_report_card_subject_grade_scale_code { get; set; }
        public string academic_year { get; set; }
        public string phase { get; set; }
        public string class1 { get; set; }
        public string section { get; set; }
        public string enroll_number { get; set; }
        public string student_name { get; set; }
        public string gender { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string sen { get; set; }
        public string sen_type { get; set; }
        public string arabic { get; set; }
        public string emirati { get; set; }
        public string subject_name { get; set; }
        public string scheme_name { get; set; }
        public string frequency { get; set; }
        public string category { get; set; }
        public string exam_name { get; set; }
        public string marks_entry_type { get; set; }
        public string exam_attr_subj_strand { get; set; }
        public float marks { get; set; }
        public string obtain_grade { get; set; }
        public float max_marks { get; set; }
        public string target_grade { get; set; }
        public string sims_gb_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_download_code { get; set; }
        public string sims_download_desc { get; set; }

    }



    public class FCBD01
    {
        public string opr { get; set; }
        public string comp_bank_code { get; set; }
        public string comp_bank_name { get; set; }
        public string comp_bank_branch_name { get; set; }
        public string comp_bank_address { get; set; }
        public string comp_bank_account_no { get; set; }
        public string comp_bank_iban_no { get; set; }
        public string comp_route_code { get; set; }
        public string comp_ifsc_code { get; set; }
        public string comp_swift_code { get; set; }
        public bool comp_bank_status { get; set; }


    }

    public class emirates {

        public string FullName { get; set; }

        public string IDN { get; set; }

        public string CardNumber { get; set; }

        public string Title { get; set; }

        public string Nationality { get; set; }

        public string ExpiryDate { get; set; }

        public string IssueDate { get; set; }

        public string IdType { get; set; }

        public string Sex { get; set; }

        public string FullName_ar { get; set; }

        public string MaritalStatus { get; set; }

        public string Occupation { get; set; }

        public string OccupationField { get; set; }

        public string DoB { get; set; }

        public string Title_ar { get; set; }

        public string Nationality_ar { get; set; }

        public string MotherName { get; set; }

        public string MotherName_ar { get; set; }

        public string FamilyId { get; set; }

        public string HusbandIDN { get; set; }

        public string SponsorType { get; set; }

        public string SponsorName { get; set; }

        public string SponsorUnifiedNumber { get; set; }

        public string ResidencyType { get; set; }

        public string ResidencyNumber { get; set; }

        public string ResidencyExpiryDate { get; set; }

        public string idn_cn { get; set; }

        public string no_mod_data { get; set; }

        public string mod_data { get; set; }

        public string sign_image { get; set; }

        public string photo { get; set; }

        public string root_cert { get; set; }

        public string home_address { get; set; }

        public string work_address { get; set; }

        public string cert_path { get; set; }

        public string Src { get; set; }

        public string Srcphoto { get; set; }

        public string Srcsign { get; set; }

        public string companyname { get; set; }

        public string Degree { get; set; }

        public string FieldofStudy { get; set; }

        public string PassportNumber { get; set; }

        public string PassportCountry { get; set; }

        public string companynamear { get; set; }

        public string companyname_ar { get; set; }

        public string FlatNo { get; set; }

        public string Street { get; set; }

        public string Area { get; set; }

        public string BldgName { get; set; }

        public string City { get; set; }

        public string Email { get; set; }

        public string ResPhone { get; set; }

        public string Mobile { get; set; }

        public string POBox { get; set; }

        public string EmirateDesc { get; set; }

        public string DateofGraduation { get; set; }

        public string placeofbirth { get; set; }

        public string Placeofstudy { get; set; }

        public string QualificationLevel { get; set; }

        public string SponsorUnifiedNo { get; set; }

        public string workaddress { get; set; }

        public string homeaddress { get; set; }

        public string motherfullname { get; set; }

        public string CardIssueDate { get; set; }

        public string CardExpiryDate { get; set; }

        public string OccupationType { get; set; }

        public string SponsorNo { get; set; }

        public string PassportType { get; set; }

        public string PassportIssueDate { get; set; }

        public string PassportExpiryDate { get; set; }

        public string CreationUser { get; set; }

        public string CreationDate { get; set; }

        public string UserName { get; set; }

        public string non_mod_data { get; set; }

        public string DegreeAr { get; set; }

        public string FieldofStudyAr { get; set; }

        public string placeofbirthAr { get; set; }

        public string Placeofstudy_ar { get; set; }

        public string QualificationLevelAr { get; set; }

        public string PassportCountryDesc { get; set; }

        public string PassportCountryAr { get; set; }

        public string OccupationTypeAr { get; set; }

        public string FulllNameAr { get; set; }

        public string motherfullnamear { get; set; }

        public string NatinalityAr { get; set; }

        public string TitleAr { get; set; }

        public string PassportCountryDescAr { get; set; }

        public string placeofbirthar { get; set; }

        public string Placeofstudyar { get; set; }
    }


    public class respoObj
    {

        public string sims_response_code { get; set; }

        public string sims_response_title { get; set; }

        public string sims_response_desc { get; set; }

        public string response_count { get; set; }

        public string sims_response_title_ot { get; set; }

        public string sims_response_desc_ot { get; set; }

        public bool sims_response_status { get; set; }

        public string sims_response_detail_code { get; set; }

        public string sims_response_detail { get; set; }

        public string sims_response_detail_ot { get; set; }

        public bool sims_response_detail_status { get; set; }

        public string responsecode { get; set; }

        public string opr { get; set; }

        public List<response_Details> response_det { get; set; }
    }

    public class response_Details
    {
        public string responsecode { get; set; }

        public string sims_response_detail_code { get; set; }

        public string sims_response_detail { get; set; }

        public string sims_response_detail_ot { get; set; }

        public bool sims_response_detail_status { get; set; }

    }
    public class sims_access_criteria
    {
        public string sims_access_code { get; set; }

        public string sims_accesss_name { get; set; }

        public string sims_access_preference { get; set; }

        public bool sims_access_hierarchy_include { get; set; }

        public bool sims_access_status { get; set; }

        public string sims_access_created_by { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_cur_code { get; set; }

    }
    public class sims_access_criteria_applications
    {
        public bool sims_appl_status { get; set; }

        public bool sims_appl_read_status { get; set; }

        public bool sims_appl_write_status { get; set; }

        public bool sims_appl_config_status { get; set; }

        public string sims_appl_code { get; set; }

        public string sims_appl_name_en { get; set; }

        public string sims_access_code { get; set; }
    }
    public class sims_access_criteria_role_Desg_user
    {
        public string sims_user_name { get; set; }
        public string sims_user_alias { get; set; }

        public string sims_role_deg_code { get; set; }

        public bool sims_status { get; set; }

        public string sims_role_deg_name { get; set; }
    }
    public class sims_access_criteria_role_Desg
    {
        public string sims_role_deg_code { get; set; }

        public string sims_role_deg_name { get; set; }
    }

    public class redbook
    {
        public string new_attendance { get; set; }
        public string new_sims_student_absent_count_with_excuse { get; set; }
        public string new_sims_student_absent_count_without_excuse { get; set; }
        public bool sims_student_attr2 { get; set; }
        public string sims_student_attr1 { get; set; }
        public string sims_column_name { get; set; }
        public string sims_column_name_dummy { get; set; }
        public string sims_moe_grade_code { get; set; }
        public string sims_moe_section_code { get; set; }
        public string sims_student_final_result { get; set; }
        public string sims_student_final_remark { get; set; }
        public string sims_student_absent_count_with_excuse { get; set; }
        public string sims_student_absent_count_without_excuse { get; set; }
        public string sims_enrolled_student_count { get; set; }
        public string sims_enrolled_student_count_local { get; set; }
        public string sims_enrolled_student_count_expat { get; set; }
        public string sims_passed_student_count { get; set; }
        public string sims_passed_student_count_local { get; set; }
        public string sims_passed_student_count_expat { get; set; }
        public string sims_promoted_student_count { get; set; }
        public string sims_retest_in_arabic { get; set; }
        public string sims_retest_in_islamic { get; set; }
        public string sims_retest_in_UAE { get; set; }
        public string sims_detained_student_count { get; set; }
        public string sims_present_student_count { get; set; }
        public string sims_present_student_count_local { get; set; }
        public string sims_present_student_count_expat { get; set; }
        public string sims_percentage_student_local { get; set; }
        public string sims_percentage_student_expat { get; set; }
        public string sims_mode_view { get; set; }
        public string result_status_code { get; set; }
        public string result_status_name { get; set; }
        public string student_name_en { get; set; }
        public string student_name_ot { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_attendance { get; set; }
        public string sims_remark { get; set; }
        public string sims_percentage { get; set; }
        public string sims_rank { get; set; }
        public string sims_with_excuse { get; set; }
        public string sims_without_excuse { get; set; }
        public string sims_enroll_number { get; set; }
        public string std_student_name { get; set; }
        public bool std_result_status { get; set; }
        public string no_on_roll { get; set; }
        public string no_passed { get; set; }
        public string no_promoted { get; set; }
        public string no_detained { get; set; }
        public string no_retest_in_arabic { get; set; }
        public string no_retest_in_education { get; set; }
        public string no_retest_in_uae { get; set; }
        public string no_present { get; set; }



    }


    public class simsSiblingConcession
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_concession_academic_year { get; set; }
        public string academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_full_name { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_academic_year { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string sims_academic_year_status { get; set; }
        public string sims_concession_number { get; set; }
        public string sims_concession_desc { get; set; }





        public string std_fee_enroll_number { get; set; }

        public string std_fee_cur_code { get; set; }

        public string std_fee_academic_year { get; set; }

        public string std_fee_grade_code { get; set; }

        public string std_fee_section_code { get; set; }

        public string std_fee_Class { get; set; }

        public string std_fee_student_name { get; set; }

        public string std_fee_section_name { get; set; }

        public bool std_fee_status { get; set; }

        public string std_fee_Sibling_Names { get; set; }

        public string std_fee_grade_name { get; set; }

        public string std_fee_sibling_count { get; set; }

        public string std_fee_Concession_status { get; set; }

        public bool std_fee_selected_status { get; set; }
        public string sims_fee_category_description { get; set; }
    }

    #region teacherLecture
    public class teacherLecture
    {
        // internal string sims_cur_short_name_en;
        public string sims_bell_sr_no { get; set; }
        public string cur_code { get; set; }
        public string teacher_code { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
        public string subject_code { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }
        public string aca_year { get; set; }
        public string bell_code { get; set; }
    }

    #endregion

    #region ttGradeSecObj
    public class ttGradeSecObj
    {
        public string teacher_code { get; set; }
        public string subject_code { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }
        public string teacher_name { get; set; }
        public string sub_name { get; set; }
        public string sub_color { get; set; }
    }

    #endregion

    #region tTimeTableGenObj
    public class tTimeTableGenObj
    {
        public string subject_code { get; set; }
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string teacher_code { get; set; }
        public string lecture_count { get; set; }
        public string lecture_practical_count { get; set; }
        public string bell_desc { get; set; }
        public string grade_name { get; set; }
        public string teacher_name { get; set; }
        public string sub_code { get; set; }
        public string sub_name { get; set; }
        public string sub_color { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }


    }

    #endregion

    #region tTimeTablePendingObj
    public class tTimeTablePendingObj
    {
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string teacher_code { get; set; }
        public string subject_code { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }
        public string sub_name { get; set; }
        public string sub_color { get; set; }
    }
    #endregion


    #region tTimeTableFinalObj
    public class tTimeTableFinalObj
    {
        public string aca_year { get; set; }
        public string bell_code { get; set; }
        public List<tTimeTableGenObj> timetabledata { get; set; }
        public List<tTimeTablePendingObj> pendingsub { get; set; }
    }
    #endregion

    #region EmergencyPickup
    public class EmergencyPickup
    {
        public string cur_code { get; set; }
        public string aca_year { get; set; }
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string enroll_num { get; set; }
        public string student_name { get; set; }
        public string pickup_name { get; set; }
        public string relation_id { get; set; }
        public string mobile_num { get; set; }
        public string img_path { get; set; }
    }
    #endregion

    #region tTimeTableObj
    public class tTimeTableObj
    {
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string grade_name { get; set; }
        public string sec_name { get; set; }
        public List<ttGradeSecObj> gradeSec { get; set; }
    }

    #endregion

    #region TimeTableObj
    public class TimeTableObj
    {
        public string teacher_code { get; set; }
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string section_code { get; set; }
        public string subject_code { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }
        public string aca_year { get; set; }
        public string bell_code { get; set; }
        public string slot_group { get; set; }
        public string grade_name { get; set; }
        public string teacher_name { get; set; }
        public string subject_name { get; set; }
        public string subject_color { get; set; }
        public string section_name { get; set; }
        public string slot_room { get; set; }
        public string cur_code { get; set; }
    }
    #endregion

    #region GradeObj
    public class GradeObj
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_aca_year { get; set; }
    }

    #endregion

    #region SectionObj
    public class SectionObj
    {
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string status { get; set; }
    }

    #endregion

    #region AcaObj
    public class AcaObj
    {
        public string aca_year { get; set; }
        public string aca_year_desc { get; set; }

        public string aca_year_status { get; set; }
    }

    #endregion

    #region Versions
    public class Versions
    {
        public string version_name { get; set; }
        public string version_update { get; set; }
    }

    #endregion
    #region SubjectObj
    public class SubjectObj
    {
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_subject_color { get; set; }
        public string sims_subject_type { get; set; }
        public string sims_subject_preference_code { get; set; }

        public string sims_teacher_name { get; set; }
    }

    #endregion

    #region TeachertObj
    public class TeachertObj
    {
        public string teacher_code { get; set; }
        public string teacher_name { get; set; }
    }

    #endregion

    #region DaySlotObj
    public class DaySlotObj
    {
        public string day_count { get; set; }
        public string slot_count { get; set; }
    }

    #endregion

    #region PreObj
    public class PreObj
    {
        public string aca_year { get; set; }
        public string bell_code { get; set; }
        public string cur_code { get; set; }
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string day_code { get; set; }
        public string slot_code { get; set; }
        public string sub_code { get; set; }
        public string sub_name { get; set; }
        public string sub_color { get; set; }
        public string preference { get; set; }
        public string teacher_code { get; set; }
        public string teacher_name { get; set; }

        public string sims_bell_appl_form_field { get; set; }



        public string sims_bell_appl_parameter { get; set; }
    }

    #endregion


    public class MograObj
    {
        public string cs_no { get; set; }
        public string project_name { get; set; }
        public string project_code { get; set; }
        public string developer_name { get; set; }
        public string developer_code { get; set; }
        public string developed_date { get; set; }
        public string status { get; set; }
        public string mod_name { get; set; }
        public string app_name { get; set; }
        public string document { get; set; }
        public string testcase { get; set; }
        public string remark { get; set; }
        public string SP { get; set; }
        public string SP_remark { get; set; }
        public bool aprove_status { get; set; }

    }

    #region GradeSecObj
    public class GradeSecObj
    {
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string aca_year { get; set; }
        public string cur_code { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public List<SectionObj> section { get; set; }
    }

    #endregion

    #region BellGradeObj
    public class BellGradeObj
    {
        public string aca_year_desc { get; set; }

        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string status { get; set; }
        public List<SectionObj> section { get; set; }

        public string bell_code { get; set; }

        public string bell_name { get; set; }

        public string aca_year { get; set; }

        public string bell_status { get; set; }
        public string curcode { get; set; }
    }

    #endregion

    #region BellLectureObj
    public class BellLectureObj
    {
        public string from_time { get; set; }
        public string to_time { get; set; }
        public string isBreak { get; set; }
        public string slot_code { get; set; }
        public string lect_desc { get; set; }
    }

    #endregion

    #region BellDayObj
    public class BellDayObj
    {
        public string day_code { get; set; }
        public string day_name { get; set; }
        public List<BellLectureObj> lecture { get; set; }
    }

    #endregion


    #region BellConfigObj
    public class BellConfigObj
    {
        public string aca_year { get; set; }
        public string bell_name { get; set; }
        public List<BellGradeObj> bellGrade { get; set; }
        public List<BellDayObj> bellDay { get; set; }
    }
    #endregion

    #region LectureSubObj
    public class LectureSubObj
    {
        public string substitution_remark { get; set; }
        public string aca_year { get; set; }
        public string bell_name { get; set; }

        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string sims_bell_subject_code { get; set; }

        public string sims_bell_slot_group { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_bell_slot_room { get; set; }

        public string bell_code { get; set; }

        public string grade_code { get; set; }

        public string sec_code { get; set; }

        public string substitution_acknowledgement_status { get; set; }

        public string substitution_status { get; set; }

        public string subs_subject_code { get; set; }

        public string subs_teacher_code { get; set; }

        public string filter_date { get; set; }

        public string subject_code { get; set; }

        public string day_code { get; set; }

        public string slot_code { get; set; }

        public string slot_group { get; set; }

        public string slot_room { get; set; }

        public string teacher_code { get; set; }
    }

    #endregion



    #region BellObj
    public class BellObj
    {
        public string bell_code { get; set; }
        public string bell_name { get; set; }
        public string aca_year { get; set; }
        public string aca_year_desc { get; set; }

        public string curcode { get; set; }

        public string curname { get; set; }
    }

    #endregion


    #region BellDetailObj
    public class BellDetailObj
    {
        public string bell_code { get; set; }
        public string day_code { get; set; }
        public string aca_year { get; set; }
        public string slot_code { get; set; }
        public string bell_break { get; set; }
        public string from_time { get; set; }
        public string to_time { get; set; }
        public string status { get; set; }
        public string lect_desc { get; set; }
    }

    #endregion


    #region BellSecObj
    public class BellSecObj
    {
        public string bell_code { get; set; }
        public string aca_year { get; set; }
        public string grade_code { get; set; }
        public string sec_code { get; set; }
        public string status { get; set; }

    }

    #endregion







    public class minutesOfMeetingType
    {
        public string sims_mom_type_code { get; set; }
        public string sims_mom_type_desc_en { get; set; }
        public bool sims_mom_type_status { get; set; }
        public string opr { get; set; }
    }

    public class activityGroupMaster
    {
        public string sims_cur_short_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_term_desc_en { get; set; }
        public string opr { get; set; }
        public string sims_activity_group_code { get; set; }
        public string sims_activity_group_desc { get; set; }
        public bool sims_activity_group_status { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public bool update_status { get; set; }

    }

    public class activityExamMaster
    {
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_exam_code { get; set; }
        public string sims_exam_term_code { get; set; }
        public string sims_exam_mark_entry_end_date { get; set; }
        public string sims_exam_marks_entry_start_date { get; set; }
        public bool sims_exam_status { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string opr { get; set; }
        public string sims_exam_desc { get; set; }

    }

    public class activityIndicator
    {
        public string sims_indicator_group_remark { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_indicator_group_desc { get; set; }
        public string sims_indicator_group_name { get; set; }
        public string sims_indicator_group_code { get; set; }
        public string sims_indicator_group_short_name { get; set; }
        public bool sims_status { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_activity_group_code { get; set; }
        public string sims_activity_group_desc { get; set; }
        public string sims_indicator_group_value { get; set; }
        public string opr { get; set; }
        public string sims_scholastic_evaluation_group_code { get; set; }
        public string sims_scholastic_evaluation_group_desc { get; set; }


        public string comn_user_name { get; set; }

        public string parent_name { get; set; }

        public string student_name { get; set; }

        public string sims_acdemic_year { get; set; }

        public string class_nm { get; set; }

        public string sims_enroll_number { get; set; }

        public string stud_Name { get; set; }

        public string sims_created_by { get; set; }

        public string sims_status_new { get; set; }

        public string sims_acdemic_year_code { get; set; }
    }

    public class scholasticEvalGroupMaster
    {
        public string sims_cur_short_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_term_desc_en { get; set; }
        public string opr { get; set; }
        public string sims_scholastic_evaluation_group_code { get; set; }
        public string sims_scholastic_evaluation_group_desc { get; set; }
        public bool sims_scholastic_evaluation_group_status { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public bool update_status { get; set; }

    }

    public class sims_block {
        public string opr { get; set; }
        public string list { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_term_code { get; set; }

        public string sims_created_by { get; set; }

        public string sims_status { get; set; }
    }


    public class reportCardAttend
    {
        public string student_name { get; set; }

        public string sims_enrollment_number { get; set; }
        public string std_name { get; set; }
        public string holiday { get; set; }
        public string present { get; set; }
        public string absent { get; set; }
        public string unmarked { get; set; }
        public string leave { get; set; }
        public string weekend { get; set; }
        public string tardy { get; set; }
        public string tardyExused { get; set; }
        public string opr { get; set; }
        public string absentExused { get; set; }
        public string notApplicable { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public int totalAttendance { get; set; }
        public int totalCount { get; set; }
        public string sims_term_code { get; set; }
        public string sims_attendance_code { get; set; }
        public string sims_attendance_cout { get; set; }

        public List<attendance_code_list> attedancecount { get; set; }
    }

    public class attendance_code_list
    {
        public string enroll_number { get; set; }
        public string attendace_code { get; set; }
    }

    #region admissionQuota
    public class admisison_quota
    {
        public string sims_q_cur_code { get; set; }
        public string sims_q_cur_name { get; set; }
        public string sims_q_academic_year { get; set; }
        public string sims_q_academic_desc { get; set; }
        public string sims_academic_year_status { get; set; }
        public string sims_q_grade_code { get; set; }
        public string sims_q_grade_desc { get; set; }

        public string sims_q_sr_no { get; set; }
        public string sims_quota_id { get; set; }
        public string sims_quota_desc { get; set; }
        public string sims_q_user_name { get; set; }
        public string total_quota_strength { get; set; }
        public string total_recommended_seats { get; set; }
        //ADMISSION
        public string sims_admission_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_admission_enroll_number { get; set; }
        public string sims_admission_parent_Name { get; set; }
        public string sims_admission_employee_code { get; set; }
        public bool sims_admission_recommend_status { get; set; }
        public string sims_user_code { get; set; }
        public string balance_recommended_seats { get; set; }
        public string sims_q_remark { get; set; }

        public bool sims_admission_done_status { get; set; }
    }
    #endregion

    public class admissionclasses
    {
        public string sims_academic_year_description { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_term_desc_en { get; set; }
        public string opr { get; set; }
        public string sims_birth_date_from { get; set; }
        public string sims_birth_date_to { get; set; }
        public string sims_admission_start_from { get; set; }
        public string sims_admission_end { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }

    }

    #region Sims507
    public class Sims507_SrchQry
    {
        public string sims_search_code { get; set; }
        public string sims_search_desc { get; set; }
        public string sims_search_short_name { get; set; }
        public string sims_search_status { get; set; }
        public string sims_search_user_code { get; set; }
        public string sims_search_query { get; set; }
        public string sims_search_user_status { get; set; }
        public string sims_search_user_name { get; set; }

        public string sims_cur_short_name_en { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_passport_name_en { get; set; }
        public string sims_student_nickname { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_religion_name_en { get; set; }
        public string sims_nationality_name_en { get; set; }
        public string sims_student_remark { get; set; }
        public string sims_house_name { get; set; }
        public string sims_parent_father_name { get; set; }
        public string sims_parent_mother_name { get; set; }
        public string sims_parent_guardian_name { get; set; }
        public string sims_parent_father_family_name { get; set; }
        public string sims_parent_mother_family_name { get; set; }
        public string sims_parent_guardian_family_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_guardian_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_guardian_email { get; set; }
        public string sims_subject_name_en { get; set; }

        public string gradecode { get; set; }
        public string sectioncode { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
    }

    public class Sims507_SrchCondn
    {
        public string srch_condn { get; set; }
        public string srch_equalto { get; set; }
        public string srch_in { get; set; }
        public string srch_like { get; set; }
        public string srch_value { get; set; }

        public string srch_code { get; set; }
        public string srch_querydesc { get; set; }
        public string srch_users { get; set; }
        public string isRadCmb { get; set; }
        public List<Sims507_SrchCondn> dataList { get; set; }
    }

    #endregion

    #region Sims548
    public class Sims548
    {

        public string emp_id { get; set; }
        public string class_details { get; set; }
        public string sims_leave_number { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_Stud_name { get; set; }
        public string sims_leave_code { get; set; }
        public string sims_leave_type { get; set; }
        public string sims_leave_start_date { get; set; }
        public string sims_leave_end_date { get; set; }
        public string sims_leave_reason { get; set; }
        public string sims_leave_application_date { get; set; }
        public string sims_leave_status { get; set; }
        public string sims_leave_status_name { get; set; }
        public string sims_leave_assigned_to { get; set; }
        public string sims_leave_status_changed_by { get; set; }
        public string sims_leave_remark { get; set; }
        public string sims_leave_remark_new { get; set; }
        public string sims_leave_remark_old { get; set; }
        public string ord1 { get; set; }
        public string ord2 { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }

        public string sims_leave_address { get; set; }
        public string sims_leave_phone { get; set; }
        public string sims_leave_mobile { get; set; }
        public string sims_leave_email { get; set; }
        public string sims_leave_attachment { get; set; }
        public bool sims_leave_missed_cycle_test_term_exam { get; set; }
    }
    #endregion


    #region Sim510 (Search User)
    public class Sim510
    {

        public string opr { get; set; }
        public string sims_search_code { get; set; }
        public string Description { get; set; }
        public string Users { get; set; }


        public string sims_search_desc { get; set; }
        public string sims_search_short_name { get; set; }
        public string sims_search_status { get; set; }
        public string sims_search_user_code { get; set; }
        public string sims_search_query { get; set; }
        public string sims_search_user_status { get; set; }
        public string sims_search_user_name { get; set; }

        public string sims_cur_short_name_en { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_passport_name_en { get; set; }
        public string sims_student_nickname { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_religion_name_en { get; set; }
        public string sims_nationality_name_en { get; set; }
        public string sims_student_remark { get; set; }
        public string sims_house_name { get; set; }
        public string sims_parent_father_name { get; set; }
        public string sims_parent_mother_name { get; set; }
        public string sims_parent_guardian_name { get; set; }
        public string sims_parent_father_family_name { get; set; }
        public string sims_parent_mother_family_name { get; set; }
        public string sims_parent_guardian_family_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_guardian_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_guardian_email { get; set; }
        public string sims_subject_name_en { get; set; }

        public string gradecode { get; set; }
        public string sectioncode { get; set; }

    }
    #endregion

    #region Alert
    public class alertclass
    {
        public string alert_number { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
        public string module_code { get; set; }
        public string module_name { get; set; }
        public string alert_msg { get; set; }
        public string alert_date { get; set; }
        public string alert_status { get; set; }
        public string alert_status_name { get; set; }
        public bool a_status { get; set; }
        public string usercode { get; set; }
        public string unread_status { get; set; }


        public string comn_alert_appl_code { get; set; }

        public string opr { get; set; }
        public string alertno { get; set; }
        public string user { get; set; }


    }
    #endregion

    #region Approval Email/SMS/Communication
    public class ApprovalESC
    {
        public string sims_recepient_id { get; set; }
        public string sims_sms_number { get; set; }
        public string sims_sms_message { get; set; }
        public string sims_sms_date { get; set; }
        public string sims_comm_number { get; set; }
        public string sims_comm_tran_number { get; set; }
        public string sims_comm_tran_date { get; set; }
        public string sims_comm_message { get; set; }
        public string sims_email_subject { get; set; }
        public string sims_email_date { get; set; }
        public string sims_email_message { get; set; }
        public string sims_email_number { get; set; }
    }
    #endregion
    public class compose_lesson_plan
    {
        public string opr { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_name_en { get; set; }
        public string month_name { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_section_name { get; set; }


        public bool isenabled { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_syllabus_code { get; set; }

        public string sims_syllabus_lesson_plan_code { get; set; }
        public string sims_syllabus_lesson_plan_description { get; set; }
        public string sims_syllabus_lesson_plan_no_of_periods { get; set; }
        public string sims_syllabus_lesson_plan_no_of_exercise { get; set; }
        public string sims_syllabus_lesson_plan_no_of_activity { get; set; }
        public string sims_syllabus_lesson_plan_no_of_homework { get; set; }
        public string sims_syllabus_lesson_plan_no_of_weekly_test { get; set; }
        public string sims_syllabus_lesson_plan_addl_requirement { get; set; }
        public string sims_syllabus_lesson_plan_creation_date { get; set; }
        public string sims_syllabus_lesson_plan_created_by { get; set; }
        public string sims_syllabus_lesson_plan_approval_date { get; set; }
        public string sims_syllabus_lesson_plan_approved_by { get; set; }
        public string sims_syllabus_lesson_plan_approval_remark { get; set; }
        public string sims_syllabus_lesson_plan_attr1 { get; set; }
        public string sims_syllabus_lesson_plan_attr2 { get; set; }
        public string sims_syllabus_lesson_plan_attr3 { get; set; }
        public string sims_syllabus_lesson_plan_attr4 { get; set; }
        public string sims_syllabus_lesson_plan_attr5 { get; set; }
        public string sims_syllabus_lesson_plan_status { get; set; }

        public string sims_syllabus_description { get; set; }
        public string sims_syllabus_unit_name { get; set; }
        public string sims_syllabus_unit_topic_name { get; set; }

        public string sims_syllabus_lesson_plan_doc_code { get; set; }
        public string sims_syllabus_lesson_plan_doc_path { get; set; }
        public string sims_syllabus_lesson_plan_doc_status { get; set; }
        public string sims_syllabus_lesson_plan_doc_upload_date { get; set; }
        public string sims_syllabus_lesson_plan_doc_uploaded_by { get; set; }

        public string sims_month_code { get; set; }

        public string sims_cur_name_en { get; set; }
        public string checkedvalues { get; set; }



    }


    public class meetings
    {
        public string sims_mom_reference_number { get; set; }
        public string sims_mom_type_code { get; set; }

        public string sims_mom_type_desc_en { get; set; }

        public string sims_mom_subject { get; set; }

        public string sims_mom_venue { get; set; }

        public string sims_mom_date { get; set; }

        public string sims_mom_start_time { get; set; }

        public string sims_mom_end_time { get; set; }

        public string sims_mom_chairperson_user_code { get; set; }

        public string sims_mom_recorder_user_code { get; set; }

        public string sims_mom_requester_user_code { get; set; }

        public string sims_mom_approver_user_code { get; set; }

        public string sims_mom_agenda { get; set; }

        public string sims_mom_status { get; set; }

        public string sims_mom_communication_status { get; set; }
        public string opr { get; set; }
        public string sims_mom_date_creation_user_code { get; set; }
        public string em_number { get; set; }

        public string em_first_name { get; set; }

        public string sims_mom_number { get; set; }

        public string attendees_name { get; set; }

        public string sims_mom_receipient_user_code { get; set; }

        public string sims_mom_requester_user_name { get; set; }

        public string sims_mom_chairperson_user_name { get; set; }

        public string sims_response_flag { get; set; }

        public string sims_mom_receipient_response_remark { get; set; }

        public string empcode { get; set; }

        public string employee_code { get; set; }

        public bool sims_mom_receipient_attendance { get; set; }

        public string sims_mom_srl_no { get; set; }

        public string sims_mom_description { get; set; }

        public string sims_mom_raised_user_code { get; set; }

        public string sims_mom_tobedone_user_code { get; set; }

        public string sims_mom_tobedone_date { get; set; }

        public string sims_mom_actual_start_time { get; set; }

        public string sims_mom_actual_end_time { get; set; }

        public string sims_mom_raised_user_name { get; set; }

        public string sims_mom_tobedone_user_name { get; set; }

        public string sims_mom_attendees_name { get; set; }

        public string sims_mom_actual_start_date { get; set; }
    }

    public class Sims692
    {
        public string sims_survey_code { get; set; }

        public string sims_survey_subject { get; set; }

        public string sims_survey_question_code { get; set; }

        public string sims_survey_question_desc_en { get; set; }

        public string questionanswer { get; set; }

        public string sims_survey_user_response_user_code { get; set; }

        public string sims_survey_user_response_answer_text { get; set; }

        public string sims_survey_question_type { get; set; }
        public string sims_survey_student_enroll_number { get; set; }

        public string opr { get; set; }

        public List<ans> answers = new List<ans>();
    }

    public class ans {

        public string sims_survey_question_answer_code { get; set; }

        public string sims_survey_question_answer_desc_en { get; set; }

        public string sims_survey_rating_code { get; set; }

        public string sims_survey_rating_img_path { get; set; }
    }

    public class syllabus_unit_sub_topic
    {

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_syllabus_code { get; set; }
        public string sims_syllabus_unit_code { get; set; }
        public string sims_syllabus_unit_topic_code { get; set; }
        public string sims_syllabus_unit_sub_topic_code { get; set; }
        public string sims_syllabus_unit_sub_topic_name { get; set; }
        public string sims_syllabus_unit_sub_topic_short_desc { get; set; }
        public string sims_syllabus_unit_sub_topic_creation_date { get; set; }
        public string sims_syllabus_unit_sub_topic_created_by { get; set; }
        public bool sims_syllabus_unit_sub_topic_status { get; set; }

        public string opr { get; set; }
    }

    public class syllabus_unit_topic
    {
        public string sims_syllabus_unit_name { get; set; }
        public string sims_section_code1 { get; set; }
        public string sims_month_code { get; set; }
        public string sims_display_name { get; set; }
        public string lesson_user { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_syllabus_code { get; set; }
        public string sims_syllabus_unit_code { get; set; }
        public string sims_syllabus_unit_topic_code { get; set; }
        public string sims_syllabus_unit_topic_name { get; set; }
        public string sims_syllabus_unit_topic_short_desc { get; set; }
        public string sims_syllabus_unit_topic_creation_date { get; set; }
        public string sims_syllabus_unit_topic_created_by { get; set; }
        public bool sims_syllabus_unit_topic_status { get; set; }
        public string opr { get; set; }

        public List<syllabus_unit_sub_topic> unit_sub_topic = new List<syllabus_unit_sub_topic>();

        public string sims_syllabus_unit_short_desc { get; set; }

        public string sims_syllabus_unit_creation_date { get; set; }

        public string sims_syllabus_unit_created_by { get; set; }
        public bool sims_syllabus_unit_status { get; set; }

    }

    public class syllabus_unit
    {
        public string sims_section_code1 { get; set; }
        public string month_name { get; set; }
        public string sims_display_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_month_code { get; set; }
        public string lesson_user { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_syllabus_code { get; set; }
        public string sims_syllabus_unit_code { get; set; }
        public string sims_syllabus_unit_name { get; set; }
        public string sims_syllabus_unit_short_desc { get; set; }
        public string sims_syllabus_unit_creation_date { get; set; }
        public string sims_syllabus_unit_created_by { get; set; }
        public string sims_syllabus_unit_approval_date { get; set; }
        public string sims_syllabus_unit_approved_by { get; set; }
        public string sims_syllabus_unit_approvar_remark { get; set; }
        public bool sims_syllabus_unit_status { get; set; }
        public string opr { get; set; }

        public List<syllabus_unit_topic> unit_topic = new List<syllabus_unit_topic>();
    }

    public class syllabus_and_lesson
    {
        public string month_name { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_syllabus_approved_name { get; set; }
        public string sims_syllabus_created_name { get; set; }

        public string sims_month_code { get; set; }
        public string sims_section_code1 { get; set; }
        public string Approved_status { get; set; }
        public string sims_syllabus_lesson_plan_doc_path { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_syllabus_code { get; set; }
        public string sims_syllabus_description { get; set; }
        public string sims_syllabus_start_date { get; set; }
        public string sims_syllabus_end_date { get; set; }
        public string sims_syllabus_remark { get; set; }
        public string sims_syllabus_creation_date { get; set; }
        public string sims_syllabus_created_by { get; set; }
        public bool sims_syllabus_status { get; set; }
        public string opr { get; set; }

        public string sims_cur_name { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_name { get; set; }

        public List<syllabus_unit> unit_lst = new List<syllabus_unit>();

        public string sims_display_name { get; set; }
        public string lesson_user { get; set; }

        public string sims_syllabus_unit_code { get; set; }

        public string sims_syllabus_unit_name { get; set; }

        public string sims_syllabus_unit_topic_code { get; set; }

        public string sims_syllabus_unit_topic_name { get; set; }
    }

    public class Pers099_qual
    {
        public string sims_syllabus_created_name { get; set; }
        public string vacancy_id { get; set; }
        public string application_id { get; set; }
        public string pays_qualification_level_code { get; set; }
        public string pays_qualification_emp_id { get; set; }
        public string pays_qualification_level { get; set; }
        public string pays_qualification_level_name { get; set; }
        public string pays_qualification_code { get; set; }
        public string pays_qualification_name { get; set; }
        public string pays_qualification { get; set; }
        public string pays_qualification_year { get; set; }
        public string pays_qualification_remark { get; set; }
        public bool pays_qualification_emp_status { get; set; }
        public string opr { get; set; }
    }

    public class Pers099_Vacancy
    {
        public Pers099_Vacancy()
        {
            qual_details = new List<Pers099_qual>();
        }
        public string em_pwd { get; set; }
        public string em_doc { get; set; }
        public string em_login_code { get; set; }
        public string em_number { get; set; }

        public string em_dept_effect_from { get; set; }
        public string em_grade_effect_from { get; set; }


        public string em_first_name { get; set; }
        public string em_last_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_family_name { get; set; }
        public string em_full_name { get; set; }
        public string em_name_ot { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string em_service_status_code { get; set; }
        public string em_service_status { get; set; }
        public string em_marital_status { get; set; }
        public bool em_bank_cash_tag { get; set; }
        public string em_ledger_name { get; set; }
        public string em_ledger_ac_no { get; set; }
        public string em_gpf_ac_no { get; set; }
        public string em_gosi_ac_no { get; set; }
        public string em_gosi_start_date { get; set; }
        public string em_pan_no { get; set; }
        public string em_labour_card_no { get; set; }
        public string em_iban_no { get; set; }
        public string em_route_code { get; set; }
        public string em_status_code { get; set; }
        public string em_status { get; set; }
        public string application_status { get; set; }
        public bool em_stop_salary_indicator { get; set; }
        public string em_leave_resume_date { get; set; }
        public bool em_leave_tag { get; set; }
        public string em_modified_on { get; set; }
        public bool em_citi_exp_tag { get; set; }
        public string em_joining_ref { get; set; }
        public string em_bank_name { get; set; }
        public string em_bank_ac_no { get; set; }
        public string em_bank_swift_code { get; set; }
        public string em_dependant_full { get; set; }
        public string em_dependant_half { get; set; }
        public string em_dependant_infant { get; set; }
        public string em_left_date { get; set; }
        public string em_left_reason { get; set; }
        public bool em_handicap_status { get; set; }
        public string em_leave_start_date { get; set; }
        public string em_leave_end_date { get; set; }
        public string em_cl_resume_date { get; set; }
        public string em_leave_resume_ref { get; set; }
        public string em_over_stay_days { get; set; }
        public string em_under_stay_days { get; set; }
        public string em_stop_salary_from { get; set; }
        public string em_unpaid_leave { get; set; }
        public bool em_punching_status { get; set; }
        public string em_punching_id { get; set; }
        public string em_passport_no { get; set; }
        public string em_pssport_issue_date { get; set; }
        public string em_passport_issuing_authority { get; set; }
        public string em_passport_issue_place { get; set; }
        public string em_passport_ret_date { get; set; }
        public string em_passport_exp_date { get; set; }
        public string em_passport_exp_rem_date { get; set; }
        public string em_visa_no { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_issuing_place { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_visa_exp_date { get; set; }
        public string em_visa_exp_rem_date { get; set; }
        public bool em_agreement { get; set; }
        public string em_agreement_start_date { get; set; }
        public string em_agreement_exp_date { get; set; }
        public string em_agreemet_exp_rem_date { get; set; }
        public string em_passport_issue_date { get; set; }
        public string em_visa_sponsor { get; set; }
        public string em_visa_type { get; set; }
        public string em_visa_type_code { get; set; }
        public string em_apartment_number { get; set; }
        public string em_building_number { get; set; }
        public string em_street_number { get; set; }
        public string em_area_number { get; set; }
        public string em_summary_address { get; set; }
        public string em_summary_address_local_language { get; set; }
        public string em_religion_code { get; set; }
        public string em_ethnicity_name { get; set; }
        public string em_img { get; set; }
        public string em_emergency_contact_name1 { get; set; }
        public string em_emergency_contact_name2 { get; set; }
        public string em_emergency_contact_number1 { get; set; }
        public string em_emergency_contact_number2 { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string em_last_login { get; set; }
        public string em_secret_question_code { get; set; }
        public string em_secret_question { get; set; }
        public string em_secret_answer { get; set; }
        public string em_Country_Code { get; set; }
        public string em_Country_name { get; set; }
        //public string em_State_Code { get; set; }
        //public string em_State_name { get; set; }
        //public string em_City_Code { get; set; }
        //public string em_City_name { get; set; }
        public string em_blood_group_code { get; set; }
        public string em_blood_group_name { get; set; }
        public string em_ethnicity_name_en { get; set; }
        public string em_ethnicity_code { get; set; }
        public string em_company_code { get; set; }
        public string em_Company_Code { get; set; }
        public string em_Company_name { get; set; }
        public string em_Company_Short_Name { get; set; }
        public string em_Dept_Code { get; set; }
        public string em_Dept_name { get; set; }
        public string em_Dept_Short_Name { get; set; }
        public string em_Dept_Company_Code { get; set; }
        public string em_Nation_Code { get; set; }
        public string em_Nation_name { get; set; }
        public string em_Grade_Code { get; set; }
        public string em_Grade_name { get; set; }
        public string em_Grade_Company_Code { get; set; }
        public string em_staff_type { get; set; }
        public string em_Staff_Type_Code { get; set; }
        public string em_Staff_Type_name { get; set; }
        public string em_salutation { get; set; }
        public string em_Salutation_Code { get; set; }
        public string em_Salutation_name { get; set; }
        public string em_Sex_Code { get; set; }
        public string em_Sex_name { get; set; }
        public string em_Marital_Status_Code { get; set; }
        public string em_Marital_Status_name { get; set; }
        public string em_desg_name { get; set; }
        public string em_Designation_Code { get; set; }
        public string em_Designation_name { get; set; }
        public string em_Designation_Company_Code { get; set; }
        public string em_Dest_Code { get; set; }
        public string em_Dest_name { get; set; }
        public string em_Religion_Code { get; set; }
        public string em_Religion_name { get; set; }
        public string em_Role_Code { get; set; }
        public string em_Role_name { get; set; }
        public string em_Role_desg_code { get; set; }
        public string em_Role_desg_company_code { get; set; }
        public string em_City_Code { get; set; }
        public string em_City_name { get; set; }
        public string em_State_Code { get; set; }
        public string em_State_name { get; set; }
        public string em_Bank_Code { get; set; }
        public string em_Bank_name { get; set; }
        public string em_fax { get; set; }
        public string em_post { get; set; }
        public string em_phone { get; set; }
        public string em_mobile { get; set; }
        public string em_email { get; set; }
        public string em_attendance_month_name { get; set; }
        public string em_attendance_working_days { get; set; }
        public string em_attendance_absent_days { get; set; }
        public string em_attendance_prasent_days { get; set; }
        public string em_attendance_leave_days { get; set; }
        public string leave_type { get; set; }
        public string leave_taken { get; set; }
        public string leave_max_allowed { get; set; }
        public string leaves_extra { get; set; }
        public string leaves_remaing { get; set; }
        public List<Pers099_qual> qual_details;
        public string opr { get; set; }
        public string sims_blood_group_name { get; set; }
        public string pays_qualification_emp_id { get; set; }
        public string pays_qualification_code { get; set; }
        public string pays_qualification_name { get; set; }
        public string pays_qualification_level { get; set; }
        public string pays_qualification_level_name { get; set; }
        public string pays_qualification_year { get; set; }
        public string pays_qualification_remark { get; set; }
        public string pays_qualification_emp_status { get; set; }
        public string emp_qual_qual_code { get; set; }
        public string sponser_name { get; set; }
        public string relation { get; set; }
        public string em_occupation { get; set; }
        public string em_workplace { get; set; }
        public string em_office_phone { get; set; }
        public string accomm { get; set; }
        public string nation_id { get; set; }
        public string em_qual_year { get; set; }
        public string em_pssport_exp_rem_date { get; set; }
        public string comn_role_code { get; set; }
        public string comn_role_name { get; set; }
        public string lic_school_code { get; set; }
        public string lic_school_name { get; set; }
        public string lic_school_country { get; set; }
        public string emp_reg_id { get; set; }
        public string emp_msg { get; set; }
        public string emp_msg1 { get; set; }
        public string em_reg_id { get; set; }
        public string emp_status { get; set; }
        public string emp { get; set; }
        public string emp_id { get; set; }

        public string sims_vacancy_code { get; set; }

        public string sims_vacancy_name { get; set; }
    }

    public class Sim041
    {


        public string sims_school_code { get; set; }

        public string sims_smtp_code { get; set; }

        public string sims_mod_code { get; set; }

        public string sims_smtp_serverip { get; set; }

        public string sims_smtp_port { get; set; }

        public string sims_smtp_username { get; set; }

        public string sims_smtp_password { get; set; }

        public string sims_smtp_from_email { get; set; }

        public bool sims_smtp_status { get; set; }

        public bool curriculum_status { get; set; }

        public string sims_smtp_auth_required { get; set; }

        public string sims_smtp_ssl_required { get; set; }

        public string sims_smtp_max_mail_hour { get; set; }

        public string sims_smtp_max_mail_day { get; set; }

        public string sims_smtp_encrypt_mode { get; set; }

        public string sims_smtp_from_email_name { get; set; }

        public string sims_smtp_config_id { get; set; }

        public string sims_smtp_signature { get; set; }

        public string lic_school_code { get; set; }

        public string lic_school_name { get; set; }

        public string opr { get; set; }

        public string sims_smtp_address { get; set; }

        public string comn_mod_code { get; set; }

        public string comn_mod_name_en { get; set; }



        public string sims_smtp_remark { get; set; }
    }

    public class Com007
    {
        public string opr { get; set; }
        public string comn_audit_start_time;

        public string sims_appl_code { get; set; }

        public string sr_no { get; set; }

        public string sims_appl_name { get; set; }

        public string sims_mod_code { get; set; }

        public string sims_mod_name { get; set; }

        public string comn_audit_user_code { get; set; }

        public string comn_user_name { get; set; }

        public string comn_mod_name_en { get; set; }

        public string comn_appl_name_en { get; set; }

        public string comn_audit_ip { get; set; }

        public string comn_audit_dns { get; set; }

        public string comn_audit_end_time { get; set; }

        public string comn_audit_remark { get; set; }

        public string comn_appl_code { get; set; }
        public string comn_appl_code_parent { get; set; }

        public string comn_mod_img { get; set; }

        public string st_time { get; set; }

        public string st_am { get; set; }

        public string st_date { get; set; }

        public string comn_appl_code_name { get; set; }

        public string comn_appl_mod_code { get; set; }

        public string comn_appl_mod_name { get; set; }

        public string cnt { get; set; }

        public string comn_appl_field_name_en { get; set; }

        public string comn_field_appl_code { get; set; }

        public bool comn_show_icon_status { get; set; }

        public string comn_appl_field_name { get; set; }

        public string comn_appl_field_desc { get; set; }

        public string comn_creation_user { get; set; }

        public string comn_appl_creation_date { get; set; }

        public string comn_field_display_order { get; set; }

        public bool comn_appl_field_status { get; set; }

        public string comn_mod_code { get; set; }




        public string comn_appl_type { get; set; }

        public string comn_appl_location { get; set; }

        public string comn_mod_name_ar { get; set; }

        public string comn_appl_name_ar { get; set; }
    }

    public class StudentImg
    {

        public string sims_enrollment_number { get; set; }

        public string sims_student_full_name { get; set; }

        public string sims_student_ea_number { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name_en { get; set; }

        public string status_name { get; set; }

        public string opr { get; set; }

        public string sims_student_img { get; set; }

        public string sims_student_img_path { get; set; }
        public string sims_behaviour_description { get; set; }
        public string sims_behaviour_code { get; set; }
    }

    public class feeDesc
    {
        public string enroll_number { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_period_paid { get; set; }



    }
    public class Sim615
    {
        public List<feeDesc> feedes { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string enroll_number { get; set; }
        public string doc_reference_no { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string dd_fee_payment_mode { get; set; }
        public string cheque_number { get; set; }
        public string dd_fee_cheque_date { get; set; }
        public string bank_name { get; set; }
        public string fee_paid { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_code { get; set; }
        public string cashier_code { get; set; }
        public string cashier_name { get; set; }
        public string payment_mode_code { get; set; }
        public string payment_mode_name { get; set; }
        public string dd_fee_amount_final { get; set; }
        public string receipt_status { get; set; }
        public string Doc_status { get; set; }
        public string doc_date { get; set; }
        public string dd_enroll_number { get; set; }
        public string dd_realize_date { get; set; }
        public string reciept_number { get; set; }
        public string doc_no { get; set; }
        public string creation_date { get; set; }
        public string grand_total { get; set; }
        public string student_name { get; set; }
        public string doc_academic_year { get; set; }
        public string doc_academic_year_description { get; set; }
        public string dt_code { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string comn_appl_parameter2 { get; set; }
        public string comn_appl_form_field_value12 { get; set; }
        public string comn_appl_parameter1 { get; set; }
        public string comn_appl_form_field_value11 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_fee_number { get; set; }
        public string sims_fee_category { get; set; }
        public string sims_parent_number { get; set; }
        public string sims_parent_name { get; set; }
        public string section_name { get; set; }

        public string sims_fee_period_term_name { get; set; }
        public string sims_parent_contact { get; set; }
        public string pending_memo { get; set; }
        public string final_total { get; set; }



        public string sims_fee_cur_code { get; set; }

        public string sims_fee_academic_year { get; set; }

        public string sims_fee_grade_code { get; set; }

        public string sims_fee_section_code { get; set; }

        public string sims_transaction_by { get; set; }
        public string sims_school_remark { get; set; }
        public string sims_parent_remark { get; set; }
        public string expected_payment_date { get; set; }
        public string sims_transaction_date { get; set; }
        public string creation_user_name { get; set; }
        public string cash_amount { get; set; }
        public string cheque_amount { get; set; }
        public string credit_card_amount { get; set; }
        public string online_payment_amount { get; set; }
        public string other_amount { get; set; }
        public string doc_status_new { get; set; }
        public string comp_curcy_dec { get; set; }

        public string doc_narration { get; set; }

        public bool sims_status { get; set; }
    }

    public class outstand
    {

        public string cnt_90 { get; set; }
        public string cnt_75 { get; set; }
        public string cnt_50 { get; set; }
        public string cnt_30 { get; set; }
        public string cnt_bel_30 { get; set; }
        public string bal_90 { get; set; }
        public string bal_75 { get; set; }
        public string bal_50 { get; set; }
        public string bal_30 { get; set; }
        public string bal_bel_30 { get; set; }
        public string total { get; set; }
    }





    public class appro
    {
        public string temp_dd_doc_no { get; set; }
        public string temp_dd_doc_date { get; set; }
        public string temp_dd_enroll_number { get; set; }
        public string total_amt { get; set; }
        public string name { get; set; }
        public string temp_dd_fee_auth_code { get; set; }

        public string temp_dd_fee_card_last_number { get; set; }
        public string temp_dd_error_messsage { get; set; }

        public string temp_dd_fee_transaction_id { get; set; }

        public string temp_dd_doc_date_time { get; set; }
        public string temp_dd_grade { get; set; }
        public string temp_dd_section { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string fee_payment_mode { get; set; }
        public string ftransaction_type { get; set; }
        public string transaction_type { get; set; }
    }

    public class comn_depandancy
    {
        public string comn_appl_code { get; set; }

        public string comn_appl_code_name { get; set; }

        public string comn_appl_field_name_en { get; set; }

        public string comn_field_appl_code { get; set; }

        public string comn_appl_field_name { get; set; }

        public string comn_show_icon_status { get; set; }

        public string comn_appl_field_desc { get; set; }

        public string comn_appl_mod_code { get; set; }

        public string comn_appl_mod_name { get; set; }

        public string comn_creation_user { get; set; }

        public string comn_appl_creation_date { get; set; }

        public string comn_appl_field_status { get; set; }

        public string user_name { get; set; }

        public string comn_field_display_order { get; set; }
    }

    public class AdmissionCancel
    {
        public string msg { get; set; }
        public string flag { get; set; }
    }

    public class fee_recipt
    {
        public string academic_year { get; set; }
        public string academic_year_name { get; set; }
        public string academic_year_status { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }

        public string from_date { get; set; }
        public string to_date { get; set; }
        public string enroll_number { get; set; }
        public string doc_reference_no { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string dd_fee_payment_mode { get; set; }
        public string cheque_number { get; set; }
        public string dd_fee_cheque_date { get; set; }
        public string bank_name { get; set; }
        public string fee_paid { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_fee_code { get; set; }
        public string cashier_code { get; set; }
        public string cashier_name { get; set; }
        public string payment_mode_code { get; set; }
        public string payment_mode_name { get; set; }
        // public string Doc_status { get; set; }
        public string doc_status { get; set; }
        public string doc_date { get; set; }
        public string doc_no { get; set; }
        public string creation_date { get; set; }
        public string grand_total { get; set; }
        public string student_name { get; set; }
        public string doc_academic_year { get; set; }
        //  public string Doc_status { get; internal set; }
    }


    public class sims043_previous
    {
        public string doc_date { get; set; }
        public string dd_other_charge_amount { get; set; }
        public string doc_no { get; set; }

        public string Receipt_Type { get; set; }

        public string doc_status { get; set; }

        public string dd_fee_amount { get; set; }

        public string total_amount { get; set; }

        public string creation_date { get; set; }

        public string chequeColor_Desc { get; set; }

        public string creation_user { get; set; }

        public string dd_fee_amount_discounted { get; set; }

        public string dd_fee_amount_final { get; set; }

        public string dd_fee_payment_mode { get; set; }

        public string cheque_number { get; set; }

        public string ChequeStatus_Desc { get; set; }

        public string sims_fee_code_description { get; set; }

        public string doc_status_code { get; set; }

        public bool isChecked { get; set; }

        public bool isexpanded { get; set; }

        //public string dd_other_charge_amount { get; set; }
    }

    #region Defaulters Email
    public class sims_defaulter
    {
        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_view_list_code { get; set; }
        public string sims_view_list_description { get; set; }
        public string sims_view_mode_code { get; set; }
        public string sims_view_mode_description { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_view_list_option { get; set; }
        public string sims_amount { get; set; }
        public string sims_search { get; set; }
        public string sims_view_mode_opt { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_fee_amount { get; set; }

        public string sims_fee_paid { get; set; }

        public string student_name { get; set; }

        public string grade_name { get; set; }

        public string section_name { get; set; }

        public string sims_sibling_parent_number { get; set; }

        public string parent_email { get; set; }

        public string sims_paid_amount { get; set; }

        public string sims_balance_amount { get; set; }

        public string sims_father_name { get; set; }

        public bool isSelected { get; set; }

        public string email_subject { get; set; }

        public string email_message { get; set; }

        public string sims_fee_amount_total { get; set; }

        public string sims_paid_amount_total { get; set; }

        public string sims_fee_type_desc { get; set; }

        public string sims_balance_amount_total { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_description { get; set; }

        public string parent_mobile { get; set; }
        public string sims_student_status { get; set; }
        public string sims_parent_number { get; set; }
        public string sims_parent_email { get; set; }



        public string sims_expected_fee { get; set; }

        public string sims_paid_fee { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_attendance_day_attendance_code { get; set; }
        public string from { get; set; }
        public string to { get; set; }

        public string email_cc { get; set; }

        public string sims_msg_sr_no { get; set; }
    }
    public class defaulter1
    {
        public string email_default_template { get; set; }
        public string sims_msg_sr_no { get; set; }
        public string sims_msg_subject { get; set; }

    }

    #endregion



    public class Defaulter_LIst_new
    {

        public string sims_grade_name_en { get; set; }
        public string section_name { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_parent_number { get; set; }
        public string sims_parent_name { get; set; }
        public string sims_fee_period_expected { get; set; }
        public string sims_fee_period_paid { get; set; }
        public string final_total { get; set; }
        public string sims_parent_contact { get; set; }
        public string sims_parent_email { get; set; }
        public string sims_appl_parameter { get; set; }
        public string min_max_value { get; set; }
        public string sims_fee_cur_code { get; set; }
        public string sims_fee_academic_year { get; set; }
        public string sims_fee_grade_code { get; set; }
        public string sims_fee_section_code { get; set; }




    }


    public class CWD01 {
        public string cur_code { get; set; }

        public string academic_year { get; set; }

        public string grade_code { get; set; }

        public string section_code { get; set; }

        public string fee_code { get; set; }

        public string sims_enroll_number { get; set; }

        public string amount { get; set; }

        public string payment_mode { get; set; }

        public string user { get; set; }

        public string receipt_date { get; set; }

        public string remark { get; set; }
    }

    public class paidby {
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
    }

    public class sims043
    {
        public string sims_fee_code_description { get; set; }
        public bool isSplitted { get; set; }
        public string feeTypeGroup { get; set; }
        public string auto_id { get; set; }
        public string FeeNumber { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string FeeEnrollNo { get; set; }
        public string FeeType { get; set; }
        public bool FeeStatus { get; set; }
        public string FeePeriod { get; set; }
        public string FeeExpected { get; set; }
        public string FeePaid { get; set; }
        public string BalanceFee { get; set; }

        //Consession Fee-Discount Type Variables    
        public string fee_concession_number { get; set; }
        public string fee_concession_description { get; set; }
        public string fee_concession_type { get; set; }
        public string fee_concession_discount_type { get; set; }
        public string fee_concession_discount_value { get; set; }
        public string fee_concession_fee_code { get; set; }
        public string fee_concession_fee_code_description { get; set; }
        public string fee_concession_academic_year { get; set; }
        public string fee_concession_enroll_number { get; set; }
        public string fee_concession_dis_amt_cal { get; set; }
        public string fee_concession_fee_period_code { get; set; }
        //end-Consession Fee-Discount Type Variables    

        //Variables for Sibling Grid
        public bool stud_sib_select_status { get; set; }
        public string stud_sib_name { get; set; }
        public string stud_sib_cur_code { get; set; }
        public string stud_sib_academic_year { get; set; }
        public string stud_sib_grade_code { get; set; }
        public string stud_sib_grade_name { get; set; }
        public string stud_sib_section_code { get; set; }
        public string stud_sib_enroll_no { get; set; }
        public string stud_sib_stu_img { get; set; }


        //Variables of Sims043
        #region Sims043
        public string std_fee_number { get; set; }
        public string std_fee_enroll_number { get; set; }
        public string std_fee_Class { get; set; }
        public string std_fee_student_name { get; set; }
        public string std_fee_cur_code { get; set; }
        public string std_fee_academic_year { get; set; }
        public string std_fee_grade_code { get; set; }

        //public string  std_fee_grade_Name { get; set; }
        public string std_fee_section_code { get; set; }

        public string std_fee_section_Name { get; set; }
        public string std_fee_grade_name { get; set; }
        public string std_fee_section_name { get; set; }
        public string std_fee_parent_id { get; set; }
        public string std_fee_parent_name { get; set; }
        public string std_fee_amount { get; set; }
        public string std_fee_last_payment_date { get; set; }
        public bool std_fee_status { get; set; }
        public string std_fee_category_name { get; set; }
        public string std_fee_type { get; set; }
        public string std_fee_paid { get; set; }
        public decimal std_fee_remaining { get; set; }
        public string std_fee_paying { get; set; }
        public string std_fee_child_period { get; set; }
        public string std_fee_child_period_No { get; set; }
        public string std_fee_child_exp_amount { get; set; }
        public string std_fee_concession_amount { get; set; }
        public string std_fee_child_paid_amount { get; set; }
        public decimal std_fee_child_remaining_amount { get; set; }
        public decimal std_fee_child_paying_amount { get; set; }
        public string std_fee_bank_name { get; set; }
        public string std_fee_bank_id { get; set; }
        public string std_fee_id { get; set; }
        public string std_fee_code { get; set; }
        public string std_TotalPaid { get; set; }
        public string std_BalanceFee { get; set; }
        public bool FeeChekStatus { get; set; }
        public bool Installment_mode { get; set; }
        public bool Installment_mode_chk { get; set; }
        public string Installment_amnt { get; set; }
        public string sims_fee_collection_mode { get; set; }
        //Payment Details
        public string paymentMode { get; set; }
        public string paymentMode_show { get; set; }
        public string chequeDDNo { get; set; }
        //public DateTime chequeDDDate { get; set; }
        public string check_amount { get; set; }
        public string chequeDDDate { get; set; }
        public string BankName { get; set; }
        public string TransactionID { get; set; }
        public string PayAmount { get; set; }
        public bool isecxess_fee { get; set; }
        public bool isTransaction { get; set; }
        public bool isCheque { get; set; }
        public bool isCash { get; set; }
        public bool isAmount { get; set; }
        public string id { get; set; }
        public bool current_flag { get; set; }
        public string PayDisAmount { get; set; }
        public string PayDisPeriodNo { get; set; }
        public string PayFeeCode { get; set; }
        #endregion

        #region sims_Fees_document
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string doc_total_amount { get; set; }
        public string doc_enroll_no { get; set; }
        public string doc_other_charge_desc { get; set; }
        public string doc_other_charge_amount { get; set; }
        public string doc_discount_pct { get; set; }
        public string doc_discount_amount { get; set; }
        public string doc_narration { get; set; }
        #endregion

        #region feeDisocunt
        public string std_fee_discount_type { get; set; }
        public string std_fee_discount_type_code { get; set; }
        public string std_fee_discount_paying_amount { get; set; }
        public string std_fee_other_type { get; set; }
        public string std_fee_other_type_code { get; set; }
        public decimal std_fee_other_paying_amount { get; set; }
        public decimal std_fee_discount_percent { get; set; }
        public bool std_fee_discount_inpercent { get; set; }
        public string std_fee_other_enrollNo { get; set; }

        #endregion

        public class Sim201
        {
            public string TypeName { get; set; }
            public string sims_appl_form_field_value1 { get; set; }
            public bool sims_device_status { get; set; }
            public string sims_device_type { get; set; }
            public string sims_device_value { get; set; }
            public string sims_enroll_number { get; set; }
            public string opr { get; set; }
            public string enroll_number { get; set; }
        }




        #region sims_Fees_document_details
        public string dd_line_no { get; set; }
        public string dd_fee_number { get; set; }
        public string dd_fee_amount { get; set; }
        public string dd_fee_amount_discounted { get; set; }
        public string dd_fee_amount_final { get; set; }
        #endregion

        public string parent_mobile_no { get; set; }
        public string parent_email_id { get; set; }

        public string fees_paid_email_btn { get; set; }

        public string fees_paid_sms_btn { get; set; }

        //19-06 Fee Agent Variables
        public string slma_ldgrctl_year { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public bool is_fee_temp { get; set; }
        public string temp_fee_amount { get; set; }
        public string std_fee_child_paying_amount_temp { get; set; }
        public string PayAmount_temp { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string std_fee_concession_amount_used { get; set; }
        public string std_orignal_exp_amount { get; set; }
        public bool IsMulti_Cheque_payment { get; set; }
        public bool isOtherFee { get; set; }
        public string student_attendance_per { get; set; }
        public string sims_concession_discount_type { get; set; }
        public string dd_fee_credit_card_code { get; set; }
        public string dd_fee_credit_card_auth_code { get; set; }
        public string doc_paying_agent_transaction_no { get; set; }
        public string doc_paying_agent_transaction_lineno { get; set; }
        public string sims_concession_discount_value { get; set; }

        public string student_attendance_last_date { get; set; }

        public string sims_student_academic_status_code { get; set; }

        public string sims_student_academic_status { get; set; }

        public string sims_search_code { get; set; }

        public string sims_search_code_name { get; set; }

        public string sims_search_code_status { get; set; }

        public string search_by_code { get; set; }
        public string search_by_name { get; set; }

        public bool student_status { get; set; }

        public string color { get; set; }

        public string paymentMode_code { get; set; }

        public bool is_parent_employee { get; set; }

        public bool is_adv_fee_defined { get; set; }

        public bool is_adv_section_defined { get; set; }

        public string adv_fee_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string is_adv_grade { get; set; }

        public string is_adv_section { get; set; }
        public string term_desc { get; set; }
        public string term_code { get; set; }
        public string total_fee { get; set; }

        public string std_fee_category_code { get; set; }
        public string fee_posting_status { get;  set; }

        public string std_fee_mother_name { get; set; }

        public string doc_code { get; set; }

        public string bill_date { get; set; }

        public string doc_type { get; set; }

        public string adjust_fee_code { get; set; }
        public string sims_fee_code_status { get; set; }

        public string sims_fee_code { get; set; }

     // public string sims_cur_code { get; set; }
     //public string sims_enroll_number { get; set; }
    }

    public class debit_creadit_note
    {
        public string doc_no { get; set; }
        public string std_fee_enroll_number { get; set; }
        public string std_fee_Class { get; set; }
        public string std_fee_student_name { get; set; }
        public string std_fee_cur_code { get; set; }
        public string std_fee_academic_year { get; set; }
        public string std_fee_grade_code { get; set; }
        public string std_fee_section_code { get; set; }
        public string std_fee_class_name { get; set; }
        public string std_fee_parent_name { get; set; }
        public string doc_total_amount { get; set; }
        public string std_fee_type { get; set; }
        public string doc_narration { get; set; }
        public string std_fee_code { get; set; }
        public string doc_date { get; set; }
        public string username { get; set; }
        public string doc_code { get; set; }
        public List<sims043> fee_list = new List<sims043>();


        public string dd_fee_amount { get; set; }

        public string std_fee_child_period_No { get; set; }

        public string std_fee_number { get; set; }

        public string doc_posting_status_desc { get; set; }

        public string doc_posting_status { get; set; }
    }

    public class Sims017
    {
        public bool sims_receivable_acno_flag { get; set; }
        public string opr { get; set; }
        public bool sims_status { get; set; }
        public string sims_concession_number { get; set; }
        public string sims_concession_description { get; set; }
        public string sims_concession_type { get; set; }
        public string sims_concession_discount_type { get; set; }
        public string sims_concession_discount_value { get; set; }
        public string sims_concession_applicable_on { get; set; }
        public bool sims_concession_corporate_billing { get; set; }
        public string student_full_name_en { get; set; }
        public string sims_concession_applicable_to { get; set; }
        public int sims_concession_onward_child { get; set; }
        public string sims_concession_fee_code { get; set; }
        public bool sims_concession_status { get; set; }
        public string sims_concession_fee_code_desc { get; set; }
        public string sims_concession_academic_year { get; set; }
        public List<SimsList017> list = new List<SimsList017>();

        public string sims_concession_academic_year_description { get; set; }
    }

    public class SimsList017
    {
        public string sims_concession_fee_code1 { get; set; }
    }

    public class Sims550
    {
        #region fee refund
        public Sims550()
        {
            fee_details = new List<Sims550_details>();
        }

        public string cur_code { get; set; }
        public string cur_desc { get; set; }
        public string fee_code { get; set; }
        public string fee_desc { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string grade_name { get; set; }
        public string grade_code { get; set; }
        public string payment_mode { get; set; }
        public string payment_mode_code { get; set; }
        public string payment_date { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string academic_year_name { get; set; }
        public string academic_year { get; set; }
        public string enroll_number { get; set; }
        public string student_name { get; set; }
        public string amount { get; set; }
        public string amount_used { get; set; }
        public decimal total_amount { get; set; }
        public string remark { get; set; }
        public bool status { get; set; }
        public string chk_Date { get; set; }
        public string chk_no { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_enroll_number { get; set; }
        #endregion

        public string parentcode { get; set; }
        public string sims_expected_amt { get; set; }
        public List<Sims550_details> fee_details { get; set; }
        public bool sims_iscash_mode { get; set; }

        public string sims_student_Name { get; set; }

        public bool sims_student_check_status { get; set; }

        public string sims_term_desc_en { get; set; }

        public string std_fee_number { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_parent_number { get; set; }

        public string studentName { get; set; }

        public string parentName { get; set; }

        public string sims_fee_code_description { get; set; }
    }
    public class Sims550_details
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_fee_description { get; set; }
        public string sims_fee_number { get; set; }
        public string sims_expected_amt { get; set; }
        public string sims_paid_amt { get; set; }
        public string sims_balance_amt { get; set; }
        public bool isOtherFee { get; set; }
        public string sims_cheque_no { get; set; }
        public string sims_cheque_date { get; set; }
        public string sims_bank_name { get; set; }
        public string Payment_mode { get; set; }

        public string sims_fee_code { get; set; }

        public bool disabled1 { get; set; }

        public string Doc_number { get; set; }

        public bool inserted { get; set; }
    }

    public class Fin060
    {

        public string gldc_comp_code { get; set; }
        public string gldc_year { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_name { get; set; }
        public string gldc_doc_type { get; set; }
        public string gldc_doc_type_name { get; set; }
        public string gldc_next_prv_no { get; set; }
        public string gldc_next_srl_no { get; set; }
        public string opr { get; set; }
        public string sims_doc_code { get; set; }
        public string sims_doc_name { get; set; }
        public string gldc_comp_name { get; set; }

        public string gldc_doc_short_name { get; set; }

        public string gldc_seq_prefix { get; set; }

        public string gldc_doc_code_type { get; set; }

        public string gldc_doc_report_url { get; set; }
    }

    public class StudentEA
    {

        public string sims_enrollment_number { get; set; }

        public string sims_student_full_name { get; set; }

        public string sims_student_ea_number { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name_en { get; set; }

        public string status_name { get; set; }

        public string opr { get; set; }
    }
    public class Sim098
    {
        public bool sims_has_your_child_any_other_disability_status { get; set; }

        public string sims_has_your_child_any_other_disability { get; set; }

        public string sims_student_health_excuses { get; set; }

        public string sims_student_full_name { get; set; }

        public string sims_health_card_number { get; set; }

        public string sims_health_card_issue_date { get; set; }

        public string sims_health_card_expiry_date { get; set; }

        //public string sims_health_card_issuing_authority { get; set; }

        public string sims_health_card_issuing_authority1 { get; set; }
        
        public string sims_blood_group_code { get; set; }

        public string sims_health_card_issuing_authority { get; set; }

        public string sims_height { get; set; }

        public string sims_wieght { get; set; }

        public string sims_teeth { get; set; }

        public string sims_health_bmi { get; set; }

        public bool sims_medication_status { get; set; }

        public string sims_medication_desc { get; set; }

        public bool sims_disability_status { get; set; }

        public string sims_disability_desc { get; set; }

        public bool sims_health_restriction_status { get; set; }

        public string sims_health_restriction_desc { get; set; }

        public bool sims_health_vision_status { get; set; }

        public string sims_health_vision_desc { get; set; }

        public bool sims_health_other_status { get; set; }

        public string sims_health_other_desc { get; set; }

        public string sims_regular_hospital_name { get; set; }

        public string sims_regular_hospital_phone { get; set; }

        public string sims_regular_doctor_name { get; set; }

        public string sims_regular_doctor_phone { get; set; }

        public bool sims_health_hearing_status { get; set; }

        public string sims_health_hearing_desc { get; set; }

        public string opr { get; set; }

        public string sims_enrollment_number { get; set; }

        public string enroll_number { get; set; }

        public string sims_blood_group_name { get; set; }

        public string icon { get; set; }

        public bool sims_Consent_medication_status { get; set; }

        public string sims_Consent_medication_desc { get; set; }

        public bool sims_Consent_medical_Examination_status { get; set; }

        public string sims_Consent_medical_Examination_desc { get; set; }

        public bool sims_Consent_medical_immunization_status { get; set; }

        public string sims_Consent_medical_immunization_desc { get; set; }

        public bool sims_submitted_vaccination_stauts { get; set; }

        public string sims_submitted_vaccination_desc { get; set; }

        public string sims_medical_remark { get; set; }

        public string sims_medical_excuses_no { get; set; }

        public string sims_medical_excuses_name { get; set; }
    }

    public class hoomroomCreation
    {
        public string sims_grade_section_code { get; set; }
        public string homeroomId { get; set; }
        public string homeroom_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string HomeroomCode { get; set; }
        public string HomeroomName { get; set; }
        public string HomeroomSubject { get; set; }
        public string HomeroomEmployeeCode { get; set; }
        public bool HomeroomStatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string ApprovedBy { get; set; }
        public string opr { get; set; }
        public string ApprovedOn { get; set; }
        public string HomeroomStudentEnrollNumber { get; set; }
        public bool Status { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_employee_code { get; set; }
        public string user_status { get; set; }

        public List<hoomroomStudent> studlist = new List<hoomroomStudent>();
        public List<hoomroomClass> gradelist = new List<hoomroomClass>();
    }

    public class hoomroomStudent
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string HomeroomStudentEnrollNumber { get; set; }
        public bool stud_status { get; set; }
        public bool Status { get; set; }
        public string HomeroomCode { get; set; }
    }

    public class hoomroomClass
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string HomeroomCode { get; set; }
        public bool Status { get; set; }
    }

    public class Sim515
    {
        public string sims_financial_year { get; set; }
        public string sims_financial_year_end_date { get; set; }
        public string opr { get; set; }
        public string sims_financial_year_start_date { get; set; }
        public string sims_financial_year_status { get; set; }
        public string sims_financial_year_status1 { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_financial_year_information { get; set; }
    }

    public class FinnAsttrans
    {
        public string gam_comp_code { get; set; }
        public string gam_comp_code_name { get; set; }

        public string gam_asst_type { get; set; }
        public string gam_asst_type_name { get; set; }

        public string em_number { get; set; }
        public string employee_name { get; set; }

        public string ass_account { get; set; }
        public string ass_accountname { get; set; }

        public string gam_item_no { get; set; }
        public string gam_item_desc { get; set; }
        public string gam_transaction_code { get; set; }
        public string gam_quantity { get; set; }
        public string gam_sell_to { get; set; }
        public string gam_cust_code { get; set; }
        public string gam_transaction_amount { get; set; }
        public string gam_revenue_acno { get; set; }
        public string gam_asset_disposal_acno { get; set; }
        public string gam_created_by { get; set; }
        public string gam_creater_remarks { get; set; }
        public string gam_approved_by { get; set; }
        public string gam_approved_date { get; set; }
        public string gam_approver_remarks { get; set; }
        public string gam_transaction_status { get; set; }
        public string opr { get; set; }
    }

    public class Fin017
    {

        public string fins_financial_period_no { get; set; }

        public string fins_financial_period_name { get; set; }

            public string month_name { get; set; }

        public string month_no { get; set; }
        public string sims_student_name { get; set; }
        public string sims_student_commence_date { get; set; }
        public string fins_receivable_acno_desc { get; set; }
        public string fins_revenue_acno_desc { get; set; }
        public string year_no { get; set; }
        //public int gam_item_no { get; set; }

        public string gam_item_no { get; set; }

        public string gam_desc_1 { get; set; }

        public string gal_asst_type { get; set; }
        public string gal_type_desc { get; set; }

        public string gam_desc_2 { get; set; }

        public string gam_supl_name { get; set; }

        public string gam_acct_code { get; set; }

        public string gam_invoice_no { get; set; }

        public decimal gam_quantity { get; set; }

        public decimal gam_invoice_amount { get; set; }

        public decimal gam_book_value { get; set; }

        public decimal gam_cum_deprn { get; set; }

        public decimal gam_ytd_deprn { get; set; }

        public decimal gam_mth_deprn { get; set; }

        public decimal gam_sale_value { get; set; }

        public decimal gam_repl_cost { get; set; }

        public decimal gam_deprn_percent { get; set; }

        public decimal gam_used_on_item { get; set; }

        public string gam_status { get; set; }

        public string gam_status_name { get; set; }

        public string year1 { get; set; }

        public string year { get; set; }

        //public string fins_year { get; set; }

        public string companyCode { get; set; }



            public string sims_enroll_number { get; set; }
            public string sims_fee_cur_code { get; set; }
            public string sims_fee_academic_year { get; set; }
        public string posting_date { get; set; }
        public string sims_fee_grade_code { get; set; }
            public string sims_grade_name_en { get; set; }
            public string sims_fee_section_code { get; set; }
            public string sims_section_name_en { get; set; }
        public string sims_fee_code { get; set; }
            public string sims_Fee_code_description { get; set; }
            public string period_no { get; set; }
            public string expected_amount { get; set; }
            public string paid_amount { get; set; }
        public string posting_amount { get; set; }
        public string final_doc_number { get; set; }
        public string posted_amount { get; set; }


        public string sims_fee_code_description { get; set; }
        public string sims_fee_period_code { get; set; }
        public string fee_amount { get; set; }
        public string discounted_amount { get; set; }
        public string fee_amount_final { get; set; }
        public string sims_receivable_acno { get; set; }
        public string sims_revenue_acno { get; set; }
        public string sims_discount_acno { get; set; }
        public string sims_adjustment_pstng_date { get; set; }
        public string doc_no { get; set; }

        public string sims_academic_year{ get; set; }
        public string sims_academic_year_description { get; set; }

        public string fins_receivable_acno { get; set; }
        public string fins_revenue_acno { get; set; }


        
    }

    #region Finn055
    //fin055   fins_documents
    public class Finn055
    {
        public string codo_comp_code_name { get; set; }
        public string codo_doc_code { get; set; }
        public string codo_doc_name { get; set; }
        public string codo_doc_type { get; set; }
        public string codo_doc_type_name { get; set; }
        public int codo_st_doc_no { get; set; }
        public int codo_end_doc_no { get; set; }
        public bool codo_dr_cr_flag { get; set; }
        public bool codo_sp_doc_flag { get; set; }

    }
    #endregion

    public class Sim539
    {
        public string sims_agenda_from_time { get; set; }
        public string sims_agenda_to_time { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_agenda_date { get; set; }
        public string sims_agenda_name { get; set; }
        public string sims_agenda_desc { get; set; }
        public string sims_agenda_sdate { get; set; }
        public string sims_agenda_edate { get; set; }
        public string sims_agenda_doc { get; set; }
        public string sims_agenda_day { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_agenda_doc_en { get; set; }
        public string sims_agenda_visible_to_parent_portal { get; set; }
        public string sims_agenda_status { get; set; }
        public string sims_isexpanded { get; set; }
        public string sims_icon { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_agenda_doc_line_no { get; set; }
        public string sims_grade { get; set; }
        public string sims_section { get; set; }

        public string sims_agenda_teacher_code { get; set; }

        public string sims_agenda_stage { get; set; }

        public string sims_agenda_page_no { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string weekno { get; set; }

        public string id { get; set; }

        public string weekdate { get; set; }

        public string weekend { get; set; }
    }


    public class AgendaDate
    {
        public string sims_agenda_date { get; set; }
        public string sims_agenda_teacher_code { get; set; }
        public List<Agenda> sims_agenda_list { get; set; }
    }

    //public class Agenda
    //{
    //}

    public class Agenda
    {
        public string sims_agenda_teacher_code { get; set; }
        public string sims_agenda_from_time { get; set; }
        public string sims_agenda_to_time { get; set; }

        public string sims_agenda_date { get; set; }
        public string sims_agenda_name { get; set; }
        public string sims_agenda_desc { get; set; }
        public string sims_agenda_sdate { get; set; }
        public string sims_agenda_edate { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade { get; set; }
        public string sims_section { get; set; }

        public string sims_agenda_stage { get; set; }
        public string sims_agenda_page_no { get; set; }
        public List<AgendaSubject> sims_agenda_sub_list { get; set; }
    }

    public class AgendaSubject
    {
        public string sims_subject_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_agenda_date { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_agenda_stage { get; set; }
        public string sims_agenda_page_no { get; set; }
        public List<AgendaDoc> sims_agenda_doc_list { get; set; }
    }

    public class AgendaDoc
    {
        public string sims_agenda_doc_en;
        public string sims_subject_code { get; set; }
        public string sims_agenda_date { get; set; }
        public string sims_agenda_number { get; set; }
        public string sims_agenda_doc_desc { get; set; }
        public string sims_agenda_doc { get; set; }
        // public string sims_agenda_doc_en { get; set; }
        public string sims_agenda_stage { get; set; }
        public string sims_agenda_page_no { get; set; }
        public string sims_agenda_doc_line_no { get; set; }
    }
    //Agenda Configuration (Sim540)
    #region 
    public class supervisor_grade_section_subject
    {
        public string sims_supervisor_code { get; set; }
        public string sims_supervisor_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_name { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_head_teacher_name { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_teacher_name { get; set; }
        public string sims_class_teacher_code { get; set; }
        public string sims_class_teacher_name { get; set; }
    }

    public class AgendaConfig
    {
        public string sims_sr_no { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_agenda_assign_type { get; set; }
        public string sims_employee_type { get; set; }
        public string sims_status { get; set; }
        public string sims_supervisor_code { get; set; }
        public string sims_supervisor_name { get; set; }
        public List<supervisor_grade_section_subject> sup_grade = new List<supervisor_grade_section_subject>();
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_agenda_type { get; set; }
        public string sims_agenda_type_name { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_head_teacher_name { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_teacher_name { get; set; }
        public string sims_class_teacher_code { get; set; }
        public string sims_class_teacher_name { get; set; }

        public string tid { get; set; }
        public string gid { get; set; }
        public string sid { get; set; }
        public bool isSelected { get;  set; }
        public bool isSelected_sup { get; internal set; }
    }
    public class supervisor
    {
        public string sims_supervisor_code { get; set; }
        public string sims_supervisor_name { get; set; }
        public List<supervisor_grade> sup_grade { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_head_teacher_name { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_teacher_name { get; set; }
        public string sims_class_teacher_code { get; set; }
        public string sims_class_teacher_name { get; set; }
        public string tid { get; set; }
        public bool isSelected { get;  set; }
    }

    public class supervisor_grade
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public List<supervisor_section> sup_sect { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_supervisor_code { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_class_teacher_code { get; set; }
        public string gid { get; set; }
        public bool isSelected { get;  set; }
    }

    public class supervisor_section
    {
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public List<supervisor_subject> sup_subject { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_supervisor_code { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_head_teacher_name { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_bell_teacher_name { get; set; }
        public string sims_class_teacher_code { get; set; }
        public string sims_class_teacher_name { get; set; }
        public string sid { get; set; }
        public bool isSelected { get;  set; }
    }

    public class supervisor_subject
    {
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public List<supervisor_subject> sup_subject { get; set; }
        public string sims_supervisor_code { get; set; }
        public string sims_section_code { get; set; }

        public string sims_head_teacher_code { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_class_teacher_code { get; set; }
        public bool isSelected { get;  set; }
    }
    #endregion


    public class Fin207
    {
        

        public string acno { get; set; }
        public string fins_fee_academic_year { get; set; }
        public string fins_fee_acno { get; set; }
        public string fins_fee_acno_old { get; set; }
        public string fins_fee_cur_code { get; set; }
        public string fins_fee_cur_name { get; set; }
        public string fins_fee_payment_type { get; set; }
        public string fins_fee_payment_type_desc { get; set; }
        public bool fins_fee_posting_status { get; set; }
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_cur_code { get; set; }
        public string acno_code { get; set; }
        public string fins_fee_advance_acno { get; set; }
        public string acct_name { get; set; }

        public string fins_fee_academic_year_desc { get; set; }

        public string fins_fee_acno_name { get; set; }

        public string fins_fee_advance_acno_name { get; set; }

        public string fins_fee_refund_acno { get; set; }

        public string fins_fee_refund_acno_name { get; set; }
        public string user_name { get; set; }
        public string fins_pdc_realize_acno { get; set; }
        public string fins_pdc_payment_acno { get; set; }

        public string fins_pdc_realize_acct_name { get; set; }

        public string fins_pdc_payment_acct_name { get; set; }
    }
    public class Fin205
    {
        public string acdemic_year { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_name { get; internal set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string comp_short_name { get; set; }
        public string fins_appl_parameter { get; set; }
        public string fins_finance_year { get; set; }
        public string fins_finance_year_desc { get; set; }
        public string gdua_acct_code { get; set; }
        public string gdua_comp_code { get; set; }
        public string gdua_dept_no { get; set; }
        public string gdua_doc_code { get; set; }
        public string gdua_doc_user_code { get; set; }
        public bool gdua_status { get; set; }
        public string gdua_year { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_name { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string opr { get; set; }
        public string user_code { get; set; }
        public string user_name { get; set; }
    }

    public class Fin011
    {
        public string gd_comp_code { get; set; }

        public string gd_division_code { get; set; }
        public string gd_division_desc { get; set; }
        public string opr { get; set; }
    }

    public class Fin138
    {


        public string acno { get; set; }
        public string coad_pty_full_name { get; set; }
        public string glma_acct_name { get; set; }
        public string opr { get; set; }
        public string pb_bank_code { get; set; }
        public string pb_bank_name { get; set; }
        public string pb_comp_code { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_sl_acno { get; set; }
        public string pb_sl_code { get; set; }
        public string pb_srl_no { get; set; }
        public string comp_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string slma_ldgrctl_code1 { get; set; }
        public string pb_gl_name { get; set; }
        public string pb_sl_name { get; set; }
        public string pb_sl_ac_name { get; set; }
        public string comp_name { get; set; }
        public bool pb_posting_flag { get; set; }
            public string glma_acct_name_desc { get; set; }
        public string acno_code { get; set; }
    }
    public class Fin226
    {
        public string opr { get; set; }
        public string glpdc_comp_code { get; set; }
        public string glpdc_year { get; set; }
        public string glpdc_doc_code { get; set; }
        public string glpdc_type { get; set; }
        public string glpdc_type_desc { get; set; }

    }

    public class Fin227
    {
        public string codp_dept_no { get; set; }
        public string codp_dept_name { get; set; }
        public string glpc_number { get; set; }
        public string glpc_comp_code { get; set; }
        public string glpc_year { get; set; }
        public string glpc_dept_no { get; set; }
        public string glpc_acct_code { get; set; }
        public string glma_acct_name { get; set; }
        public string glpc_max_limit { get; set; }
        public string glpc_min_limit { get; set; }
        public string glpc_max_limit_per_transaction { get; set; }
        public string glpc_curr_balance { get; set; }
        public string glpc_creation_date { get; set; }
        public string glpc_creation_user { get; set; }
        public string glpc_authorize_date { get; set; }
        public string glpc_authorize_user { get; set; }
        public string glpc_cur_date { get; set; }
        public bool glpc_cost_center_flag { get; set; }
        public bool glpc_status { get; set; }
        public string opr { get; set; }

        public string glma_acct_full_name { get; set; }
        public string glma_acct_code { get; set; }
        public string glpca_prepare_user { get; set; }

    }

    public class Fin17A
    {
        public string fins_financial_period_name { get; set; }
        public string fins_financial_period_no { get; set; }
    }

    public class FinVoucher
    {

        public string doc_code { get; set; }

        public string final_doc_no { get; set; }

        public string doc_date { get; set; }

        public string ledger_doc_narr { get; set; }

        public string acct_code { get; set; }

        public string acct_name { get; set; }

        public string doc_amount { get; set; }

        public string username { get; set; }
    }
    public class fins054
    {
        public string EmpName;

        public string gldu_user_name { get; set; }
        public bool gldu_prepare { get; set; }
        public bool gldu_verify { get; set; }
        public bool gldu_authorize { get; set; }
        public string gldu_verify_user { get; set; }
        public string gldu_authorize_user { get; set; }
        public string gldu_comp_code { get; set; }
        public string gldu_dept_no { get; set; }
        public string gldu_dept_no_name { get; set; }
        public string gldu_verify_user_name { get; set; }
        public string gldu_authorize_user_name { get; set; }
        public string gldu_user_name_data { get; set; }
        public List<fins054> ListColl { get; set; }

        public string gldu_user_name_old { get; set; }
        public bool gldu_prepare_old { get; set; }
        public bool gldu_verify_old { get; set; }
        public bool gldu_authorize_old { get; set; }
        public string gldu_verify_user_old { get; set; }
        public string gldu_authorize_user_old { get; set; }
        public string gldu_comp_code_old { get; set; }
        public string gldu_dept_no_old { get; set; }
        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string comn_user_name { get; set; }
        public string opr { get; set; }
        public string user_name { get; set; }

        public string gldu_doc_code { get; set; }

        public string gldu_doc_code_name { get; set; }

        public string gldu_doc_year { get; set; }
    }



    public class BankReCon
    {
        public string acct_code { get; set; }
        public string acct_name { get; set; }
        public string doc_no { get; set; }
        public string gdua_acct_code { get; set; }
        public string glbr_acct_code { get; set; }
        public string glbr_bank_date { get; set; }
        public string glbr_cheque_due_date { get; set; }
        public string glbr_cheque_no { get; set; }
        public string glbr_comp_code { get; set; }
        public string glbr_creation_date { get; set; }
        public string glbr_creation_user { get; set; }
        public string glbr_doc_code { get; set; }
        public string glbr_doc_remark { get; set; }
        public string glbr_our_doc_no { get; set; }
        public string glma_acct_name { get; set; }
        public string gltr_acct_code { get; set; }
        public string gltr_comp_code { get; set; }
        public string gltr_doc_code { get; set; }
        public string gltr_doc_narr { get; set; }
        public string gltr_our_doc_no { get; set; }
        public string gltr_pstng_date { get; set; }
        public string gltr_tran_amt { get; set; }
        public string opr { get; set; }
    }


    public class Fin102
    {
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string dept_no { get; set; }
        public string from_date { get; set; }
        public string ldgr_code { get; set; }
        public string ledger_code { get; set; }
        public string ledger_name { get; set; }
        public string opr { get; set; }
        public string pc_amount { get; set; }
        public string pc_bank_from_name { get; set; }
        public string pc_cheque_no { get; set; }
        public string pc_cur_date { get; set; }
        public string pc_dept_no { get; set; }
        public string pc_our_doc_no { get; set; }
        public string pc_ref_no { get; set; }
        public string pc_sl_acno { get; set; }
        public string pc_sl_acno_name { get; set; }
        public string pc_submission_date { get; set; }
        public decimal ps_amount { get; set; }
        public string ps_bank_code { get; set; }
        public string ps_bank_code_name { get; set; }
        public string ps_cheque_no { get; set; }
        public string ps_comp_code { get; set; }
        public string ps_dept_no { get; set; }
        public string ps_discd { get; set; }
        public string ps_due_date { get; set; }
        public string ps_ldgr_code { get; set; }
        public string ps_ldgr_code_name { get; set; }
        public string ps_rec_type { get; set; }
        public bool ps_select { get; set; }
        public string ps_sl_acno { get; set; }
        public string ps_sl_acno_name { get; set; }
        public string slac_ctrl_acno { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string to_date { get; set; }

        public string pdc_from_date { get; set; }
        public string pdc_to_date { get; set; }
        public bool temp_select { get; set; }
        public int cnt { get; set; }
    }

    #region sims150...(Incidence Action)

    public class Sims150
    {
        public string sims_action_category_code { get; set; }
        public string cur_level_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_level_code { get; set; }
        public string sims_incidence_consequence_cur_code { get; set; }
        public string sims_incidence_consequence_cur_level_code { get; set; }

        public string sims_action_code { get; set; }
        public string sims_action_sub_code { get; set; }
        public string sims_action_desc { get; set; }
        public string sims_action_type { get; set; }
        public string sims_action_color_code { get; set; }
        public bool sims_action_escalation_flag { get; set; }
        public bool sims_action_sms_flag { get; set; }
        public bool sims_action_email_flag { get; set; }
        public bool sims_action_portal_flag { get; set; }
        public bool sims_action_roll_over_flag { get; set; }
        public string sims_action_point { get; set; }
        public bool sims_action_status { get; set; }
        public string opr { get; set; }
        public string sims_cur_name { get; set; }
        public string cur_level_name { get; set; }

        public string sims_incidence_action_code_desc { get; set; }

        public string sims_appl_parameter { get; set; }
    }
    #endregion


    public class Dtcn01
    {

        public string sims_detention_level_code { get; set; }

        public string sims_detention_name { get; set; }
        public string sims_detention_name_ot { get; set; }
        public string sims_detention_point { get; set; }
        public string sims_display_order { get; set; }

        public bool sims_detention_level_status { get; set; }
        public string sims_level_created_user_code { get; set; }
        public string opr { get; set; }
        public string sims_level_created_date { get; set; }
        public string sims_user_access_role { get; set; }
        public string comn_role_code { get; set; }
        public string comn_role_name { get; set; }
    }



    public class FINR155
    {
        
        public string gltr_year       { get; set; }
        public string gltr_doc_code   { get; set; }
        public string gltr_our_doc_no { get; set; }
        public string gltr_pstng_date { get; set; }
        public string gltr_doc_narr   { get; set; }
        public string gltr_tran_amt   { get; set; }
        public string print_pv_status { get; set; }

    }


    #region sims152(Consequence)
    public class Sims152
    {

        public string sims_incidence_consequence_code { get; set; }
        public string sims_incidence_consequence_desc { get; set; }
        public string sims_incidence_consequence_type { get; set; }
        public string sims_incidence_consequence_point { get; set; }

        public string cur_level_code { get; set; }
        public string sims_incidence_consequence_cur_code { get; set; }
        public string sims_incidence_consequence_cur_level_code { get; set; }
        public bool sims_incidence_consequence_status { get; set; }


        public string sims_incidence_consequence_cur_name { get; set; }

        public string sims_cur_level_code { get; set; }

        public string cur_level_code_name { get; set; }

        public string opr { get; set; }
    }

    #endregion

    #region sims151(Action Category)
    public class Sims151
    {

        public string sims_incidence_action_code { get; set; }
        public string sims_incidence_action_code_desc { get; set; }
        public bool sims_incidence_action_status { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region sims154(Warning)
    public class Sims154
    {

        public string sims_incidence_warning_desc { get; set; }
        public bool sims_incidence_warning_status { get; set; }
        public string sims_incidence_warning_code { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region sims153(Timeline)
    public class Sims153
    {

        public string sims_incidence_timeline_code { get; set; }
        public string sims_incidence_timeline_desc { get; set; }
        public string sims_incidence_timeline_start_time { get; set; }
        public string sims_incidence_timeline_end_time { get; set; }
        public bool sims_incidence_timeline_status { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims159(Homeroom Batch)
    public class Sims159
    {
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_batch_code { get; set; }
        public string sims_batch_name { get; set; }
        public bool sims_batch_status { get; set; }

        public string sims_homeroom_code { get; set; }

        public string sims_homeroom_name { get; set; }

        public string opr { get; set; }

        public string sims_cur_code { get; set; }
    }
    #endregion

    #region bell_section_subject(Sims060)
    public class Sims060
    {
        public int total_count { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_old_teacher_code { get; set; }
        public string sims_academic_year { get; set; }
        public bool ischange { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string _sims_section_name { get; set; }

        public string sims_grade_code { get; set; }
        public string _sims_grade_name { get; set; }
        public string subcode { get; set; }
        public string sims_bell_slot_group_desc { get; set; }
        public string sims_bell_slot_room_desc { get; set; }

        public string sims_bell_desc { get; set; }

        // public string sims_bell_code { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_gb_teacher_code_name { get; set; }
        // public string sims_bell_lecture_count { get; set; }
        // public string sims_bell_lecture_practical_count { get; set; }
        //public string sims_bell_slot_group { get; set; }
        public string sims_bell_slot_room { get; set; }

        public string sims_bell_group_order { get; set; }
        public int sims_bell_lecutre_count { get; set; }
        public int sims_bell_lecture_per_week { get; set; }
        public int sims_bell_slot_per_lecture { get; set; }
        public int sims_bell_lecture_practical_count { get; set; }
        public int sims_bell_practical_per_week { get; set; }
        public int sims_bell_slot_per_practical { get; set; }
        public string sims_lesson_type { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_bell_slot_group_code { get; set; }
        //public string sims_bell_slot_room_code { get; set; }

        public List<string> _sims_section_promote_code { get; set; }

        //for sims_bell_subject_teacher_prefrence
        public string sims_bell_day_desc { get; set; }
        public string sims_slot_time { get; set; }
        public string sims_cur_level_code { get; set; }
        public string sims_cur_level_name { get; set; }
        public string sims_teacher_preference { get; set; }
        public string sims_subject_preference { get; set; }
        public bool sims_pre_status { get; set; }
        public string sims_grade_section { get; set; }
        public bool sims_sub_status { get; set; }

        public string sims_gb_teacher_code { get; set; }

        public string sims_employee_code { get; set; }

        public string sims_lesson_type_code { get; set; }

        public string academic_year { get; set; }

        public string grade_code { get; set; }

        public string section_code { get; set; }

        public string sims_section_code { get; set; }

        public string opr { get; set; }

        public string sims_bell_code { get; set; }

        public string sims_lession_type1 { get; set; }

        public string sims_bell_slot_group { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string sims_bell_slot_room_code { get; set; }

        public string sims_section_name { get; set; }

        public string sims_grade_name { get; set; }

        public string teacher_code { get; set; }

        public bool sims_bell_section_block { get; set; }

        public string sims_bell_section_block_size { get; set; }
        public string sims_section_subject_term_code { get; set; }

        public string sims_bell_section_term { get; set; }
        public bool sims_is_primary_teacher { get; set; }
    }
    #endregion

    #region section_subject(Sim666)
    public class Sims666 {
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string academicYear { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }
    }
    #endregion


    //Head_Teacher_Mapping
    public class Sims186
    {
        public bool ischecked { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_class_name { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public bool sims_status { get; set; }
        public string sims_section_code { get; set; }
        public string sims_academic_year_desc { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_subject_name { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_section_name { get; set; }

        public string sims_head_teacher_code { get; set; }

        public string sims_head_teacher_name { get; set; }

        public bool sims_head_teacher_status { get; set; }

        public string opr { get; set; }

        public int myid { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string sims_employee_code { get; set; }
         
        public string sims_subject_name_en { get; set; }
    }

    #region Sims187(Supervisior)
    public class Sims187
    {

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name { get; set; }

        public string sims_supervisor_code { get; set; }

        public string sims_supervisor_name { get; set; }

        public bool sims_supervisor_status { get; set; }

        public bool ischecked { get; set; }

        public int myid { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims_Work_Flow
    public class Sims048
    {
        //for sims_work_flow
        public bool select_status { get; set; }
        public string sims_workflow { get; set; }
        public string sims_worfflow_description { get; set; }

        public string sims_workflow_srno { get; set; }
        public string sims_workflow_mod_code { get; set; }
        public string sims_workflow_appl_code { get; set; }
        public string sims_workflow_type { get; set; }
        public string sims_workflow_name { get; set; }

        public string sims_workflow_appl_name { get; set; }
        public string sims_workflow_mod_name { get; set; }
        public string sims_workflow_type_name { get; set; }

        public string sims_workflow_det_empno { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string dg_code { get; set; }
        public string dg_name { get; set; }

        public string sims_workflow_employee_code { get; set; }
        public string sims_workflow_employee_name { get; set; }
        public string sims_workflow_order { get; set; }
        public string sims_workflow_effectivity { get; set; }
        public bool sims_workflow_surpassable { get; set; }
        public bool sims_workflow_status { get; set; }
        public bool sims_workflow_editable { get; set; }
    }
    #endregion

    public class search_app
    {
        public string name { get; set; }
        public string url { get; set; }

        public string comn_appl_name_en { get; set; }
        public string comn_appl_code { get; set; }
        public string comn_mod_name_en { get; set; }
        public string comn_appl_type { get; set; }

        public string comn_mod_code { get; set; }

        public string location { get; set; }

        public string name_only { get; set; }

        public string comn_mod_name_ar { get; set; }

        public string name_only_ar { get; set; }

        public string name_ar { get; set; }
    }

    public class Comn006
    {
        public string comn_user_code { get; set; }
        public string comn_user_appl_code { get; set; }
        public string comn_user_appl_name { get; set; }
        public string comn_user_role_id { get; set; }
        public string comn_user_role_name { get; set; }
        public string comn_user_appl_fav { get; set; }
        public string comn_user_appl_count { get; set; }
        public bool comn_user_read { get; set; }
        public bool comn_user_insert { get; set; }
        public bool comn_user_update { get; set; }
        public bool comn_user_delete { get; set; }
        public bool comn_user_massupdate { get; set; }
        public bool comn_user_admin { get; set; }
        public bool all { get; set; }
        public bool comn_user_appl_status { get; set; }
        public string comn_role_name { get; set; }
        public string module_name { get; set; }
        public string appl_type { get; set; }
        public string comn_role_code { get; set; }

        public string users { get; set; }

        public string comn_user_appl_assigned_by_user_code { get; set; }
    }


    public class Comn019
    {
        #region Comn019
        //Company Variables

        public string fins_comp_code { get; set; }
        public string fins_comp_name { get; set; }
        public string fins_comp_short_name { get; set; }
        public string comp_code { get; set; }
        public string comp_status { get; set; }
        //Grid

        public string comp_code_1 { get; set; }
        public bool comp_status_1 { get; set; }
        public string comp_code_2 { get; set; }
        public bool comp_status_2 { get; set; }
        public string comp_code_3 { get; set; }
        public bool comp_status_3 { get; set; }
        public string comp_code_4 { get; set; }
        public bool comp_status_4 { get; set; }
        public string comp_code_5 { get; set; }
        public bool comp_status_5 { get; set; }
        public string comp_code_6 { get; set; }
        public bool comp_status_6 { get; set; }
        public string comp_code_7 { get; set; }
        public bool comp_status_7 { get; set; }
        public string comp_code_8 { get; set; }
        public bool comp_status_8 { get; set; }
        public string comp_code_9 { get; set; }
        public bool comp_status_9 { get; set; }
        public string comp_code_10 { get; set; }
        public bool comp_status_10 { get; set; }

        public string comp_code_11 { get; set; }
        public bool comp_status_11 { get; set; }
        public string comp_code_12 { get; set; }
        public bool comp_status_12 { get; set; }
        public string comp_code_13 { get; set; }
        public bool comp_status_13 { get; set; }
        public string comp_code_14 { get; set; }
        public bool comp_status_14 { get; set; }
        public string comp_code_15 { get; set; }
        public bool comp_status_15 { get; set; }
        public string comp_code_16 { get; set; }
        public bool comp_status_16 { get; set; }
        public string comp_code_17 { get; set; }
        public bool comp_status_17 { get; set; }
        public string comp_code_18 { get; set; }
        public bool comp_status_18 { get; set; }
        public string comp_code_19 { get; set; }
        public bool comp_status_19 { get; set; }
        public string comp_code_20 { get; set; }
        public bool comp_status_20 { get; set; }


        //Application
        public string fins_comn_user_appl_code { get; set; }
        public string fins_comn_user_appl_name { get; set; }
        //other filds
        public string fins_user_appl_code { get; set; }
        public string fins_user_code { get; set; }
        public string fins_user_name { get; set; }
        public bool comn_user_appl_fins_comp_status { get; set; }
        #endregion

        public string username { get; set; }
    }



    //SectionScreening
    public class Sim038
    {
        public int MyProperty { get; set; }

        public string adm_mark_criteria_code_name { get; set; }

        public string screening_criteria_code_name { get; set; }

        public string screening_criteria_type { get; set; }

        public string screening_grade_code { get; set; }

        public string screening_grade_code_name { get; set; }

        public string screening_cur_code { get; set; }

        public string screening_cur_code_name { get; set; }

        public string screening_academic_year { get; set; }

        public string screening_section_code { get; set; }

        public string screening_section_code_name { get; set; }

        public string screening_marks { get; set; }

        public string screening_rating { get; set; }

        public string screening_grade_sec { get; set; }

        public string opr { get; set; }

        public string screening_criteria_code { get; set; }

        public int myid { get; set; }

        public string sims_grade_code { get; set; }

        public string screening_academic_year_desc { get; set; }


        public string sims_admission_number{ get; set; }
        public string sims_admission_academic_year{ get; set; }
        public string sims_student_attribute12 { get; set; }
        public string full_name{ get; set; }
        public string sims_admission_date{ get; set; }
        public string sims_grade_name_en { get; set; }

        public string sims_cur_code_new{ get; set; }
        public string sims_grade_code_new { get; set; }

        public string sims_cur_code_old{ get; set; }
        public string sims_grade_code_old { get; set; }
        public string sims_mark_assesment_grade { get; set; }
        public string sims_mark_assesment_grade_name { get; set; }
        public string sims_mark_assesment_grade_description { get; set; }
        public string sims_grade_scale { get; set; }


        public string screening_rating_code { get; set; }

        public string screening_rating_group { get; set; }

        public string screening_rating_group1 { get; set; }

        public string sims_survey_rating_group_desc_en { get; set; }

        public string sims_survey_rating_group_code { get; set; }
    }


    //Section(Sim035)
    public class Sim035
    {
        public string sims_section_name { get; set; }

        public string sims_section_code { get; set; }

        public string sims_grade_sec { get; set; }
    }

    #region Sims012(Admission_Criteria)
    public class Sims012
    {
        public int sims_criteria_code { get; set; }
        public string sims_criteria_name_en { get; set; }
        public string sims_criteria_name_ot { get; set; }
        public bool sims_criteria_mandatory { get; set; }
        public string sims_criteria_type { get; set; }
        public string sims_criteria_type_name { get; set; }
        public bool sims_criteria_status { get; set; }
        public string opr { get; set; }
        public string sp_code { get; set; }
        public string sp_name { get; set; }
        public string sp_type { get; set; }
        public string sp_appl_code { get; set; }
        public bool sp_read { get; set; }
        public bool sp_insert { get; set; }
        public bool sp_update { get; set; }
        public bool sp_delete { get; set; }
        public string sp_remark { get; set; }
        public string comn_appl_name_en { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        
    }
    #endregion


    public class Cls10T
    {
        public string sims_student_enroll_number { get; set; }
        public string sims_student_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string section_name { get; set; }
        public string student_name { get; set; }
        public string sims_student_dob { get; set; }
        public string sims_student_nationality_code { get; set; }
        public string nationality_name { get; set; }
        public string sims_student_religion_code { get; set; }
        public string std_religion { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_teacher_code { get; set; }
        public string sibling_detil { get; set; }

        public string sims_bell_section_code { get; set; }
        public string sims_section_name_en { get; set; }

        public string sims_bell_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
    }


    #region Calender
    public class Uccw008
    {
        public bool sims_calendar_event_priority1 { get; set; }
        public string sims_appl_parameter_calender { get; set; }
        public string sims_appl_form_field_value1_calender { get; set; }
        public string sims_appl_form_field_value2_calender { get; set; }
        public string sims_appl_form_field_value3_calender { get; set; }

        public string sims_calendar_number { get; set; }
        public string sims_calendar_date { get; set; }
        public string sims_calendar_event_start_time { get; set; }
        public string sims_calendar_event_end_time { get; set; }
        public string sims_calendar_event_type { get; set; }
        public string sims_calendar_event_category { get; set; }
        public bool sims_calendar_event_priority { get; set; }
        public string sims_calendar_event_short_desc { get; set; }
        public string sims_calendar_event_desc { get; set; }
        public bool sims_calendar_event_status { get; set; }
        public string sims_calendar_time_marker { get; set; }

        public string sims_calendar_event_time { get; set; }
        public string sims_calendar_event_type1 { get; set; }
        public string sims_calendar_event_color { get; set; }
        public string sims_calendar_end_date { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_parameter { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value1 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value2 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value3 { get; set; }



        public string id { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string title { get; set; }

        public string sims_event_color { get; set; }

        public string sims_calendar_event_category_desc { get; set; }

        public string sims_appl_form_field { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string color_code { get; set; }

        public string opr { get; set; }
        public string grade_cur_code { get;  set; }
    }
    #endregion

    #region Sims533 Fee Term
    public class Sims533
    {

        public string sims_fee_term_code { get; set; }
        public string opr { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_term_desc_ar { get; set; }
        public string sims_term_desc_fr { get; set; }
        public string sims_term_desc_ot { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public bool sims_term_status { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_term_cur_short_name_en { get; set; }


        public string sims_academic_year_desc { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }
    }

    public class student_inv
    {
            public string sims_enroll_number{ get; set; }
			public string sims_fee_cur_code{ get; set; }
			public string sims_fee_academic_year{ get; set; }
            public string sims_academic_year_description { get; set; }
            public string sims_cur_short_name_en { get; set; }
            public string student_name { get; set; }
			public string sims_fee_number{ get; set; }
			public string sims_fee_code{ get; set; }
			public string sims_fee_grade_code{ get; set; }
			public string sims_fee_section_code{ get; set; }
			public string expected_amount{ get; set; }
			public string posted_amount{ get; set; }
			public string difference_amount{ get; set; }
			public string sims_fee_period_term{ get; set; }
			public string concession{ get; set; }
			public string sims_grade_name_en{ get; set; }
			public string sims_fee_code_description{ get; set; }
			public string advance_revenue_acno{ get; set; }
			public string fins_revenue_acno{ get; set; }
			public string fins_receivable_acno{ get; set; }
			public string fins_discount_acno{ get; set; }
            public string inv_date { get; set; }
            public string enroll_no_Feecode { get; set; }
            public string opr { get; set; }
            public string user { get; set; }



            public string doc_code { get; set; }

            public string doc_date { get; set; }

            public string finance_year { get; set; }

            public string fins_comp_code { get; set; }

            public string doc_from_date { get; set; }

            public string doc_to_date { get; set; }
    }


    #endregion

    #region sims168 House Allocation
    public class sims168
    {


        public string curr_code { get; set; }
        public string curr_name { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string student_name { get; set; }
        public string enroll { get; set; }
        public string house_code { get; set; }
        public bool house_code1 { get; set; }
        public bool house_code2 { get; set; }
        public bool house_code3 { get; set; }
        public bool house_code4 { get; set; }
        public bool house_code5 { get; set; }
        public bool house_code6 { get; set; }
        public bool house_code7 { get; set; }
        public bool house_code8 { get; set; }
        public bool house_code9 { get; set; }
        public bool house_code10 { get; set; }

        public string color { get; set; }
        public string sims_house_name { get; set; }
        public string house_code_name { get; set; }


        public List<house> house_lst = new List<house>();
    }

    #endregion
    public class house
    {
        public string sims_house_name { get; set; }
        public string house_code { get; set; }
        public bool sims_status { get; set; }



        public string assign_count { get; set; }
    }




    #region Sims029(House)
    public class Sims029 : simsClass
    {
        public string sims_house_color { get; set; }
        public string parent_name { get; set; }
        public string sims_house_objective { get; set; }
       
        public string sims_house_pledge { get; set; }
        public string sims_grade_name_en { get; set; }
        public string house_name { get; set; }
        public string house_code { get; set; }
        //public string sims_grade_name_en { get; set; }

        //public string sims_house_pledge { get; set; }
        //public string sims_grade_name_en { get; set; }
        //public string house_name { get; set; }
        //public string house_code { get; set; }
        //public string sims_grade_name_en { get; set; }
        public string opr { get; set; }

        public string level_cur_code_name { get; set; }

        public string sims_cur_short_name_en { get; set; }
        public string level_cur_code { get; set; }
        public string grade_cur_code_name { get; set; }
        public string level_grade_cur_code_name { get; set; }
        public string subject_cur_code_name { get; set; }
        public string section_fee_cur_code_name { get; set; }

        // public string section_term_cur_name{ get; set; }
        public string screening_cur_code_name { get; set; }
        //public string sims_cur_code{ get; set; }
        public string std_section_cur_code_name { get; set; }
        public string sims_library_item_cur_code { get; set; }
        public string sims_library_item_cur_name { get; set; }

        public string sims_cur_code_name { get; set; }
        public string sims_house_name { get; set; }
        public string sims_house_code { get; set; }
        public bool sims_house_status { get; set; }

        public string sims_parent_number { get; set; }

        public string sims_parent_father_first_name { get; set; }
       
        public string sims_sibling_student_enroll_number { get; set; }

        public string student_name { get; set; }

        public string house_nasupriyame { get; set; }
        //public string sims_parent_number { get; internal set; }
        //public string sims_sibling_student_enroll_number { get; internal set; }
        //public string student_name { get; internal set; }
        //public string house_name { get; internal set; }
        //public string parent_name { get; internal set; }



      

        //public string house_name { get; set; }
    }
    #endregion
    public class color
    {
        public string sims_house_code { get; set; }
        public string sims_house_color { get; set; }

    }

    #region Sim061 Bell
    public class Sim061
    {
        public string opr { get; set; }

        public string sims_bell_academic_year { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_desc { get; set; }
        public bool sims_bell_status { get; set; }

        public string sims_academic_year_description { get; set; }
    }
    #endregion

    #region Learntron

    public class Lrnton
    {
        public string comn_audit_user_name { get; set; }
        public string comn_audit_user_appl_code { get; set; }
        public string comn_audit_ip { get; set; }
        public string comn_audit_dns { get; set; }
        public string comn_audit_remark { get; set; }
    }
    #endregion

    #region Sim064 Bell Day
    public class Sim064
    {
        public string opr { get; set; }



        public string sims_bell_academic_year { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_day_code { get; set; }
        public string sims_bell_day_desc { get; set; }
        public string sims_bell_day_short_desc { get; set; }
        public string sims_bell_day_order { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_bell_desc { get; set; }

    }
    #endregion

    #region Sim065 Bell Details
    public class Sim065
    {
        public string opr { get; set; }

        public string sims_bell_desc { get; set; }
        public string sims_bell_day_desc { get; set; }
        public string sims_bell_academic_year { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_day_code { get; set; }
        public string sims_bell_slot_code { get; set; }
        public bool sims_bell_break { get; set; }
        public string sims_bell_start_time { get; set; }
        public string sims_bell_end_time { get; set; }
        public string sims_bell_slot_desc { get; set; }
        public string sims_academic_year_description { get; set; }
        public bool sims_bell_status { get; set; }

    }
    #endregion


    #region Sim065 Bell Slot Group
    public class Sim062
    {
        public string opr { get; set; }

        public string sims_bell_slot_group { get; set; }
        public string sims_bell_slot_group_desc { get; set; }
        public bool sims_bell_slot_group_status { get; set; }

    }
    #endregion

    #region My Change

    #region SimSTD (Student Behaviour Target Details)

    public class SimSTD
    {
        public string opr { get; set; }
        public string sims_target_code { get; set; }
        public string sims_target_desc { get; set; }
        public string sims_target_code_line_no { get; set; }
        public bool sims_student_behaviour_target_status { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_from_date { get; set; }
        public string sims_upto_date { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_isexpanded { get; set; }
        public string sims_isexpanded1 { get; set; }
        public string sims_icon { get; set; }
        public List<SimSTD> sims_target_code_list { get; set; }
        public List<SimSTD> sims_target_code_line_no_list { get; set; }
    }

    #endregion

    #region Transport Service Classes...

    #region Sim079 (Transport Fees)

    public class Sim079
    {
        public string sims_transport_fee_code_type { get; set; }

        public string sims_transport_code_type { get; set; }

        public string sims_transport_fee_number { get; set; }

        public string sims_transport_code { get; set; }

        public string sims_transport_route_direction_name { get; set; }

        public string sims_transport_route_name { get; set; }

        public string sims_fee_category_description { get; set; }

        public string sims_transport_frequency { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_category { get; set; }
        public string sims_transport_amount { get; set; }
        public string sims_transport_installment_min_amount { get; set; }

        public string sims_fee_code { get; set; }
        public string sims_fee_code_description { get; set; }

        public string opr { get; set; }

        public bool sims_transport_status { get; set; }
        public bool sims_transport_installment_mode { get; set; }
        public bool sims_transport_mandatory { get; set; }
        public bool sims_transport_refundable { get; set; }
        public bool sims_transport_manual_receipt { get; set; }
        public string transport_fee_period1 { get; set; }
        public string transport_fee_period2 { get; set; }
        public string transport_fee_period3 { get; set; }
        public string transport_fee_period4 { get; set; }
        public string transport_fee_period5 { get; set; }
        public string transport_fee_period6 { get; set; }
        public string transport_fee_period7 { get; set; }
        public string transport_fee_period8 { get; set; }
        public string transport_fee_period9 { get; set; }
        public string transport_fee_period10 { get; set; }
        public string transport_fee_period11 { get; set; }
        public string transport_fee_period12 { get; set; }
        public string FeeFrequency { get; set; }
        public string FeeCodeType { get; set; }
    }

    #endregion

    #region Sims081 (Transport Route Student)
    public class Sims081
    {
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_route_fee { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_transport_drop_stop_code { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public string sims_transport_enroll_number { get; set; }
        public string sims_transport_pickup_stop_code { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_route_direction_1 { get; set; }
        public string sims_transport_route_short_name { get; set; }
        public string sims_transport_route_student_stop_lat { get; set; }
        public string sims_transport_route_student_stop_long { get; set; }
        public string sims_transport_stop_code { get; set; }
        public string sims_transport_stop_lat { get; set; }
        public string sims_transport_stop_lat_drop { get; set; }
        public string sims_transport_stop_long { get; set; }
        public string sims_transport_stop_long_drop { get; set; }
        public string sims_transport_route_student_update_status { get; set; }
        public string sims_transport_route_student_update_by { get; set; }
        public string sims_transport_stop_name { get; set; }
        public string sims_transport_route_code1 { get; set; }
        public string ed_exception_id { get; set; }
        public string ed_exception_type { get; set; }
        public string ed_appl_name { get; set; }
        public string ed_db_tbl_name { get; set; }
        public string ed_exception_msg { get; set; }
        public string ed_keycode { get; set; }
        public string ed_reg_date { get; set; }


        //  public bool sims_transport_stop_status { get; set; }
        public string sims_transport_vehicle_seating_capacity { get; set; }
        public string comn_user_name { get; set; }
        public string EmpName { get; set; }
        public string em_mobile { get; set; }
        public string em_email { get; set; }
        public string sims_transport_em_number { get; set; }
        public string sims_transport_effective_from_old { get; set; }
        public string sims_transport_effective_upto_old { get; set; }

        public string sims_transport_route_code_old { get; set; }
        public string sims_transport_route_direction_old { get; set; }


        public string sims_vehicle_seating_capacity { get; set; }
        public string sims_allocated_seat { get; set; }
        public string sims_max_to_exceed_seating_capacity { get; set; }

        public string sub_opr { get; set; }

        public string sims_enroll_number { get; set; }

        public string student_name { get; set; }
        public string sims_parent_number { get; set; }
        public string sims_parent_login_code { get; set; }
        public string parent_name { get; set; }
        public string cmd { get; set; }
        public string username { get; set; }
        public string paswrd { get; set; }
        public string enroll_no { get; set; }
        public string first_name { get;  set; }
        public string card_no { get; set; }
        public string gender { get; set; }
        public string class_id { get; set; }
        public string section_id { get; set; }
        public string dob { get; set; }
        public string trip_type { get; set; }
        public string pick_bs_no_id { get; set; }
        public string pick_shift_id { get; set; }
        public string pick_point_id { get; set; }
        public string drop_bs_no_id { get; set; }
        public string drop_shift_id { get; set; }
        public string drop_point_id { get; set; }
        public string status { get; set; }
        public string roll_no { get; set; }
        public string address1 { get; set; }
        public string father_name { get; set; }
        public string father_mob { get; set; }
        public string email { get; set; }
        public string sims_student_dob { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_father_area_number { get; set; }
        public string sims_student_date { get; set; }
        public string sims_student_transport_status { get; set; }
        public string nationality { get; set; }
        public string sims_student_gender { get; set; }
        public string mother_name { get; set; }
        public string rfid_card { get; set; }
        public string mother_mob { get; set; }
        public string area { get; set; }
        public string adm_date { get; set; }
        public string sims_transport_grade_code { get; set; }
        public string sims_transport_section_code { get; set; }
        public string is_transport { get; set; }
    }

    public class Sims081_Msg
    {
        public string sims_enroll { get; set; }
        public string sims_bus_no { get; set; }

        public string sims_status { get; set; }
        public string sims_msg { get; set; }
    }

    #endregion

    #region Sims083 (Transport Route)
    public class Sims083
    {

        public string opr { get; set; }


        public string sims_academic_year { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_route_direction_name { get; set; }
        public string sims_transport_route_short_name { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_transport_route_vehicle_code { get; set; }
        public string sims_transport_route_vehicle_codename { get; set; }
        public string sims_transport_route_driver_code { get; set; }
        public string sims_transport_route_driver_codename { get; set; }
        public string sims_transport_route_caretaker_code { get; set; }
        public string sims_transport_route_caretaker_codename { get; set; }
        public bool sims_transport_route_status { get; set; }
        public string sims_driver_code { get; set; }
        public string sims_driver_name { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_transport_vehicle_name_plate { get; set; }

        public string sims_transport_vehicle_code { get; set; }

        public string sims_caretaker_name { get; set; }

        public string sims_caretaker_code { get; set; }
    }
    #endregion

    #region Sims084 (Transport Stop)
    public class Sims084
    {
        public string opr { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_transport_stop_code { get; set; }
        public string sims_transport_stop_name { get; set; }
        public string sims_transport_stop_landmark { get; set; }
        public string sims_transport_stop_lat { get; set; }
        public string sims_transport_stop_long { get; set; }
        public bool sims_transport_stop_status { get; set; }

    }
    #endregion

    #region Sims085 (Transport Route Stop)
    public class Sims085
    {
        public string sims_transport_stop_code { get; set; }
        public string sims_transport_stop_name { get; set; }
        public string sims_academic_year { get; set; }

        public string sims_transport_route_code_name { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_route_stop_code { get; set; }
        public string sims_transport_route_stop_codename { get; set; }
        public string sims_transport_route_stop_expected_time { get; set; }
        public string sims_transport_route_stop_waiting_time { get; set; }
        public bool sims_transport_route_stop_status { get; set; }
        public string sims_transport_route_direction_name { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_name { get; set; }
        public string opr { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_transport_enroll_number { get; set; }
        public string sims_transport_academic_year { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public string sims_transport_em_number { get; set; }
    }
    #endregion

    #region Sims086 (Transport Vehicle)
    public class Sims086
    {
        public string opr { get; set; }
        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_vehicle_registration_number { get; set; }
        public string sims_transport_vehicle_name_plate { get; set; }
        public string sims_transport_vehicle_ownership { get; set; }
        public string sims_transport_vehicle_ownership_name { get; set; }
        public string sims_transport_manufacturer_name { get; set; }
        public string sims_transport_vehicle_model_name { get; set; }
        public string sims_transport_vehicle_model_year { get; set; }
        public string sims_transport_vehicle_registration_date { get; set; }
        public string sims_transport_vehicle_registration_expiry_date { get; set; }
        public int sims_transport_vehicle_seating_capacity { get; set; }
        public int sims_transport_vehicle_virtual_seating_capacity { get; set; }
        public string sims_transport_vehicle_power { get; set; }
        public string sims_transport_vehicle_transmission { get; set; }
        public string sims_transport_vehicle_transmission_name { get; set; }
        public string sims_transport_vehicle_color { get; set; }
        public string sims_transport_vehicle_date_of_purchase { get; set; }
        public bool sims_transport_vehicle_camera_enabled { get; set; }
        public bool sims_transport_vehicle_security_enabled { get; set; }
        public bool sims_transport_vehicle_status { get; set; }

        public string sims_transport_vehicle_insurance_no { get; set; }
        public string sims_transport_vehicle_insurance_cmp { get; set; }
        public string sims_transport_vehicle_insurance_issue_date { get; set; }
        public bool vehicle_code_check { get; set; }
        public string sims_transport_vehicle_insurance_expiry_date { get; set; }
    }
    #endregion

    #region Sims087 (Transport Caretaker)
    public class Sims087
    {

        public string opr { get; set; }
        public string sims_caretaker_code { get; set; }
        public string sims_caretaker_email { get; set; }
        public string sims_caretaker_name { get; set; }
        //public string sims_transport_route_caretaker_codename {get; set;}
        public string sims_employee_code { get; set; }
        public string sims_employee_code_name { get; set; }
        public string sims_user_code { get; set; }
        public string sims_user_code_name { get; set; }
        public string sims_caretaker_img { get; set; }
        public string sims_caretaker_img_url { get; set; }
        public string sims_experience_years { get; set; }
        public bool sims_caretaker_status { get; set; }
        public string sims_caretaker_info { get; set; }
        public string emp_full_name { get; set; }
        public string sims_employee_name { get; set; }
        public string comn_user_phone_number { get; set; }
public string em_sex { get; set; }
        public string em_mobile { get; set; }
        public string addressEmployee { get; set; }
        public string em_date_of_birth { get; set; }
        public string em_visa_number { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_expiry_date { get; set; }
        public string em_visa_issuing_place { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_visa_type { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
    }
    #endregion

    #region Sims088 (Transport Driver)
    public class Sims088
    {
        public string sims_driver_gender_name { get; set; }
        public string sims_driver_license_vehicle_mode_name { get; set; }
        public string sims_driver_visa_type_name { get; set; }
        public string opr { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_employee_code_name { get; set; }
        public string sims_user_code_name { get; set; }
        public string sims_user_code { get; set; }
        public string sims_driver_code { get; set; }
        public string sims_driver_name { get; set; }
        public string sims_driver_employee_code { get; set; }
        public string sims_employee_name { get; set; }
        public string sims_driver_user_code { get; set; }
        public string sims_driver_type { get; set; }
        public string sims_driver_type_name { get; set; }
        public string sims_driver_img { get; set; }
        public string sims_driver_img_url { get; set; }
        public string sims_driver_experience_years { get; set; }
        public string sims_driver_gender { get; set; }
        public string sims_driver_address { get; set; }
        public string sims_driver_mobile_number1 { get; set; }
        public string sims_driver_mobile_number2 { get; set; }
        public string sims_driver_mobile_number3 { get; set; }
        public string sims_driver_driving_license_number { get; set; }
        public string sims_driver_date_of_birth { get; set; }
        public string sims_driver_license_issue_date { get; set; }
        public string sims_driver_license_expiry_date { get; set; }
        public string sims_driver_license_place_of_issue { get; set; }
        public string sims_driver_license_vehicle_category { get; set; }
        public string sims_driver_license_vehicle_mode { get; set; }
        public string sims_driver_visa_number { get; set; }
        public string sims_driver_visa_issue_date { get; set; }
        public string sims_driver_visa_expiry_date { get; set; }
        public string sims_driver_visa_issuing_place { get; set; }
        public string sims_driver_visa_issuing_authority { get; set; }
        public string sims_driver_visa_type { get; set; }
        public string sims_driver_national_id { get; set; }
        public string sims_driver_national_id_issue_date { get; set; }
        public string sims_driver_national_id_expiry_date { get; set; }
        public bool sims_driver_status { get; set; }
        public string sims_driver_license_vehicle_categoryname { get; set; }
        public string sims_driver_license_vehicle_modename { get; set; }
        public string sims_driver_gender_code { get; set; }
        public string dg_code { get; set; }
        public string dg_desc { get; set; }
        public string sims_driver_email { get; set; }
        public string comn_user_phone_number { get; set; }
        public string sims_driver_dot_license_no { get; set; }
        public string sims_driver_dot_license_issue_date { get; set; }
        public string sims_driver_dot_license_expiry_date { get; set; }
    }
    #endregion

    #region Sims525 (Update/Cancle Student Transport)
    public class Sims525
    {
        public string sims_png { get; set; }
        public string sims_parent_name { get; set; }
        public string sims_get_status { get; set; }
        public string sims_get_status_en { get; set; }
        public string sims_parent_id { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_transport_vehicle_code_2 { get; set; }
        public string sims_transport_vehicle_name_2 { get; set; }
        public string sims_transport_vehicle_seating_capacity_2 { get; set; }
        public string sims_transport_route_name_2 { get; set; }
        public string sims_transport_route_code_2 { get; set; }
        public string sims_transport_route_direction_2 { get; set; }
        public string sims_transport_route_direction_name_2 { get; set; }
        public string sims_transport_stop_code_2 { get; set; }
        public string sims_transport_stop_name_2 { get; set; }

        public string opr { get; set; }
        public string sub_opr { get; set; }
        public string sims_transport_route_student_code { get; set; }
        public string sims_appl_parameter { get; set; }

        public string sims_student_name { get; set; }
        public string sims_fee_cur_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_transport_enroll_number { get; set; }

        public string sims_transport_condition_code { get; set; }
        public string sims_transport_academic_year { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_fee_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_fee_section_code { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_fee_number { get; set; }
        public string sims_transport_route_student_update_by { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_transport_route_rejecte_date { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_transport_route_direction_1 { get; set; }
        public string sims_transport_pickup_stop_code_1 { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_drop_stop_code_1 { get; set; }
        public string sims_transport_seat_number { get; set; }
        public string sims_transport_pickup_stop_code { get; set; }
        public string sims_transport_drop_stop_code { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }

        public string sims_transport_effective_from_new { get; set; }
        public string sims_transport_effective_upto_new { get; set; }

        public string sims_transport_vehicle_seating_capacity { get; set; }
        public bool sims_transport_route_student_status { get; set; }
        public string V_Name { get; set; }
        public string V_Code { get; set; }
        public string V_Name_Code { get; set; }


        public string sims_fee_period1_paid { get; set; }
        public string sims_fee_period2_paid { get; set; }
        public string sims_fee_period3_paid { get; set; }
        public string sims_fee_period4_paid { get; set; }
        public string sims_fee_period5_paid { get; set; }
        public string sims_fee_period6_paid { get; set; }
        public string sims_fee_period7_paid { get; set; }
        public string sims_fee_period8_paid { get; set; }
        public string sims_fee_period9_paid { get; set; }
        public string sims_fee_period10_paid { get; set; }
        public string sims_fee_period11_paid { get; set; }
        public string sims_fee_period12_paid { get; set; }

        public double sims_paid_total { get; set; }
        public double sims_pending_amount { get; set; }
        public double sims_total_amount { get; set; }

        public string sims_fee_period1 { get; set; }
        public string sims_fee_period2 { get; set; }
        public string sims_fee_period3 { get; set; }
        public string sims_fee_period4 { get; set; }
        public string sims_fee_period5 { get; set; }
        public string sims_fee_period6 { get; set; }
        public string sims_fee_period7 { get; set; }
        public string sims_fee_period8 { get; set; }
        public string sims_fee_period9 { get; set; }
        public string sims_fee_period10 { get; set; }
        public string sims_fee_period11 { get; set; }
        public string sims_fee_period12 { get; set; }



        public string sims_transport_period1 { get; set; }
        public string sims_transport_period2 { get; set; }
        public string sims_transport_period3 { get; set; }
        public string sims_transport_period4 { get; set; }
        public string sims_transport_period5 { get; set; }
        public string sims_transport_period6 { get; set; }
        public string sims_transport_period7 { get; set; }
        public string sims_transport_period8 { get; set; }
        public string sims_transport_period9 { get; set; }
        public string sims_transport_period10 { get; set; }
        public string sims_transport_period11 { get; set; }
        public string sims_transport_period12 { get; set; }
        public string sims_transport_toatal { get; set; }
        public string sims_transport_stop_name { get; set; }
        public string sims_transport_stop_code { get; set; }
        public string sims_transport_effective_upto_old { get; set; }
        public string sims_transport_effective_from_old { get; set; }
        public string sims_transport_pickup_stop_code_old { get; set; }
        public string sims_transport_drop_stop_code_old { get; set; }
        public string sims_transport_route_code_old { get; set; }
        public string sims_transport_route_direction_old { get; set; }

        public bool sims_check_month_wise { get; set; }
        public string pays_employee_name { get; set; }
        public string sims_transport_route_employee_code { get; set; }
        public string sims_transport_em_number { get; set; }
        public string StartMonth { get; set; }
        public string EndtMonth { get; set; }
        public string employee_name { get; set; }
        public string sims_transport_route_employee_stop_lat { get; set; }
        public string sims_transport_route_employee_stop_long { get; set; }

        public string sims_transport_route_stop_code { get; set; }

        public string sims_transport_effective_from1 { get; set; }
        public string sims_transport_effective_upto1 { get; set; }
        public string sims_academic_year_status { get; set; }



        public string sims_academic_year_description { get; set; }

        public string both_amt { get; set; }
        public string sims_transport_vehicle_safe_travel_code { get; set; }
        public string sims_transport_grade_code { get; set; }
        public string sims_transport_section_code { get; set; }
        public string rfid_card { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_student_dob { get; set; }
    }
    #endregion

    #region Sims519 (Approve Student Transport)
    public class Sims519
    {

        public string opr { get; set; }
        public string sims_transport_route_student_code { get; set; }
        public string sims_student_name { get; set; }
        public string sims_fee_cur_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_transport_enroll_number { get; set; }
        public string sims_transport_academic_year { get; set; }
        public string C_direction { get; set; }
        public string sims_transport_route_student_request_by { get; set; }
        public string sims_transport_route_student_request_date { get; set; }
        public string sims_transport_route_approve_by { get; set; }
        public string sims_transport_route_approve_date { get; set; }
        public string sims_transport_route_reject_by { get; set; }
        public string sims_transport_route_reject_reason { get; set; }
        public string sims_transport_route_rejecte_date { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string Available_Seat { get; set; }
        public string Allocated_Seat { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string StartMonth { get; set; }
        public string EndtMonth { get; set; }
        public string sims_transport_pickup_stop_code { get; set; }
        public string sims_transport_drop_stop_code { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_route_name { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public string sims_transport_vehicle_seating_capacity { get; set; }
        public bool sims_transport_route_student_status { get; set; }
    }
    #endregion

    #region SimTVS(Transport Vehicle Service)

    public class SimTVS
    {
        public string opr { get; set; }
        public string sims_transport_maintenance_doc_no { get; set; }
        public string sims_transport_vehicle_registration_number { get; set; }
        public string sims_driver_code { get; set; }
        public string sims_driver_name { get; set; }
        public string sims_transport_maintenance_line_no { get; set; }
        public string sims_transport_maintenance_desc { get; set; }
        public string sims_transport_maintenance_amount { get; set; }
        public bool sims_transport_maintenance_detail_status { get; set; }

        public string sims_transport_vehicle_code { get; set; }
        public string sims_transport_maintenance_date { get; set; }
        public string sims_transport_maintenance_service_station { get; set; }
        public string sims_transport_maintenance_purpose { get; set; }
        public string sims_transport_maintenance_total_amount { get; set; }
        public string sims_transport_maintenance_approve_by { get; set; }
        public string sims_transport_maintenance_driver_code { get; set; }
        public string sims_transport_maintenance_next_date { get; set; }
        public bool sims_transport_maintenance_status { get; set; }
        public string sims_transport_maintenance_Quantity { get; set; }
    }

    #endregion

    #region Fins234 (Vehicle Expense)

    public class Fins234
    {
        public string opr { get; set; }
        public string fvex_transaction_number { get; set; }
        public string sims_created_by { get; set; }
        public string fvex_vechile_expense_date { get; set; }
        public string fvex_vehicle_code { get; set; }
        public string fvex_vehicle_name { get; set; }
        public string fvex_expense_type { get; set; }
        public string fvex_expense_amount { get; set; }
        public string fvex_expense_account_dr_ldgr_code { get; set; }
        public string fvex_expense_account_dr { get; set; }
        public string fvex_expense_account_cr_ldgr_code { get; set; }
        public string fvex_expense_account_cr { get; set; }
        public string fvex_expense_remark { get; set; }
        public string fvex_expense_creation_date { get; set; }
        public string fvex_expense_creation_user { get; set; }
        public string fvex_expense_status { get; set; }
        public string fvex_dd_cheque_number { get; set; }
        public string fvex_dd_cheque_due_date { get; set; }
        public string fvex_dd_cheque_bank_code { get; set; }
        public string fvex_cc_number { get; set; }
        public string fvex_dd_cheque_bank_name { get; set; }

        public string fvex_expense_request_no { get; set; }
    }

    #endregion

    #endregion

    #region SimCMF (Compalint Registration)

    public class SimCMF
    {
        public string opr { get; set; }
        public string sims_complaint_srl_no { get; set; }
        public string sims_complaint_subject { get; set; }
        public string sims_complaint_desc { get; set; }
        public string sims_complaint_status { get; set; }
        public string sims_complaint_registration_date { get; set; }
        public string sims_complaint_register_user_code { get; set; }
        public string sims_complaint_assigned_to_user_code { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name_en { get; set; }

        public string sims_medical_checkup_schedule_srl_no { get; set; }
        public string sims_medical_checkup_schedule_date { get; set; }

        public string sims_special_date { get; set; }
        public string sims_year_date { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public string sims_term_date_en { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_jan_date { get; set; }
        public string sims_feb_date { get; set; }
        public string sims_mar_date { get; set; }
        public string sims_apr_date { get; set; }
        public string sims_may_date { get; set; }
        public string sims_jun_date { get; set; }
        public string sims_jly_date { get; set; }
        public string sims_aug_date { get; set; }
        public string sims_sep_date { get; set; }
        public string sims_oct_date { get; set; }
        public string sims_nov_date { get; set; }
        public string sims_dec_date { get; set; }
        public string sims_date_desc { get; set; }
        public string sims_date_code { get; set; }


        public string sims_enroll_number { get; set; }

        public string sims_medical_visit_date { get; set; }
        public string sims_attendant_observation { get; set; }
        public string sims_followup_comment { get; set; }

        //Medical Term

        public string sims_health_card_number { get; set; }
        public bool sims_health_restriction_status { get; set; }
        public string sims_health_restriction_desc { get; set; }
        public bool sims_medication_status { get; set; }
        public string sims_medication_desc { get; set; }
        public string sims_height { get; set; }
        public string sims_wieght { get; set; }
        public string sims_teeth { get; set; }
        public string sims_health_bmi { get; set; }
        public string sims_health_other_remark { get; set; }
        public string std_name { get; set; }
        public string datastatus { get; set; }

        public string sims_action_desc { get; set; }
        public string sims_action_code { get; set; }

        //public string sims_complaint_status { get; set; }
    }

    #endregion

    #endregion

    #region Inv145(Interest_Type)
    public class Inv145
    {
        public string opr { get; set; }

        public string int_code { get; set; }
        public string int_desc { get; set; }

    }
    #endregion

    public class Invs063
    {
        public string fin_year { get; set; }
        public string fin_year_desc { get; set; }
        public string gl_item_no { get; set; }
        public string gl_accname { get; set; }
        public string depcode { get; set; }
        public string depname { get; set; }
        public string compcode_sub { get; set; }
        public string compname_sub { get; set; }
        public string finyear_sub { get; set; }
        public string revenue_acc_no_name_sub { get; set; }
        public string stock_acc_no_name_sub { get; set; }
        public string cost_acc_no_name_sub { get; set; }
        public string dis_acc_no_name_sub { get; set; }
        public string revenue_acc_no_sub { get; set; }
        public string stock_acc_no_sub { get; set; }
        public string cost_acc_no_sub { get; set; }
        public string dis_acc_no_sub { get; set; }
        public string compcode { get; set; }
        public string compname { get; set; }
        public string finyear { get; set; }
        public string revenue_acc_no_name { get; set; }
        public string stock_acc_no_name { get; set; }
        public string cost_acc_no_name { get; set; }
        public string dis_acc_no_name { get; set; }
        public string revenue_acc_no { get; set; }
        public string stock_acc_no { get; set; }
        public string cost_acc_no { get; set; }
        public string dis_acc_no { get; set; }
        public string catname { get; set; }
        public string opr { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string pc_parent_code { get; set; }
        public string subcatname { get; set; }
        public string pc_category_group { get; set; }
        public string invs_appl_parameter { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public string pc_fin_year_desc { get; set; }
    }

    public class Inv124
    {
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string invs_appl_code { get; set; }
        public string invs_appl_code_old { get; set; }
        public string invs_appl_form_field { get; set; }
        public string invs_appl_form_field_old { get; set; }
        public string invs_appl_form_field_value1 { get; set; }
        public string invs_appl_form_field_value1_old { get; set; }
        public string invs_appl_form_field_value2 { get; set; }
        public string invs_appl_form_field_value2_old { get; set; }
        public string invs_appl_form_field_value3 { get; set; }
        public string invs_appl_form_field_value3_old { get; set; }
        public string invs_appl_form_field_value4 { get; set; }
        public string invs_appl_form_field_value4_old { get; set; }
        public string invs_appl_parameter { get; set; }
        public string invs_appl_parameter_old { get; set; }
        public string invs_comp_code { get; set; }
        public string invs_comp_code_old { get; set; }
        public string opr { get; set; }
    }


    #region Inv027(Purchase_Expenses_Type)
    public class Inv027
    {
        public string opr { get; set; }

        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string pet_expense_acno { get; set; }
        public string pet_expense_acname { get; set; }
        public string glma_ldgr_code { get; set; }
        public string pet_code { get; set; }
        public string pet_desc { get; set; }
        public bool pet_flag { get; set; }
    }
    #endregion

    #region Inv133(Custom_Clearance_Type)
    public class Inv133
    {
        public string opr { get; set; }

        public string ccl_reg_no { get; set; }
        public string ccl_date { get; set; }
        public string sr_shipment_no { get; set; }
        public string ccl_customs_entry_no { get; set; }
        public string ccl_customs_entry_date { get; set; }
        public string ccl_remarks { get; set; }
        public string ccl_bill_of_lading_date { get; set; }
        public string ccl_bill_of_lading { get; set; }

    }
    #endregion

    #region Inv132(create_costing_sheet)
    public class Inv132
    {
        public string opr { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string fa_code { get; set; }
        public string cs_no { get; set; }
        public string fa_name { get; set; }
        public string sup_name { get; set; }
        public string sup_code { get; set; }
        public string loc_code { get; set; }
        public string loc_name { get; set; }
        public string dm_code { get; set; }
        public string dm_name { get; set; }
        public string trt_code { get; set; }
        public string trt_desc { get; set; }
        public string cs_prov_no { get; set; }
        public string cs_date { get; set; }
        public string cs_costaccount { get; set; }
        public string cs_desc { get; set; }
        public string cs_supl_other_charge { get; set; }
        public string cs_forward_agent_charge { get; set; }
        public string cs_remarks { get; set; }
        public string lc_no { get; set; }
        public string ccl_reg_no { get; set; }
        public string cs_payment_due_date { get; set; }
        public string cs_loading_note_no { get; set; }
        public string cs_loading_note_date { get; set; }
        public string cs_received_location { get; set; }
        public string cs_received_by { get; set; }
        public string cur_code { get; set; }
        public string cs_prov_date { get; set; }
        public string cs_authorized_date { get; set; }
    }
    #endregion

    #region Inv134(Custom_Expenses)
    public class Inv134
    {
        public string opr { get; set; }
        public string ccl_reg_no { get; set; }
        public string pet_code { get; set; }
        public string pet_desc { get; set; }
        public string ce_ref_no { get; set; }
        public string ce_amount { get; set; }
        public string ce_ref_date { get; set; }
        public string cc_no { get; set; }
        public string old_pet_code { get; set; }
        public string ccl_date { get; set; }
        public string sr_shipment_no { get; set; }
    }
    #endregion
    #region ot_hour(Overtime_Hour)
    public class ot_hour
    {
        public string emp_name { get; set; }
        public string login_code { get; set; }
        public string comp_code { get; set; }
        public string comp_name { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
        public string desg_code { get; set; }
        public string desg_name { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string emp_id { get; set; }
        public string ot_type { get; set; }
        public string ot_name { get; set; }
        public string year_month { get; set; }
        public string year_month_name { get; set; }
        public string ot_hou { get; set; }
    }
    #endregion
    #region Inv021(Item_Master)
    public class Inv021
    {
        public string Vat { get; set; }
        public double im_Vat_Percentage { get; set; }
        public bool im_one_time_flag { get; set; }
        public bool im_agency_flag { get; set; }
        public bool im_assembly_ind { get; set; }
        public bool im_proprietary_flag { get; set; }
        public bool im_reusable_flag { get; set; }
        public bool im_supersed_ind { get; set; }
        public bool im_obsolete_excess_ind { get; set; }
        public bool im_invest_flag { get; set; }
        public bool im_status_ind { get; set; }


        public string dep_code { get; set; }
        public string pc_fin_year { get; set; }
        public string pc_revenue_acno { get; set; }
        public string pc_stock_acno { get; set; }
        public string pc_cost_acno { get; set; }
        public string pc_discont_acno { get; set; }
        public string im_approximate_lead_time { get; set; }
        public string im_approximate_price { get; set; }
        public string im_average_month_demand { get; set; }
        public string im_creation_date { get; set; }
        public string im_desc { get; set; }
        public string im_economic_order_qty { get; set; }
        public string im_estimated_price { get; set; }
        public string im_img { get; set; }
        public string im_inv_no { get; set; }
        public string im_item_code { get; set; }
        public string im_last_issue_date { get; set; }
        public string im_last_receipt_date { get; set; }
        public string im_last_supp_code { get; set; }
        public string im_last_supp_cur { get; set; }
        public string im_malc_rate { get; set; }
        public string im_malc_rate_old { get; set; }
        public string im_manufact_serial_no { get; set; }
        public string im_mark_up { get; set; }
        public string im_max_level { get; set; }
        public string im_min_level { get; set; }
        public string im_model_name { get; set; }
        public string im_no_of_packing { get; set; }
        public string im_reorder_level { get; set; }
        public string im_rso_qty { get; set; }
        public bool im_sellprice_freeze_ind { get; set; }
        public string im_sellprice_special { get; set; }
        public string im_sell_min_qty { get; set; }
        public string im_sell_price { get; set; }
        public string im_sell_price_including_vat { get; set; }
        public string im_including_vat { get; set; }
        public string im_sell_price_old { get; set; }
        public string im_status_date { get; set; }
        public string im_stock_check_date { get; set; }
        public string im_supl_buying_price { get; set; }
        public string im_supl_buying_price_old { get; set; }
        public string im_supl_catalog_price { get; set; }
        public string im_supl_min_qty { get; set; }
        public string im_supl_price_date { get; set; }
        public string im_trade_cat { get; set; }
        public string opr { get; set; }
        public string pc_code { get; set; }
        public string pc_desc { get; set; }
        public string sec_code { get; set; }
        public string sec_name { get; set; }
        public string sg_desc { get; set; }
        public string sg_name { get; set; }
        public string sup_code { get; set; }
        public string trade_cat_name { get; set; }
        public string uom_code { get; set; }
        public string uom_code_has { get; set; }
        public string uom_name { get; set; }
        public string dep_name { get; set; }
        public string sup_name { get; set; }
        public string uom_name_has { get; set; }
    }
    #endregion

    #region Inv059(Salesman)
    public class Inv059
    {

        public string opr { get; set; }
        public string sm_code { get; set; }
        public string com_code { get; set; }
        public string com_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string sm_password { get; set; }
        public string sm_name { get; set; }
        public string sm_min_comm { get; set; }
        public string sm_max_comm { get; set; }
        public string sm_max_discount { get; set; }
        public string sm_target_level_1 { get; set; }
        public string sm_pct_1_comm { get; set; }
        public string sm_target_level_2 { get; set; }
        public string sm_pct_2_comm { get; set; }
        public string sm_target_level_3 { get; set; }
        public string sm_max_disc_pass { get; set; }
        public bool sm_status { get; set; }
        public string username { get; set; }
    }
    #endregion

    #region Prospect Dashboard Report
    public class prospect
    {
        public string sims_pros_number { get; set; }
        public string sims_pros_sibling_enroll_number { get; set; }
        public string full_student_name { get; set; }
        public string school_remark { get; set; }
        public string parent_remark { get; set; }
        public string sims_created_by { get; set; }
        public string sims_employee_code { get; set; }
        public string sims_pros_status { get; set; }
        public string sims_created_date { get; set; }

    }
    #endregion

    //public class Sims081_Msg
    //{
    //    public string sims_enroll { get; set; }
    //    public string sims_bus_no { get; set; }

    //    public string sims_status { get; set; }
    //    public string sims_msg { get; set; }
    //}

    #region Sims182 CouncilDetails
    public class Sims182
    {
        // internal string sims_cur_short_name_en;

        public string opr { get; set; }
        public string sims_council_code { get; set; }
        public string sims_council_name { get; set; }
        public bool sims_council_status { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_short_name_en { get; set; }
    }

    #endregion

    #region Sims189(StudentHouseCouncil)
    public class Sims189
    {

        public string opr { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_status { get; set; }
        public string sims_stud_name { get; set; }
        public string sims_emp_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_old_enroll_number { get; set; }
        public string sims_student_full_name { get; set; }
        public string sims_house_code { get; set; }
        public string sims_house_name { get; set; }
        public string sims_council_code { get; set; }
        public string sims_council_name { get; set; }
        public string old_sims_house_council_enroll_start_date { get; set; }
        public string sims_house_council_enroll_start_date { get; set; }
        public string sims_house_council_enroll_end_date { get; set; }
        public string sims_user_login_code { get; set; }
        public string old_sims_user_login_code { get; set; }

        //Student Details
        public string sims_student_enroll_number { get; set; }
        public string Student_name { get; set; }
        public string sims_grade_code_name { get; set; }
        public string sims_section_code_name { get; set; }
        public string sims_student_house { get; set; }
        public string sims_student_house_name { get; set; }

        //Employee Details
        public string em_login_code { get; set; }
        public string em_number { get; set; }
        public string EmpName { get; set; }
        public string em_mobile { get; set; }
        public string em_email { get; set; }
        public string em_service_status { get; set; }
        public string em_service_status_name { get; set; }
        public string company_code { get; set; }

        public string em_department { get; set; }
        public string resignedEmpFlag { get; set; }
        public string em_desg_code { get; set; }

        public string afe_grade_code { get; set; }
        public string afe_airport { get; set; }
        public string afe_applicable_frequency { get; set; }
        public string afe_travelling_date { get; set; }
        public string afe_returing_date { get; set; }
        public string afe_reimbursement { get; set; }
        public string afe_nation_code { get; set; }
        public string afe_dest_code { get; set; }
        public bool afe_finalize_status { get; set; }
        public string afe_id { get; set; }
        
    }

    #endregion


    #region
    public class mappingClass
    {
        public string sims_admission_number { get; set; }
        public string name { get; set; }
        public string sims_employee_ID { get; set; }
        public string created_by { get; set; }
        public string @sims_application_pros_adm_status { get; set; }

        public string sims_emp_code_name { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_admission_pros_number { get; set; }
        public string sims_grade { get; set; }

        public string sims_parent_login_code { get; set; }
        public string parent_nm { get; set; }
        public string sims_employee_code { get; set; }
    }
    #endregion

    #region  Sim002(Country)
    public class Sim002
    {
        public string sims_continent { get; set; }
        public string sims_region_code_name { get; set; }
        public string sims_currency_code_name { get; set; }

        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string sims_country_name_ar { get; set; }
        public string sims_country_name_fr { get; set; }
        public string sims_country_name_ot { get; set; }
        public string opr { get; set; }

        public string sims_currency_dec { get; set; }
        public string sims_currency_code { get; set; }
        public string sims_region_code { get; set; }
        public string sims_region_name_en { get; set; }
        public bool sims_status { get; set; }


    }

    #endregion

    #region  Sim006(Ethnicity)
    public class Sim006
    {
        public string sims_ethnicity_code { get; set; }
        public string sims_ethnicity_name_en { get; set; }
        public string opr { get; set; }
        public string sims_ethnicity_name_ar { get; set; }
        public string sims_ethnicity_name_fr { get; set; }
        public string sims_ethnicity_name_ot { get; set; }
        public bool sims_ethnicity_status { get; set; }


    }

    #endregion
    public class ClinicInfo
    {
        public string sims_clinic_code { get; set; }
        public string opr { get; set; }
        public string sims_clinic_name_en {get;set;}
        public string sims_contact_person_name{get;set;}
        public string sims_email_id{get;set;}
        public string sims_doctor_name{get;set;}
        public string sims_address { get; set; }
        public bool sims_clinic_status { get; set; }
}

    #region  Sim007(Language)
    public class Sim007
    {
        public string sims_language_code { get; set; }
        public string sims_language_name_en { get; set; }
        public string opr { get; set; }
        public string sims_language_name_ar { get; set; }
        public string sims_language_name_fr { get; set; }
        public string sims_language_name_ot { get; set; }
        public bool sims_language_status { get; set; }

    }

    #endregion

    #region  Sims055(State)
    public class Sims055
    {

        public string opr { get; set; }
        public string sims_state_code { get; set; }
        public string sims_country_code { get; set; }
        public string sims_country_code_name { get; set; }
        public string sims_state_name_en { get; set; }
        public string sims_state_name_ar { get; set; }
        public string sims_state_name_fr { get; set; }
        public string sims_state_name_ot { get; set; }
        public bool sims_status { get; set; }

        public string sims_country_name_en { get; set; }


    }

    #endregion

    #region Sim056 CityDetails
    public class Sim056
    {

        public string opr { get; set; }
        public string sims_city_code { get; set; }
        public string sims_state_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_state_name { get; set; }
        public string sims_city_name_en { get; set; }
        public string sims_city_name_ar { get; set; }
        public string sims_city_name_fr { get; set; }
        public string sims_city_name_ot { get; set; }
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string sims_state_name_en { get; set; }
    }

    #endregion


    public class otp
    {
        public string sims_otp { get; set; }

        public string sims_otp_status { get; set; }

        public string opr { get; set; }
    }

    #region Sims057(Region)
    public class Sims057
    {
        public string opr { get; set; }
        public string sims_region_code { get; set; }
        public string sims_region_name_en { get; set; }
        public string sims_region_name_ar { get; set; }
        public string sims_region_name_fr { get; set; }
        public string sims_region_name_ot { get; set; }
        public bool sims_status { get; set; }

    }
    #endregion

    #region Inv008(Supplier Department)
    public class Inv008
    {

        public string opr { get; set; }
        public string sup_code { get; set; }
        public string sup_name { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }


    }
    #endregion

    #region Per254(Define_Category)
    public class Per254
    {
        public string opr { get; set; }
        public string ca_code { get; set; }
        public string ca_company_code { get; set; }
        public string comp_name { get; set; }
        public string ca_desc { get; set; }
        public bool ca_process_tag { get; set; }
        public string desg_company_code { get; set; }
        public string desg_company_code_name { get; set; }
        public string ca_company_name { get; set; }
    }
    #endregion

    #region Per244(Common Code)
    public class Per244
    {
        public string opr { get; set; }
        public string srno { get; set; }
        public string cl_code { get; set; }
        public string cl_desc { get; set; }
        public string cl_change_desc { get; set; }
        public bool cl_carry_forward { get; set; }
        public string cl_leave_document_code { get; set; }
        public List<comn_details> cmn_det { get; set; }

    }

    public class comn_details
    {
        public string applicable_from { get; set; }
        public string srno { get; set; }

        public string applicable_to { get; set; }
        public string applicable_value { get; set; }
    }

    #endregion

    #region Pers137(Overtime_Hour)
    public class Pers137
    {
        public string opr { get; set; }
        public string noh_dept_code { get; set; }
        public string codp_dept_name { get; set; }
        public string noh_grade_code { get; set; }
        public string gr_desc { get; set; }
        public string noh_emp_id { get; set; }
        public string em_number { get; set; }
        public string noh_emp_name { get; set; }
        public string noh_company_code { get; set; }
        public string company_name { get; set; }
        public string noh_ot_hou { get; set; }
        public string noh_year_month { get; set; }
        public string noh_ot_type { get; set; }
        public string noh_ot_type_name { get; set; }
    }
    #endregion

    #region Per138(Overtime_Rate)
    public class Per138
    {

        public string opr { get; set; }


        public string fm_code { get; set; }
        public string fm_desc { get; set; }
        public string dr_company_code { get; set; }
        public string dr_company_name { get; set; }
        public string dr_dept_code { get; set; }
        public string dr_dept_name { get; set; }
        public string dr_grade_code { get; set; }
        public string dr_grade_name { get; set; }
        public string dr_ot_formula { get; set; }
        public string dr_ot_formula_name { get; set; }
        public string dr_ot_rate { get; set; }
        public string dr_holiday_ot_formula { get; set; }
        public string dr_holiday_ot_formula_name { get; set; }
        public string dr_holiday_ot_rate { get; set; }
        public string dr_special_ot_formula { get; set; }
        public string dr_special_ot_formula_name { get; set; }
        public string dr_special_ot_rate { get; set; }
        public string dr_special_holi_ot_formula { get; set; }
        public string dr_special_holi_ot_formula_name { get; set; }
        public string dr_special_holi_ot_rate { get; set; }
        public string dr_travel_rate { get; set; }
    }
    #endregion

    #region Per058(Pay Code)
    public class Per058
    {

        public string opr { get; set; }

        public string pc_code { get; set; }
        public string company_name { get; set; }
        public string pc_company_code { get; set; }

        public string dedn_tag { get; set; }
        public string pc_earn_dedn_tag { get; set; }
        public string cp_desc { get; set; }
        public string cp_change_desc { get; set; }
        public int cp_display_order { get; set; }
        public bool pc_company_grade_tag { get; set; }

        public string pc_citi_exp_tag { get; set; }

    }
    #endregion

    #region Per066(Pay_Code_Details)
    public class Per066
    {
        public string pd_company_code { get; set; }
        public string company_name { get; set; }

        public string opr { get; set; }

        public string pd_grade_code { get; set; }
        public string pd_grade_code_nm { get; set; }

        public string pd_pay_code { get; set; }
        public string pd_pay_code_nm { get; set; }
        public string pd_formula_code { get; set; }
        public string pd_formula_nm { get; set; }

        public string pd_credit_acno { get; set; }
        public string pd_credit_acno_nm { get; set; }

        public string pd_debit_acno { get; set; }
        public string pd_debit_acno_nm { get; set; }
        public bool pd_percent_amount_tag { get; set; }
        public bool pd_daily_hourly_tag { get; set; }
        public string pd_amount { get; set; }


    }
    #endregion

    #region Sim163(Location)
    public class Sim163
    {
        public string opr { get; set; }

        public string sims_location_code { get; set; }
        public string sims_location_desc { get; set; }

        public bool sims_location_status { get; set; }

    }
    #endregion

    #region Sim685(Grade Scale)
    public class Sim685
    {
        public string opr { get; set; }

        

        public bool sims_location_status { get; set; }


        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_mark_assesment_grade_name { get; set; }

        public string sims_mark_assesment_grade_code { get; set; }

        public string grade_code { get; set; }
    }
    #endregion

    #region Sim162(Building)
    public class Sim162
    {
        public string opr { get; set; }
        public string sims_building_code { get; set; }
        public string sims_building_desc { get; set; }
        public string sims_buiding_student_capacity { get; set; }

    }
    #endregion

    #region Per130(Loan_Amount)
    public class Per130
    {

        public string opr { get; set; }
        public string la_grade_code { get; set; }
        public string GradeName { get; set; }
        public string la_no_of_installment { get; set; }

        public string la_company_code { get; set; }
        public string la_loan_code { get; set; }
        public string la_debit_acno { get; set; }
        public string la_ledger_code { get; set; }
        public string la_amount { get; set; }
        public string la_comp_emp_tag { get; set; }
        public string CompanyName { get; set; }
        public string LoanName { get; set; }
        public string LedgerName { get; set; }
        public string DebitAccName { get; set; }
    }
    #endregion

    #region Pers323(gratuity Criteria)
    public class Pers323
    {
        public int srno { get; set; }

        public string grt_criteria_code { get; set; }

        public string grt_criteria_name { get; set; }

        public int grt_criteria_min_days { get; set; }

        public string gr_desc { get; set; }

        public string grt_acct_code { get; set; }

        public string grt_bank_acct_code { get; set; }

        public string grt_cash_acct_code { get; set; }

        public string grt_airfare_acct_code { get; set; }

        public bool grt_status { get; set; }

        public string opr { get; set; }

        public string grt_pay_grade { get; set; }

        public string gr_code { get; set; }

        public string grt_acct_name { get; set; }

        public string grt_bank_acct_name { get; set; }

        public string grt_cash_acct_name { get; set; }

        public string grt_airfare_acct_name { get; set; }

        public int year { get; set; }

        public int month { get; set; }

        public int days { get; set; }
    }
    #endregion

    #region Finn003
    public class Finn003
    {
        public string glac_acct_code { get; set; }

        public string comp_inter_comp_acct { get; set; }

        public string comp_against_inter_comp_acct { get; set; }

        public string glac_acct_name { get; set; }
    }
    #endregion

    #region Pers324(gratuity Calculation)
    public class Pers324
    {

        public string grt_criteria_code { get; set; }

        public string grt_criteria_name { get; set; }

        public string grt_applicable_days { get; set; }

        public string grt_working_days { get; set; }
        public bool grt_status { get; set; }

        public string opr { get; set; }

    }
    #endregion
    #region Per131(Loan_Code)
    public class Per131
    {
        // public string LedgerName { get; set; }
        public string lc_ledger_code { get; set; }
        public string opr { get; set; }



        public string lc_company_code { get; set; }
        public string lc_desc { get; set; }
        public string lc_debit_acno { get; set; }
        public string lc_ac_type { get; set; }
        public string CompanyName { get; set; }
        public string lc_code { get; set; }
        public string lc_debit_acno_nm { get; set; }
        public string ledgerName { get; set; }
    }
    #endregion

    #region Per134(Loan_register)
    public class Per134
    {
        public string Loan_number { get; set; }
        public string opr { get; set; }
        public string LoanName { get; set; }
        public string lo_code { get; set; }
        public string EmployeeName { get; set; }
        public string lo_number { get; set; }
        public string DESIGNATION { get; set; }
        public string lo_gaunt_desg { get; set; }
        public string lo_gaunt_address1 { get; set; }
        public string em_first_name { get; set; }
        public string CompanyName { get; set; }
        public string lo_company_code { get; set; }
        public string lo_sanction_date { get; set; }
        public string lo_amount { get; set; }
        public string lo_paid_amount { get; set; }
        public bool lo_fully_rec_tag { get; set; }
        public bool lo_monthly_rec_tag { get; set; }
        public string lo_no_of_installment { get; set; }
        public string lo_repaid_installment { get; set; }
        public bool lo_mgr_confirm_tag { get; set; }
        public string lo_gaunt_no { get; set; }
        public string lo_reference { get; set; }
        public string lo_gaunt_name { get; set; }
        public string lo_gaunt_address2 { get; set; }
        public string lo_gaunt_comp_name { get; set; }
        public string lo_gaunt_address3 { get; set; }
        //  public string employeeName { get;  set; }
    }
    #endregion

    #region Per132(Loan_Confirmation)
    public class Per132
    {
        public string CompanyName { get; set; }
        public string EmployeeName { get; set; }
        public string lm_amount { get; set; }
        public string lm_company_code { get; set; }
        public string lm_loan_code { get; set; }
        public string lm_mgr_confirm_tag { get; set; }
        public string managerConfirmTag { get; set; }
        public string lm_number { get; set; }
        public string lm_sanction_date { get; set; }
        public string lm_year_month { get; set; }
        public string LoanName { get; set; }
        public string opr { get; set; }
    }
    #endregion


   
   
    #region Per135(Mngr_Confirmation)
    public class Per135
    {
        public string CompanyName { get; set; }

        //public string EmployeeName { get; set; }
        public string ca_company_code_name { get; set; }
        public string mc_number { get; set; }
        public string employee_name { get; set; }
        public string employeeName { get; set; }
        public string mc_company_code { get; set; }
        public string mc_old_basic { get; set; }
        public string mc_new_basic { get; set; }
        public bool mc_mgr_confirm_tag { get; set; }
        public string mc_effect_from { get; set; }
        public string mc_retro_effect_from { get; set; }

        public string opr { get; set; }
        public string co_name { get; set; }
    }
    #endregion

    #region fins118(Fins_Schedule)
    public class fins118
    {
        public string company_full_name { get; set; }
        public string company_name { get; set; }
        public string glsc_comp_code { get; set; }
        public string glsc_sched_code { get; set; }
        public string glsc_sched_name { get; set; }
        public string glsc_year { get; set; }
        public string glsc_year_code { get; set; }
        public string opr { get; set; }
        public string glsg_group_code { get; set; }
        public string glsg_group_description { get; set; }
        public bool gsgd_sign { get; set; }
        public string glsc_sched_grp_code { get; set; }

    }
    #endregion

    #region FINR15(PrintVoucher)
    public class FINR15
    {
        public string gltd_doc_code { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_post_date { get; set; }
        public string pty_name { get; set; }
        public string voucher_amt { get; set; }

        public string fins_appl_form_field_value1 { get; set; }

        public string gldc_doc_short_name { get; set; }

        
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date  { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_doc_date { get; set; }
        public string print_pv_status { get; set; }
        public string print_cheque_status { get; set; }
       

    }
    #endregion


    public class feeposfeetype
    {
         public string fins_vat_posting_acno { get; set; }
         public string fins_vat_posting_acno_name { get; set; }
        public string vat_ac_status { get; set; }

        public string sims_fee_cur_code { get; set; }
         public string sims_fee_academic_year{ get; set; }
                
         public string sims_fee_code{ get; set; }
         public string sims_fee_code_description{ get; set; }
                 
         public string fins_revenue_acno{ get; set; }
         public string revenue_acno_name{ get; set; }
                 
         public string fins_receivable_acno{ get; set; }
         public string receivable_acno_name{ get; set; }
                 
         public string fins_discount_acno{ get; set; }
         public string discount_acno_name{ get; set; }

         public string fins_curr_advance_received_acno{ get; set; }
         public string fins_curr_adv_receivable_acno { get; set; }
                 
         public string fins_advance_academic_year_acno{ get; set; }
         public string advance_academic_year_acno_name { get; set; }

         public bool fins_fees_posting_status { get; set; }
       
       

        public string fins_curr_advance_received_acno_jan { get; set; }
        public string fins_curr_advance_received_acno_feb { get; set; }
        public string fins_curr_advance_received_acno_mar { get; set; }
        public string fins_curr_advance_received_acno_apr { get; set; }
        public string fins_curr_advance_received_acno_may { get; set; }
        public string fins_curr_advance_received_acno_jun { get; set; }
        public string fins_curr_advance_received_acno_jul { get; set; }
        public string fins_curr_advance_received_acno_aug { get; set; }
        public string fins_curr_advance_received_acno_sep { get; set; }
        public string fins_curr_advance_received_acno_oct { get; set; }
        public string fins_curr_advance_received_acno_nov { get; set; }
        public string fins_curr_advance_received_acno_dec { get; set; }

        public string fins_receivable_acno_jan { get; set; }
        public string fins_receivable_acno_feb { get; set; }
        public string fins_receivable_acno_mar { get; set; }
        public string fins_receivable_acno_apr { get; set; }
        public string fins_receivable_acno_may { get; set; }
        public string fins_receivable_acno_jun { get; set; }
        public string fins_receivable_acno_jul { get; set; }
        public string fins_receivable_acno_aug { get; set; }
        public string fins_receivable_acno_sep { get; set; }
        public string fins_receivable_acno_oct { get; set; }
        public string fins_receivable_acno_nov { get; set; }
        public string fins_receivable_acno_dec { get; set; }






        public string opr { get; set; }


        public string fins_curr_advance_received_acno_jan_name { get; set; }

        public string fins_curr_advance_received_acno_feb_name { get; set; }

        public string fins_curr_advance_received_acno_mar_name { get; set; }

        public string fins_curr_advance_received_acno_apr_name { get; set; }

        public string fins_curr_advance_received_acno_may_name { get; set; }

        public string fins_curr_advance_received_acno_jun_name { get; set; }

        public string fins_curr_advance_received_acno_jul_name { get; set; }

        public string fins_curr_advance_received_acno_aug_name { get; set; }

        public string fins_curr_advance_received_acno_sep_name { get; set; }

        public string fins_curr_advance_received_acno_oct_name { get; set; }

        public string fins_curr_advance_received_acno_nov_name { get; set; }

        public string fins_curr_advance_received_acno_dec_name { get; set; }

        public string fins_receivable_acno_jan_name { get; set; }

        public string fins_receivable_acno_feb_name { get; set; }

        public string fins_receivable_acno_mar_name { get; set; }

        public string fins_receivable_acno_apr_name { get; set; }

        public string fins_receivable_acno_may_name { get; set; }

        public string fins_receivable_acno_jun_name { get; set; }

        public string fins_receivable_acno_jul_name { get; set; }

        public string fins_receivable_acno_aug_name { get; set; }

        public string fins_receivable_acno_sep_name { get; set; }

        public string fins_receivable_acno_oct_name { get; set; }

        public string fins_receivable_acno_nov_name { get; set; }

        public string fins_receivable_acno_dec_name { get; set; }
    }


    public class TrialBalance
    {
       public string  acct_name	{ get; set; }
       public string  glma_acct_code	{ get; set; }
       public string  glma_dept_no	{ get; set; }
       public string  codp_dept_name	{ get; set; }
       public string  codp_short_name	{ get; set; }
       public string  glma_op_bal_amt_dr	{ get; set; }
       public string  glma_op_bal_amt_cr	{ get; set; }
       public string  glac_sched_code	{ get; set; }
       public string  glsc_sched_name	{ get; set; }
       public string  gsgd_group_code	{ get; set; }
       public string  glsg_group_description	{ get; set; }
       public string  transaction_amt_cr	{ get; set; }
       public string  transaction_amt_dr	{ get; set; }
       public string  closing_balance_dr	{ get; set; }
       public string  closing_balance_cr	{ get; set; }
       public string parent_name { get; set; }


       public string glsg_group_code { get; set; }

       public string glsc_sched_code { get; set; }


       public string gltr_comp_code{ get; set; }
       public string gltr_year{ get; set; }
       public string gltr_dept_no{ get; set; }
       public string gltr_acct_code{ get; set; }
       public string glma_acct_name{ get; set; }
       public string gltr_pstng_date{ get; set; }
       public string gltr_doc_code{ get; set; }
       public string gltr_our_doc_no{ get; set; }
       public string gltr_doc_narr{ get; set; }
       public string debit{ get; set; }
       public string credit{ get; set; }




       public string glsg_comp_code { get; set; }

       public string glsg_group_type { get; set; }

       public string glsg_group_ind { get; set; }

       public string glsg_parent_group { get; set; }

       public string glpr_bud_amt { get; set; }

       public string budget_variance { get; set; }

       public string budget_percentage { get; set; }

       public string glac_display_acct_code { get; set; }
    }


    #region SimTCA (Transport Configure)

    public class SimTCA
    {
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_module_code { get; set; }
        public string sims_module_name { get; set; }
        public string sims_application_code { get; set; }
        public string sims_application_name { get; set; }
        public string sims_academic_year_description { get; set; }
    }

    #endregion



    #region Fin143(Subledger_Matching)
    public class Fin143
    {
        public string gldc_doc_type { get; set; }
        public string sltr_doc_code { get; set; }

        public string gltd_comp_code { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string sltr_slmast_acno { get; set; }
        public string sltr_slmast_acname { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }


        public string sltr_pstng_date { get; set; }
        public string sltr_our_doc_no { get; set; }
        public string sltr_dept_no { get; set; }
        public string sltr_ldgr_code { get; set; }
        public bool sltr_match_ind { get; set; }

        public string sltr_comp_code { get; set; }
        public string sltr_pstng_no { get; set; }
        public string sltr_pstng_seq_no { get; set; }
        public string sltr_year { get; set; }
        public string sltr_tran_amt { get; set; }
        public string sltr_tran_amt_credit { get; set; }
        public string sltr_tran_amt_debite { get; set; }

        public string opr { get; set; }
        public string codp_dept_name { get; set; }
        public string coad_pty_short_name { get; set; }
        public string gldc_doc_name { get; set; }
    }
    #endregion

    #region fins069(Fins_ledgrControl)
    public class fins069
    {
        public string company_name { get; set; }

        public string sllc_match_type_desc { get; set; }
        public string sllc_match_type { get; set; }

        public string opr { get; set; }
        public string sllc_earlst_pstng_date { get; set; }
        public string sllc_ldgr_type { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string sllc_ldgr_code { get; set; }
        public string sllc_year { get; set; }
        public string sllc_comp_code { get; set; }
        public string company_name1 { get; set; }
        public string sllc_prd_no { get; set; }
        public string sllc_password { get; set; }
        public bool sllc_curcy_flag { get; set; }
        public bool sllc_yob_flag { get; set; }
        public bool sllc_qty_flag { get; set; }
        public string ldgr_name { get; set; }
        public string slac_ldgr_code { get; set; }
        public string slac_ctrl_acno { get; set; }



        public string glma_acct_name { get; set; }
        public string slac_comp_code { get; set; }
        public string slac_year { get; set; }
        public string slac_cntrl_class { get; set; }
        public string slac_cntrl_class_desc { get; set; }
        public string slac_sales_clr_acno { get; set; }
        public string old_slac_cntrl_class { get; set; }
    }
    #endregion

    #region fins108(Fins_Payment Term)
    public class fins108
    {
        public string opr { get; set; }
        public string company_name { get; set; }
        public string PT_COMP_Code { get; set; }
        public string PT_TERM_NAME { get; set; }
        public string PT_NO_OF_DAYS { get; set; }
        public bool PT_ENABLED { get; set; }
        public string org_PT_TERM_NAME { get; set; }
    }
    #endregion

    ////for Employee_profile
    #region Per099(Employee_Profile)
    public class Per099
    {

        public string opr { get; set; }
        public string em_login_code { get; set; }
        public string em_number { get; set; }
        public string em_company_name { get; set; }
        public string em_company_code { get; set; }
        public string em_Deptt_Code { get; set; }
        public string em_dept_name { get; set; }
        public string em_dept_effect_from { get; set; }
        public string em_desg_code { get; set; }
        public string em_desg_name { get; set; }
        public string em_Dest_Code { get; set; }
        public string em_nation_name { get; set; }
        public string em_Grade_Code { get; set; }
        public string em_grade_name { get; set; }


        public string em_grade_effect_from { get; set; }
        public string em_Salutation_Code { get; set; }
        public string em_salutation { get; set; }
        public string em_first_name { get; set; }
        public string em_middle_name { get; set; }
        public string em_last_name { get; set; }

        public string em_full_name { get; set; }
        public string em_family_name { get; set; }
        public string em_name_ot { get; set; }

        public string em_date_of_birth { get; set; }
        public string em_Sex_Code { get; set; }
        public string em_sex { get; set; }
        public string em_marital_status { get; set; }
        public string em_Marital_Status_Code { get; set; }
        public string em_Religion_code { get; set; }
        public string em_religion_name { get; set; }
        public string em_ethnicity_code { get; set; }
        public string em_ethnicity_name { get; set; }
        public string em_apartment_number { get; set; }
        public string em_building_number { get; set; }
        public string em_street_number { get; set; }
        public string em_area_number { get; set; }
        public string em_summary_address { get; set; }
        public string em_summary_address_local_language { get; set; }

        public string em_city_code { get; set; }
        public string em_city { get; set; }
        public string em_state_code { get; set; }
        public string em_state { get; set; }
        public string em_Country_Code { get; set; }
        public string em_country { get; set; }
        public string em_Nation_Code { get; set; }
        public string em_service_status_code { get; set; }
        public string em_phone { get; set; }
        public string em_mobile { get; set; }
        public string em_email { get; set; }
        public string em_fax { get; set; }
        public string em_po_box { get; set; }
        public string em_img { get; set; }
        public string em_dependant_full { get; set; }



        public string em_dependant_half { get; set; }
        public string em_dependant_infant { get; set; }
        public string em_emergency_contact_name1 { get; set; }
        public string em_emergency_contact_name2 { get; set; }
        public string em_emergency_contact_number1 { get; set; }
        public string em_emergency_contact_number2 { get; set; }
        public string em_date_of_join { get; set; }
        public string em_service_status { get; set; }
        public string em_staff_type { get; set; }
        public string em_Staff_Type_Code { get; set; }
        public string em_bank_cash_tag { get; set; }
        public string em_ledger_ac_no { get; set; }
        public string em_gpf_ac_no { get; set; }
        public string em_gosi_ac_no { get; set; }


        public string em_gosi_start_date { get; set; }
        public string em_pan_no { get; set; }
        public string em_iban_no { get; set; }
        public string em_labour_card_no { get; set; }
        public string em_route_code { get; set; }
        public string em_status { get; set; }
        public string em_status_code { get; set; }
        public bool em_stop_salary_indicator { get; set; }
        public string em_leave_resume_date { get; set; }
        public bool em_leave_tag { get; set; }
        public bool em_citi_exp_tag { get; set; }
        public string em_joining_ref { get; set; }
        public string em_bank_code { get; set; }
        public string em_bank_name { get; set; }


        public string em_bank_ac_no { get; set; }
        public string em_bank_swift_code { get; set; }
        public string em_left_date { get; set; }
        public string em_left_reason { get; set; }
        public bool em_handicap_status { get; set; }
        public string em_leave_start_date { get; set; }
        public string em_leave_end_date { get; set; }
        public string em_cl_resume_date { get; set; }
        public string em_leave_resume_ref { get; set; }
        public string em_over_stay_days { get; set; }
        public string em_stop_salary_from { get; set; }
        public string em_under_stay_days { get; set; }
        public string em_unpaid_leave { get; set; }
        public bool em_punching_status { get; set; }


        public string em_punching_id { get; set; }
        public string em_passport_no { get; set; }
        public string em_passport_issue_date { get; set; }
        public string em_pssport_exp_rem_date { get; set; }
        public string em_passport_exp_date { get; set; }
        public string em_passport_ret_date { get; set; }
        public string em_passport_issuing_authority { get; set; }
        public string em_passport_issue_place { get; set; }
        public string em_visa_no { get; set; }
        public string em_visa_type { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_exp_date { get; set; }
        public string em_visa_exp_rem_date { get; set; }
        public string em_visa_issuing_place { get; set; }

        public string em_national_id { get; set; }
        public string em_visa_issuing_authority { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string em_modified_on { get; set; }
        public string em_last_login { get; set; }
        public string em_secret_question_code { get; set; }
        public string em_secret_answer { get; set; }
        public bool em_agreement { get; set; }
        public string em_agreement_start_date { get; set; }
        public string em_agreement_exp_date { get; set; }
        public string em_agreemet_exp_rem_date { get; set; }
        public string em_blood_group_code { get; set; }
        public string em_blood_group_name { get; set; }
        public string em_adec_approval_number { get; set; }


        public string em_adec_approval_Date { get; set; }
        public string em_adec_approved_designation { get; set; }
        public string em_adec_approved_qualification { get; set; }
        public string em_adec_approved_subject { get; set; }
        public string em_adec_approved_level { get; set; }
        public string em_health_card_number { get; set; }
        public string em_health_card_issue_date { get; set; }
        public string em_health_card_expiry_date { get; set; }
        public string em_rta_number { get; set; }
        public string em_rta_issue_date { get; set; }
        public string em_rta_expiry_date { get; set; }
        public string em_attendance_month_name { get; set; }
        public string em_attendance_working_days { get; set; }
        public string em_attendance_present_days { get; set; }
        public string em_attendance_absent_days { get; set; }
        public string em_attendance_leave_days { get; set; }


        public string leave_type { get; set; }
        public string remain_days { get; set; }
        public string max_leaves { get; set; }
        public string taken_leaves { get; set; }
        public string extra_leaves { get; set; }
        public string grade_sec { get; set; }
        public string subject_name { get; set; }
        public string lecture_count { get; set; }
        public string practical_count { get; set; }
    }
    #endregion


    public class audit
    {

        public string comn_audit_user_code { get; set; }

        public string comn_appl_name_en { get; set; }

        public string comn_audit_start_time { get; set; }

        public string comn_audit_end_time { get; set; }

        public string comn_audit_remark { get; set; }

        public string opr { get; set; }

        public string comn_audit_ip { get; set; }

        public string comn_audit_dns { get; set; }
    }

    public class classwisesectionstudent
    {

        public string sims_student_Name { get; set; }
        public string sims_enroll_number { get; set; }

        public List<classwisestudentsection> section_list = new List<classwisestudentsection>();
    }

    public class classwisestudentsection
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code_new { get; set; }
        public string sims_section_code_new { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string effect_from_date { get; set; }
        public string fee_code_list { get; set; }
        public string sims_section_name_en { get; set; }
        public bool section_status { get; set; }
    }


    public class Sims158
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_homeroom_code { get; set; }
        public string sims_homeroom_name { get; set; }
        public bool sims_homeroom_status { get; set; }
        public string sims_homeroom_location_details { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string opr { get; set; }

        public string sims_cur_short_name_en { get; set; }
    }






    #region Sim065 Bell Slot Room
    public class Sim063
    {
        public string opr { get; set; }

        public string sims_bell_slot_room_code { get; set; }
        public string sims_bell_slot_room_desc { get; set; }
        public bool sims_bell_slot_room_status { get; set; }

    }
    #endregion


    public class Sims044
    {

        public string sims_cur_code { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

        public string count { get; set; }

        public string sims_section_code { get; set; }

        public string sims_student_enroll_number { get; set; }

        public string Student_name { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_subject_type_code { get; set; }

        public string sims_subject_group_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_fee_code_description { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_section_code_new { get; set; }

        public string sims_grade_code_new { get; set; }

        public string effect_from_date { get; set; }

        public string sims_allocation_status { get; set; }

        public string sims_section_stregth { get; set; }
    }



    public class isexistteacher
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }
        public bool isexist { get; set; }
    }

    public class sim179
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }

        public string sims_concession_academic_year { get; set; }

        public string academic_year { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name { get; set; }

        public string sims_cur_short_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string sims_timetable_cur_name { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string sims_employee_code { get; set; }

        public bool upd { get; set; }

        public string opr { get; set; }
    }

    public class sims173Parameters
    {
        public string sims_cur_name { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_co_desc { get; set; }
        public string sims_co_code { get; set; }

        public string sims_dept_name { get; set; }
        public string sims_dept_code { get; set; }

        public string sims_degn_name { get; set; }
        public string sims_degn_code { get; set; }

        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
    }
    public class sims173
    {
        public bool ischange { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_emp_name { get; set; }
        public bool sims_emp_status { get; set; }
        public bool sims_emp_cc { get; set; }
        public bool sims_emp_ca { get; set; }
        public string sims_teacher_type { get; set; }

        public string company_code { get; set; }

        public string dept_code { get; set; }

        public string desg_code { get; set; }

        public string grade_code { get; set; }

        public string em_login_code { get; set; }
    }


    #region Sims098(Student Health Details)
    public class Sims098
    {
        public string student_enroll_no { get; set; }
        public string student_full_name { get; set; }

        public string sims_disability_desc { get; set; }
        public string sims_medication_desc { get; set; }
        public string sims_health_other_desc { get; set; }
        public string sims_health_hearing_desc { get; set; }
        public string sims_health_vision_desc { get; set; }
        public string sims_health_restriction_desc { get; set; }
        public bool sims_disability_status { get; set; }
        public bool sims_medication_status { get; set; }
        public bool sims_health_other_status { get; set; }
        public bool sims_health_hearing_status { get; set; }
        public bool sims_health_vision_status { get; set; }
        public bool sims_health_restriction_status { get; set; }

        //Sims101

        public string health_card_number { get; set; }
        public string health_card_issue_date { get; set; }
        public string health_card_expiry_date { get; set; }
        public string health_card_issuing_authority { get; set; }
        public string student_blood_group_code { get; set; }
        public bool student_health_restriction_status { get; set; }
        public string student_disability_status { get; set; }
        public string disability_desc { get; set; }
        public string student_medication_status { get; set; }
        public string health_other_status { get; set; }
        public bool health_hearing_status { get; set; }
        public string health_vision_status { get; set; }
        public string regular_doctor_name { get; set; }
        public string regular_doctor_phone { get; set; }
        public string regular_hospital_name { get; set; }
        public string regular_hospital_phone { get; set; }
        public string student_height { get; set; }
        public string student_wieght { get; set; }
        public string student_teeth { get; set; }

        public string student_blood_group_cd { get; set; }
    }
    #endregion

    #region Prospect(Sims034)
    //Pros
    public class Sims034 : simsClass
    {
        public string sims_parent_father_nationality1_code { get; set; }
        public string sims_parent_father_nationality2_code { get; set; }

        public string sims_parent_mother_nationality1_code { get; set; }
        public string sims_parent_mother_nationality2_code { get; set; }

        public string sims_parent_guardian_nationality1_code { get; set; }
        public string sims_parent_guardian_nationality2_code { get; set; }

        public string sims_student_legal_custody { get; set; }
        public string sims_student_birth_country_code { get; set; }
        public string sims_student_birth_country { get; set; }
        public string sims_student_main_language_r_code { get; set; }
        public string sims_student_main_language_r { get; set; }
        public string sims_student_main_language_w_code { get; set; }
        public string sims_student_main_language_w { get; set; }
        public string sims_student_main_language_s_code { get; set; }
        public string sims_student_main_language_s { get; set; }
        public string sims_appl_form_field { get; set; }
        public string sims_appl_parameter { get; set; }

        public string sims_student_religion_code { get; set; }
        public string sims_student_religion_name { get; set; }



        public string sims_pros_number { get; set; }
        public string sims_pros_application_number { get; set; }
        public string sims_pros_date_created { get; set; }
        public string sims_pros_school_code { get; set; }
        public string sims_pros_school_name { get; set; }
        public string sims_pros_cur_code { get; set; }
        public string sims_pros_cur_name { get; set; }
        public string sims_pros_academic_year { get; set; }
        public string sims_pros_grade_code { get; set; }
        public string sims_pros_term_code { get; set; }
        public string sims_pros_tentative_joining_date { get; set; }
        public string sims_pros_passport_number { get; set; }
        public string sims_pros_passport_issue_date { get; set; }
        public string sims_pros_passport_expiry_date { get; set; }
        public string sims_pros_passport_issuing_authority { get; set; }
        public string sims_pros_passport_issue_place { get; set; }
        public string sims_pros_passport_first_name_en { get; set; }
        public string sims_pros_passport_middle_name_en { get; set; }
        public string sims_pros_passport_last_name_en { get; set; }
        public string sims_pros_family_name_en { get; set; }
        public string sims_pros_nickname { get; set; }
        public string sims_pros_passport_first_name_ot { get; set; }
        public string sims_pros_passport_middle_name_ot { get; set; }
        public string sims_pros_passport_last_name_ot { get; set; }
        public string sims_pros_family_name_ot { get; set; }
        public string sims_pros_gender { get; set; }
        public string sims_pros_religion_code { get; set; }
        public string sims_pros_dob { get; set; }
        public string sims_pros_birth_country_code { get; set; }
        public string sims_pros_nationality_code { get; set; }
        public string sims_pros_ethnicity_code { get; set; }
        public string sims_pros_visa_number { get; set; }
        public string sims_pros_visa_issue_date { get; set; }
        public string sims_pros_visa_expiry_date { get; set; }
        public string sims_pros_visa_issuing_place { get; set; }
        public string sims_pros_visa_issuing_authority { get; set; }
        public string sims_pros_visa_type { get; set; }
        public string sims_pros_national_id { get; set; }
        public string sims_pros_national_id_issue_date { get; set; }
        public string sims_pros_national_id_expiry_date { get; set; }
        public string sims_pros_main_language_code { get; set; }
        public string sims_pros_main_language { get; set; }
        public string sims_pros_main_language_r { get; set; }
        public string sims_pros_main_language_w { get; set; }
        public string sims_pros_main_language_s { get; set; }
        public string sims_pros_main_language_m_code { get; set; }
        public string sims_pros_main_language_m { get; set; }
        public string sims_pros_other_language { get; set; }
        public string sims_pros_sibling_status { get; set; }
        public string sims_pros_sibling_name { get; set; }
        public string sims_pros_sibling_enroll_number { get; set; }
        public string sims_pros_parent_id { get; set; }
        public string sims_pros_sibling_dob { get; set; }
        public string sims_pros_sibling_school_code { get; set; }
        public string sims_pros_primary_contact_code { get; set; }
        public string sims_pros_primary_contact_pref { get; set; }
        public string sims_pros_fee_payment_contact_pref { get; set; }
        public string sims_pros_transport_status { get; set; }
        public string sims_pros_transport_desc { get; set; }
        public string sims_pros_father_salutation_code { get; set; }
        public string sims_pros_father_first_name { get; set; }
        public string sims_pros_father_middle_name { get; set; }
        public string sims_pros_father_last_name { get; set; }
        public string sims_pros_father_family_name { get; set; }
        public string sims_pros_father_name_ot { get; set; }
        public string sims_pros_father_nationality1_code { get; set; }
        public string sims_pros_father_nationality2_code { get; set; }
        public string sims_pros_father_appartment_number { get; set; }
        public string sims_pros_father_building_number { get; set; }
        public string sims_pros_father_street_number { get; set; }
        public string sims_pros_father_area_number { get; set; }
        public string sims_pros_father_summary_address { get; set; }
        public string sims_pros_father_city_code { get; set; }
        public string sims_pros_father_city { get; set; }
        public string sims_pros_father_state_code { get; set; }
        public string sims_pros_father_state { get; set; }
        public string sims_pros_father_country_code { get; set; }
        public string sims_pros_father_country { get; set; }
        public string sims_pros_father_phone { get; set; }
        public string sims_pros_father_mobile { get; set; }
        public string sims_pros_father_email { get; set; }
        public string sims_pros_father_fax { get; set; }
        public string sims_pros_father_po_box { get; set; }
        public string sims_pros_father_occupation { get; set; }
        public string sims_pros_father_company { get; set; }
        public string sims_pros_father_passport_number { get; set; }
        public string sims_pros_mother_salutation_code { get; set; }
        public string sims_pros_mother_first_name { get; set; }
        public string sims_pros_mother_middle_name { get; set; }
        public string sims_pros_mother_last_name { get; set; }
        public string sims_pros_mother_family_name { get; set; }
        public string sims_pros_mother_name_ot { get; set; }
        public string sims_pros_mother_nationality1_code { get; set; }
        public string sims_pros_mother_nationality2_code { get; set; }
        public string sims_pros_mother_appartment_number { get; set; }
        public string sims_pros_mother_building_number { get; set; }
        public string sims_pros_mother_street_number { get; set; }
        public string sims_pros_mother_area_number { get; set; }
        public string sims_pros_mother_summary_address { get; set; }
        public string sims_pros_mother_city_code { get; set; }
        public string sims_pros_mother_city { get; set; }
        public string sims_pros_mother_state_code { get; set; }
        public string sims_pros_mother_state { get; set; }
        public string sims_pros_mother_country_code { get; set; }
        public string sims_pros_mother_country { get; set; }
        public string sims_pros_mother_phone { get; set; }
        public string sims_pros_mother_mobile { get; set; }
        public string sims_pros_mother_email { get; set; }
        public string sims_pros_mother_fax { get; set; }
        public string sims_pros_mother_po_box { get; set; }
        public string sims_pros_mother_occupation { get; set; }
        public string sims_pros_mother_company { get; set; }
        public string sims_pros_mother_passport_number { get; set; }
        public string sims_pros_guardian_salutation_code { get; set; }
        public string sims_pros_guardian_first_name { get; set; }
        public string sims_pros_guardian_middle_name { get; set; }
        public string sims_pros_guardian_last_name { get; set; }
        public string sims_pros_guardian_family_name { get; set; }
        public string sims_pros_guardian_name_ot { get; set; }
        public string sims_pros_guardian_nationality1_code { get; set; }
        public string sims_pros_guardian_nationality2_code { get; set; }
        public string sims_pros_guardian_appartment_number { get; set; }
        public string sims_pros_guardian_building_number { get; set; }
        public string sims_pros_guardian_street_number { get; set; }
        public string sims_pros_guardian_area_number { get; set; }
        public string sims_pros_guardian_summary_address { get; set; }
        public string sims_pros_guardian_city_code { get; set; }
        public string sims_pros_guardian_city { get; set; }
        public string sims_pros_guardian_state_code { get; set; }
        public string sims_pros_guardian_state { get; set; }
        public string sims_pros_guardian_country_code { get; set; }
        public string sims_pros_guardian_country { get; set; }
        public string sims_pros_guardian_phone { get; set; }
        public string sims_pros_guardian_mobile { get; set; }
        public string sims_pros_guardian_email { get; set; }
        public string sims_pros_guardian_fax { get; set; }
        public string sims_pros_guardian_po_box { get; set; }
        public string sims_pros_guardian_occupation { get; set; }
        public string sims_pros_guardian_company { get; set; }
        public string sims_pros_guardian_relationship_code { get; set; }
        public string sims_pros_guardian_passport_number { get; set; }
        public string sims_parent_is_employment_status { get; set; }
        public string sims_parent_is_employement_comp_code { get; set; }
        public string sims_parent_is_employment_number { get; set; }
        public string sims_pros_marketing_code { get; set; }
        public string sims_pros_marketing_description { get; set; }
        public string sims_pros_parent_status_code { get; set; }
        public string sims_pros_legal_custody { get; set; }
        public string sims_pros_current_school_name { get; set; }
        public string sims_pros_current_school_head_teacher { get; set; }
        public string sims_pros_current_school_enroll_number { get; set; }
        public string sims_pros_current_school_grade { get; set; }
        public string sims_pros_current_school_cur { get; set; }
        public string sims_pros_current_school_language { get; set; }
        public string sims_pros_current_school_address { get; set; }
        public string sims_pros_current_school_city_code { get; set; }
        public string sims_pros_current_school_city { get; set; }
        public string sims_pros_current_school_country_code { get; set; }
        public string sims_pros_current_school_country { get; set; }
        public string sims_pros_current_school_phone { get; set; }
        public string sims_pros_current_school_fax { get; set; }
        public string sims_pros_current_school_from_date { get; set; }
        public string sims_pros_current_school_to_date { get; set; }
        public string sims_pros_current_school_status { get; set; }
        public string sims_pros_health_card_number { get; set; }
        public string sims_pros_health_card_issue_date { get; set; }
        public string sims_pros_health_card_expiry_date { get; set; }
        public string sims_pros_health_card_issuing_authority { get; set; }
        public string sims_pros_blood_group_code { get; set; }
        public string sims_pros_health_restriction_status { get; set; }
        public string sims_pros_health_restriction_desc { get; set; }
        public string sims_pros_disability_status { get; set; }
        public string sims_pros_disability_desc { get; set; }
        public string sims_pros_medication_status { get; set; }
        public string sims_pros_medication_desc { get; set; }
        public string sims_pros_health_other_status { get; set; }
        public string sims_pros_health_other_desc { get; set; }
        public string sims_pros_health_hearing_status { get; set; }
        public string sims_pros_health_hearing_desc { get; set; }
        public string sims_pros_health_vision_status { get; set; }
        public string sims_pros_health_vision_desc { get; set; }
        public string sims_pros_language_support_status { get; set; }
        public string sims_pros_language_support_desc { get; set; }
        public string sims_pros_behaviour_status { get; set; }
        public string sims_pros_behaviour_desc { get; set; }
        public string sims_pros_gifted_status { get; set; }
        public string sims_pros_gifted_desc { get; set; }
        public string sims_pros_music_status { get; set; }
        public string sims_pros_music_desc { get; set; }
        public string sims_pros_sports_status { get; set; }
        public string sims_pros_sports_desc { get; set; }
        public string sims_pros_declaration_status { get; set; }
        public string sims_pros_status { get; set; }
        public string sims_pros_ip { get; set; }
        public string sims_pros_dns { get; set; }
        public string sims_pros_user_code { get; set; }

        public string sims_pros_academic_year_description { get; set; }
    }
    #endregion



    public class tab
    {
        public string comn_user_tab_no { get; set; }
        public string comn_user_tab_name { get; set; }
        public bool comn_status { get; set; }
    }

    public class tablist
    {
        public string comn_user_tab { get; set; }
    }
    public class global_role
    {
        public List<tab> tablist = new List<tab>();
        public string comn_role_code { get; set; }
        public string comn_user_tab_name { get; set; }

        public string comn_role_name { get; set; }
        public bool ischange { get; set; }

        //public string comn_user_tab_name { get; set; }
        //public string comn_user_tab_name { get; set; }

    }


    public class subject
    {
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public bool sims_status { get; set; }

        public string sims_mark_code { get; set; }

        public string sims_mark_grade_high_marks { get; set; }
    }

    public class subjectGradeSection
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }

        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public bool sims_status { get; set; }

        public string sims_mark_code { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string emp_name { get; set; }
        public bool mapping_status { get; set; }

        public string sims_mark_grade_high_marks { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
    }




    #region sims089_StuduntSectoinSubject
    public class Sims089
    {
        public string sims_grade_section_code { get; set; }
        public string sims_cur_code { get; set; }
        public string level_cur_code_name { get; set; }
        public int sims_student_enroll_number_new { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_grade_code { get; set; }
        public string grade_code_name { get; set; }
        public string sims_cur_code_name { get; set; }

        public string sims_section_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }

        public string sims_student_name { get; set; }
        public string sims_student_enroll_number { get; set; }

        public List<subject> sublist = new List<subject>();
        public List<subjectGradeSection> sub_grade_section = new List<subjectGradeSection>();
        public bool ischange { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_subject_code { get; set; }
        public string sims_section_term { get; set; }


        
        public string sims_term_code{ get; set; }
        public string sims_term_desc_en { get; set; }


        public string sims_mark_code { get; set; }

        public string sims_subject_baseline_score_type { get; set; }

        public string sims_mark_grade_name { get; set; }

        public string sims_religion_name { get; set; }

        public string sims_roll_number { get; set; }
        public int sims_roll_number_new { get; set; }


        public string sims_mark_grade_high_marks { get; set; }

        //public string sims_section_term { get; set; }

        public string HomeroomSubject { get; set; }

        public string sims_Section_code { get; set; }

      //  public string sims_cur_code_name { get; set; }
    }

    #endregion

    #region Sims090(basle line mark entry)
    public class Sims090
    {
        public string opr { get; set; }
        
        public string sims_mark_grade_code { get; set; }

        public string sims_mark_grade_name { get; set; }

        public string sims_subject_baseline_score_type { get; set; }

        public string sims_mark_grade_high { get; set; }

        public bool M { get; set; }

        public bool G { get; set; }
    }
    #endregion


    #region Sims004(CurLevel)
    public class Sims004
    {

        public string level_cur_code { get; set; }

        public string level_cur_code_name { get; set; }

        public string level_code { get; set; }

        public string level_name_en { get; set; }

        public string level_name_ar { get; set; }

        public string level_name_fr { get; set; }

        public string level_name_ot { get; set; }

        public bool level_status { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_name { get; set; }

        public string opr { get; set; }
    }
    #endregion





    public class Sims005
    {

        public string sims_cur_level_code { get; set; }

        public string sims_cur_level_name_en { get; set; }

        public string level_grade_cur_code { get; set; }

        public string level_grade_level_code { get; set; }

        public string level_grade_level_code_name { get; set; }

        public string level_grade_academic_year { get; set; }

        public string level_grade_grade_code { get; set; }

        public string level_grade_grade_code_name { get; set; }

        public string level_grade_cur_short_name { get; set; }

        public string sims_academic_year_description { get; set; }

        public string opr { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_cur_code { get; set; }
    }
    //#region Sims005(Curlevelgrade)
    //public class Sims005
    //{

    //    public string level_grade_cur_code { get; set; }

    //    public string level_grade_level_code { get; set; }

    //    public string level_grade_level_code_name { get; set; }

    //    public string level_grade_academic_year { get; set; }

    //    public string level_grade_grade_code { get; set; }

    //    public string level_grade_grade_code_name { get; set; }

    //    public string level_grade_cur_short_name { get; set; }

    //    public string sims_academic_year_description { get; set; }

    //    public string sims_library_item_cur_level_name { get; set; }

    //    public string sims_library_item_cur_level_code { get; set; }

    //    public string sims_library_item_subscription_cur_level_code { get; set; }

    //    public string sims_library_item_subscription_cur_level_name { get; set; }

    //    public string section_fee_grade_name { get; set; }

    //    public string std_section_grade_name_en { get; set; }

    //    public string section_term_grade_name { get; set; }

    //    public string sims_grade_code_name { get; set; }

    //    public string screening_grade_code_name { get; set; }

    //    public string sims_report_grade_name { get; set; }

    //    public string sims_report_grade_code { get; set; }

    //    public string opr { get; set; }

    //    public int cnt { get; set; }
    //}
    //#endregion


    #region Sims046(Subject Group)

    #endregion



    //pooja

    public class simsClass
    {
        /// <summary>
        /// 
        public string emailid { get; set; }
        public bool chk_email { get; set; }
        public List<Sims179> Grade { get; set; }
        public List<Sims179> Section { get; set; }
        public List<Sims179> Curriculum { get; set; }
        public List<Sims179> Academicyear { get; set; }
        //public List<Sims179> usergroup { get; set; }

        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }


        public string sims_employee_code { get; set; }
        public string grade_name { get; set; }

        public string student_count { get; set; }
        public string sims_fee_academic_year { get; set; }

        public string sims_concession_academic_year { get; set; }
        public string sims_timetable_cur_name { get; set; }
        public string sims_enroll_number { get; set; }

        public string section_term_grade_name { get; set; }
        public string sims_report_grade_code { get; set; }
        public string sims_report_grade_name { get; set; }
        public string sims_report_section_code { get; set; }
        public string sims_report_section_name { get; set; }

        public string student_full_name { get; set; }
        public string sims_cur_name { get; set; }

        public string academic_year { get; set; }



        #region Common Service Variables

        public string sims_cur_code { get; set; }
        public string sims_cur_full_name { get; set; }
        public string sims_cur_short_name { get; set; }

        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year_start_date { get; set; }
        public string sims_academic_year_end_date { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }

        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }

        public string sims_term_code { get; set; }
        public string sims_term_desc { get; set; }


        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }

        #endregion




        // public string level_grade_academic_year { get; set; }
        public string section_fee_academic_year { get; set; }
        public string std_section_academic_year { get; set; }
        public string section_term_academic_year { get; set; }

        public string screening_academic_year { get; set; }
        public string sims_cur_year { get; set; }
        public string grade_academic_year { get; set; }

        public string subject_country_code { get; set; }
        public string subject_country_code_name { get; set; }
        public string sims_admission_father_country_code { get; set; }

        public string sims_admission_mother_country_code { get; set; }

        public string sims_admission_guardian_country_code { get; set; }

        public string country_name { get; set; }
        public string nationality_country_name { get; set; }


        public string sims_student_cur_code { get; set; }
        //

        public string section_term_cur_code { get; set; }
        public string section_term_cur_name { get; set; }
        public string comm_cur_code { get; set; }
        public string grade_code_name { get; set; }


        public string sims_active_form { get; set; }

        public string sims_active_form_status { get; set; }

        public bool upd { get; set; }





        public string sims_academic_year_status { get; set; }

        public string sims_sectiom_promote_code { get; set; }
        public string sims_academic_year_status_desc { get;  set; }
    }


    //Term(sim054)
    public class Sims_Term
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_term_desc_ar { get; set; }

        public string sims_term_desc_fr { get; set; }

        public string sims_term_desc_ot { get; set; }

        public string sims_term_start_date { get; set; }

        public string sims_term_end_date { get; set; }

        public string sims_cur_name { get; set; }

        public bool sims_term_status { get; set; }

        public string sims_academic_year_description { get; set; }

        public string opr { get; set; }

        public string sims_cur_cur_name { get; set; }

        public int myid { get; set; }
    }

    //ppFieldStatus
    #region  Sims501(ppFieldStatus)
    public class Sims501
    {
        public string sims_field_code { get; set; }

        public string sims_field_name { get; set; }

        public bool sims_field_iseditable { get; set; }

        public bool sims_field_isvisible { get; set; }
    }
    #endregion

    //attendance Code

    public class empAttendanceCode
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string sims_category_code { get; set; }

        public string pays_attendance_code { get; set; }

        public string pays_attendance_code_numeric { get; set; }

        public string pays_attendance_short_desc { get; set; }

        public string pays_attendance_description { get; set; }

        public bool pays_attendance_unexcused_flag { get; set; }

        public string pays_attendance_color_code { get; set; }

        public bool pays_attendance_status { get; set; }

        public string pays_attendance_present_value { get; set; }

        public string pays_attendance_absent_value { get; set; }

        public string pays_attendance_tardy_value { get; set; }

        public string pays_attendance_type_value { get; set; }

        public bool pays_attendance_teacher_can_use_flag { get; set; }

        public string pays_attendance_status_inactive_date { get; set; }

        public string sims_attendance_cur_code { get; set; }
        public string pays_attendance_leave_code { get; set; }
    }

    #region  Sims156(attendance_code)
    public class Sims156
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_cur_full_name_en { get; set; }

        public string sims_category_code { get; set; }

        public string sims_attendance_code { get; set; }

        public string sims_attendance_code_numeric { get; set; }

        public string sims_attendance_short_desc { get; set; }

        public string sims_attendance_description { get; set; }

        public bool sims_attendance_teacher_can_use_flag { get; set; }

        public string sims_attendance_color_code { get; set; }

        public bool sims_attendance_status { get; set; }

        public string sims_attendance_present_value { get; set; }

        public string sims_attendance_absent_value { get; set; }

        public string sims_attendance_tardy_value { get; set; }

        public string sims_attendance_type_value { get; set; }

        public bool sims_attendance_unexcused_flag { get; set; }

        public string sims_attendance_status_inactive_date { get; set; }

        public string sims_attendance_cur_code { get; set; }
    }
    #endregion

    //sims_parameter
    #region  Sims031(sims_parameter)
    public class Sims031
    {
        public string sims_mod_name { get; set; }
        public string sims_doc_mod_code_name { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_form_field_value2 { get; set; }
        public string sims_appl_form_field_value3 { get; set; }
        public string sims_appl_form_field_value4 { get; set; }
        public string sims_appl_name { get; set; }
        public string sims_appl_code { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field { get; set; }

        public string sims_mod_code { get; set; }

        public string comn_appl_mod_code { get; set; }

        public string comn_application_code { get; set; }

        public string appl_form_field { get; set; }

        public string appl_form_field_value1 { get; set; }

        public string appl_parameter { get; set; }

        public int myid { get; set; }

        public string opr { get; set; }
    }


    #region(Tejas)
    //Application
    public class SchoolParameter
    {
        public string id { get; set; }
        public string comn_appl_mod_name { get; set; }
        public string sims_mod_name { get; set; }
        public string sims_doc_mod_code_name { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_form_field_value2 { get; set; }
        public string sims_appl_form_field_value3 { get; set; }
        public string sims_appl_form_field_value4 { get; set; }
        public string sims_appl_name { get; set; }
        public string sims_appl_code { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field { get; set; }
        public string sims_appl_form_field_id { get; set; }

        public string sims_mod_code { get; set; }

        public string comn_appl_mod_code { get; set; }

        public string comn_application_code { get; set; }

        public string appl_form_field { get; set; }

        public string appl_form_field_value1 { get; set; }

        public string appl_parameter { get; set; }

        public int myid { get; set; }

        public string opr { get; set; }
    }
    #endregion


    public class reporting
    {
        public string comn_mod_name { get; set; }
        public string comn_doc_mod_code_name { get; set; }
        public string comn_appl_form_field_value1 { get; set; }
        public string comn_appl_form_field_value2 { get; set; }
        public string comn_appl_form_field_value3 { get; set; }
        public string comn_appl_form_field_value4 { get; set; }
        public string comn_appl_form_field_value5 { get; set; }
        public string comn_appl_name { get; set; }
        public string comn_appl_code { get; set; }
        public string comn_appl_parameter { get; set; }
        public string comn_appl_form_field { get; set; }
                     
        public string comn_mod_code { get; set; }

        public string comn_appl_mod_code { get; set; }
        public string comn_serial_number { get; set; }

        public string comn_application_code { get; set; }

        public string appl_form_field { get; set; }

        public string appl_form_field_value1 { get; set; }

        public string appl_parameter { get; set; }

        public int myid { get; set; }

        public string opr { get; set; }
    }

    #endregion

    #region Sims164(Moderator)
    public class Sims164
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_comm_code { get; set; }
        public string sims_comm_code_name { get; set; }
        public bool sims_email_flag { get; set; }
        public bool sims_sms_flag { get; set; }
        public bool sims_bcc_flag { get; set; }
        public bool sims_bcc_email_flag { get; set; }
        public bool sims_bcc_sms_flag { get; set; }
        public bool sims_comm_status { get; set; }

        public string sims_attendance_cur_code { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string opr { get; set; }
    }
    #endregion


    #region Sims518
    public class Sims518
    {
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_attendance_rule_code { get; set; }
        public string sims_attendance_rule_name { get; set; }
        public string sims_attendance_category_code { get; set; }
        public string sims_attendance_attendance_code { get; set; }
        public string sims_attendance_threshold_value1 { get; set; }
        public string sims_attendance_threshold_value2 { get; set; }
        public string sims_attendance_threshold_value3 { get; set; }
        public string sims_attendance_threshold_value4 { get; set; }
        public string sims_attendance_threshold_value5 { get; set; }
        public bool sims_attendance_report_card_flag { get; set; }
        public bool sims_attendance_term_flag { get; set; }
        public bool sims_attendance_academic_year_flag { get; set; }
        public string sims_attendance_consequence_code { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_attendance_consequence_name { get; set; }

        public string sims_attendance_attendance_name { get; set; }

        public string opr { get; set; }

        public string sims_academic_year_desc { get; set; }
    }
    #endregion

    #region Sims045(subject)
    public class Sims045
    {
        public bool sims_isArabic_subject { get; set; }

        public string subject_cur_code { get; set; }

        public string subject_cur_code_name { get; set; }

        public string subject_code { get; set; }

        public string subject_group_code { get; set; }

        public string subject_group_code_name { get; set; }

        public string subject_name_en { get; set; }

        public string subject_name_ar { get; set; }

        public string subject_name_fr { get; set; }

        public string subject_name_ot { get; set; }

        public string subject_name_abbr { get; set; }

        public string subject_moe_code { get; set; }

        public bool subject_status { get; set; }

        public string subject_type { get; set; }

        public string subject_type_name { get; set; }

        public string subject_country_code { get; set; }

        public string subject_country_code_name { get; set; }

        public string sims_attendance_cur_code { get; set; }

        public bool status { get; set; }

        public string strMessage { get; set; }

        public string opr { get; set; }

        public string subject_color_code { get; set; }

        public string sims_subject_color_code { get; set; }
        public string subject_language { get; set; }

        public string sims_religion_language { get; set; }

        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_attribute_group_code { get; set; }
        public string sims_attribute_grade { get; set; }
        public string sims_attribute_name { get; set; }
        public string sims_attribute_name_ot { get; set; }
        public string sims_attribute_display_order { get; set; }
        public bool sims_attribute_status { get; set; }
    }
    #endregion
    public class attribute
    {
        
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_attribute_group_code { get; set; }
        public string sims_attribute_grade { get; set; }
        public string sims_attribute_name { get; set; }
        public string sims_attribute_name_ot { get; set; }
        public string sims_attribute_display_order { get; set; }
        public bool sims_attribute_status { get; set; }
        public string sims_section_name { get; set; }
        public string sims_attribute_group_name_en { get; set; }
        public string sims_attribute_group_name_ot { get; set; }
        public string sims_attribute_group_type { get; set; }
        public bool sims_attribute_group_status { get; set; }
        public string class1 { get; set; }
        public string sims_attribute_group_name { get; set; }
        public string sims_attribute_code { get; set; }
        public string sims_attribute_name_en { get; set; }
        public string sims_attribute_type { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_cat_assign_subject_attribute_max_score { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_term_code { get; set; }
        public string sims_attribute_max_score { get; set; }
        public string sims_display_order { get; set; }
    }

    #region Sims046(Subject Group)
    public class Sims046
    {

        public string sims_cur_name { get; set; }

        public string subject_group_cur_code { get; set; }

        public string subject_group_code { get; set; }

        public string subject_group_name_eng { get; set; }

        public string subject_group_name_ar { get; set; }

        public string subject_group_name_fr { get; set; }

        public string subject_group_name_ot { get; set; }

        public string sims_attendance_cur_code { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string opr { get; set; }

        public bool status { get; set; }

        public string strMessage { get; set; }

        public bool sims_subject_group_isMandatory { get; set; }
    }
    #endregion

    #region Sims047(Subject Type)
    public class Sims047
    {
        public string sims_cur_name { get; set; }

        public string subject_type_code { get; set; }

        public string subject_type_name_ar { get; set; }

        public string subject_type_name_fr { get; set; }

        public string subject_type_name_ot { get; set; }

        public string subject_type_name_eng { get; set; }

        public string sims_attendance_cur_code { get; set; }

        public bool status { get; set; }

        public string strMessage { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims165(Moderator User)
    public class Sims165
    {
        public string sims_comm_code_name { get; set; }
        public string sims_comm_name { get; set; }
        public string sims_comm_user_code { get; set; }
        public string sims_comm_user_name { get; set; }
        public bool sims_comm_user_code_status { get; set; }
        public string sims_comm_email_user_code { get; set; }
        public bool sims_comm_email_user_code_status { get; set; }
        public string sims_comm_sms_user_code { get; set; }
        public bool sims_comm_sms_user_code_status { get; set; }
        public string sims_comm_bcc_user_code { get; set; }
        public bool sims_comm_bcc_user_code_status { get; set; }
        public string sims_comm_bcc_email_user_code { get; set; }
        public bool sims_comm_bcc_email_user_code_status { get; set; }
        public string sims_comm_bcc_sms_user_code { get; set; }
        public bool sims_comm_bcc_sms_user_code_status { get; set; }

        public string sims_cur_short_name { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_attendance_cur_code { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string sims_comm_code { get; set; }

        public int myid { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Sims049(Workflow Details)
    public class Sims049
    {

        public string sims_workflow_mod_code { get; set; }

        public string comn_mod_name_en { get; set; }

        public string sims_workflow_appl_code { get; set; }

        public string comn_appl_name_en { get; set; }

        public string sims_workflow { get; set; }

        public string sims_workflow_description { get; set; }

        public string workflow_nm { get; set; }

        public string sims_workflow_srno { get; set; }

        public string sims_workflow_type { get; set; }

        public string WorkFlowType_Desc { get; set; }

        public bool sims_workflow_status { get; set; }

        public string sims_workflow_type_Desc { get; set; }
    }
    #endregion



    #region Comn018(View Circular)
    public class Comn018
    {
        public string sims_circular_number { get; set; }

        public string sims_circular_date { get; set; }

        public string sims_circular_publish_date { get; set; }

        public string sims_circular_title { get; set; }

        public string sims_circular_short_desc { get; set; }

        public string sims_circular_desc { get; set; }

        public string sims_circular_file_path1 { get; set; }

        public string sims_circular_file_path2 { get; set; }

        public string sims_circular_file_path3 { get; set; }

        public string sims_cur_code { get; set; }

        public string cur_name { get; set; }

        public string sims_circular_type { get; set; }

        public string circular_type { get; set; }

        public string sims_circular_created_user_code { get; set; }

        public string circular_category { get; set; }

        public string sims_circular_display_order { get; set; }

        public bool publish_status { get; set; }
    }
    #endregion

    #region Comn016(View News)
    public class Comn016
    {
        public string sims_news_number { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_news_date { get; set; }

        public string sims_news_publish_date { get; set; }

        public string sims_news_expiry_date { get; set; }

        public string sims_news_title { get; set; }

        public string sims_news_short_desc { get; set; }

        public string sims_news_desc { get; set; }

        public string sims_news_file_path1 { get; set; }

        public string sims_news_file_path3 { get; set; }

        public string sims_news_file_path2 { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_news_type { get; set; }

        public string news_type { get; set; }

        public string sims_news_category { get; set; }

        public string news_category { get; set; }

        public string sims_news_created_user { get; set; }

        public string sims_news_display_order { get; set; }

        public bool publish_status { get; set; }
    }
    #endregion

    #region Sims001(Academic Year)
    public class Sims001
    {
        public string sims_cur_code { get; set; }

        public string sims_cur_code_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string sims_academic_year_status { get; set; }

        public string sims_academic_year_status_name { get; set; }

        public string sims_academic_year_status_previous { get; set; }

        public string opr { get; set; }

        public int myid { get; set; }
    }
    #endregion

    #region Section_Subject

    public class Sims039
    {
        public string _sims_cur_cur_name { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_abbr { get; set; }
        public string sims_moe_code { get; set; }
        public string sims_subject_type_name { get; set; }
        public bool sims_subject_status { get; set; }
        public string sims_subject_group_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_display_order { get; set; }
        public string sims_section_name { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_subject_mdl_course_id { get; set; }
        public string sims_section_subject_credit_hours { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_grade_name { get; set; }

        public List<Sims039> sims_subjects { get; set; }

        public string opr { get; set; }

        public string sims_subject_code1 { get; set; }
    }

    #endregion 


    public class SecTermsubject
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        public string sims_subject_group_code{ get; set; }
        public string sims_display_order{ get; set; }
        public string sims_display_report_order{ get; set; }
        public string sims_section_subject_credit_hours{ get; set; }
        public bool sims_section_subject_term_status { get; set; }
        public string sims_section_subject_term_code { get; set; }
        public string opr { get; set; }


        public string sims_section_subject_term_dependency_code{ get; set; }
        
        public string sims_subject_main_subject_code{ get; set; }
        public string sims_subject_group_main_subject_group_code{ get; set; }
        public string sims_subject_dependent_subject_code{ get; set; }
        public string sims_subject_group_dependent_subject_group_code{ get; set; }
        public bool sims_section_subject_term_dependency_status { get; set; }



        public List<SecTerm> term = new List<SecTerm>();

    }
    public class SecTerm
    {

        public string sims_display_order { get; set; }
        public string sims_display_report_order { get; set; }
        public string sims_section_subject_credit_hours { get; set; }
        public string sims_term_code{ get; set; }
        public string sims_term_desc_en { get; set; }
        public bool sims_subject_status { get; set; }

        public bool sims_section_subject_term_dependency_status { get; set; }
    }


    #region Sims003(Curriculum)
    public class Sims003
    {

        public string curriculum_code { get; set; }

        public string curriculum_short_name { get; set; }

        public string curriculum_short_name_ar { get; set; }

        public string curriculum_short_name_fr { get; set; }

        public string curriculum_short_name_ot { get; set; }

        public string curriculum_full_name { get; set; }

        public string curriculum_full_name_ar { get; set; }

        public string curriculum_full_name_fr { get; set; }

        public string curriculum_full_name_ot { get; set; }

        public bool curriculum_status { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region Ganesh

    #region Sims565(Prospect Fees)

    public class Sims565
    {
        public string sims_doc_no { get; set; }
        public string sims_doc_date { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_student_full_name { get; set; }
        public string sims_parent_full_name { get; set; }
        public string sims_grade_fee_amount { get; set; }
        public string sims_pros_number { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_amount { get; set; }
        public string sims_fee_status { get; set; }
        public string sims_user_name { get; set; }
        public string sims_doc_status { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_rev_acc_no { get; set; }
        public string sims_cash_acc_no { get; set; }
        public int sr_no { get; set; }
        public string sims_doc_status_content { get; set; }
        public string slma_pty_bank_id { get; set; }
        public string slma_pty_bank_name { get; set; }
        public string pb_gl_acno { get; set; }
        public string sims_doc_status_desc { get; set; }
        public string sims_fee_payment_mode { get; set; }
        public string sims_fee_cheque_bank_code { get; set; }
        public string sims_fee_cheque_number { get; set; }
        public string sims_fee_cheque_date { get; set; }
    }

    #endregion

    #region Sims521(Admission Fees)

    public class Sims521
    {
        public string sims_doc_no { get; set; }
        public string sims_doc_date { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_student_full_name { get; set; }
        public string sims_parent_full_name { get; set; }
        public string sims_grade_fee_amount { get; set; }
        public string sims_admission_no { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_amount { get; set; }
        public string sims_fee_status { get; set; }
        public string sims_user_name { get; set; }
        public string sims_doc_status { get; set; }
        public string sims_fee_code_description { get; set; }
        public string sims_rev_acc_no { get; set; }
        public string sims_cash_acc_no { get; set; }
        public string sr_no { get; set; }

        public string sims_doc_status_content { get; set; }

        public string slma_pty_bank_id { get; set; }
        public string slma_pty_bank_name { get; set; }
        public string pb_gl_acno { get; set; }



        public string sims_doc_status_desc { get; set; }


        public string sims_fee_payment_mode { get; set; }

        public string sims_fee_cheque_bank_code { get; set; }

        public string sims_fee_cheque_number { get; set; }

        public string sims_fee_cheque_date { get; set; }

        public string sims_fee_payment_desc { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_doc_remark { get; set; }

    }


    #endregion
    #region sims171

    public class Sims171
    {

        public string sims_timetable_number { get; set; }
        public string sims_timetable_filename { get; set; }
        public bool sims_timetable_status { get; set; }

        public string sims_timetable_cur_name { get; set; }

        public string sims_academic_year { get; set; }
        public string sims_timetable_employee_id { get; set; }
        public string employee_name { get; set; }
        public string sims_grade_name { get; set; }

        public string sims_section_name { get; set; }

        public string opr { get; set; }

        public string sims_grade { get; set; }

        public string sims_section { get; set; }

        public string sims_timetable_cur { get; set; }
    }

    #endregion


    #region Sims563(Admission Fee)

    public class Sims563
    {
        public string opr_upd;

        public int sr_no { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_fee_code { get; set; }
        public string sims_fee_name { get; set; }
        public string sims_fee_amount { get; set; }
        public string sims_rev_acc_no { get; set; }
        public string sims_rev_acc_name { get; set; }
        public string sims_cash_acc_no { get; set; }
        public string sims_cash_acc_name { get; set; }
        public string acc_name { get; set; }
        public string acc_code { get; set; }

        public bool sims_fee_status { get; set; }
        public bool sims_is_fee_adjustable { get; set; }
        public string sims_adjustable_fee_type { get; set; }
        public string sims_adjustable_fee_type_name { get; set; }

        public string is_admission_or_prospect_fee { get; set; }
        public string opr { get; set; }

        public string sims_academic_year_description { get; set; }

        public string FeeCount { get; set; }


        public string sims_cur_short_name_en { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string sims_behaviour_code { get; set; }
        public string sims_behaviour_type { get; set; }
        public string sims_behaviour_description { get; set; }
        public string sims_behaviour_points { get; set; }
        public string sims_behaviour_img { get; set; }
        public bool sims_behaviour_status { get; set; }
        public bool disable_status { get;  set; }
    }
    #endregion

    #region Sims105(Certificate Issue Employee)

    public class Sims105
    {
        public string sims_certificate_number { get; set; }
        public string sims_certificate_name { get; set; }
        public string sims_certificate_request_number { get; set; }
        public string user_type { get; set; }
        public string sims_certificate_issue_number { get; set; }
        public string sims_certificate_issue_date { get; set; }
        public string sims_certificate_issue_person_number { get; set; }
        public bool sims_certificate_print_flag { get; set; }
        public string sims_certificate_print_count { get; set; }
        public string sims_certificate_doc_path { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_certificate_name_display { get; set; }
        public string user_type_code { get; set; }
        public string opr1 { get; set; }
        public string username { get; set; }
        public string opr { get; set; }

        public string sims_certificate_request_date { get; set; }
        public string sims_certificate_requested_by { get; set; }
        public string sims_certificate_requested_by_name {get;set;}
        public string sims_certificate_reason { get; set; }
        public string sims_certificate_dept_code { get; set; }
        public string sims_certificate_dept_name { get; set; }
        public string sims_certificate_expected_date { get; set; }
        public string sims_certificate_employee_number { get; set; }
        public string sims_certificate_approved_by { get; set; }
        public string sims_certificate_approve_date { get; set; }
        public string sims_certificate_issued_by { get; set; }
        public string sims_certificate_approved_status { get; set; }
        public string sims_certificate_request_status { get; set; }
        public string print_count { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_certificate_other_purpose_reason { get; set; }

        public string sims_certificate_PurposeName { get; set; }

        public string report_loc { get; set; }
    }
    #endregion

    #region Sims104(Certificate Master)
    public class Sims104
    {
        public string sims_cur_code { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_certificate_number { get; set; }

        public string sims_certificate_name { get; set; }

        public string sims_certificate_desc { get; set; }

        public bool sims_certificate_status { get; set; }

        public string sims_certificate_signatory5 { get; set; }

        public string sims_certificate_signatory4 { get; set; }

        public string sims_certificate_signatory3 { get; set; }

        public string sims_certificate_signatory2 { get; set; }

        public string sims_certificate_signatory1 { get; set; }

        public string sims_certificate_watermark { get; set; }

        public string sims_certificate_footer5 { get; set; }

        public string sims_certificate_footer4 { get; set; }

        public string sims_certificate_footer3 { get; set; }

        public string sims_certificate_footer2 { get; set; }

        public string sims_certificate_footer1 { get; set; }

        public string sims_certificate_body15 { get; set; }

        public string sims_certificate_body14 { get; set; }

        public string sims_certificate_body13 { get; set; }

        public string sims_certificate_body12 { get; set; }

        public string sims_certificate_body11 { get; set; }

        public string sims_certificate_body10 { get; set; }

        public string sims_certificate_body9 { get; set; }

        public string sims_certificate_body8 { get; set; }

        public string sims_certificate_body7 { get; set; }

        public string sims_certificate_body6 { get; set; }

        public string sims_certificate_body5 { get; set; }

        public string sims_certificate_body4 { get; set; }

        public string sims_certificate_body3 { get; set; }

        public string sims_certificate_body2 { get; set; }

        public string sims_certificate_body1 { get; set; }

        public string sims_certificate_header5 { get; set; }

        public string sims_certificate_header4 { get; set; }

        public string sims_certificate_header3 { get; set; }

        public string sims_certificate_header2 { get; set; }

        public string sims_certificate_header1 { get; set; }

        public string sims_certificate_count_flag { get; set; }

        public string sims_cur_code_name { get; set; }

        public string sims_workflow_type { get; set; }

        public string opr { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_appl_form_field_value1 { get; set; }
    }
    #endregion

    #endregion

    public class sims013
    {
        public string sims_concession_from_date { get; set; }
            public string sims_concession_to_date { get; set; }
        public string sims_concession_term { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_fee_academic_year { get; set; }
        public string sims_concession_number { get; set; }
        public string sims_concession_description { get; set; }
        public string sims_concession_fee_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_transaction_number { get; set; }
        public string sims_created_by { get; set; }
        public List<sims013_details> list = new List<sims013_details>();
        public string sims_fee_code_description { get; set; }

        public string sims_Expected_fee { get; set; }

        public string sims_Concession_amount { get; set; }
        public string sims_parent_number { get; set; }
        public string parent_name { get; set; }
        public bool sims_student_employee_comp_code_flag { get; set; }
        public string sims_student_employee_comp_code { get; set; }
       // public string sims_fee_code_description { get; set; }

    }
    public class sims013_details
    {
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_fee_academic_year { get; set; }
        public string sims_concession_number { get; set; }
        public string sims_concession_description { get; set; }
        public string sims_concession_fee_code { get; set; }
        public bool sims_status { get; set; }
        public string sims_transaction_number { get; set; }
        public string sims_created_by { get; set; }
    }

    #region Sims130(Library Status)
    public class Sims130
    {
        public int sims_library_status_code { get; set; }
        public string sims_library_status_name { get; set; }
    }
    #endregion

    #region Sims021(Sims_Fee_Category)
    public class Sims021
    {

        public string sims_fee_category { get; set; }
        public string sims_fee_category_desc { get; set; }
        public bool sims_category_status { get; set; }

        public string opr { get; set; }
    }
    #endregion

    #region  Sims022(Fee_type)
    public class Sims022
    {
         public string sims_appl_parameter;
       public string sims_appl_form_field_value1;
        public string sims_fee_code_desc { get; set; }
        public bool sims_fee_posting_status { get; set; }
        public bool sims_fee_code_overpaid { get; set; }
        public string sims_fee_code { get; set; }
        public bool sims_fee_code_status { get; set; }
        public string invoice_type { get; set; }

        public bool status { get; set; }

        public string strMessage { get; set; }

        public string opr { get; set; }

        public bool fee_recievable_staus { get; set; }
        public string sims_section_ob_fee_code { get; set; }
        public bool fee_applicable_status { get; set; }
        public string sims_fee_code_vat_percentage { get; set; }
        public bool sims_fee_Exampted { get; set; }

        public string sims_openingbalfee_desc { get; set; }

        public string sims_fee_category_code { get; set; }

        public string gldc_doc_code { get; set; }

        public string gldc_doc_name { get; set; }

        public string doc_type { get; set; }

        public string fee_code { get; set; }

        public string sims_fee_code_type { get; set; }

        public string sims_fee_code_type_desc{ get; set; }


        public string sims_fee_frequency { get; set; }

        public string sims_fee_amount { get; set; }

        public string period_desc { get; set; }

        public string period_code { get; set; }
        public bool sims_fee_code_posting_flag { get; set; }

        public int sims_scriptid { get; set; }
        public string sims_scriptname { get; set; }
        public string sims_startdate { get; set; }
        public string sims_nextdate { get; set; }
        public string sims_alertdays { get; set; }
        public bool sims_status { get; set; }
        public string sims_control_field { get; set; }
        public string sims_alert_name { get; set; }
        public string sims_alert_desc { get; set; }
        public string sims_alert_time { get; set; }
        public string sims_alert_frequncy { get; set; }
        public string sims_alert_frequncy_time { get; set; }
        public string sims_alert_frequncy_update_count { get; set; }
        public string sims_alert_frequncy_time_to_add { get; set; }
        public string sims_alert_due_days { get; set; }
        public string sims_msg_sr_no { get; set; }
        public string sims_msg_appl_code { get; set; }
        public string sims_msg_subject { get; set; }



      //  public string invoice_type { get; set; }
    }
    #endregion

    #region Sims541(att_criteria)
    public class Sims541
    {

        public string sims_attendance_cur_code { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_grade_name_en { get; set; }

        public bool sims_attendance_day_wise_flag { get; set; }

        public bool sims_attendance_slot_wise_flag { get; set; }

        public bool sims_attendance_am_pm_wise_flag { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string sims_appl_parameter_reg { get; set; }

        public string sims_appl_form_field_value1_reg { get; set; }
    }
    #endregion

    //My Change

    #region  (Defaulter List)  

    public class view_student_attendance
    {
        public string sims_academic_year { get; set; }
        public string sims_enrollment_number { get; set; }
        public string sims_grade_code { get; set; }
        public string section { get; set; }
        public string studentName { get; set; }

        public List<attendance_details> att_details = new List<attendance_details>();

    }



    public class attendance_details
    {
        public string present { get; set; }
        public string absent { get; set; }
        public string absent_excused { get; set; }
        public string Tardy { get; set; }
        public string Unmarked { get; set; }
        public string total { get; set; }
        public string TotalAttendanceDays { get; set; }
        public string Percentage { get; set; }
        public string Month_No { get; set; }
        public string month_name { get; set; }
    }


    public class Sims561    
    {
        

        public string doc_path_available_flag { get; set; }

        public string sims_absent_notification_flag { get; set; }
        public string sims_absent_notification_flag_sms  { get; set; }
        public string sims_absent_notification_flag_alert { get; set; }
        public string sims_absent_notification_flag_email { get; set; }
        
        public string sims_absent_notification_sms   { get; set; }
        public string sims_absent_notification_alert { get; set; }
        public string sims_absent_notification_email { get; set; }
        public string sims_teacher_name { get; set; }


        public string sims_criteria_name_en { get; set; }
        public string sims_admission_doc_path { get; set; }
        public string sims_admission_doc_status { get; set; }
        public string sims_admission_date_created { get; set; }
        public bool sims_admission_doc_verify { get; set; }

        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }

        public string student_full_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_attendance_day_attendance_code { get; set; }

        public string sims_email_id { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_guardian_email { get; set; }

        public bool sims_send_email { get; set; }
        public bool sims_send_alert { get; set; }
        public bool sims_send_sms { get; set; }
        public string sims_parent_number { get; set; }
        public string opr { get; set; }
        public string sims_from_date { get; set; }

        public string sims_contact_number { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_guardian_mobile { get; set; }

        public string sims_contact_person { get; set; }
        public string emp_name { get; set; }
        public string attendance_type { get; set; }
        public string stud_name { get; set; }

        public string sims_communicate_to_father { get; set; }
        public string sims_communicate_to_mother { get; set; }
        public string sims_communicate_to_guardian { get; set; }

        //public bool sims_father_send_email { get; set; }
        //public bool sims_mother_send_email { get; set; }
        //public bool sims_guardian_send_email { get; set; }
        //public bool sims_father_send_alert { get; set; }
        //public bool sims_mother_send_alert { get; set; }
        //public bool sims_guardian_send_alert { get; set; }
        //public bool sims_father_send_sms { get; set; }
        //public bool sims_mother_send_sms { get; set; }
        //public bool sims_guardian_send_sms { get; set; }

        public List<document> doc_list = new List<document>();
    }


    public class document
    {

        public string sims_admission_doc_path { get; set; }

        public string sims_admission_doc_status { get; set; }

        public string sims_criteria_name_en { get; set; }

        public bool doc_path_available_flag { get; set; }

        public bool sims_admission_doc_verify { get; set; }
    }
    #endregion

    #region CancelReceipt
    public class cancel_receipt
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string doc_user { get; set; }
        public string doc_no { get; set; }
        public string doc_date { get; set; }
        public string doc_amount { get; set; }
        public string doc_status { get; set; }
        public string doc_code { get; set; }
        public string enrollment_no { get; set; }
        public string studentName { get; set; }
        public string studentclass { get; set; }
        public string cur_code { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }
        public bool isEnabled { get; set; }
        public string doc_reference_no { get; set; }
        public string doc_generated_by { get; set; }
        public bool IsSiblingReceipt { get; set; }
        public string sims_academic_year_status { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_concession_description { get; set; }


    }



    #endregion



    //#region Invoice Generation For Student
    //public class sims541
    //{
    //    //public sims541()
    //    //{
    //    //    iv_documents = new List<sims_invoice_documents>();
    //    //}

    //    public string sims_cur_code { get; set; }
    //    public string sims_cur_name { get; set; }
    //    public string sims_academic_year { get; set; }
    //    public string sims_grade_code { get; set; }
    //    public string sims_grade_name { get; set; }
    //    public string sims_section_code { get; set; }
    //    public string sims_section_name { get; set; }

    //    //student Details
    //    public string sims_enroll_number { get; set; }
    //    public string sims_student_name { get; set; }
    //    public string sims_parent_name { get; set; }
    //    public string sims_student_class { get; set; }
    //    public string sims_expected_amount { get; set; }
    //    public string sims_paid_amount { get; set; }
    //    public string sims_invoice_amount { get; set; }

    //    //Bool Variables
    //    public bool sims_invoice_status { get; set; }
    //    public bool sims_invoice_student_status { get; set; }

    //    //Fee Variables
    //    public string sims_fee_code { get; set; }
    //    public string sims_period_code { get; set; }

    //    //Paying Agent Variables
    //    public string sims_acc_no { get; set; }
    //    public string slma_slgldrg_no { get; set; }
    //    public string sims_transacation_no { get; set; }
    //    public string sims_transacation_line_no { get; set; }


    //    public string sims_invoce_mode { get; set; }
    //    public string sims_invoce_mode_code { get; set; }

    //    public string sims_invoce_frq_name { get; set; }
    //    public string sims_invoce_frq_name_value { get; set; }

    //    public string sims_start_date { get; set; }
    //    public string sims_end_date { get; set; }
    //    public string slma_ldgrctl_code { get; set; }
    //    public string slma_ldgrctl_name { get; set; }
    //    public string coad_pty_full_name { get; set; }
    //    public string in_no { get; set; }
    //    public string in_date { get; set; }
    //    public string in_status { get; set; }
    //    public string in_status_code { get; set; }

    //    //public List<sims_invoice_documents> iv_documents;

    //    //Emailids
    //    public string father_email { get; set; }
    //    public string mother_email { get; set; }
    //}
    //#endregion



    public class Com008
    {

        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string opr { get; set; }
    }

    public class Sim016
    {
        public string sims_employee_no { get; set; }
        public string sims_employee_name { get; set; }
        public string sims_employee_adhaar_card_no { get; set; }
        public string sims_employee_pan_card_no{ get; set; }
        public string sims_employee_date_of_birth{ get; set; }
        public string sims_employee_mobile_no { get; set; }
        public string sims_employee_address { get; set; }
        public string sims_employee_gender { get; set; }
        public string sims_employee_email { get; set; }
        public string sims_employee_status { get; set; }
        public string sims_employee_col1 { get; set; }
        public string sims_employee_col2 { get; set; }
        public string opr { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        
    }

    public class Sim017
    {
        public string sims_employee2_no { get; set; }
        public string sims_employee2_adhaar_card_no { get; set; }
        public string sims_employee2_date_of_apply { get; set; }
        public string sims_employee2_result { get; set; }
        public string sims_employee2_update { get; set; }
        public string sims_employee2_remark { get; set; }

        public string opr { get; set; }

    }

    public class Com009
    {
        public string comn_role_code { get; set; }
        public string comn_role_name { get; set; }
        public bool comn_role_status { get; set; }
        public string opr { get; set; }
        public string role_code { get; set; }
    }


    public class Comn014
    {
        public string comn_reset_user_code { get; set; }
        public string comn_reset_user_name { get; set; }
        public string comn_user_new_password { get; set; }
        public bool comn_reset_user_captcha_status { get; set; }
        public string comn_reset_user_email { get; set; }
        public string comn_reset_user_status { get; set; }
        public string comn_reset_user_status_code { get; set; }
        public string comn_reset_user_expiry_date { get; set; }
        public string comn_user_selected_user { get; set; }
        public string comn_user_name { get; set; }
        public string comn_user_Confirmpassword { get; set; }
        public bool comn_user_captcha_status { get; set; }
        public string comn_user_email { get; set; }
        public string comn_user_status { get; set; }
        public string comn_user_expiry_date { get; set; }
        public string comn_user_code { get; set; }
        public string comn_user_alias { get; set; }
        public string comn_user_password { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_date_created { get; set; }
        public string comn_user_last_login { get; set; }
        public string comn_user_emp_code { get; set; }
        public string comn_user_enroll_number { get; set; }
        public string comn_user_phone_number { get; set; }
        public string comn_user_secret_question_code { get; set; }
        public string comn_user_secret_answer { get; set; }
        public string comn_user_remark { get; set; }
        public string comn_user_created_by_user_code { get; set; }
        public string comn_user_created_date { get; set; }
        public string comn_user_status_name { get; set; }
        public string opr { get; set; }
    }


    public class CommonUserControlClass
    {
        //Variables of Search
        public string name { get; set; }
        public string code { get; set; }
        public string search_parent_no { get; set; }
        
        public int sims_roll_number { get; set; }

        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }

        public string search_std_name { get; set; }
        public string search_std_enroll_no { get; set; }
        public string search_std_grade_name { get; set; }
        public string search_std_section_name { get; set; }
        public string search_std_family_name { get; set; }
        public string search_std_transport_bus { get; set; }
        public string search_std_passport_no { get; set; }
        public string search_std_email_id { get; set; }
        public string search_std_mobile_no { get; set; }
        //for grid
        public string teacher_name { get; set; }
        public string s_enroll_no { get; set; }
        public string s_sname_in_english { get; set; }
        public string s_sname_in_arabic { get; set; }
        public string s_class { get; set; }
        public string s_parent_id { get; set; }
        public string search_std_image { get; set; }
        public string s_parent_name { get; set; }
        public string Mail_id { get; set; }
        public string Mobile_no { get; set; }
        public string Teacher_id { get; set; }
        public string Teacher_Name { get; set; }
        public string Teacher_Type { get; set; }
        public string Teacher_employee_code { get; set; }
        public string user_code { get; set; }
        public string user_name { get; set; }
        public string user_group_id { get; set; }
        public string user_date_created { get; set; }
        public bool user_captcha_status { get; set; }
        public string user_last_login { get; set; }
        public string user_status { get; set; }
        public string user_expiry_date { get; set; }
        //end grid

        public string search_parent_name { get; set; }
        public string search_parent_id { get; set; }
        public string search_par_image { get; set; }
        public string search_parent_email_id { get; set; }
        public string search_parent_mobile_no { get; set; }

        public string search_parent_first_name { get; set; }
        public string search_parent_midd_name { get; set; }
        public string search_parent_last_name { get; set; }
        public string search_parent_Summary_address { get; set; }

        public string search_teacher_name { get; set; }
        public string search_teacher_id { get; set; }
        public string search_teacher_email_id { get; set; }
        public string search_teacher_mobile_no { get; set; }

        public string search_user_name { get; set; }
        public string search_user_email_id { get; set; }

        public string mother_name { get; set; }
        public string mother_email_id { get; set; }
        public string mother_mobile_no { get; set; }


        public string guardian_name { get; set; }
        public string guardian_email_id { get; set; }
        public string guardian_mobile_no { get; set; }
        public string designation { get; set; }
        public string designation_name { get; set; }
        public string Sdate { get; set; }

        public string sims_academic_year { get; set; }
        public string sims_academic_year_status { get; set; }

        public bool isSelected { get; set; }

        //BY 'My Query' Tab
        public string search_query { get; set; }
        public string search_code { get; set; }

        public string sims_search_code { get; set; }
        public string sims_search_desc { get; set; }
        public string sims_search_short_name { get; set; }
        public string sims_search_status { get; set; }
        public string sims_search_user_code { get; set; }
        public string sims_search_query { get; set; }
        public string sims_search_user_status { get; set; }
        public string sims_search_user_name { get; set; }

        public string sims_cur_short_name_en { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_passport_name_en { get; set; }
        public string sims_student_nickname { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_religion_name_en { get; set; }
        public string sims_nationality_name_en { get; set; }
        public string sims_student_remark { get; set; }
        public string sims_house_name { get; set; }
        public string sims_parent_father_name { get; set; }
        public string sims_parent_mother_name { get; set; }
        public string sims_parent_guardian_name { get; set; }
        public string sims_parent_father_family_name { get; set; }
        public string sims_parent_mother_family_name { get; set; }
        public string sims_parent_guardian_family_name { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_guardian_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_guardian_email { get; set; }
        public string sims_subject_name_en { get; set; }

        public string status { get; set; }
        public string status_name { get; set; }
        //End of Search

        public string s_cur_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string std_national_id { get; set; }

        public string joining_date { get; set; }
        public string exp_date { get; set; }
        public string renew_date { get; set; }

        public string sims_student_ea_number { get; set; }

        public string cancelled_date { get; set; }

        public string search_std_middle_name { get; set; }

        public string search_std_last_name { get; set; }

        public string search_parent_mother_name { get; set; }

        public string search_parent_mother_email_id { get; set; }

        public string search_parent_mother_mobile_no { get; set; }

        public string search_parent_emp_id { get; set; }

        public string sims_parent_emp_id { get; set; }

        public string search_mother_first_name { get; set; }

        public string search_mother_last_name { get; set; }

        public string search_mother_midd_name { get; set; }

        public string search_guardian_first_name { get; set; }

        public string search_guardian_last_name { get; set; }

        public string search_guardian_midd_name { get; set; }

        public string membership_type { get; set; }

        public string sims_enroll_number { get; set; }

        public string student_name { get; set; }

        public string sims_parent_number { get; set; }

        public string parent_name { get; set; }

        public string sims_student_dob { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_father_area_number { get; set; }
        public string sims_student_date { get; set; }
        public string sims_student_transport_status { get; set; }
        public string nationality { get; set; }
        public string rfid_card { get; set; }
        public string sims_transport_grade_code { get; set; }
        public string sims_transport_section_code { get; set; }
        
    }    //end_pooja

    #region Sims001_curlevel
    public class Sims001_curlevel
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_level_code { get; set; }
        public string sims_cur_level_name_en { get; set; }
    }
    #endregion

    //#region Sims084 (Transport Stop)
    //public class Sims084
    //{
    //    public string sims_academic_year { get; set; }
    //    public int sims_transport_stop_code { get; set; }
    //    public string sims_transport_stop_name { get; set; }
    //    public string sims_transport_stop_landmark { get; set; }
    //    public string sims_transport_stop_lat { get; set; }
    //    public string sims_transport_stop_long { get; set; }
    //    public bool sims_transport_stop_status { get; set; }

    //}
    //#endregion

    //#region Sims085 (Transport Route Stop)
    //public class Sims085
    //{
    //    public string sims_academic_year { get; set; }
    //    //public string sims_transport_route_code { get; set; }
    //    public string sims_transport_route_code_name { get; set; }
    //    public string sims_transport_route_direction { get; set; }
    //    public string sims_transport_route_stop_code { get; set; }
    //    public string sims_transport_route_stop_codename { get; set; }
    //    public string sims_transport_route_stop_expected_time { get; set; }
    //    public string sims_transport_route_stop_waiting_time { get; set; }
    //    public bool sims_transport_route_stop_status { get; set; }
    //    public string sims_transport_route_direction_name { get; set; }
    //    public string sims_transport_route_code { get; set; }
    //}
    //#endregion






    //#region Sims088 (Transport Driver)
    //public class Sims088
    //{
    //    public string sims_employee_code_name { get; set; }
    //    public string sims_user_code_name { get; set; }
    //    public string sims_driver_code { get; set; }
    //    public string sims_driver_name { get; set; }
    //    public string sims_driver_employee_code { get; set; }
    //    public string sims_employee_name { get; set; }
    //    public string sims_driver_user_code { get; set; }
    //    public string sims_driver_type { get; set; }
    //    public string sims_driver_type_name { get; set; }
    //    public string sims_driver_img { get; set; }
    //    public string sims_driver_img_url { get; set; }
    //    public string sims_driver_experience_years { get; set; }
    //    public string sims_driver_gender { get; set; }
    //    public string sims_driver_address { get; set; }
    //    public string sims_driver_mobile_number1 { get; set; }
    //    public string sims_driver_mobile_number2 { get; set; }
    //    public string sims_driver_mobile_number3 { get; set; }
    //    public string sims_driver_driving_license_number { get; set; }
    //    public DateTime? sims_driver_date_of_birth { get; set; }
    //    public DateTime? sims_driver_license_issue_date { get; set; }
    //    public DateTime? sims_driver_license_expiry_date { get; set; }
    //    public string sims_driver_license_place_of_issue { get; set; }
    //    public string sims_driver_license_vehicle_category { get; set; }
    //    public string sims_driver_license_vehicle_mode { get; set; }
    //    public string sims_driver_visa_number { get; set; }
    //    public DateTime? sims_driver_visa_issue_date { get; set; }
    //    public DateTime? sims_driver_visa_expiry_date { get; set; }
    //    public string sims_driver_visa_issuing_place { get; set; }
    //    public string sims_driver_visa_issuing_authority { get; set; }
    //    public string sims_driver_visa_type { get; set; }
    //    public string sims_driver_national_id { get; set; }
    //    public DateTime? sims_driver_national_id_issue_date { get; set; }
    //    public DateTime? sims_driver_national_id_expiry_date { get; set; }
    //    public bool sims_driver_status { get; set; }
    //    public string sims_driver_license_vehicle_categoryname { get; set; }
    //    public string sims_driver_license_vehicle_modename { get; set; }
    //}
    //#endregion

    //#region Sims087 (Transport Caretaker)
    //public class Sims087
    //{
    //    public string sims_caretaker_code { get; set; }
    //    public string sims_caretaker_name { get; set; }
    //    //public string sims_transport_route_caretaker_codename {get; set;}
    //    //public string sims_employee_code { get; set; }
    //    public string sims_employee_code_name { get; set; }
    //    public string sims_user_code { get; set; }
    //    public string sims_user_code_name { get; set; }
    //    public string sims_caretaker_img { get; set; }
    //    public string sims_caretaker_img_url { get; set; }
    //    public string sims_experience_years { get; set; }
    //    public bool sims_caretaker_status { get; set; }
    //    public string sims_caretaker_info { get; set; }

    //}
    //#endregion

    //#region Sims086 (Transport Vehicle)
    //public class Sims086
    //{
    //    public string sims_transport_vehicle_code { get; set; }
    //    public string sims_transport_vehicle_registration_number { get; set; }
    //    public string sims_transport_vehicle_name_plate { get; set; }
    //    public string sims_transport_vehicle_ownership { get; set; }
    //    public string sims_transport_vehicle_ownership_name { get; set; }
    //    public string sims_transport_manufacturer_name { get; set; }
    //    public string sims_transport_vehicle_model_name { get; set; }
    //    public string sims_transport_vehicle_model_year { get; set; }
    //    public string sims_transport_vehicle_registration_date { get; set; }
    //    public string sims_transport_vehicle_registration_expiry_date { get; set; }
    //    public int sims_transport_vehicle_seating_capacity { get; set; }
    //    public string sims_transport_vehicle_power { get; set; }
    //    public string sims_transport_vehicle_transmission { get; set; }
    //    public string sims_transport_vehicle_transmission_name { get; set; }
    //    public string sims_transport_vehicle_color { get; set; }
    //    public string sims_transport_vehicle_date_of_purchase { get; set; }
    //    public bool sims_transport_vehicle_camera_enabled { get; set; }
    //    public bool sims_transport_vehicle_security_enabled { get; set; }
    //    public bool sims_transport_vehicle_status { get; set; }
    //}
    //#endregion

    #region Sims006(Ethnicity)
    public class Sims006
    {
        public string sims_ethnicity_code { get; set; }
        public string sims_ethnicity_name_en { get; set; }
        public string sims_ethnicity_name_ar { get; set; }
        public string sims_ethnicity_name_fr { get; set; }
        public string sims_ethnicity_name_ot { get; set; }
        public bool sims_ethnicity_status { get; set; }
    }
    #endregion



    public class EmailT
    {
        public string sims_email_attachment { get; set; }
        public string sims_email_date { get; set; }
        public string sims_email_father_email_id1 { get; set; }
        public string sims_email_message { get; set; }
        public string sims_email_number { get; set; }
        public string sims_email_subject { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_sender_email_id { get; set; }
        public string sims_user_name { get; set; }
    }




    public class Sims040
    {
        public string sibling_student_enrollNo { get; set; }
        public string sibling_student_name { get; set; }
        public string sibling_student_img { get; set; }
        public string sibling_parent_code { get; set; }
        public string sibling_parent_name { get; set; }
        public string sibling_parent_img { get; set; }
        public string sims_sibling_student_enroll_number { get; set; }
        public string sims_sibling_parent_number { get; set; }


        public string sims_sibling_old_parent_number { get; set; }
    }
    public class Uccw035
    {
        public string sib_name { get; set; }
        public string sib_enroll { get; set; }
        public string sib_admiss_date { get; set; }
        public string sib_photo { get; set; }
        public string sib_gender { get; set; }
        public string sib_class { get; set; }
        public string parent_name { get; set; }
        public string parent_number { get; set; }
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string section_code { get; set; }

        public string sims_parent_father_nationality1_code { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_father_country_code { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_father_passport_number { get; set; }
        public string sims_parent_father_img { get; set; }
    }





    #region Sims160(Homeroom Batch Student)
    public class Sims160
    {
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_batch_code { get; set; }
        public string sims_batch_name { get; set; }
        public string sims_batch_enroll_number { get; set; }
        public string sims_batch_enroll_name { get; set; }
        public bool sims_batch_status { get; set; }
        public string sims_student_name { get; set; }



        public string sims_homeroom_code { get; set; }

        public string sims_homeroom_name { get; set; }

        public string sims_student_passport_first_name_en { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string opr { get; set; }
    }
    #endregion

    //#region Sims182 CouncilDetails
    //public class Sims182
    //{
    //    public string sims_council_code { get; set; }
    //    public string sims_council_name { get; set; }
    //    public bool sims_council_status { get; set; }
    //    public string sims_cur_code { get; set; }
    //    public string sims_cur_short_name { get; set; }
    //    public string sims_academic_year_description { get; set; }
    //    public string sims_academic_year { get; set; }
    //}

    //#endregion   




    #region Sims161(Homeroom Teacher)
    public class Sims161
    {
        public string sims_cur_short_name_en;
        //public string sims_cur_name {get;set;}
        //public string sims_academic_year {get;set;}
        public string sims_homeroom_name { get; set; }
        public string sims_homeroom_teacher_code { get; set; }
        public string sims_homeroom_teacher_name { get; set; }
        public bool sims_status { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_homeroom_code { get; set; }

        public string sims_cur_code { get; set; }

        public string opr { get; set; }

        public string sims_homeroom_teacher_code1 { get; set; }

        public string sims_batch_name { get; set; }

        public string sims_batch_code { get; set; }

        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string old_sims_homeroom_teacher_code { get; set; }
    }
    #endregion


    public class License
    {
        public string lic_lms_url { get; set; }
        public string opr { get; set; }
        public string lic_school_code { get; set; }
        public string lic_school_name { get; set; }
        public string lic_school_address { get; set; }
        public string lic_contact_person { get; set; }
        public string lic_fax_no { get; set; }
        public string lic_tel_no { get; set; }
        public string lic_email { get; set; }
        public string lic_website_url { get; set; }
        public string lic_school_region { get; set; }
        public string lic_school_country { get; set; }
        public string lic_school_curriculum { get; set; }
        public string lic_school_color_code1 { get; set; }
        public string lic_school_color_code2 { get; set; }
        public string lic_school_color_code3 { get; set; }
        public string lic_product_key { get; set; }
        public string lic_product_name { get; set; }
        public string lic_product_type { get; set; }
        public string lic_product_mem_qty { get; set; }
        public string lic_product_mem_qty_prorata { get; set; }
        public string lic_product_amt { get; set; }
        public string lic_agreement_date { get; set; }
        public string lic_subscription_date { get; set; }
        public string lic_subscription_expiry_date { get; set; }
        public string lic_grace_30 { get; set; }
        public string lic_product_key_30 { get; set; }
        public string lic_grace_60 { get; set; }
        public string lic_product_key_60 { get; set; }
        public string lic_grace_90 { get; set; }
        public string lic_product_key_90 { get; set; }
        public bool lic_status { get; set; }
        public string lic_school_logo { get; set; }
        public string lic_school_lms_link { get; set; }
        public string lic_time_zone { get; set; }

        public string stringlic_subscription_date { get; set; }
        public string lic_lat { get; set; }
        public string expirytime { get; set; }
        public string lic_long { get; set; }
        public string lic_school_short_name { get; set; }
        public string lic_school_other_name { get; set; }
        public string lic_product_mem_qty_prora { get; set; }
        public string lic_subscription_expiry_old_date { get; set; }
        public string datediffre1 { get; set; }
        public string datediffre2 { get; set; }
        public string datediffre3 { get; set; }
        public string lic_school_type { get; set; }
        public string lic_school_type_name { get; set; }

        public string lic_school_microsoft_client_id { get; set; }

        public string lic_school_microsoft_login { get; set; }

        public string lic_school_microsoft_redirect_url { get; set; }

        public string lic_school_microsoft_tenant_id { get; set; }
    }

    #region Sims166(Alert)
    public class Sims166
    {
        public string alert_number { get; set; }
        public string alert_user_code { get; set; }
        public string alert_mod_code { get; set; }
        public string alert_mod_code1 { get; set; }
        public string alert_date { get; set; }
        public string alert_message { get; set; }
        public string alert_overdue_day { get; set; }
        public bool alert_status { get; set; }
        public string alert_type_code { get; set; }
        public string alert_priority_code { get; set; }
        public string alert_overdue_day_msg { get; set; }
    }
    #endregion

    #region Sims008(Nationality)
    public class Sims008
    {
        public string nationality_code { get; set; }
        public string nationality_country_code { get; set; }
        public string nationality_country_name { get; set; }
        public string nationality_name_eng { get; set; }
        public string sims_country_name_en { get; set; }
        public string nationality_name_ar { get; set; }
        public string nationality_name_fr { get; set; }
        public string nationality_name_ot { get; set; }
        public bool nationality_status { get; set; }
        public string sims_country_code { get; set; }

        public string opr { get; set; }
    }
    #endregion

    public class country1
    {
        public string sims_country_code { get; set; }
        public string sims_country_name_en { get; set; }
        public string sims_country_name_ar { get; set; }
        public string sims_country_name_fr { get; set; }
        public string sims_country_name_ot { get; set; }
        public string sims_continent { get; set; }
        public string sims_region_code { get; set; }
        public string sims_region_code_name { get; set; }
        public string sims_currency_code { get; set; }
        public string sims_currency_code_name { get; set; }
        public string sims_currency_dec { get; set; }
        public string sims_country_img { get; set; }
        public bool sims_status { get; set; }
    }

    #region Sim501

    public class Sim501
    {
        public string sims_field_code { get; set; }
        public string sims_field_name { get; set; }
        public bool sims_field_iseditable { get; set; }
        public bool sims_field_isvisible { get; set; }
    }
    #endregion

    #region Sim156

    public class Sim156
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_category_code { get; set; }
        public string sims_attendance_code { get; set; }
        public string sims_attendance_code_numeric { get; set; }
        public string sims_attendance_short_desc { get; set; }
        public string sims_attendance_description { get; set; }
        public bool sims_attendance_teacher_can_use_flag { get; set; }
        public string sims_attendance_color_code { get; set; }
        public bool sims_attendance_status { get; set; }
        public string sims_bell_slot_code { get; set; }
        public string sims_bell_slot_desc { get; set; }
    }
    #endregion

    #region Sims092-Portal Refrence

    public class Sims092
    {

        public string sims_cur_name { get; set; }
        public string sims_reference_number { get; set; }
        public string sims_reference_name { get; set; }
        public string sims_reference_filename { get; set; }
        public bool sims_reference_status { get; set; }
        public string sims_reference_publish_date { get; set; }
        public string sims_reference_expiry_date { get; set; }
        public string sims_reference_desc { get; set; }
        public bool sims_learn_arabic_status { get; set; }
        public string sims_timetable_filename { get; set; }
        public string sims_cur_code { get; set; }
        public bool sims_reference_video_resource { get; set; }
        public string opr { get; set; }
    }
    #endregion


    #region Sims126(Library Membership Type)
    public class Sims126
    {
        public string sims_library_membership_type_code { get; set; }
        public string sims_library_membership_type_name { get; set; }
        public bool sims_library_membership_type_status { get; set; }
    }
    #endregion

    public class Sims201
    {
        public string sims_enroll_number { get; set; }
        public string sims_device_type { get; set; }
        public string sims_device_value { get; set; }
        public bool sims_device_status { get; set; }
        public string sims_device_type_code { get; set; }


        public string ennumber { get; set; }
    }
    //Sims015

    #region Sims015
    public class Sims015
    {
        public string sims_calendar_number { get; set; }
        public string sims_calendar_date { get; set; }
        public string sims_calendar_end_date { get; set; }
        public string sims_calendar_event_start_time { get; set; }
        public string sims_calendar_event_end_time { get; set; }
        public string sims_calendar_event_type { get; set; }
        public string sims_calendar_event_category { get; set; }
        public string sims_calendar_event_priority { get; set; }
        public string sims_calendar_event_short_desc { get; set; }
        public string sims_calendar_event_desc { get; set; }
        public string sims_calendar_event_status { get; set; }
        public string sims_calendar_time_marker { get; set; }

        //parameter -category
        public string sims_calendar_sims_appl_parameter { get; set; }
        public string sims_calendar_sims_appl_form_field_value1 { get; set; }
        public string sims_calendar_sims_appl_form_field_value2 { get; set; }
        public string sims_calendar_sims_appl_form_field_value3 { get; set; }

        //parameter-TimeMaker
        public string sims_calendar_sims_appl_TimeMarker_parameter { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value1 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value2 { get; set; }
        public string sims_calendar_sims_appl_TimeMarker_form_field_value3 { get; set; }
    }
    #endregion

    public class Sims179
    {

        public string sims_term_desc_en;
        public string sims_academic_year_description { get; set; }

        public string sims_circular_type  { get; set; }
        public string sims_circular_desc  { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_desc_ar { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_class_name { get; set; }
        public string sims_emp_code { get; set; }
        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public bool sims_status { get; set; }

        public string sims_section_code { get; set; }

        public string sims_academic_year_desc { get; set; }
        public string sims_academic_year_status { get; set; }
        public string doc_no { get; set; }
        public string dd_fee_number { get; set; }
        public string sims_fee_code_description { get; set; }
        public string fee_paid { get; set; }
        public string dd_fee_payment_mode { get; set; }
        public string enroll_number { get; set; }
        public string doc_date { get; set; }
        public string student_name { get; set; }
        public string doc_reference_no { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string fee_status_desc { get; set; }
        public string bank_name { get; set; }
        public string cheque_number { get; set; }
        public string dd_fee_cheque_date { get; set; }

        //public string sims_term_desc_en { get; set; }

        public string sims_cur_name { get; set; }

        public string TotalCount { get; set; }

        public string sims_academic_year_start_date { get; set; }

        public string sims_academic_year_end_date { get; set; }

        public string comn_mod_code { get; set; }

        public string comn_mod_name_en { get; set; }

        public string em_first_name { get; set; }
    }

    #region Sims009-Religion
    public class Sims009
    {
        public string opr { get; set; }
        public string sims_religion_code { get; set; }
        public string sims_religion_name_en { get; set; }
        public string sims_religion_name_ot { get; set; }

        public string sims_religion_name_ar { get; set; }
        public string sims_religion_name_fr { get; set; }
        public string sims_religion_img { get; set; }
        public string sims_religion_img_url { get; set; }
        public bool sims_religion_status { get; set; }
    }
    #endregion

    //comn

    public class Comn005
    {
        public string Group_Code { get; set; }
        public string User_Name { get; set; }
        public string Enrollment_Number { get; set; }
        public string Employee_code { get; set; }
        public string Email_id { get; set; }
        public string Date_Creation { get; set; }
        public string User_Code { get; set; }
        public string User_password { get; set; }
        public string Expiry_Date { get; set; }
        public string Last_Login { get; set; }
        public bool Captcha_Status { get; set; }
        public bool User_Status { get; set; }
        public string User_State { get; set; }
        public string user_image { get; set; }
        public string userstatue_code { get; set; }
        public string UserAlertCount { get; set; }
        public string UserCircularCount { get; set; }
        public string UserNewsCount { get; set; }
        public string messageCount { get; set; }

        public string Group_Nm { get; set; }
        public string FullNameEn { get; set; }
        public string Image { get; set; }
        public string date_of_birth { get; set; }
        public string em_date_of_join { get; set; }
        public string comn_user_phone_number { get; set; }
        public string comn_user_alias { get; set; }

        public string Designation { get; set; }
        public string DesigLtr { get; set; }
        public string paglink { get; set; }
        public bool isAdmin { get; set; }
        public string comp { get; set; }
        // Class For comn user
        public string comn_user_code { get; set; }
        public string comn_user_name { get; set; }
        public string comn_user_password { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string comn_user_date_created { get; set; }
        public bool comn_user_captcha_status { get; set; }
        public string comn_user_email { get; set; }
        public string comn_user_secret_question_code { get; set; }
        public string comn_user_secret_question { get; set; }
        public string comn_user_secret_answer { get; set; }
        public string comn_user_status { get; set; }
        public string comn_user_status_name { get; set; }
        public string comn_user_expiry_date { get; set; }
        public string comn_user_remark { get; set; }

        public string opr { get; set; }

        public bool user_status1 { get; set; }

        public string comn_user_last_login { get; set; }
        public string lic_school_name { get; internal set; }
        public string lic_school_logo { get; internal set; }
    }

    public class Sim987
    {

        public string sims_admission_sen_compelted_status { get; set; }
        public bool sims_admission_sen_approve_status { get; set; }
        public string sims_admission_number { get; set; }
        public string sims_admission_date { get; set; }
        public string name { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_admission_dob { get; set; }
        public string sims_admission_mobile { get; set; }
        public string admission_class { get; set; }
        public string show_button { get; set; }
        public string sims_admission_academic_year { get; set; }
        public string sims_admission_grade_code { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string opr { get; set; }
        public bool sims_admission_special_education_status { get; set; }



        public string sims_sen_status_remark { get; set; }

        public string sims_sen_approve_remark { get; set; }

        public string sims_admission_sen_status_created_by { get; set; }
        public string sims_admission_sen_status_created_date { get; set; }
        public string sims_admission_sen_approve_created_by { get; set; }
        public string sims_admission_sen_approve_created_date { get; set; }
        public string value { get; set; }
        public string user_name { get; set; }
        public string sims_admission_sr_no { get; set; }
        public string sims_sen_document_transaction_number { get; set; }
        public string sims_sen_document_admission_number { get; set; }
        public string sims_sen_document_number { get; set; }
        public string sims_sen_document_name { get; set; }
        public string sims_sen_document_remark { get; set; }
        public string sims_sen_document_created_by { get; set; }
        public string sims_sen_document_created_date { get; set; } 

    }

    public class Sim999
    {

        // public string sims_sr_no { get; set; }
        public string opr { get; set; }
        public string empName { get; set; }
        public string em_number { get; set; }
        public string sims_sr_no { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_list { get; set; }
        public bool sims_status { get; set; }
        public string sims_clr_emp_id { get; set; }
        public bool sims_fee_clr_status { get; set; }
        public bool sims_finn_clr_status { get; set; }
        public bool sims_inv_clr_status { get; set; }
        public bool sims_inci_clr_status { get; set; }
        public bool sims_lib_clr_status { get; set; }
        public bool sims_trans_clr_status { get; set; }
        public bool sims_acad_clr_status { get; set; }
        public bool sims_admin_clr_status { get; set; }
        public bool sims_other1_clr_status { get; set; }
        public bool sims_other2_clr_status { get; set; }
        public bool sims_other3_clr_status { get; set; }
        public bool sims_clr_emp_status { get; set; }
        public bool sims_clr_super_user_flag { get; set; }

        public string grades { get; set; }
        public bool dstatus { get; set; }

        public string sims_clr_cur_code { get; set; }
        public string sims_clr_academic_year { get; set; }
        public string sims_clr_employee_id { get; set; }
        public string sims_clr_access_code { get; set; }
        public string sims_clr_class_code { get; set; }
        public string sims_clr_preference_order { get; set; }
        public string sims_clr_status { get; set; }
        public string sims_clr_created_by { get; set; }
        public string sims_clr_created_date { get; set; }
        public string sims_clr_access_from_date { get; set; }
        public string sims_clr_access_to_date { get; set; }
        public string em_first_name { get; set; }
        public string comn_mod_name_en { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_clr_sr_no { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_clr_is_super_user { get; set; }
    }

    public class AlertConfigObj
    {
        public string sims_scriptid;
        public string status;
        public string opr;
        public string sims_scriptname { get; set; }
        public string sims_alert_desc { get; set; }
        public string sims_alertdays { get; set; }
        public string sims_control_field { get; set; }
        public string sims_status { get; set; }
        
        public bool sims_status1 { get; set; }
        public string sims_alert_name { get; set; }
        public string sims_startdate { get; set; }
        public string sims_nextdate { get; set; }
    }


    public class Sims540
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public bool sims_status { get; set; }

        public string sims_section_name_en { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_grade_name_en { get; set; }
    }


    #region Update Fee Category
    public class Sims566
    {
        public string cur { get; set; }
        public string cur_desc { get; set; }
        public string aca { get; set; }
        public string aca_desc { get; set; }
        public string aca_status { get; set; }
        public string grade { get; set; }
        public string grade_desc { get; set; }
        public string sec { get; set; }
        public string sec_desc { get; set; }
        public string enroll { get; set; }
        public string stud_name { get; set; }
        public string fee_code { get; set; }
        public string fee_cat { get; set; }

    }
    #endregion

    public class Comn013
    {
        public int comn_master_sp_code { get; set; }
        public string comn_master_sp_name { get; set; }
        public string comn_master_sp_type_code { get; set; }
        public string comn_master_sp_type { get; set; }
        public bool comn_master_sp_read { get; set; }
        public bool comn_master_sp_insert { get; set; }
        public bool comn_master_sp_update { get; set; }
        public bool comn_master_sp_delete { get; set; }
        public string comn_master_sp_remark { get; set; }
        public string comn_master_sp_mod_name { get; set; }
        public string comn_master_sp_appl_name { get; set; }
        public string comn_master_sp_appl_location { get; set; }
        public string comn_master_sp_appl_logic { get; set; }
        public int comn_master_sp_appl_display_order { get; set; }
        public bool comn_master_sp_appl_status { get; set; }
        public string comn_appl_code { get; set; }
    }


    #region Transport_Route(PP011)
    public class PP011
    {
        public string sims_student_name { get; set; }
        public string sims_transport_enroll_number { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string sims_transport_route_code_name { get; set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_academic_year { get; set; }
        public string sims_transport_route_code { get; set; }
        public string sims_transport_effective_from { get; set; }
        public string sims_transport_effective_upto { get; set; }
        public string sims_transport_pickup_stop_code { get; set; }
        public string sims_transport_pickup_stop_name { get; set; }
        public string sims_transport_drop_stop_code { get; set; }
        public string sims_transport_drop_stop_name { get; set; }
        public string sims_student_img { get; set; }

        public string sims_driver_name { get; set; }

        public string sims_caretaker_name { get; set; }
    }
    #endregion

    #region Sims059(Section Term)
    public class Sims059
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_section_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_term_start_date { get; set; }
        public string sims_term_end_date { get; set; }
        public string sims_report_section_code { get; set; }
        public string sims_report_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_code_name { get; set; }
        public string section_fee_section_name { get; set; }
        public string screening_section_code_name { get; set; }
        public string std_section_name_en { get; set; }
        public string section_term_cur_level_name { get; set; }
        public string section_term_cur_level_code { get; set; }
        public string section_term_code { get; set; }
        public string section_term_name { get; set; }
        public string section_term_start_date { get; set; }
        public string section_term_end_date { get; set; }
        public string section_term2_name { get; set; }
        public string section_term2_code { get; set; }
        public string section_term2_start_date { get; set; }
        public string section_term2_end_date { get; set; }
        public string section_term3_name { get; set; }
        public string section_term3_code { get; set; }
        public string section_term3_start_date { get; set; }
        public string section_term3_end_date { get; set; }
        public string section_term4_name { get; set; }
        public string section_term4_code { get; set; }
        public string section_term4_start_date { get; set; }
        public string section_term4_end_date { get; set; }
        public string section_term5_name { get; set; }
        public string section_term5_code { get; set; }
        public string section_term5_start_date { get; set; }
        public string section_term5_end_date { get; set; }
        public string section_term6_name { get; set; }
        public string section_term6_code { get; set; }
        public string section_term6_start_date { get; set; }
        public string section_term6_end_date { get; set; }
        public bool section_term_status { get; set; }

        public string sims_cur_short_name_en { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_term_code { get; set; }
    }
    #endregion

    #region Sims024(Grade)
    public class Sims024
    {

        public string grade_cur_code { get; set; }
        public string grade_cur_code_name { get; set; }
        public string grade_academic_year { get; set; }
        public string grade_academic_year_desc { get; set; }
        public string grade_name_en { get; set; }
        public string grade_name_ar { get; set; }
        public string grade_name_fr { get; set; }
        public string grade_name_ot { get; set; }
        public string grade_code { get; set; }

        public string opr { get; set; }

        public int myid { get; set; }

        public bool sims_grade_status { get; set; }

        public string sims_display_order { get; set; }

        public string  comp_code{ get; set; }
        public string comp_name { get; set; }


        public string house_name { get; set; }
        
        public string sims_parent_number { get; set; }
    }


    //public class Sims02422
    //{
    //    public bool count { get; set; }
    //}
    
    #endregion

    #region  section
    public class Sims035
    {
        public string sims_academic_year { get; set; }
        public string _sims_grade_name { get; set; }
        public string _sims_section_name { get; set; }
        public string sims_section_stregth { get; set; }
        public string sims_section_gender { get; set; }
        public string sims_section_promote_code { get; set; }
        public string sims_section_boys_promote_code { get; set; }
        public string sims_section_boys_promote_desc { get; set; }
        public string sims_section_girls_promote_code { get; set; }
        public string sims_section_girls_promote_desc { get; set; }
        public bool sims_status { get; set; }
        public string sims_section_name_ar { get; set; }
        public string sims_section_name_fr { get; set; }
        public string sims_section_name_ot { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string cnt { get; set; }
        public string sims_cur_code { get; set; }
        public string grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public bool mapped { get; set; }

        public string sims_cur_name { get; set; }

        public string sims_academic_year_desc { get; set; }

        public string grade_name_en { get; set; }

        public string opr { get; set; }

        public string sims_section_gender_code { get; set; }

        public string sims_section_promote_desc { get; set; }

        public string sims_section_gender_desc { get; set; }

        public short count { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_section_order_no { get; set; }

            public string lic_school_code { get; set; }

        public string school_name { get; set; }
        public string sims_school_code { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_section_shift_code { get; set; }
      
    }
    #endregion

    #region Sims110(Certificate User Group)
    public class Sims110
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_certificate_number { get; set; }
        public string sims_certificate_name { get; set; }
        public string sims_user_group_code { get; set; }
        public string sims_user_group_name { get; set; }
        public bool sims_user_group_enable { get; set; }
    }
    #endregion

    #region  Invs005

    public class Invs005
    {

        public string dr_code { get; set; }
        public string dept_code { get; set; }
        public string dr_desc { get; set; }
        public string dr_account_no { get; set; }
        public string glma_comp_code { get; set; }
        public string glma_dept_no { get; set; }
        public string glma_acct_code { get; set; }
        public string Saleaccount { get; set; }
        public string glma_acct_name { get; set; }
        public string account_code { get; set; }
        public string dep_code { get; set; }
        public string dep_name { get; set; }
        public string opr { get; set; }

        public string pc_code { get; set; }
        public string pc_fin_year { get; set; }
        public string pc_revenue_acno { get; set; }
        public string pc_stock_acno { get; set; }
        public string pc_cost_acno { get; set; }
        public string pc_discont_acno { get; set; }
        public bool pc_status { get; set; }
        public string pc_desc { get; set; }


        public string revenue_accno { get; set; }
        public string revenue_acct_name { get; set; }
        public string stock_accno { get; set; }
        public string stock_acct_name { get; set; }
        public string cost_accno { get; set; }
        public string cost_acct_name { get; set; }

        public string discount_accno { get; set; }
        public string discount_acct_name { get; set; }
    }

    #endregion

    #region  Invs017
    public class Invs017
    {
        public string invs_code { get; set; }
        public string invs_name { get; set; }
        public decimal invs_lead_time { get; set; }
        public string invs_type { get; set; }
    }

    #endregion

    //Finn043 (Currency)
    public class Finn043
    {
        public string excg_comp_code { get; set; }
        public string excg_comp_code_name { get; set; }

        public string excg_curcy_code { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal excg_min_rate { get; set; }
        public decimal excg_max_rate { get; set; }
        public string excg_deci_desc { get; set; }
        public int excg_curcy_decimal { get; set; }
        public bool excg_enabled_flag { get; set; }
        public decimal excg_reval_rate { get; set; }
        public string excg_effective_date { get; set; }
    }

    public class fins138
    {

        public string pb_bank_code { get; set; }
        public string pb_bank_name { get; set; }
        public string pb_gl_acno { get; set; }
        public string pb_gl_acno_name { get; set; }
        public string pb_sl_code { get; set; }
        public string pb_sl_code_name { get; set; }
        public string pb_sl_acno { get; set; }
        public string pb_sl_acno_name { get; set; }
        public string pb_srl_no { get; set; }
        public string pb_comp_code { get; set; }
    }

    public class municipality
    {
        public string opr { get; set; }
        public Boolean municipality_status { get; set; }
        public string municipality_code { get; set; }
        public string english_name { get; set; }
        public string arabic_name { get; set; }
        public string other_name { get; set; }
    }
    public class municipalityZone
    {
        public string opr { get; set; }
        public Boolean municipalityZone_status { get; set; }
        public string municipality_code { get; set; }
        public string municipalityZone_code { get; set; }
        public string municipality_name { get; set; }
        public string english_name { get; set; }
        public string arabic_name { get; set; }
        public string other_name { get; set; }
        public string sims_municipalityZone_mother_area { get; set; }
        public string sims_municipalityZone_father_area { get; set; }

        public string sims_municipalityZone_guardian_area { get; set; }
        public string sims_municipalityZone_mother_area_code { get; set; }
        public string sims_municipalityZone_father_area_code { get; set; }
        public string sims_municipalityZone_guardian_area_code { get; set; }

        public string sims_municipality_code { get;set; }
        public string sims_municipality_name_en { get; set; }

    }

    #region Invs138
    public class Invs138
    {

        public string lc_date { get; set; }
        public string bk_code { get; set; }
        public string cur_code { get; set; }
        public string lc_exchange_rate { get; set; }
        public string sup_code { get; set; }
        public string lc_bank_lc_no { get; set; }
        public string lc_open_date { get; set; }
        public string lc_beneficiary { get; set; }
        public string lc_amount { get; set; }
        public string lc_revised_amount { get; set; }
        public string lc_utilized_amount { get; set; }
        public string lc_cancelled_amount { get; set; }
        public string lc_bank_charges { get; set; }
        public string lc_valid_date { get; set; }
        public string lc_terms { get; set; }
        public string lc_remarks { get; set; }
        public string lc_close_date { get; set; }
        public string lc_no1 { get; set; }
        public string bk_name { get; set; }
        public string sup_name { get; set; }
        public string excg_curcy_desc { get; set; }
        public string excg_curcy_code { get; set; }
        public string opr { get; set; }

    }
    #endregion

    public class Invs018
    {
        public string company_code { get; set; }
        public string company_short_name { get; set; }
        public string company_name { get; set; }
        public string comp_currency_name { get; set; }
        public string comp_currency_code { get; set; }
    }

    //Invs020
    public class Invs020
    {
        public string cp_code { get; set; }
        public string cp_desc { get; set; }
        public string cp_value { get; set; }

    }


    #region Sims190
    public class Sims190
    {
        public string sims_behaviour_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_behaviour_code { get; set; }
        public string sims_behaviour_cur_level_code { get; set; }
        public string sims_cur_level_name_en { get; set; }
        public string sims_behaviour_type { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_behaviour_description { get; set; }
        public string sims_behaviour_points { get; set; }
        public bool sims_behaviour_status { get; set; }
        public string sims_behaviour_img { get; set; }

        public string sims_cur_level_code { get; set; }
         public string sims_cur_code { get; set; }
        public string sims_appl_parameter { get; set; }
        public string opr { get; set; }
        public string sims_behaviour_enroll_number { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_behaviour_date { get; set; }
        public string sims_behaviour_note { get; set; } 

    }
    #endregion

    public class Invs001
    {
        public string uom_code { get; set; }
        public string uom_name { get; set; }
    }

    #region  Sims030(MemberShip Fees)
    public class Sims030
    {
        public string sims_membership_fee_number { get; set; }
        public string sims_membership_fee_academic_year { get; set; }
        public string sims_membership_fee_grade_name { get; set; }
        public string sims_membership_fee_section_name { get; set; }
        public string sims_membership_fee_code { get; set; }
        public string sims_membership_fee_code_name { get; set; }
        public string sims_membership_fee_amount { get; set; }
        public bool sims_membership_fee_installment_mode { get; set; }
        public int sims_membership_fee_installment_min_amount { get; set; }
        public bool sims_membership_fee_status { get; set; }
        //End of Sims030  

    }
    #endregion

    #region Sims036(Section_Fee)
    public class Sims036
    {

        public string full_name { get; set; }
        public string sims_fee_new_amt { get; set; }

        public string sims_fee_period_code { get; set; }
        public string sims_fee_period_amt { get; set; }

        public string transport_fee_category_name { get; set; }
        public string transport_fee_code_desctiption { get; set; }
        public string transport_fee_frequency_name { get; set; }
        public string sims_library_fee_frequency { get; set; }

        public string section_fee_number { get; set; }
        public string section_fee_cur_code { get; set; }
        public string section_fee_cur_code_name { get; set; }
        //public string section_fee_academic_year { get; set; }
        public string section_fee_grade_code { get; set; }
        public string section_fee_grade_name { get; set; }
        public string section_fee_section_code { get; set; }
        public string section_fee_section_name { get; set; }
        public string section_fee_code { get; set; }
        public string section_fee_code_desctiption { get; set; }
        public string section_fee_code_type { get; set; }
        public string section_fee_code_type_name { get; set; }
        public bool section_fee_refundable { get; set; }
        public bool section_fee_manual_receipt { get; set; }
        public string section_fee_frequency { get; set; }
        public string section_fee_frequency_name { get; set; }
        public bool section_fee_mandatory { get; set; }
        public string section_fee_amount { get; set; }
        public bool section_fee_installment_mode { get; set; }
        public string section_fee_installment_min_amount { get; set; }
        public string section_fee_period1 { get; set; }
        public string section_fee_period2 { get; set; }
        public string section_fee_period3 { get; set; }
        public string section_fee_period4 { get; set; }
        public string section_fee_period5 { get; set; }
        public string section_fee_period6 { get; set; }
        public string section_fee_period7 { get; set; }
        public string section_fee_period8 { get; set; }
        public string section_fee_period9 { get; set; }
        public string section_fee_period10 { get; set; }
        public string section_fee_period11 { get; set; }
        public string section_fee_period12 { get; set; }
        public bool section_fee_status { get; set; }
        public string section_fee_category { get; set; }
        public string section_fee_category_name { get; set; }
        //End of Sims036

        public string section_fee_cur_code_name_to { get; set; }
        public string section_fee_level_code_name { get; set; }
        public string section_fee_academic_year_to { get; set; }
        public bool copyto { get; set; }
        public string section_fee_grade_name_to { get; set; }
        public string section_fee_grade_code_to { get; set; }
        public string section_fee_section_name_to { get; set; }
        public string section_fee_section_code_to { get; set; }
        public string section_fee_category_name_to { get; set; }
        public bool overwrite { get; set; }

        public bool section_fee_split_flag { get; set; }

        public string section_fee_academic_year { get; set; }

        public string section_name { get; set; }

        public string cur_name { get; set; }

        public string academic_year { get; set; }

        public string grade_name { get; set; }

        public string fee_category { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_cur_full_name { get; set; }

        public string sims_cur_short_name { get; set; }

        public string sims_term_start_date { get; set; }

        public string sims_month_code { get; set; }
        public string sims_academic_year_description { get; set; }
    }
    #endregion


    public class copy_section_fee
    {

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_fee_cat { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_fee_code { get; set; }

       public string sims_fee_cur_code_to { get; set; }
            public string sims_fee_academic_year_to { get; set; }
            public string sims_fee_grade_code_to { get; set; }
            public string sims_fee_section_code_to { get; set; }

    }

    #region Pers092 pays_shift_master
    public class Pers092
    {
        public string sh_company_code { get; set; }
        public string sh_company_name { get; set; }
        public string sh_shift_id { get; set; }
        public string sh_shift_desc { get; set; }
        public string sh_shift1_in { get; set; }
        public string sh_shift1_out { get; set; }
        public string sh_shift2_in { get; set; }
        public string sh_shift2_out { get; set; }
    }
    #endregion    

    public class SMTP_Details
    {
        public string SMTP_SchoolName { get; set; }
        public string SMTP_SchoolCode { get; set; }
        public string SMTP_Code { get; set; }
        public string SMTP_Name { get; set; }
        public string SMTP_ModCode { get; set; }
        public string SMTP_ModName { get; set; }
        public string SMTP_ServiceIP { get; set; }
        public string SMTP_Port { get; set; }
        public string SMTP_UserName { get; set; }
        public string SMTP_Password { get; set; }
        public string SMTP_FromEmail { get; set; }
        public string SMTP_Status { get; set; }
        public string SMTP_Authentication_Req { get; set; }
        public string SMTP_SSL_Req { get; set; }
        public string SMTP_Remark { get; set; }
        public string SMTP_MaxMailHour { get; set; }
        public string SMTP_MaxMailDay { get; set; }
        public string SMTP_EncryptMode { get; set; }
        public string SMTP_FromEmailName { get; set; }
        public string SMTP_Config_ID { get; set; }
        public string SMTP_Signature { get; set; }
        public string SMTP_Address { get; set; }

        public int total { get; set; }
    }

    public class Sims_Location
    {
        public string Sims_Location_Code { get; set; }
        public string Sims_Location_Desc { get; set; }
        public bool sims_location_status { get; set; }

        public string sims_location_desc { get; set; }

        public bool Sims_Location_Status { get; set; }
    }

    class Sims_Homeroom
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_homeroom_code { get; set; }
        public string sims_homeroom_name { get; set; }
        public bool sims_homeroom_status { get; set; }
        public string sims_homeroom_location_details { get; set; }


        public string sims_cur_full_name_en { get; set; }
    }

    public class Curr
    {
        public string curriculum_code { get; set; }
        public string curriculum_short_name { get; set; }
        public string curriculum_short_name_ar { get; set; }
        public string curriculum_short_name_fr { get; set; }
        public string curriculum_short_name_ot { get; set; }
        public string curriculum_full_name { get; set; }
        public string curriculum_full_name_ar { get; set; }
        public string curriculum_full_name_fr { get; set; }
        public string curriculum_full_name_ot { get; set; }
        public bool curriculum_status { get; set; }
    }

    public class Region
    {
        public string Sims_Region_code { get; set; }
        public string Sims_Region_name_en { get; set; }
        public string Sims_Region_name_ar { get; set; }
        public string Sims_Region_name_fr { get; set; }
        public string Sims_Region_name_ot { get; set; }
        public bool Sims_Region_status { get; set; }
    }

    #region House
    public class House1
    {
        public string Sims_House_Cur_Code { get; set; }
        public string Sims_House_Academic_Year { get; set; }
        public string Sims_House_Code { get; set; }
        public string Sims_House_Name { get; set; }
        public string Sims_House_Color { get; set; }
        public string Sims_House_Objective { get; set; }
        public string Sims_House_Pledge { get; set; }
        public bool Sims_House_Status { get; set; }

        public string sims_cur_short_name_en { get; set; }
    }

    #endregion

    public class Comn003
    {
        public string module_code { get; set; }
        public string module_name_en { get; set; }
        public string module_name_ar { get; set; }
        public string module_name_fr { get; set; }
        public string module_name_ot { get; set; }
        public string module_create_date { get; set; }
        public string module_short_desc { get; set; }
        public string module_desc { get; set; }
        public string module_keywords { get; set; }
        public bool module_status { get; set; }
        public string module_version { get; set; }
        public string module_color { get; set; }
        public string module_location { get; set; }
        public string module_dislpay_order { get; set; }
        public string module_Image { get; set; }
              public string opr { get; set; }
    }

    public class year
    {
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
    }

    #region ParentCirculer
    public class Uccw007
    {
        public string sims_circular_date { get; set; }
        public string sims_circular_publish_date { get; set; }
        public string sims_circular_title { get; set; }
        public string sims_circular_short_desc { get; set; }
        public string sims_circular_desc { get; set; }
        public string sims_circular_file_name1 { get; set; }
        public string sims_circular_file_name2 { get; set; }
        public string sims_circular_file_name3 { get; set; }
        public string sims_circular_file_path1 { get; set; }
        public string sims_circular_file_path2 { get; set; }
        public string sims_circular_file_path3 { get; set; }
        public bool ciculerfileflag { get; set; }
        public string sims_circular_number { get; set; }
    }
    #endregion

    //sanjay files
    #region Sims079(Transport_Fees)
    public class Sims079
    {
        public string sims_academic_year_description { get; set; }
        public string transport_fee_route_direction_name { get; set; }
        public string transport_fee_academic_year { get; set; }
        public string transport_fee_route_code { get; set; }
        public string transport_fee_route_code_name { get; set; }
        public string transport_fee_route_direction { get; set; }
        public string transport_fee_category_code { get; set; }
        public string transport_fee_category_name { get; set; }
        public string transport_fee_code { get; set; }
        public string transport_fee_code_desctiption { get; set; }
        public string transport_fee_code_type { get; set; }
        public string transport_fee_code_type_name { get; set; }
        public bool transport_fee_refundable { get; set; }
        public bool transport_fee_manual_receipt { get; set; }
        public string transport_fee_frequency { get; set; }
        public string transport_fee_frequency_name { get; set; }
        public bool transport_fee_mandatory { get; set; }
        public string transport_fee_amount { get; set; }
        public bool transport_fee_installment_mode { get; set; }
        public string transport_fee_installment_min_amount { get; set; }
        public string transport_fee_period1 { get; set; }
        public string transport_fee_period2 { get; set; }
        public string transport_fee_period3 { get; set; }
        public string transport_fee_period4 { get; set; }
        public string transport_fee_period5 { get; set; }
        public string transport_fee_period6 { get; set; }
        public string transport_fee_period7 { get; set; }
        public string transport_fee_period8 { get; set; }
        public string transport_fee_period9 { get; set; }
        public string transport_fee_period10 { get; set; }
        public string transport_fee_period11 { get; set; }
        public string transport_fee_period12 { get; set; }
        public bool transport_fee_status { get; set; }
        public string transport_fee_number { get; set; }
    }

    #endregion



    public class pormote_class
    {
        public string promote_code { get; set; }
        public string promote_name { get; set; }
    }
    public class Sims101
    {
        public string parent_name{ get; set; }
         //public string parent_name{ get; set; }
        public string promote_code { get; set; }
       public string user_name { get; set; }
        public string pramoted_grade { get; set; }
        public string promote_name { get; set; }
        public string sims_sims_grades { get; set; }
        public List<pormote_class> P_class { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_academic_years { get; set; }
        public string sims_section_promote_code { get; set; }
        public string grade_names { get; set; }
        public string pramoted_code { get; set; }
        public string opr { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_school_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string grade_code { get; set; }
        public string section_name { get; set; }


        public string sims_medical_visit_number { get; set; }
        public string medical_visit_date { get; set; }
        public string medical_attended_by { get; set; }
        public string medical_complaint_desc { get; set; }
        public string medical_attendant_observation { get; set; }
        public string medical_health_education { get; set; }
        public string medical_followup_comment { get; set; }
        public string student_enroll_no { get; set; }
        public string sims_employee_number { get; set; }
        public string sims_user_code_created_by { get; set; }
        public bool sims_enrollno { get; set; }
        public bool sims_empno { get; set; }
        public string sims_student_name { get; set; }
        public string sims_Emp_name { get; set; }
        public string sims_CreatedBy_name { get; set; }
        public string sims_AttendedBy_name { get; set; }

        public string sims_medicine_code { get; set; }
        public string sims_medicine_name { get; set; }
        public string sims_medicine_dosage { get; set; }
        public string glpdc_type { get; set; }
        public string sims_medicine_duration { get; set; }
        public bool sims_status { get; set; }

        public string sims_student_gender { get; set; }
        public string sims_student_dob { get; set; }
        public string em_sex { get; set; }
        public string em_img { get; set; }
        public string em_date_of_birth { get; set; }
        public string name_in_english { get; set; }
        public string sub_class { get; set; }
        public string sims_student_img { get; set; }
        public string emp_name { get; set; }

        public string sims_blood_pressure { get; set; }
        public string sims_temperature { get; set; }
        public string sims_pulse_rate { get; set; }
        public string sims_respiratory_rate { get; set; }
        public string sims_spo2 { get; set; }

        public string sims_medical_visit_date { get; set; }
        public string sims_complaint_desc { get; set; }
        public string sims_attendant_observation { get; set; }
        public string sims_followup_comment { get; set; }

        public string sims_term_code { get; set; }
        public string sims_term_desc_en { get; set; }
        //Medical Term
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }


        public string sims_enrollment_number { get; set; }
        public string sims_academic_year_old { get; set; }
        public string sims_health_card_number { get; set; }
        public bool sims_health_restriction_status { get; set; }
        public string sims_health_restriction_desc { get; set; }
        public bool sims_medication_status { get; set; }
        public string sims_medication_desc { get; set; }
        public string sims_height { get; set; }
        public string sims_wieght { get; set; }
        public string sims_teeth { get; set; }
        public string sims_health_bmi { get; set; }
        public string sims_health_other_remark { get; set; }
        public string std_name { get; set; }
        public string datastatus { get; set; }
        public string sims_drug_allergy { get; set; }


        public string sims_gb_number { get; set; }
        public string sims_subject_name_en { get; set; }

        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_behaviour_date { get; set; }
        public string sims_behaviour_code { get; set; }
        public string sims_behaviour_note { get; set; }
        public string sims_medical_examination_name { get;  set; }

        public List<medicin_Details> med_det { get; set; }

        public string sims_medical_visit_nursing_treatment { get; set; }




        public string sims_medical_visit_InTime { get; set; }

        public string sims_medical_visit_OutTime { get; set; }

        public string sims_medical_stud_status { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string adv_flag { get; set; }

        public string primary_value { get; set; }

        public string secondary_value { get; set; }

        public string sims_medical_Action_Time { get; set; }

        public string sims_action_health_center { get; set; }

        public string sims_medical_primary_action { get; set; }

        public string sims_medical_action_date { get; set; }

        public string sims_medical_action_time { get; set; }

        public string sims_medical_secondary_action { get; set; }

        public string sims_medical_health_center { get; set; }

        public string sims_medical_action_home { get; set; }

        public string sims_medical_action_comment { get; set; }

        public string sims_home { get; set; }

       // public string parent_name { get; set; }
       // public string user_name { get; set; }

        //public string parent_name { get; set; }
    }
    public class medicin_Details
    {
        public string sims_medicine_dosage { get; set; }
        public string sims_medical_visit_number { get; set; }
        public string sims_medicine_code { get; set; }
        public string sims_medicine_duration { get; set; }

    }

    #region Sims103(Medical Visit Medicine)
    public class Sims103
    {
        public string sims_medical_visit_number { get; set; }
        public string sims_medical_visit_numberName { get; set; }
        public int sims_serial_number { get; set; }
        //public string sims_medicine_code { get; set; }
        public string sims_medicine_name { get; set; }
        public string sims_medicine_dosage { get; set; }
        public string sims_medicine_duration { get; set; }
        public bool sims_status { get; set; }
    }

    #endregion

    #region Sims102(Medical Visit Examination)
    public class Sims102
    {
        public int sims_examination_code { get; set; }
        public string sims_examination_name { get; set; }
        public string sims_examination_desc { get; set; }
    }
    #endregion

    #region Sims100(Medical Susceptibility)
    public class Sims100
    {
        public int sims_susceptibility_code { get; set; }
        public string sims_susceptibility_desc { get; set; }
        public bool sims_susceptibility_status { get; set; }

    }
    #endregion

    #region Sims095(Medical Immunization Group)
    public class Sims095
    {

        public int sims_immunization_age_group_code { get; set; }
        public string sims_immunization_age_group_desc { get; set; }
        public bool sims_immunization_status { get; set; }
        public bool sims_immunization_age_group_status { get; set; }
    }
    #endregion

    public class SimsClass
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_academic_year_start_date { get; set; }
        public string sims_academic_year_end_date { get; set; }
    }

    #region Sims115(Library Attribute)
    public class Sims115
    {
        public string sims_library_attribute_code { get; set; }
        public string sims_library_attribute_name { get; set; }
        public bool sims_library_attribute_status { get; set; }
    }
    #endregion

    #region Sims117(Library Catalogue Attribute)
    public class Sims117
    {
        public string sims_library_catalogue_code { get; set; }
        public string sims_library_catalogue_code_name { get; set; }
        public string sims_library_attribute_code_name { get; set; }
        public string sims_library_attribute_code { get; set; }
    }
    #endregion

    #region Sims118(Library Category)
    public class Sims118
    {
        public string sims_library_category_code { get; set; }
        public string sims_library_category_name { get; set; }
    }
    #endregion


    #region Sims119(Library Catalogue)
    public class Sims119
    {
        public string sims_library_catalogue_code { get; set; }
        public string sims_library_catalogue_name { get; set; }
        public bool sims_library_catalogue_status { get; set; }
    }
    #endregion

    #region Sims122(Library Item Style)
    public class Sims122
    {
        public string sims_library_item_style_code { get; set; }
        public string sims_library_item_style_name { get; set; }
    }
    #endregion

    #region Sims123(Library Location)
    public class Sims123
    {
        public string sims_library_location_code { get; set; }
        public string sims_library_location_name { get; set; }
        public string sims_library_location_address1 { get; set; }
        public string sims_library_location_address2 { get; set; }
        public string sims_library_location_contact_person { get; set; }
        public string sims_library_location_phone { get; set; }
    }
    #endregion

    #region Sims127(Library Privilege)
    public class Sims127
    {
        public string sims_library_privilege_number { get; set; }
        public string sims_library_privilege_name { get; set; }
        public string sims_library_user_group_code { get; set; }
        public string sims_library_user_group_name { get; set; }
        public string sims_library_membership_typename { get; set; }
        public string sims_library_borrow_limit { get; set; }
        public string sims_library_allowed_limit { get; set; }
        public string sims_library_reservation_limit { get; set; }
        public string sims_library_duration { get; set; }
        public bool sims_library_promote_flag { get; set; }
        public string sims_library_quota { get; set; }
        public string sims_library_no_issue_after { get; set; }
        public bool sims_library_auto_renew_flag { get; set; }
        public string sims_library_grace_days { get; set; }
        public bool sims_library_privilege_status { get; set; }
        public bool sims_library_reservation_flag { get; set; }
        public string sims_library_reservation_release_limit { get; set; }
        public string sims_library_membership_type_code { get; set; }
    }
    #endregion

    #region LibraryTranaction
    public class Sims137
    {
        public string sims_library_transaction_type_code { get; set; }
        public string sims_library_transaction_type_name { get; set; }


    }
    #endregion

    #region LibrarySubCatagory
    public class Sims131
    {
        public string sims_library_subcategory_code { get; set; }
        public string sims_library_subcategory_name { get; set; }

    }
    #endregion



    #region Sims134(Library Subscription Type)
    public class Sims134
    {
        public string sims_library_subscription_code { get; set; }
        public string sims_library_subscription_name { get; set; }
        public bool sims_library_subscription_status { get; set; }
    }
    #endregion

    public class Finn_ast_depr
    {
        public string depr_item_no { get; set; }
        public string depr_qty { get; set; }
        public string depr_invamt { get; set; }
        public string depr_valaddcum { get; set; }
        public string depr_valaddmtd { get; set; }
        public string depr_valaddyr { get; set; }
        public string depr_bookval { get; set; }
        public string depr_cumdepr { get; set; }
        public string depr_ytddepr { get; set; }
        public string depr_mthepr { get; set; }
        public string depr_date { get; set; }
        public string depr_jvno { get; set; }
        public string depr_percentage { get; set; }
    }

    #region Finn021
    public class Finn021
    {
        public string gam_comp_code { get; set; }
        public string gam_comp_code_name { get; set; }
        public string gam_dept_code { get; set; }
        public string gam_dept_code_name { get; set; }
        public string gam_asst_type { get; set; }
        public string gam_asst_type_name { get; set; }
        public string gam_item_no { get; set; }
        public string gam_item_no1 { get; set; }
        public string gam_location { get; set; }
        public string gam_location_name { get; set; }
        public string gam_desc_1 { get; set; }
        public string gam_desc_2 { get; set; }
        public string gam_supl_name { get; set; }
        public string gam_acct_code { get; set; }
        public string gam_acct_code_name { get; set; }
        public string gam_order_no { get; set; }
        public string gam_invoice_no { get; set; }
        public string gam_new_sec { get; set; }
        public decimal gam_page_no { get; set; }
        public decimal gam_line_no { get; set; }
        public decimal gam_quantity { get; set; }
        public decimal gam_invoice_amount { get; set; }
        public decimal gam_val_add_cum { get; set; }
        public decimal gam_val_add_mtd { get; set; }
        public decimal gam_val_add_ytd { get; set; }
        public decimal gam_book_value { get; set; }
        public decimal gam_cum_deprn { get; set; }
        public decimal gam_ytd_deprn { get; set; }
        public decimal gam_mth_deprn { get; set; }
        public decimal gam_sale_value { get; set; }
        public decimal gam_repl_cost { get; set; }
        public string gam_receipt_date { get; set; }
        public string gam_sale_date { get; set; }
        public string gam_entry_date { get; set; }
        public string gam_amend_date { get; set; }
        public decimal gam_deprn_percent { get; set; }
        public decimal gam_used_on_item { get; set; }
        public string gam_status_name { get; set; }

        //Summary Variables
        public int gam_items { get; set; }
        public decimal gam_total_quantity { get; set; }
        public decimal gam_amount { get; set; }
        public string gam_sold { get; set; }
        public string gam_delete { get; set; }
        public string gam_dep { get; set; }
        public string gam_status { get; set; }

        public string opr { get; set; }

        public string gam_supl_code { get; set; }

        public string gam_status_code { get; set; }

        public string gam_comp_name { get; set; }

        public string empname { get; set; }
        public string empcode { get; set; }

        public string em_number { get; set; }
        public string employee_name { get; set; }
    }
    #endregion





    public class Fin149
    {
        public string fins_application_name { get; set; }

        public string fins_comp_code { get; set; }

        public string fins_appl_code { get; set; }

        public string fins_appl_form_field { get; set; }

        public string fins_appl_parameter { get; set; }

        public string fins_appl_form_field_value1 { get; set; }

        public string fins_appl_form_field_value2 { get; set; }

        public string fins_appl_form_field_value3 { get; set; }

        public string fins_appl_form_field_value4 { get; set; }

        public string fins_application_code { get; set; }

        public string fins_comp_name { get; set; }

        public string comn_appl_name_en { get; set; }

        public string opr { get; set; }

        public string desg_company_code { get; set; }

        public string desg_company_code_name { get; set; }

        public string fins_appl_form_field_old { get; set; }

        public string fins_appl_parameter_old { get; set; }

        public string fins_appl_form_field_value1_old { get; set; }

        public string fins_appl_form_field_value2_old { get; set; }

        public string fins_appl_form_field_value3_old { get; set; }

        public string fins_appl_form_field_value4_old { get; set; }

        public string fins_comp_short_name { get; set; }
    }


    #region Sims099(Medical Student Susceptibility)
    public class Sims099
    {
        //public string sims_enrollment_number { get; set; }
        //public string sims_susceptibility_code { get; set; }
        //public string sims_susceptibility_desc { get; set; }
        //public string susceptibility { get; set; }
        //public string sims_enrollment_no { get; set; }
        //public string scode_old { get; set; }
        //public string opr { get; set; }

        public string sims_isexpanded { get; set; }
        public string sims_icon { get; set; }

        public string sims_enrollment_no { get; set; }
        public string sims_susceptibility_code { get; set; }
        public string sims_susceptibility_desc { get; set; }
        public string susceptibility { get; set; }
        public string susceptibility1 { get; set; }
        public string scode_old { get; set; }
        public string sims_student_name { get; set; }
        public string GradeSection_Name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }

        public List<Sims099> sims_susceptibility_desc_group { get; set; }

        public string opr { get; set; }


        //public string susceptibility1 { get; set; }
    }
    #endregion

    public class Sim019
    {

        public string sims_doc_mod_code { get; set; }

        public string sims_doc_mod_name { get; set; }

        public string sims_doc_code { get; set; }

        public string sims_doc_name { get; set; }

        public string opr { get; set; }

        public string sims_doc_cur_code { get; set; }

        public string sims_doc_srno { get; set; }

        public string sims_doc_desc { get; set; }

        public bool sims_doc_status { get; set; }

        public string sims_doc_cur_name { get; set; }

        //public bool desg_communication_status { get; set; }

        public string sims_cur_code { get; set; }
    }

    public class Sim020
    {

        public string sims_doc_mod_code { get; set; }

        public string sims_doc_mod_name { get; set; }

        public string sims_doc_code { get; set; }

        public string sims_doc_name { get; set; }

        public string opr { get; set; }

        public string sims_doc_cur_code { get; set; }

        public string sims_doc_srno { get; set; }

        public string sims_doc_desc { get; set; }

        public bool sims_doc_status { get; set; }

        public string sims_doc_cur_name { get; set; }

        //public bool desg_communication_status { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_doc_created_by { get; set; }

        public string sims_doc_create_date { get; set; }
    }
    //public class Sim539
    //{

    //    public string sims_subject_code { get; set; }

    //    public string sims_grade_code { get; set; }

    //    public string sims_section_code { get; set; }

    //    public string sims_subject_name { get; set; }

    //    public string sims_agenda_date { get; set; }

    //    public string sims_agenda_name { get; set; }

    //    public string sims_agenda_desc { get; set; }

    //    public string sims_agenda_sdate { get; set; }

    //    public string sims_agenda_edate { get; set; }

    //    public string sims_agenda_doc { get; set; }

    //    public string sims_agenda_day { get; set; }
    //}
    public class Sims018
    {
        public string opr { get; set; }
        public string sims_transaction_number { get; set; }
        public string sims_concession_from_date { get; set; }
        public string sims_concession_to_date { get; set; }
        public string sims_created_by { get; set; }
        public string sims_created_date { get; set; }
        public bool sims_status { get; set; }
        public string sims_cur_short_name { get; set; }
        public string sims_fee_academic_year { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_fee_academic_year_desc { get; set; }
        public string sims_cur_short { get; set; }
        public string student_full_name_en { get; set; }
        public string sims_concession_number { get; set; }
        public string sims_concession_description { get; set; }
        public string sims_concession_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_status1 { get; set; }
        public string sims_parent_number { get; set; }
        public string parent_name { get; set; }
        public string sims_student_employee_comp_code { get; set; }
        public bool sims_student_employee_comp_code_flag { get; set; }

    }

    //#region Sims036(Section_Fee)
    // public class Sims036
    // {
    //     public string transport_fee_category_name { get; set; }
    //     public string transport_fee_code_desctiption { get; set; }
    //     public string transport_fee_frequency_name { get; set; }
    //     public string sims_library_fee_frequency { get; set; }

    //     public string section_fee_number { get; set; }
    //     public string section_fee_cur_code { get; set; }
    //     public string section_fee_cur_code_name { get; set; }
    //     //public string section_fee_academic_year { get; set; }
    //     public string section_fee_grade_code { get; set; }
    //     public string section_fee_grade_name { get; set; }
    //     public string section_fee_section_code { get; set; }
    //     public string section_fee_section_name { get; set; }
    //     public string section_fee_code { get; set; }
    //     public string section_fee_code_desctiption { get; set; }
    //     public string section_fee_code_type { get; set; }
    //     public string section_fee_code_type_name { get; set; }
    //     public bool section_fee_refundable { get; set; }
    //     public bool section_fee_manual_receipt { get; set; }
    //     public string section_fee_frequency { get; set; }
    //     public string section_fee_frequency_name { get; set; }
    //     public bool section_fee_mandatory { get; set; }
    //     public string section_fee_amount { get; set; }
    //     public bool section_fee_installment_mode { get; set; }
    //     public string section_fee_installment_min_amount { get; set; }
    //     public string section_fee_period1 { get; set; }
    //     public string section_fee_period2 { get; set; }
    //     public string section_fee_period3 { get; set; }
    //     public string section_fee_period4 { get; set; }
    //     public string section_fee_period5 { get; set; }
    //     public string section_fee_period6 { get; set; }
    //     public string section_fee_period7 { get; set; }
    //     public string section_fee_period8 { get; set; }
    //     public string section_fee_period9 { get; set; }
    //     public string section_fee_period10 { get; set; }
    //     public string section_fee_period11 { get; set; }
    //     public string section_fee_period12 { get; set; }
    //     public bool section_fee_status { get; set; }
    //     public string section_fee_category { get; set; }
    //     public string section_fee_category_name { get; set; }
    //     //End of Sims036

    //     public string section_fee_cur_code_name_to { get; set; }
    //     public string section_fee_level_code_name { get; set; }
    //     public string section_fee_academic_year_to { get; set; }
    //     public bool copyto { get; set; }
    //     public string section_fee_grade_name_to { get; set; }
    //     public string section_fee_grade_code_to { get; set; }
    //     public string section_fee_section_name_to { get; set; }
    //     public string section_fee_section_code_to { get; set; }
    //     public string section_fee_category_name_to { get; set; }
    //     public bool overwrite { get; set; }

    //     public bool section_fee_split_flag { get; set; }

    //     public string section_fee_academic_year { get; set; }
    // }

    // #endregion

    #region TreeViewClasses

    public class comn_modules
    {
        public string module_code { get; set; }
        public string module_name_en { get; set; }
        public string module_name_ar { get; set; }
        public string module_name_fr { get; set; }
        public string module_name_ot { get; set; }
        public string module_create_date { get; set; }
        public string module_short_desc { get; set; }
        public string module_desc { get; set; }
        public string module_keywords { get; set; }
        public bool module_status { get; set; }
        public string module_version { get; set; }
        public string module_color { get; set; }
        public string module_location { get; set; }
        public int module_dislpay_order { get; set; }
        public string module_Image { get; set; }
        public List<Comn_parameterstypes> comn_type { get; set; }

        public List<Comn_applications> comn_appl { get; set; }

        public string label { get; set; }

        public string sims_appl_code { get; set; }

        public string sims_appl_name { get; set; }
    }

    public class Comn_parameterstypes
    {
        public string comn_type { get; set; }
        public string comn_type_code { get; set; }
        public string comn_type_name { get; set; }
        public List<Comn_applications> comn_appl { get; set; }

        public string comn_type_name_ar { get; set; }
    }

    public class Comn_applications
    {
        public string comn_appl_code { get; set; }
        public string comn_appl_name_en { get; set; }
        public string comn_appl_tag { get; set; }
        public string comn_appl_location { get; set; }
        public string comn_url { get; set; }
        public string comn_controller { get; set; }

        public string comn_mod_code { get; set; }

        public string comn_appl_type { get; set; }

        public bool comn_appl_status { get; set; }

        public bool comn_user_insert { get; set; }

        public bool comn_user_update { get; set; }

        public bool comn_user_delete { get; set; }

        public string comn_appl_name_ar { get; set; }
    }

    #endregion

    #region Appl Collaboration Details

    public class acoll_det
    {
        public string appl_srno { get; set; }
        public string modcode { get; set; }
        public string module_name { get; set; }
        public string applcode { get; set; }
        public string application_name { get; set; }
        public string form_control { get; set; }
        public string SmtpID { get; set; }
        public bool ifemail { get; set; }
        public bool ifsms { get; set; }
        public bool ifalert { get; set; }
        public bool ifmalert { get; set; }
        public bool ifstatus { get; set; }
        public string email_template { get; set; }
        public string sms_template { get; set; }
        public string alert_template { get; set; }
        public string email_recipients { get; set; }
        public string sms_recipients { get; set; }
        public bool ifemailfather { get; set; }
        public bool ifemailmother { get; set; }
        public bool ifemailguardian { get; set; }
        public bool ifemailactive { get; set; }
        public bool ifsmsfather { get; set; }
        public bool ifsmsmother { get; set; }
        public bool ifsmsguardian { get; set; }
        public bool ifsmsactive { get; set; }
        public string email_temp_srno { get; set; }
        public string sms_temp_srno { get; set; }
        public string alert_temp_srno { get; set; }
        public string malert_temp_srno { get; set; }
        public string opr { get; set; }

        public string smtpuser { get; set; }
        public string smtpids { get; set; }

        public string msg_srno { get; set; }
        public string msg_email_subject { get; set; }
        public string msg_email_body { get; set; }
        public string msg_email_signature { get; set; }
        public string msg_sms_subject { get; set; }
        public string msg_sms_body { get; set; }
        public string msg_sms_signature { get; set; }
        public string msg_alert_subject { get; set; }
        public string msg_alert_body { get; set; }
        public string msg_alert_signature { get; set; }

        public string strMessage { get; set; }
        public bool msgstatus { get; set; }
    }

    #endregion

    public class emaildetails
    {
        public string e_date { get; set; }
        public List<lst_emails> e_details { get; set; }

        public string email_body { get; set; }
        public string email_id { get; set; }
        public string status { get; set; }
    }

    public class emailApproval
    {
        public string receiveMail{ get; set; }

        public string subject { get; set; }
        public string message { get; set; }
        public string mailDate{ get; set; }

        public string sims_email_number { get; set; }
    }

    public class smsApproval
    {
        public string receivesms { get; set; }

        public string subject { get; set; }
        public string message { get; set; }
        public string smsDate { get; set; }

        public string sims_sms_number { get; set; }
    }

    public class lst_emails
    {
        public string email_subject { get; set; }
        public string email_count { get; set; }
    }

    #region Circular
    public class Comn015
    {
        public string sims_circular_remark { get; set; }
        public string circular_code { get; set; }
        public string circular_desc { get; set; }
        public bool enablecircular { get; set; }
        public string sims_news_number { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_news_date { get; set; }
        public string sims_news_publish_date { get; set; }
        public string sims_news_expiry_date { get; set; }
        public string sims_news_title { get; set; }
        public string sims_news_short_desc { get; set; }
        public string sims_news_desc { get; set; }
        public string sims_news_file_path1 { get; set; }
        public string sims_news_file_path2 { get; set; }
        public string sims_news_file_path3 { get; set; }
        public string sims_news_file_path1_en { get; set; }
        public string sims_news_file_path2_en { get; set; }
        public string sims_news_file_path3_en { get; set; }
        public string sims_news_type { get; set; }
        public string sims_news_type_code { get; set; }
        public string sims_news_category { get; set; }
        public string sims_news_cat_code { get; set; }
        public string sims_news_created_user { get; set; }
        public string sims_display_order { get; set; }
        public string sims_news_user_group { get; set; }
        public bool sims_news_status { get; set; }
        public bool publish_status { get; set; }
        public string sims_news_ifalert { get; set; }
        public string sims_news_pstatus { get; set; }
        public string sims_news_user_status { get; set; }
        public bool sims_checkstatus { get; set; }

        public string sims_news_user_group_code { get; set; }
        public List<string> lst_news_groups { get; set; }

        //for circular
        public string sims_circular_number { get; set; }
        public string sims_circular_date { get; set; }
        public string sims_circular_publish_date { get; set; }
        public string sims_circular_expiry_date { get; set; }
        public string sims_circular_title { get; set; }
        public string sims_circular_short_desc { get; set; }
        public string sims_circular_desc { get; set; }
        public string sims_circular_file_path1 { get; set; }
        public string sims_circular_file_path2 { get; set; }
        public string sims_circular_file_path3 { get; set; }
        public string sims_circular_file_path1_en { get; set; }
        public string sims_circular_file_path2_en { get; set; }
        public string sims_circular_file_path3_en { get; set; }
        public string sims_circular_type { get; set; }
        public string sims_circular_type_code { get; set; }
        public string sims_circular_category { get; set; }
        public string sims_circular_cat_code { get; set; }
        public string sims_circular_created_user { get; set; }
        public string sims_circular_display_order { get; set; }
        public string sims_circular_user_group { get; set; }
        public bool sims_circular_status { get; set; }

        public string sims_circular_ifalert { get; set; }
        public string sims_circular_pstatus { get; set; }
        public string sims_circular_user_status { get; set; }

        public string sims_circular_user_group_code { get; set; }
        public List<string> lst_groups { get; set; }

        //For GradeSections/Dept/Desg
        public string sims_circular_gradesections { get; set; }
        public string sims_circular_grade { get; set; }
        public string sims_circular_desg { get; set; }
        public string sims_circular_dept { get; set; }
        public bool sims_circular_ifGrSec { get; set; }
        public bool sims_circular_ifDesg { get; set; }
        public bool sims_circular_ifDept { get; set; }
        public string sims_circular_file { get; set; }

        public List<string> lst_grade { get; set; }
        public List<string> lst_section { get; set; }
        public List<string> lst_desg { get; set; }
        public List<string> lst_dept { get; set; }
        public bool enablenews { get; set; }

        //--------------------------------------------------------------------------------------
        //For GradeSections/Dept/Desg(for News)
        public string sims_news_gradesections { get; set; }
        public string sims_news_grade { get; set; }
        public string sims_news_desg { get; set; }
        public string sims_news_dept { get; set; }
        public bool sims_news_ifGrSec { get; set; }
        public bool sims_news_ifDesg { get; set; }
        public bool sims_news_ifDept { get; set; }
        public string sims_news_file { get; set; }

        public List<string> lst_grade_news { get; set; }
        public List<string> lst_section_news { get; set; }
        public List<string> lst_desg_news { get; set; }
        public List<string> lst_dept_news { get; set; }

        public List<news_docs> news_doc_lst { get; set; }

        public string sims_circular_alert_status { get; set; }

        public string sims_circular_email_status { get; set; }
    }
   
    public class ComnCircularDetails
    {
        public string sims_circular_number { get; set; }
        public string sims_circular_date { get; set; }
        public string sims_circular_publish_date { get; set; }
        public string comn_user_name { get; set; }
        public string sims_comn_user_group_name { get; set; }
        public string sims_acknowledgment_count { get; set; }
        public string sims_sent_count { get; set; }
        public string sims_news_title { get; set; }
        public string sims_circular_user_name { get; set; }
        public string sims_UserName { get; set; }
        public string sims_circular_title { get; set; }

        public string comn_user_code { get; set; }
        public string comn_user_group_name { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string UserName { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }

        public string sims_sibling_parent_number { get; set; }
        public string Parent_UserName { get; set; }
        public string sims_sibling_student_enroll_number { get; set; }
        public string Student_UserName { get; set; }
        
        public string employee_UserName { get; set; }
        public string em_email { get; set; }
        public string comn_user_email { get; set; }

       public string username { get; set; }

       public string parent_username { get; set; }

       public string student_username { get; set; }

       public string comn_user_alias { get; set; }
    }
    public class news_docs
    {
        public string sims_news_originalfile { get; set; }
        public string sims_news_newfile { get; set; }
    }
    #endregion

    public class Fee_code_list
    {
        public string sims_fee_code { get; set; }

        public string sims_fee_category { get; set; }

        public string sims_fee_code_description { get; set; }

        public bool isassigned { get; set; }

        public bool sims_fee_status { get; set; }
    }

    public class concessiondetails
    {

        public string sims_concession_no { get; set; }

        public string sims_concession_desc { get; set; }

        public string sims_concession_type { get; set; }

        public string sims_conession_fee_type { get; set; }

        public string sims_conession_to_date { get; set; }

        public string sims_conession_status { get; set; }

        public string sims_fee_period1_concession { get; set; }

        public string sims_fee_period1_concession_paid { get; set; }

        public string sims_fee_period2_concession { get; set; }

        public string sims_fee_period2_concession_paid { get; set; }

        public string sims_fee_period3_concession { get; set; }

        public string sims_fee_period3_concession_paid { get; set; }

        public string sims_fee_period4_concession { get; set; }

        public string sims_fee_period4_concession_paid { get; set; }

        public string sims_fee_period5_concession { get; set; }

        public string sims_fee_period5_concession_paid { get; set; }

        public string sims_fee_period6_concession { get; set; }

        public string sims_fee_period6_concession_paid { get; set; }

        public string sims_fee_period7_concession { get; set; }

        public string sims_fee_period7_concession_paid { get; set; }

        public string sims_fee_period8_concession_paid { get; set; }

        public string sims_fee_period8_concession { get; set; }

        public string sims_fee_period9_concession { get; set; }

        public string sims_fee_period9_concession_paid { get; set; }

        public string sims_fee_period10_concession { get; set; }

        public string sims_fee_period10_concession_paid { get; set; }

        public string sims_fee_period11_concession { get; set; }

        public string sims_fee_period11_concession_paid { get; set; }

        public string sims_fee_period12_concession { get; set; }

        public string sims_fee_period12_concession_paid { get; set; }

        public bool sims_fee_status { get; set; }
    }

    public class Sims052
    {

        public string sims_fee_period_code { get; set; }
            public string sims_fee_period_amt { get; set; }
         
        public string user_name { get; set; }
         public List<Fee_code_list> sublist = new List<Fee_code_list>();
        public List<concessiondetails> concessiondetails = new List<concessiondetails>();
        public string sims_enroll_number { get; set; }

        public string sims_fee_cur_code { get; set; }

        public string sims_fee_academic_year { get; set; }

        public string sims_fee_grade_code { get; set; }

        public string sims_fee_section_code { get; set; }

        public string sims_fee_category { get; set; }

        public string sims_fee_category1 { get; set; }

        public string Concession { get; set; }

        public string studentName { get; set; }

        public string sims_fee_code { get; set; }

        public string sims_fee_category_description { get; set; }

        public string sims_fee_code_description { get; set; }

        public bool isassigned { get; set; }

        public string sims_fee_number { get; set; }

        public string sims_fee_frequency { get; set; }

        public string sims_fee_amount { get; set; }

        public bool sims_fee_installment_mode { get; set; }

        public string sims_fee_installment_min_amount { get; set; }

        public string sims_fee_period1 { get; set; }

        public string sims_fee_period2 { get; set; }

        public string sims_fee_period3 { get; set; }

        public string sims_fee_period4 { get; set; }

        public string sims_fee_period5 { get; set; }

        public string sims_fee_period6 { get; set; }

        public string sims_fee_period7 { get; set; }

        public string sims_fee_period8 { get; set; }

        public string sims_fee_period9 { get; set; }

        public string sims_fee_period10 { get; set; }

        public string sims_fee_period11 { get; set; }

        public string sims_fee_period12 { get; set; }

        public bool sims_fee_status { get; set; }

        public string sims_term_start_date { get; set; }

        public string sims_month_code { get; set; }
    }

    #region Student(Sims042)
    public class Sims042
    {
        public string birth_country_code { get; set; }
        public string birth_country { get; set; }
        public string sims_admission_family_monthly_income_desc { get; set; }
       // public string std_communication_date1 { get; set; }
       // public string std_follow_date1 { get; set; }

       // public string sims_parent_father_dob { get; set; }
       // public string sims_parent_mother_dob { get; set; }
       // public string std_follow_date1 { get; set; }
       // public string std_follow_date1 { get; set; }


        public string sibling_count { get; set; }

        //public string sims_student_enroll_number { get; set; }
        public string student_enroll_no { get; set; }
        public string sims_parent_number { get; set; }
        public string opr { get; set; }
        public string enrollno { get; set; }
        public string search_std_enroll_no { get; set; }
        public string sims_nationality_code { get; set; }
        public string sims_country_code { get; set; }
        public string sims_parent_father_first_name { get; set; }
        public string sims_parent_father_last_name { get; set; }
        public string sims_nationality_name_en { get; set; }

        public string sims_student_cur_code { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_middle_name_en { get; set; }
        public string sims_student_passport_last_name_en { get; set; }
        public string sims_student_family_name_en { get; set; }
        public string sims_student_nickname { get; set; }
        public string sims_student_passport_first_name_ot { get; set; }
        public string sims_student_passport_middle_name_ot { get; set; }
        public string sims_student_passport_last_name_ot { get; set; }
        public string sims_student_family_name_ot { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_student_gender_code { get; set; }
        public string sims_student_religion_code { get; set; }
        public string sims_student_passport_full_name_en { get; set; }
        public string sims_student_passport_full_name_ar { get; set; }
        public string sims_student_religion { get; set; }
        public string sims_student_dob { get; set; }
       // public string sims_country_code { get; set; }
        public string sims_student_birth_country_code { get; set; }
        public string sims_student_birth_country { get; set; }
        public string sims_student_nationality_code { get; set; }
        public string sims_student_ethnicity_code { get; set; }
        public string sims_student_ethnicity { get; set; }
        public string sims_student_visa_number { get; set; }
        public string sims_student_visa_issue_date { get; set; }
        public string sims_student_visa_expiry_date { get; set; }
        public string sims_student_visa_issuing_place { get; set; }
        public string sims_student_visa_issuing_authority { get; set; }
        public string sims_student_visa_type { get; set; }
        public string sims_student_visa_type_code { get; set; }
        public string sims_student_national_id { get; set; }
        public string sims_student_national_id_issue_date { get; set; }
        public string sims_student_national_id_expiry_date { get; set; }
        public string sims_student_main_language_code { get; set; }
        public string sims_student_main_language { get; set; }
        public string sims_student_main_language_r_code { get; set; }
        public string sims_student_main_language_r { get; set; }
        public string sims_student_main_language_w_code { get; set; }
        public string sims_student_main_language_w { get; set; }
        public string sims_student_main_language_s_code { get; set; }
        public string sims_student_main_language_s { get; set; }
        public string sims_student_main_language_m { get; set; }
        public string sims_student_main_language_m_code { get; set; }
        public string sims_student_other_language { get; set; }
        public string sims_student_primary_contact_code { get; set; }
        public string sims_student_primary_contact_pref { get; set; }
        public string sims_student_fee_payment_contact_pref { get; set; }
        public bool sims_student_transport_status { get; set; }
        public string sims_student_parent_status_code { get; set; }
        public string sims_student_parent_status { get; set; }
        public string sims_student_legal_custody { get; set; }
        public string sims_student_emergency_contact_name1 { get; set; }
        public string sims_student_emergency_contact_name2 { get; set; }
        public string sims_student_emergency_contact_number1 { get; set; }
        public string sims_student_emergency_contact_number2 { get; set; }
        public bool sims_student_language_support_status { get; set; }
        public string sims_student_language_support_desc { get; set; }
        public bool sims_student_behaviour_status { get; set; }
        public string sims_student_behaviour_desc { get; set; }
        public bool sims_student_gifted_status { get; set; }
        public string sims_student_gifted_desc { get; set; }
        public bool sims_student_music_status { get; set; }
        public string sims_student_music_desc { get; set; }
        public bool sims_student_sports_status { get; set; }
        public string sims_student_sports_desc { get; set; }
        public string sims_student_date { get; set; }
        public string sims_student_commence_date { get; set; }
        public string sims_student_remark { get; set; }
        public string sims_student_login_id { get; set; }
        public string sims_student_current_school_code { get; set; }
        public string student_current_school_code { get; set; }
        public string sims_student_employee_comp_code { get; set; }
        public string sims_student_employee_code { get; set; }
        public string sims_student_last_login { get; set; }
        public string sims_student_secret_question_code { get; set; }
        public string sims_student_secret_question { get; set; }
        public string sims_student_secret_answer { get; set; }
        public string sims_student_academic_status { get; set; }
        public string sims_student_academic_status_code { get; set; }
        public string sims_student_financial_status { get; set; }
        public string sims_student_financial_status_code { get; set; }
        public string sims_student_house { get; set; }
        public string student_house { get; set; }
        public string sims_student_class_rank { get; set; }
        public string sims_student_honour_roll { get; set; }
        public string sims_student_ea_number { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_student_ea_registration_date { get; set; }
        public string sims_student_ea_transfer { get; set; }
        public string sims_student_ea_transfer_code { get; set; }
        public string sims_student_ea_status { get; set; }
        public string sims_student_ea_status_code { get; set; }
        public string sims_student_passport_number { get; set; }
        public string sims_student_passport_issue_date { get; set; }
        public string sims_student_passport_expiry_date { get; set; }
        public string sims_student_passport_issuing_authority { get; set; }
        public string sims_student_passport_issue_place { get; set; }
        public string sims_student_section_strength { get; set; }
        public string sims_student_prev_school { get; set; }
        public string sims_student_image { get; set; }
        public string sims_student_attribute5 { get; set; }
        public string sims_student_home_appartment_number { get; set; }
        public string sims_student_home_building_number { get; set; }
        public string sims_student_home_street_number { get; set; }
        public string sims_student_home_area_number { get; set; }
        public string sims_student_home_landmark { get; set; }
        public string sims_student_home_country_code { get; set; }
        public string sims_student_home_state { get; set; }
        public string sims_student_home_city { get; set; }
        public string sims_student_home_phone { get; set; }
        public string sims_student_home_po_box { get; set; }
        public string sims_student_home_summary_address { get; set; }

        //PP001
        public string sims_student_parameter_code { get; set; }
        public string student_full_name { get; set; }
        public string student_full_name_ot { get; set; }
        public string sims_parent_country_code { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_father_city { get; set; }
        public string sims_parent_father_state { get; set; }
        public string sims_parent_father_po_box { get; set; }
        public string sims_parent_father_phone { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_father_fax { get; set; }

        public string sims_sibling_student_img_path { get; set; }
        public string sims_sibling_enroll_number { get; set; }
        public string sims_sibling_name { get; set; }
        public string sims_sibling_student_nickname { get; set; }
        public string sims_sibling_academic_year { get; set; }
        public string sims_sibling_grade_name_en { get; set; }
        public string sims_sibling_section_name_en { get; set; }

        public string sopr { get; set; }
        public string sims_previous_school_cur_name { get; set; }
        public string sims_previous_school_academic_year { get; set; }
        public string sims_previous_school_class { get; set; }
        public string sims_previous_school_country { get; set; }
        public string sims_previous_school_address { get; set; }
        public string parent_flag { get; set; }

        //public string student_other_language { get; set; }

        //public string student_date { get; set; }
        //public string student_commence_date { get; set; }
        //public string student_class_rank { get; set; }
        //public string student_last_login { get; set; }
        //public string student_academic_status { get; set; }
        //public string student_financial_status { get; set; }

        //public string student_honour_roll { get; set; }
        //public string student_transport_status { get; set; }
        //public string student_remark { get; set; }
        //public string pp001_student_dob { get; set; }

        //public string student_dob { get; set; }
        //public string student_visa_issue_date { get; set; }
        //public string student_visa_expiry_date { get; set; }
        //public string student_national_id_issue_date { get; set; }
        //public string student_national_id_expiry_date { get; set; }

        //public string sims_student_ea_number { get; set; }
        public bool language_support_status { get; set; }
        public string language_support_desc { get; set; }
        public bool behaviour_status { get; set; }
        public string behaviour_desc { get; set; }

        public bool sims_student_learning_therapy_status { get; set; }

        public string sims_student_learning_therapy_desc { get; set; }

        public string sims_student_special_education_desc { get; set; }

        public bool sims_student_special_education_status { get; set; }

        public bool sims_student_falled_grade_status { get; set; }

        public string sims_student_falled_grade_desc { get; set; }

        public bool sims_student_communication_status { get; set; }

        public string sims_student_communication_desc { get; set; }

        public string sims_student_specialAct_desc { get; set; }

        public bool sims_student_specialAct_status { get; set; }

        public string sims_student_nationality { get; set; }
        public string sims_admission_mother_nationality1_code { get; set; }
        public string sims_admission_mother_nationality2_code { get; set; }
        public string sims_admission_guardian_nationality1_code { get; set; }
        public string sims_admission_guardian_nationality2_code { get; set; }
        public string sims_admission_father_nationality1_code { get; set; }
        public string sims_admission_father_nationality2_code { get; set; }



        public string sims_grade_name { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_section_name { get; set; }

        public string EditButtonVisibility { get; set; }

        public string sims_student_nationality_name { get; set; }

        public string sims_admission_mother_nationality1_name { get; set; }

        public string sims_admission_guardian_nationality1_name { get; set; }

        public string sims_admission_father_nationality1_name { get; set; }

        public string sims_country_name_en { get; set; }

        public string sims_sibling_grade_code { get; set; }

        public string sims_sibling_section_code { get; set; }


        //Subject details
        public string sims_subject_code { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_bell_teacher_code { get; set; }

        public string teacherName { get; set; }

        //StaffChild

        public string comp_code { get; set; }

        public string comp_name { get; set; }

        public string em_login_code { get; set; }

        public string employeeName { get; set; }

        public string codp_dept_no { get; set; }

        public string codp_dept_name { get; set; }

        public string codp_short_name { get; set; }

        public string dg_code { get; set; }

        public string dg_desc { get; set; }

        public string sims_student_lbl { get; set; }
        public string sims_student_category { get;  set; }
        public string student_category_name { get;  set; }
        public string sims_appl_parameter { get;  set; }
        public string sims_appl_form_field_value1 { get;  set; }

        public string sims_student_pan_no { get; set; }

        public string sims_student_voter_id { get; set; }
        public string sims_student_birth_place { get; set; }
        public string comn_user_email { get; set; }
        public string sims_admission_family_monthly_income { get; set; }
        public string sims_cur_code { get; set; }

        public string sims_student_admission_grade_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string sims_student_attribute3 { get; set; }
        public string sims_student_attribute1 { get; set; }
        public string sims_student_attribute2 { get; set; }
        //public string sims_parent_father_first_name { get; set; }
        //public string sims_parent_father_last_name { get; set; }
    }
    #endregion

    #region Student(part_Section)
    public class part_Section
    {

        public string sims_admission_criteria_part_number { get; set; }

        public string sims_admission_criteria_part_Name { get; set; }

        public string sims_admission_criteria_section_number { get; set; }

        public string sims_admission_criteria_section_Name { get; set; }
    }
    #endregion


    #region StudentProfile
    #region (LeaveApplication)
    public class LeaveApplication
    {
        public string sims_leave_number { get; set; }
        public string sims_parent_id { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_leave_code { get; set; }
        public string cl_desc { get; set; }

        public string sims_leave_start_date { get; set; }
        public string sims_leave_end_date { get; set; }
        public string sims_leave_reason { get; set; }
        public string sims_leave_application_date { get; set; }
        public string sims_leave_status { get; set; }

        public string sims_leave_assigned_to { get; set; }
        public string sims_leave_status_changed_by { get; set; }

        public string sims_academic_year { get; set; }
    }

    #endregion


    public class Uccw257
    {
        public string sims_gb_name { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_gb_cat_assign_max_score { get; set; }
        public string sims_gb_cat_assign_max_score_correct { get; set; }
        public string final_mark { get; set; }
        public string sims_attendance_cd { get; set; }
        public string sims_attendance_desc { get; set; }
        public string sims_month_no { get; set; }
        public string sims_month_name { get; set; }
        public int sims_attendance_cd_p { get; set; }
        public int sims_attendance_cd_a { get; set; }
        public int sims_attendance_cd_t { get; set; }
        public int sims_attendance_cd_te { get; set; }
        public int sims_attendance_cd_um { get; set; }
        public int sims_attendance_cd_w { get; set; }
        public int sims_attendance_cd_h { get; set; }
        public int sims_attendance_cd_na { get; set; }
        public int sims_attendance_cd_ae { get; set; }
        public int sims_attendance_totaldays { get; set; }

        public string sims_month_attendance_cd_p { get; set; }
        public string sims_month_attendance_cd_a { get; set; }
        public string sims_month_attendance_cd_t { get; set; }
        public string sims_month_attendance_cd_te { get; set; }
        public string sims_month_attendance_cd_um { get; set; }
        public string sims_month_attendance_cd_na { get; set; }
        public string sims_month_attendance_cd_ae { get; set; }
        public int attendance_count { get; set; }
        //  public string sims_report_card_name { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_attendance_day_cd { get; set; }
    }




    #endregion

    public class admissionClass
    {
        #region Sims010(Admission)

        public string sims_parent_guardian_relationship_code { get; set; }
        public string sims_parent_number { get; set; }
        public string sims_parent_father_occupation_local_language { get; set; }
        public string sims_parent_father_occupation_location_local_language { get; set; }
        public string sims_parent_mother_occupation_local_language { get; set; }
        public string sims_parent_guardian_occupation_local_language { get; set; }
        public string sims_parent_guardian_occupation_location_local_language { get; set; }
        public bool sims_parent_is_employment_status { get; set; }
        public string sims_parent_is_employement_comp_code { get; set; }
        public string sims_parent_is_employment_number { get; set; }
        public string sims_admission_login_name { get; set; }
        public string sims_admission_guardian_family_name_en { get; set; }
        public string sims_admission_mother_family_name_en { get; set; }
        public string sims_admission_student_enroll_number { get; set; }
        public string sims_admission_blood_group_code { get; set; }
        public string sims_admission_blood_group_name { get; set; }
        public string sims_admission_commencement_date { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_admission_cur_name { get; set; }
        public string sims_admission_current_school_address { get; set; }
        public string sims_admission_current_school_city { get; set; }
        public string sims_admission_current_school_country_code { get; set; }
        public string sims_admission_current_school_cur { get; set; }
        public string sims_admission_current_school_enroll_number { get; set; }
        public string sims_admission_current_school_fax { get; set; }
        public string sims_admission_current_school_from_date { get; set; }
        public string sims_admission_current_school_grade { get; set; }
        public string sims_admission_current_school_head_teacher { get; set; }
        public string sims_admission_current_school_language { get; set; }
        public string sims_admission_current_school_name { get; set; }
        public string sims_admission_current_school_phone { get; set; }
        public string sims_admission_current_school_status { get; set; }
        public string sims_admission_current_school_to_date { get; set; }
        public string sims_admission_date { get; set; }
        public string sims_admission_declaration_status { get; set; }
        public string sims_admission_disability_desc { get; set; }
        public string sims_admission_disability_status { get; set; }
        public string sims_admission_dns { get; set; }
        public string sims_admission_dob { get; set; }
        public string sims_admission_employee_code { get; set; }
        public string sims_admission_employee_school_code { get; set; }
        public string sims_admission_employee_type { get; set; }
        public string sims_admission_ethnicity_code { get; set; }
        public string sims_admission_family_name_en { get; set; }
        public string sims_admission_family_name_ot { get; set; }
        public string sims_admission_father_appartment_number { get; set; }
        public string sims_admission_father_area_number { get; set; }
        public string sims_admission_father_building_number { get; set; }
        public string sims_admission_father_city { get; set; }
        public string sims_admission_father_city_code { get; set; }
        public string sims_admission_father_company { get; set; }
        public string sims_admission_father_country_code { get; set; }
        public string sims_admission_father_email { get; set; }
        public string sims_admission_father_family_name { get; set; }
        public string sims_admission_father_fax { get; set; }
        public string sims_admission_father_first_name { get; set; }
        public string sims_admission_father_last_name { get; set; }
        public string sims_admission_father_middle_name { get; set; }
        public string sims_admission_father_full_name { get; set; }
        public string sims_admission_father_mobile { get; set; }
        public string sims_admission_father_name_ot { get; set; }
        public string sims_admission_father_nationality1_code { get; set; }
        public string sims_admission_father_nationality2_code { get; set; }
        public string sims_admission_father_occupation { get; set; }

        public string sims_admission_father_passport_number { get; set; }
        public string sims_parent_father_passport_issue_date { get; set; }
        public string sims_parent_father_passport_expiry_date { get; set; }
        public string sims_parent_father_national_id { get; set; }
        public string sims_parent_father_national_id_issue_date { get; set; }
        public string sims_parent_father_national_id_expiry_date { get; set; }


        public string sims_admission_father_phone { get; set; }
        public string sims_admission_father_po_box { get; set; }
        public string sims_admission_father_salutation_code { get; set; }
        public string sims_admission_father_state { get; set; }
        public string sims_admission_father_state_code { get; set; }
        public string sims_admission_father_street_number { get; set; }
        public string sims_admission_father_summary_address { get; set; }
        public string sims_admission_fee_payment_contact_pref { get; set; }
        public string sims_admission_fees_paid_status { get; set; }
        public string sims_admission_gender { get; set; }
        public string sims_admission_gender_name { get; set; }
        public string sims_admission_gifted_desc { get; set; }
        public string sims_admission_gifted_status { get; set; }
        public string sims_admission_grade_code { get; set; }
        public string sims_admission_guardian_appartment_number { get; set; }
        public string sims_admission_guardian_area_number { get; set; }
        public string sims_admission_guardian_building_number { get; set; }
        public string sims_admission_guardian_city { get; set; }
        public string sims_admission_guardian_company { get; set; }
        public string sims_admission_guardian_country_code { get; set; }
        public string sims_admission_guardian_email { get; set; }
        public string sims_admission_guardian_family_name { get; set; }
        public string sims_admission_guardian_fax { get; set; }
        public string sims_admission_guardian_full_name { get; set; }
        public string sims_admission_guardian_first_name { get; set; }
        public string sims_admission_guardian_last_name { get; set; }
        public string sims_admission_guardian_middle_name { get; set; }
        public string sims_admission_guardian_mobile { get; set; }
        public string sims_admission_guardian_name_ot { get; set; }
        public string sims_admission_guardian_nationality1_code { get; set; }
        public string sims_admission_guardian_nationality2_code { get; set; }
        public string sims_admission_guardian_occupation { get; set; }

        public string sims_admission_guardian_passport_number { get; set; }
        public string sims_parent_guardian_passport_issue_date { get; set; }
        public string sims_parent_guardian_passport_expiry_date { get; set; }
        public string sims_parent_guardian_national_id { get; set; }
        public string sims_parent_guardian_national_id_issue_date { get; set; }
        public string sims_parent_guardian_national_id_expiry_date { get; set; }

        public string sims_admission_guardian_phone { get; set; }
        public string sims_admission_guardian_po_box { get; set; }
        public string sims_admission_guardian_relationship_code { get; set; }
        public string sims_admission_guardian_salutation_code { get; set; }
        public string sims_admission_guardian_state { get; set; }
        public string sims_admission_guardian_street_number { get; set; }
        public string sims_admission_guardian_summary_address { get; set; }
        public string sims_admission_health_card_expiry_date { get; set; }
        public string sims_admission_health_card_issue_date { get; set; }
        public string sims_admission_health_card_issuing_authority { get; set; }
        public string sims_admission_health_card_number { get; set; }
        public string sims_admission_health_hearing_desc { get; set; }
        public string sims_admission_health_hearing_status { get; set; }
        public string sims_admission_health_other_desc { get; set; }
        public string sims_admission_health_other_status { get; set; }
        public string sims_admission_health_restriction_desc { get; set; }
        public string sims_admission_health_restriction_status { get; set; }
        public string sims_admission_health_vision_desc { get; set; }
        public string sims_admission_health_vision_status { get; set; }
        public string sims_admission_ip { get; set; }
        public string sims_admission_language_support_desc { get; set; }
        public string sims_admission_language_support_status { get; set; }
        public string sims_admission_legal_custody { get; set; }
        public string sims_admission_legal_custody_name { get; set; }
        public string sims_admission_main_language_code { get; set; }
        public string sims_admission_main_language_m { get; set; }
        public string sims_admission_main_language_r { get; set; }
        public string sims_admission_main_language_s { get; set; }
        public string sims_admission_main_language_w { get; set; }
        public string sims_admission_marketing_code { get; set; }
        public string sims_admission_marketing_description { get; set; }
        public string sims_admission_medication_desc { get; set; }
        public string sims_admission_medication_status { get; set; }
        public string sims_admission_mother_appartment_number { get; set; }
        public string sims_admission_mother_area_number { get; set; }
        public string sims_admission_mother_building_number { get; set; }
        public string sims_admission_mother_city { get; set; }
        public string sims_admission_mother_company { get; set; }
        public string sims_admission_mother_country_code { get; set; }
        public string sims_admission_mother_email { get; set; }
        public string sims_admission_mother_family_name { get; set; }
        public string sims_admission_mother_fax { get; set; }
        public string sims_admission_mother_full_name { get; set; }
        public string sims_admission_mother_first_name { get; set; }
        public string sims_admission_mother_last_name { get; set; }
        public string sims_admission_mother_middle_name { get; set; }
        public string sims_admission_mother_mobile { get; set; }
        public string sims_admission_mother_name_ot { get; set; }
        public string sims_admission_mother_nationality1_code { get; set; }
        public string sims_admission_mother_nationality2_code { get; set; }
        public string sims_admission_mother_occupation { get; set; }

        public string sims_admission_mother_passport_number { get; set; }
        public string sims_parent_mother_passport_issue_date { get; set; }
        public string sims_parent_mother_passport_expiry_date { get; set; }
        public string sims_parent_mother_national_id { get; set; }
        public string sims_parent_mother_national_id_issue_date { get; set; }
        public string sims_parent_mother_national_id_expiry_date { get; set; }

        public string sims_admission_mother_phone { get; set; }
        public string sims_admission_mother_po_box { get; set; }
        public string sims_admission_mother_salutation_code { get; set; }
        public string sims_admission_mother_state { get; set; }
        public string sims_admission_mother_street_number { get; set; }
        public string sims_admission_mother_summary_address { get; set; }
        public string sims_admission_music_desc { get; set; }
        public string sims_admission_music_status { get; set; }
        public string sims_admission_national_id { get; set; }
        public string sims_admission_national_id_expiry_date { get; set; }
        public string sims_admission_national_id_issue_date { get; set; }
        //  public string sims_admission_national_id_expiry_date { get; set; }
        // public string sims_admission_national_id_issue_date { get; set; }
        public string sims_admission_nationality_code { get; set; }
        public string sims_admission_nickname { get; set; }
        public string sims_admission_number { get; set; }
        public string sims_admission_other_language { get; set; }
        public string sims_admission_parent_id { get; set; }
        public string sims_admission_parent_status_code { get; set; }
        public string sims_admission_passport_expiry_date { get; set; }
        public string sims_admission_passport_first_name_en { get; set; }
        public string sims_admission_passport_first_name_ot { get; set; }
        public string sims_admission_passport_issue_date { get; set; }
        public string sims_admission_passport_issue_place { get; set; }
        public string sims_admission_passport_issuing_authority { get; set; }
        public string sims_admission_passport_last_name_en { get; set; }
        public string sims_admission_passport_last_name_ot { get; set; }
        public string sims_admission_passport_middle_name_en { get; set; }
        public string sims_admission_passport_middle_name_ot { get; set; }
        public string sims_admission_passport_number { get; set; }
        public string sims_admission_primary_contact_code { get; set; }
        public string sims_admission_primary_contact_pref { get; set; }
        public string sims_admission_pros_application_number { get; set; }
        public string sims_admission_pros_number { get; set; }
        public string sims_admission_religion_code { get; set; }
        public string sims_admission_school_code { get; set; }
        public string sims_admission_sibling_dob { get; set; }
        public string sims_admission_sibling_enroll_number { get; set; }
        public string sims_admission_sibling_name { get; set; }
        public string sims_admission_sibling_school_code { get; set; }
        public string sims_admission_sibling_status { get; set; }
        public string sims_admission_sports_desc { get; set; }
        public string sims_admission_sports_status { get; set; }
        public string sims_admission_status { get; set; }
        public string sims_admission_tentative_joining_date { get; set; }
        public string sims_admission_term_code { get; set; }
        public string sims_admission_transport_desc { get; set; }
        public string sims_admission_transport_status { get; set; }
        public string sims_admission_user_code { get; set; }
        public string sims_admission_visa_expiry_date { get; set; }
        public string sims_admission_visa_issue_date { get; set; }
        //public string sims_admission_visa_expiry_date { get; set; }
        //public string sims_admission_visa_issue_date { get; set; }
        public string sims_admission_visa_issuing_authority { get; set; }
        public string sims_admission_visa_issuing_place { get; set; }
        public string sims_admission_visa_number { get; set; }
        public string sims_admission_visa_type { get; set; }
        public string sims_admission_section_code { get; set; }
        public string sims_admission_fee_category { get; set; }
        public string sims_admission_fee_category_code { get; set; }

        //student login
        public string sims_admisison_student_user_sequnce_code { get; set; }
        public string sims_admisison_student_user_password { get; set; }
        public string sims_admisison_student_user_group_code { get; set; }
        public string sims_admisison_student_user_date_created { get; set; }

        //parent details-login
        public string sims_admisison_parent_sequnce_code { get; set; }
        public string sims_admisison_parent_user_sequnce_code { get; set; }
        public string sims_admisison_parent_user_password { get; set; }
        public string sims_admisison_parent_user_group_code { get; set; }
        public string sims_admisison_parent_user_date_created { get; set; }

        //student image
        public string sims_admisison_student_image { get; set; }
        //Parent image
        public string sims_admisison_father_image { get; set; }
        //mother
        public string sims_admisison_mother_image { get; set; }
        //guardian
        public string sims_admisison_guardian_image { get; set; }

        public string active_member { get; set; }
        #endregion


        public string sims_admission_mother_state_code { get; set; }

        public string sims_admission_guardian_state_code { get; set; }

        public string sims_parent_father_mobile_isd_code { get; set; }

        public string sims_parent_father_dob { get; set; }

        public string sims_parent_mother_dob { get; set; }
    }



    public class UserGroup1
    {
        public string Comn_User_Group_Code { get; set; }
        public string Comn_User_Group_Name { get; set; }

        public string user_group_code { get; set; }

        public string user_group_name { get; set; }
    }

    #region album sims078
    public class Sims078
    {
        public string sims_album_code { get; set; }
        public string academic_year { get; set; }
        public string sims_album_name { get; set; }
        public string sims_album_description { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_path { get; set; }
        public string sims_album_ImageName { get; set; }
        public string sims_album_photo_description { get; set; }
        public string sims_album_thumbnail_path { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public decimal sims_album_photo_price { get; set; }
        public string sims_album_photo_status { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_album_publish_date { get; set; }
        public string sims_album_expiry_date { get; set; }
        public bool sims_album_visible_to_portal { get; set; }
        public bool sims_album_status { get; set; }
        public string sims_tile_color { get; set; }
        public string Classes { get; set; }
        public string UserGroups { get; set; }
        public List<GRD> ClassList { get; set; }
        public List<UserGroup1> UserGroupList { get; set; }

        public string image { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
         public string sims_section_code { get; set; }
        public bool IsSelected { get; set; }
        public string sims_academic_start_date { get; set; }
        public string sims_academic_end_date { get; set; }

        public string tname { get; set; }

        public string grade { get; set; }
        public string AlbumCount { get; set; }

        public string ClassName { get; set; }
          public string ClassCode { get; set; }
        public string msg_body_temp { get; set; }
        public string sims_form_field { get; set; }
        public string sims_mod_code { get; set; }
        public string sims_appl_code { get; set; }
        public string sims_email_flag { get; set; }
        public string sims_smtp_id { get; set; }
        public string sims_email_templ_no { get; set; }
        public string sims_sms_flag { get; set; }
        public string sims_sms_templ_no { get; set; }
        public string sims_alert_flag { get; set; }
        public string sims_alert_templ_no { get; set; }
        public bool sims_status { get; set; }
    }

    public class GRD
    {
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public List<SEC> Sections { get; set; }
    }

    public class SEC
    {
        public string sims_gradeCode_s { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
    }


    #endregion


    #region album sims078
    public class Sims078Html
    {
        public string sims_album_code { get; set; }
        public string academic_year { get; set; }
        public string sims_album_name { get; set; }
        public string sims_album_description { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_path { get; set; }
        public string sims_album_ImageName { get; set; }
        public string sims_album_photo_description { get; set; }
        public string sims_album_thumbnail_path { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public decimal sims_album_photo_price { get; set; }
        public string sims_album_photo_status { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_album_publish_date { get; set; }
        public string sims_album_expiry_date { get; set; }
        public bool sims_album_visible_to_portal { get; set; }
        public bool sims_album_status { get; set; }
        public string sims_tile_color { get; set; }
        public string Classes { get; set; }
        public string UserGroups { get; set; }

        public string image { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public bool IsSelected { get; set; }
        public string sims_academic_start_date { get; set; }
        public string sims_academic_end_date { get; set; }

        public string tname { get; set; }

        public string grade { get; set; }
        public string AlbumCount { get; set; }

        public string ClassName { get; set; }

        public string ClassCode { get; set; }
       // public string sims_msg_body { get; set; }
        public string opr { get; set; }
    }


    public class Sims078Ht
    {
        public string sims_album_code { get; set; }
        public string sims_album_photo_serial_number { get; set; }
        public string sims_album_photo_id { get; set; }
        public string sims_album_photo_description { get; set; }
        public bool sims_album_photo_sell_flag { get; set; }
        public string sims_album_photo_price { get; set; }
        public string sims_album_photo_path { get; set; }
        public string opr { get; set; }
        public string sims_album_photo_status { get; set; }
        public List<string> sims_album_photo_path_lst { get; set; }

    }
    #endregion

    public class sims_section_elective_subject_master
    {

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_language_code { get; set; }
        public string sims_language_desc_remark { get; set; }
        public string sims_elective_subject { get; set; }
        public string sims_compulsory_subject { get; set; }
        public string sims_line_no { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string subject_code { get; set; }
        public string sims_display_order { get; set; }
        public string sims_language_desc { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string sims_academic_year_description { get; set; }
        public bool sims_status { get; set; }

        public List<string> gradelist { get; set; }
        public List<string> sectionlist { get; set; }
        public List<string> subjectlist { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_status_chk { get; set; }
    }

    #region Sims526(UploadDocument)

    public class Sims526
    {

        public List<Uccw229> cri_lst { get; set; }
        public string count { get; set; }
        public string grade_code { get; set; }
        public string grade_name_en { get; set; }
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_image { get; set; }
        public string sims_student_img { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_full_name_en { get; set; }
        public string Student_name { get; set; }
    }
    #endregion


    #region Sims183(BoardDetail)

    public class Sims183
    {
        public string opr { get; set; }
        public string sims_board_address { get; set; }
        public string sims_board_code { get; set; }
        public string sims_board_contact_no { get; set; }
        public string sims_board_name { get; set; }
        public string sims_board_short_name { get; set; }
        public bool sims_board_status { get; set; }

    }
    #endregion

    #region Sims184(BoardExamDetail)

    public class Sims184
    {
        public string opr { get; set; }
        public string sims_board_code { get; set; }
        public string sims_board_exam_code { get; set; }
        public string sims_board_exam_fee { get; set; }
        public string sims_board_exam_name { get; set; }
        public string sims_board_exam_short_name { get; set; }
        public bool sims_board_exam_status { get; set; }
        public string sims_board_exam_year { get; set; }
        public string sims_board_short_name { get; set; }
    }
    #endregion

    #region Sims185(studentexamboard)

    public class Sims185
    {
        public string opr { get; set; }
        public string registration_num { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_board_code { get; set; }
        public string sims_board_exam_cod { get; set; }
        public string sims_board_exam_code { get; set; }
        public string sims_board_exam_enroll_number { get; set; }
        public string sims_board_exam_name { get; set; }
        public string sims_board_exam_registration_no { get; set; }
        public bool sims_board_exam_status { get; set; }
        public bool sims_board_exam_student_fee_paid_status { get; set; }
        public bool sims_board_exam_student_status { get; set; }
        public string sims_board_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_student_admission_grade_code { get; set; }
        public string sims_student_cur_code { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_full_name_en { get; set; }
        public string StudentName { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_roll_number { get; set; }
    }
    #endregion

    #region Pers116(LeaveType)

    public class Pers116
    {
        public string opr { get; set; }
        public bool le_accumulated_tag { get; set; }
        public string le_company_code { get; set; }
        public string le_formula_code { get; set; }
        public string le_leave_code { get; set; }
        public string le_max_days_allowed { get; set; }
        public string co_company_code { get; set; }
        public string co_desc { get; set; }
        public string fm_code { get; set; }
        public string fm_desc { get; set; }
        public string cl_code { get; set; }
        public string cl_desc { get; set; }
        public string lCode { get; set; }

    }
    #endregion

    #region Sims175(StudentImageView)

    public class Sims175
    {
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_image { get; set; }
        public string sims_student_img { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_middle_name_en { get; set; }
        public string sims_student_passport_full_name_en { get; set; }

        public string parent_Father_name { get; set; }

        public string sims_parent_father_img { get; set; }

        public string parent_Mother_name { get; set; }

        public string sims_parent_mother_img { get; set; }

        public string sims_parent_login_code { get; set; }
    }
    #endregion

    public class ass_att_file
    {
        public string file_line_no { get; set; }
        public string file_name_path { get; set; }

    }
    public class Sim508
    {
        public List<ass_att_file> file_coll { get; set; }

        public string opr { get; set; }
        public string file_format { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_academic_year_end_date { get; set; }
        public string stud_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_assignment_desc { get; set; }
        public string sims_assignment_doc_line_no { get; set; }
        public string sims_assignment_doc_path_name { get; set; }
        public string sims_assignment_doc_path { get; set; }
        public string sims_assignment_freeze_date { get; set; }
        public string sims_assignment_number { get; set; }
        public string sims_assignment_start_date { get; set; }
        public bool sims_assignment_status { get; set; }
        public string sims_assignment_status_1 { get; set; }
        public string sims_assignment_subject_code { get; set; }
        public string sims_assignment_submission_date { get; set; }
        public string sims_assignment_teacher_code { get; set; }
        public string sims_assignment_term_code { get; set; }
        public string sims_assignment_title { get; set; }
        public string sims_bell_teacher_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_doc_line  { get; set; }
        public string sims508_doc_name { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_subject_Code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_term_code { get; set; }
    }

    #region Sims108(Certificate Request)
    public class Sims108
    {
        public string sims_certificate_name { get; set; }
        public string sims_certificate_number { get; set; }
        public string sims_certificate_request_number { get; set; }
        public string sims_certificate_enroll_number { get; set; }
        public string sims_certificate_request_date { get; set; }
        public string sims_certificate_requested_by { get; set; }
        public string sims_certificate_reason { get; set; }
        public string sims_certificate_expected_date { get; set; }

        public string sims_certificate_issued_by { get; set; }
        public string sims_certificate_request_status { get; set; }

        public string opr { get; set; }
        public string user_type { get; set; }
        //public string sims_certificate_parent_number { get; set; }
        //public string sims_certificate_employee_number { get; set; }
    }
    #endregion


    #region Sim509(Teacher Assignment View)
    public class Sim509
    {
        public string assign_no { get; set; }
        public string sims_assignment_enroll_number { get; set; }
        public string reject_remark { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_grade_code { get; set; }
        public string class_details { get; set; }
        public string sims_section_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_assignment_title { get; set; }
        public string sims_assignment_desc { get; set; }
        public string sims_assignment_start_date { get; set; }
        public string sims_assignment_submission_date { get; set; }
        public string sims_assignment_freeze_date { get; set; }
        public string sims_assignment_status { get; set; }
        public string sims_assignment_status_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_name { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_assignment_number { get; set; }
        public bool isselected { get; set; }
        public bool sims_archive_status { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_name { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_name { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_gb_cat_assign_name { get; set; }
        public string sims_assignment_teacher_code { get; set; }
        public string sims_assignment_teacher_name { get; set; }

        public string sims_assignment_subject_code { get; set; }
        public string stud_name { get; set; }
        public string stud_code { get; set; }
        public string stud_status { get; set; }
        public string sims_assignment_doc_path { get; set; }
        public bool status { get; set; }
        public string sims_gb_cat_assign_date { get; set; }
        public string sims_gb_cat_assign_due_date { get; set; }
        public string sims_term_code { get; set; }

        public string sims_employee_code { get; set; }


        public string opr { get; set; }

        public string sims_student_passport_first_name_en { get; set; }
        public string sims_assignment_student_remark { get; set; }
        public List<sims509_doctdetails> doc_details { get; set; }

        public string subjectName { get; set; }
        public string Grade { get; set; }
        public string Section { get; set; }
        public string submission { get; set; }
        public string assign_date { get; set; }


        public string sims_student_enroll_number { get; set; }
        ////Student Assignment Details
        public string StudentName { get; set; }
        public string sims_assignment_teacher_remark { get; set; }
        public string count { get; set; }
        public string sims_assignment_student_doc_date { get; set; }
        public string sims_assignment_student_doc_path { get; set; }

        public string sims509_enroll_no { get; set; }
        public string sims509_desc { get; set; }
        public string sims509_desc_name { get; set; }
        public string sims509_Assignment_No { get; set; }
        public string sims_doc_line { get; set; }
        public string sims509_doc_remark { get; set; }

    }

    public class sims509_doctdetails
    {
        public string sims509_enroll_no { get; set; }
        public string sims509_doc_remark { get; set; }
        public string sims_doc_line { get; set; }
        public string sims509_doc { get; set; }
        public string sims509_doc_name { get; set; }
    }
    #endregion


    public class meetingsclass
    {
        public string sims_mom_reference_number { get; set; }
        public string sims_mom_type_code { get; set; }

        public string sims_mom_type_desc_en { get; set; }

        public string sims_mom_subject { get; set; }

        public string sims_mom_venue { get; set; }

        public string sims_mom_date { get; set; }

        public string sims_mom_start_time { get; set; }

        public string sims_mom_end_time { get; set; }

        public string sims_mom_chairperson_user_code { get; set; }

        public string sims_mom_recorder_user_code { get; set; }

        public string sims_mom_requester_user_code { get; set; }

        public string sims_mom_approver_user_code { get; set; }

        public string sims_mom_agenda { get; set; }

        public string sims_mom_status { get; set; }

        public string sims_mom_communication_status { get; set; }
        public string opr { get; set; }
        public string sims_mom_date_creation_user_code { get; set; }
        public string em_number { get; set; }

        public string em_first_name { get; set; }

        public string sims_mom_number { get; set; }

        public string attendees_name { get; set; }

        public string sims_mom_receipient_user_code { get; set; }

        public string sims_mom_requester_user_name { get; set; }

        public string sims_mom_chairperson_user_name { get; set; }

        public string sims_response_flag { get; set; }

        public string sims_mom_receipient_response_remark { get; set; }

        public string empcode { get; set; }

        public string employee_code { get; set; }

        public bool sims_mom_receipient_attendance { get; set; }

        public string sims_mom_srl_no { get; set; }

        public string sims_mom_description { get; set; }

        public string sims_mom_raised_user_code { get; set; }

        public string sims_mom_tobedone_user_code { get; set; }

        public string sims_mom_tobedone_date { get; set; }

        public string sims_mom_actual_start_time { get; set; }

        public string sims_mom_actual_end_time { get; set; }

        public string sims_mom_raised_user_name { get; set; }

        public string sims_mom_tobedone_user_name { get; set; }

        public string sims_mom_attendees_name { get; set; }

        public string sims_mom_actual_start_date { get; set; }

        public string meeting_conductor { get; set; }
    }




    //public class Com007
    //{

    //    public string st_time { get; set; }

    //    public string comn_appl_name_en { get; set; }
    //    public string comn_audit_dns { get; set; }
    //    public string comn_audit_end_time { get; set; }
    //    public string comn_audit_ip { get; set; }
    //    public string comn_audit_remark { get; set; }
    //    public string comn_audit_start_time { get; set; }
    //    public string comn_audit_user_code { get; set; }
    //    public string comn_mod_name_en { get; set; }
    //    public string comn_user_name { get; set; }
    //    public string comn_appl_code { get; internal set; }
    //    public string st_am { get; internal set; }
    //    public string comn_mod_img { get; set; }
    //    public string st_date { get; set; }
    //}
    public class AuditAlert
    {
        public string comn_alert_date { get; set; }
        public string comn_alert_message { get; set; }
        public string comn_alert_number { get; set; }
        public string comn_alert_priority_code { get; set; }
        public string comn_alert_status { get; set; }
        public string comn_alert_type_code { get; set; }
        public string comn_alert_user_code { get; set; }
        public string comn_mod_name_en { get; set; }
        public string st_am { get; set; }
        public string st_date { get; set; }
        public string st_time { get; set; }
    }

    public class Mograversion
{
    public string comn_appl_project_code { get; set; }
    public string comn_appl_project_name { get; set; }
    public string comn_mod_code { get; set; }
    public string comn_mod_name_e { get; set; }
    public string comn_appl_code { get; set; }
    public string comn_appl_name { get; set; }
    public string lic_changeset_no{ get; set; }
    public string lic_appl_code{ get; set; }
    public string lic_appl_name { get; set; }
    public string lic_sp_name{ get; set; }
    public string lic_sp_remark{ get; set; }
    public string lic_remark{ get; set; }
    public string lic_document { get; set; }
    public string lic_test_case{ get; set; }
    public string lic_approve_by { get; set; }
    public string approved_by { get; set; }
    public string opr { get; set; }
    public bool doccheck{ get; set; }
    public bool testcheck { get; set; }
    public string lic_version_number{ get; set; }
    public string lic_release_number{ get; set; }
    public string lic_patch_number { get; set; }


}


    public class masterconfig
    {

        public string comn_mod_code{ get; set; }
        public string comn_mod_name_en{ get; set; }
        public string comn_appl_code{ get; set; }
        public string comn_appl_name_en{ get; set; }
        public string comn_sequence_code{ get; set; }
        public string comn_description{ get; set; }
        public string comn_cur_code{ get; set; }
        public string comn_academic_year{ get; set; }
        public string comn_character_count{ get; set; }
        public string comn_character_sequence{ get; set; }
        public string comn_number_count{ get; set; }
        public string comn_number_sequence{ get; set; }
        public string comn_parameter_ref { get; set; }
        public string sims_mod_code{ get; set; }
		public string sims_appl_code{ get; set; }
		public string sims_appl_form_field{ get; set; }
		public string sims_appl_parameter{ get; set; }
		public string sims_appl_form_field_value1{ get; set; }
        public string sims_appl_form_field_value2 { get; set; }
        public string opr { get; set; }



    }



    public class email
    {
        public string sims_email_cc { get; set; }
        public string sims_email_to { get; set; }
        public string comn_appl_code { get; set; }
        public string comn_appl_mod_code { get; set; }

        public string comn_appl_name_en { get; set; }
        public bool comn_appl_status { get; set; }
        public string comn_appl_type { get; set; }
        public string comn_app_mode { get; set; }
        public string comn_mod_code { get; set; }
        public string comn_mod_name_en { get; set; }
        public bool comn_mod_status { get; set; }
        public string message_body { get; set; }
        public string opr { get; set; }
        public string sims_msg_appl_code { get; set; }
        public string sims_msg_body { get; set; }
        public string sims_msg_mod_code { get; set; }
        public string sims_msg_signature { get; set; }
        public string sims_msg_sr_no { get; set; }
        public bool sims_msg_status { get; set; }
        public string sims_msg_subject { get; set; }
        public string sims_msg_type { get; set; }
        




    }

    #region Sims176 (Student Report)


    public class Sims176
    {
        public string sims_roll_number { get; set; }
        public string sims_student_dob1 { get; set; }
        public string comn_user_app_status { get; set; }
        public string comn_user_device_status { get; set; }
        public string comn_user_last_login { get; set; }

        public string sims_transport_route_in { get; set; }
        public string sims_transport_bus_in { get; set; }
        public string sims_transport_route_out { get; set; }
        public string sims_transport_bus_out { get; set; }
        //public string sims_roll_number { get; set; }
        //public string sims_student_dob1 { get; set; }

        public string sims_active_member { get; set; }
        public string sims_contact_name { get; set; }
        public string sims_contact_number { get; set; }
        public string sims_fee_contact_person { get; set; }
        public string sims_fee_Pref { get; set; }
        public string sims_MOE_Number { get; set; }
        public string sims_primary_contact_email_id { get; set; }
        public string sims_fee_Payment_Contact_email_id { get; set; }
        public string sims_fee_Payment_Contact_Mobile_Number { get; set; }
        public string sims_fee_stream { get; set; }
        public string sims_parent_number { get; set; }
        public string mobileNo { get; set; }
        
            

        public string sims_student_main_language_name { get; set; }
        public string sims_health_detail { get; set; }
        public string cur_shrt_name { get; set; }
        public string sims_blood_group { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_name { get; set; }
        public string cur_shrt_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }

        public string sims_transport_route_name { get; set; }
        public string sims_house_name { get; set; }
        public List<Sims176> section_lst { get; set; }
        public string user_group_type { get; set; }

        public string sims_student_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_middle_name_en { get; set; }
        public string sims_student_passport_last_name_en { get; set; }
        public string sims_student_family_name_en { get; set; }
        public string sims_student_nickname { get; set; }

        public string sims_student_passport_first_name_ot { get; set; }
        public string sims_student_passport_middle_name_en_ot { get; set; }
        public string sims_student_passport_last_name_en_ot { get; set; }
        public string sims_student_family_name_ot { get; set; }
        public string student_name_ { get; set; }
        public string sims_student_attribute1 { get; set; }
        public string sims_student_attribute2 { get; set; }
        public string sims_student_attribute3 { get; set; }
        public string sims_student_attribute4 { get; set; }
        public string sims_student_attribute5 { get; set; }

        public string grade_name { get; set; }
        public string section_name { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_student_date { get; set; }
        public string sims_student_commence_date { get; set; }
        public string student_birth_country { get; set; }
        public string sims_student_admitted_in_class { get; set; }
        public string sims_student_dob { get; set; }
        public string country_name { get; set; }
        public string std_religion { get; set; }
        public string nationality_name { get; set; }
        public string sims_student_remark { get; set; }
        public string sims_student_admission_grade_code { get; set; }
        public string sims_student_admission_academic_year { get; set; }
        public string sims_student_prev_school { get; set; }
        public string sims_student_visa_number { get; set; }
        public string sims_student_visa_type { get; set; }
        public string sims_student_visa_issuing_place { get; set; }
        public string sims_student_visa_issuing_authority { get; set; }
        public string sims_student_visa_issue_date { get; set; }
        public string sims_student_visa_expiry_date { get; set; }
        public string sims_student_national_id { get; set; }
        public string sims_student_national_id_issue_date { get; set; }
        public string sims_student_national_id_expiry_date { get; set; }
        public string sims_student_ea_number { get; set; }
        public string sims_student_ea_registration_date { get; set; }
        public string sims_student_ea_status { get; set; }
        public string sims_student_ea_transfer_status { get; set; }
        public string sims_student_passport_number { get; set; }
        public string sims_student_passport_issuing_authority { get; set; }
        public string sims_student_passport_issue_place { get; set; }
        public string sims_student_passport_issue_date { get; set; }
        public string sims_student_passport_expiry_date { get; set; }
        public string sims_student_age { get; set; }
        public string sims_sibling_student_number { get; set; }
        public string Student_Admission_Status { get; set; }
        public string sims_ethnicity_name_en { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string guardian_name { get; set; }
        public string father_nationality1 { get; set; }
        public string father_nationality2 { get; set; }
        public string mother_nationality1 { get; set; }
        public string mother_nationality2 { get; set; }
        public string guardian_nationality1 { get; set; }
        public string guardian_nationality2 { get; set; }
        public string sims_parent_father_name_ot { get; set; }
        public string sims_parent_mother_name_ot { get; set; }
        public string sims_parent_guardian_name_ot { get; set; }
        public string sims_parent_father_occupation { get; set; }
        public string sims_parent_mother_occupation { get; set; }
        public string sims_parent_guardian_occupation { get; set; }
        public string sims_parent_father_company { get; set; }
        public string sims_parent_mother_company { get; set; }
        public string sims_parent_guardian_company { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_mother_summary_address { get; set; }
        public string sims_parent_guardian_summary_address { get; set; }
        public string sims_parent_father_phone { get; set; }
        public string sims_parent_mother_phone { get; set; }
        public string sims_parent_guardian_phone { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }

        public string sims_parent_father_street_number { get; set; }
        public string sims_parent_father_area_number { get; set; }
        public string sims_parent_father_state { get; set; }
        public string sims_parent_father_city { get; set; }
        public string sims_parent_mother_street_number { get; set; }
        public string sims_parent_mother_area_number { get; set; }
        public string sims_parent_mother_state { get; set; }
        public string sims_parent_mother_city { get; set; }

        public string sims_parent_guardian_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_guardian_email { get; set; }
        public string sims_parent_father_fax { get; set; }
        public string sims_parent_mother_fax { get; set; }
        public string sims_parent_guardian_fax { get; set; }
        public string sims_parent_father_passport_number { get; set; }
        public string sims_parent_father_passport_expiry_date { get; set; }
        public string sims_parent_mother_passport_number { get; set; }
        public string sims_parent_mother_passport_expiry_date { get; set; }
        public string sims_parent_guardian_passport_number { get; set; }
        public string sims_parent_guardian_passport_expiry_date2 { get; set; }
        public string sims_parent_father_national_id { get; set; }
        public string sims_parent_father_national_id__expiry_date { get; set; }
        public string sims_parent_mother_national_id { get; set; }
        public string sims_parent_mother_national_id_expiry_date { get; set; }
        public string sims_parent_guardian_national_id { get; set; }
        public string sims_parent_guardian_national_id_expiry_date { get; set; }
        public string Sr_no { get; set; }
        public string sims_transport_bus_name { get; set; }
        public string sims_mai_status { get;  set; }
        public string sims_aids_status { get;  set; }
        public string sims_llma_status { get;  set; }
        public string sims_transport_route_direction { get; set; }
        public string sims_transport_Pickup_bus_name { get; set; }
        public string sims_transport_Drop_bus_name { get; set; }
        public string sims_transport_route_student_status { get; set; }
        public string sims_student_mother_language_name { get; set; }

        public string sims_student_emergency_contact_name1 { get; set; }
        public string sims_student_emergency_contact_number1 { get; set; }
        public string sims_student_emergency_contact_name2 { get; set; }
        public string sims_student_emergency_contact_number2 { get; set; }
        public string sims_student_pan_no { get; set; }
        public string sims_student_voter_id { get; set; }
        public string sims_student_primary_contact_pref { get; set; }
        public string sims_student_primary_contact_code { get; set; }
        public string sims_student_fee_payment_contact_pref { get; set; }

        public string sims_student_employee_comp_code { get; set; }
        public string sims_student_employee_code { get; set; }
        public int sims_student_enroll_number1 { get;  set; }

        public string stu_roll { get; set; }

        public int stu_roll1 { get; set; }

        public string sims_student_status { get; set; }
        public string sims_student_email { get; set; }
        public string sims_student_last_name_reverse { get; set; }
    }


    #endregion


    #region Sims546 (Admission Report)

    public class admission_report
    {
        public string Sr_no { get; set; }
        public string sims_admission_number { get; set; }
        public string sims_admission_date { get; set; }
        public string sims_admission_school_code { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_admission_academic_year { get; set; }
        public string sims_admission_grade_code { get; set; }
        public string sims_admission_tentative_joining_date { get; set; }
        public string sims_admission_passport_number { get; set; }
        public string sims_admission_passport_issue_date { get; set; }
        public string sims_admission_passport_expiry_date { get; set; }
        public string sims_admission_passport_issuing_authority { get; set; }
        public string sims_admission_passport_issue_place { get; set; }
        public string sims_admission_passport_first_name_en { get; set; }
        public string sims_admission_passport_middle_name_en { get; set; }
        public string sims_admission_passport_last_name_en { get; set; }
        public string sims_admission_family_name_en { get; set; }
        public string sims_admission_nickname { get; set; }
        public string sims_admission_gender { get; set; }
        public string sims_admission_religion_code { get; set; }
        public string sims_admission_dob { get; set; }
        public string sims_admission_commencement_date { get; set; }
        public string sims_admission_birth_country_code { get; set; }
        public string sims_admission_nationality_code { get; set; }
        public string sims_admission_visa_number { get; set; }
        public string sims_admission_visa_issue_date { get; set; }
        public string sims_admission_visa_expiry_date { get; set; }
        public string sims_admission_visa_issuing_place { get; set; }
        public string sims_admission_visa_issuing_authority { get; set; }
        public string sims_admission_visa_type { get; set; }
        public string sims_admission_national_id { get; set; }
        public string sims_admission_national_id_issue_date { get; set; }
        public string sims_admission_national_id_expiry_date { get; set; }
        public string sims_admission_sibling_name { get; set; }
        public string sims_admission_sibling_status { get; set; }
        public string sims_admission_sibling_enroll_number { get; set; }
        public string sims_admission_parent_id { get; set; }
        public string sims_admission_sibling_dob { get; set; }
        public string sims_admission_primary_contact_pref { get; set; }
        public string sims_admission_transport_status { get; set; }
        public string sims_admission_transport_desc { get; set; }
        public string sims_admission_father_salutation_code { get; set; }
        public string sims_admission_father_first_name { get; set; }
        public string sims_admission_father_middle_name { get; set; }
        public string sims_admission_father_last_name { get; set; }
        public string sims_admission_father_family_name { get; set; }
        public string sims_admission_father_nationality1_code { get; set; }
        public string sims_admission_father_nationality2_code { get; set; }
        public string sims_admission_father_appartment_number { get; set; }
        public string sims_admission_father_building_number { get; set; }
        public string sims_admission_father_street_number { get; set; }
        public string sims_admission_father_area_number { get; set; }
        public string sims_admission_father_summary_address { get; set; }
        public string sims_admission_father_city { get; set; }
        public string sims_admission_father_state { get; set; }
        public string sims_admission_father_country_code { get; set; }
        public string sims_admission_father_phone { get; set; }
        public string sims_admission_father_mobile { get; set; }
        public string sims_admission_father_email { get; set; }
        public string sims_admission_father_fax { get; set; }
        public string sims_admission_father_po_box { get; set; }
        public string sims_admission_father_occupation { get; set; }
        public string sims_admission_father_company { get; set; }
        public string sims_admission_father_passport_number { get; set; }
        public string sims_admission_mother_salutation_code { get; set; }
        public string sims_admission_mother_first_name { get; set; }
        public string sims_admission_mother_middle_name { get; set; }
        public string sims_admission_mother_last_name { get; set; }
        public string sims_admission_mother_family_name { get; set; }
        public string sims_admission_mother_name_ot { get; set; }
        public string sims_admission_mother_nationality1_code { get; set; }
        public string sims_admission_mother_nationality2_code { get; set; }
        public string sims_admission_mother_appartment_number { get; set; }
        public string sims_admission_mother_building_number { get; set; }
        public string sims_admission_mother_street_number { get; set; }
        public string sims_admission_mother_area_number { get; set; }
        public string sims_admission_mother_summary_address { get; set; }
        public string sims_admission_mother_city { get; set; }
        public string sims_admission_mother_state { get; set; }
        public string sims_admission_mother_country_code { get; set; }
        public string sims_admission_mother_phone { get; set; }
        public string sims_admission_mother_mobile { get; set; }
        public string sims_admission_mother_email { get; set; }
        public string sims_admission_mother_fax { get; set; }
        public string sims_admission_mother_po_box { get; set; }
        public string sims_admission_mother_occupation { get; set; }
        public string sims_admission_mother_company { get; set; }
        public string sims_admission_mother_passport_number { get; set; }
        public string sims_admission_guardian_salutation_code { get; set; }
        public string sims_admission_guardian_first_name { get; set; }
        public string sims_admission_guardian_middle_name { get; set; }
        public string sims_admission_guardian_last_name { get; set; }
        public string sims_admission_guardian_family_name { get; set; }
        public string sims_admission_guardian_name_ot { get; set; }
        public string sims_admission_guardian_nationality1_code { get; set; }
        public string sims_admission_guardian_nationality2_code { get; set; }
        public string sims_admission_guardian_appartment_number { get; set; }
        public string sims_admission_guardian_building_number { get; set; }
        public string sims_admission_guardian_street_number { get; set; }
        public string sims_admission_guardian_area_number { get; set; }
        public string sims_admission_guardian_summary_address { get; set; }
        public string sims_admission_guardian_city { get; set; }
        public string sims_admission_guardian_state { get; set; }
        public string sims_admission_guardian_country_code { get; set; }
        public string sims_admission_guardian_phone { get; set; }
        public string sims_admission_guardian_mobile { get; set; }
        public string sims_admission_guardian_email { get; set; }
        public string sims_admission_guardian_fax { get; set; }
        public string sims_admission_guardian_po_box { get; set; }
        public string sims_admission_guardian_occupation { get; set; }
        public string sims_admission_guardian_company { get; set; }
        public string sims_admission_guardian_relationship_code { get; set; }
        public string sims_admission_guardian_passport_number { get; set; }
        public string sims_admission_marketing_code { get; set; }
        public string sims_admission_marketing_description { get; set; }
        public string sims_admission_legal_custody { get; set; }
        public string sims_admission_current_school_name { get; set; }
        public string sims_admission_current_school_head_teacher { get; set; }
        public string sims_admission_current_school_enroll_number { get; set; }
        public string sims_admission_current_school_grade { get; set; }
        public string sims_admission_current_school_cur { get; set; }
        public string sims_admission_current_school_language { get; set; }
        public string sims_admission_current_school_address { get; set; }
        public string sims_admission_current_school_city { get; set; }
        public string sims_admission_current_school_country_code { get; set; }
        public string sims_admission_current_school_phone { get; set; }
        public string sims_admission_current_school_fax { get; set; }
        public string sims_admission_current_school_from_date { get; set; }
        public string sims_admission_current_school_to_date { get; set; }
        public string sims_admission_declaration_status { get; set; }
        public string sims_admission_fees_paid_status { get; set; }
        public string sims_admission_status { get; set; }
        public string sims_admission_user_code { get; set; }
        public string sims_admission_section_code { get; set; }
        public string sims_admission_fee_category { get; set; }
        public string sims_admission_father_national_id { get; set; }
        public string sims_admission_mother_national_id { get; set; }
        public string sims_admission_guardian_national_id { get; set; }
        public string sims_student_organizational_skills { get; set; }
        public string sims_student_social_skills { get; set; }
        public string sims_student_special_education { get; set; }
        public string sims_admission_father_company_address { get; set; }
        public string sims_admission_father_company_phone { get; set; }
        public string sims_admission_father_qualification { get; set; }
        public string sims_admission_mother_company_address { get; set; }
        public string sims_admission_mother_company_phone { get; set; }
        public string sims_admission_mother_qualification { get; set; }
        public string sims_admission_mother_national_id_expiry_date { get; set; }
        public string sims_admission_guardian_national_id_expiry_date { get; set; }
        public string sims_admission_father_national_id_expiry_date { get; set; }
        public string sims_admission_father_passport_expiry_date { get; set; }
        public string sims_admission_mother_passport_expiry_date { get; set; }
        public string sims_admission_guardian_passport_expiry_date { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_grade_name_en { get; set; }

public string hsc_institution_name { get; set; }
        public string hsc_institution_place { get; set; }
        public string board_name { get; set; }
        public string year_pasing { get; set; }
        public string seat_no { get; set; }
        public string ssc_seat_no { get; set; }
        public string ssc_no_of_attempts { get; set; }
        public string no_of_attempts { get; set; }
        public string hsc_marks { get; set; }
        public string marks_out { get; set; }
        public string hsc_percentage { get; set; }
        public string hsc_percentage_maths { get; set; }
        public string hsc_percentage_science { get; set; }
        public string XI_percentage_science { get; set; }
        public string XI_percentage_maths { get; set; }
        public string ssc_math { get; set; }
        public string ssc_science { get; set; }
        public string fy_institution_name { get; set; }
        public string fy_institution_place { get; set; }
        public string fy_board_name { get; set; }
        public string fy_year_pasing { get; set; }
        public string fy_seat_no { get; set; }
        public string fy_no_of_attempts_I { get; set; }
        public string fy_no_of_obtained_I { get; set; }
        public string fy_marks_out_I { get; set; }
        public string fy_percentage_I { get; set; }
        public string fy_no_of_attempts_II { get; set; }
        public string fy_no_of_obtained_II { get; set; }
        public string fy_marks_out_II { get; set; }
        public string fy_percentage_II { get; set; }
        public string sy_institution_name { get; set; }
        public string sy_institution_place { get; set; }
        public string sy_board_name { get; set; }
        public string sy_year_pasing { get; set; }
        public string sy_seat_no { get; set; }
        public string sy_no_of_attempts_III { get; set; }
        public string sy_no_of_obtained_III { get; set; }
        public string sy_marks_out_III { get; set; }
        public string sy_percentage_III { get; set; }
        public string sy_no_of_attempts_IV { get; set; }
        public string sy_no_of_obtained_IV { get; set; }
        public string sy_marks_out_IV { get; set; }
        public string sy_percentage_IV { get; set; }
        public string sims_admission_student_email{ get; set; }
        public string sims_admission_student_mobile { get; set; }
        public string sims_admission_is_physically_handicapped { get; set; }
        public string sims_admission_marital_status { get; set; }
        public string sims_student_category { get; set; }
        public string sims_admission_blood_group_code { get; set; }
        public string sims_admission_is_freedom_fighter { get; set; }
        public string sims_admission_birthplace { get; set; }
    }

    #endregion


    #region Sims120(Library Fee)

    public class Sims120
    {
        public string opr { get; set; }
        public string sims_library_privilege_name { get; set; }
        public string sims_library_privilege_number { get; set; }
        public string sims_library_fee_number { get; set; }
        public string sims_library_fee_name { get; set; }
        public string sims_library_fee_deposit_amount { get; set; }
        public string sims_library_fee_amount { get; set; }
        public string sims_library_fee_manual_receipt_number { get; set; }
        public string sims_library_fee_frequency { get; set; }
        public bool sims_library_fee_installment_mode { get; set; }
        public string sims_library_fee_installment_minimum_amount { get; set; }
        public string sims_library_fee_period1 { get; set; }
        public string sims_library_fee_period2 { get; set; }
        public string sims_library_fee_period3 { get; set; }
        public string sims_library_fee_period4 { get; set; }
        public string sims_library_fee_period5 { get; set; }
        public string sims_library_fee_period6 { get; set; }
        public string sims_library_fee_period7 { get; set; }
        public string sims_library_fee_period8 { get; set; }
        public string sims_library_fee_period9 { get; set; }
        public string sims_library_fee_period10 { get; set; }
        public string sims_library_fee_period11 { get; set; }
        public string sims_library_fee_period12 { get; set; }
        public bool sims_library_fee_status { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
    }

    #endregion

    #region Sims668(Tc Certificate Requiest)

    public class Sims668
    {
        public string opr { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public string sims_tc_certificate_reason { get; set; }
        public string sims_tc_certificate_requested_by { get; set; }
        public string sims_tc_certificate_request_date { get; set; }
        public string sims_library_fee_amount { get; set; }
        public string sims_tc_certificate_request_number { get; set; }
        public string sims_tc_certificate_expected_date { get; set; }
        public string sims_tc_certificate_request_status { get; set; }
        public string sims_tc_certificate_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_tc_certificate_academic_year { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }

        public bool sims_fee_clr_status { get; set; }
        public bool sims_finn_clr_status { get; set; }
        public bool sims_inv_clr_status { get; set; }
        public bool sims_inci_clr_status { get; set; }
        public bool sims_lib_clr_status { get; set; }
        public bool sims_trans_clr_status { get; set; }
        public bool sims_acad_clr_status { get; set; }
        public bool sims_admin_clr_status { get; set; }
        public bool sims_other1_clr_status { get; set; }
        public bool sims_other2_clr_status { get; set; }
        public bool sims_other3_clr_status { get; set; }

        public string year_desc { get; set; }
        
        public string sims_tc_direct_approve_status_code { get; set; }
        public string sims_tc_direct_approve_status { get; set; }

        public string sims_tc_certificate_academic_year_desc { get; set; }
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_appl_parameter_desc { get; set; }

        public string sims_clr_sr_no { get; set; }
        public string sims_clr_cur_code { get; set; }
        public string sims_clr_academic_year { get; set; }
        public string sims_clr_employee_id { get; set; }
        public string sims_clr_access_code { get; set; }
        public string sims_clr_class_code { get; set; }
        public string sims_clr_preference_order { get; set; }
        public string sims_clr_status { get; set; }
        public string sims_clr_created_by { get; set; }
        public string sims_clr_created_date { get; set; }
        public string sims_clr_access_from_date { get; set; }
        public string sims_clr_access_to_date { get; set; }

        public string sims_sr_no { get; set; }
        public string sims_clr_by { get; set; }
        public string sims_clr_remark { get; set; }
        public string sims_clr_date { get; set; }

    }

    #endregion

    #region Sims670 Clear Cancel Admission
    public class Sim670
    {
        public string sims_clr_tran_remark { get; set; }
        public string sims_clr_tran_created_date { get; set; }
        public string sims_clr_tran_employee_id { get; set; }
        public bool sims_clr_tran_status { get; set; }
        public string sims_clr_tran_access_code { get; set; }
        public string opr { get; set; }


        public string sims_sr_no { get; set; }
        public string sims_enroll_number { get; set; }
        public bool sims_fee_clr_status { get; set; }
        public string sims_fee_clr_by { get; set; }
        public string sims_fee_clr_date { get; set; }
        public string sims_fee_clr_remark { get; set; }
        public bool sims_finn_clr_status { get; set; }
        public string sims_finn_clr_by { get; set; }
        public string sims_finn_clr_date { get; set; }
        public string sims_finn_clr_remark { get; set; }
        public bool sims_inv_clr_status { get; set; }
        public string sims_inv_clr_by { get; set; }
        public string sims_inv_clr_date { get; set; }
        public string sims_inv_clr_remark { get; set; }
        public bool sims_inci_clr_status { get; set; }
        public string sims_inci_clr_by { get; set; }
        public string sims_inci_clr_date { get; set; }
        public string sims_inci_clr_remark { get; set; }
        public bool sims_lib_clr_status { get; set; }
        public string sims_lib_clr_by { get; set; }
        public string sims_lib_clr_date { get; set; }
        public string sims_lib_clr_remark { get; set; }
        public bool sims_trans_clr_status { get; set; }
        public string sims_trans_clr_by { get; set; }
        public string sims_trans_clr_date { get; set; }
        public string sims_trans_clr_remark { get; set; }
        public bool sims_acad_clr_status { get; set; }
        public string sims_acad_clr_by { get; set; }
        public string sims_acad_clr_date { get; set; }
        public string sims_acad_clr_remark { get; set; }
        public bool sims_admin_clr_status { get; set; }
        public string sims_admin_clr_by { get; set; }
        public string sims_admin_clr_date { get; set; }
        public string sims_admin_clr_remark { get; set; }
        public bool sims_other1_clr_status { get; set; }
        public string sims_other1_clr_by { get; set; }
        public string sims_other1_clr_date { get; set; }
        public string sims_other1_clr_remark { get; set; }
        public bool sims_other2_clr_status { get; set; }
        public string sims_other2_clr_by { get; set; }
        public string sims_other2_clr_date { get; set; }
        public string sims_other2_clr_remark { get; set; }
        public bool sims_other3_clr_status { get; set; }
        public string sims_other3_clr_by { get; set; }
        public string sims_other3_clr_date { get; set; }
        public string sims_other3_clr_remark { get; set; }
        public bool sims_clr_status { get; set; }
        public bool sims_clr_super_user_flag { get; set; }

        public string dept { get; set; }
        public string status { get; set; }
        public string date { get; set; }
        public string clear_by { get; set; }
        public string remark { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_fee_period1 { get; set; }
        public string sims_fee_period1_paid { get; set; }
        public string sims_fee_period1_concession_paid { get; set; }

        public string sims_fee_period2 { get; set; }
        public string sims_fee_period2_paid { get; set; }
        public string sims_fee_period2_concession_paid { get; set; }

        public string sims_fee_period3 { get; set; }
        public string sims_fee_period3_paid { get; set; }
        public string sims_fee_period3_concession_paid { get; set; }

        public string sims_fee_period4 { get; set; }
        public string sims_fee_period4_paid { get; set; }
        public string sims_fee_period4_concession_paid { get; set; }

        public string sims_fee_period5 { get; set; }
        public string sims_fee_period5_paid { get; set; }
        public string sims_fee_period5_concession_paid { get; set; }

        public string sims_fee_period6 { get; set; }
        public string sims_fee_period6_paid { get; set; }
        public string sims_fee_period6_concession_paid { get; set; }

        public string sims_fee_period7 { get; set; }
        public string sims_fee_period7_paid { get; set; }
        public string sims_fee_period7_concession_paid { get; set; }

        public string sims_fee_period8 { get; set; }
        public string sims_fee_period8_paid { get; set; }
        public string sims_fee_period8_concession_paid { get; set; }

        public string sims_fee_period9 { get; set; }
        public string sims_fee_period9_paid { get; set; }
        public string sims_fee_period9_concession_paid { get; set; }

        public string sims_fee_period10 { get; set; }
        public string sims_fee_period10_paid { get; set; }
        public string sims_fee_period10_concession_paid { get; set; }

        public string sims_fee_period11 { get; set; }
        public string sims_fee_period11_paid { get; set; }
        public string sims_fee_period11_concession_paid { get; set; }

        public string sims_fee_period12 { get; set; }
        public string sims_fee_period12_paid { get; set; }
        public string sims_fee_period12_concession_paid { get; set; }

        public string sims_fee_code_description { get; set; }
    }
    #endregion

    #region Sim684(Certificate TC Approve pearl)
    public class sims684
    {
        public string sims_certificate_number { get; set; }
        public string sims_certificate_enroll_number { get; set; }
        public string sims_certificate_date_of_leaving { get; set; }
        public string sims_certificate_reason_of_leaving { get; set; }
        public string sims_certificate_general_conduct { get; set; }
        public string sims_certificate_academic_progress { get; set; }
        public string sims_certificate_subject_studied { get; set; }
        public string sims_certificate_remark { get; set; }
        public string sims_certificate_attendance_remark { get; set; }
        public string sims_certificate_qualified_for_promotion { get; set; }
        public string sims_certificate_qualified_for_promotion_name { get; set; }
        public string sims_certificate_date_of_issue { get; set; }
        public string sims_certificate_fee_paid { get; set; }
        public bool sims_certificate_sc_st_status { get; set; }
        public string sims_certificate_field1 { get; set; }
        public string sims_certificate_field2 { get; set; }
        public string sims_certificate_field3 { get; set; }
        public string sims_certificate_field3_name { get; set; }
        public string sims_certificate_field4 { get; set; }
        public string sims_certificate_field5 { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string sims_certificate_name { get; set; }
        public int sims_certificate_registration_register_no { get; set; }
        public int sims_certificate_registration_serial_no { get; set; }
        public int sims_certificate_result_register_no { get; set; }
        public int sims_certificate_result_serial_no { get; set; }
        public string stud_name { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_admission { get; set; }
        public string dobw { get; set; }
        public string present_class { get; set; }
        public string present_sec { get; set; }
        public string admitted_class { get; set; }
        public string result_end_acad_yr { get; set; }
        public string promoted_class { get; set; }
        public string promoted_acad_yr { get; set; }
        public string present_days { get; set; }
        public string total_days { get; set; }
        public string stream { get; set; }
        public string stream_code { get; set; }
        public string Credits { get; set; }
        public string registrar { get; set; }
        public string principle { get; set; }
        public string doc_date { get; set; }
        public string sims_certificate_subject_studied_ar { get; set; }
        public string opr { get; set; }
        public string tC_Date { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_certificate_academic_year { get; set; }
        public string sims_certificate_student_caste { get; set; }
        public string sims_certificate_student_caste_name { get; set; }
        public string sims_certificate_student_fail { get; set; }
        public string sims_certificate_student_fail_name { get; set; }
        public string sims_student_fee_discount_reason { get; set; }
        public string sims_student_exam_result { get; set; }
        public string sims_student_promoted_to { get; set; }
        public string sims_student_fees_paid_lastmonth { get; set; }
        public string sims_certificate_appl_date_tc { get; set; }
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string academic_year_name { get; set; }
        public string curr_name { get; set; }

        public string sims_student_national_id { get; set; }
        public string bus_name { get; set; }
        public string refund_acc1 { get; set; }
        public string refund_acc2 { get; set; }
        public string email { get; set; }
        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public string last_attendance_day { get; set; }
        public string sims_certificate_appove_date { get; set; }
        public string sims_certificate_student_games { get; set; }
        public string sims_certificate_req_status { get; set; }
        public string sims_student_admission_number { get; set; }
        public string yearcurrentstart { get; set; }
        public string yearcurrentyend { get; set; }
        public string sims_certificate_req_date { get; set; }

        public string sims_student_ea_number { get; set; }

        public string sims_student_gender { get; set; }

        public string sims_student_exam_appeared { get; set; }

        public string sims_certificate_doc_path_name { get; set; }

        public string sims_certificate_doc_status { get; set; }

        public string sims_grade_name { get; set; }

        public string sims_grade_code { get; set; }

        public string leave_date { get; set; }

        public string sims_certificate_admission_date { get; set; }

        public string sims_certificate_joined_class { get; set; }

        public string sims_certificate_academic_year_desc { get; set; }

        public string sims_academic_year { get; set; }
    }
    #endregion

    #region Sims505 (Certificate TC Parameter)
    public class sims505
    {
        public string sims_student_ea_number { get; set; }
        public string sims_student_exam_appeared { get; set; }
        public string sims_certificate_sequence_number { get; set; }
        public string total_fees_paid { get; set; }
        public string sims_promoted_class { get; set; }
        public string sims_student_gender { get; set; }
        public string sims_certificate_number { get; set; }
        public string sims_certificate_enroll_number { get; set; }
        public string sims_certificate_date_of_leaving { get; set; }
        public string sims_certificate_reason_of_leaving { get; set; }
        public string sims_certificate_general_conduct { get; set; }
        public string sims_certificate_academic_progress { get; set; }
        public string sims_certificate_subject_studied { get; set; }
        public string sims_certificate_remark { get; set; }
        public string sims_certificate_attendance_remark { get; set; }
        public bool sims_certificate_qualified_for_promotion { get; set; }
        public string sims_certificate_date_of_issue { get; set; }
        public bool sims_certificate_fee_paid { get; set; }
        public bool sims_certificate_sc_st_status { get; set; }
        public string sims_certificate_field1 { get; set; }
        public string sims_certificate_field2 { get; set; }
        public string sims_certificate_field3 { get; set; }
        public string sims_certificate_field4 { get; set; }
        public string sims_certificate_field5 { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string sims_certificate_name { get; set; }
        public int sims_certificate_registration_register_no { get; set; }
        public int sims_certificate_registration_serial_no { get; set; }
        public int sims_certificate_result_register_no { get; set; }
        public int sims_certificate_result_serial_no { get; set; }
        public string stud_name { get; set; }
        public string father_name { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_admission { get; set; }
        public string dobw { get; set; }
        public string present_class { get; set; }
        public string present_sec { get; set; }
        public string admitted_class { get; set; }
        public string result_end_acad_yr { get; set; }
        public string promoted_class { get; set; }
        public string promoted_acad_yr { get; set; }
        public string present_days { get; set; }
        public string total_days { get; set; }
        public string stream { get; set; }
        public string stream_code { get; set; }
        public string Credits { get; set; }
        public string registrar { get; set; }
        public string principle { get; set; }

        public string curr_code { get; set; }
        public string curr_name { get; set; }
        public string academic_year { get; set; }
        public string academic_year_name { get; set; }
        public string sims_certificate_subject_studied_ar { get; set; }
        public string opr { get; set; }

        public string sims_working_days { get; set; }
        public string sims_manually_attendance { get; set; }
        public string sims_birth_place { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_certificate_student_fail_name { get; set; }
        public string sims_certificate_student_fail { get; set; }
        public string mother_name { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_certificate_academic_year { get; set; }
        public string sims_student_fees_paid_lastmonth { get; set; }
        public string sims_student_national_id { get; set; }
        public string bus_name { get; set; }
        public string sims_student_fee_discount_reason { get; set; }
        public string sims_student_exam_result { get; set; }
        public string sims_student_promoted_to { get; set; }
        public string sims_certificate_qualified_for_promotion_name { get; set; }
        public string sims_certificate_req_date { get; set; }
        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public string email { get; set; }
        public string refund_acc1 { get; set; }
        public string refund_acc2 { get; set; }
        public string sims_certificate_req_status { get; set; }
        public string sims_certificate_student_games { get; set; }
        public string sims_student_admission_number { get; set; }
        public string last_attendance_day { get; set; }
        public string tC_Date { get; set; }
        public string sims_certificate_field3_name { get; set; }

        public string exam_year { get; set; }

        public string sims_certificate_exam_year { get; set; }
        public string sims_certificate_student_caste_name { get; set; }
        public string sims_certificate_student_caste { get; set; }


        public string sims_certificate_joined_class { get; set; }

        //public string sims_certificate_sequence_number { get; set; }

        //public string total_fees_paid { get; set; }

        //public string sims_promoted_class { get; set; }
    }
    #endregion

    #region sims681 (Certificate TC Parameter_brs)
    public class sims681
    {
        public string sims_certificate_number { get; set; }
        public string sims_certificate_enroll_number { get; set; }
        public string sims_certificate_date_of_leaving { get; set; }
        public string sims_certificate_reason_of_leaving { get; set; }
        public string sims_certificate_general_conduct { get; set; }
        public string sims_certificate_academic_progress { get; set; }
        public string sims_certificate_subject_studied { get; set; }
        public string sims_certificate_remark { get; set; }
        public string sims_certificate_attendance_remark { get; set; }
        public string sims_certificate_qualified_for_promotion { get; set; }
        public string sims_certificate_qualified_for_promotion_name { get; set; }
        public string sims_certificate_date_of_issue { get; set; }
        public string sims_certificate_fee_paid { get; set; }
        public bool sims_certificate_sc_st_status { get; set; }
        public string sims_certificate_field1 { get; set; }
        public string sims_certificate_field2 { get; set; }
        public string sims_certificate_field3 { get; set; }
        public string sims_certificate_field4 { get; set; }
        public string sims_certificate_field5 { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string sims_certificate_name { get; set; }
        public int sims_certificate_registration_register_no { get; set; }
        public int sims_certificate_registration_serial_no { get; set; }
        public int sims_certificate_result_register_no { get; set; }
        public int sims_certificate_result_serial_no { get; set; }
        public string stud_name { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_admission { get; set; }
        public string dobw { get; set; }
        public string present_class { get; set; }
        public string present_sec { get; set; }
        public string admitted_class { get; set; }
        public string result_end_acad_yr { get; set; }
        public string promoted_class { get; set; }
        public string promoted_acad_yr { get; set; }
        public string present_days { get; set; }
        public string total_days { get; set; }
        public string stream { get; set; }
        public string stream_code { get; set; }
        public string Credits { get; set; }
        public string registrar { get; set; }
        public string principle { get; set; }
        public string doc_date { get; set; }
        public string sims_certificate_subject_studied_ar { get; set; }
        public string opr { get; set; }
        public string tC_Date { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_certificate_academic_year { get; set; }
        public string sims_certificate_student_caste { get; set; }
        public string sims_certificate_student_caste_name { get; set; }
        public string sims_certificate_student_fail { get; set; }
        public string sims_certificate_student_fail_name { get; set; }
        public string sims_student_fee_discount_reason { get; set; }
        public string sims_student_exam_result { get; set; }
        public string sims_student_promoted_to { get; set; }
        public string sims_student_fees_paid_lastmonth { get; set; }
        public string sims_certificate_appl_date_tc { get; set; }
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string academic_year_name { get; set; }
        public string curr_name { get; set; }
    }
    #endregion

    #region Finn141
    public class Finn141
    {
        public string sllc_ldgr_code;
        public string sllc_ldgr_name;
        public string gltd_comp_code { get; set; }
        public string gltd_doc_code { get; set; }
        public string gltd_prov_doc_no { get; set; }
        public string gltd_final_doc_no { get; set; }
        public string gltd_doc_date { get; set; }
        public string gltd_doc_narr { get; set; }
        public string gltd_remarks { get; set; }
        public string gltd_doc_flag { get; set; }
        public string gltd_cur_status { get; set; }
        public string gltd_prepare_user { get; set; }
        public string gltd_prepare_date { get; set; }
        public string gltd_verify_user { get; set; }
        public string gltd_verify_date { get; set; }
        public string gltd_authorize_user { get; set; }
        public string gltd_authorize_date { get; set; }
        public string gltd_payment_user { get; set; }
        public string gltd_payment_date { get; set; }
        public string gltd_post_date { get; set; }
        public int gltd_post_no { get; set; }
        public int gltd_save_srl { get; set; }
        public string gltd_batch_source { get; set; }
        public string gltd_paid_to { get; set; }
        public string gltd_cheque_no { get; set; }
        public string master_acno { get; set; }
        public string master_ac_cdname { get; set; }
        public string master_acno_name { get; set; }


        ////// fins_temp_doc_details
        public string gldd_comp_code { get; set; }
        public string gldd_doc_code { get; set; }
        public string gldd_prov_doc_no { get; set; }
        public string gldd_final_doc_no { get; set; }
        public decimal gldd_line_no { get; set; }
        public string gldd_doc_narr { get; set; }
        public string gldd_ledger_code { get; set; }
        public string gldd_ledger_name { get; set; }
        public string gldd_acct_code { get; set; }
        public string gldd_acct_name { get; set; }
        public decimal gldd_doc_amount { get; set; }
        public decimal gldd_doc_amount_credit { get; set; }
        public decimal gldd_doc_amount_debit { get; set; }
        public string gldd_fc_code { get; set; }
        public string gldd_fc_name { get; set; }
        public decimal gldd_fc_rate { get; set; }
        public decimal gldd_fc_amount_debit { get; set; }
        public decimal gldd_fc_amount_credit { get; set; }
        public string gldd_dept_code { get; set; }
        public string gldd_dept_name { get; set; }
        public string gldd_party_ref_no { get; set; }
        public string gldd_party_ref_date { get; set; }
        public string gldd_bank_date { get; set; }
        public string gldd_cheque_no { get; set; }
        public string gldd_cheque_due_date { get; set; }
        public string excg_curcy_desc { get; set; }
        public decimal glbt_pstng_no { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string gldc_doc_code { get; set; }
        public string gldc_doc_type { get; set; }

        public string sltr_pty_bank_id { get; set; }

        public string coad_tel_no { get; set; }

        public string coad_po_box { get; set; }

        public string coad_line_1 { get; set; }

        public string coad_line_2 { get; set; }

        public decimal sltr_yob_amt { get; set; }

        public string glma_acct_code { get; set; }

        public string pc_bank_code { get; set; }
    }
    #endregion

    public class Sim613
    {
        public string max_num { get; set; }
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_honour_academic_year { get; set; }
        public string sims_honour_code { get; set; }
        public string sims_honour_criteria_type { get; set; }
        public string sims_honour_cur_code { get; set; }
        public string sims_honour_cur_name { get; set; }
        public string sims_honour_desc { get; set; }
        public string sims_honour_min_criteria { get; set; }
        public bool sims_honour_status { get; set; }
        public string sims_honour_type { get; set; }
        public string s_cur_code { get; set; }
        public string year_desc { get; set; }
    }

    public class SimFTD
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_section_code { get; set; }
        public string sims_grade_code { get; set; }
        public string temp_dd_doc_date { get; set; }
        public string temp_dd_enroll_number { get; set; }
        public string temp_dd_fee_transaction_id { get; set; }
        public string temp_dd_fee_code { get; set; }
        public string temp_dd_fee_period_code { get; set; }
        public string temp_dd_fee_amount { get; set; }
        public string sims_fee_code_description { get; set; }

    }


    public class sims_fee_name
    {
        public string temp_dd_doc_no { get; set; }

        public string temp_dd_fee_amount { get; set; }

        public string temp_dd_fee_name { get; set; }
    }
    public class sims_temp_fee_document_ppn
    {
        public List<sims_fee_name> sims_fee_name { get; set; }
        public string temp_dd_doc_no { get; set; }
        public string temp_dd_doc_date { get; set; }
        public string temp_dd_enroll_number { get; set; }
        public string temp_dd_line_no { get; set; }
        public string temp_dd_fee_number { get; set; }
        public string temp_dd_fee_code { get; set; }
        public string temp_dd_fee_period_code { get; set; }
        public string temp_dd_fee_amount { get; set; }
        public string temp_dd_fee_amount_discounted { get; set; }
        public string temp_dd_fee_amount_final { get; set; }
        public string temp_dd_fee_payment_mode { get; set; }
        public string temp_dd_fee_transaction_id { get; set; }
        public string temp_dd_fee_academic_year { get; set; }
        public string temp_dd_fee_status { get; set; }
        public string temp_dd_creation_user { get; set; }
        public string temp_dd_creation_date { get; set; }

        public string authorizeid { get; set; }
        public string cardtype { get; set; }
        public string batchno { get; set; }
        public string receiptno { get; set; }
        public string acqresponsecode { get; set; }



        public string temp_dd_fee_card_last_number { get; set; }

        public string Student_Info { get; set; }

        public string total_amt { get; set; }

        public string temp_dd_realize_date { get; set; }
    }

    public class Sim614
    {

        public string codp_dept_name { get; set; }
        public string codp_dept_no { get; set; }
        public string comp_name { get; set; }
        public string dg_code { get; set; }
        public string dg_desc { get; set; }
        public string emp_Name { get; set; }
        public string em_img { get; set; }
        public string fullname { get; set; }
        public string gradesection { get; set; }
        public string grade_name { get; set; }
        public string image { get; set; }
        public string section_name { get; set; }
        public string sims_honor_emp_code { get; set; }
        public string sims_honor_enroll_number { get; set; }
        public string sims_honor_grade_code { get; set; }
        public string sims_honor_section_code { get; set; }
        public string sims_honor_user_number { get; set; }
        public string sims_honour_academic_year { get; set; }
        public string sims_honour_code { get; set; }
        public string sims_honour_cur_code { get; set; }
        public string sims_honour_cur_name { get; set; }
        public string sims_honour_user_status { get; set; }
        public string sims_honour_value { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_student_image { get; set; }
        public string sims_student_passport_first_name_en { get; set; }
        public string sims_student_passport_full_name_en { get; set; }
        public string year_desc { get; set; }
    }

    #region Sims613 (Re-Admission)
    public class Sims613
    {

        public string username { get; set; }
        public string sims_enroll_no { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string stud_full_name { get; set; }

        public string sims_academic_year_new { get; set; }
        public string sims_cur_code_new { get; set; }
        public string sims_grade_code_new { get; set; }
        public string sims_section_code_new { get; set; }
        public string opr { get; set; }

        public string sims_attendance_date { get; set; }
        public string sims_fee_from_date { get; set; }
        public string sims_readmission_remark { get; set; }

    }
    #endregion

    #region Sims505 (Certificate TC Parameter)
    public class tccert
    {
        public string sims_certificate_number { get; set; }
        public string sims_certificate_enroll_number { get; set; }
        public string sims_certificate_date_of_leaving { get; set; }
        public string sims_certificate_reason_of_leaving { get; set; }
        public string sims_certificate_general_conduct { get; set; }
        public string sims_certificate_academic_progress { get; set; }
        public string sims_certificate_subject_studied { get; set; }
        public string sims_certificate_remark { get; set; }
        public string sims_certificate_attendance_remark { get; set; }
        public bool sims_certificate_qualified_for_promotion { get; set; }
        public string sims_certificate_qualified_for_promotion_name { get; set; }
        public string sims_certificate_date_of_issue { get; set; }
        public bool sims_certificate_fee_paid { get; set; }
        public bool sims_certificate_sc_st_status { get; set; }
        public string sims_certificate_field1 { get; set; }
        public string sims_certificate_field2 { get; set; }
        public string sims_certificate_field3 { get; set; }
        public string sims_certificate_field4 { get; set; }
        public string sims_certificate_field5 { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string sims_certificate_name { get; set; }
        public int sims_certificate_registration_register_no { get; set; }
        public int sims_certificate_registration_serial_no { get; set; }
        public int sims_certificate_result_register_no { get; set; }
        public int sims_certificate_result_serial_no { get; set; }
        public string stud_name { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string religion { get; set; }
        public string nationality { get; set; }
        public string date_of_birth { get; set; }
        public string date_of_admission { get; set; }
        public string dobw { get; set; }
        public string present_class { get; set; }
        public string present_sec { get; set; }
        public string admitted_class { get; set; }
        public string result_end_acad_yr { get; set; }
        public string promoted_class { get; set; }
        public string promoted_acad_yr { get; set; }
        public string present_days { get; set; }
        public string total_days { get; set; }
        public string stream { get; set; }
        public string stream_code { get; set; }
        public string Credits { get; set; }
        public string registrar { get; set; }
        public string principle { get; set; }
        public string doc_date { get; set; }
        public string sims_certificate_subject_studied_ar { get; set; }
        public string opr { get; set; }
        public string tC_Date { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_certificate_academic_year { get; set; }
        public string sims_certificate_student_caste { get; set; }
        public string sims_certificate_student_caste_name { get; set; }
        public string sims_certificate_student_fail { get; set; }
        public string sims_certificate_student_fail_name { get; set; }
        public string sims_student_fee_discount_reason { get; set; }
        public string sims_student_exam_result { get; set; }
        public string sims_student_promoted_to { get; set; }
        public string sims_student_fees_paid_lastmonth { get; set; }
        public string sims_certificate_appl_date_tc { get; set; }
        public string curr_code { get; set; }
        public string academic_year { get; set; }
        public string academic_year_name { get; set; }
        public string curr_name { get; set; }

        public string sims_student_national_id { get; set; }
        public string bus_name { get; set; }
        public string refund_acc1 { get; set; }
        public string refund_acc2 { get; set; }
        public string email { get; set; }
        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public string last_attendance_day { get; set; }
        public string sims_certificate_appove_date { get; set; }
        public string sims_certificate_student_games { get; set; }
        public string sims_certificate_req_status { get; set; }
        public string sims_student_admission_number { get; set; }
        public string yearcurrentstart { get; set; }
        public string yearcurrentyend { get; set; }
        public string sims_certificate_req_date { get; set; }

        public string sims_student_gender { get; set; }

        public string sims_student_exam_appeared { get; set; }

        public string sims_student_ea_number { get; set; }
    }
    #endregion


    #region Sims685 (Add Exam Paper)
    public class Sims685
    {

        public string sims_academic_year { get; set; }
        public string s_cur_code { get; set; }
        public string sims_cur_short_name_en { get; set; }
        public string search_std_grade_name { get; set; }
        public string sims_grade_name { get; set; }
        public string search_std_section_name { get; set; }
        public string sims_section_name { get; set; }
        public string opr { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_code_name { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_exam_code { get; set; }
        public string sims_exam_code_name { get; set; }
        public string sims_exam_desc { get; set; }
        public string sims_doc_desc { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_code_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_code_name { get; set; }
        public bool sims_exam_paper_status { get; set; }
        public string sims_exam_paper_path { get; set; }
        public string sims_exam_paper_display_name { get; set; }
        public string trans_type { get; set; }
        public string sims_exam_paper_srl_no { get; set; }
        public string sims_exam_paper_created_by { get; set; }
        public string sims_exam_paper_title { get; set; }
        public string sims_exam_attach_refno { get; set; }
    }

    #endregion

    public class ques_answerN
    {
        public string sims_questionbank_question_answer_desc_en { get; set; }
        public string sims_questionbank_question_answer_code { get; set; }
        public string sims_questionbank_question_type { get; set; }
        public string sims_questionbank_question_answer_image { get; set; }
        public bool sims_questionbank_question_correct_answer_flag { get; set; }
        public bool sims_question_answer_flag_value { get; set; }


    }

    public class QuestionBank
    {
        public int sims_questionbank_srl_no { get; set; } 
        public string sims_questionbank_code { get; set; }
        public string sims_questionbank_type { get; set; }
        public string sims_questionbank_subject { get; set; }
        public string sims_questionbank_subject_ot { get; set; }
        public string sims_questionbank_desc_en { get; set; }
        public string sims_questionbank_desc_ot { get; set; }
        public string sims_questionbank_start_date { get; set; }
        public string sims_questionbank_end_date { get; set; }
        public Boolean sims_questionbank_annonimity_flag { get; set; }
        public Boolean sims_questionbank_ppn_flag { get; set; }
        public Boolean sims_questionbank_erp_flag { get; set; }
        public Boolean sims_questionbank_multiple_attempt_flag { get; set; }
        public Boolean sims_questionbank_end_of_questionbank_remark_flag { get; set; }
        public Boolean sims_questionbank_end_of_questionbank_suggestion_flag { get; set; }
        public Boolean sims_questionbank_end_of_questionbank_rating_flag { get; set; }
        public int sims_questionbank_max_attempt_count { get; set; }
        public int sims_questionbank_max_allowed_time_in_minute { get; set; }
        public int sims_questionbank_no_of_question_to_attempt { get; set; }
        public int sims_questionbank_no_of_question_available { get; set; }
        public string sims_questionbank_created_by_user { get; set; }
        public string sims_questionbank_creation_time { get; set; }
        public string sims_questionbank_status { get;set;}
        public string sims_questionbank_gender_code { get; set; }
        public string sims_questionbank_type_name { get; set; }


        public string sims_questionbank_status_desc { get; set; }
        public string sims_questionbank_dept_no { get; set; }
        public string sims_questionbank_user_group_code { get; set; }
        public string sims_questionbank_grade_code { get; set; }
        public string sims_questionbank_section_code { get; set; }
        public string sims_questionbank_acad_year { get; set; }
       // public string sims_questionbank_gender_code { get; set; }
        public string sims_questionbank_gradesection_code { get; set; }
        public string opr { get; set; }
        public string sims_questionbank_age_group_code { get; set; }
        /// <summary>
        /// from here
        /// </summary>
        public string codp_dept_name { get; set; }
        public string comn_user_group_name { get; set; }
        public string sims_questionbank_question_code { get; set; }
        public string sims_questionbank_question_desc_en { get; set; }
        public string sims_questionbank_question_desc_ot { get; set; }
        public string sims_questionbank_question_type { get; set; }
        public string sims_questionbank_question_weightage { get; set; }
        public string sims_questionbank_question_display_order { get; set; }
        public string sims_questionbank_question_difficulty_level { get; set; }
        public string sims_questionbank_question_rating_group_code { get; set; }
        public Boolean sims_questionbank_question_skip_flag { get; set; }
        public Boolean sims_questionbank_question_status { get; set; }
        public string sims_questionbank_question_answer_desc_en { get; set; }
        public string sims_questionbank_question_answer_desc_ot { get; set; }
        public string sims_questionbank_question_answer_weightage { get; set; }
        public string sims_questionbank_question_answer_display_order { get; set; }
        public string sims_questionbank_question_answer_status { get; set; }
        public Boolean sims_questionbank_question_correct_answer_flag { get; set; }
        public string sims_questionbank_question_difficulty_level_name { get; set; }
        
        public string sims_questionbank_skill_code { get; set; }
        public string sims_questionbank_skill_name { get; set; }
        //public string sims_questionbank_section_code { get; set; }
        public string sims_questionbank_section_name { get; set; }
        public string sims_questionbank_subsection_code { get; set; }
        public string sims_questionbank_subsection_name { get; set; }
        public List<ques_answerN> que_ans { get; set; }
        public string sims_question_question_type { get; set; }
        public string sims_question_question_type_name { get; set; }
        public string sims_questionbank_question_answer_code { get; set; }
        public string sims_questionbank_question_image_path { get; set; }
        public string sims_questionbank_question_answer_explanation { get; set; }
        public string sims_questionbank_question_header_code { get; set; }
        public string sims_questionbank_question_header_code_description { get; set; }
        public string  sims_questionbank_question_section { get; set; }
        public string  sims_questionbank_question_subsection { get; set; }
        public string sims_questionbank_question_skill { get; set; }
        public int sims_questionbank_question_display_order_sort { get; set; }

        //till here

        public string sims_survey_user_group_code { get; set; }
        public string sims_survey_dept_no { get; set; }
        public string sims_survey_acad_year{ get; set; }
        public string sims_survey_grade_code { get; set; }
        public string sims_survey_section_code  { get; set; }
        public string sims_survey_gender_code   { get; set; }
        public string sims_survey_age_group_code{ get; set; }
        public string sims_survey_status { get; set; }

        public string total_questionbank_question { get; set; }
        public string total_question_attempted { get; set; }
        public string left_question { get; set; }
        public string total_correct_answer { get; set; }
        public string total_wrong_answer { get; set; }
        public string maximum_marks { get; set; }
        public string correct_marks { get; set; }
        public string wrong_marks { get; set; }
        public string total_marks { get; set; }


    }
    public class Survey
    {

        public string codp_dept_name      {get;set;}
        public string comn_user_group_name{get;set;}
        public string survey_type_code { get; set; }

        public string survey_type_name { get; set; }

        public string user_grp_code { get; set; }

        public string user_grp_name { get; set; }

        public int myid { get; set; }

        public int sims_survey_srl_no { get; set; }

        public string sims_survey_code { get; set; }

        public string sims_survey_subject { get; set; }

        public string sims_survey_subject_ot { get; set; }

        public string sims_survey_desc_en { get; set; }

        public string sims_survey_desc_ot { get; set; }

        public string sims_survey_start_date { get; set; }

        public string sims_survey_end_date { get; set; }

        public bool sims_survey_annonimity_flag { get; set; }

        public bool sims_survey_ppn_flag { get; set; }

        public string sims_survey_type { get; set; }

        public bool sims_survey_erp_flag { get; set; }

        public bool sims_survey_multiple_attempt_flag { get; set; }

        public bool sims_survey_end_of_survey_remark_flag { get; set; }

        public bool sims_survey_end_of_survey_rating_flag { get; set; }

        public bool sims_survey_end_of_survey_suggestion_flag { get; set; }

        public int sims_survey_max_attempt_count { get; set; }

        public int sims_survey_max_allowed_time_in_minute { get; set; }

        public int sims_survey_no_of_question_available { get; set; }

        public int sims_survey_no_of_question_to_attempt { get; set; }

        public string sims_survey_created_by_user { get; set; }

        public string sims_survey_creation_time { get; set; }

        public string sims_survey_status { get; set; }

        public string sims_survey_dept_no { get; set; }

        public string sims_survey_user_group_code { get; set; }

        public string sims_survey_grade_code { get; set; }

        public string sims_survey_section_code { get; set; }

        public string sims_survey_gender_code { get; set; }

        public string sims_survey_gradesection_code { get; set; }

        public string sims_survey_acad_year { get; set; }

        public string opr { get; set; }

        public string sims_survey_age_group_code { get; set; }

        public string sims_survey_status_code { get; set; }

        public string sims_survey_status_desc { get; set; }
    }

    public class Sims711
    {

        public string sims_survey_user_response_code { get; set; }
        public string sims_survey_no_of_question_available { get; set; }
        public string sims_survey_type { get; set; }
        public string sims_survey_no_of_question_to_attempt { get; set; }
        public string sims_survey_question_difficulty_level_name { get; set; }
        public string sims_survey_question_code { get; set; }
            public string sims_survey_question_desc_en { get; set; }
            public string sims_survey_question_desc_ot { get; set; }
            public string sims_survey_question_type { get; set; }
            public string sims_survey_question_type_name { get; set; }
            public string sims_survey_question_weightage { get; set; }
            public string sims_survey_question_display_order { get; set; }
            public string sims_survey_question_difficulty_level { get; set; }
            public string sims_survey_question_rating_group_code { get; set; }
            public bool sims_survey_question_status { get; set; }
            public string sims_survey_question_answer_code { get; set; }
            public string sims_survey_question_answer_desc_en { get; set; }
            public string sims_survey_question_answer_desc_ot { get; set; }
            public string sims_survey_question_answer_weightage { get; set; }
            public string sims_survey_question_answer_display_order { get; set; }
            public string sims_survey_question_answer_status { get; set; }
        public bool sims_survey_question_correct_answer_flag { get; set; }
        public string sims_survey_rating_group_code { get; set; }
        public string sims_survey_rating_group_desc_en { get; set; }
        public string sims_survey_rating_code { get; set; }
        public string sims_survey_rating_name { get; set; }
        public string sims_survey_rating_desc { get; set; }

        public bool question_status { get; set; }
        public bool sims_survey_question_skip_flag { get; set; }
         public string sims_survey_code { get; set; }

        public List<ques_answer> que_ans = new List<ques_answer>();

        public string sims_survey_question_desc_Arabic { get; set; }
    }

  

        public class ques_answer
    {
        public string sims_survey_question_answer_code { get; set; }
        public string sims_survey_question_answer_desc_en { get; set; }
        public string sims_survey_question_type { get; set; }

        public string sims_survey_rating_code { get; set; }

        public string sims_survey_rating_img_path { get; set; }

        public bool sims_survey_question_correct_answer_flag { get; set; }
    }

    public class Sims_blak_list
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string comn_user_name { get; set; }
        public bool comn_user_status { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string StudentName { get; set; }
        public string Class { get; set; }
        public string sims_sibling_parent_number { get; set; }
        public string Balance { get; set; }
    }

    #region gradeBookNewConfig
    public class gradeBookNewConfig
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_config_desc { get; set; }
        public string sims_report_card_config_status { get; set; }
    }

    #endregion

    #region subject_config
    public class subject_config
    {
        public List<subjectDet> SubjectDet { get; set; }
        public List<termDet> TermDet { get; set; }
        public List<catDet> CatDet { get; set; }
        public List<assignDet> AssignDet { get; set; }


        public string narr_grade_group_code { get; set; }

        public string sims_report_card_assign_number { get; set; }

        public string sims_report_card_category_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_report_card_assign_max_score1 { get; set; }
        public string assignmentstatus { get; set; }
        public string sims_subject_code1 { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_report_card_subject_grade_scale_code { get; set; }
    }
    #endregion

    
    public class subjectDet
    {
        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_subject_color { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_subject_code { get; set; }
    }

    public class termDet
    {
        public termDet()
        {
            CatDetChild = new List<catDet>();
        }
        public List<catDet> CatDetChild { get; set; }
        public string sims_report_card_term_code { get; set; }

        public string sims_report_card_term_desc { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string term_cnt { get; set; }
        public string cat_cnt { get; set; }
        public string assign_cnt { get; set; }
    }

    public class catDet
    {
        public catDet()
        {
            assignDetchild = new List<assignDet>();
        }
        public List<assignDet> assignDetchild { get; set; }
        public string sims_report_card_category_code { get; set; }

        public string sims_report_card_category_name { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string cat_cnt { get; set; }

        public string sims_report_card_category_color_code { get; set; }
        public string assign_cnt { get; set; }
    }

    public class assignDet
    {
        public List<actualDet> ActualDet { get; set; }
        public string sims_report_card_assign_name { get; set; }

        public string sims_report_card_assign_number { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_report_card_category_code { get; set; }

        public string assign_cnt { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_config_assign_max_score { get; set; }

        public bool Assignmentstatus { get; set; }

        public string narr_grade_group_code { get; set; }

        public string sims_report_card_subject_grade_scale_code { get; set; }

        public string sims_report_card_assign_max_score1 { get; set; }
    }

    public class actualDet
    {
        public string sims_cur_code { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_report_card_assign_name { get; set; }

        public string sims_report_card_assign_number { get; set; }

        public string sims_report_card_category_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_config_assign_max_score { get; set; }

        public string narr_grade_group_code { get; set; }

        //public bool Assignmentstatus { get; set; }

        public string sims_report_card_assign_max_score1 { get; set; }

        public bool assignmentstatus { get; set; }

        public bool checkeddelstatus { get; set; }
        public string sims_subject_name_en { get; set; }

        public string narr_grade_name { get; set; }
        public string sims_report_card_assign_grade_scale_code { get; set; }
        public string sims_report_card_assign_type { get; set; }
        public string narr_grade_group_code_old { get; set; }
        public string subject_flag { get; set; }
        public string all_saved_config { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_report_card_config_desc { get; set; }
        public List<sub_assign> sub_assign { get; set; }
    }

    public class sub_assign
    {
     public string sims_cur_code { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_report_card_assign_name { get; set; }

        public string sims_report_card_assign_number { get; set; }

        public string sims_report_card_category_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_config_assign_max_score { get; set; }

        public string narr_grade_group_code { get; set; }

        //public bool Assignmentstatus { get; set; }

        public string sims_report_card_assign_max_score1 { get; set; }

        public bool assignmentstatus { get; set; }

        public bool checkeddelstatus { get; set; }

        public string narr_grade_name { get; set; }
        public string sims_report_card_assign_grade_scale_code { get; set; }
        public string sims_report_card_assign_type { get; set; }
        public string narr_grade_group_code_old { get; set; }
        public string subject_flag { get; set; }
        public string all_saved_config { get; set; }

        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_report_card_term_desc { get; set; }
        public string sims_subject_co_scholastic_srl_no { get; set; }
        public bool assignmentDisablestatus { get; set; }
        public string sims_report_card_config_desc { get; set; }
       
    }


    #region gradeBookNewTerm
    public class gradeBookNewTerm
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_term_code { get; set; }
        public string sims_report_card_term_desc { get; set; }
        public string sims_report_card_term_desc_ot { get; set; }
        public string sims_report_card_term_st_date { get; set; }
        public string sims_report_card_term_end_date { get; set; }
        public string sims_report_card_term_status { get; set; }
        public string sims_report_card_term_grade_scale { get; set; }

        public bool inserted_flg { get; set; }

        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_subject_attribute_group_code { get; set; }
        public string sims_subject_attribute_code { get; set; }
        public string sims_cat_assign_subject_attribute_max_score { get; set; }
        public string sims_gb_cat_assign_subject_attribute_max_score_correct { get; set; }
        public string sims_gb_cat_assign_subject_attribute_grade_completed_status { get; set; }
        public string sims_term_display_order { get; set; }
        public string sims_report_card_master_term_code { get; set; }
        public string sims_report_card_is_co_scolastic { get; set; }
        public string sims_subject_attribute_code_status { get; set; }
        public string sims_attribute_code { get; set; }
        public string save_Attribut_For_Allclass { get; set; }

    }
    #endregion

    #region gradeBookNewConfigCategory
    public class gradeBookNewConfigCategory
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_category_code { get; set; }
        public string sims_report_card_term_code { get; set; }
        public string sims_report_card_term_code_old { get; set; }
        public string sims_report_card_category_st_date { get; set; }
        public string sims_report_card_category_end_date { get; set; }
        public string sims_report_card_category_publish_date { get; set; }
        public string sims_report_card_category_freeze_status { get; set; }
        public string sims_report_card_category_config_status { get; set; }
      //  public string sims_report_card_term_code_old { get; set; }
    }

    #endregion

    #region gradeBookNewGradeSec
    public class gradeBookNewGradeSec
    {
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string aca_year { get; set; }
        public string cur_code { get; set; }
        public int config_code { get; set; }
        public List<SectionObj> section { get; set; }

        public string section_status { get; set; }

        public string sims_report_card_config_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_cur_code { get; set; }
        public string status { get; set; }
        public string sims_report_card_frequncy { get; set; }
        public string sims_report_card_frequncy_flag { get; set; }
    }

    #endregion

    #region gradeBookNewConfigAssignment
    public class gradeBookNewConfigAssignment
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_category_code { get; set; }
        public string sims_report_card_term_code { get; set; }
        public string sims_report_card_category_code_old { get; set; }
        public string sims_report_card_term_code_old { get; set; }
        public string sims_report_card_assign_number { get; set; }
        public string sims_report_card_assign_st_date { get; set; }
        public string sims_report_card_assign_end { get; set; }
        public string sims_report_card_assign_publish_date { get; set; }
        public string sims_report_card_assign_max_score { get; set; }
        public string sims_report_card_assign_freeze_status { get; set; }
        public string sims_report_card_assign_config_status { get; set; }
        public string sims_allow_edit { get; set; }
        public string sims_report_card_assign_weightage_value { get; set; }
        public string sims_report_card_assign_weightage_type { get; set; }
        public string sims_report_card_assign_points_possible { get; set; }
        public string sims_assign_remark_visible { get; set; }
        public string sims_report_card_parent_assignment_code { get; set; }
        public string sims_assign_total_percentage_visible { get; set; }
        public string sims_assign_point_possible_visible { get; set; }
        public string sims_report_card_assign_score_type { get; set; }
        public string sims_report_card_assign_include_in_final_grade { get; set; }
    }
    #endregion

    #region gradeBookNewCategory
    public class gradeBookNewCategory
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_category_code { get; set; }
        public string sims_report_card_category_name { get; set; }
        public string sims_report_card_category_name_ot { get; set; }
        public string sims_report_card_category_desc { get; set; }
        public string sims_report_card_category_type { get; set; }
        public string sims_report_card_category_weightage_type { get; set; }
        public string sims_appl_form_field { get; set; }
        public string sims_report_card_category_weightage_value { get; set; }
        public string sims_report_card_category_max_of_assign_count { get; set; }
        public string sims_report_card_category_grade_scale_code { get; set; }
        public string sims_report_card_category_score_type { get; set; }
        public string sims_report_card_category_points_possible { get; set; }
        public string sims_report_card_category_color_code { get; set; }
        public string sims_report_card_category_include_in_final_grade { get; set; }
        public string sims_report_card_category_status { get; set; }
        public string sims_report_card_parent_category_Code { get; set; }
        public string sims_report_card_is_co_scolastic { get; set; }
    }

    #endregion

    #region gradeBookNewAssignment
    public class gradeBookNewAssignment
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_report_card_assign_number { get; set; }
        public string sims_report_card_assign_name { get; set; }
        public string sims_report_card_assign_name_ot { get; set; }
        public string sims_report_card_assign_desc { get; set; }
        public string sims_report_card_assign_type { get; set; }
        public string sims_appl_form_field { get; set; }
        public string sims_report_card_assign_include_in_final_grade { get; set; }
        public string sims_report_card_assign_status { get; set; }
        public string sims_report_card_assign_grade_scale_code { get; set; }
        public string assign_type { get; set; }
        public string sims_report_card_assign_weightage_value { get; set; }
        public string sims_report_card_assign_weightage_type { get; set; }
        public string sims_report_card_assign_points_possible { get; set; }
        public string sims_report_card_is_co_scolastic { get; set; }
        public string sims_report_card_assign_score_type { get; set; }
        public string sims_report_card_parent_assignment_code { get; set; }
    }

    #endregion

    #region SubjectData
    public class SubjectData
    {

        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_srl_no { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_assign_final_grade { get; set; }
        public decimal sims_student_assign_received_point { get; set; }
        public string sims_student_assign_exam_status { get; set; }
        public string sims_student_assign_comment { get; set; }
        public string sims_student_assign_entered_by { get; set; }
        public string sims_student_assign_status { get; set; }
        public decimal sims_gb_cat_assign_max_point { get; set; }
        public bool isDirty { get; set; }

        public string status { get; set; }

        public object sims_reject_remark { get; set; }
    }
    #endregion

    #region Get attribute student
    public class SubjectAttributeStd
    {

    public string sims_cur_code { get; set; }
    public string sims_academic_year { get; set; }
    public string sims_grade_code { get; set; }
    public string sims_section_code	 { get; set; }
    public string sims_enroll_number { get; set; }
    public string student_name { get; set; }
    public string sims_allocation_status { get; set; }
    public string sims_subject_srl_no { get; set; }
    public string sims_student_assign_final_grade { get; set; }
    public string sims_student_assign_received_point { get; set; }
    public string sims_student_assign_exam_status { get; set; }
    public string sims_student_assign_comment { get; set; }
    public string sims_student_assign_completed_date { get; set; }
    public string sims_student_assign_entered_by { get; set; }
    public string sims_student_assign_status { get; set; }
    public string sims_gb_cat_assign_max_point { get; set; }
    public string sims_attribute_code { get; set; }
    public string sims_attribute_name { get; set; }
    public string disflag { get; set; }
    public string sims_allow_edit { get; set; }
    public string sims_report_card_term_code { get; set; }
    public List<stud_attr> stud_attr { get; set; }

    }

    public class subAttrList
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_subject_attribute_group_code { get; set; }
        public string sims_subject_attribute_code { get; set; }
        public string sims_subject_attribute_enroll_number { get; set; }
        public string sims_gb_cat_assign_subject_attribute_mark { get; set; }
        public string sims_gb_cat_assign_subject_attribute_final_grade { get; set; }
        public string sims_gb_cat_assign_subject_attribute_max_point { get; set; }
        public string sims_gb_cat_assign_subject_attribute_max_correct { get; set; }
        public string sims_gb_cat_assign_subject_attribute_comment { get; set; }
        public string sims_gb_cat_assign_subject_attribute_status { get; set; }
        public string sims_gb_cat_assign_subject_attribute_completed_date { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string sims_report_card_term_code { get; set; }

        public string status { get; set; }

        public string entered_by { get; set; }

        public string sims_reject_remark { get; set; }
    }

        public class stud_attr
    {

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_allocation_status { get; set; }
        public string sims_subject_srl_no { get; set; }
        public string sims_student_assign_final_grade { get; set; }
        public string sims_student_assign_received_point { get; set; }
        public string sims_student_assign_exam_status { get; set; }
        public string sims_student_assign_comment { get; set; }
        public string sims_student_assign_completed_date { get; set; }
        public string sims_student_assign_entered_by { get; set; }
        public string sims_student_assign_status { get; set; }
        public string sims_gb_cat_assign_max_point { get; set; }
        public string sims_attribute_code { get; set; }
        public string sims_attribute_name { get; set; }
        public string sims_gb_number { get; set; }
        public string sims_gb_cat_code { get; set; }
        public string sims_gb_cat_assign_number { get; set; }
        public string sims_subject_attribute_group_code { get; set; }
        public string sims_report_card_config_code { get; set; }
        public string disflag { get; set; }
        public bool sims_allow_edit { get; set; }
        public string sims_report_card_term_code { get; set; }
    }
    #endregion

    #region CoscholasticData
    public class CoscholasticData
    {

        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_subject_srl_no { get; set; }
        public string sims_student_assign_comment { get; set; }
        public string sims_student_assign_entered_by { get; set; }
        public string sims_student_assign_status { get; set; }
        public string sims_co_scholastic_discriptive_indicator_grade_code { get; set; }
        public decimal sims_report_card_co_scholastic_grade_point { get; set; }
        public bool isDirty { get; set; }
    }
    #endregion

    public class Mogra_Version_Details
        {
            public string comn_appl_name;
            public string lic_changeset_no { get; set; }
            public string lic_school_code { get; set; }
            public string lic_school_name { get; set; }
            public string lic_version_date { get; set; }
            public string lic_version_number { get; set; }
            public string lic_release_number { get; set; }
            public string lic_patch_number { get; set; }
            public bool lic_notes { get; set; }
            public bool lic_version_status { get; set; }
            public bool lic_test_case { get; set; }
            public bool lic_document { get; set; }
            public string lic_remark { get; set; }
            public string lic_sp_name { get; set; }
            public string lic_sp_remark { get; set; }
            public string lic_approve_by { get; set; }
            public string lic_approve_date { get; set; }
            public string lic_deployed { get; set; }
            public string lic_appl_code { get; set; }
            public string comn_appl_name_en { get; set; }
            public string comn_mod_name_e { get; set; }

            public string is_new_flag { get; set; }

            public string comn_mod_code { get; set; }
        }
        public class Mogra_version_applications
        {
            public string comn_appl_code { get; set; }
            public string comn_appl_name { get; set; }

            public string comn_mod_code { get; set; }

            public string comn_mod_name_e { get; set; }
        }

        public class Mogra_Version
        {
            public Mogra_Version()
            {
                mogra_details = new List<Mogra_Version_Details>();
                comn_applications = new List<Mogra_version_applications>();
            }
            public string lic_changeset_no{ get; set; }
            public string lic_project_code { get; set; }
            public string lic_project_name { get; set; }
            public string lic_developed_by { get; set; }
            public string lic_developed_date { get; set; }
            public string lic_approved_by { get; set; }
            public string lic_deployed_by { get; set; }
            public string lic_deployed_date { get; set; }
            public string lic_deployed_status { get; set; }
            public string lic_creation_date { get; set; }
            public string lic_approved_date { get; set; }
            public string lic_version_number { get; set; }
            public string lic_release_number { get; set; }
            public string lic_patch_number { get; set; }
            public List<Mogra_version_applications> comn_applications { get; set; }
            public List<Mogra_Version_Details> mogra_details { get; set; }

            public string comn_mod_code { get; set; }

            public string comn_mod_name_e { get; set; }
        }

        public class surveyObj
        {
            public surveyObj()
            {
                survey_det = new List<survey_Details>();
            }

            public string RatingCode { get; set; }

            public string RatingGroupCode { get; set; }

            public string DescriptionG { get; set; }

            public string DescriptionOtherLanguage { get; set; }

            public bool StatusR { get; set; }

            public string RatingName { get; set; }

            public string DescriptionD { get; set; }

            public string Point { get; set; }
            public string opr { get; set; }
            public bool StatusD { get; set; }

            public List<survey_Details> survey_det { get; set; }
        public string sims_survey_rating_img_path { get;  set; }
        }

        public class survey_Details
        {
            
            public string RatingCode { get; set; }

            public string RatingName { get; set; }

            public string DescriptionD { get; set; }

            public bool StatusD { get; set; }

            public string Point { get; set; }

            public string RatingGroupCode { get; set; }
        public string sims_survey_rating_img_path { get;  set; }
        }

        public class survey_eval
        {

            public string sims_survey_user_response_srl_no { get; set; }

            public string sims_survey_user_response_user_code { get; set; }

            public string sims_survey_code { get; set; }

            public string sims_survey_question_code { get; set; }

            public string sims_survey_question_desc_en { get; set; }

            public string sims_survey_user_response_answer_code { get; set; }
            public string sims_survey_rating_desc { get; set; }
            public string sims_survey_rating_point { get; set; }
            public string sims_survey_rating_img_path { get; set; }
            public string sims_survey_student_enroll_number { get; set; }
            public string sims_survey_user_response_start_time { get; set; }
            public string sims_survey_user_response_end_time { get; set; }
        }
    public class SubjectTeacherListRpt
    {


        public string sims_grade_name_en { get; set; }

    public string sims_section_name_en { get; set; }

    public string sims_subject_name_en { get; set; }

    public string sims_teacher_name { get; set; }

    public string sims_bell_teacher_code { get; set; }

    public int sims_bell_lecture_per_week { get; set; }

   public int Student_cnt { get; set; }

    }
    public class StudentInfoRpt
    {
        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

            }

    public class AR_R20
    {
        public string sims_attendance_rule_code { get; set; }
        public string sims_attendance_rule_name { get; set; }
        public string sims_enrollment_number { get; set; }
        public string student_name { get; set; }
        public string class_name { get; set; }
        public string start_occ_date { get; set; }
        public string end_occ_date { get; set; }
        public int no_of_occ { get; set; }
        public string sims_mom_status { get; set; }
        public string status { get; set; }
        public string status_desc { get; set; }

    }




    //public class Dtcn01
    //{

    //    public string sims_detention_level_code { get; set; }

    //    public string sims_detention_name { get; set; }
    //    public string sims_detention_name_ot { get; set; }
    //    public string sims_detention_point { get; set; }
    //    public string sims_display_order { get; set; }

    //    public bool sims_detention_level_status { get; set; }
    //    public string sims_level_created_user_code { get; set; }
    //    public string opr { get; set; }
    //    public string sims_level_created_date { get; set; }

    //    public string cur_code { get; set; }
    //    public string academic_year { get; set; }
    //    public string enroll { get; set; }
    //}

    #region student Assesment Schedule
    public class studAss
    {

        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string sims_student_name { get; set; }

        public string sims_pros_no { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_acad_yr { get; set; }

        public string sims_appl_date { get; set; }

        public string sims_assesment_date { get; set; }

        public string sims_assesment_teacher_code { get; set; }

        public string sims_assesment_teacher_name { get; set; }

        public string opr { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_prosno_list { get; set; }

        public string sims_assessment_created_user { get; set; }

        public string admission_no { get; set; }

        public string sims_attendance_code { get; set; }
        public string  sims_admission_student_remark { get; set; }

        public string sims_attendance_cout { get; set; }

        public string sims_assesment_time { get; set; }

        public string sims_admission_father_email { get; set; }

        public string sims_admission_father_mobile { get; set; }

        public string sims_enroll_no { get; set; }

        public string sims_student_remark { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string sims_appl_parameter { get; set; }

        public string sims_assesment_std { get; set; }

        public string sims_assesment_interval_min { get; set; }

        public string sims_assesment_interval_hr { get; set; }

        public string secondary_value { get; set; }

        public string secondary_parameter { get; set; }

        public string primary_parameter { get; set; }

        public string primary_value { get; set; }

   //     public object sims_admission_student_remark { get; set; }
    }
    #endregion

    #region Employee Assesment Schedule
    public class EmpAss
    {
 
        public string opr { get; set; }

        public string interviwer_name { get; set; }

        public string em_applicant_id { get; set; }

        public string em_employee_id { get; set; }

        public string em_interview_date { get; set; }

        public string em_interview_time { get; set; }

        public string em_interview_remark { get; set; }

        public string em_interview_created_date { get; set; }

        public string em_interview_created_user { get; set; }

        public string em_interview_status { get; set; }

        public string sims_teacher_code { get; set; }

        public string sims_teacher_name { get; set; }

        public string teacher_name { get; set; }

        public string em_application_date { get; set; }

        public string emp_name { get; set; }

        public string sims_assesment_teacher_name { get; set; }

        public string sims_assesment_time { get; set; }

        public string sims_assesment_date { get; set; }

        public string sims_appl_date { get; set; }

        public string sims_student_name { get; set; }

        public string sims_pros_no { get; set; }

        public string sims_admission_father_email { get; set; }

        public string sims_admission_father_mobile { get; set; }

        public string sims_prosno_list { get; set; }

        public string sims_assessment_created_user { get; set; }

        public string em_sr_no { get; set; }

        public string emplyee_name { get; set; }

        public string em_doc_path { get; set; }

        public string d { get; set; }

        public string em_round { get; set; }

        public string pays_vacancy_roles { get; set; }

        public string em_desg_code { get; set; }

        public string em_nation_code { get; set; }


        public string dg_desc { get; set; }

        public string sims_nationality_name_en { get; set; }

        public string em_dept_code { get; set; }

        public string codp_dept_name { get; set; }

        public string ename { get; set; }

        public string em_date_of_birth { get; set; }

        public string em_sex { get; set; }

        public string em_mobile { get; set; }

        public string em_email { get; set; }

        public string em_company_code { get; set; }

        public string em_salutation { get; set; }

        public string em_first_name { get; set; }

        public string em_middle_name { get; set; }

        public string em_last_name { get; set; }

        public string em_family_name { get; set; }

        public string em_name_ot { get; set; }

        public string em_marital_status { get; set; }

        public string em_religion_code { get; set; }

        public string em_ethnicity_code { get; set; }

        public string em_appartment_number { get; set; }

        public string em_building_number { get; set; }

        public string em_street_number { get; set; }

        public string em_area_number { get; set; }

        public string em_summary_address { get; set; }

        public string em_city { get; set; }

        public string em_state { get; set; }

        public string em_country_code { get; set; }

        public string em_phone { get; set; }

        public string em_fax { get; set; }

        public string em_po_box { get; set; }

        public string em_passport_number { get; set; }

        public string em_passport_issue_date { get; set; }

        public string em_passport_expiry_date { get; set; }

        public string em_passport_issuing_authority { get; set; }

        public string em_passport_issue_place { get; set; }

        public string em_visa_number { get; set; }

        public string em_visa_issue_date { get; set; }

        public string em_visa_expiry_date { get; set; }

        public string em_visa_issuing_place { get; set; }

        public string em_visa_issuing_authority { get; set; }

        public string em_visa_type { get; set; }

        public string em_national_id { get; set; }

        public string em_national_id_issue_date { get; set; }

        public string em_national_id_expiry_date { get; set; }

        public string em_pan_no { get; set; }

        public string en_labour_card_no { get; set; }

        public string em_img { get; set; }

        public string em_social_address { get; set; }

        public string em_emergency_contact_name1 { get; set; }

        public string em_emergency_contact_name2 { get; set; }

        public string em_emergency_contact_number1 { get; set; }

        public string em_expected_date_of_join { get; set; }

        public string em_emergency_contact_number2 { get; set; }

        public string em_joining_ref { get; set; }

        public string em_handicap_status { get; set; }

        public string em_blood_group_code { get; set; }

        public string em_doc { get; set; }

        public string em_password { get; set; }

        public string em_application_status { get; set; }

        public string em_user_id { get; set; }

        public string em_dest_code { get; set; }

        public string em_staff_type { get; set; }

        public string em_grade_code { get; set; }

        public string em_agreement { get; set; }

        public string em_agreement_start_date { get; set; }

        public string em_agreement_exp_date { get; set; }

        public string offer_letter_doc { get; set; }

        public string em_offer_letter_accepted_flag { get; set; }

        public string em_offer_letter_show_flag { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
    }
    #endregion

    public class DetDes
    {
        public string opr { get; set; }
        public string sims_detention_level_code { get; set; }
        public string sims_detention_desc_code { get; set; }
        public string sims_detention_description { get; set; }
        public string sims_detention_description_ot { get; set; }
        public string sims_display_order { get; set; }
        public bool   sims_detention_desc_status { get; set; }
        public string sims_detention_desc_created_user_code { get; set; }
        public string sims_detention_desc_created_date { get; set; }
        public string sims_detention_name { get; set; }
    }


    public class Thrsho
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_detention_threshold1 { get; set; }
        public string sims_detention_threshold1old { get; set; }
        public string sims_detention_threshold2 { get; set; }
        public string sims_detention_threshold2old { get; set; }
        public string sims_detention_threshold3 { get; set; }
        public string sims_detention_threshold3old { get; set; }
        public bool sims_detention_threshold_status { get; set; }
        public string sims_detention_threshold_created_user_code { get; set; }
        public string sims_detention_threshold_created_date { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_cur_name { get; set; }

    }



    public class DSR001
    {
        public string opr { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string class_name { get; set; }
        public string sims_detention_name { get; set; }
        public string sims_detention_description { get; set; }
        public string sims_detention_point { get; set; }
        public string date { get; set; }
        public string reg_by { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }

        public string sims_detention_remark { get; set; }
        public string sims_detention_level_code { get; set; }
        public string sims_detention_desc_code { get; set; }

        public string cur_code { get; set; }
        public string academic_year { get; set; }
        public string enroll { get; set; }

        public string enroll_no { get; set; }
       
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
      
        public string em_login_code { get; set; }
        public string employee_name { get; set; }
       
        
       
        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }

        public string sims_detention_transaction_number { get; set; }
        
        public string sims_detention_transaction_notified_user_code { get; set; }
        public string sims_detention_transaction_status { get; set; }
        public string sims_detention_transaction_created_user_code { get; set; }
        public string sims_detention_transaction_created_date { get; set; }
        public string sims_detention_transaction_updated_user_code { get; set; }
        public string sims_detention_transaction_updated_date { get; set; }
       
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }

        public string mom_start_date { get; set; }
        public string mom_end_date { get; set; }




        }

    public class dataimport
    {
        public string employee_number { get; set; }
        public string company_code { get; set; }
        public string pay_code { get; set; }
        public string effective_from_date { get; set; }
        public string amount { get; set; }
        public string effective_upto_date { get; set; }
        public string remark { get; set; }
        public string number { get; set; }
        public string effective_from { get; set; }
        public string effective_upto { get; set; }
        public string rowid{ get; set; }
        public string opr { get; set; }
    }
    public class RBAR01
    {
        public string sims_subject_code { get; set; }

        public string opr { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string class_name { get; set; }      

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }      
        

        public string cur_code { get; set; }
        public string academic_year { get; set; }
        

        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }

        public string mom_start_date { get; set; }
        public string mom_end_date { get; set; }

        public string report_call { get; set; }
        public string sims_term_code { get; set; }

        public string sims_appl_form_field { get; set; }
       public string sims_term_desc_en { get; set; }
    }

    public class SBSA01
    {
       
 public string sims_cur_code {get;set;}
        public string sims_academic_year {get;set;}
    public string sims_grade_code {get;set;}
public string sims_section_code {get;set;}
public string student_name {get;set;}
public string sims_gb_cat_assign_enroll_number {get;set;}
public string sims_grade_name_en {get;set;}
public string sims_section_name_en {get;set;}
public string sims_subject_type_name_en {get;set;}
public string final_marks {get;set;}
public string sims_term_code {get;set;}
public string sims_term_desc_en {get;set;}
public string sims_subject_code {get;set;}
public string sims_subject_name_en {get;set;}
        public string sims_subject_type_code { get; set; }

    }
    public class DSD001
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string capacity { get; set; }
        public string intake_student { get; set; }
        public string vacancy { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_grade_code { get; set; }
        public string present { get; set; }
        public string absent { get; set; }
        public string unmark { get; set; }
        public string registration { get; set; }
        public string lt_start_date { get; set; }
        public string lt_end_date { get; set; }
        public string punch_date { get; set; }

        public string em_punching_id { get; set; }

        public string em_name { get; set; }

        public string att_shift1_in { get; set; }

        public string att_shift2_out { get; set; }

        public string em_login_code { get; set; }

        public string leave_code { get; set; }

        public string leave_type { get; set; }

        public string att_absent_flag { get; set; }

        public string att_date { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_admission_date { get; set; }

        public string sims_admission_number { get; set; }

        public string sims_admission_application_number { get; set; }

        public string sims_admission_pros_number { get; set; }

        public string student_name { get; set; }

        public string today_date { get; set; }

        public string dd_fee_payment_mode { get; set; }

        public string dd_fee_payment_mode_desc { get; set; }

        public string fee_amount { get; set; }

        public string dd_fee_number { get; set; }

        public string enroll_number { get; set; }

        public string sims_fee_code_description { get; set; }

        public string fee_month { get; set; }

        public string fee_paid { get; set; }

        public string fee_payment_mode { get; set; }

        public string fee_payment_mode_desc { get; set; }

        public string value { get; set; }

        public string label { get; set; }

        public string t_value { get; set; }
        public string year { get; set; }

        public string teaching { get; set; }

        public string admin { get; set; }

        public string nonteaching { get; set; }

        public string Total { get; set; }

        public string emp_yr { get; set; }

        public string Join { get; set; }

        public string Left { get; set; }

        public string stud_intake { get; set; }
        public string sims_month { get; set; }

        public string total_use { get; set; }

        public string erp { get; set; }

        public string mobileapp { get; set; }

        public string parent_name { get; set; }
        public string parent_code { get; set; }
        public string comn_audit_user_appl_code { get; set; }
        public string comn_audit_start_time { get; set; }
        public string comn_audit_remark { get; set; }
        public string sims_year { get; set; }
        public string sims_date { get; set; }
        public string total_parent { get; set; }
        public string active_parent_current_year { get; set; }
        public string active_parent_current_month { get; set; }
        public string active_parent_current_date { get; set; }
        


    }

    public class CIDR01
    {
        public string sims_appl_parameter { get; set; }

        public string opr { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string sims_comm_tran_number { get; set; }
        public string sims_comm_message { get; set; }

        public string sims_comm_tran_date { get; set; }
        public string sims_comm_sender_id { get; set; }
       
        public string reciver_name { get; set; }


        public string sender_name { get; set; }
        public string sims_comm_recepient_id { get; set; }
        public string no_msg_sent { get; set; }


        public string comn_user_group_code { get; set; }

        

    }

    public class CSSR01
        {
        public string opr { get; set; }
        public string comp_name  { get; set; }
        public string comp_code  { get; set; }
         public string slma_comp_code { get; set; }
        public string slma_ldgrctl_year { get; set; }
        public string slma_ldgrctl_code { get; set; }
        public string sllc_ldgr_type { get; set; }
        public string sllc_ldgr_name { get; set; }
        public string slma_acno { get; set; }
        public string slma_status { get; set; }
        public string slma_name	  { get; set; }
    public string slma_op_bal_amt { get; set; }
    public string slma_op_bal_amt_dr { get; set; }
    public string slma_op_bal_amt_cr { get; set; }
    public string slma_close_bal_amt { get; set; }
    public string slma_close_bal_amt_dr { get; set; }
    public string slma_close_bal_amt_cr { get; set; }
    public string slma_tran_amt_dr { get; set; }
    public string slma_tran_amt_cr { get; set; }
    public string sltr_tran_amt { get; set; }
    public string comn_appl_parameter { get; set; }
    public string comn_appl_form_field_value1 { get; set; }
    public string sllc_ldgr_code { get; set; }
        public string total_op { get; set; }
        public string total_debit { get; set; }
        public string total_credit { get; set; }
        public string total_closing { get; set; }


    }

    public class BAPL01
    {
        public string comp_name { get; set; }
        public string comp_code { get; set; }
       public string group_code { get; set; }
        public string group_name { get; set; }
        public string subgroup_code { get; set; }
        public string subgroup_name { get; set; }
        public string acct_name { get; set; }
        public string glma_acct_code { get; set; }
        public string glma_dept_no { get; set; }
        public string codp_dept_name { get; set; }
        public string actual_amount { get; set; }
        public string glpr_bud_amt { get; set; }
        public string glfp_prd_name { get; set; }
        public string glfp_prd_no { get; set; }
        public string glfp_prd_st_date { get; set; }
        public string glma_year { get; set; }
        public string variance { get; set; }
        public string variance_per { get; set; }

        public List<BAPLlist1> list1 { get; set; }
    }

    public class BAPLlist1

    {
        public string glfp_prd_name { get; set; }
       public string glfp_prd_no { get; set; }
        public string glma_acct_code { get; set; }
         public string actual_amount { get; set; }
        public string glpr_bud_amt { get; set; }
        public string variance { get; set; }
        public string variance_per { get; set; }
        public string group_name { get; set; }
        public string subgroup_code { get; set; }
        public string subgroup_name { get; set; }
        public string acct_name { get; set; }
        public string group_code { get; set; }



    }

    public class Fdr001
    {
        public string flag { get; set; }
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string  sims_cur_code { get; set; }
        public string  sims_grade_code { get; set; }
        public string  sims_section_code { get; set; }
        public string  sims_enroll_number { get; set; }
        public string  student_name { get; set; }
        public string sims_student_img { get; set; }
        
        public string  sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string parent_name { get; set; }
           public string feeling_code { get; set; }
        public string sims_obsprog_code { get; set; }
        public string sims_obsprog_subcode1 { get; set; }
        public string sims_obsprog_subcode2 { get; set; }
        public string sims_obsprog_subcode2_description { get; set; }

       
        public string feeling_desc { get; set; }
        public string activity_code { get; set; }
        public string activity_desc { get; set; }
        public string frequency_code { get; set; }
        public string frequency_desc { get; set; }
        public string quantity_code { get; set; }
        public string quantity_desc { get; set; }
        public string please_prov_code { get; set; }
        public string please_prov_desc { get; set; }
        public string please_prov_qty_code { get; set; }
        public string please_prov_qty_desc { get; set; }

        public string sims_student_feeling { get; set; }
        public string sims_student_activity { get; set; }
        public string sims_student_nappy_changing { get; set; }
        public string sims_student_nappy_changing_type { get; set; }
        public string sims_student_nappy_changing_frequency { get; set; }
        public string sims_student_nappy_changing_remark { get; set; }
        public string sims_student_toilet_training { get; set; }
        public string sims_student_toilet_training_type { get; set; }
        public string sims_student_toilet_training_frequency { get; set; }
        public string sims_student_toilet_training_remark { get; set; }
        public string sims_student_fluid_intake { get; set; }
        public string sims_student_fluid_intake_type { get; set; }
        public string sims_student_fluid_intake_qty { get; set; }
        public string sims_student_fluid_intake_frequency { get; set; }
        public string sims_student_fluid_intake_remark { get; set; }
        public string sims_student_food { get; set; }
        public string sims_student_food_type { get; set; }
        public string sims_student_food_quantity { get; set; }
        public string sims_student_food_frequency { get; set; }
        public string sims_student_slep_time_am { get; set; }
        public string sims_student_slep_time_pm { get; set; }
        public bool sims_student_any_incidence { get; set; }
        public string sims_student_incidence_remark { get; set; }
        public string sims_student_provide_type { get; set; }
        public string sims_student_provide_quantity { get; set; }
        public string sims_student_teacher_comment { get; set; }
        public string sims_student_parent_comment { get; set; }
        public string sims_student_transaction_date { get; set; }
        public string  mom_start_date { get; set; }
        public string sims_student_created_by { get; set; }

        public string sims_student_nappy_changing_dry                  { get; set; }
       public string sims_student_nappy_changing_dry_frequency		  { get; set; }
            public string  sims_student_nappy_changing_dry_Remark			  { get; set; }
            public string  sims_student_nappy_changing_wet				  { get; set; }
            public string  sims_student_nappy_changing_wet_frequency 		  { get; set; }
            public string  sims_student_nappy_changing_wet_Remark		  { get; set; }
            public string  sims_student_nappy_changing_bm					  { get; set; }
            public string  sims_student_nappy_changing_bm_frequency		  { get; set; }
            public string  sims_student_nappy_changing_bm_Remark			  { get; set; }
            public string  sims_student_toilet_training_Number			  { get; set; }

        public string sims_student_toilet_training_Number1 { get; set; }
        public string sims_student_fluid_intake_Milk { get; set; }
        public string  sims_student_toilet_training_Number1_frequency	  { get; set; }
            public string  sims_student_toilet_training_Number1_Remark	  { get; set; }
            public string  sims_student_toilet_training_Number2			  { get; set; }
            public string  sims_student_toilet_training_Number2_frequency	  { get; set; }
            public string  sims_student_toilet_training_Number2_Remark		  { get; set; }
            public string  sims_student_toilet_training_Askedtogo			  { get; set; }
            public string  sims_student_toilet_training_Askedtogo_frequency  { get; set; }
            public string  sims_student_toilet_training_Askedtogo_Remark	  { get; set; }
            public string sims_student_fluid_intake_water_frequency		   { get; set; }
            public string sims_student_fluid_intake_Milk_frequency { get; set; }
            public string  sims_student_fluid_intake_Milk_qty				  { get; set; }
            public string  sims_student_fluid_intake_Milk_remark			  { get; set; }
            public string  sims_student_fluid_intake_Water					  { get; set; }
            public string  sims_student_fluid_intake_Water_qty			  { get; set; }
            public string  sims_student_fluid_intake_Water_remark		  { get; set; }
            public string  sims_student_food_Morning_snack					  { get; set; }
            public string  sims_student_food_Morning_snack_quantity		  { get; set; }
            public string  sims_student_food_Morning_snack_remark			  { get; set; }
            public string  sims_student_food_Lunch				  { get; set; }
            public string  sims_student_food_Lunch_quantity				  { get; set; }
            public string  sims_student_food_Lunch_remark					  { get; set; }
            public string  sims_student_food_Afternoon_snack				  { get; set; }
            public string  sims_student_food_Afternoon_snack_quantity	  { get; set; }
            public string  sims_student_food_Afternoon_snack_remark		  { get; set; }


  public string sims_student_please_provide_diaper { get; set; }
        public string sims_student_please_provide_diaper_qty { get; set; }
        public string sims_student_please_provide_diaper_remark { get; set; }
        public string sims_student_please_provide_Wet_wipes { get; set; }
        public string sims_student_please_provide_Wet_wipes_qty { get; set; }
        public string sims_student_please_provide_Wet_wipes_remark { get; set; }
        public string sims_student_please_provide_Wet_Formula { get; set; }
        public string sims_student_please_provide_Wet_Formula_qty { get; set; }
        public string sims_student_please_provide_Wet_Formula_remark { get; set; }
        public string sims_student_please_provide_Wet_Clothing { get; set; }
        public string sims_student_please_provide_Wet_Clothing_qty { get; set; }
        public string sims_student_please_provide_Wet_Clothing_remark { get; set; }
        public string sims_student_please_provide_Wet_Others { get; set; }
        public string sims_student_please_provide_Wet_Others_qty { get; set; }
        public string sims_student_please_provide_Wet_Others_remark { get; set; }







    }


    public class Fdrn01
    {
        public string flag { get; set; }
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }

        public string opr1 { get; set; }
        public string sims_academic_year1 { get; set; }
        public string sims_cur_code1 { get; set; }
        public string sims_grade_code1 { get; set; }
        public string sims_section_code1 { get; set; }
        public string sims_enroll_number1 { get; set; }
        public string sims_student_created_by1 { get; set; }
    public string mom_start_date1 { get; set; }



        public string student_name { get; set; }
        public string sims_student_img { get; set; }

        public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string parent_name { get; set; }
        public string feeling_code { get; set; }
        public string feeling_desc { get; set; }
        public string activity_code { get; set; }
        public string activity_desc { get; set; }
        public string frequency_code { get; set; }
        public string frequency_desc { get; set; }
        public string quantity_code { get; set; }
        public string quantity_desc { get; set; }
        public string please_prov_code { get; set; }
        public string please_prov_desc { get; set; }
        public string please_prov_qty_code { get; set; }
        public string please_prov_qty_desc { get; set; }

        public string sims_student_feeling { get; set; }
        public string sims_student_activity { get; set; }
        public string sims_student_nappy_changing { get; set; }
        public string sims_student_nappy_changing_type { get; set; }
        public string sims_student_nappy_changing_frequency { get; set; }
        public string sims_student_nappy_changing_remark { get; set; }
        public string sims_student_toilet_training { get; set; }
        public string sims_student_toilet_training_type { get; set; }
        public string sims_student_toilet_training_frequency { get; set; }
        public string sims_student_toilet_training_remark { get; set; }
        public string sims_student_fluid_intake { get; set; }
        public string sims_student_fluid_intake_type { get; set; }
        public string sims_student_fluid_intake_qty { get; set; }
        public string sims_student_fluid_intake_frequency { get; set; }
        public string sims_student_fluid_intake_remark { get; set; }
        public string sims_student_food { get; set; }
        public string sims_student_food_type { get; set; }
        public string sims_student_food_quantity { get; set; }
        public string sims_student_food_frequency { get; set; }
        public string sims_student_slep_time_am { get; set; }
        public string sims_student_slep_time_pm { get; set; }
        public bool sims_student_any_incidence { get; set; }
        public string sims_student_incidence_remark { get; set; }
        public string sims_student_provide_type { get; set; }
        public string sims_student_provide_quantity { get; set; }
        public string sims_obsprog_code_tag_remark { get; set; }
        public string sims_student_teacher_comment { get; set; }
        public string sims_student_parent_comment { get; set; }
        public string sims_student_transaction_date { get; set; }
        public string mom_start_date { get; set; }
        public string sims_student_created_by { get; set; }

        public string sims_obsprog_incidence_desc { get; set; }

        public string sims_obsprog_incidence_location { get; set; }
        public string sims_obsprog_incidence_teacher_remark { get; set; }
        public string sims_obsprog_incidence_parent_remark { get; set; }


        public string sims_student_nappy_changing_dry { get; set; }
        public string sims_student_nappy_changing_dry_frequency { get; set; }
        public string sims_student_nappy_changing_dry_Remark { get; set; }
        public string sims_student_nappy_changing_wet { get; set; }
        public string sims_student_nappy_changing_wet_frequency { get; set; }
        public string sims_student_nappy_changing_wet_Remark { get; set; }
        public string sims_student_nappy_changing_bm { get; set; }
        public string sims_student_nappy_changing_bm_frequency { get; set; }
        public string sims_student_nappy_changing_bm_Remark { get; set; }
        public string sims_student_toilet_training_Number { get; set; }

        public string sims_student_toilet_training_Number1 { get; set; }
        public string sims_student_fluid_intake_Milk { get; set; }
        public string sims_student_toilet_training_Number1_frequency { get; set; }
        public string sims_student_toilet_training_Number1_Remark { get; set; }
        public string sims_student_toilet_training_Number2 { get; set; }
        public string sims_student_toilet_training_Number2_frequency { get; set; }
        public string sims_student_toilet_training_Number2_Remark { get; set; }
        public string sims_student_toilet_training_Askedtogo { get; set; }
        public string sims_student_toilet_training_Askedtogo_frequency { get; set; }
        public string sims_student_toilet_training_Askedtogo_Remark { get; set; }
        public string sims_student_fluid_intake_water_frequency { get; set; }
        public string sims_student_fluid_intake_Milk_frequency { get; set; }
        public string sims_student_fluid_intake_Milk_qty { get; set; }
        public string sims_student_fluid_intake_Milk_remark { get; set; }
        public string sims_student_fluid_intake_Water { get; set; }
        public string sims_student_fluid_intake_Water_qty { get; set; }
        public string sims_student_fluid_intake_Water_remark { get; set; }
        public string sims_student_food_Morning_snack { get; set; }
        public string sims_student_food_Morning_snack_quantity { get; set; }
        public string sims_student_food_Morning_snack_remark { get; set; }
        public string sims_student_food_Lunch { get; set; }
        public string sims_student_food_Lunch_quantity { get; set; }
        public string sims_student_food_Lunch_remark { get; set; }
        public string sims_student_food_Afternoon_snack { get; set; }
        public string sims_student_food_Afternoon_snack_quantity { get; set; }
        public string sims_student_food_Afternoon_snack_remark { get; set; }


        public string sims_student_please_provide_diaper { get; set; }
        public string sims_student_please_provide_diaper_qty { get; set; }
        public string sims_student_please_provide_diaper_remark { get; set; }
        public string sims_student_please_provide_Wet_wipes { get; set; }
        public string sims_student_please_provide_Wet_wipes_qty { get; set; }
        public string sims_student_please_provide_Wet_wipes_remark { get; set; }
        public string sims_student_please_provide_Wet_Formula { get; set; }
        public string sims_student_please_provide_Wet_Formula_qty { get; set; }
        public string sims_student_please_provide_Wet_Formula_remark { get; set; }
        public string sims_student_please_provide_Wet_Clothing { get; set; }
        public string sims_student_please_provide_Wet_Clothing_qty { get; set; }
        public string sims_student_please_provide_Wet_Clothing_remark { get; set; }
        public string sims_student_please_provide_Wet_Others { get; set; }
        public string sims_student_please_provide_Wet_Others_qty { get; set; }
        public string sims_student_please_provide_Wet_Others_remark { get; set; }

        public string dry_frequency_code { get; set; }
        public string dry_frequency_desc { get; set; }
        public string dry_sims_obsprog_code_tag_code { get; set; }
        public string nppy_sims_obsprog_subcode1 { get; set; }
        public string wet_frequency_code { get; set; }
        public string wet_frequency_desc { get; set; }
        public string wet_sims_obsprog_code_tag_code { get; set; }
        
        public string bm_frequency_code { get; set; }
        public string bm_frequency_desc { get; set; }
        public string bm_sims_obsprog_code_tag_code { get; set; }
        
        public string n1_frequency_code { get; set; }
        public string n1_frequency_desc { get; set; }
        public string n1_sims_obsprog_code_tag_code { get; set; }
        public string toi_sims_obsprog_subcode1 { get; set; }
        public string n2_frequency_code { get; set; }
        public string n2_frequency_desc { get; set; }
        public string n2_sims_obsprog_code_tag_code { get; set; }
        public string sims_obsprog_subcode1 { get; set; }
        
        public string ask_frequency_code { get; set; }
        public string ask_frequency_desc { get; set; }
        public string ask_sims_obsprog_code_tag_code { get; set; }
        
        public string milk_frequency_code { get; set; }
        public string milk_frequency_desc { get; set; }
        public string milk_sims_obsprog_code_tag_code { get; set; }
        public string flu_sims_obsprog_subcode1 { get; set; }
        public string milk_frequency_qty_code { get; set; }
        public string milk_frequency_qty_desc { get; set; }
        public string wat_frequency_code { get; set; }
        public string wat_frequency_desc { get; set; }
        public string wat_sims_obsprog_code_tag_code { get; set; }
        public string wat_frequency_qty_code { get; set; }
        public string wat_frequency_qty_desc { get; set; }
        public string jui_frequency_code { get; set; }
        public string jui_frequency_desc { get; set; }
        public string jui_sims_obsprog_code_tag_code { get; set; }
        public string jui_frequency_qty_code { get; set; }
        public string jui_frequency_qty_desc { get; set; }
        public string mor_frequency_code { get; set; }
        public string mor_frequency_desc { get; set; }
        public string mor_sims_obsprog_code_tag_code { get; set; }
        public string foo_sims_obsprog_subcode1 { get; set; }
        public string lunch_frequency_code { get; set; }
        public string lunch_frequency_desc { get; set; }
        public string lunch_sims_obsprog_code_tag_code { get; set; }
        public string aft_frequency_code { get; set; }
        public string aft_frequency_desc { get; set; }
        public string aft_sims_obsprog_code_tag_code { get; set; }
        public string sims_enroll_no { get; set; }
        public string sims_obsprog_number { get; set; }
        public string sims_obsprog_date { get; set; }
        public string sims_obsprog_code { get; set; }
        public string sims_obsprog_subcode2 { get; set; }
        public string sims_obsprog_subcode3 { get; set; }
        public string sims_obsprog_code_tag_code { get; set; }
        public string sims_obsprog_code_tag_parameter_frequency_code { get; set; }
        public string sims_obsprog_code_tag_parameter_frequency_quantity_code { get; set; }
        public string sims_obsprog_teacher_remark { get; set; }
        public string sims_obsprog_parent_remark { get; set; }
        public string sims_obsprog_communication_flag { get; set; }
        public string sims_obsprog_status { get; set; }
        public string sims_obsprog_created_date { get; set; }
        public string sims_obsprog__created_by { get; set; }
        public string sims_obsprog_path { get; set; }        
        public string sims_obsprog_subcode2_description { get; set; }
        public string sims_obsprog_code_tag_parameter_frequency_description { get; set; }
        public string sims_obsprog_code_tag_parameter_frequency_quantity_code_description { get; set; }











        public string sims_student_name { get; set; }

        public string sims_student_enroll_number { get; set; }

        public string HomeroomCode { get; set; }

        public string sims_homeroom_name { get; set; }
    }



    public class PIDR01
    {
        public string opr { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
         public string sims_grade_name_en { get; set; }
        public string sims_section_name_en { get; set; }


        public string doc_sr_code { get; set; }
        public string doc_prov_no { get; set; }
        public string im_inv_no { get; set; }
        public string dd_qty { get; set; }
        public string dd_issue_qty { get; set; }
        public string balance_qty { get; set; }
        public string dd_outstanding_qty { get; set; }
        public string im_desc { get; set; }
        public string sal_type { get; set; }
        public string part_status { get; set; }
        public string cus_account_no { get; set; }
        public string creation_user { get; set; }

        public string name { get; set; }
        public string father_name { get; set; }
       
    }

    public class SLDR01
    {
        public string opr { get; set; }
        public string current_emp_code { get; set; }
        public string current_teacher_name { get; set; }
        public string current_subject_name { get; set; }
        public string sub_emp_code { get; set; }
        public string current_teacher_code { get; set; }
        public string  sub_teacher_code { get; set; }
        public string sims_bell_substitution_date { get; set; }
        public string sub_teacher_name { get; set; }
        public string sub_subject_name { get; set; }


        public string day_date { get; set; }
        public string time { get; set; }
        public string sims_bell_slot_desc { get; set; }
        public string class1 { get; set; }
        public string remark { get; set; }
        public string approved_by { get; set; }

        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_desc { get; set; }
        public string mom_start_date { get; set; }
        public string mom_end_date { get; set; }
        public string report_status { get; set; }


    }

    public class studentattendance
    {
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_desc { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_attendance_type_code { get; set; }
        public string sims_attendance_type_name { get; set; }
        public string sims_attendance_type_details_code { get; set; }
        public string sims_attendance_type_details_name { get; set; }
        public string sims_attendance_date { get; set; }
        public string sims_attendance_sort_code { get; set; }
        public string sims_attendance_sort_name { get; set; }

        public string sims_attendance_code { get; set; }

        public string sims_attendance_code_sort_name { get; set; }

        public string sims_attendance_code_name { get; set; }

        public string sims_student_name { get; set; }

        public string sims_student_fname { get; set; }

        public string sims_student_nickname { get; set; }

        public string sims_student_img { get; set; }

        public string sims_student_enroll { get; set; }

        public string sims_attendance_color { get; set; }

        public bool sims_student_bday { get; set; }

        public string homeroombatch { get; set; }
        public string sims_homeroom_code { get; set; }
        public string sims_batch_code { get; set; }
        public string attednacedate { get; set; }
        public string sims_subject_name_en { get; set; }
        public string sims_bell_subject_code { get; set; }

        public bool sims_isholiday { get; set; }

        public string sims_bus_number { get; set; }

        public string sims_term_code { get; set; }

        public string sims_term_desc_en { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_class_name { get; set; }

        public string sims_present { get; set; }

        public string sims_absent { get; set; }

        public string sims_absent_excused { get; set; }

        public string sims_Tardy { get; set; }

        public string sims_Unmarked { get; set; }

        public string sims_total { get; set; }

        public string sims_TotalAttendanceDays { get; set; }

        public string sims_Percentage { get; set; }
        
        public string sims_category_code { get; internal set; }
        
        public string sims_from_date { get; set; }
        
        public string sims_attendance_day_comment { get; set; }


        public string sims_subject_code { get; set; }

        public string slot { get; set; }

        public string slot_name { get; set; }

        public string sims_teacher_name { get; set; }

        public string sims_employee_code { get; set; }
    }

    public class SENLST
    {
         public string sims_admission_subject_group { get; set; }
         public string sims_admission_subject_group_value { get; set; }
         public string sims_created_by { get; set; }
         public string sims_diagnosis { get; set; }
         public bool sims_admission_sen_approve_status { get; set; }
         public string sims_admission_number { get; set; }
         public string sims_admission_date { get; set; }
         public string name { get; set; }
         public string sims_grade_name_en { get; set; }
         public string sims_section_name_en { get; set; }
         public string sims_academic_year_description { get; set; }
         public string sims_cur_short_name_en { get; set; }
         public string sims_admission_dob { get; set; }
         public string sims_admission_mobile { get; set; }
         public string admission_class { get; set; }
         public string show_button { get; set; }
         public string sims_admission_academic_year { get; set; }
         public string sims_admission_grade_code { get; set; }
         public string sims_admission_cur_code { get; set; }
         // public string sims_sr_no { get; set; }
         public string opr { get; set; }
         public bool sims_admission_special_education_status { get; set; }



         public string sims_sen_status_remark { get; set; }

         public string sims_sen_approve_remark { get; set; }

         public string sims_admission_sen_status_created_by { get; set; }
         public string sims_admission_sen_status_created_date { get; set; }
         public string sims_admission_sen_approve_created_by { get; set; }
         public string sims_admission_sen_approve_created_date { get; set; }
         public string value { get; set; }
         public string user_name { get; set; }
         public string sims_admission_sr_no { get; set; }
         public bool sims_admission_sen_compelted_status { get; set; }
         public string sims_sen_rejected_remark { get; set; }
         public string sims_sen_rejected_by { get; set; }
         public string sims_sen_transaction_number { get; set; }
     
    }


    public class FTLR01
    {
        public string opr { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string current_emp_code { get; set; }
        public string current_teacher_name { get; set; }
        public string current_subject_name { get; set; }
        public string sub_emp_code { get; set; }
        public string current_teacher_code { get; set; }
        public string sub_teacher_code { get; set; }
        public string sims_bell_substitution_date { get; set; }
        public string sims_day_name_en { get; set; }
        public string sims_day_code { get; set; }
        public string day_date { get; set; }
        public string time { get; set; }
        public string sims_bell_slot_desc { get; set; }
        public string class1 { get; set; }
        public string sims_bell_slot { get; set; }
        public string approved_by { get; set; }

        public string sims_teacher_code { get; set; }
        public string sims_teacher_name { get; set; }
        public string sims_bell_code { get; set; }
        public string sims_bell_desc { get; set; }
        public string sims_bell_academic_year { get; set; }
       
        public string sims_bell_day_code { get; set; }
        public string sims_bell_slot_code { get; set; }
       
        public string day { get; set; }
       
    
    public string teacher_status { get; set; }
    public string teacher_hide { get; set; }

    public List<FTLR01_child> sub;

    }

    public class FTLR01_child
    {

        public string sims_bell_academic_year { get; set; }

        public string sims_bell_day_code { get; set; }
        public string sims_bell_slot_code { get; set; }

        public string day { get; set; }
        public string sims_teacher_code { get;  set; }
        public string sims_teacher_name { get;  set; }
        public string sims_bell_desc { get;  set; }
        public string sims_bell_slot { get;  set; }
        public string time { get;  set; }
        public string class1 { get;  set; }
        public string teacher_status { get; set; }
    }


    public class CQRQC
    {
        
        public string opr { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public string sims_comm_status { get; set; }

        public string sims_comm_number { get; set; }

        public string sims_comm_tran_number { get; set; }

        public string sims_comm_date { get; set; }

        public string sims_comm_tran_date { get; set; }

        public string sims_comm_category { get; set; }

        public string sims_comm_user_code { get; set; }

        public string sims_subject_id { get; set; }

        public string sims_comm_subject { get; set; }

        public string sender_id { get; set; }

        public string rece_id { get; set; }

        public string sims_comm_message { get; set; }

        public string Initial_sender_name { get; set; }

        public string Initial_receiver_name { get; set; }

        public string sender_name { get; set; }

        public string receiver_name { get; set; }

        public string initial_send { get; set; }

        public string initial_receive { get; set; }

        public string sims_parent_father_mobile { get; set; }

        public string sims_parent_father_email { get; set; }
    }

    #region Com100(communication query report)

    public class Com100
    {
        public string opr { get; set; }
      
         public string sims_cur_code { get; set; }

         public string sims_academic_year { get; set; }

         public string sims_comm_number { get; set; }

         public string sims_comm_tran_number { get; set; }

         public string sims_comm_date { get; set; }

         public string sims_comm_tran_date { get; set; }

         public string sims_comm_category { get; set; }

         public string sims_comm_user_code { get; set; }

         public string sims_subject_id { get; set; }

         public string sims_comm_subject { get; set; }

         public string sender_id { get; set; }

         public string rece_id { get; set; }

         public string sims_comm_message { get; set; }

         public string Initial_sender_name { get; set; }

         public string Initial_receiver_name { get; set; }

         public string sender_name { get; set; }

         public string receiver_name { get; set; }

         public string initial_send { get; set; }

         public string initial_receive { get; set; }

         public string sims_grade_code { get; set; }

         public string sims_section_code { get; set; }

         public string sims_comm_status { get; set; }
    }

    #endregion

    public class alloc_roll_no
    {
        public string sims_student_enroll_number { get; set; }

        public string sims_student_name { get; set; }

        public string sims_roll_number { get; set; }
        public string message { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }

        public string sims_enroll_roll { get; set; }

        public string sims_first_name { get; set; }

        public string sims_middle_name { get; set; }

        public string sims_last_name { get; set; }

        public string seat_no { get; set; }

        public string sims_board_exam_enroll_number { get; set; }

        public string sims_board_exam_registration_no { get; set; }
        public string sims_student_gender { get; set; }
    }
    public class empdash
    {
        public string sims_appl_form_field_value1 { get; set; }
        public string em_service_Status { get; set; }
        public string count { get; set; }

        public string em_grade_code { get; set; }
        public string sims_cur_code { get; set; }
        public string gr_desc { get; set; }
        public string em_login_code { get; set; }
        public string emp_name { get; set; }
        public string em_date_of_birth { get; set; }

        public string em_sex { get; set; }
        public string em_mobile { get; set; }
        public string em_email { get; set; }
        public string codp_dept_name { get; set; }

        public string dg_desc { get; set; }
        public string em_left_date { get; set; }
        public string em_left_reason { get; set; }
        public string em_date_of_join { get; set; }

        public string em_visa_number { get; set; }
        public string em_visa_issue_date { get; set; }
        public string em_visa_expiry_date { get; set; }
        public string em_national_id { get; set; }
        public string em_national_id_issue_date { get; set; }
        public string em_national_id_expiry_date { get; set; }
        public string Type { get; set; }
    }




    public class MIS412

    {
        public string user_wise_count { get; set; }
        public string comn_audit_user_code { get; set; }
        public string comn_user_full_name { get; set; }
        public string comn_user_device_model { get; set; }
        public string count_of_device { get; set; }

        public string count_of_mobile_device { get; set; }
        public string user_id_code { get; set; }
        public string user_mobile_number { get; set; }
        public string user_email_address { get; set; }
        public string comn_user_device_status { get; set; }

        //
        public string comn_mod_code { get; set; }
        public string comn_mod_name_en { get; set; }
        public string comn_user_name { get; set; }
        public string sims_user_name { get; set; }
        public string comn_user_group_code { get; set; }
        public string comn_user_date_created { get; set; }
        public string comn_user_last_login { get; set; }
        public string comn_audit_start_time { get; set; }
        public string comn_audit_end_time { get; set; }
        public string count { get; set; }
        public string comn_audit_user_appl_code { get; set; }
        public string comn_user_status { get; set; }
        public string comn_appl_name_en { get; set; }
       
    }

    public class CSR001
    {
        public string pc_ref_no { get; set; }
        public string pc_submission_date { get; set; }
        public string sub_cheque_amt { get; set; }
        public string no_sub_cheque { get; set; }
        public string re_sub_cheque_amt { get; set; }
        public string no_re_sub_cheque { get; set; }
        public string out_cheque_amt { get; set; }
        public string no_out_cheque { get; set; }
        public string ret_cheque_amt { get; set; }
        public string no_ret_cheque { get; set; }
        public string cancle_cheque_amt { get; set; }
        public string no_cancle_cheque { get; set; }
        public string realise_cheque_amt { get; set; }
        public string no_realise_cheque { get; set; }

    }
    public class ATRC01
    {
        public string comn_alert_user_code { get; set; }
        public string comn_alert_date { get; set; }
        public string comn_alert_message { get; set; }
        public string priority { get; set; }
        public string comn_alert_type_code { get; set; }
        public string user_name { get; set; }
        public string comn_alert_status { get; set; }
    }

    public class SAR011
    {

        public string sims_stage_sr_no { get; set; }

        public string sims_stage_description { get; set; }

        public string opr { get; set; }

        public bool sims_stage_status { get; set; }
    }
    public class SLRQ01
    {
        public string sims_student_cur_code { get; set; }
        public string sims_student_academic_year { get; set; }
        public string sims_student_enroll_number { get; set; }

        public string sims_parent_login_code { get; set; }

        public string student_name { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_parent_father_mobile { get; set; }

        public string sims_parent_mother_mobile { get; set; }

        public string leaving_date { get; set; }

        public string school { get; set; }

        public string oversease { get; set; }

        public string sims_student_remark { get; set; }
        public string sims_student_academic_status { get; set; }
        public string student_left { get; set; }
        public string appearing { get; set; }



        public string sims_questionbank_code { get; set; }

        public string sims_questionbank_subject { get; set; }

        public object q_code { get; set; }

        public string from_date { get; set; }

        public string to_date { get; set; }
    }

    public class atdr01
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string capacity { get; set; }
        public string intake_student { get; set; }
        public string vacancy { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_grade_code { get; set; }
        public string present { get; set; }
        public string absent { get; set; }
        public string unmark { get; set; }

        public string others { get; set; }
        public string registration { get; set; }

        public string punch_date { get; set; }

        public string em_punching_id { get; set; }

        public string em_name { get; set; }

        public string att_shift1_in { get; set; }

        public string att_shift2_out { get; set; }

        public string em_login_code { get; set; }

        public string leave_code { get; set; }

        public string leave_type { get; set; }

        public string att_absent_flag { get; set; }

        public string att_date { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_admission_date { get; set; }

        public string sims_admission_number { get; set; }

        public string sims_admission_application_number { get; set; }

        public string sims_admission_pros_number { get; set; }

        public string student_name { get; set; }

        public string today_date { get; set; }

        public string dd_fee_payment_mode { get; set; }

        public string dd_fee_payment_mode_desc { get; set; }

        public string fee_amount { get; set; }

        public string dd_fee_number { get; set; }

        public string enroll_number { get; set; }

        public string sims_fee_code_description { get; set; }

        public string fee_month { get; set; }

        public string fee_paid { get; set; }

        public string sims_section_code { get; set; }

        public string sims_enroll_number { get; set; }


        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string flag { get; set; }

        public string att_month { get; set; }
        public string sims_attendance_date { get; set; }

        public string sims_appl_parameter { get; set; }
        public string sims_appl_form_field_value1 { get; set; }
        public string month_status { get; set; }

    }
 }
   
    public class ACT100
    {

        public string opr { get; set; }

        public string sims_activity_number { get; set; }
     
        public string sims_activity_name { get; set; }

        public string sims_enroll_number { get; set; }

        public string sims_activity_points { get; set; }

        public string sims_enrollment_number { get; set; }

        public string sims_student_full_name { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_grade_code { get; set; }

        public string sims_section_code { get; set; }

        public int sims_roll_number { get; set; }

        public string stud_Add_Activity { get; set; }

        public string sims_grade_name_en { get; set; }

        public string sims_section_name_en { get; set; }

    public string sims_term_code { get; set; }

    public string sims_term_desc_en { get; set; }

    public string sims_term_number { get; set; }

    public string sims_activity_max_point { get; set; }
    public string sims_activity_max_point_subject { get; set; }
    public string sims_activity_priority { get; set; }

}

public class que_bank_product {

    public string sims_questionbank_set_no { get; set; }

    public string sims_questionbank_set_name { get; set; }

    public string sims_questionbank_code { get; set; }

    public string sims_questionbank_set_cost { get; set; }

    public Boolean sims_questionbank_set_status { get; set; }

    public string sims_questionbank_set_created_by { get; set; }

    public Boolean sims_questionbank_set_free_status { get; set; }

    public string sims_questionbank_set_logo_path { get; set; }

    public string opr { get; set; }

    public string comp_curcy_code { get; set; }
    public string comp_curcy_dec { get; set; }

    public string sims_grade_code { get; set; }
    public string sims_grade_name { get; set; }

    public string sims_questionbank_set_created_date { get; set; }

    public string sims_questionbank_subject { get; set; }
}

public class sim178
{
    public string sims_cur_code { get; set; }
    public string sims_academic_year { get; set; }

    public string sims_grade_code { get; set; }
    public string sims_grade_name_en { get; set; }
    public string sims_section_code { get; set; }
    public string sims_section_name_en { get; set; }
    public string sims_class_teacher_code { get; set; }
    public string sims_class_assistant_teacher_code { get; set; }
    public string sims_class_teacher_status { get; set; }
    public string sims_teacher_code { get; set; }
    public string sims_teacher_assi_code { get; set; }
    public string sims_teacher_super_code { get; set; }
    public string sims_supervisor_code { get; set; }
    public string opr { get; set; }

    public string class_teacher_name { get; set; }
    public string class_assistant_name { get; set; }
    public string sims_supervisor_name { get; set; }
}
public class reportflg
{
    public string sims_appl_form_field_value1 { get; set; }

    public string sims_appl_form_field_value2 { get; set; }

    public string sims_appl_form_field_value3 { get; set; }

    public string sims_grade_code { get; set; }

    public string sims_section_code { get; set; }

    public string sims_appl_parameter { get; set; }

    public string sims_appl_form_field_value4 { get; set; }
}

public class reg
{
    public string pays_user_name { get; set; }
    public string pays_user_password { get; set; }
    public string pays_user_date_created { get; set; }
    public string pays_user_email { get; set; }
    public string pays_user_status { get; set; }
    public string pays_user_nationalid { get; set; }
    public string pays_user_mobileno { get; set; }
    public string opr { get; set; }
}


public class INVPDF {
    public object with_vat { get; set; }

    public object grand_total { get; set; }

    public object grand_total_disc { get; set; }

    public object final_amount { get; set; }

    public object Amt_word_N { get; set; }

    public object in_date_new { get; set; }

    public object print_date { get; set; }

    public object sr_no { get; set; }

    public object in_date { get; set; }

    public object creation_user { get; set; }

    public object student_name { get; set; }

    public object grade { get; set; }

    public object sims_student_admission_number { get; set; }

    public object parent_name { get; set; }

    public object parent_address { get; set; }

    public object sims_academic_year { get; set; }

    public object id_fee_amount { get; set; }

    public object id_fee_amount_discounted { get; set; }

    public object sims_grade_code { get; set; }

    public object sims_section_code { get; set; }

    public object enroll_number { get; set; }

    public object in_total_amount { get; set; }

    public object sims_fee_code { get; set; }

    public object sims_fee_code_description { get; set; }

    public object Amt_word { get; set; }

    public object Prev_outstanding { get; set; }


    public object term_code { get; set; }

    public object in_no { get; set; }

    public object sims_fee_code_vat_percentage { get; set; }

    public object sims_student_commence_date { get; set; }

    public object dupli_in { get; set; }

    public object final_am { get; set; }

    public object new_dis { get; set; }




}

public class Clar101
{
    public string opr { get; set; }
    public string sims_health_parameter_sr_no { get; set; }
    public string sims_health_parameter_code { get; set; }
    public string sims_health_parameter_name_en { get; set; }
    public string sims_health_parameter_description { get; set; }
    public string sims_health_parameter_name_ot { get; set; }
    public string sims_health_parameter_min_value { get; set; }
    public string sims_health_parameter_max_value { get; set; }
    public string sims_health_parameter_default_value { get; set; }
    public string sims_health_parameter_unit { get; set; }
    public string sims_health_parameter_display_order { get; set; }
    public bool sims_health_parameter_isNumeric { get; set; }
    public string sims_health_parameter_image { get; set; }
    public string sims_health_parameter_status { get; set; }
    public string sims_health_parameter_created_by { get; set; }
    public string sims_health_parameter_create_datetime { get; set; }
    public string sims_health_parameter_check_code { get; set; }
    public List<parameter_Details> med_det { get; set; }
    
    public string sims_health_parameter_check_name_en { get; set; }
    public string sims_health_parameter_check_min_value { get; set; }
    public string sims_health_parameter_check_max_value { get; set; }
    public string sims_health_parameter_check_comment { get; set; } 

    public string user { get; set; } 
  
}


 
public class parameter_Details
{
    public string sims_health_parameter_code { get; set; }
    public string sims_health_parameter_description { get; set; }
    public string sims_health_parameter_check_name_en { get; set; }
    public string sims_health_parameter_check_min_value { get; set; }
    public string sims_health_parameter_check_max_value { get; set; }
    public string sims_health_parameter_check_comment { get; set; }
   // public string sims_health_parameter_sr_no { get; set; }
    public string user { get; set; }

    public string sims_health_parameter_check_code { get; set; }
    public string sims_health_parameter_check_name_ot { get; set; }
    public string sims_health_parameter_check_status { get; set; }
    public string sims_health_parameter_check_created_by { get; set; }
    public string sims_health_parameter_check_created_datetime { get; set; }
}



    public class health_camp
{
    public string sims_health_camp_code { get; set; }
    public string sims_health_package_code { get; set; }
    public string sims_health_cur_code { get; set; }
    public string sims_health_camp_name_en { get; set; }
    public string sims_health_camp_date { get; set; }
    public string sims_health_camp_desc { get; set; }
    public string sims_health_camp_status { get; set; }
    public string sims_health_package_name_en { get; set; }
    public string sims_cur_short_name_en { get; set; }
    public string sims_student_enroll_number { get; set; }
    public string stud_name { get; set; }
    public string sims_student_dob { get; set; }
    public string sims_student_date { get; set; }
    public string sims_grade_code { get; set; }
    public string sims_section_code { get; set; }
    public string sims_grade_name_en { get; set; }
    public string sims_section_name_en { get; set; }
    public string stud_age { get; set; }


    public string sims_health_enroll_number { get; set; }
    public string sims_health_camp_employee_code { get; set; }
    public string sims_health_camp_transaction_date { get; set; }
    public string sims_health_camp_report_code { get; set; }
    public string sims_health_test_code { get; set; }
    public string sims_health_parameter_code { get; set; }
    public string sims_health_parameter_check_code { get; set; }
    public string sims_health_camp_parameter_check_value { get; set; }
    public string sims_health_camp_parameter_check_comment { get; set; }
    public string sims_health_camp_transaction_status { get; set; }
    public bool sims_health_camp_student_absent_status { get; set; }
    public string comn_audit_user_location { get; set; }
    public string comn_audit_ip { get; set; }
    public string sims_student_gender { get; set; }
    public string sims_health_test_name_en { get; set; }
    public string sims_health_test_file { get; set; }
    public string sims_health_test_file_uploaded_by { get; set; }
    public string sims_health_file_status { get; set; }
    public string sims_health_csf_sr_no { get; set; }
    public string sims_health_csfd_sr_no { get; set; }

}


public class health_camp_package
{
    public string sims_health_package_code { get; set; }
    public string sims_health_package_name_en { get; set; }
    public string sims_health_enroll_number { get; set; }
    public string sims_health_camp_code { get; set; }

    public List<health_camp_test> test_lst { get; set; }
}

public class health_camp_test
{
    public string sims_health_package_code { get; set; }
    public string sims_health_test_code { get; set; }
    public string sims_health_test_name_en { get; set; }
    public string sims_health_test_display_order { get; set; }
    public string sims_health_enroll_number { get; set; }


    public List<health_camp_parameter> parameter_lst { get; set; }

}

public class health_camp_parameter
{
    public string sims_health_test_code { get; set; }

    public string sims_health_parameter_code { get; set; }
    public string sims_health_parameter_name_en { get; set; }
    public string sims_health_parameter_min_value { get; set; }
    public string sims_health_parameter_max_value { get; set; }
    public string sims_health_parameter_default_value { get; set; }
    public string sims_health_parameter_unit { get; set; }
    public string sims_health_parameter_isNumeric { get; set; }
    public string sims_health_parameter_display_order { get; set; }

    public string sims_health_camp_code { get; set; }
    //public string sims_health_camp_test_code { get; set; }
    //public string sims_health_camp_parameter_code { get; set; }
    //public string sims_health_camp_parameter_check_code  { get; set; }
    public string sims_health_camp_parameter_check_value { get; set; }
    public string sims_health_camp_parameter_check_comment { get; set; }
    public string sims_health_camp_report_code { get; set; }
    public string sims_health_enroll_number { get; set; }

    public List<health_camp_check> check_lst { get; set; }

}

public class health_camp_check
{
    public string sims_health_parameter_code { get; set; }

    public string sims_health_parameter_check_code { get; set; }
    public string sims_health_parameter_check_name_en { get; set; }
    public string sims_health_parameter_check_min_value { get; set; }
    public string sims_health_parameter_check_max_value { get; set; }
    public string sims_health_parameter_check_comment { get; set; }
       
    public string sims_health_camp_test_code { get; set; }
    public string sims_health_camp_parameter_code { get; set; }
    public string sims_health_camp_parameter_check_code { get; set; }

    public string sims_health_camp_parameter_check_value { get; set; }

    public string sims_health_camp_parameter_check_comment { get; set; }
    public string sims_health_camp_report_code { get; set; }
}

public class signaturedata
{
    public string cc { get; set; }
    public string ay { get; set; }
    public string gc { get; set; }
    public string sc { get; set; }
    public string search_stud { get; set; }
    public string pid { get; set; }
    public string signdate { get; set; }
    public string signdetails { get; set; }
    public string signimgpath { get; set; }
    public string signtakenbyuser { get; set; }
    public string term_code { get; set; }
    public string term1feepaydate { get; set; }
    public string term2feepaydate { get; set; }
    public string term3feepaydate { get; set; }
    
}

public class sims7401
{

    public string msg_body_temp { get; set; }
    public string sims_form_field { get; set; }
    public string sims_mod_code { get; set; }
    public string sims_appl_code { get; set; }
    public bool sims_email_flag { get; set; }
    public string sims_smtp_id { get; set; }
    public string sims_email_templ_no { get; set; }
    public bool sims_sms_flag { get; set; }
    public string sims_sms_templ_no { get; set; }
    public bool sims_alert_flag { get; set; }
    public string sims_alert_templ_no { get; set; }
    public bool sims_status { get; set; }
    public string sims_msg_appl_code { get; set; }
    public string sims_msg_sr_no { get; set; }
    public string sims_msg_subject { get; set; }
   public string opr { get; set; }
    public string sims_smtp_code { get; set; }
    public string sims_smtp_username { get; set; }
    public string comn_mod_code { get; set; }
    public string comn_mod_name_en { get; set; }
    public string comn_appl_name_en { get; set; }

}

#region studentTargetTerm
public class studentTargetTerm
{
    public string aca_year { get; set; }
    public string cur_code { get; set; }
    public string grade_code { get; set; }
    public string section_code { get; set; }
    public string subject_code { get; set; }
    public string term_code { get; set; }
    public string sims_enroll_number { get; set; }
    public string sims_mark_grade_code { get; set; }
    public string sims_grade_scale_code { get; set; }
    public string sims_report_card_att1 { get; set; }
    public string sims_report_card_att2 { get; set; }
    public string sims_report_card_att3 { get; set; }
    public string sims_report_card_student_target_status { get; set; }
    public string sims_user { get; set; }
    public string sims_comment1_desc { get; set; }
    public string term_code_n { get; set; }
}

public class stdrpt
{
public string sims_appl_parameter { get; set; }
    public string sims_appl_form_field_value1 { get; set; }
}
#endregion

