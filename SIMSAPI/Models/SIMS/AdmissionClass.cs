﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIMSAPI.Models.SIMS
{
    #region sims_parent_admission_list

    public class parent_admission_list
    {
        public string sims_admission_parent_REG_id { get; set; }
        public string sims_parent_name { get; set; }
        public string sims_admission_parent_email { get; set; }
        public string sims_admission_parent_mobile { get; set; }
        public string sims_admission_number { get; set; }
        public string sims_admission_pros_number { get; set; }
        public bool sims_admission_flag { get; set; }
    }
    #endregion
    public class AdmissionClass
    {
    }

    public class sims_admission_parent
    {
        public string sims_admission_parent_REG_id { get; set; }
        public string sims_admission_parent_salutation_code { get; set; }
        public string sims_admission_parent_first_name { get; set; }
        public string sims_admission_parent_middle_name { get; set; }
        public string sims_admission_parent_last_name { get; set; }
        public string sims_admission_parent_family_name { get; set; }
        public string sims_admission_parent_name_ot { get; set; }
        public string sims_admission_parent_nationality1_code { get; set; }
        public string sims_admission_parent_appartment_number { get; set; }
        public string sims_admission_parent_building_number { get; set; }
        public string sims_admission_parent_street_number { get; set; }
        public string sims_admission_parent_area_number { get; set; }
        public string sims_admission_parent_summary_address { get; set; }
        public string sims_admission_parent_city { get; set; }
        public string sims_admission_parent_state { get; set; }
        public string sims_admission_parent_country_code { get; set; }
        public string sims_admission_parent_phone { get; set; }
        public string sims_admission_parent_mobile { get; set; }
        public string sims_admission_parent_email { get; set; }
        public string sims_admission_parent_fax { get; set; }
        public string sims_admission_parent_po_box { get; set; }
        public string sims_admission_parent_occupation { get; set; }
        public string sims_admission_parent_company { get; set; }
        public string sims_admission_parent_passport_number { get; set; }
        public string sims_admission_parent_national_id { get; set; }
        public string sims_admission_parent_qualification { get; set; }
        public string sims_admission_parent_company_address { get; set; }
        public string sims_admission_parent_company_phone { get; set; }
        public string sims_admission_ip { get; set; }
        public string sims_admission_dns { get; set; }
        public string sims_admission_relation_with_stud { get; set; }
        public string sims_admission_password { get; set; }
        public string sims_admission_password1 { get; set; }
        public string sims_status { get; set; }

        public string sims_msg_body { get; set; }

        public string sims_msg_signature { get; set; }

        public string sims_msg_subject { get; set; }

        public object OldPasscode { get; set; }

        public object NewPasscode { get; set; }

        public object UserName { get; set; }

        public object school_code { get; set; }

        public object prospect_no { get; set; }
    }


    public class sims_student_cls_status
    {
        public string sims_sr_no { get; set; }
        public string sims_enroll_number { get; set; }
        public string student_name { get; set; }
        public string sims_fee_clr_status { get; set; }
        public string sims_fee_clr_by { get; set; }
        public string sims_fee_clr_date { get; set; }
        public string sims_fee_clr_remark { get; set; }
        public string sims_finn_clr_status { get; set; }
        public string sims_finn_clr_by { get; set; }
        public string sims_finn_clr_date { get; set; }
        public string sims_finn_clr_remark { get; set; }
        public string sims_inv_clr_status { get; set; }
        public string sims_inv_clr_by { get; set; }
        public string sims_inv_clr_date { get; set; }
        public string sims_inv_clr_remark { get; set; }
        public string sims_inci_clr_status { get; set; }
        public string sims_inci_clr_by { get; set; }
        public string sims_inci_clr_date { get; set; }
        public string sims_inci_clr_remark { get; set; }
        public string sims_lib_clr_status { get; set; }
        public string sims_lib_clr_by { get; set; }
        public string sims_lib_clr_date { get; set; }
        public string sims_lib_clr_remark { get; set; }
        public string sims_trans_clr_status { get; set; }
        public string sims_trans_clr_by { get; set; }
        public string sims_trans_clr_date { get; set; }
        public string sims_trans_clr_remark { get; set; }
        public string sims_acad_clr_status { get; set; }
        public string sims_acad_clr_by { get; set; }
        public string sims_acad_clr_date { get; set; }
        public string sims_acad_clr_remark { get; set; }
        public string sims_admin_clr_status { get; set; }
        public string sims_admin_clr_by { get; set; }
        public string sims_admin_clr_date { get; set; }
        public string sims_admin_clr_remark { get; set; }
        public string sims_other1_clr_status { get; set; }
        public string sims_other1_clr_by { get; set; }
        public string sims_other1_clr_date { get; set; }
        public string sims_other1_clr_remark { get; set; }
        public string sims_other2_clr_status { get; set; }
        public string sims_other2_clr_by { get; set; }
        public string sims_other2_clr_date { get; set; }
        public string sims_other2_clr_remark { get; set; }
        public string sims_other3_clr_status { get; set; }
        public string sims_other3_clr_by { get; set; }
        public string sims_other3_clr_date { get; set; }
        public string sims_other3_clr_remark { get; set; }
        public string sims_clr_status { get; set; }

    }
    public class sims_admission_quota_master
    {

        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_year_description { get; set; }
        public string sims_quota_desc { get; set; }
        public string sims_q_strength { get; set; }
        public string sims_q_status { get; set; }
        public string sims_quota_id { get; set; }
        public string opr { get; set; }
    }

    public class sims_admission_quota_user
    {
        public string sims_academic_year_description { get; set; }
        public string sims_q_cur_code { get; set; }
        public string sims_q_user_alias { get; set; }
        public string sims_q_academic_year { get; set; }
        public string sims_q_grade_code { get; set; }
        public string sims_quota_id { get; set; }
        public string sims_q_user_name { get; set; }
        public int sims_q_strength { get; set; }
        public string sims_q_status { get; set; }
        public string sims_grade_name_en { get; set; }
        public string sims_quota_desc { get; set; }
        public string mq_strength { get; set; }


    }
    public class Uccw229
    {
        public string admis_num { get; set; }
        public string sims_criteria_code { get; set; }
        public string sims_criteria_name_en { get; set; }
        public string sims_criteria_type { get; set; }
        public string sims_admission_doc_path { get; set; }
        public bool if_uploaded { get; set; }
        public bool sims_admission_doc_status { get; set; }
        public bool sims_admission_doc_verify { get; set; }
        public bool if_status { get; set; }
        public bool if_verify { get; set; }
        public int count { get; set; }
        public string opr { get; set; }
        public int sims_count { get; set; }


        public string sims_admission_enroll_number { get; set; }
        public string sims_admission_number { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_admission_academic_year { get; set; }
        public string sims_admission_grade_code { get; set; }
        public string sims_admission_section_code { get; set; }
        public string sims_admission_criteria_code { get; set; }
        public string sims_admission_doc_path_new { get; set; }
        public string sims_admission_marks { get; set; }
        public string sims_admission_rating { get; set; }
        public string sims_admission_user_code { get; set; }
    }

    public class SDFU // Student Details File Upload
    {
        public string opr { get; set; }
        public string sims_sr_no { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_file_uploaded_by { get; set; }
        public string sims_file_uploaded_datetime { get; set; }
        public string sims_file_path { get; set; }

        public string sims_admission_application_number { get; set; }
        public string sims_admission_date { get; set; }
        public string sims_admission_cur_code { get; set; }
        public string sims_admission_academic_year { get; set; }
        public string sims_admission_grade_code { get; set; }
        public string sims_admission_tentative_joining_date { get; set; }
        public string sims_admission_passport_first_name_en { get; set; }
        public string sims_admission_passport_middle_name_en { get; set; }
        public string sims_admission_passport_last_name_en { get; set; }
        public string sims_admission_gender { get; set; }
        public string sims_admission_dob { get; set; }
        public string sims_admission_father_first_name { get; set; }
        public string sims_admission_father_middle_name { get; set; }
        public string sims_admission_father_last_name { get; set; }
        public string sims_admission_father_mobile { get; set; }
        public string sims_admission_father_email { get; set; }
        public string sims_admission_mother_first_name { get; set; }
        public string sims_admission_mother_middle_name { get; set; }
        public string sims_admission_mother_last_name { get; set; }
        public string sims_admission_mother_mobile { get; set; }
        public string sims_admission_mother_email { get; set; }
    }

    public class AcesObj // Aces Customer
    {
        public string opr { get; set; }
        public string country_code { get; set; }
        public string state_code { get; set; }
        public string region_code { get; set; }
        public string district_code { get; set; }
        public string city_code { get; set; }
        public string taluka_code { get; set; }
        public string area_code { get; set; }
        public string school_code { get; set; }
        public string school_type { get; set; }
        public string school_name { get; set; }
        public string school_short_name { get; set; }
        public string school_other_name { get; set; }
        public string school_logo { get; set; }
        public string school_address { get; set; }
        public string contact_person { get; set; }
        public string fax_no { get; set; }
        public string tel_no { get; set; }
        public string email { get; set; }
        public string website_url { get; set; }
        public string school_curriculum { get; set; }
        public string agreement_date { get; set; }
        public string subscription_date { get; set; }
        public string subscription_expiry_date { get; set; }
        public string grace_30 { get; set; }
        public string grace_60 { get; set; }
        public string grace_90 { get; set; }
        public string status { get; set; }
        public string time_zone { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class Uccw230
    {
        public string admis_num { get; set; }
        public string sims_admission_criteria_code { get; set; }
        public string sims_criteria_name_en { get; set; }
        public string sims_admission_marks { get; set; }
        public string sims_marks { get; set; }
        public string sims_admission_rating { get; set; }
        public string sims_criteria_type { get; set; }
        public string opr { get; set; }

        public string sims_criteria_status { get; set; }

        public string sims_admission_criteria_section_number { get; set; }

        public string sims_admission_criteria_part_number { get; set; }

        public string sims_mark_assesment_grade_description { get; set; }

        public string sims_admission_user_code { get; set; }

        public string sims_admission_memo_from { get; set; }

        public string sims_admission_memo_to { get; set; }

        public string sims_admission_mutiple { get; set; }

        public string sims_admission_rating1 { get; set; }

        public string sims_mark_assesment_grade_point { get; set; }
    }

    public class enquiry
    {

        public string sname { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
        public string grade { get; set; }
        public string pname { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }

        public string gradeSectionCode { get; set; }

        public string gradeSectionName { get; set; }

        public string academic_year { get; set; }

        public string academic_year_desc { get; set; }

        public object password { get; set; }

        public string parent_id { get; set; }

        public string sibling_id { get; set; }

        public string cur_code { get; set; }

        public string curr_name { get; set; }

        public string nationality_name { get; set; }

        public string nationality_code { get; set; }

        public string referal_code { get; set; }

        public string referal_name { get; set; }

        public object curr_code { get; set; }

        public object transport_status { get; set; }

        public object school_code { get; set; }

        public object pros_number { get; set; }

        public string sims_pros_number { get; set; }

        public string sims_pros_attribute1 { get; set; }

        public string sims_pros_status { get; set; }

        public string sims_academic_year_description { get; set; }

        public string sims_grade_name_en { get; set; }
    }



    public class Uccw034
    {
        public string admis_num { get; set; }
        public string comm_method { get; set; }
        public string comm_date { get; set; }
        public string comm_desc { get; set; }
        public string enq_rem { get; set; }
        public string status { get; set; }
        public string comm_mode_desc { get; set; }
        public string Father_email { get; set; }
        public string Mother_email { get; set; }
        public string Guardian_email { get; set; }

        public string pros_num { get; set; }

        public object opr { get; set; }
    }

    public class Sims028
    {

        public string sims_academic_year { get; set; }
        public string sims_honour_enroll_number { get; set; }
        public string sims_honour_student_first_name { get; set; }
        public string sims_honour_grade { get; set; }
        public string sims_honour_student_middle_name { get; set; }
        public string sims_honour_student_last_name { get; set; }
        public string sims_honour_student_name { get; set; }
        public string sims_honour_code { get; set; }
        public string sims_honour_cur_code { get; set; }
        public string sims_honour_academic_year { get; set; }
        public string sims_student_enroll_number { get; set; }
        public string sims_cur_full_name_en { get; set; }
        public string sims_section_name_en { get; set; }
        public string sims_grade_name_en { get; set; }

        public string sims_cur_code { get; set; }
        public string sims_cur_code_name { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }


        public string sims_student_honour_roll { get; set; }

        public List<subject_grade> subjects { get; set; }
    }

    #region SubjectConfigTermObj
    public class subjectConfigTermObj
    {
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string aca_year { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public List<subjectConfigcategoryObj> category { get; set; }

        public string term_code { get; set; }

        public string sims_report_card_term_code { get; set; }

        public string sims_report_card_term_desc { get; set; }

        public string cur_code { get; set; }

        public string config_code { get; set; }

        public List<subjectConfigTermObj> term { get; set; }
    }



    #endregion

    #region categoryObj
    public class subjectConfigcategoryObj
    {
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string status { get; set; }

        public string sims_report_card_category_code { get; set; }

        public string sims_report_card_category_name { get; set; }
    }

    #endregion

    public class subject_grade
    {
        public string sims_subject_code { get; set; }

        public string sims_subject_name_en { get; set; }
    }

    #region CancelAdmission
    public class Sims010_Auth
    {
        public string total_opeing_cnt { get; set; }
        public string total_upcomeing_cnt { get; set; }
        public string total_ltrissued_cnt { get; set; }
        public string total_penvacncy_cnt { get; set; }

        public string sims_allocation_status { get; set; }
        public string stud_full_name_reg { get; set; }
        public string House { get; set; }
        public string Stud_Summary_Address { get; set; }
        public string Father_email { get; set; }
        public string Mother_email { get; set; }
        public string Guardian_email { get; set; }
        public string FatherNameReg { get; set; }
        public string FatherSummary_Address { get; set; }
        public string FatherName { get; set; }
        public string marketing_description { get; set; }
        public string sims_student_attribute10 { get; set; }
        public string sims_student_attribute1 { get; set; }
        public Sims010_Auth()
        {
            //this.coll = new List<SectionDetails>();
            this.SectionCollection = new List<SectionDetails>();
        }
        public string curr_code { get; set; }
        public string curr_name { get; set; }
        public string academic_year { get; set; }
        public string academic_year_desc { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }

        public string stud_full_name { get; set; }
        public string admission_number { get; set; }
        public string fee_category { get; set; }
        public string admission_date { get; set; }
        public string pros_num { get; set; }
        public string school_code { get; set; }
        public string school_name { get; set; }
        public string term_code { get; set; }
        public string term_name { get; set; }
        public string tent_join_date { get; set; }
        public string gender { get; set; }
        public string gender_code { get; set; }
        public string birth_date { get; set; }

        public string sims_admission_dob { get; set; }
        public string sims_admission_national_id { get; set; }

        public string communication_admi_date { get; set; }
        public string prospect_fee_status { get; set; }

        public string communication_date { get; set; }
        public DateTime communication_date1 { get; set; }

        public string age { get; set; }
        public string sims_pros_national_id { get; set; }
        public string nation { get; set; }
        public string sibling_enroll { get; set; }
        public string empN_code { get; set; }
        public string empl_code { get; set; }
        public string fee_category_desc { get; set; }
        //Collection Variables
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string Total_Strength { get; set; }
        public string Actual_Strength { get; set; }
        public string SecCount { get; set; }
        public string StrengthDet { get; set; }
        public string up_docsstatus { get; set; }//by shweta
        public string up_marksstatus { get; set; }//by shweta



        //private List<SectionDetails> coll;
        public List<SectionDetails> SectionCollection
        {
            get;
            set;
        }

        public string sims_academic_year_description { get; set; }

        public string admission_no_stud_name { get; set; }

        public string sims_admission_status { get; set; }

        public string sims_admission_status_desc { get; set; }

        public string ClassTeacherName { get; set; }

        public string waiting_cnt { get; set; }

        public string rejected_cnt { get; set; }

        public string Completed_cnt { get; set; }

        public string appl_count { get; set; }

        public string sims_msg_type { get; set; }

        public string sims_msg_subject { get; set; }

        public string sims_msg_body { get; set; }

        public string sims_msg_signature { get; set; }

        public string emailid { get; set; }

        public bool chk_email { get; set; }

        public string sims_admission_quota_code { get; set; }

        public string pros_date { get; set; }

        public DateTime pros_date1 { get; set; }


        public string quota_code { get; set; }

        public string path_cnt { get; set; }

        public string criteria_cnt { get; set; }

        public string sims_admission_parent_REG_id { get; set; }

        public string siblingNo_cnt { get; set; }

        public string nationality_code { get; set; }

        public string nationality_name { get; set; }

        public string gender_cnt { get; set; }

        public string empcode_cnt { get; set; }

        public string sims_msg_sr_no { get; set; }

        public string sims_admission_recommendation { get; set; }
        public string result { get; set; }

        public string TCApplied { get; set; }

        public string waiting_paid { get; set; }

        public string waiting_unpaid { get; set; }

        public string feepaid { get; set; }

        public string sims_house_code { get; set; }

        public string sims_house_name { get; set; }

        public string sims_admission_management_quota { get; set; }

        public string sims_quota_id { get; set; }

        public string selected_cnt { get; set; }

        public string remark { get; set; }

        public string admission_status { get; set; }

        public string admission_status_code { get; set; }

        public string sims_appl_form_field_value1 { get; set; }

        public string comn_user_role_id { get; set; }

        public string sims_student_ea_number { get; set; }
        public string subject_code { get; set; }
        public string subject_name { get; set; }

        public string father_salary { get; set; }

        public string mother_salary { get; set; }

        public string transport_desc { get; set; }

        public string father_occupation { get; set; }

        public string mother_occupation { get; set; }

        public string enroll_no { get; set; }

        public string father_mobile { get; set; }

        public string mother_mobile { get; set; }

        //public string father_email { get; set; }

        //public string mother_email { get; set; }

        public string sims_fee_month_code { get; set; }

        public string sims_fee_month_name { get; set; }

        public string sims_student_img { get; set; }

        public string sims_father_img { get; set; }

        public string sims_mother_img { get; set; }

        public string admenroll_cnt { get; set; }

        public string sen_cnt { get; set; }

        public string actual_marks { get; set; }

        public string max_marks { get; set; }

        public string sims_pros_attribute1 { get; set; }

        public string sims_follow_up_date { get; set; }

        public DateTime sims_follow_up_date1 { get; set; }



        public int pros_num1 { get; set; }

        public string sims_pros_marketing_description { get; set; }

        public int admission_number1 { get; set; }

        public DateTime communication_admi_date1 { get; set; }

        public DateTime admission_date1 { get; set; }

        public int admission_number_new { get; set; }

        public string sims_ename { get; set; }

        public string first_name { get; set; }

        public string middle_name { get; set; }

        public string last_name { get; set; }

        public string sims_admission_completed_status { get; set; }

        public string direct_cnt { get; set; }

        public string sims_admission_transport_address { get; set; }

        public string sims_admission_total_memorization { get; set; }

        public string sims_admission_exactly_total_memorization { get; set; }

        public string sims_admission_reading_quran { get; set; }

        public string sims_admission_grade { get; set; }

        public string sims_admission_grade_levels { get; set; }

        public string sims_admission_levels { get; set; }

        public string sims_admission_total_memorization_to { get; set; }

        public string sims_admission_exactly_total_memorization_to { get; set; }

        public string sims_admission_attend_any_quran_center_before { get; set; }

        public string sims_admission_parent_id { get; set; }

        public string sims_admission_parent_nm { get; set; }

        public string pref_date { get; set; }

        public DateTime pref_date1 { get; set; }
        public string current_fyschool_name { get; set; }

        public string sims_admission_fycenter_name { get; set; }

        public string sims_admission_fyboardname { get; set; }

        public string sims_admission_fypassingyear_exam { get; set; }

        public string sims_admission_fyseat_no { get; set; }

        public string sims_fyadmission_no_of_attempts_sem1 { get; set; }

        public string sims_fyadmission_marks_obtained_sem1 { get; set; }

        public string sims_fyadmission_marks_out_of_Sem1 { get; set; }

        public string sims_fyadmission_percentage_obtained_sem1 { get; set; }

        public string sims_fyadmission_no_of_attempts_sem2 { get; set; }

        public string sims_fyadmission_marks_obtained_sem2 { get; set; }

        public string sims_fyadmission_marks_out_of_Sem2 { get; set; }

        public string sims_fyadmission_percentage_obtained_sem2 { get; set; }

        public string current_syschool_name { get; set; }

        public string sims_admission_sycenter_name { get; set; }

        public string sims_syadmission_percentage_obtained_sem4 { get; set; }

        public string sims_syadmission_marks_out_of_sem4 { get; set; }

        public string sims_syadmission_marks_obtained_sem4 { get; set; }

        public string sims_syadmission_no_of_attempts_sem4 { get; set; }

        public string sims_syadmission_percentage_obtained_sem3 { get; set; }

        public string sims_syadmission_marks_out_of_Sem3 { get; set; }

        public string sims_syadmission_marks_obtained_sem3 { get; set; }

        public string sims_syadmission_no_of_attempts_sem3 { get; set; }

        public string sims_admission_syseat_no { get; set; }

        public string sims_admission_sypassingyear_exam { get; set; }

        public string sims_admission_syboardname { get; set; }



        public string sims_admission_boardname { get; set; }

        public string sims_admission_center_name { get; set; }

        public string sims_admission_passingyear_exam { get; set; }

        public string sims_admission_seat_no { get; set; }

        public string sims_admission_no_of_attempts { get; set; }

        public string sims_admission_marks_obtained { get; set; }

        public string sims_admission_marks_out_of { get; set; }

        public string sims_admission_percentage_obtained { get; set; }

        public string current_school_name { get; set; }

        public string sims_student_mobile { get; set; }
    }

    public class param
    {
        public string curr_code { get; set; }
        public string AcadmicYear { get; set; }
        public string gradeCode { get; set; }
        public string admission_status { get; set; }
        public string gender_code { get; set; }
        public string nationality_code { get; set; }
        public string employee_code { get; set; }
        public string sibling_enroll { get; set; }
        public string sectionCode { get; set; }
        public string adm_no { get; set; }
        public string adm_name { get; set; }
    }

    public class SectionDetails
    {
        public string CurriculamCode { get; set; }
        public string CurriculamName { get; set; }
        public string AcademicYear { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string SectionCode { get; set; }
        public string SectionName { get; set; }
        public string Gender { get; set; }
        public string TotalStrength { get; set; }
        public string ActualStrength { get; set; }
        public string SecCount { get; set; }
    }

    #region Cancel Admission

    public class CancelAdmission
    {
        public string Enroll { get; set; }
        public Sims010_Auth StudentDet { get; set; }
        public List<LibDets> LibDet { get; set; }
        public List<IncidenceDets> IncidenceDet { get; set; }
        public List<InvsDets> InvsDet { get; set; }
        public List<FeeDets> FeeDet { get; set; }
        public List<ExamDet> ExamDet { get; set; }
    }

    public class LibDets
    {
        public string BookName { get; set; }
        public string Quantity { get; set; }
        public string ExpctdReturnDate { get; set; }
        public string ItemRate { get; set; }
        public string Status { get; set; }
    }

    public class IncidenceDets
    {
        public string incidence { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Building { get; set; }
        public string ActionDesc { get; set; }
        public string Warning { get; set; }
        public string Status { get; set; }
    }

    public class FeeDets
    {
        public string FeeDescription { get; set; }
        public string TotalAmmount { get; set; }
        public string PaidAmmount { get; set; }
        public string Concession { get; set; }
        public string RemainingAmmount { get; set; }
        public string FeeCode { get; set; }
        public bool Ignored { get; set; }
    }

    public class InvsDets
    {
        public string ItemName { get; set; }
        public string ItemQty { get; set; }
        public string Ammount { get; set; }
    }

    public class ExamDet
    {
        public string ExamName { get; set; }
        public string BoardName { get; set; }
        public string RegNumber { get; set; }
        public string Status { get; set; }
    }

    #endregion
    #endregion

    public class subject_lst
    {

        public string sims_subject_code { get; set; }

        public string sims_student_attribute3 { get; set; }

        public bool sims_status { get; set; }

        public string sims_subject_name_en { get; set; }

        public string sims_compulsory_subject { get; set; }

        public string enroll_number { get; set; }
    }

    public class smain
    {
        public List<subject_lst> sublist { get; set; }

        public string enroll_number { get; set; }

        public string student_name { get; set; }

        public string sims_subject_code { get; set; }

        public string sims_subject_name_en { get; set; }
    }


    public class Sims010_Edit
    {
        public List<string> SubjectDet1 { get; set; }
        public List<subject_lst> sublist { get; set; }
        public string sims_subject_group_code { get; set; }
        public string sims_subject_pref { get; set; }
        public string sims_language_desc_remark { get; set; }
        public string sims_birth_date_from { get; set; }
        public string sims_birth_date_to { get; set; }
        #region student basic
        public List<string> getstatus { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string curr_code { get; set; }
        public string curr_name { get; set; }
        public string academic_year { get; set; }
        public string grade_code { get; set; }
        public string grade_name { get; set; }
        public string section_code { get; set; }
        public string section_name { get; set; }
        public string term_code { get; set; }
        public string term_name { get; set; }
        public string appl_num { get; set; }
        public string pros_appl_num { get; set; }
        public string pros_num { get; set; }
        public string comm_date { get; set; }
        public string birth_country { get; set; }
        public string birth_country_code { get; set; }
        public string ethinicity_code { get; set; }
        public string ethinicity { get; set; }
        public string stud_full_name { get; set; }
        public string admission_number { get; set; }
        public string fee_category_desc { get; set; }
        public string fee_category_code { get; set; }
        public string admission_date { get; set; }
        public string school_code { get; set; }
        public string school_name { get; set; }
        public string tent_join_date { get; set; }
        public string gender_code { get; set; }
        public string gender_desc { get; set; }
        public string birth_date { get; set; }
        public string nationality { get; set; }
        public string nationality_code { get; set; }
        public string family_name { get; set; }
        public string family_name_ot { get; set; }
        public string first_name_ot { get; set; }
        public string midd_name_ot { get; set; }
        public string last_name_ot { get; set; }
        public string nicke_name { get; set; }
        public string religion_code { get; set; }
        public string religion_desc { get; set; }
        public string enroll { get; set; }
        #endregion

        #region passport details
        public string passport_num { get; set; }
        public string passport_issue_date { get; set; }
        public string passport_issue_auth { get; set; }
        public string passport_issue_place { get; set; }
        public string passport_expiry { get; set; }

        #endregion

        #region Visa Details
        public string visa_number { get; set; }
        public string visa_issue_date { get; set; }
        public string visa_expiry_date { get; set; }
        public string visa_issuing_place { get; set; }
        public string visa_issuing_authority { get; set; }
        public string visa_type { get; set; }
        public string national_id { get; set; }
        public string national_id_expiry_date { get; set; }
        public string national_id_issue_date { get; set; }
        #endregion

        #region Language details
        public string main_language_code { get; set; }
        public string main_language_desc { get; set; }
        public string main_language_r_code { get; set; }
        public string main_language_r { get; set; }
        public string main_language_w_code { get; set; }
        public string main_language_w { get; set; }
        public string main_language_s_code { get; set; }
        public string main_language_s { get; set; }
        public string other_language_code { get; set; }
        public string other_language { get; set; }
        public string motherTounge_language_code { get; set; }
        public string motherTounge_language { get; set; }
        #endregion

        #region sibling details

        public string sibling_name { get; set; }
        public bool sibling_status { get; set; }
        public string sibling_enroll { get; set; }
        public string parent_id { get; set; }
        public string sibling_dob { get; set; }
        public string sibling_school_code { get; set; }
        public string sibling_school_name { get; set; }
        #endregion

        #region employee details
        public bool employee_type { get; set; }
        public string employee_school_code { get; set; }
        public string employee_school_name { get; set; }
        public string employee_comp_code { get; set; }
        public string employee_code { get; set; }
        #endregion

        #region Contact pref
        public bool primary_contact_code { get; set; }
        public string primary_contact_desc { get; set; }
        public string primary_contact_pref_code { get; set; }
        public string primary_contact_pref_desc { get; set; }
        public string fee_payment_contact_pref_code { get; set; }
        public string fee_payment_contact_pref_desc { get; set; }
        public bool transport_status { get; set; }
        public string transport_desc { get; set; }
        #endregion

        #region father details
        public string father_salutation_code { get; set; }
        public string father_salutation_desc { get; set; }
        public string father_first_name { get; set; }
        public string father_middle_name { get; set; }
        public string father_last_name { get; set; }
        public string father_family_name { get; set; }
        public string father_name_ot { get; set; }
        public string father_nationality1_code { get; set; }
        public string father_nationality1_desc { get; set; }
        public string father_nationality2_code { get; set; }
        public string father_nationality2_desc { get; set; }
        public string father_appartment_number { get; set; }
        public string father_building_number { get; set; }
        public string father_street_number { get; set; }
        public string father_area_number { get; set; }
        public string father_summary_address { get; set; }
        public string father_city { get; set; }
        public string father_state { get; set; }
        public string father_country { get; set; }
        public string father_country_code { get; set; }
        public string father_phone { get; set; }
        public string father_mobile { get; set; }
        public string father_email { get; set; }
        public string father_fax { get; set; }
        public string father_po_box { get; set; }
        public string father_occupation { get; set; }
        public string father_company { get; set; }
        public string father_passport_number { get; set; }
        public string sims_parent_father_img { get; set; }

        #endregion

        #region Mother Details
        public string mother_salutation_code { get; set; }
        public string mother_salutation_desc { get; set; }
        public string mother_first_name { get; set; }
        public string mother_middle_name { get; set; }
        public string mother_last_name { get; set; }
        public string mother_family_name { get; set; }
        public string mother_name_ot { get; set; }
        public string mother_nationality1_code { get; set; }
        public string mother_nationality1_desc { get; set; }
        public string mother_nationality2_code { get; set; }
        public string mother_nationality2_desc { get; set; }
        public string mother_appartment_number { get; set; }
        public string mother_building_number { get; set; }
        public string mother_street_number { get; set; }
        public string mother_area_number { get; set; }
        public string mother_summary_address { get; set; }
        public string mother_city { get; set; }
        public string mother_state { get; set; }
        public string mother_country { get; set; }
        public string mother_country_code { get; set; }
        public string mother_phone { get; set; }
        public string mother_mobile { get; set; }
        public string mother_email { get; set; }
        public string mother_fax { get; set; }
        public string mother_po_box { get; set; }
        public string mother_occupation { get; set; }
        public string mother_company { get; set; }
        public string mother_passport_number { get; set; }
        public string sims_parent_mother_img { get; set; }

        #endregion

        #region guardian details
        public string guardian_salutation_code { get; set; }
        public string guardian_salutation_desc { get; set; }
        public string guardian_first_name { get; set; }
        public string guardian_middle_name { get; set; }
        public string guardian_last_name { get; set; }
        public string guardian_family_name { get; set; }
        public string guardian_name_ot { get; set; }
        public string guardian_nationality1_code { get; set; }
        public string guardian_nationality1_desc { get; set; }
        public string guardian_nationality2_code { get; set; }
        public string guardian_nationality2_desc { get; set; }
        public string guardian_appartment_number { get; set; }
        public string guardian_building_number { get; set; }
        public string guardian_street_number { get; set; }
        public string guardian_area_number { get; set; }
        public string guardian_summary_address { get; set; }
        public string guardian_city { get; set; }
        public string guardian_state { get; set; }
        public string guardian_country { get; set; }
        public string guardian_country_code { get; set; }
        public string guardian_phone { get; set; }
        public string guardian_mobile { get; set; }
        public string guardian_email { get; set; }
        public string guardian_fax { get; set; }
        public string guardian_po_box { get; set; }
        public string guardian_occupation { get; set; }
        public string guardian_company { get; set; }
        public string guardian_passport_number { get; set; }
        public string sims_parent_guardian_img { get; set; }
        #endregion

        #region school details
        public string current_school_name { get; set; }
        public string current_school_head_teacher { get; set; }
        public string current_school_enroll_number { get; set; }
        public string current_school_grade { get; set; }
        public string current_school_cur { get; set; }
        public string current_school_language { get; set; }
        public string current_school_address { get; set; }
        public string current_school_city { get; set; }
        public string current_school_country { get; set; }
        public string current_school_country_code { get; set; }
        public string current_school_phone { get; set; }
        public string current_school_fax { get; set; }
        public string current_school_from_date { get; set; }
        public string current_school_to_date { get; set; }
        public bool current_school_status { get; set; }

        #endregion

        #region health details
        public string health_card_number { get; set; }
        public string health_card_issue_date { get; set; }
        public string health_card_expiry_date { get; set; }
        public string health_card_issuing_authority { get; set; }
        public string blood_group_code { get; set; }
        public string blood_group_desc { get; set; }
        public bool health_restriction_status { get; set; }
        public string health_restriction_desc { get; set; }
        public bool disability_status { get; set; }
        public string disability_desc { get; set; }
        public bool medication_status { get; set; }
        public string medication_desc { get; set; }
        public bool health_other_status { get; set; }
        public string health_other_desc { get; set; }
        public bool health_hearing_status { get; set; }
        public string health_hearing_desc { get; set; }
        public bool health_vision_status { get; set; }
        public string health_vision_desc { get; set; }

        #endregion

        #region other
        public bool language_support_status { get; set; }
        public string language_support_desc { get; set; }
        public bool behaviour_status { get; set; }
        public string behaviour_desc { get; set; }
        public bool gifted_status { get; set; }
        public string gifted_desc { get; set; }
        public bool music_status { get; set; }
        public string music_desc { get; set; }
        public bool sports_status { get; set; }
        public string sports_desc { get; set; }
        public bool declaration_status { get; set; }
        public bool fees_paid_status { get; set; }
        public string status { get; set; }
        public string ip { get; set; }
        public string dns { get; set; }
        public string user_code { get; set; }
        public bool marketing_code { get; set; }
        public string marketing_description { get; set; }
        public bool parent_status_code { get; set; }
        public string legal_custody_code { get; set; }
        public string legal_custody_name { get; set; }

        //for the 
        public string Aycur { get; set; }

        public string GrCur { get; set; }
        public string GrAca { get; set; }

        public string SecCur { get; set; }
        public string SecAcy { get; set; }
        public string SecGr { get; set; }

        public string sims_parent_father_passport_issue_date { get; set; }
        public string sims_parent_father_passport_expiry_date { get; set; }

        public string sims_parent_father_national_id_issue_date { get; set; }
        public string sims_parent_father_national_id_expiry_date { get; set; }

        public string sims_parent_mother_passport_issue_date { get; set; }
        public string sims_parent_mother_passport_expiry_date { get; set; }

        public string sims_parent_mother_national_id_issue_date { get; set; }
        public string sims_parent_mother_national_id_expiry_date { get; set; }

        public string sims_parent_gua_passport_issue_date { get; set; }
        public string sims_parent_gua_passport_expiry_date { get; set; }
        public string sims_parent_gua_national_id { get; set; }
        public string sims_parent_gua_national_id_issue_date { get; set; }
        public string sims_parent_gua_national_id_expiry_date { get; set; }


        public string sims_parent_number { get; set; }
        public string sims_parent_login_code { get; set; }
        public string sims_parent_father_building_number { get; set; }
        public string sims_parent_father_salutation_code { get; set; }
        public string sims_parent_father_salutation_code1 { get; set; }
        public string sims_parent_father_first_name { get; set; }
        public string sims_parent_father_middle_name { get; set; }
        public string sims_parent_father_last_name { get; set; }
        public string sims_parent_father_family_name { get; set; }
        public string sims_parent_father_name_ot { get; set; }
        public string sims_parent_father_nationality1_code { get; set; }
        public string sims_parent_father_nationality1_code1 { get; set; }
        public string sims_parent_father_nationality2_code1 { get; set; }
        public string sims_parent_father_nationality2_code { get; set; }
        public string sims_parent_father_appartment_number { get; set; }
        public string sims_parent_father_street_number { get; set; }
        public string sims_parent_father_area_number { get; set; }
        public string sims_parent_father_summary_address { get; set; }
        public string sims_parent_father_city1 { get; set; }
        public string sims_parent_father_state1 { get; set; }
        public string sims_parent_father_country_code1 { get; set; }
        public string sims_parent_father_phone { get; set; }
        public string sims_parent_father_mobile { get; set; }
        public string sims_parent_father_email { get; set; }
        public string sims_parent_father_fax { get; set; }
        public string sims_parent_father_po_box { get; set; }
        public string sims_parent_father_occupation { get; set; }
        public string sims_parent_father_occupation_local_language { get; set; }
        public string sims_parent_father_occupation_location_local_language { get; set; }
        public string sims_parent_father_passport_number { get; set; }
        public string sims_parent_mother_salutation_code1 { get; set; }
        public string sims_parent_mother_salutation_code { get; set; }
        public string sims_parent_mother_first_name { get; set; }
        public string sims_parent_mother_middle_name { get; set; }
        public string sims_parent_mother_last_name { get; set; }
        public string sims_parent_mother_family_name { get; set; }
        public string sims_parent_mother_name_ot { get; set; }
        public string sims_parent_mother_nationality1_code1 { get; set; }
        public string sims_parent_mother_appartment_number { get; set; }
        public string sims_parent_mother_nationality2_code1 { get; set; }
        public string sims_parent_mother_building_number { get; set; }
        public string sims_parent_mother_area_number { get; set; }
        public string sims_parent_mother_street_number { get; set; }
        public string sims_parent_mother_summary_address { get; set; }
        public string sims_parent_mother_city1 { get; set; }
        public string sims_parent_mother_state1 { get; set; }
        public string sims_parent_mother_phone { get; set; }
        public string sims_parent_mother_country_code1 { get; set; }
        public string sims_parent_mother_mobile { get; set; }
        public string sims_parent_mother_email { get; set; }
        public string sims_parent_mother_fax { get; set; }
        public string sims_parent_mother_po_box { get; set; }
        public string sims_parent_mother_occupation { get; set; }
        public string sims_parent_mother_occupation_local_language { get; set; }
        public string sims_parent_mother_company { get; set; }
        public string sims_parent_mother_passport_number { get; set; }
        public string sims_parent_mother_nationality1_code { get; set; }
        public string sims_parent_mother_nationality2_code { get; set; }
        public string sims_parent_guardian_salutation_code1 { get; set; }
        public string sims_parent_guardian_salutation_code { get; set; }
        public string sims_parent_guardian_first_name { get; set; }
        public string sims_parent_guardian_middle_name { get; set; }
        public string sims_parent_guardian_last_name { get; set; }
        public string sims_parent_guardian_family_name { get; set; }
        public string sims_parent_guardian_nationality1_code1 { get; set; }
        public string sims_parent_guardian_name_ot { get; set; }
        public string sims_parent_guardian_nationality1_code { get; set; }
        public string sims_parent_guardian_nationality2_code { get; set; }
        public string sims_parent_guardian_nationality2_code1 { get; set; }
        public string sims_parent_guardian_appartment_number { get; set; }
        public string sims_parent_guardian_building_number { get; set; }
        public string sims_parent_guardian_street_number { get; set; }
        public string sims_parent_guardian_summary_address { get; set; }
        public string sims_parent_guardian_area_number { get; set; }
        public string sims_parent_guardian_city1 { get; set; }
        public string sims_parent_guardian_state1 { get; set; }
        public string sims_parent_guardian_country_code1 { get; set; }
        public string sims_parent_guardian_phone { get; set; }
        public string sims_parent_guardian_mobile { get; set; }
        public string sims_parent_guardian_email { get; set; }
        public string sims_parent_guardian_fax { get; set; }
        public string sims_parent_guardian_po_box { get; set; }
        public string sims_parent_guardian_occupation { get; set; }
        public string sims_parent_guardian_occupation_local_language { get; set; }
        public string sims_parent_guardian_occupation_location_local_language { get; set; }
        public string sims_parent_guardian_company { get; set; }
        public string sims_parent_guardian_relationship_code1 { get; set; }
        public string sims_parent_guardian_passport_number { get; set; }
        public string sims_parent_is_employment_status { get; set; }
        public string sims_parent_is_employement_comp_code1 { get; set; }
        public string sims_parent_is_employment_number { get; set; }
        public string sims_parent_father_national_id__expiry_date { get; set; }
        public string sims_parent_guardian_passport_issue_date { get; set; }
        public string sims_parent_guardian_passport_expiry_date2 { get; set; }
        public string sims_parent_guardian_national_id_issue_date { get; set; }
        public string sims_parent_guardian_national_id_expiry_date { get; set; }


        #endregion

        public string username { get; set; }

        public string academic_year_desc { get; set; }

        public string termCur { get; set; }

        public string termAy { get; set; }

        public string strMessage { get; set; }

        public bool chkstatus { get; set; }

        public string sims_student_enroll_number { get; set; }

        public string opr { get; set; }

        public string sims_student_attribute1 { get; set; }

        public string sims_student_attribute2 { get; set; }

        public string sims_student_attribute3 { get; set; }

        public string sims_student_attribute4 { get; set; }

        public string sims_student_attribute5 { get; set; }

        public string sims_student_attribute8 { get; set; }

        public string sims_student_attribute6 { get; set; }

        public string sims_student_attribute7 { get; set; }

        public string sims_student_attribute10 { get; set; }

        public string sims_student_attribute12 { get; set; }

        public string sims_student_attribute11 { get; set; }

        public object sims_student_attribute9 { get; set; }

        public string sims_student_health_respiratory_status { get; set; }

        public string sims_student_health_respiratory_desc { get; set; }

        public string sims_student_health_hay_fever_desc { get; set; }

        public string sims_student_health_epilepsy_desc { get; set; }

        public string sims_student_health_hay_fever_status { get; set; }

        public string sims_student_health_skin_status { get; set; }

        public string sims_student_health_skin_desc { get; set; }

        public string sims_student_health_diabetes_status { get; set; }

        public string sims_student_health_diabetes_desc { get; set; }

        public string sims_student_health_surgery_status { get; set; }

        public string sims_student_health_surgery_desc { get; set; }

        public string sims_student_health_epilepsy_status { get; set; }

        public string sims_student_emergency_contact_number1 { get; set; }

        public string sims_student_emergency_contact_name1 { get; set; }

        public string sims_admission_father_national_id { get; set; }

        public string sims_parent_father_national_id { get; set; }

        public string sims_parent_mother_national_id { get; set; }

        public string sims_parent_guardian_national_id { get; set; }

        public string student_pass { get; set; }

        public string sims_msg_body { get; set; }

        public string sims_msg_signature { get; set; }

        public string sims_msg_subject { get; set; }

        public string visa_type_desc { get; set; }

        public string visa_type_code { get; set; }

        public string sims_appl_parameter_cat_code { get; set; }
        public string sims_appl_form_field_value1_cat_desc { get; set; }
        public string sims_appl_parameter_marital_statuscode { get; set; }

        public string sims_appl_form_field_value1_marital_statusdesc { get; set; }

        public string sims_appl_parameter_castCatcode { get; set; }
        public string sims_appl_form_field_value1_castcatdesc { get; set; }
        public string sims_city_code { get; set; }
        public string sims_city_state_code { get; set; }
        public string sims_city_name_en { get; set; }
        public string sims_state_code { get; set; }

        public string guardian_relationship_code { get; set; }

        public string reason { get; set; }

        public string father_family_name_ot { get; set; }

        public string sims_admission_mother_national_id { get; set; }

        public string sims_admission_guardian_national_id { get; set; }

        public string section_strength { get; set; }

        public string female_strength { get; set; }

        public string male_strength { get; set; }

        public string current_strength { get; set; }

        public string secgender { get; set; }

        public string sims_admission_quota_code { get; set; }

        public string quota_code { get; set; }

        public string cur_code { get; set; }

        public string cancel_date { get; set; }

        public string sims_admission_parent_REG_id { get; set; }

        public string sims_pros_organizational_skills { get; set; }

        public string sims_pros_social_skills { get; set; }

        public string sims_admission_recommendation { get; set; }

        public string sims_national_id_code { get; set; }

        public string sims_national_id_name { get; set; }

        public string sims_fee_month_code { get; set; }

        public string sims_fee_month_name { get; set; }

        public string houseCur { get; set; }

        public string houseAca { get; set; }

        public string sims_house_code { get; set; }

        public string sims_house_name { get; set; }

        public string sec_count { get; set; }

        public string acdemic_year_status { get; set; }

        public object sims_student_ea_number { get; set; }

        public object sims_subject_code { get; set; }

        public string father_salary { get; set; }

        public string mother_salary { get; set; }

        public string sims_student_img { get; set; }

        public string sims_father_img { get; set; }

        public string sims_mother_img { get; set; }

        public string stream_code { get; set; }

        public string stream_desc { get; set; }

        public string sims_subject_name { get; set; }

        public string sims_subject_second_lang_code { get; set; }

        public string sims_subject_third_lang_code { get; set; }

        public string sims_admission_second_lang_code { get; set; }

        public string sims_admission_second_lang_name { get; set; }

        public string comn_seq { get; set; }

        public string subject_code { get; set; }

        public string subject_name { get; set; }

        public bool sims_admission_learning_therapy_status { get; set; }

        public string sims_admission_learning_therapy_desc { get; set; }

        public string sims_admission_special_education_desc { get; set; }

        public bool sims_admission_special_education_status { get; set; }

        public bool sims_admission_falled_grade_status { get; set; }

        public string sims_admission_falled_grade_desc { get; set; }

        public bool sims_admission_communication_status { get; set; }

        public string sims_admission_communication_desc { get; set; }

        public string sims_admission_specialAct_desc { get; set; }

        public bool sims_admission_specialAct_status { get; set; }

        public object sims_admission_third_lang_code { get; set; }

        public object sims_admission_subject_code { get; set; }

        public string sims_admission_third_lang_name { get; set; }

        public string home_phone { get; set; }
        public string home_landmark { get; set; }
        public string home_po_box { get; set; }
        public string home_summary_address { get; set; }
        public string home_country_code { get; set; }
        public string home_state { get; set; }
        public string home_city { get; set; }
        public string home_area_number { get; set; }
        public string home_street_number { get; set; }
        public string home_building_number { get; set; }
        public string home_appartment_number { get; set; }
        public string home_city_code { get; set; }
        public string home_state_code { get; set; }
        public string home_country_isdcode { get; set; }

        public string enroll_number { get; set; }

        public string student_name { get; set; }

        public string sims_cur_code { get; set; }

        public string sims_academic_year { get; set; }

        public string sims_sims_grades { get; set; }

        public string sims_sims_section { get; set; }

        public string sims_enrollment_number { get; set; }

        public string status_code { get; set; }

        public string survey_status_code { get; set; }

        public string survey_status { get; set; }

        public string parent_comn_seq { get; set; }

        public string sims_language_code { get; set; }

        public string sims_language_desc { get; set; }

        public string sims_elective_subject { get; set; }

        public string sims_compulsory_subject { get; set; }

        public string sims_appl_parameter_sch { get; set; }

        public string sims_appl_form_field_value1_sch { get; set; }

        public string mailmsg_subject { get; set; }

        public string mailmsg_body { get; set; }

        public string category_code { get; set; }

        public string category_desc { get; set; }

        public string sims_student_category { get; set; }

        public object sims_admission_parent_first_name { get; set; }

        public object prospect_no { get; set; }

        public object sims_admission_parent_national_id { get; set; }

        public bool sen_disable { get; set; }

        public string passport_expiry_n { get; set; }

        public string sims_student_panNo { get; set; }

        public string sims_student_voterId { get; set; }

        public string gender { get; set; }

        public string fee_category { get; set; }

        public string feepaid { get; set; }

        public string sims_admission_dob { get; set; }

        public string sims_admission_national_id { get; set; }

        public string communication_admi_date { get; set; }

        public string prospect_fee_status { get; set; }

        public string result { get; set; }

        public string nation { get; set; }

        public string empN_code { get; set; }

        public string empl_code { get; set; }

        public string FatherName { get; set; }

        public string criteria_cnt { get; set; }

        public string path_cnt { get; set; }

        public string sims_quota_id { get; set; }

        public string sims_admission_management_quota { get; set; }

        public string admission_status_code { get; set; }

        public string admission_status { get; set; }

        public string remark { get; set; }

        public string up_docsstatus { get; set; }

        public string up_marksstatus { get; set; }

        public string enroll_no { get; set; }

        public string actual_marks { get; set; }

        public string max_marks { get; set; }

        public string sims_admission_completed_status { get; set; }

        public string sims_admission_passport_full_name_en { get; set; }


        public string current_fyschool_name { get; set; }

        public string sims_admission_fycenter_name { get; set; }

        public string sims_admission_fyboardname { get; set; }

        public string sims_admission_fypassingyear_exam { get; set; }

        public string sims_admission_fyseat_no { get; set; }

        public string sims_fyadmission_no_of_attempts_sem1 { get; set; }

        public string sims_fyadmission_marks_obtained_sem1 { get; set; }

        public string sims_fyadmission_marks_out_of_Sem1 { get; set; }

        public string sims_fyadmission_percentage_obtained_sem1 { get; set; }

        public string sims_fyadmission_no_of_attempts_sem2 { get; set; }

        public string sims_fyadmission_marks_obtained_sem2 { get; set; }

        public string sims_fyadmission_marks_out_of_Sem2 { get; set; }

        public string sims_fyadmission_percentage_obtained_sem2 { get; set; }

        public string current_syschool_name { get; set; }

        public string sims_admission_sycenter_name { get; set; }

        public string sims_syadmission_percentage_obtained_sem4 { get; set; }

        public string sims_syadmission_marks_out_of_sem4 { get; set; }

        public string sims_syadmission_marks_obtained_sem4 { get; set; }

        public string sims_syadmission_no_of_attempts_sem4 { get; set; }

        public string sims_syadmission_percentage_obtained_sem3 { get; set; }

        public string sims_syadmission_marks_out_of_Sem3 { get; set; }

        public string sims_syadmission_marks_obtained_sem3 { get; set; }

        public string sims_syadmission_no_of_attempts_sem3 { get; set; }

        public string sims_admission_syseat_no { get; set; }

        public string sims_admission_sypassingyear_exam { get; set; }

        public string sims_admission_syboardname { get; set; }



        public string sims_admission_boardname { get; set; }

        public string sims_admission_center_name { get; set; }

        public string sims_admission_passingyear_exam { get; set; }

        public string sims_admission_seat_no { get; set; }

        public string sims_admission_no_of_attempts { get; set; }

        public string sims_admission_marks_obtained { get; set; }

        public string sims_admission_marks_out_of { get; set; }

        public string sims_admission_percentage_obtained { get; set; }

        public string sims_subject_name_en { get; set; }

       // public string sims_state_code { get; set; }

        public string sims_state_name_en { get; set; }
    }

    public class subjectDet1
    {
        public string sims_subject_code { get; set; }
    }

    public class AdmQuotaDet
    {
        public bool isChecked { get; set; }
        public int sr_no { get; set; }
        public string opr { get; set; }
        public string cur_Code { get; set; }
        public string cur_Name { get; set; }
        public string academic_year { get; set; }
        public string academic_year_Desc { get; set; }
        public string grade_code { get; set; }
        public string grade_Name { get; set; }
        public string desg_code { get; set; }
        public string desg_desc { get; set; }
        public string QuotaDesc { get; set; }
        public int strength { get; set; }
        public string status { get; set; }
    }

    public class recu
    {

        public string em_applicant_id { get; set; }

        public string pays_vacancy_roles { get; set; }

        public string dg_desc { get; set; }

        public string sims_nationality_name_en { get; set; }

        public string codp_dept_name { get; set; }

        public string ename { get; set; }

        public string em_date_of_birth { get; set; }

        public string em_sex { get; set; }

        public string em_mobile { get; set; }

        public string em_email { get; set; }

        public string em_application_status { get; set; }

        public string em_application_date { get; set; }

        public DateTime em_application_date1 { get; set; }
    }

    //public class part_Section
    //{


    //    public string sims_admission_criteria_part_number { get; set; }

    //    public string sims_admission_criteria_part_Name { get; set; }

    //    public string sims_admission_criteria_section_number { get; set; }

    //    public string sims_admission_criteria_section_Name { get; set; }
    //}


    //public class part_Section {

    //    public string sims_admission_criteria_part_number { get; set; }

    //    public string sims_admission_criteria_part_Name { get; set; }

    //    public string sims_admission_criteria_section_number { get; set; }

    //    public string sims_admission_criteria_section_Name { get; set; }
    //}
    public class reg_dash
    {

        public string sims_assesment_date { get; set; }

        public string event_nm { get; set; }

        public string sims_assesment_time { get; set; }

        public string em_name { get; set; }

        public string sims_admission_no { get; set; }

        public string std_name { get; set; }

        public string sims_admission_comm_desc { get; set; }

        public string sims_admission_comm_admission_number { get; set; }

        public string sims_admission_comm_date { get; set; }

        public string sims_comm_message_feedback { get; set; }

        public string sims_comm_message_remark { get; set; }

        public string sims_comm_user_by { get; set; }

        public string sims_comm_date { get; set; }

        public string sims_follow_up_date { get; set; }
    }

    public class reregistration
    {

        public string sims_parent_login_code { get; set; }

        public int sims_parent_login_code1 { get; set; }

        public string sims_student_enroll_number { get; set; }

        public string std_status { get; set; }

        public string std_communication_date { get; set; }

        public string std_follow_date { get; set; }

        public string std_assigned_employee { get; set; }

        public string pname { get; set; }

        public string std_name { get; set; }

        public string sims_student_gender { get; set; }

        public string std_next_grade { get; set; }

        public string std_willing { get; set; }

        public string sims_parent_father_mobile { get; set; }

        public string sims_parent_father_email { get; set; }

        public string std_reg_date { get; set; }

        public string cnt { get; set; }

        public string sims_class { get; set; }

        public string status { get; set; }

        public string sims_seat_cofirm_tran_id { get; set; }

        public bool is_disabled { get; set; }

        public string sims_status { get; set; }

        public DateTime std_communication_date1 { get; set; }

        public DateTime std_follow_date1 { get; set; }
    }

}