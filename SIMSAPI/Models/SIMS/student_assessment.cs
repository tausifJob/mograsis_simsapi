﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SIMSAPI.Models.SIMS
{
    #region Activity Master
    public class sims_activity_students
    {
        public sims_activity_students()
        {
            indicators = new List<sims_indicator>();
        }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_enroll_number { get; set; }
        public string sims_student_name { get; set; }
        public List<sims_indicator> indicators { get; set; }
    }

    public class sims_indicator_comment_master
    {
        public string sims_indicator_comment_code { get; set; }
        public string sims_indicator_group_code { get; set; }
        public string sims_indicator_group_desc { get; set; }
        public string sims_indicator_code { get; set; }
        public string sims_activity_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_term_code { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_comment_desc { get; set; }
        public string sims_comment_desc_ar { get; set; }
        public bool sims_indicator_comment_status { get; set; }
        public bool sims_indicator_comment_delete_status { get; set; }
        public string sims_indicator_group_short_name { get; set; }

        public string sims_indicator_comment_code_old { get; set; }
    }

    public class sims_indicator_group
    {
        public string sims_indicator_group_code { get; set; }
        public string sims_activity_group_code { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_indicator_group_short_name { get; set; }
        public string sims_indicator_group_name { get; set; }
        public string sims_indicator_group_desc { get; set; }
        public string sims_indicator_group_remark { get; set; }
        public string sims_indicator_group_value { get; set; }
        public string sims_status { get; set; }
    }
    public class sims_activity_group_master
    {
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_activity_group_code { get; set; }
        public string sims_activity_group_desc { get; set; }
        public bool sims_activity_group_status { get; set; }
    }
    public class sims_indicator
    {
        public sims_indicator()
        {
            comments = new List<sims_indicator_comment_master>();
        }
        public string sims_config_sr_no { get; set; }
        public string sims_activity_sr_no { get; set; }
        public string narr_grade_group_code { get; set; }
        public string narr_grade_group_name { get; set; }
        public string sims_indicator_name { get; set; }
        public string sims_indicator_desc { get; set; }
        public string sims_indicator_rmk { get; set; }
        public string sims_indicator_max_pnt { get; set; }
        public string sims_indicator_min_pnt { get; set; }
        public bool sims_indicator_status { get; set; }
        public List<sims_indicator_comment_master> comments { get; set; }
        public string sims_indicator_name_ar { get; set; }
    }
    public class sims_activity
    {
        public sims_activity()
        {
            indicators = new List<sims_indicator>();
        }
        public List<sims_indicator> indicators { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_term_code { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_section_code { get; set; }
        public string sims_subject_code { get; set; }
        public bool sims_activity_status { get; set; }
        public string sims_config_sr_no { get; set; }
        public string narr_grade_group_code { get; set; }
        public string sims_activity_sr_no { get; set; }
        public string sims_activity_name { get; set; }
        public string sims_activity_rmk { get; set; }
        public string sims_activity_desc { get; set; }
        public string sims_activity_max_pnt { get; set; }
        public string sims_activity_min_pnt { get; set; }

        public string sims_activity_name_ar { get; set; }
    }
    public class sims_activity_master
    {
        public sims_activity_master()
        {
            lst_activity = new List<sims_activity>();
        }
        public string sims_cur_code { get; set; }
        public string sims_cur_name { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_academic_desc { get; set; }
        public string sims_term_code { get; set; }
        public string sims_term_name { get; set; }
        public string sims_academic_status { get; set; }
        public string sims_grade_code { get; set; }
        public string sims_grade_name { get; set; }
        public string sims_section_code { get; set; }
        public string sims_section_name { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_subject_name { get; set; }
        public string sims_subject_code_lst { get; set; }
        public string sims_type_code { get; set; }
        public string sims_type_name { get; set; }
        public string sims_type_value { get; set; }
        public string sims_config_desc { get; set; }
        public bool sims_activity_status { get; set; }
        public object sims_section_code_lst { get; set; }
        public string sims_acativity_config_name { get; set; }
        public string sims_config_sr_no { get; set; }
        public string sims_activity_sr_no { get; set; }

        public string narr_grade_group_code { get; set; }
        public string narr_grade_group_name { get; set; }
        public string sims_activity_name { get; set; }
        public string sims_activity_rmk { get; set; }
        public string sims_activity_desc { get; set; }
        public string sims_activity_max_pnt { get; set; }
        public string sims_activity_min_pnt { get; set; }
        public List<sims_activity> lst_activity { get; set; }
        public string sims_indicator_name { get; set; }
        public string sims_indicator_desc { get; set; }
        public string sims_indicator_rmk { get; set; }
        public string sims_indicator_max_pnt { get; set; }
        public string sims_indicator_min_pnt { get; set; }
        public bool sims_indicator_status { get; set; }
        public string sims_user_code { get; set; }
        public string sims_activity_name_ar { get; set; }
        public string sims_indicator_name_ar { get; set; }
    }
    public class sims_indicator_copy
    {
        public sims_indicator_copy()
        {
            activity = new List<sims_activity>();
            indicators = new List<sims_indicator>();
        }
        public List<sims_activity> activity { get; set; }
        public List<sims_indicator> indicators { get; set; }
    }
    public class sims_activity_copy
    {
        public sims_activity_copy()
        {
            lst_sims_activity_sr_no = new List<string>();
        }
        public List<string> lst_sims_activity_sr_no;

        public string sims_config_sr_no { get; set; }
        public string sims_cur_code { get; set; }
        public string sims_academic_year { get; set; }
        public string sims_subject_code { get; set; }
        public string sims_term_code { get; set; }
        public string to_subject_code { get; set; }
        public string to_term_code { get; set; }
        public bool isIndicators { get; set; }
        public bool isComments { get; set; }

    }
    #endregion

}