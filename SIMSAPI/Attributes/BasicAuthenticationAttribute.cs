﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using SIMSAPI.Identity;
using SIMSAPI.Models;
using SIMSAPI.Helper;
using SIMSAPI.Models.Common;
using System.Data.SqlClient;

namespace SIMSAPI.Attributes
{
    public class BasicAuthenticationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                string authToken = actionContext.Request.Headers.Authorization.Parameter;
                string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authToken));

                string username = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                string password = decodedToken.Substring(decodedToken.IndexOf(":") + 1);
                DBConnection dbConn = new DBConnection();
                dbConn.Open();
                SqlDataReader dr = dbConn.ExecuteStoreProcedure("[comn].[CheckLogin_proc]",
                   new List<SqlParameter>()
                    {
                        new SqlParameter("@lic_school_code", CommonStaticClass.school_code),
                        new SqlParameter("@UserName",username),
                        new SqlParameter("@Password",password)
                    });
                string result = string.Empty;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        result = dr["Msg"].ToString();
                    }
                }
                else
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

                if (!string.IsNullOrEmpty(result) && result.Equals("True"))
                {
                    HttpContext.Current.User = new GenericPrincipal(new ApiIdentity(new User() { Username = username, Password = password }), new string[] { });
                    base.OnActionExecuting(actionContext);
                }
                else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}